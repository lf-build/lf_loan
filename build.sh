#!/usr/bin/env bash

dnu restore src --ignore-failed-sources --no-cache -s http://107.170.250.178/guestAuth/app/nuget/v1/FeedService.svc/ -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
dnu restore test --ignore-failed-sources --no-cache -s http://107.170.250.178/guestAuth/app/nuget/v1/FeedService.svc/ -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/

# check if path is supplied, otherwise everything
WORKING_PATH="."

if [ -n "$1" ]
then
    WORKING_PATH="$1"
fi

echo "Specified PATH: $WORKING_PATH"

# building all projects
for proj in `find "$WORKING_PATH" -maxdepth 2 -type d | grep -v "artifacts"`
do
    if test -f "$proj/project.json"
    then
        echo "Building $proj:"
        dnu build "$proj"
    fi
done

#read -n 1 -s
