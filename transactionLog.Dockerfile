FROM registry.lendfoundry.com/base:beta8

ADD ./global.json /app/

ADD ./src/LendFoundry.Loans.TransactionLog /app/src/LendFoundry.Loans.TransactionLog
WORKDIR /app/src/LendFoundry.Loans.TransactionLog
RUN eval "$CMD_RESTORE"
RUN dnu build

EXPOSE 5000
ENTRYPOINT dnx kestrel
