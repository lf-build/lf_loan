﻿using AutoMapper;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Client.Adapters;
using LendFoundry.Loans.Client.Models;
using RestSharp;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System;

namespace LendFoundry.Loans.Client
{
    public class LoanService : ILoanService
    {
        static LoanService()
        {
            Mapper.CreateMap<Models.LoanTerms, LoanTerms>()
                .ForMember(dest => dest.LoanAmount, options => options.MapFrom(src => src.LoanAmount))
                .ForMember(dest => dest.Fees, options => options.MapFrom(src => src.Fees.Select(fee => new Fee
                {
                    Amount = fee.Amount,
                    Code = fee.Code,
                    Type = fee.Type
                })));

            Mapper.CreateMap<Models.BankAccount, BankAccount>()
                .ForMember(d => d.EffectiveDate, opt => opt.MapFrom(b => b.EffectiveDate));
            
            Mapper.CreateMap<Models.Fee, Fee>();
            Mapper.CreateMap<Models.Borrower, Borrower>()
                .ForMember(d => d.DateOfBirth, opt => opt.MapFrom(b => b.DateOfBirth.Date));

            Mapper.CreateMap<Models.InstallmentInfo, InstallmentInfo>()
                .ForMember(d => d.Loan, opt => opt.MapFrom(l => Mapper.Map<Loan>(l.Loan)))
                .ForMember(d => d.Installment, opt => opt.MapFrom(l => Mapper.Map<Installment>(l.Installment)));

            Mapper.CreateMap<Models.Loan, Loan>()
                .ForMember(d => d.Terms, opt => opt.MapFrom(l => Mapper.Map<LoanTerms>(l.Terms)))
                .ForMember(d => d.Status, opt => opt.MapFrom(l => new LoanStatus(l.Status.Code, l.Status.Description)))
                .ForMember(d => d.Borrowers, opt => opt.MapFrom(l => Mapper.Map<List<Borrower>>(l.Borrowers)))
                .ForMember(d => d.BankAccounts, opt => opt.MapFrom(l => Mapper.Map<List<BankAccount>>(l.BankInfo)));

            Mapper.CreateMap<Models.Address, Address>();
            Mapper.CreateMap<Models.Phone, Phone>();
            Mapper.CreateMap<Models.Installment, Installment>();
            Mapper.CreateMap<LoanTransaction, Transaction>()
                .ForMember(dest => dest.LoanReferenceNumber, options => options.MapFrom(src => src.LoanReference))
                .ForMember(dest => dest.Timestamp, options => options.MapFrom(src => src.Timestamp));
        }

        public LoanService(IServiceClient client, ILogger logger)
        {
            Client = client;
            Logger = logger;
        }

        private ILogger Logger { get; }

        private IServiceClient Client { get; }

        public ILoanDetails GetLoanDetails(string referenceNumber)
        {
            if (string.IsNullOrEmpty(referenceNumber))
                throw new ArgumentNullException(nameof(referenceNumber));

            var request = new RestRequest("{referenceNumber}", Method.GET) { RequestFormat = DataFormat.Json };
            request.AddUrlSegment(nameof(referenceNumber), referenceNumber);
            request.AddHeader("Content-Type", "application/json");
            var loanDetails = Client.Execute<Models.LoanDetails>(request);

            return new LoanDetails
            {
                LoanInfo = new LoanInfoAdapter(loanDetails.Info),
                Transactions = Mapper.Map<List<Transaction>>(loanDetails.Transactions),
                PaymentSchedule = new PaymentSchedule
                {
                    LoanReferenceNumber = loanDetails.Info.ReferenceNumber,
                    Installments = Mapper.Map<List<Installment>>(loanDetails.Schedule)
                }
            };
        }

        public ILoanInfo GetLoanInfo(string referenceNumber)
        {
            if (string.IsNullOrEmpty(referenceNumber))
                throw new ArgumentNullException(nameof(referenceNumber));

            var request = new RestRequest("{referenceNumber}/info", Method.GET) { RequestFormat = DataFormat.Json };
            request.AddUrlSegment(nameof(referenceNumber), referenceNumber);
            var loanInfo = Client.Execute<LoanInfo>(request);

            return new LoanInfoAdapter(loanInfo);
        }

        public IEnumerable<IInstallment> GetPaymentSchedule(string referenceNumber)
        {
            if (string.IsNullOrEmpty(referenceNumber))
                throw new ArgumentNullException(nameof(referenceNumber));

            var request = new RestRequest("{referenceNumber}/schedule", Method.GET) { RequestFormat = DataFormat.Json };
            request.AddUrlSegment(nameof(referenceNumber), referenceNumber);
            return Client.Execute<List<Installment>>(request);
        }

        public IEnumerable<ITransaction> GetTransactions(string referenceNumber)
        {
            if (string.IsNullOrEmpty(referenceNumber))
                throw new ArgumentNullException(nameof(referenceNumber));

            var request = new RestRequest("{referenceNumber}/transactions", Method.GET) { RequestFormat = DataFormat.Json };
            request.AddUrlSegment(nameof(referenceNumber), referenceNumber);
            var transactions = Client.Execute<List<LoanTransaction>>(request);
            return Mapper.Map<List<Transaction>>(transactions);
        }

        public IEnumerable<ILoanInfo> GetLoansDueIn(DateTime date)
        {
            List<LoanInfo> loans = null;
            try
            {
                var request = new RestRequest("/filters/due-in/{dueDate}");
                request.AddUrlSegment("dueDate", date.ToString("yyyy-MM-dd"));

                loans = Client.Execute<List<LoanInfo>>(request);
                return loans.Select(l => new LoanInfoAdapter(l)).ToList();
            }
            catch (ClientException exception)
            {
                Logger.Error("GetLoansDueIn request failed", exception);
                throw;
            }
            catch (Exception ex)
            {
                Logger.Error("GetLoansDue Adapter failed", ex, new { loans });
                return new List<ILoanInfo>();
            }
        }

        public IEnumerable<IInstallmentInfo> GetInstallmentsDueIn(DateTime date, PaymentMethod paymentMethod)
        {
            try
            {
                var request = new RestRequest("/filters/due-in/{dueDate}/{paymentMethod}/installments");
                request.AddUrlSegment("dueDate", date.ToString("yyyy-MM-dd"));
                request.AddUrlSegment(nameof(paymentMethod), paymentMethod.ToString());
                var installments = Client.Execute<List<Models.InstallmentInfo>>(request);



                return Mapper.Map<List<InstallmentInfo>>(installments);
            }
            catch (Exception exception)
            {
                Logger.Error("GetInstallmentsDueIn request failed", exception);
                throw;
            }
        }

        public IEnumerable<IInstallmentInfo> GetInstallmentsDueIn(DateTime date, PaymentMethod paymentMethod, InstallmentType type)
        {
            try
            {
                var request = new RestRequest("/filters/due-in/{dueDate}/{paymentMethod}/installments/{type}");
                request.AddUrlSegment("dueDate", date.ToString("yyyy-MM-dd"));
                request.AddUrlSegment(nameof(paymentMethod), paymentMethod.ToString());
                request.AddUrlSegment(nameof(type), type.ToString());
                var installments = Client.Execute<List<Models.InstallmentInfo>>(request);
                return Mapper.Map<List<InstallmentInfo>>(installments);
            }
            catch (Exception exception)
            {
                Logger.Error("GetInstallmentsDueIn request failed", exception);
                throw;
            }
        }

        public IEnumerable<IInstallmentInfo> GetInstallmentsDueInByAnniversaryDate(DateTime date, PaymentMethod paymentMethod, InstallmentType type)
        {
            try
            {
                var request = new RestRequest("/filters/anniversaryDate/{dueDate}/{paymentMethod}/installments/{type}");
                request.AddUrlSegment("dueDate", date.ToString("yyyy-MM-dd"));
                request.AddUrlSegment(nameof(paymentMethod), paymentMethod.ToString());
                request.AddUrlSegment(nameof(type), type.ToString());
                var installments = Client.Execute<List<Models.InstallmentInfo>>(request);
                return Mapper.Map<List<InstallmentInfo>>(installments);
            }
            catch (Exception exception)
            {
                Logger.Error("GetInstallmentsDueInByAnniversaryDate request failed", exception);
                throw;
            }
        }

        public void UpdateInstallmentStatus(string loanReferenceNumber, DateTimeOffset dueDate, InstallmentStatus newStatus)
        {
            if (string.IsNullOrEmpty(loanReferenceNumber))
                throw new ArgumentNullException(nameof(loanReferenceNumber));

            var request = new RestRequest("{loanReferenceNumber}/installment/{dueDate}/status", Method.PUT);
            request.AddUrlSegment(nameof(loanReferenceNumber), loanReferenceNumber);
            request.AddUrlSegment(nameof(dueDate), dueDate.ToString("yyyy-MM-dd"));
            request.AddJsonBody(newStatus.ToString());
            Client.ExecuteAsync(request);
        }

        public IPayoffAmount GetPayoffAmount(string loanReferenceNumber, DateTime date)
        {
            if (string.IsNullOrWhiteSpace(loanReferenceNumber))
                throw new ArgumentNullException(nameof(loanReferenceNumber));

            if (date.Ticks == 0)
                throw new ArgumentException($"Invalid date: {date.ToString(CultureInfo.InvariantCulture)}", nameof(date));

            var request = new RestRequest("{loanReferenceNumber}/payoff/amount/{date}", Method.GET) { RequestFormat = DataFormat.Json };
            request.AddUrlSegment(nameof(loanReferenceNumber), loanReferenceNumber);
            request.AddUrlSegment(nameof(date), date.ToString("yyyy-MM-dd"));
            return Client.Execute<PayoffAmount>(request);
        }

        public void AddBank(string referenceNumber, BankAccount bankAccount)
        {
            var request = new RestRequest("{referenceNumber}/bank", Method.PUT) { RequestFormat = DataFormat.Json };
            request.AddUrlSegment(nameof(referenceNumber), referenceNumber);
            request.AddJsonBody(bankAccount);
            Client.ExecuteAsync(request);
        }

        public void SetBankAsPrimary(string referenceNumber, string accountNumber)
        {
            var request = new RestRequest("{referenceNumber}/bank/{accountNumber}/primary", Method.POST) { RequestFormat = DataFormat.Json };
            request.AddUrlSegment(nameof(referenceNumber), referenceNumber);
            request.AddUrlSegment(nameof(accountNumber), accountNumber);
            Client.ExecuteAsync(request);
        }

        public LoanStatus GetStatus(string referenceNumber)
        {
            var request = new RestRequest("{referenceNumber}/status", Method.GET) { RequestFormat = DataFormat.Json };
            request.AddUrlSegment(nameof(referenceNumber), referenceNumber);
            return Client.Execute<LoanStatus>(request);
        }

        public LoanStatus GetStatusAttributes(string referenceNumber)
        {
            var request = new RestRequest("{referenceNumber}/status/attributes", Method.GET) { RequestFormat = DataFormat.Json };
            request.AddUrlSegment(nameof(referenceNumber), referenceNumber);
            return Client.Execute<LoanStatus>(request);
        }

        public void UpdateLoanStatus(string referenceNumber, string nextStatus)
        {
            var request = new RestRequest("{referenceNumber}/status/{nextStatus}", Method.POST) { RequestFormat = DataFormat.Json };
            request.AddUrlSegment(nameof(referenceNumber), referenceNumber);
            request.AddUrlSegment(nameof(nextStatus), nextStatus);
            Client.Execute(request);
        }

        public void UpdateLoanStatus(string referenceNumber, string nextStatus, DateTimeOffset? startDate, DateTimeOffset? endDate)
        {
            var request = new RestRequest("{referenceNumber}/status/{nextStatus}/{startDate?}/{endDate?}", Method.POST) { RequestFormat = DataFormat.Json };
            request.AddUrlSegment(nameof(referenceNumber), referenceNumber);
            request.AddUrlSegment(nameof(nextStatus), nextStatus);

            if (startDate.HasValue)
                request.AddUrlSegment(nameof(startDate), startDate.Value.ToString());

            if (endDate.HasValue)
                request.AddUrlSegment(nameof(endDate), endDate.ToString());
            Client.Execute(request);
        }

        public void UpdatePaymentMethod(string referenceNumber, PaymentMethod paymentMethod)
        {
            var request = new RestRequest("{referenceNumber}/change/{paymentMethod}", Method.POST) { RequestFormat = DataFormat.Json };
            request.AddUrlSegment(nameof(referenceNumber), referenceNumber);
            request.AddUrlSegment(nameof(paymentMethod), paymentMethod.ToString());
            Client.ExecuteAsync(request);
        }

        public bool IsAllowedAction(string referenceNumber, Status.Actions actionType)
        {
            var request = new RestRequest("{referenceNumber}/is-allowed/{actionType}", Method.GET) { RequestFormat = DataFormat.Json };
            request.AddUrlSegment(nameof(referenceNumber), referenceNumber);
            request.AddUrlSegment(nameof(actionType), actionType.ToString());
            return Client.Execute(request);
        }

        public LoanSummary GetLoanSummary(string referenceNumber)
        {
            if (string.IsNullOrEmpty(referenceNumber))
                throw new ArgumentNullException(nameof(referenceNumber));
            var request = new RestRequest("{referenceNumber}/summary", Method.GET) { RequestFormat = DataFormat.Json };
            request.AddUrlSegment(nameof(referenceNumber), referenceNumber);
            return Client.Execute<LoanSummary>(request);
        }
    }
}