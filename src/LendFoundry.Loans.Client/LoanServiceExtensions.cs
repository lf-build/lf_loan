﻿using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Loans.Client
{
    public static class LoanServiceExtensions
    {
        public static IServiceCollection AddLoanService(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<ILoanServiceFactory>(p => new LoanServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ILoanServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
