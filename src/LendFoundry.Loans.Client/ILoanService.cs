﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Client
{
    public interface ILoanService
    {
        ILoanDetails GetLoanDetails(string referenceNumber);
        ILoanInfo GetLoanInfo(string referenceNumber);
        IEnumerable<ITransaction> GetTransactions(string loanReferenceNumber);
        IEnumerable<IInstallment> GetPaymentSchedule(string loanReferenceNumber);        
        IEnumerable<ILoanInfo> GetLoansDueIn(DateTime date);
        IEnumerable<IInstallmentInfo> GetInstallmentsDueIn(DateTime date, PaymentMethod paymentMethod);
        IEnumerable<IInstallmentInfo> GetInstallmentsDueIn(DateTime date, PaymentMethod paymentMethod, InstallmentType type);

        IEnumerable<IInstallmentInfo> GetInstallmentsDueInByAnniversaryDate(DateTime date, PaymentMethod paymentMethod, InstallmentType type);

        IPayoffAmount GetPayoffAmount(string loanReferenceNumber, DateTime date);
        void AddBank(string referenceNumber, BankAccount bankAccount);
        void SetBankAsPrimary(string referenceNumber, string accountNumber);
        LoanStatus GetStatus(string referenceNumber);
        LoanStatus GetStatusAttributes(string referenceNumber);
        void UpdateLoanStatus(string referenceNumber, string nextStatus);
        void UpdateLoanStatus(string referenceNumber, string nextStatus, DateTimeOffset? startDate, DateTimeOffset? endDate);
        void UpdatePaymentMethod(string referenceNumber, PaymentMethod paymentMethod);
        bool IsAllowedAction(string referenceNumber, Status.Actions actionType);

        LoanSummary GetLoanSummary(string referenceNumber);
    }
}
