﻿namespace LendFoundry.Loans.Client.Models
{
    public class Loan
    {
        public string ReferenceNumber { get; set; }
        public string FundingSource { get; set; }
        public string Purpose { get; set; }
        public LoanStatus Status { get; set; }
        public LoanInvestor Investor { get; set; }
        public Score[] Scores { get; set; }
        public string Grade { get; set; }
        public double PreCloseDti { get; set; }
        public double PostCloseDti { get; set; }
        public double MonthlyIncome { get; set; }
        public string HomeOwnership { get; set; }
        public string CampaignCode { get; set; }
        public Borrower[] Borrowers { get; set; }
        public LoanTerms Terms { get; set; }
        public BankAccount[] BankInfo { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
    }
}