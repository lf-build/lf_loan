﻿using System;

namespace LendFoundry.Loans.Client.Models
{
    public class Borrower
    {
        public string ReferenceNumber { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public DateTimeOffset DateOfBirth { get; set; }
        public string SSN { get; set; }
        public string Email { get; set; }
        public bool IsPrimary { get; set; }
        public Address[] Addresses { get; set; }
        public Phone[] Phones { get; set; }
    }
}
