﻿namespace LendFoundry.Loans.Client.Models
{
    public class LoanStatus
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
