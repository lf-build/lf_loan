﻿using System;
using Newtonsoft.Json;

namespace LendFoundry.Loans.Client.Models
{
    public class LoanInfo
    {
        public string ReferenceNumber { get; set; }
        public LoanStatus Status { get; set; }
        public string FundingSource { get; set; }
        public string Purpose { get; set; }
        public LoanInvestor Investor { get; set; }
        public Score[] Scores { get; set; }
        public string Grade { get; set; }
        public double? PreCloseDti { get; set; }
        public double? PostCloseDti { get; set; }
        public double MonthlyIncome { get; set; }
        public string HomeOwnership { get; set; }
        public string CampaignCode { get; set; }
        public Borrower[] Borrowers { get; set; }
        public LoanTerms Terms { get; set; }

        /// <summary>
        /// Unusually in this field has added a JsonAttribute to specify to the Serializer should fill this field with the Property BankInfo
        /// </summary>
        [JsonProperty(PropertyName = "BankInfo")]
        public BankAccount[] BankAccounts { get; set; }

        public PaymentMethod PaymentMethod { get; set; }
        public DateTimeOffset? NextDueDate { get; set; }
        public double NextInstallmentAmount { get; set; }
        public double CurrentDue { get; set; }
        public int DaysPastDue { get; set; }
        public DateTimeOffset? LastPaymentDate { get; set; }
        public double LastPaymentAmount { get; set; }
        public double RemainingBalance { get; set; }
        public bool IsInGracePeriod { get; set; }
    }
}