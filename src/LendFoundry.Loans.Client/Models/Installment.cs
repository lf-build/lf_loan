﻿using System;

namespace LendFoundry.Loans.Client.Models
{
    public class Installment : IInstallment
    {
        public PaymentMethod PaymentMethod { get; set; }
        public DateTimeOffset? AnniversaryDate { get; set; }
        public DateTimeOffset? DueDate { get; set; }
        public DateTimeOffset? PaymentDate { get; set; }
        public string PaymentId { get; set; }
        public double OpeningBalance { get; set; }
        public double PaymentAmount { get; set; }
        public double Principal { get; set; }
        public double Interest { get; set; }
        public double EndingBalance { get; set; }
        public InstallmentStatus Status { get; set; }
        public InstallmentType Type { get; set; }
    }
}
