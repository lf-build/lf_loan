﻿namespace LendFoundry.Loans.Client.Models
{
    public class Phone
    {
        public PhoneType Type { get; set; }
        public string Number { get; set; }
        public bool IsPrimary { get; set; }
    }
}
