﻿namespace LendFoundry.Loans.Client.Models
{
    public class Fee
    {
        public string Code { get; set; }
        public FeeType Type { get; set; }
        public double Amount { get; set; }
    }
}
