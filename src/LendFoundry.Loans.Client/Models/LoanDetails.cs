﻿namespace LendFoundry.Loans.Client.Models
{
    public class LoanDetails
    {
        public LoanInfo Info { get; set; }
        public LoanTransaction[] Transactions { get; set; }
        public Installment[] Schedule { get; set; }
    }
}
