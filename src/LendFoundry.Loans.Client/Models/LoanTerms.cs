﻿using System;

namespace LendFoundry.Loans.Client.Models
{
    public class LoanTerms
    {
        public double LoanAmount { get; set; }
        public DateTimeOffset ApplicationDate { get; set; }
        public DateTimeOffset OriginationDate { get; set; }
        public DateTimeOffset FundedDate { get; set; }
        public int Term { get; set; }
        public double Rate { get; set; }
        public RateType RateType { get; set; }
        public Fee[] Fees { get; set; }
        public double APR { get; set; }
        public double PaymentAmount { get; set; }
    }
}
