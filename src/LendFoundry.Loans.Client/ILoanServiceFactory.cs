using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Client
{
    public interface ILoanServiceFactory
    {
        ILoanService Create(ITokenReader reader);        
    }
}