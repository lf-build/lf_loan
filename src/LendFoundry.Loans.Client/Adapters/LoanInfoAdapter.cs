﻿using AutoMapper;
using LendFoundry.Loans.Client.Models;
using LendFoundry.Loans.Schedule;
using System.Collections.Generic;

namespace LendFoundry.Loans.Client.Adapters
{
    public class LoanInfoAdapter : ILoanInfo
    {
        public LoanInfoAdapter(LoanInfo model)
        {
            Loan = new Loan
            {
                ReferenceNumber = model.ReferenceNumber,
                Status = new LoanStatus(model.Status.Code, model.Status.Description),
                Borrowers = Mapper.Map<List<Borrower>>(model.Borrowers),
                Terms = Mapper.Map<LoanTerms>(model.Terms),
                Purpose = model.Purpose,
                Investor = model.Investor,
                BankAccounts = Mapper.Map<List<BankAccount>>(model.BankAccounts),
                CampaignCode = model.CampaignCode,
                FundingSource = model.FundingSource,
                Grade = model.Grade,
                HomeOwnership = model.HomeOwnership,
                MonthlyIncome = model.MonthlyIncome,
                PaymentMethod = model.PaymentMethod,
                PreCloseDti = model.PreCloseDti,
                PostCloseDti = model.PostCloseDti,
                Scores =  model.Scores
            };

            Summary = new PaymentScheduleSummary
            {
                CurrentDue = model.CurrentDue,
                DaysPastDue = model.DaysPastDue,
                LastPaymentAmount = model.LastPaymentAmount,
                LastPaymentDate = model.LastPaymentDate,
                NextDueDate = model.NextDueDate,
                NextInstallmentAmount = model.NextInstallmentAmount,
                RemainingBalance = model.RemainingBalance,
                IsInGracePeriod = model.IsInGracePeriod
            };
        }

        public ILoan Loan { get; set; }
        public IPaymentScheduleSummary Summary { get; set; }
    }
}