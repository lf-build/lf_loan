﻿using System;

namespace LendFoundry.Loans.Schedule
{
    public class InstallmentNotFoundException : ArgumentException
    {
        public InstallmentNotFoundException(string message) : base(message)
        {
        }

        public InstallmentNotFoundException(string paymentId, string loanReferenceNumber) : base($"Installment with payment id #{paymentId} not found for loan #{loanReferenceNumber}")
        {
        }
    }
}
