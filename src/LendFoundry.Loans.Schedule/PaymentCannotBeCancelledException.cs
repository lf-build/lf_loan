﻿using System;

namespace LendFoundry.Loans.Schedule
{
    public class PaymentCannotBeCancelledException : Exception
    {
        public PaymentCannotBeCancelledException(string message) : base(message)
        {
        }
    }
}
