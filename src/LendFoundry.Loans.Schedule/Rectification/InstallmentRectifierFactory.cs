﻿using LendFoundry.Amortization.Client;
using LendFoundry.Loans.Schedule.Interest;
using LendFoundry.Loans.Schedule.Rectification.Strategies;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Schedule.Rectification
{
    internal class InstallmentRectifierFactory : IInstallmentRectifierFactory
    {
        public InstallmentRectifierFactory(IAmortizationService amortizationService, EarlyPaymentInterestCalculator earlyPaymentInterestCalculator)
        {
            Rectifiers = new IInstallmentRectifier[]
            {
                new ExtraPaymentRectifier(),
                new PayoffRectifier(),
                new CompletedScheduledInstallmentRectifier(),
                new EarlyCheckPaymentRectifier(earlyPaymentInterestCalculator),
                new ReamortizationRectifier(amortizationService)
            };
        }

        private IInstallmentRectifier[] Rectifiers { get; }

        public IEnumerable<IInstallmentRectifier> Create(IInstallment installment, ScheduleRectificationContext context)
        {
            return Rectifiers.Where(r => r.CanRectify(installment, context));
        }
    }
}
