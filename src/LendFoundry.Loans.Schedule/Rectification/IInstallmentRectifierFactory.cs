﻿using System.Collections.Generic;

namespace LendFoundry.Loans.Schedule.Rectification
{
    internal interface IInstallmentRectifierFactory
    {
        IEnumerable<IInstallmentRectifier> Create(IInstallment installment, ScheduleRectificationContext context);
    }
}
