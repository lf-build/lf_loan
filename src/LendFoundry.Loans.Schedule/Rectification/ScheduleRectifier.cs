﻿using LendFoundry.Amortization.Client;
using LendFoundry.Loans.Schedule.Rectification.Policies;
using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Schedule.Rectification
{
    internal class ScheduleRectifier : IScheduleRectifier
    {
        public ScheduleRectifier(IInstallmentRectifierFactory installmentRectifierFactory)
        {
            InstallmentRectifierFactory = installmentRectifierFactory;
        }

        private IInstallmentRectifierFactory InstallmentRectifierFactory { get; }

        public void RectifySchedule(List<IInstallment> installments, IInstallment referenceInstallment, ILoan loan)
        {
            RectifySchedule(installments, referenceInstallment, loan, NoRectificationPolicy.Instance);
        }

        public void RectifySchedule(List<IInstallment> installments, IInstallment referenceInstallment, ILoan loan, IRectificationPolicy rectificationPolicy)
        {
            installments.Sort(InstallmentComparer.Instance);

            rectificationPolicy.BeforeRectification(installments);

            new Iterator<IInstallment>(installments)
                .StartingOn(referenceInstallment)
                .ForEach((cursor) =>
                {
                    var installment = cursor.Current;

                    var context = new ScheduleRectificationContext
                    {
                        Loan = loan,
                        Installments = installments,
                        ReferenceInstallment = referenceInstallment,
                        PreviousInstallment = cursor.Previous,
                        NextInstallment = cursor.Next,
                        IsLastInstallment = cursor.IsLast,
                        RectificationPolicy = rectificationPolicy
                    };

                    var installmentRectifiers = InstallmentRectifierFactory.Create(installment, context);

                    foreach(var installmentRectifier in installmentRectifiers)
                        installmentRectifier.Rectify(installment, context);
                });
        }

        private class Iterator<T>
        {
            private const int BeforeFirstItem = -1;

            public Iterator(List<T> items)
            {
                Items = items;
            }

            private List<T> Items { get; }
            private int StartIndex { get; set; }

            public Iterator<T> StartingOn(T item)
            {
                StartIndex = item == null ? BeforeFirstItem : Items.IndexOf(item);
                return this;
            }

            public void ForEach(Action<Cursor<T>> action)
            {
                var index = StartIndex;
                do
                {
                    var current = index >= 0 && index < Items.Count ? Items[index] : default(T);
                    var previous = index > 0 ? Items[index - 1] : default(T);
                    var next = index < Items.Count - 1 ? Items[index + 1] : default(T);
                    var isLast = index == Items.Count - 1;

                    action(new Cursor<T>
                    {
                        Current = current,
                        Previous = previous,
                        Next = next,
                        IsLast = isLast
                    });

                    index++;
                }
                while (index < Items.Count);
            }
        }

        private class Cursor<T>
        {
            public T Current { get; internal set; }
            public bool IsLast { get; internal set; }
            public T Next { get; internal set; }
            public T Previous { get; internal set; }
        }
    }
}
