﻿using System.Collections.Generic;

namespace LendFoundry.Loans.Schedule.Rectification
{
    internal interface IRectificationPolicy
    {
        void BeforeRectification(List<IInstallment> schedule);
        void AfterReamortization(List<IInstallment> schedule);
    }
}
