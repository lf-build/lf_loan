﻿using System.Collections.Generic;

namespace LendFoundry.Loans.Schedule.Rectification
{
    internal interface IScheduleRectifier
    {
        void RectifySchedule(List<IInstallment> installments, IInstallment referenceInstallment, ILoan loan);
        void RectifySchedule(List<IInstallment> installments, IInstallment referenceInstallment, ILoan loan, IRectificationPolicy rectificationPolicy);
    }
}
