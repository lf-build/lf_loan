﻿using System.Collections.Generic;

namespace LendFoundry.Loans.Schedule.Rectification
{
    internal class ScheduleRectificationContext
    {
        public ILoan Loan { get; internal set; }
        public List<IInstallment> Installments { get; internal set; }
        public IInstallment NextInstallment { get; internal set; }
        public IInstallment PreviousInstallment { get; internal set; }
        public IInstallment ReferenceInstallment { get; internal set; }
        public bool IsLastInstallment { get; internal set; }
        public IRectificationPolicy RectificationPolicy { get; internal set; }
    }
}
