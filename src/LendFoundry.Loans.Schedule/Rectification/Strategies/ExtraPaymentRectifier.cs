﻿using System;

namespace LendFoundry.Loans.Schedule.Rectification.Strategies
{
    internal class ExtraPaymentRectifier : IInstallmentRectifier
    {
        public bool CanRectify(IInstallment installment, ScheduleRectificationContext context)
        {
            return installment?.Type == InstallmentType.Additional;
        }

        public void Rectify(IInstallment installment, ScheduleRectificationContext context)
        {
            if (!CanRectify(installment, context))
                throw new ArgumentException($"Cannot rectify installment of type: {installment.Type}");

            //TODO: P&I extra payment
            var interest = 0.0;

            installment.Principal = installment.PaymentAmount - interest;
            installment.OpeningBalance = context.PreviousInstallment?.EndingBalance ?? context.Loan.Terms.LoanAmount;
            installment.EndingBalance = installment.OpeningBalance - installment.Principal;
        }
    }
}
