﻿namespace LendFoundry.Loans.Schedule.Rectification.Strategies
{
    internal class PayoffRectifier : IInstallmentRectifier
    {
        public bool CanRectify(IInstallment installment, ScheduleRectificationContext context)
        {
            return installment?.Type == InstallmentType.Payoff;
        }

        public void Rectify(IInstallment installment, ScheduleRectificationContext context)
        {
            context.Installments.RemoveAll(i => i.DueDate >= installment.DueDate && i.Type != InstallmentType.Payoff);
        }
    }
}
