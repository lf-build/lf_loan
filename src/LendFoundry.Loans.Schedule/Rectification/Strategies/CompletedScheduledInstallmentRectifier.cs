﻿using System;
using System.Linq;

namespace LendFoundry.Loans.Schedule.Rectification.Strategies
{
    internal class CompletedScheduledInstallmentRectifier : IInstallmentRectifier
    {
        public bool CanRectify(IInstallment installment, ScheduleRectificationContext context)
        {
            return
                installment?.Type == InstallmentType.Scheduled &&
                installment?.Status == InstallmentStatus.Completed;
        }

        /// <remarks>
        /// When a payment is cancelled before completed scheduled installments, they must be rectified
        /// since the ending balance of the cancelled payment may change. Completed installments are
        /// not removed from the schedule after reamortization, which means at that point we have
        /// one duplicate installment for each completed scheduled installment. So we copy the
        /// opening balance and the interest from the scheduled installment to the completed installment
        /// and then rectify the principal and the ending balance.
        /// </remarks>
        public void Rectify(IInstallment installment, ScheduleRectificationContext context)
        {
            var referenceDate = context.ReferenceInstallment?.DueDate ?? context.Loan.Terms.OriginationDate;

            var remainingInstallments = context.Installments
                .Where(i => i.DueDate > referenceDate)
                .Where(i => i.Type == InstallmentType.Scheduled);

            var scheduledInstallments = remainingInstallments
                .Where(i => i.Status == InstallmentStatus.Scheduled);

            var completedInstallments = remainingInstallments
                .Where(i => i.Status == InstallmentStatus.Completed);

            completedInstallments.ToList().ForEach(payment =>
            {
                var scheduledInstallment = scheduledInstallments.SingleOrDefault(i => i.AnniversaryDate == payment.AnniversaryDate);

                if (scheduledInstallment != null)
                {
                    payment.OpeningBalance = scheduledInstallment.OpeningBalance;
                    payment.Interest = scheduledInstallment.Interest;
                    payment.Principal = Math.Round(payment.PaymentAmount - payment.Interest, 2, MidpointRounding.AwayFromZero);
                    payment.EndingBalance = Math.Round(payment.OpeningBalance - payment.Principal, 2, MidpointRounding.AwayFromZero);

                    context.Installments.Remove(scheduledInstallment);
                }
            });
        }
    }
}
