﻿using LendFoundry.Loans.Schedule.Interest;
using System;

namespace LendFoundry.Loans.Schedule.Rectification.Strategies
{
    internal class EarlyCheckPaymentRectifier : IInstallmentRectifier
    {
        public EarlyCheckPaymentRectifier(EarlyPaymentInterestCalculator interestCalculator)
        {
            InterestCalculator = interestCalculator;
        }

        private EarlyPaymentInterestCalculator InterestCalculator { get; }

        public bool CanRectify(IInstallment installment, ScheduleRectificationContext context)
        {
            return installment?.Type == InstallmentType.Scheduled && 
                   installment?.PaymentMethod == PaymentMethod.Check && 
                   installment?.PaymentDate != null && 
                   installment?.PaymentDate < installment.AnniversaryDate;
        }

        public void Rectify(IInstallment installment, ScheduleRectificationContext context)
        {
            installment.OpeningBalance = context.PreviousInstallment?.EndingBalance ?? context.Loan.Terms.LoanAmount;
            installment.Interest = InterestCalculator.GetInterest(installment, installment.PaymentDate.Value, installment.PaymentAmount, context.Loan, context.Installments);
            installment.Principal = Math.Round(installment.PaymentAmount - installment.Interest, 2, MidpointRounding.AwayFromZero);
            installment.EndingBalance = Math.Round(installment.OpeningBalance - installment.Principal, 2, MidpointRounding.AwayFromZero);
        }
    }
}
