﻿using LendFoundry.Amortization;
using LendFoundry.Amortization.Client;
using LendFoundry.Amortization.Interest;
using System;
using System.Collections.Generic;
using System.Linq;
using AmortizationInstallment = LendFoundry.Amortization.IInstallment;

namespace LendFoundry.Loans.Schedule.Rectification.Strategies
{
    internal class ReamortizationRectifier : IInstallmentRectifier
    {
        public ReamortizationRectifier(IAmortizationService amortizationService)
        {
            AmortizationService = amortizationService;
        }

        private IAmortizationService AmortizationService { get; }

        public bool CanRectify(IInstallment installment, ScheduleRectificationContext context)
        {
            var doesCurrentInstallmentTriggerReamortization =
                installment == context.ReferenceInstallment ||
                installment?.Type == InstallmentType.Additional ||
                installment?.Status == InstallmentStatus.Completed ||
                IsEarlyPayment(installment);

            var doesNextInstallmentTriggerReamortization =
                IsScheduled(context.NextInstallment) ||
                context.IsLastInstallment;

            return doesCurrentInstallmentTriggerReamortization &&
                   doesNextInstallmentTriggerReamortization;
        }

        private bool IsScheduled(IInstallment installment)
        {
            return installment?.Type == InstallmentType.Scheduled && !IsEarlyPayment(installment);
        }

        public void Rectify(IInstallment installment, ScheduleRectificationContext context)
        {
            Reamortize(context.Installments, installment, context.Loan);
            context.RectificationPolicy.AfterReamortization(context.Installments);
        }

        private void Reamortize(List<IInstallment> installments, IInstallment referenceInstallment, ILoan loan)
        {
            var loanTerms = new Amortization.LoanTerms
            {
                OriginationDate = loan.Terms.OriginationDate,
                LoanAmount = loan.Terms.LoanAmount,
                Term = loan.Terms.Term,
                Rate = loan.Terms.Rate
            };

            var request = new AmortizationRequest
            {
                LoanTerms = loanTerms,
                ReferenceDate = GetReferenceDate(referenceInstallment, installments),
                PrincipalBalance = referenceInstallment?.EndingBalance,
            };

            if (referenceInstallment != null)
                request.OpeningInterest = GetInterestForNextScheduledInstallment(referenceInstallment, installments, loanTerms);

            var remainingInstallments = AmortizationService.Amortize(request).ToList();
            var referenceDate = referenceInstallment?.DueDate ?? loan.Terms.OriginationDate;

            // Remove scheduled installments
            installments.RemoveAll(i =>
                i.DueDate > referenceDate &&
                (i.PaymentDate == null || i.PaymentDate > referenceDate) && //don't remove any early payment before the reference date
                i.Type == InstallmentType.Scheduled &&
                i.Status == InstallmentStatus.Scheduled);

            // Add new installments from reamortization
            installments.AddRange(remainingInstallments.Select(i => CreateInstallment(i, loan)));

            // Sort them by due date
            installments.Sort(InstallmentComparer.Instance);
        }

        private static DateTimeOffset? GetReferenceDate(IInstallment referenceInstallment, List<IInstallment> installments)
        {
            if (referenceInstallment == null)
                return null;

            var earlyPayment = GetEarlyPaymentAcross(referenceInstallment.DueDate.Value, installments);

            return earlyPayment?.AnniversaryDate ?? referenceInstallment.DueDate;
        }

        private static IInstallment GetEarlyPaymentAcross(DateTimeOffset date, List<IInstallment> installments)
        {
            return installments
                .Where(i => date >= i.PaymentDate)
                .Where(i => date < i.AnniversaryDate)
                .FirstOrDefault();
        }

        //TODO: Refactoring: interest calculation code in EarlyPaymentInterestCalculator is a duplicate of this code
        private double? GetInterestForNextScheduledInstallment(IInstallment referenceInstallment, IEnumerable<IInstallment> installments, Amortization.ILoanTerms loanTerms)
        {
            if (IsLastInstallment(referenceInstallment) || (referenceInstallment.Type != InstallmentType.Additional && !IsEarlyPayment(referenceInstallment)))
                return null;

            var previous = GetPreviousInterestInstallment(referenceInstallment, installments);
            var next = GetNextInterestInstallment(referenceInstallment, installments);

            var start = GetInterestPaymentDate(previous) ?? loanTerms.OriginationDate;
            var end = next?.AnniversaryDate ?? next?.DueDate;

            var interestAccrualPeriods = new List<InterestAccrualPeriod>();

            if (start == loanTerms.OriginationDate)
            {
                interestAccrualPeriods.Add(new InterestAccrualPeriod
                {
                    Principal = loanTerms.LoanAmount,
                    Start = loanTerms.OriginationDate
                });
            }

            var buckets = GetBuckets(GetInstallments(installments, start, end));
            foreach (var bucket in buckets)
            {
                interestAccrualPeriods.Add(new InterestAccrualPeriod
                {
                    Principal = bucket.Principal.Value,
                    Start = bucket.Date
                });
            }

            return AmortizationService.GetSimpleInterest(loanTerms, interestAccrualPeriods);
        }

        private static IEnumerable<IInstallment> GetInstallments(IEnumerable<IInstallment> installments, DateTimeOffset start, DateTimeOffset? end)
        {
            var buckets = installments.Where(i => GetInterestPaymentDate(i) >= start);

            if (end != null)
                buckets = buckets.Where(i => GetInterestPaymentDate(i) < end);

            return buckets;
        }

        private static IInstallment GetNextInterestInstallment(IInstallment referenceInstallment, IEnumerable<IInstallment> installments)
        {
            return installments
                .Where(i => GetInterestPaymentDate(i) > GetInterestPaymentDate(referenceInstallment))
                .Where(i => i.Interest > 0.0)
                .FirstOrDefault();
        }

        private static IInstallment GetPreviousInterestInstallment(IInstallment referenceInstallment, IEnumerable<IInstallment> installments)
        {
            return installments
                .Where(i => GetInterestPaymentDate(i) <= referenceInstallment.DueDate)
                .Where(i => i.Interest > 0.0)
                .LastOrDefault();
        }

        private IInstallment CreateInstallment(AmortizationInstallment amortizationInstallment, ILoan loan)
        {
            return new Installment
            {
                Type = InstallmentType.Scheduled,
                Status = InstallmentStatus.Scheduled,
                AnniversaryDate = amortizationInstallment.AnniversaryDate,
                DueDate = amortizationInstallment.DueDate,
                OpeningBalance = amortizationInstallment.OpeningBalance,
                PaymentAmount = amortizationInstallment.Amount,
                Interest = amortizationInstallment.Interest,
                Principal = amortizationInstallment.Principal,
                EndingBalance = amortizationInstallment.EndingBalance,
                PaymentMethod = loan.PaymentMethod
            };
        }

        private static DateTimeOffset? GetInterestPaymentDate(IInstallment installment)
        {
            if (installment == null)
                return null;

            return IsEarlyPayment(installment) ? installment.PaymentDate : installment?.AnniversaryDate ?? installment?.DueDate;
        }

        private static bool IsEarlyPayment(IInstallment installment)
        {
            return 
                installment?.Type == InstallmentType.Scheduled &&
                installment?.PaymentDate < installment?.AnniversaryDate;
        }

        private static bool IsLastInstallment(IInstallment installment)
        {
            return installment.EndingBalance <= 0.0;
        }

        private IEnumerable<PrincipalBalanceChange> GetBuckets(IEnumerable<IInstallment> installments)
        {
            var buckets = new List<PrincipalBalanceChange>();

            buckets.AddRange(installments
                .Select(i => new PrincipalBalanceChange
                {
                    Date = GetInterestPaymentDate(i).Value,
                    Principal = i.EndingBalance
                })
            );

            // Regarding early payments, in the loop above their payment
            // dates are added to the bucket list, and in the loop below
            // we add their anniversary dates. This is necessary because
            // interest should accrue up to the next anniversary date.
            // If we don't add the early payment's anniversary date here,
            // interest will accrue up to the early payment's anniversary
            // date, which means we'd miss one month worth of interest
            // which should accrue after the early payment.
            buckets.AddRange(installments
                .Where(IsEarlyPayment)
                .Select(i => new PrincipalBalanceChange
                {
                    Date = i.AnniversaryDate.Value,
                    // The principal should be the same as the previous
                    // bucket's. Think of this bucket as a continuation
                    // of the previous bucket on to the next anniversary
                    // date. The principal will be set during the next loop.
                    Principal = null
                })
            );

            buckets = buckets.OrderBy(b => b.Date).ToList();

            // Null principals should be equal to the previous bucket's principal
            for (int index = 1; index < buckets.Count; index++)
            {
                var bucket = buckets[index];

                if (bucket.Principal == null)
                    bucket.Principal = buckets[index - 1].Principal;
            }

            return buckets;
        }

        private class PrincipalBalanceChange
        {
            public DateTimeOffset Date { get; set; }
            public double? Principal { get; set; }
        }
    }
}
