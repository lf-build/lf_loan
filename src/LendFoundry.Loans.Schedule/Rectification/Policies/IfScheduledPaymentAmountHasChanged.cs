﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Schedule.Rectification.Policies
{
    internal class IfScheduledPaymentAmountHasChanged : IRectificationPolicy
    {
        private IRectificationPolicy TargetPolicy { get; }
        private IInstallment OriginalInstallment { get; }

        public IfScheduledPaymentAmountHasChanged(IRectificationPolicy targetPolicy, IInstallment originalInstallment)
        {
            if (originalInstallment.Type != InstallmentType.Scheduled)
                throw new ArgumentException($"Wrong installment type: {originalInstallment.Type}");

            TargetPolicy = targetPolicy;
            OriginalInstallment = originalInstallment;
        }

        public void BeforeRectification(List<IInstallment> schedule)
        {
            if (HasPaymentAmountChanged(schedule))
                TargetPolicy.BeforeRectification(schedule);
        }

        public void AfterReamortization(List<IInstallment> schedule)
        {
            if (HasPaymentAmountChanged(schedule))
                TargetPolicy.AfterReamortization(schedule);
        }

        private bool HasPaymentAmountChanged(List<IInstallment> schedule)
        {
            var updatedInstallment = schedule
                .Where(i => i.Type == InstallmentType.Scheduled)
                .Where(i => i.DueDate == OriginalInstallment.DueDate)
                .Single();

            return updatedInstallment.PaymentAmount != OriginalInstallment.PaymentAmount;
        }

        internal static IRectificationPolicy Then(IRectificationPolicy targetPolicy, IInstallment originalInstallment)
        {
            return new IfScheduledPaymentAmountHasChanged(targetPolicy, originalInstallment);
        }
    }
}
