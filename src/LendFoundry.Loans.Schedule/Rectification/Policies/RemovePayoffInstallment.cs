﻿using System.Collections.Generic;

namespace LendFoundry.Loans.Schedule.Rectification.Policies
{
    internal class RemovePayoffInstallment : IRectificationPolicy
    {
        public void BeforeRectification(List<IInstallment> schedule)
        {
            RemovePayoff(schedule);
        }

        public void AfterReamortization(List<IInstallment> schedule)
        {
            RemovePayoff(schedule);
        }

        private static void RemovePayoff(List<IInstallment> schedule)
        {
            schedule.RemoveAll(i => i.Type == InstallmentType.Payoff);
        }
    }
}
