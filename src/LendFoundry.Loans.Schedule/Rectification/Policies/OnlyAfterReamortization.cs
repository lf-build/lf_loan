﻿using System.Collections.Generic;

namespace LendFoundry.Loans.Schedule.Rectification.Policies
{
    internal class OnlyAfterReamortization : IRectificationPolicy
    {
        private IRectificationPolicy TargetPolicy { get; }

        public OnlyAfterReamortization(IRectificationPolicy targetPolicy)
        {
            TargetPolicy = targetPolicy;
        }

        public void BeforeRectification(List<IInstallment> schedule)
        {
        }

        public void AfterReamortization(List<IInstallment> schedule)
        {
            TargetPolicy.AfterReamortization(schedule);
        }

        internal static IRectificationPolicy Do(IRectificationPolicy targetPolicy)
        {
            return new OnlyAfterReamortization(targetPolicy);
        }
    }
}
