﻿using System.Collections.Generic;

namespace LendFoundry.Loans.Schedule.Rectification.Policies
{
    internal class NoRectificationPolicy : IRectificationPolicy
    {
        public static readonly IRectificationPolicy Instance = new NoRectificationPolicy();

        public void AfterReamortization(List<IInstallment> installments)
        {
        }

        public void BeforeRectification(List<IInstallment> schedule)
        {
        }
    }
}
