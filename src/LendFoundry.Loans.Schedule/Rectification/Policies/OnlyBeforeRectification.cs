﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Schedule.Rectification.Policies
{
    internal class OnlyBeforeRectification : IRectificationPolicy
    {
        private IRectificationPolicy TargetPolicy { get; }

        public OnlyBeforeRectification(IRectificationPolicy targetPolicy)
        {
            TargetPolicy = targetPolicy;
        }

        public void AfterReamortization(List<IInstallment> schedule)
        {
        }

        public void BeforeRectification(List<IInstallment> schedule)
        {
            TargetPolicy.BeforeRectification(schedule);
        }

        internal static IRectificationPolicy Do(IRectificationPolicy targetPolicy)
        {
            return new OnlyBeforeRectification(targetPolicy);
        }
    }
}
