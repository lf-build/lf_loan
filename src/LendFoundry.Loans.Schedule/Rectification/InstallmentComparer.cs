﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Schedule.Rectification
{
    internal class InstallmentComparer : IEqualityComparer<IInstallment>, IComparer<IInstallment>
    {
        public static readonly InstallmentComparer Instance = new InstallmentComparer();

        public int Compare(IInstallment i1, IInstallment i2)
        {
            var result = DateOf(i1).CompareTo(DateOf(i2));

            if (result == 0)
                result = i1.Type == InstallmentType.Scheduled ? -1 : 1;

            return result;
        }

        public bool Equals(IInstallment i1, IInstallment i2)
        {
            return DateOf(i1) == DateOf(i2);
        }

        public int GetHashCode(IInstallment installment)
        {
            return DateOf(installment).GetHashCode();
        }

        private static DateTimeOffset DateOf(IInstallment installment)
        {
            return (installment.PaymentDate ?? installment.DueDate).Value;
        }
    }
}
