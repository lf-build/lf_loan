﻿namespace LendFoundry.Loans.Schedule.Rectification
{
    internal interface IInstallmentRectifier
    {
        bool CanRectify(IInstallment installment, ScheduleRectificationContext context);
        void Rectify(IInstallment installment, ScheduleRectificationContext context);
    }
}
