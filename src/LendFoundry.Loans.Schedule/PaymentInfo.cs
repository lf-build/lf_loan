﻿using System;

namespace LendFoundry.Loans.Schedule
{
    public class PaymentInfo
    {
        public string LoanReferenceNumber { get; set; }
        public DateTimeOffset DueDate { get; set; }
        public double Amount { get; set; }
        public string PaymentId { get; set; }
        public InstallmentType Type { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
    }
}