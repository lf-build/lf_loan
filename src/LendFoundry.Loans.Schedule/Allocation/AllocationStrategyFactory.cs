﻿using LendFoundry.Amortization.Client;
using LendFoundry.Loans.Schedule.Rectification;
using LendFoundry.Loans.Schedule.Allocation.Strategies;
using System;
using System.Collections.Generic;
using LendFoundry.Loans.Schedule.Interest;
using LendFoundry.Loans.Schedule.Calculators;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Loans.Schedule.Allocation
{
    internal class AllocationStrategyFactory : IAllocationStrategyFactory
    {
        public AllocationStrategyFactory(
            PaymentScheduleConfiguration configuration,
            IAmortizationService amortizationService,
            IScheduleRectifier scheduleRectifier,
            IGracePeriodCalculator gracePeriodCalculator,
            EarlyPaymentInterestCalculator interestCalculator,
            ITenantTime tenantTime)
        {
            Configuration = configuration;
            AmortizationService = amortizationService;
            ScheduleRectifier = scheduleRectifier;

            Strategies = new List<IAllocationStrategy>
            {
                new EarlyCheckPaymentAllocationStrategy(AmortizationService, ScheduleRectifier, interestCalculator, Configuration),
                new ScheduledPaymentAllocationStrategy(AmortizationService, ScheduleRectifier, gracePeriodCalculator, Configuration),
                new ExtraPaymentAllocationStrategy(AmortizationService, ScheduleRectifier),
                new PayoffAllocationStrategy(ScheduleRectifier, tenantTime)
            };
        }

        private IEnumerable<IAllocationStrategy> Strategies { get; }
        private PaymentScheduleConfiguration Configuration { get; }
        private IAmortizationService AmortizationService { get; }
        private IScheduleRectifier ScheduleRectifier { get; }

        public IAllocationStrategy Create(PaymentInfo payment, IPaymentSchedule schedule, ILoan loan)
        {
            foreach (var strategy in Strategies)
                if (strategy.CanAllocate(payment, schedule, loan))
                    return strategy;

            throw new ArgumentException($"No allocation strategy found for installment type '{payment.Type}' and payment method '{payment.PaymentMethod}'");
        }
    }
}
