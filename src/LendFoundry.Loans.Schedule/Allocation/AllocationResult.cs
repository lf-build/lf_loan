﻿using LendFoundry.Loans.Schedule.Rectification;
using LendFoundry.Loans.Schedule.Rectification.Policies;

namespace LendFoundry.Loans.Schedule.Allocation
{
    internal class AllocationResult
    {
        public IInstallment Installment { get; set; }
        public IRectificationPolicy RectificationPolicy { get; set; } = NoRectificationPolicy.Instance;
    }
}
