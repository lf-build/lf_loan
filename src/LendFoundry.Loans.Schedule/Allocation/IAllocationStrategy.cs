﻿using System.Collections.Generic;

namespace LendFoundry.Loans.Schedule.Allocation
{
    internal interface IAllocationStrategy
    {
        bool CanAllocate(PaymentInfo payment, IPaymentSchedule schedule, ILoan loan);

        /// <summary>
        /// Adds a payment
        /// </summary>
        /// <returns>The complete, updated schedule</returns>
        IEnumerable<IInstallment> AddPayment(PaymentInfo payment, IPaymentSchedule schedule, ILoan loan);

        /// <summary>
        /// Cancels a payment
        /// </summary>
        /// <returns>The complete, updated schedule</returns>
        IEnumerable<IInstallment> CancelPayment(PaymentInfo payment, IPaymentSchedule schedule, ILoan loan);
    }
}
