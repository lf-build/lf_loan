﻿namespace LendFoundry.Loans.Schedule.Allocation
{
    internal interface IAllocationStrategyFactory
    {
        IAllocationStrategy Create(PaymentInfo payment, IPaymentSchedule schedule, ILoan loan);
    }
}
