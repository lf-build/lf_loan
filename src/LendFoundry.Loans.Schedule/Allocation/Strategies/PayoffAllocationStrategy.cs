﻿using LendFoundry.Foundation.Date;
using LendFoundry.Loans.Schedule.Rectification;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Schedule.Allocation.Strategies
{
    internal class PayoffAllocationStrategy : IAllocationStrategy
    {
        public PayoffAllocationStrategy(IScheduleRectifier scheduleRectifier, ITenantTime tenantTime)
        {
            ScheduleRectifier = scheduleRectifier;
            TenantTime = tenantTime;
        }

        private IScheduleRectifier ScheduleRectifier { get; }
        private ITenantTime TenantTime { get; }

        public bool CanAllocate(PaymentInfo payment, IPaymentSchedule schedule, ILoan loan)
        {
            return payment.Type == InstallmentType.Payoff;
        }

        public IEnumerable<IInstallment> AddPayment(PaymentInfo payment, IPaymentSchedule schedule, ILoan loan)
        {
            //TODO: check payoff amount

            AssertPaymentCanBeScheduled(payment, schedule, loan);

            var installments = schedule.Installments.ToList();

            RemoveInstallmentsAfter(payment, installments);

            MarkLastPastDueInstallmentsAsUnpaid(installments);

            installments.Add(CreatePayoffInstallment(payment, loan, installments));

            return installments;
        }

        private void MarkLastPastDueInstallmentsAsUnpaid(List<IInstallment> installments)
        {
            var last = installments.LastOrDefault();

            if (IsPastDue(last))
            {
                last.PaymentAmount = 0.0;
                last.Interest = 0.0;
                last.Principal = 0.0;
                last.EndingBalance = last.OpeningBalance;
                last.Status = InstallmentStatus.Completed;
            }
        }

        private bool IsPastDue(IInstallment last)
        {
            return
                last != null &&
                last.Type == InstallmentType.Scheduled &&
                last.Status == InstallmentStatus.Scheduled &&
                last.DueDate < TenantTime.Today;
        }

        private static void RemoveInstallmentsAfter(PaymentInfo payment, List<IInstallment> installments)
        {
            installments.RemoveAll(i =>
                i.DueDate >= payment.DueDate &&
                i.Status == InstallmentStatus.Scheduled &&
                i.PaymentId != payment.PaymentId
            );
        }

        private static IInstallment CreatePayoffInstallment(PaymentInfo payment, ILoan loan, List<IInstallment> installments)
        {
            var previousInstallment = installments.DueBefore(payment.DueDate).LastOrDefault();

            var installment = new Installment
            {
                Type = InstallmentType.Payoff,
                Status = InstallmentStatus.Scheduled,
                AnniversaryDate = null,
                DueDate = payment.DueDate,
                PaymentDate = payment.DueDate,
                PaymentAmount = payment.Amount,
                PaymentId = payment.PaymentId,
                PaymentMethod = payment.PaymentMethod
            };

            installment.OpeningBalance = previousInstallment?.EndingBalance ?? loan.Terms.LoanAmount;
            installment.Interest = Math.Round(payment.Amount - installment.OpeningBalance, 2, MidpointRounding.AwayFromZero);
            installment.Principal = Math.Round(payment.Amount - installment.Interest, 2, MidpointRounding.AwayFromZero);
            installment.EndingBalance = Math.Round(installment.OpeningBalance - installment.Principal, 2, MidpointRounding.AwayFromZero);
            return installment;
        }

        public IEnumerable<IInstallment> CancelPayment(PaymentInfo payment, IPaymentSchedule schedule, ILoan loan)
        {
            AssertInstallmentHasPaymentId(payment);

            var installments = schedule.Installments.ToList();
            var payoff = installments.Single(i => i.PaymentId == payment.PaymentId);

            AssertInstallmentTypeIsPayoff(payoff);

            installments.Remove(payoff);

            var previousInstallment = installments.LastOrDefault(i => i.DueDate < payoff.DueDate);

            ScheduleRectifier.RectifySchedule(installments, previousInstallment, loan);

            return installments;
        }

        private void AssertNotBeforeOriginationDate(PaymentInfo paymentInfo, ILoan loan)
        {
            if (paymentInfo.DueDate <= loan.Terms.OriginationDate)
                throw new ArgumentException("Payment cannot be scheduled before or on origination date");
        }

        private static void AssertInstallmentTypeIsPayoff(IInstallment payoff)
        {
            if (payoff.Type != InstallmentType.Payoff)
                throw new ArgumentException($"Wrong installment type: {payoff.Type}");
        }

        private static void AssertInstallmentHasPaymentId(PaymentInfo payment)
        {
            if (string.IsNullOrWhiteSpace(payment.PaymentId))
                throw new ArgumentException($"Installment has no payment id (DueDate = {payment.DueDate.ToString("yyy-MM-dd")})");
        }

        private void AssertPaymentCanBeScheduled(PaymentInfo payment, IPaymentSchedule schedule, ILoan loan)
        {
            AssertInstallmentTypeIsPayoff(payment);
            AssertNoPayoffIsAlreadyScheduled(schedule);
            AssertNotBeforeOriginationDate(payment, loan);
            AssertNotBeforeAnyCompletedInstallment(payment, schedule.Installments);
            AssertNotAfterLastInstallment(payment, schedule.Installments);
        }

        private static void AssertNoPayoffIsAlreadyScheduled(IPaymentSchedule schedule)
        {
            if (schedule.Installments.IsPayoffScheduled())
                throw new ArgumentException("Payoff cannot be scheduled because a payoff is already scheduled");
        }

        private static void AssertInstallmentTypeIsPayoff(PaymentInfo payment)
        {
            if (payment.Type != InstallmentType.Payoff)
                throw new ArgumentException($"Wrong payment type: {payment.Type}");
        }

        private static void AssertNotBeforeAnyCompletedInstallment(PaymentInfo payment, IEnumerable<IInstallment> installments)
        {
            if (installments.AnyCompletedStartingOn(payment.DueDate))
                throw new ArgumentException("Payment cannot be scheduled before or on same date as a completed installment");
        }

        private void AssertNotAfterLastInstallment(PaymentInfo paymentInfo, IEnumerable<IInstallment> installments)
        {
            if (paymentInfo.DueDate >= installments.Last().DueDate)
                throw new ArgumentException("Payment cannot be scheduled after or on same date as a the last installment");
        }
    }
}
