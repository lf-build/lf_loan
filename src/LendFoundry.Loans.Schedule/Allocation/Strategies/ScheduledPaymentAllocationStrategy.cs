﻿using LendFoundry.Amortization.Client;
using LendFoundry.Loans.Schedule.Calculators;
using LendFoundry.Loans.Schedule.Rectification;
using LendFoundry.Loans.Schedule.Rectification.Policies;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Schedule.Allocation.Strategies
{
    internal class ScheduledPaymentAllocationStrategy : RectifyingAllocationStrategy
    {
        public ScheduledPaymentAllocationStrategy
        (
            IAmortizationService amortizationService,
            IScheduleRectifier scheduleRectifier,
            IGracePeriodCalculator gracePeriodCalculator,
            PaymentScheduleConfiguration configuration
        )
            : base(amortizationService, scheduleRectifier)
        {
            Configuration = configuration;
            GracePeriodCalculator = gracePeriodCalculator;
        }

        private PaymentScheduleConfiguration Configuration { get; }
        private IGracePeriodCalculator GracePeriodCalculator { get; }

        public override bool CanAllocate(PaymentInfo payment, IPaymentSchedule schedule, ILoan loan)
        {
            return payment.Type == InstallmentType.Scheduled;
        }
            
        protected override AllocationResult AddPayment(PaymentInfo payment, List<IInstallment> installments, ILoan loan)
        {
            if (payment.Type != InstallmentType.Scheduled)
                throw new ArgumentException($"Wrong payment type: {payment.Type}");

            var scheduledInstallment = installments
                .Where(i => i.Type == InstallmentType.Scheduled)
                .Where(i => i.Status == InstallmentStatus.Scheduled)
                .First();

            if (payment.DueDate < scheduledInstallment.AnniversaryDate)
                throw new ArgumentException($"Installment not found due on {payment.DueDate.ToString("yyyy-MM-dd")}");

            if (!GracePeriodCalculator.IsInGracePeriod(payment.DueDate, installments))
                throw new ArgumentException("Cannot add a scheduled payment past grace period");

            var isExpectedPaymentAmount = payment.Amount == scheduledInstallment.PaymentAmount;

            UpdateInstallment(scheduledInstallment, payment);

            return new AllocationResult
            {
                Installment = scheduledInstallment,
                RectificationPolicy = isExpectedPaymentAmount ? NoRectificationPolicy.Instance : OnlyBeforeRectification.Do(new RemovePayoffInstallment())
            };
        }

        protected override AllocationResult CancelPayment(PaymentInfo payment, List<IInstallment> installments)
        {
            if (payment.Type != InstallmentType.Scheduled)
                throw new ArgumentException($"Wrong installment type: {payment.Type}");

            var installment = installments.Single(i => i.PaymentId == payment.PaymentId);

            installment.Status = InstallmentStatus.Scheduled;
            installment.PaymentDate = null;
            installment.PaymentId = null;

            return new AllocationResult
            {
                Installment = installment,
                RectificationPolicy = OnlyAfterReamortization.Do(IfScheduledPaymentAmountHasChanged.Then(new RemovePayoffInstallment(), installment))
            };
        }

        private void UpdateInstallment(IInstallment installment, PaymentInfo payment)
        {
            //TODO: cannot schedule payment with amount greater than principal balance
            installment.PaymentAmount = payment.Amount;
            installment.PaymentId = payment.PaymentId;
            installment.PaymentDate = payment.DueDate;
            installment.Principal = payment.Amount - installment.Interest;
            installment.EndingBalance = installment.OpeningBalance - installment.Principal;
        }
    }
}
