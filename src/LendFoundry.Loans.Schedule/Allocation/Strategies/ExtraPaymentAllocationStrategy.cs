﻿using LendFoundry.Amortization.Client;
using LendFoundry.Loans.Schedule.Rectification;
using LendFoundry.Loans.Schedule.Rectification.Policies;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Schedule.Allocation.Strategies
{
    internal class ExtraPaymentAllocationStrategy : RectifyingAllocationStrategy
    {
        public ExtraPaymentAllocationStrategy(IAmortizationService amortizationService, IScheduleRectifier scheduleRectifier) : base(amortizationService, scheduleRectifier)
        {
        }

        public override bool CanAllocate(PaymentInfo payment, IPaymentSchedule schedule, ILoan loan)
        {
            return payment.Type == InstallmentType.Additional;
        }

        protected override AllocationResult AddPayment(PaymentInfo paymentInfo, List<IInstallment> installments, ILoan loan)
        {
            //TODO: cannot schedule payment with amount greater than principal balance
            AssertPaymentCanBeScheduled(paymentInfo, installments, loan);

            var extraPayment = new Installment
            {
                /*
                    Interest and opening/ending balances are set during the schedule rectification process,
                    which occurs after allocation, so there's no point in doing that here again.
                */
                Type = InstallmentType.Additional,
                Status = InstallmentStatus.Scheduled,
                AnniversaryDate = null,
                DueDate = paymentInfo.DueDate,
                PaymentDate = paymentInfo.DueDate,
                PaymentAmount = paymentInfo.Amount,
                PaymentId = paymentInfo.PaymentId,
                PaymentMethod = paymentInfo.PaymentMethod
            };

            installments.Add(extraPayment);

            return new AllocationResult
            {
                Installment = extraPayment,
                RectificationPolicy = OnlyBeforeRectification.Do(new RemovePayoffInstallment())
            };
        }

        protected override AllocationResult CancelPayment(PaymentInfo payment, List<IInstallment> installments)
        {
            AssertInstallmentHasPaymentId(payment);

            var extraPayment = installments.Single(i => i.PaymentId == payment.PaymentId);

            AssertInstallmentTypeIsAdditional(extraPayment);

            installments.Remove(extraPayment);

            return new AllocationResult
            {
                Installment = extraPayment,
                RectificationPolicy = OnlyBeforeRectification.Do(new RemovePayoffInstallment())
            };
        }

        private void AssertPaymentCanBeScheduled(PaymentInfo paymentInfo, List<IInstallment> installments, ILoan loan)
        {
            AssertPaymentTypeIsAdditional(paymentInfo);
            AssertNotBeforeOriginationDate(paymentInfo, loan);
            AssertNotAfterLastInstallment(paymentInfo, installments);
        }

        private void AssertNotBeforeOriginationDate(PaymentInfo paymentInfo, ILoan loan)
        {
            if (paymentInfo.DueDate <= loan.Terms.OriginationDate)
                throw new ArgumentException("Payment cannot be scheduled before or on origination date");
        }

        private static void AssertPaymentTypeIsAdditional(PaymentInfo paymentInfo)
        {
            if (paymentInfo.Type != InstallmentType.Additional)
                throw new ArgumentException($"Wrong payment type: {paymentInfo.Type}");
        }

        private void AssertNotAfterLastInstallment(PaymentInfo paymentInfo, List<IInstallment> installments)
        {
            if (paymentInfo.DueDate >= installments.Last().DueDate)
                throw new ArgumentException("Payment cannot be scheduled after or on same date as a the last installment");
        }

        private static void AssertInstallmentTypeIsAdditional(IInstallment extraPayment)
        {
            if (extraPayment.Type != InstallmentType.Additional)
                throw new ArgumentException($"Wrong installment type: {extraPayment.Type}");
        }

        private static void AssertInstallmentHasPaymentId(PaymentInfo payment)
        {
            if (string.IsNullOrWhiteSpace(payment.PaymentId))
                throw new ArgumentException($"Installment has no payment id (DueDate = {payment.DueDate.ToString("yyy-MM-dd")})");
        }
    }
}
