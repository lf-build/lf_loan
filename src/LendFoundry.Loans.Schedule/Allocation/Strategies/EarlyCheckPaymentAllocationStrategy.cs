﻿using LendFoundry.Amortization.Client;
using LendFoundry.Loans.Schedule.Interest;
using LendFoundry.Loans.Schedule.Rectification;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Schedule.Allocation.Strategies
{
    internal class EarlyCheckPaymentAllocationStrategy : RectifyingAllocationStrategy
    {
        public EarlyCheckPaymentAllocationStrategy(
            IAmortizationService amortizationService,
            IScheduleRectifier scheduleRectifier,
            EarlyPaymentInterestCalculator interestCalculator,
            PaymentScheduleConfiguration configuration)
            : base(amortizationService, scheduleRectifier)
        {
            InterestCalculator = interestCalculator;
            Configuration = configuration;
        }

        private EarlyPaymentInterestCalculator InterestCalculator { get; }
        private PaymentScheduleConfiguration Configuration { get; }

        public override bool CanAllocate(PaymentInfo payment, IPaymentSchedule schedule, ILoan loan)
        {
            return payment.PaymentMethod == PaymentMethod.Check &&
                   payment.Type == InstallmentType.Scheduled &&
                   !HasPaymentBeenAdded(payment, schedule.Installments) && // This strategy doesn't cancel payments, that should be handled by another strategy
                   IsEarlyPayment(payment, schedule.Installments.ToList(), loan);
        }

        private bool HasPaymentBeenAdded(PaymentInfo payment, IEnumerable<IInstallment> installments)
        {
            return installments.Any(i => i.PaymentId == payment.PaymentId);
        }

        protected override AllocationResult AddPayment(PaymentInfo payment, List<IInstallment> installments, ILoan loan)
        {
            Validate(payment, installments);

            var installment = GetInstallment(installments);
            UpdateInstallment(installment, payment, loan, installments);

            return new AllocationResult
            {
                Installment = installment
                //TODO: RectificationPolicy = OnlyBeforeRectification.Do(new RemovePayoffInstallment())
            };
        }

        protected override AllocationResult CancelPayment(PaymentInfo payment, List<IInstallment> installments)
        {
            throw new NotImplementedException();
        }

        private static IInstallment GetInstallment(List<IInstallment> installments)
        {
            var installment = installments
                .Where(i => i.Type == InstallmentType.Scheduled)
                .Where(i => i.Status == InstallmentStatus.Scheduled)
                .FirstOrDefault();

            if (installment == null)
                throw new ArgumentException("No scheduled installment found");

            return installment;
        }

        private bool IsEarlyPayment(PaymentInfo payment, List<IInstallment> installments, ILoan loan)
        {
            var installment = GetInstallment(installments);
            return payment.DueDate < installment.AnniversaryDate && HasScheduledPaymentAmount(payment, installments, loan);
        }

        private bool HasScheduledPaymentAmount(PaymentInfo payment, List<IInstallment> installments, ILoan loan)
        {
            return payment.Amount == GetExpectedPaymentAmount(payment.DueDate, installments, loan);
        }

        private double GetExpectedPaymentAmount(DateTimeOffset paymentDate, List<IInstallment> installments, ILoan loan)
        {
            var installment = GetInstallment(installments);
            var isLastInstallment = installment.EndingBalance == 0.0;

            if (isLastInstallment)
            {
                /*
                    The payment amount for the last installment will always be less than the
                    scheduled amount because we're accruing less interest. To find out the
                    expected payment amount we calculate the interest for the early payment
                    then add it to the principal balance outstanding.
                */
                var interest = InterestCalculator.GetInterest(installment, paymentDate, installment.PaymentAmount, loan, installments);
                return Math.Round(installment.OpeningBalance + interest, 2, MidpointRounding.AwayFromZero);
            }

            return installment.PaymentAmount;
        }

        private void UpdateInstallment(IInstallment installment, PaymentInfo payment, ILoan loan, List<IInstallment> installments)
        {
            installment.PaymentMethod = payment.PaymentMethod;
            installment.PaymentAmount = payment.Amount;
            installment.PaymentId = payment.PaymentId;
            installment.PaymentDate = payment.DueDate;

        }

        private static void Validate(PaymentInfo payment, List<IInstallment> installments)
        {
            if (payment.PaymentMethod != PaymentMethod.Check)
                throw new ArgumentException($"Wrong payment method: {payment.PaymentMethod}");

            if (payment.Type != InstallmentType.Scheduled)
                throw new ArgumentException($"Wrong payment type: {payment.Type}");
        }
    }
}
