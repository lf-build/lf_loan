﻿using LendFoundry.Amortization.Client;
using LendFoundry.Loans.Schedule.Rectification;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Schedule.Allocation.Strategies
{
    internal abstract class RectifyingAllocationStrategy : IAllocationStrategy
    {
        public RectifyingAllocationStrategy(IAmortizationService amortizationService, IScheduleRectifier scheduleRectifier)
        {
            AmortizationService = amortizationService;
            ScheduleRectifier = scheduleRectifier;
        }

        protected IAmortizationService AmortizationService { get; }
        private IScheduleRectifier ScheduleRectifier { get; }

        protected abstract AllocationResult AddPayment(PaymentInfo payment, List<IInstallment> installments, ILoan loan);
        protected abstract AllocationResult CancelPayment(PaymentInfo payment, List<IInstallment> installments);

        public abstract bool CanAllocate(PaymentInfo payment, IPaymentSchedule schedule, ILoan loan);

        public IEnumerable<IInstallment> AddPayment(PaymentInfo payment, IPaymentSchedule schedule, ILoan loan)
        {
            var installments = schedule.Installments.ToList();
            var result = AddPayment(payment, installments, loan);

            ScheduleRectifier.RectifySchedule(installments, result.Installment, loan, result.RectificationPolicy);

            return installments;
        }

        public IEnumerable<IInstallment> CancelPayment(PaymentInfo payment, IPaymentSchedule schedule, ILoan loan)
        {
            var installments = schedule.Installments.ToList();
            var previousInstallment = installments.LastOrDefault(i => i.DueDate < payment.DueDate);

            var result = CancelPayment(payment, installments);

            ScheduleRectifier.RectifySchedule(installments, previousInstallment, loan, result.RectificationPolicy);

            return installments;
        }
    }
}
