﻿using System;

namespace LendFoundry.Loans.Schedule
{
    public interface IGracePeriod
    {
        int Duration { get; }
        DateTimeOffset GetStartDate(IInstallment installment);
        DateTimeOffset GetEndDateReference(IInstallment installment);
    }
}
