﻿using System;

namespace LendFoundry.Loans.Schedule
{
    public class GracePeriod : IGracePeriod
    {
        public GracePeriod(PaymentScheduleConfiguration configuration)
        {
            Configuration = configuration;
        }

        private PaymentScheduleConfiguration Configuration { get; }

        public int Duration => Configuration.GracePeriod;
        public DateTimeOffset GetStartDate(IInstallment installment) => installment.AnniversaryDate.Value;
        public DateTimeOffset GetEndDateReference(IInstallment installment) => installment.DueDate.Value;
    }
}
