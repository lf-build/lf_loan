﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Schedule
{
    public interface IPaymentScheduleServiceFactory
    {
        IPaymentScheduleService Create(ITokenReader reader);
    }
}