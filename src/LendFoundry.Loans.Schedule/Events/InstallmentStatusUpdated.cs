﻿using System;

namespace LendFoundry.Loans.Schedule.Events
{
    public class InstallmentStatusUpdated
    {
        public InstallmentStatusUpdated(string loanReferenceNumber, DateTimeOffset dueDate, InstallmentStatus newStatus)
        {
            LoanReferenceNumber = loanReferenceNumber;
            DueDate = dueDate;
            NewStatus = newStatus;
        }

        public DateTimeOffset DueDate { get; }
        public string LoanReferenceNumber { get; }
        public InstallmentStatus NewStatus { get; }
    }
}
