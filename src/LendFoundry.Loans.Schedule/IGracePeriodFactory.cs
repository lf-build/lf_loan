﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Schedule
{
    public interface IGracePeriodFactory
    {
        IGracePeriod Create(ITokenReader reader);
    }
}
