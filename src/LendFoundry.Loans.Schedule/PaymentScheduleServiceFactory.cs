﻿using LendFoundry.Amortization.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Repository;
using LendFoundry.Loans.Schedule.Calculators;
using LendFoundry.Loans.Schedule.Data;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Schedule
{
    public class PaymentScheduleServiceFactory : IPaymentScheduleServiceFactory
    {
        public PaymentScheduleServiceFactory
        (
            IConfigurationServiceFactory configurationFactory,
            IConfigurationServiceFactory<LoanConfiguration> loanConfigurationFactory,
            IAmortizationServiceFactory amortizationServiceFactory,
            IPaymentScheduleRepositoryFactory scheduleRepositoryFactory,
            ILoanRepositoryFactory loanRepositoryFactory,
            IPayoffAmountCalculatorFactory payoffAmountCalculatorFactory,
            IGracePeriodCalculatorFactory gracePeriodCalculatorFactory,
            IDaysPastDueCalculatorFactory daysPastDueCalculatorFactory,
            ICurrentDueCalculatorFactory currentDueCalculatorFactory,
            IEventHubClientFactory eventHubFactory,
            ITenantTimeFactory tenantTimeFactory,
            ILoggerFactory loggerFactory
        )
        {
            ConfigurationFactory = configurationFactory;
            LoanConfigurationFactory = loanConfigurationFactory;
            ScheduleRepositoryFactory = scheduleRepositoryFactory;
            EventHubFactory = eventHubFactory;
            TenantTimeFactory = tenantTimeFactory;
            LoanRepositoryFactory = loanRepositoryFactory;
            AmortizationServiceFactory = amortizationServiceFactory;
            PayoffAmountCalculatorFactory = payoffAmountCalculatorFactory;
            GracePeriodCalculatorFactory = gracePeriodCalculatorFactory;
            DaysPastDueCalculatorFactory = daysPastDueCalculatorFactory;
            CurrentDueCalculatorFactory = currentDueCalculatorFactory;
            LoggerFactory = loggerFactory;
        }

        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private IConfigurationServiceFactory<LoanConfiguration> LoanConfigurationFactory { get; }

        private IPaymentScheduleRepositoryFactory ScheduleRepositoryFactory { get; }

        private IEventHubClientFactory EventHubFactory { get; }

        private ITenantTimeFactory TenantTimeFactory { get; }

        private ILoanRepositoryFactory LoanRepositoryFactory { get; }

        private IAmortizationServiceFactory AmortizationServiceFactory { get; }

        private IPayoffAmountCalculatorFactory PayoffAmountCalculatorFactory { get; }

        private IGracePeriodCalculatorFactory GracePeriodCalculatorFactory { get; }

        private IDaysPastDueCalculatorFactory DaysPastDueCalculatorFactory { get; }

        private ICurrentDueCalculatorFactory CurrentDueCalculatorFactory { get; }

        private ILoggerFactory LoggerFactory { get; }

        public IPaymentScheduleService Create(ITokenReader reader)
        {
            return new PaymentScheduleService
            (
                LoanConfigurationFactory.Create(reader).Get().Schedule,
                AmortizationServiceFactory.Create(reader),
                ScheduleRepositoryFactory.Create(reader),
                LoanRepositoryFactory.Create(reader),
                PayoffAmountCalculatorFactory.Create(reader),
                GracePeriodCalculatorFactory.Create(reader),
                DaysPastDueCalculatorFactory.Create(reader),
                CurrentDueCalculatorFactory.Create(reader),
                EventHubFactory.Create(reader),
                TenantTimeFactory.Create(ConfigurationFactory, reader),
                LoggerFactory.CreateLogger()
            );
        }
    }
}
