using LendFoundry.Amortization;
using LendFoundry.Amortization.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Events;
using LendFoundry.Loans.Repository;
using LendFoundry.Loans.Schedule.Allocation;
using LendFoundry.Loans.Schedule.Calculators;
using LendFoundry.Loans.Schedule.Data;
using LendFoundry.Loans.Schedule.Events;
using LendFoundry.Loans.Schedule.Interest;
using LendFoundry.Loans.Schedule.Rectification;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Schedule
{
    public class PaymentScheduleService : IPaymentScheduleService
    {
        public PaymentScheduleService(
            PaymentScheduleConfiguration config,
            IAmortizationService amortizationService,
            IPaymentScheduleRepository scheduleRepository,
            ILoanRepository loanRepository,
            IPayoffAmountCalculator payoffAmountCalculator,
            IGracePeriodCalculator gracePeriodCalculator,
            IDaysPastDueCalculator daysPastDueCalculator,
            ICurrentDueCalculator currentDueCalculator,
            IEventHubClient eventHubClient,
            ITenantTime tenantTime,
            ILogger logger
        )
        {
            if (scheduleRepository == null)
                throw new ArgumentException("Repository cannot be null");

            Config = config;
            ScheduleRepository = scheduleRepository;
            LoanRepository = loanRepository;
            AmortizationService = amortizationService;
            TenantTime = tenantTime;
            EventHubClient = eventHubClient;
            PayoffAmountCalculator = payoffAmountCalculator;
            GracePeriodCalculator = gracePeriodCalculator;
            DaysPastDueCalculator = daysPastDueCalculator;
            CurrentDueCalculator = currentDueCalculator;
            Logger = logger;

            //TODO: inject these dependencies
            var earlyPaymentInterestCalculator = new EarlyPaymentInterestCalculator(amortizationService);
            var installmentRectifierFactory = new InstallmentRectifierFactory(amortizationService, earlyPaymentInterestCalculator);
            var scheduleRectifier = new ScheduleRectifier(installmentRectifierFactory);
            AllocationStrategyFactory = new AllocationStrategyFactory(Config, AmortizationService, scheduleRectifier, gracePeriodCalculator, earlyPaymentInterestCalculator, tenantTime);
        }

        private ILogger Logger { get; }

        private ITenantTime TenantTime { get; }

        private PaymentScheduleConfiguration Config { get; }

        private IPaymentScheduleRepository ScheduleRepository { get; }

        private ILoanRepository LoanRepository { get; }

        private IAmortizationService AmortizationService { get; }

        private IEventHubClient EventHubClient { get; }

        private IAllocationStrategyFactory AllocationStrategyFactory { get; }

        private IPayoffAmountCalculator PayoffAmountCalculator { get; }

        private IGracePeriodCalculator GracePeriodCalculator { get; }

        private IDaysPastDueCalculator DaysPastDueCalculator { get; }

        private ICurrentDueCalculator CurrentDueCalculator { get; }

        public IPayoffAmount GetPayoffAmount(string loanReferenceNumber, DateTimeOffset payoffDate)
        {
            if (string.IsNullOrWhiteSpace(loanReferenceNumber))
                throw new InvalidArgumentException($"Cannot be null or empty: {nameof(loanReferenceNumber)}");

            var loan = LoanRepository.Get(loanReferenceNumber);
            if (loan == null)
                throw new InvalidArgumentException($"Loan not found: {loanReferenceNumber}");

            var schedule = Get(loanReferenceNumber);
            if (schedule == null)
                throw new InvalidArgumentException($"Schedule not found for loan: {loanReferenceNumber}");

            return PayoffAmountCalculator.GetPayoffAmount(payoffDate, loan, schedule);
        }

        public void Create(ILoan loan)
        {
            try
            {
                var installments = AmortizationService.Amortize(new AmortizationRequest
                {
                    LoanTerms = new Amortization.LoanTerms
                    {
                        OriginationDate = loan.Terms.OriginationDate.Date,
                        LoanAmount = loan.Terms.LoanAmount,
                        Term = loan.Terms.Term,
                        Rate = loan.Terms.Rate
                    }
                });

                if (installments == null)
                    Logger.Info("The Amortization Service does not found the installments.");

                ScheduleRepository.Create(new PaymentSchedule
                {
                    LoanReferenceNumber = loan.ReferenceNumber,
                    Installments = installments.Select(payment => (IInstallment)new Installment
                    {
                        DueDate = TenantTime.Create(payment.DueDate.Year, payment.DueDate.Month, payment.DueDate.Day),
                        AnniversaryDate = TenantTime.Create(payment.AnniversaryDate.Year, payment.AnniversaryDate.Month,
                        payment.AnniversaryDate.Day),
                        OpeningBalance = payment.OpeningBalance,
                        PaymentAmount = payment.Amount,
                        Interest = payment.Interest,
                        Principal = payment.Principal,
                        EndingBalance = payment.EndingBalance,
                        Status = InstallmentStatus.Scheduled,
                        Type = InstallmentType.Scheduled,
                        PaymentMethod = loan.PaymentMethod
                    }).ToList()
                });
            }
            catch (Exception ex)
            {
                Logger.Error("Unhandled exception while create the loan: ", ex);
                throw;
            }
        }

        public void Delete(string loanReferenceNumber)
        {
            if (string.IsNullOrEmpty(loanReferenceNumber))
                throw new ArgumentNullException(nameof(loanReferenceNumber));

            ScheduleRepository.Delete(loanReferenceNumber);
        }

        public IPaymentSchedule Get(string loanReferenceNumber)
        {
            if (string.IsNullOrEmpty(loanReferenceNumber))
                throw new ArgumentNullException(nameof(loanReferenceNumber));

            return ScheduleRepository.Get(loanReferenceNumber);
        }

        public void AddPayment(PaymentInfo payment)
        {
            try
            {
                //TODO: handle payment amount smaller than interest (principal paid is negative)
                //TODO: payment service should be updated when scheduling a payoff

                var schedule = ScheduleRepository.Get(payment.LoanReferenceNumber);
                if (schedule == null) throw new ArgumentException($"Payment schedule not found for loan #{payment.LoanReferenceNumber}");

                var loan = LoanRepository.Get(payment.LoanReferenceNumber);
                if (loan == null) throw new LoanNotFoundException(payment.LoanReferenceNumber);

                payment.DueDate = TenantTime.FromDate(payment.DueDate.Date);

                var allocationStrategy = AllocationStrategyFactory.Create(payment, schedule, loan);
                var updatedSchedule = allocationStrategy.AddPayment(payment, schedule, loan);

                ScheduleRepository.ReplaceInstallments(payment.LoanReferenceNumber, updatedSchedule);

                EventHubClient.Publish(nameof(LoanUpdated), new LoanUpdated(payment.LoanReferenceNumber));
                Logger.Info($"The 'AddPayment' method was executed and {nameof(LoanUpdated)} event published");
            }
            catch (Exception ex)
            {
                Logger.Error($"Unhandled exception while add payment for loan {payment.LoanReferenceNumber}. Erro: ", ex);
                throw;
            }
        }

        public void CancelPayment(string loanReferenceNumber, string paymentId)
        {
            var schedule = ScheduleRepository.Get(loanReferenceNumber);
            if (schedule == null) throw new ArgumentException($"Payment schedule not found for loan #{loanReferenceNumber}");

            var installment = schedule.Installments.SingleOrDefault(i => i.PaymentId == paymentId);
            if (installment == null) throw new InstallmentNotFoundException($"Installment not found with payment id {paymentId}");

            var loan = LoanRepository.Get(loanReferenceNumber);
            if (loan == null) throw new LoanNotFoundException(loanReferenceNumber);

            var payment = new PaymentInfo
            {
                LoanReferenceNumber = loanReferenceNumber,
                Amount = installment.PaymentAmount,
                DueDate = installment.PaymentDate ?? installment.DueDate.Value,
                PaymentId = installment.PaymentId,
                PaymentMethod = installment.PaymentMethod,
                Type = installment.Type
            };

            var allocationStrategy = AllocationStrategyFactory.Create(payment, schedule, loan);
            var updatedSchedule = allocationStrategy.CancelPayment(payment, schedule, loan);

            ScheduleRepository.ReplaceInstallments(schedule.LoanReferenceNumber, updatedSchedule);

            EventHubClient.Publish(nameof(LoanUpdated), new LoanUpdated(loanReferenceNumber));
            Logger.Info($"The 'CancelPayment' method was executed and {nameof(LoanUpdated)} event published");
        }

        public void ConfirmPayment(string loanReferenceNumber, string paymentId, DateTimeOffset date)
        {
            ScheduleRepository.MarkInstallmentAsCompleted(loanReferenceNumber, paymentId, TenantTime.FromDate(date.Date));
            EventHubClient.Publish(nameof(LoanUpdated), new LoanUpdated(loanReferenceNumber));
            Logger.Info($"The 'ConfirmPayment' method was executed and {nameof(LoanUpdated)} event published");
        }

        public IPaymentScheduleSummary GetSummary(string loanReferenceNumber)
        {
            var loan = LoanRepository.Get(loanReferenceNumber);
            var schedule = ScheduleRepository.Get(loanReferenceNumber);
            var lastPaidInstallment = schedule.Installments.LastOrDefault(i => i.Status == InstallmentStatus.Completed && i.PaymentAmount > 0.0);
            var firstScheduledInstallment = schedule.FirstScheduledInstallment;
            var nextScheduledInstallment = schedule.Installments.FirstOrDefault(installment => installment.DueDate > TenantTime.Today && installment.Status == InstallmentStatus.Scheduled);
            return new PaymentScheduleSummary
            {
                NextDueDate = firstScheduledInstallment?.DueDate,
                NextInstallmentAmount = nextScheduledInstallment?.PaymentAmount, //TODO: add unit test for this
                LastPaymentDate = lastPaidInstallment?.PaymentDate,
                LastPaymentAmount = lastPaidInstallment?.PaymentAmount,
                DaysPastDue = DaysPastDueCalculator.GetDaysPastDue(TenantTime.Today, schedule, loan.Terms),
                RemainingBalance = GetRemainingBalance(loan, schedule),
                CurrentDue = CurrentDueCalculator.GetCurrentDue(TenantTime.Today, schedule, loan.Terms),
                IsInGracePeriod = GracePeriodCalculator.IsInGracePeriod(TenantTime.Today, schedule)
            };
        }

        public IEnumerable<string> GetLoans(int minDaysDue)
        {
            var maxDueDate = TenantTime.Today.AddDays(-minDaysDue);
            var minDueDate = TenantTime.Now;
            return ScheduleRepository.GetLoans(InstallmentStatus.Scheduled, minDueDate, maxDueDate);
        }

        public IEnumerable<string> GetLoans(int minDaysDue, int maxDaysDue)
        {
            var maxDueDate = TenantTime.Today.AddDays(-minDaysDue);
            var minDueDate = TenantTime.Today.AddDays(-maxDaysDue);
            return ScheduleRepository.GetLoans(InstallmentStatus.Scheduled, minDueDate, maxDueDate);
        }

        public IEnumerable<string> GetLoansDueIn(DateTimeOffset dueDate)
        {
            return ScheduleRepository.GetLoansDueIn(dueDate);
        }

        public IEnumerable<IPaymentSchedule> GetInstallmentsDueIn(DateTimeOffset dueDate, PaymentMethod paymentMethod)
        {
            return ScheduleRepository.GetInstallmentsDueIn(dueDate, paymentMethod);
        }

        public IEnumerable<IPaymentSchedule> GetInstallmentsDueIn(DateTimeOffset date, PaymentMethod paymentMethod, InstallmentType type)
        {
            return ScheduleRepository.GetInstallmentsDueIn(date, paymentMethod, type);
        }

        public IEnumerable<IPaymentSchedule> GetInstallmentsDueInByAnniversaryDate(DateTimeOffset date, PaymentMethod paymentMethod, InstallmentType type)
        {
            return ScheduleRepository.GetInstallmentsDueInByAnniversaryDate(date, paymentMethod, type);
        }

        public void UpdateInstallmentStatus(string loanReferenceNumber, DateTimeOffset dueDate, InstallmentStatus newStatus)
        {
            ScheduleRepository.UpdateInstallmentStatus(loanReferenceNumber, dueDate, newStatus);
            EventHubClient.Publish(nameof(InstallmentStatusUpdated), new InstallmentStatusUpdated(loanReferenceNumber, dueDate, newStatus));
            Logger.Info($"The 'UpdateInstallmentStatus' method was executed and {nameof(InstallmentStatusUpdated)} event published");
        }

        private static double GetRemainingBalance(ILoan loan, IPaymentSchedule schedule)
        {
            var principalPaid = schedule.Installments
                .Where(i => i.Status == InstallmentStatus.Completed)
                .Sum(i => i.Principal);

            var remainingBalance = loan.Terms.LoanAmount - principalPaid;

            return Math.Round(remainingBalance, 2);
        }

        public void UpdatePaymentMethodOfScheduledInstallments(string loanReferenceNumber,
            PaymentMethod newPaymentMethod)
        {
            ScheduleRepository.UpdatePaymentMethodOfScheduledInstallments(loanReferenceNumber, newPaymentMethod);
        }

        public double GetCurrentDue(string loanReferenceNumber, DateTimeOffset date)
        {
            var loan = LoanRepository.Get(loanReferenceNumber);

            if (loan == null)
                throw new LoanNotFoundException(loanReferenceNumber);

            var schedule = ScheduleRepository.Get(loanReferenceNumber);

            if (schedule == null)
                throw new NotFoundException($"Payment schedule not found for loan {loanReferenceNumber}");

            return CurrentDueCalculator.GetCurrentDue(date, schedule, loan.Terms);
        }
    }
}
