﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Schedule
{
    internal static class InstallmentListExtensions
    {
        public static bool IsPayoffScheduled(this IEnumerable<IInstallment> installments)
        {
            return installments.Any(i => i.Type == InstallmentType.Payoff);
        }

        public static IInstallment Previous(this IEnumerable<IInstallment> installments, DateTimeOffset date, InstallmentType type)
        {
            return installments
                .LastOrDefault(i => i.DueDate < date && i.Type == type);
        }

        public static IInstallment PreviousScheduled(this IEnumerable<IInstallment> installments, DateTimeOffset date)
        {
            return Previous(installments, date, InstallmentType.Scheduled);
        }

        public static IInstallment Next(this IEnumerable<IInstallment> installments, DateTimeOffset date, InstallmentType type)
        {
            return installments
                .FirstOrDefault(i => i.DueDate > date && i.Type == type);
        }

        public static IInstallment NextScheduled(this IEnumerable<IInstallment> installments, DateTimeOffset date)
        {
            return Next(installments, date, InstallmentType.Scheduled);
        }

        public static IEnumerable<IInstallment> Extras(this IEnumerable<IInstallment> installments, DateTimeOffset start, DateTimeOffset? end)
        {
            var extras = installments
                .Where(i => i.Type == InstallmentType.Additional)
                .Where(i => i.DueDate > start);

            if (end.HasValue)
                extras = extras.Where(i => i.DueDate < end);

            return extras;
        }

        public static IEnumerable<IInstallment> DueBefore(this IEnumerable<IInstallment> installments, DateTimeOffset date)
        {
            return installments.Where(i => i.DueDate < date);
        }

        public static bool AnyCompletedStartingOn(this IEnumerable<IInstallment> installments, DateTimeOffset date)
        {
            return AnyStartingOn(installments, date, InstallmentStatus.Completed);
        }

        public static bool AnyStartingOn(this IEnumerable<IInstallment> installments, DateTimeOffset date, InstallmentStatus status)
        {
            return installments
                .Any(i => i.PaymentDate >= date && i.Status == status);
        }
    }
}
