using LendFoundry.EventHub.Abstractions;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Events;
using LendFoundry.Loans.Payment.Events;
using LendFoundry.Loans.Payment;
using LendFoundry.Security.Tokens;
using System;
using LendFoundry.Configuration.Client;

namespace LendFoundry.Loans.Schedule
{
    public class EventSubscriptions : 
        IEventSubscription<PaymentAdded>, 
        IEventSubscription<PaymentReceived>, 
        IEventSubscription<PaymentFailed>, 
        IEventSubscription<PaymentCancelled>,
        IEventSubscription<PaymentMethodChanged>
    {
        public EventSubscriptions
            (
                ITokenReaderFactory tokenReaderFactory, 
                IPaymentScheduleServiceFactory scheduleServiceFactory, 
                ILoggerFactory loggerFactory,
                IConfigurationServiceFactory configurationFactory,
                ITenantTimeFactory tenantTimeFactory
            )
        {
            TokenReaderFactory = tokenReaderFactory;
            ScheduleServiceFactory = scheduleServiceFactory;
            LoggerFactory = loggerFactory;
            ConfigurationFactory = configurationFactory;
            TenantTimeFactory = tenantTimeFactory;
        }

        private ITokenReaderFactory TokenReaderFactory { get; }

        private IPaymentScheduleServiceFactory ScheduleServiceFactory { get; }

        private ILoggerFactory LoggerFactory { get; }

        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private ITenantTimeFactory TenantTimeFactory { get; }

        public void Handle(IEventInfo @event, PaymentAdded paymentAdded)
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            try
            {
                if (DoesPaymentAffectTheSchedule(paymentAdded))
                {
                    LogEvent(paymentAdded, logger);
                    Service(@event).AddPayment(new PaymentInfo
                    {
                        LoanReferenceNumber = paymentAdded.LoanReferenceNumber,
                        PaymentId = paymentAdded.PaymentId,
                        Type = ToInstallmentType(paymentAdded.Type),
                        DueDate = paymentAdded.DueDate,
                        Amount = paymentAdded.Amount,
                        PaymentMethod = paymentAdded.Method
                    });
                }
            }
            catch (Exception exception)
            {
                logger.Error("Error attempting to add a payment", exception, paymentAdded);
            }
        }

        public void Handle(IEventInfo @event, PaymentReceived paymentReceived)
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            try
            {
                if (DoesPaymentAffectTheSchedule(paymentReceived))
                {
                    LogEvent(paymentReceived, logger);
                    Service(@event).ConfirmPayment
                    (
                        loanReferenceNumber: paymentReceived.LoanReferenceNumber,
                        paymentId: paymentReceived.PaymentId,
                        date: paymentReceived.DueDate
                    );
                }
            }
            catch (Exception exception)
            {
                logger.Error("Error attempting to confirm a payment", exception, paymentReceived);
            }
        }

        public void Handle(IEventInfo @event, PaymentFailed paymentFailed)
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            try
            {
                if (DoesPaymentAffectTheSchedule(paymentFailed))
                {
                    LogEvent(paymentFailed, logger);
                    Service(@event).CancelPayment
                    (
                        loanReferenceNumber: paymentFailed.LoanReferenceNumber,
                        paymentId: paymentFailed.PaymentId
                    );
                }
            }
            catch (Exception exception)
            {
                logger.Error("Error attempting to cancel a payment", exception, paymentFailed);
            }
        }

        public void Handle(IEventInfo @event, PaymentCancelled data)
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            try
            {
                if (DoesPaymentAffectTheSchedule(data))
                {
                    LogEvent(data, logger);
                    Service(@event).CancelPayment
                    (
                        loanReferenceNumber: data.LoanReferenceNumber,
                        paymentId: data.PaymentId
                    );
                }
            }
            catch (Exception exception)
            {
                logger.Error("Error attempting to cancel a payment", exception, data);
            }
        }
        
        private static void LogEvent(PaymentAdded paymentAdded, ILogger logger)
        {
            logger.Info("Handling event " + paymentAdded.GetType().Name + ": Id={PaymentId} LoanReferenceNumber={LoanReferenceNumber} DueDate={DueDate} Type={Type} Amount={Amount} Method={Method}", new
            {
                paymentAdded.PaymentId,
                paymentAdded.LoanReferenceNumber,
                paymentAdded.Type,
                paymentAdded.Method,
                DueDate = paymentAdded.DueDate.ToString("yyyy-MM-dd"),
                paymentAdded.Amount
            });
        }

        private static bool DoesPaymentAffectTheSchedule(PaymentAdded paymentAdded)
        {
            return paymentAdded.Type == PaymentType.Extra ||
                   paymentAdded.Type == PaymentType.Scheduled ||
                   paymentAdded.Type == PaymentType.Payoff;
        }

        private IPaymentScheduleService Service(IEventInfo @event)
        {
            var reader = TokenReaderFactory.Create(@event.TenantId, "schedule");
            return ScheduleServiceFactory.Create(reader);
        }

        public void Handle(IEventInfo @event, PaymentMethodChanged data)
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            try
            {
                Service(@event)
                    .UpdatePaymentMethodOfScheduledInstallments(data.ReferenceNumber, data.NewPaymentMethod);

                logger.Info($"New payment method changed from {data.OldPaymentMethod} to {data.NewPaymentMethod} for loan {data.ReferenceNumber}");
            }
            catch (Exception ex)
            {
                logger.Error("Error attempting to update installment payment method", ex, data);
            }
        }

        private static InstallmentType ToInstallmentType(PaymentType paymentType)
        {
            switch (paymentType)
            {
                case PaymentType.Scheduled: return InstallmentType.Scheduled;
                case PaymentType.Extra: return InstallmentType.Additional;
                case PaymentType.Payoff: return InstallmentType.Payoff;
                default:
                    throw new ArgumentException($"Unhandled payment type: {paymentType}");
            }
        }
    }
}