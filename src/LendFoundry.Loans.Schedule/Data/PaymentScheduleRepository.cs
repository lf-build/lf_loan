﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Loans.Schedule.Data.Core;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Loans.Schedule.Data
{
    public class PaymentScheduleRepository : MongoRepository<IPaymentSchedule, PaymentSchedule>, IPaymentScheduleRepository
    {
        static PaymentScheduleRepository()
        {
            BsonClassMap.RegisterClassMap<PaymentSchedule>(map =>
            {
                map.AutoMap();

                var type = typeof(PaymentSchedule);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IInstallment, Installment>());

            BsonClassMap.RegisterClassMap<Installment>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.DueDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(m => m.PaymentDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(m => m.AnniversaryDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));

                var type = typeof(Installment);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        public PaymentScheduleRepository(IMongoConfiguration configuration, ITenantService tenantService) :
                base(tenantService, configuration, "schedule")
        {
            CreateIndexIfNotExists
            (
               indexName: "loan-reference-number",
               index: Builders<IPaymentSchedule>.IndexKeys.Ascending(i => i.LoanReferenceNumber)
            );
        }

        public void Create(IPaymentSchedule paymentSchedule)
        {
            if (paymentSchedule == null)
                throw new ArgumentException(LocalResources.MsgInvalidPaymentSchedule);

            if (string.IsNullOrEmpty(paymentSchedule.LoanReferenceNumber))
                throw new ArgumentException(LocalResources.MsgInvalidLoan);

            if (paymentSchedule.Installments.IsNullOrEmpty())
                throw new ArgumentException(LocalResources.MsgInvalidInstallment);

            paymentSchedule.Id = ObjectId.GenerateNewId().ToString();
            paymentSchedule.TenantId = TenantService.Current.Id;
            Collection.InsertOneAsync(paymentSchedule);
        }

        public Task Delete(string loanReferenceNumber)
        {
            if (string.IsNullOrEmpty(loanReferenceNumber))
                throw new ArgumentNullException(nameof(loanReferenceNumber));

            return Collection.FindOneAndDeleteAsync(d =>
                    d.LoanReferenceNumber == loanReferenceNumber &&
                    d.TenantId == TenantService.Current.Id);
        }

        public IPaymentSchedule Get(string loanReferenceNumber)
        {
            var schedule = GetInstance(loanReferenceNumber);

            if (schedule == null)
                return null;

            return new PaymentSchedule
            {
                LoanReferenceNumber = schedule.LoanReferenceNumber,
                Installments = schedule.Installments
            };
        }

        private IPaymentSchedule GetInstance(string loanReferenceNumber)
        {
            if (string.IsNullOrEmpty(loanReferenceNumber))
                throw new ArgumentNullException(nameof(loanReferenceNumber));

            return Query.Where(i => i.TenantId == TenantService.Current.Id)
                .FirstOrDefault(q => q.LoanReferenceNumber == loanReferenceNumber);
        }

        public IEnumerable<string> GetLoans(InstallmentStatus installmentStatus, DateTimeOffset minDueDate, DateTimeOffset maxDueDate)
        {
            if (minDueDate > maxDueDate)
                throw new ArgumentException($"{nameof(minDueDate)} cannot be greater than {nameof(maxDueDate)}", nameof(minDueDate));

            return Query.Where(schedule => schedule.Installments.Any(installment =>
                            installment.DueDate >= minDueDate &&
                            installment.DueDate <= maxDueDate &&
                            installment.Status == installmentStatus
                        )).Select(schedule => schedule.LoanReferenceNumber);
        }

        public IEnumerable<string> GetLoans(InstallmentStatus installmentStatus, DateTimeOffset minDueDate, DateTimeOffset maxDueDate, PaymentMethod paymentMethod)
        {
            if (minDueDate > maxDueDate)
                throw new ArgumentException($"{nameof(minDueDate)} cannot be greater than {nameof(maxDueDate)}", nameof(minDueDate));

            return Query.Where(schedule => schedule.Installments.Any(installment =>
                    installment.DueDate >= minDueDate &&
                    installment.DueDate <= maxDueDate &&
                    installment.Status == installmentStatus &&
                    installment.PaymentMethod == paymentMethod
                )).Select(schedule => schedule.LoanReferenceNumber);


        }

        [Obsolete]
        public void MarkInstallmentAsCompleted(string loanReferenceNumber, DateTimeOffset dueDate, string transactionId, DateTimeOffset transactionDate)
        {
            // It first uses lloanReferenceNumber and dueDate to find the installment,
            // then it updates its status, transactionId and transactionDate properties.

            var payment = Query.FirstOrDefault(p => p.LoanReferenceNumber == loanReferenceNumber);

            if (payment == null)
                throw new Exception("Payment not found");

            if (payment.Installments == null || !payment.Installments.Any())
                throw new Exception("Payment has no installments");

            var installment = payment.Installments.FirstOrDefault(i => i.DueDate == dueDate);
            if (installment == null)
                throw new Exception("Installment not found");

            installment.PaymentId = transactionId;
            installment.PaymentDate = transactionDate;
            installment.Status = InstallmentStatus.Completed;

            var update = Builders<IPaymentSchedule>.Update.Set(s => s.Installments, payment.Installments);
            Collection.FindOneAndUpdateAsync(p =>
                p.LoanReferenceNumber == loanReferenceNumber &&
                p.TenantId == TenantService.Current.Id, update);
        }

        public void ReplaceInstallments(string loanReferenceNumber, IEnumerable<IInstallment> installments)
        {
            var schedule = Query.FirstOrDefault(p => p.LoanReferenceNumber == loanReferenceNumber);
            if (schedule == null) throw new Exception($"Schedule not found for loan {loanReferenceNumber}");

            var update = Builders<IPaymentSchedule>.Update.Set(s => s.Installments, installments);

            Collection.FindOneAndUpdateAsync(p =>
                p.LoanReferenceNumber == loanReferenceNumber &&
                p.TenantId == TenantService.Current.Id, update);
        }

        public IEnumerable<string> GetLoansDueIn(DateTimeOffset dueDate)
        {
            return Query.Where(schedule => schedule.Installments.Any(installment =>
                    installment.Type == InstallmentType.Scheduled &&
                    installment.DueDate == dueDate))
                .Select(schedule => schedule.LoanReferenceNumber);
        }

        public IEnumerable<IPaymentSchedule> GetInstallmentsDueIn(DateTimeOffset dueDate, PaymentMethod paymentMethod)
        {
            return Query.Where(schedule => schedule.Installments.Any(installment =>
                    installment.Status == InstallmentStatus.Scheduled &&
                    installment.PaymentMethod == paymentMethod &&
                    installment.DueDate == dueDate));
        }

        public IEnumerable<IPaymentSchedule> GetInstallmentsDueIn(DateTimeOffset date, PaymentMethod paymentMethod, InstallmentType type)
        {
            return Query.Where(schedule => schedule.Installments.Any(installment =>
                    installment.Type == type &&
                    installment.Status == InstallmentStatus.Scheduled &&
                    installment.PaymentMethod == paymentMethod &&
                    installment.DueDate == date));
        }

        public IEnumerable<IPaymentSchedule> GetInstallmentsDueInByAnniversaryDate(DateTimeOffset date, PaymentMethod paymentMethod, InstallmentType type)
        {
            return Query.Where(schedule => schedule.Installments.Any(installment =>
                    installment.Type == type &&
                    installment.Status == InstallmentStatus.Scheduled &&
                    installment.PaymentMethod == paymentMethod &&
                    installment.AnniversaryDate.Value == date));
        }

        public void UpdateInstallmentStatus(string loanReferenceNumber, DateTimeOffset dueDate, InstallmentStatus newStatus)
        {
            var schedule = GetInstance(loanReferenceNumber);

            if (schedule == null)
                throw new InvalidArgumentException($"Schedule not found for loan with reference number {loanReferenceNumber}");

            var installment = schedule.Installments.FirstOrDefault(i => i.DueDate == dueDate);

            if (installment == null)
                throw new InvalidArgumentException($"Installment not found for the due date {dueDate} and loan {loanReferenceNumber}");

            installment.Status = newStatus;

            var task = Collection.ReplaceOneAsync(p =>
                p.TenantId == TenantService.Current.Id &&
                p.LoanReferenceNumber == loanReferenceNumber,
                schedule);

            task.Wait();
        }

        public void AssignPaymentToInstallment(string loanReferenceNumber, DateTimeOffset dueDate, string paymentId, DateTimeOffset? paymentDate)
        {
            // It first uses lloanReferenceNumber and dueDate to find the installment,
            // then it updates its status, transactionId and transactionDate properties.

            var schedule = Query.FirstOrDefault(p => p.LoanReferenceNumber == loanReferenceNumber);

            if (schedule == null)
                throw new ArgumentException($"Payment schedule not found for loan #{loanReferenceNumber}");

            if (schedule.Installments == null || !schedule.Installments.Any())
                throw new ArgumentException($"Payment schedule has no installments (loan #{loanReferenceNumber})");

            var installment = schedule.Installments.FirstOrDefault(i => i.DueDate == dueDate);
            if (installment == null)
                throw new ArgumentException($"Installment not found with due date = {dueDate} (loan #{loanReferenceNumber})");

            installment.PaymentId = paymentId;

            if (paymentDate.HasValue)
            {
                installment.PaymentDate = paymentDate.Value;
            }

            var update = Builders<IPaymentSchedule>.Update.Set(s => s.Installments, schedule.Installments);
            Collection.FindOneAndUpdateAsync(p =>
                p.LoanReferenceNumber == loanReferenceNumber &&
                p.TenantId == TenantService.Current.Id, update);
        }

        public void MarkInstallmentAsCompleted(string loanReferenceNumber, string paymentId, DateTimeOffset paymentDate)
        {
            var schedule = Query.FirstOrDefault(p => p.LoanReferenceNumber == loanReferenceNumber);

            if (schedule == null)
                throw new Exception($"Payment schedule not found for loan #{loanReferenceNumber}");

            if (schedule.Installments == null || !schedule.Installments.Any())
                throw new Exception($"Payment schedule has no installments (loan #{loanReferenceNumber})");

            var installment = schedule.Installments.FirstOrDefault(i => i.PaymentId == paymentId);
            if (installment == null)
                throw new Exception($"Installment not found with payment id = {paymentId} (loan #{loanReferenceNumber})");

            installment.PaymentDate = paymentDate;
            installment.Status = InstallmentStatus.Completed;

            var update = Builders<IPaymentSchedule>.Update.Set(s => s.Installments, schedule.Installments);
            Collection.FindOneAndUpdateAsync(p =>
                p.LoanReferenceNumber == loanReferenceNumber &&
                p.TenantId == TenantService.Current.Id, update);
        }

        public void UpdatePaymentMethodOfScheduledInstallments(string loanReferenceNumber, PaymentMethod newPaymentMethod)
        {
            var schedule = GetInstance(loanReferenceNumber);

            if (schedule == null)
                throw new InvalidArgumentException($"Schedule not found for loan with reference number {loanReferenceNumber}");

            schedule.Installments = schedule.Installments.Select(i =>
            {
                if (i.Status == InstallmentStatus.Scheduled)
                    i.PaymentMethod = newPaymentMethod;
                return i;
            }).ToList();

            Update(schedule);
        }

        private static class LocalResources
        {
            public static string MsgInvalidPaymentSchedule => "Payment Schedule must be informed";
            public static string MsgInvalidLoan => "Loan Reference must be informed";
            public static string MsgInvalidInstallment => "At least one Installment must be informed";
        }
    }
}
