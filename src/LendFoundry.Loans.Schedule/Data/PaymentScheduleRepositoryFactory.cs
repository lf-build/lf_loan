﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Loans.Schedule.Data.Core;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;

namespace LendFoundry.Loans.Schedule.Data
{
    public class PaymentScheduleRepositoryFactory : IPaymentScheduleRepositoryFactory
    {
        public PaymentScheduleRepositoryFactory(IMongoConfiguration configuration, ITenantServiceFactory tenantServiceFactory)
        {
            Configuration = configuration;
            TenantServiceFactory = tenantServiceFactory;
        }

        private IMongoConfiguration Configuration { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }

        public IPaymentScheduleRepository Create(ITokenReader reader)
        {
            return new PaymentScheduleRepository(Configuration, TenantServiceFactory.Create(reader));
        }
    }
}
