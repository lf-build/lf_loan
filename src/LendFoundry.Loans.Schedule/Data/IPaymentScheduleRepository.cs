﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Loans.Schedule.Data
{
    public interface IPaymentScheduleRepository
    {
        /// <summary>
        /// Inserts a payment schedule into the repository
        /// </summary>
        void Create(IPaymentSchedule paymentSchedule);

        /// <summary>
        /// Deletes the payment schedule of a loan
        /// </summary>
        Task Delete(string loanReferenceNumber);

        /// <summary>
        /// Gets the payment schedule of a loan
        /// </summary>
        IPaymentSchedule Get(string loanReferenceNumber);

        /// <summary>
        /// Finds loans containing installments with the due date in the given date interval (inclusive).
        /// </summary>
        /// <param name="installmentStatus"></param>
        /// <param name="minDueDate">The minimum due date (inclusive)</param>
        /// <param name="maxDueDate">The maximum due date (inclusive)</param>
        /// <returns>The reference numbers of loans with installments that meet the given criteria</returns>
        IEnumerable<string> GetLoans(InstallmentStatus installmentStatus, DateTimeOffset minDueDate, DateTimeOffset maxDueDate);

        /// <summary>
        /// Finds loans containing installments with the given status and payment method,
        /// and with the due date in the given date interval (inclusive).
        /// </summary>
        /// <param name="installmentStatus">The required installment status</param>
        /// <param name="minDueDate">The minimum due date (inclusive)</param>
        /// <param name="maxDueDate">The maximum due date (inclusive)</param>
        /// <param name="paymentMethod">The required payment method</param>
        /// <returns>The reference numbers of loans with installments that meet the given criteria</returns>
        IEnumerable<string> GetLoans(InstallmentStatus installmentStatus, DateTimeOffset minDueDate, DateTimeOffset maxDueDate, PaymentMethod paymentMethod);

        /// <summary>
        /// Changes the status of a installment to Completed.
        /// </summary>
        /// <remarks>
        /// It first uses lloanReferenceNumber and dueDate to find the installment,
        /// then it updates its status, transactionId and transactionDate properties.
        /// </remarks>
        /// <param name="loanReferenceNumber">Used to find the installment</param>
        /// <param name="dueDate">Used to find the installment</param>
        /// <param name="transactionId">The installment's new transactionId</param>
        /// <param name="transactionDate">The installment's new transactionDate</param>
        [Obsolete]
        void MarkInstallmentAsCompleted(string loanReferenceNumber, DateTimeOffset dueDate, string transactionId, DateTimeOffset transactionDate);

        void MarkInstallmentAsCompleted(string loanReferenceNumber, string paymentId, DateTimeOffset paymentDate);

        void ReplaceInstallments(string loanReferenceNumber, IEnumerable<IInstallment> installments);

        IEnumerable<string> GetLoansDueIn(DateTimeOffset dueDate);

        IEnumerable<IPaymentSchedule> GetInstallmentsDueIn(DateTimeOffset dueDate, PaymentMethod paymentMethod);

        IEnumerable<IPaymentSchedule> GetInstallmentsDueIn(DateTimeOffset date, PaymentMethod paymentMethod, InstallmentType type);

        IEnumerable<IPaymentSchedule> GetInstallmentsDueInByAnniversaryDate(DateTimeOffset date, PaymentMethod paymentMethod, InstallmentType type);

        void UpdateInstallmentStatus(string loanReferenceNumber, DateTimeOffset dueDate, InstallmentStatus newStatus);

        void AssignPaymentToInstallment(string loanReferenceNumber, DateTimeOffset dueDate, string paymentId, DateTimeOffset? paymentDate);

        /// <summary>
        /// Updates payment method of all scheduled installment
        /// </summary>
        /// <param name="loanReferenceNumber">Loan reference</param>
        /// <param name="newPaymentMethod">New payment method to be set</param>
        void UpdatePaymentMethodOfScheduledInstallments(string loanReferenceNumber, PaymentMethod newPaymentMethod);
    }
}