﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Schedule.Data
{
    public interface IPaymentScheduleRepositoryFactory
    {
        IPaymentScheduleRepository Create(ITokenReader reader);
    }
}
