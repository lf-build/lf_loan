﻿using System;

namespace LendFoundry.Loans.Schedule.Calculators
{
    public interface IDaysPastDueCalculator
    {
        int GetDaysPastDue(DateTimeOffset date, IPaymentSchedule schedule, ILoanTerms terms);
    }
}
