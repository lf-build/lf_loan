﻿using LendFoundry.Amortization.Client;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Schedule.Calculators
{
    public class CatchUpAmountCalculatorFactory : ICatchUpAmountCalculatorFactory
    {
        public CatchUpAmountCalculatorFactory(IGracePeriodFactory gracePeriodFactory, IAmortizationServiceFactory amortizationServiceFactory)
        {
            GracePeriodFactory = gracePeriodFactory;
            AmortizationServiceFactory = amortizationServiceFactory;
        }

        private IGracePeriodFactory GracePeriodFactory { get; }
        private IAmortizationServiceFactory AmortizationServiceFactory { get; }

        public ICatchUpAmountCalculator Create(ITokenReader reader)
        {
            return new CatchUpAmountCalculator(GracePeriodFactory.Create(reader), AmortizationServiceFactory.Create(reader));
        }
    }
}
