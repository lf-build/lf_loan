﻿using System;
using System.Linq;

namespace LendFoundry.Loans.Schedule.Calculators
{
	public class DaysPastDueCalculator : IDaysPastDueCalculator
	{
		private ICurrentDueCalculator CurrentDueCalculator { get; }

		public DaysPastDueCalculator(ICurrentDueCalculator currentDueCalculator)
		{
			CurrentDueCalculator = currentDueCalculator;
		}

		public int GetDaysPastDue(DateTimeOffset date, IPaymentSchedule schedule, ILoanTerms terms)
		{
			var installment = GetFirstInstallmentWithCurrentDue(date, schedule, terms);

			return installment == null ? 0 : (date - installment.DueDate.GetValueOrDefault()).Days;
		}
		private IInstallment GetFirstInstallmentWithCurrentDue(DateTimeOffset date, IPaymentSchedule schedule, ILoanTerms loanTerms)
		{
			var currentDue = CurrentDueCalculator.GetCurrentDue(date, schedule, loanTerms);

			var payment = new PaymentSchedule { Installments = schedule.Installments };

			return currentDue == 0 ? null : FindInstallment(payment, loanTerms, date);
		}

		private IInstallment FindInstallment(IPaymentSchedule paymentSchedule, ILoanTerms loanTerms, DateTimeOffset date)
		{
			if (paymentSchedule.Installments.Count().Equals(1))
			{
				return paymentSchedule.Installments.First();
			}

			var installments = paymentSchedule.Installments
				.Where(a => a.DueDate <= date && a.Type == InstallmentType.Scheduled)
				.OrderByDescending(a => a.AnniversaryDate);

			foreach (var installment in installments)
			{
				var current = CurrentDueCalculator.GetCurrentDue(installment.DueDate.GetValueOrDefault(), paymentSchedule, loanTerms);

				if (current == 0)
				{
					return installments.ToList().Previous(installment);
				}
			}

			return installments.LastOrDefault();
		}
	}
}
