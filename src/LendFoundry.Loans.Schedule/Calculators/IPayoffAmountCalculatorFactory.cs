﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Schedule.Calculators
{
    public interface IPayoffAmountCalculatorFactory
    {
        IPayoffAmountCalculator Create(ITokenReader reader);
    }
}
