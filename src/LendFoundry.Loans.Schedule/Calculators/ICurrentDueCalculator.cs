﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Schedule.Calculators
{
    public interface ICurrentDueCalculator
    {
        double GetCurrentDue(DateTimeOffset date, IPaymentSchedule schedule, ILoanTerms loanTerms);
    }
}
