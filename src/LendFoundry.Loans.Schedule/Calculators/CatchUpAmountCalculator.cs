﻿using LendFoundry.Amortization;
using LendFoundry.Amortization.Client;
using LendFoundry.Amortization.Interest;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Schedule.Calculators
{
    public class CatchUpAmountCalculator : ICatchUpAmountCalculator
    {
        public CatchUpAmountCalculator(IGracePeriod gracePeriod, IAmortizationService amortizationService)
        {
            GracePeriod = gracePeriod;
            AmortizationService = amortizationService;
        }

        private IGracePeriod GracePeriod { get; }
        private IAmortizationService AmortizationService { get; }

        public double GetCatchUpAmount(DateTimeOffset date, IPaymentSchedule schedule, ILoanTerms terms)
        {
            var amortizationTable = AmortizationService.Amortize(new AmortizationRequest
            {
                LoanTerms = ForAmortization(terms)
            });

            var principalDue = GetPrincipalDue(date, schedule, amortizationTable, terms);
            var interestDue = GetInterestDue(date, schedule, amortizationTable, terms);
            var catchUpAmount = Math.Round(principalDue + interestDue, 2, MidpointRounding.AwayFromZero);
            return Math.Max(0.0, catchUpAmount);
        }

        private double GetPrincipalDue(DateTimeOffset date, IPaymentSchedule schedule, IEnumerable<Amortization.IInstallment> amortizationTable, ILoanTerms terms)
        {
            var expectedBalance = GetExpectedPrincipalBalance(date, amortizationTable, terms);
            var actualBalance = GetActualPrincipalBalance(date, schedule, terms);
            return actualBalance - expectedBalance;
        }

        private static double GetActualPrincipalBalance(DateTimeOffset date, IPaymentSchedule schedule, ILoanTerms terms)
        {
            var installment = schedule.Installments.LastOrDefault(i => InterestPaymentDateOf(i) <= date && i.Status == InstallmentStatus.Completed);
            return installment?.EndingBalance ?? terms.LoanAmount;
        }

        private static double GetExpectedPrincipalBalance(DateTimeOffset date, IEnumerable<Amortization.IInstallment> amortizationTable, ILoanTerms terms)
        {
            var installment = amortizationTable.LastOrDefault(i => i.AnniversaryDate <= date);
            return installment?.EndingBalance ?? terms.LoanAmount;
        }

        private double GetInterestDue(DateTimeOffset date, IPaymentSchedule schedule, IEnumerable<Amortization.IInstallment> amortizationTable, ILoanTerms terms)
        {
            var totalDue = GetTotalInterestDue(date, schedule, amortizationTable, terms);
            var totalPaid = GetTotalInterestPaid(date, schedule);
            return totalDue - totalPaid;
        }

        private double GetTotalInterestDue(DateTimeOffset date, IPaymentSchedule schedule, IEnumerable<Amortization.IInstallment> amortizationTable, ILoanTerms terms)
        {
            var installments = schedule.Installments
                .Where(i => InterestPaymentDateOf(i) <= date);

            var lastScheduledInstallment = installments
                .Where(i => i.Type == InstallmentType.Scheduled)
                .Where(i => i.AnniversaryDate != null)
                .LastOrDefault();

            if (lastScheduledInstallment == null)
                return 0.0;

            installments = installments
                .Where(i => InterestPaymentDateOf(i) <= InterestPaymentDateOf(lastScheduledInstallment));

            var periods = GetInterestAccrualPeriods(installments, terms, date);

            return AmortizationService.GetSimpleInterest(ForAmortization(terms), periods, new InterestAccrualOptions
            {
                EffectiveEndDate = EffectiveEndDate.PreviousAnniversary
            });
        }

        private IEnumerable<InterestAccrualPeriod> GetInterestAccrualPeriods(IEnumerable<IInstallment> installments, ILoanTerms terms, DateTimeOffset date)
        {
            var periods = new List<InterestAccrualPeriod>();

            if (installments.Any())
            {
                var principalBalance = terms.LoanAmount;

                periods.Add(new InterestAccrualPeriod { Principal = principalBalance, Start = terms.OriginationDate });

                foreach (var installment in AllExceptLast(installments))
                {
                    var hasPrincipalBalanceChanged = principalBalance != installment.EndingBalance;

                    if (installment.Status == InstallmentStatus.Completed && hasPrincipalBalanceChanged)
                    {
                        principalBalance = installment.EndingBalance;

                        periods.Add(new InterestAccrualPeriod
                        {
                            Start = InterestPaymentDateOf(installment),
                            Principal = principalBalance
                        });
                    }
                }

                periods.Last().End = date;
            }

            return periods;
        }

        private static IEnumerable<IInstallment> AllExceptLast(IEnumerable<IInstallment> installments)
        {
            return installments.Any() ? installments.Except(new[] { installments.Last() }) : installments;
        }

        private static DateTimeOffset InterestPaymentDateOf(IInstallment i)
        {
            if (IsEarlyPayment(i))
                return i.PaymentDate.Value;

            return (i.AnniversaryDate ?? i.DueDate).Value;
        }

        private static bool IsEarlyPayment(IInstallment i)
        {
            return i.PaymentDate != null && i.AnniversaryDate != null && i.PaymentDate < i.AnniversaryDate;
        }

        private double GetTotalInterestPaid(DateTimeOffset date, IPaymentSchedule schedule)
        {
            var payments = schedule.Installments
                .Where(i => (InterestPaymentDateOf(i)) <= date)
                .Where(i => i.Status == InstallmentStatus.Completed);

            return payments.Sum(i => i.Interest);
        }

        private Amortization.ILoanTerms ForAmortization(ILoanTerms loanTerms)
        {
            return new Amortization.LoanTerms
            {
                OriginationDate = loanTerms.OriginationDate,
                LoanAmount = loanTerms.LoanAmount,
                Term = loanTerms.Term,
                Rate = loanTerms.Rate
            };
        }
    }
}
