﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Schedule.Calculators
{
    public interface ICatchUpAmountCalculatorFactory
    {
        ICatchUpAmountCalculator Create(ITokenReader reader);
    }
}
