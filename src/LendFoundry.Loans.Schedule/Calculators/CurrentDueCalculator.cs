﻿using LendFoundry.Amortization;
using LendFoundry.Amortization.Client;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Schedule.Calculators
{
    public class CurrentDueCalculator : ICurrentDueCalculator
    {
        private IAmortizationService AmortizationService { get; }

        public CurrentDueCalculator(IAmortizationService amortizationService)
        {
            AmortizationService = amortizationService;
        }
		
		public double GetCurrentDue(DateTimeOffset date, IPaymentSchedule schedule, ILoanTerms loanTerms)
        {
            IEnumerable<Amortization.IInstallment> originalSchedule = AmortizationService.Amortize(new AmortizationRequest
            {
                LoanTerms = new Amortization.LoanTerms
                {
                    OriginationDate = loanTerms.OriginationDate,
                    LoanAmount = loanTerms.LoanAmount,
                    Term = loanTerms.Term,
                    Rate = loanTerms.Rate
                }
            });

            var expectedSchedule = GetExpectedSchedule(schedule, originalSchedule);

            return CalculateCurrentDue(date, schedule, expectedSchedule);
        }

        private static IEnumerable<ExpectedPayment> GetExpectedSchedule(IPaymentSchedule schedule, IEnumerable<Amortization.IInstallment> originalSchedule)
        {
            var lastInstallment = schedule.Installments.LastOrDefault(i => i.Type == InstallmentType.Scheduled);

            if (lastInstallment == null)
                return new ExpectedPayment[] { };

            var lastAnniversary = lastInstallment.AnniversaryDate.Value;

            var expectedSchedule = originalSchedule
                .Where(i => i.AnniversaryDate < lastAnniversary)
                .Select(i => new ExpectedPayment
                {
                    Date = i.AnniversaryDate,
                    Amount = i.Amount
                })
                .ToList();

            expectedSchedule.Add(new ExpectedPayment
            {
                Date = lastInstallment.AnniversaryDate.Value,
                Amount = lastInstallment.PaymentAmount
            });

            return expectedSchedule;
        }

        private double CalculateCurrentDue(DateTimeOffset date, IPaymentSchedule schedule, IEnumerable<ExpectedPayment> originalSchedule)
	    {
		    var totalDue = TotalDue(date, schedule, originalSchedule);
		    var totalPaid = TotalPaid(date, schedule);
            var delta = Delta(originalSchedule, schedule, date);
            var currentDue = Math.Round(totalDue - totalPaid + delta, 2, MidpointRounding.AwayFromZero);

		    return Math.Max(0, currentDue);
	    }

	    private double TotalDue(DateTimeOffset date, IPaymentSchedule schedule, IEnumerable<ExpectedPayment> originalSchedule)
        {
            var totalDue = DueInstallments(date, schedule, originalSchedule).Sum(i => i.Amount);
            return Math.Round(totalDue, 2, MidpointRounding.AwayFromZero);
        }

        private static double TotalPaid(DateTimeOffset date, IPaymentSchedule schedule)
        {
            var totalPaid = PaidInstallments(date, schedule).Sum(i => i.PaymentAmount);
            return Math.Round(totalPaid, 2, MidpointRounding.AwayFromZero);
        }

        private IEnumerable<ExpectedPayment> DueInstallments(DateTimeOffset date, IPaymentSchedule schedule, IEnumerable<ExpectedPayment> originalSchedule)
        {
            var dueDate = DateForDueInstallments(date, schedule, originalSchedule);
            return originalSchedule
                .Where(i => i.Date <= dueDate);
        }

        private DateTimeOffset DateForDueInstallments(DateTimeOffset date, IPaymentSchedule schedule, IEnumerable<ExpectedPayment> originalSchedule)
        {
            var lastPayment = PaidInstallments(date, schedule).LastOrDefault();

            if (lastPayment != null && lastPayment.EndingBalance == 0.0 && lastPayment.Status == InstallmentStatus.Completed)
                /*
                 If the loan has been paid off at this point, all installments
                 from the original schedule should be included in the total due calculation
                */
                return originalSchedule.Last().Date;

            return date;
        }

        private static IEnumerable<IInstallment> PaidInstallments(DateTimeOffset date, IPaymentSchedule schedule)
        {
            return schedule.Installments
                .Where(i => i.Type == InstallmentType.Scheduled)
                .Where(i => i.Status == InstallmentStatus.Completed)
                .Where(i => (i.AnniversaryDate ?? i.PaymentDate) <= date);
        }

        /// <summary>
        /// Gets the delta between the total amount originally owed and total amount actually owed
        /// </summary>
        /// <remarks>
        /// The delta allows the current due to be correctly adjusted for the last installment.
        /// When the payment schedule deviates from the original schedule (e.g.: by making early,
        /// late, extra payments, etc), the last installment is adjusted by the reamortization
        /// process to accommodate the changes in principal payment caused by changes in interest
        /// accrual during the life of the loan as to balance it out. That difference must be accounted
        /// for in the current due when the date is past the last installment.
        /// </remarks>
        private double Delta(IEnumerable<ExpectedPayment> originalSchedule, IPaymentSchedule actualSchedule, DateTimeOffset date)
        {
            var lastInstallment = actualSchedule.Installments.Last();
            var lastDate = (lastInstallment.AnniversaryDate ?? lastInstallment.DueDate).Value;

            if (date < lastDate)
                return 0.0;

            var totalOriginallyOwed = originalSchedule.Sum(i => i.Amount);
            var totalActuallyOwed = actualSchedule.Installments.Where(i => i.Type == InstallmentType.Scheduled).Sum(i => i.PaymentAmount);
            var delta = totalActuallyOwed - totalOriginallyOwed;

            return Math.Round(delta, 2, MidpointRounding.AwayFromZero);
        }

        private class ExpectedPayment
        {
            public DateTimeOffset Date { get; set; }
            public double Amount { get; set; }
        }
    }
}
