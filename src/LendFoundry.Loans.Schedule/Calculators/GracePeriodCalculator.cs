﻿using System;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Loans.Schedule.Calculators
{
    public class GracePeriodCalculator : IGracePeriodCalculator
    {
        private IGracePeriod GracePeriod { get; }

        public GracePeriodCalculator(IGracePeriod gracePeriod)
        {
            GracePeriod = gracePeriod;
        }

        public bool IsBeforeGracePeriod(DateTimeOffset date, IPaymentSchedule schedule)
        {
            var gracePeriod = GetGracePeriod(schedule);
            return date < gracePeriod?.Start;
        }

        public bool IsInGracePeriod(DateTimeOffset date, IPaymentSchedule schedule)
        {
            return IsInGracePeriod(date, schedule.Installments);
        }

        public bool IsInGracePeriod(DateTimeOffset date, IEnumerable<IInstallment> installments)
        {
            var gracePeriod = GetGracePeriod(installments);
            return date >= gracePeriod?.Start && date <= gracePeriod?.End;
        }

        public bool IsPastGracePeriod(DateTimeOffset date, IPaymentSchedule schedule)
        {
            var gracePeriod = GetGracePeriod(schedule);
            return date > gracePeriod?.End;
        }

        public Interval GetGracePeriod(IPaymentSchedule schedule)
        {
            return GetGracePeriod(schedule.Installments);
        }

        private Interval GetGracePeriod(IEnumerable<IInstallment> installments)
        {
            var installment = GetInstallment(installments);
            return installment != null ? GetGracePeriod(installment) : null;
        }

        public Interval GetGracePeriod(IInstallment installment)
        {
            if (!installment.AnniversaryDate.HasValue)
                throw new ArgumentException("Installment must have anniversary date");

            var start = GracePeriod.GetStartDate(installment);
            var endReference = GracePeriod.GetEndDateReference(installment);
            var end = endReference.AddDays(GracePeriod.Duration);
            return new Interval(start, end);
        }

        private static IInstallment GetInstallment(IEnumerable<IInstallment> installments)
        {
            return installments
                .Where(i => i.Type == InstallmentType.Scheduled)
                .Where(i => i.Status == InstallmentStatus.Scheduled)
                .FirstOrDefault();
        }

        public Interval GetGracePeriod(DateTimeOffset date)
        {
            return new Interval(date, date.AddDays(GracePeriod.Duration));
        }
    }
}
