﻿using LendFoundry.Amortization.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Schedule.Calculators
{
    public class PayoffAmountCalculatorFactory : IPayoffAmountCalculatorFactory
    {
        public PayoffAmountCalculatorFactory
        (
            IConfigurationServiceFactory configurationServiceFactory,
            IConfigurationServiceFactory<LoanConfiguration> loanConfigurationServiceFactory,
            IAmortizationServiceFactory amortizationServiceFactory,
            ITenantTimeFactory tenantTimeFactory
        )
        {
            LoanConfigurationServiceFactory = loanConfigurationServiceFactory;
            AmortizationServiceFactory = amortizationServiceFactory;
            ConfigurationServiceFactory = configurationServiceFactory;
            TenantTimeFactory = tenantTimeFactory;
        }

        private IConfigurationServiceFactory ConfigurationServiceFactory { get; }
        private IConfigurationServiceFactory<LoanConfiguration> LoanConfigurationServiceFactory { get; }
        private IAmortizationServiceFactory AmortizationServiceFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }

        public IPayoffAmountCalculator Create(ITokenReader reader)
        {
            var configuration = LoanConfigurationServiceFactory.Create(reader).Get();
            return new PayoffAmountCalculator
            (
                configuration.Schedule,
                AmortizationServiceFactory.Create(reader),
                TenantTimeFactory.Create(ConfigurationServiceFactory, reader)
            );
        }
    }
}
