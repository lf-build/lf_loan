﻿using LendFoundry.Amortization.Client;
using LendFoundry.Amortization.Interest;
using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Schedule.Calculators
{
    public class PayoffAmountCalculator : IPayoffAmountCalculator
    {
        public PayoffAmountCalculator(PaymentScheduleConfiguration configuration, IAmortizationService amortizationService, ITenantTime tenantTime)
        {
            Configuration = configuration;
            AmortizationService = amortizationService;
            TenantTime = tenantTime;
        }

        private PaymentScheduleConfiguration Configuration { get; }
        private IAmortizationService AmortizationService { get; }
        private ITenantTime TenantTime { get; }

        public IPayoffAmount GetPayoffAmount(DateTimeOffset payoffDate, ILoan loan, IPaymentSchedule schedule)
        {
            if (loan == null) throw new ArgumentNullException(nameof(loan));
            if (schedule == null) throw new ArgumentNullException(nameof(schedule));

            AssertDateIsPastOriginationDate(payoffDate, loan);
            AssertDateIsPastLastCompletedPayment(payoffDate, schedule);
            AssertDateIsBeforeLastInstallment(payoffDate, schedule);

            var payoffSchedule = CreatePayoffSchedule(schedule, payoffDate);

            var interest = GetInterest(payoffDate, loan, payoffSchedule);
            var precedingInstallment = payoffSchedule.Installments.LastOrDefault();
            var balance = precedingInstallment?.EndingBalance ?? loan.Terms.LoanAmount;

            return new PayoffAmount
            {
                Amount = Math.Round(balance + interest, 2),
                ValidUntil = GetValidUntilDate(payoffDate)
            };
        }

        private IPaymentSchedule CreatePayoffSchedule(IPaymentSchedule schedule, DateTimeOffset payoffDate)
        {
            var installments = schedule.Installments
                .Where(i => (IsEarlyPayment(i) ? i.PaymentDate : i.DueDate) < payoffDate)
                .ToList();

            var last = installments.LastOrDefault();

            if (IsPastDue(last))
                installments.Remove(last);

            return new PaymentSchedule { Installments = installments };
        }

        private bool IsPastDue(IInstallment last)
        {
            return
                last != null &&
                last.Type == InstallmentType.Scheduled &&
                last.Status == InstallmentStatus.Scheduled &&
                last.DueDate < TenantTime.Today;
        }

        private double GetInterest(DateTimeOffset payoffDate, ILoan loan, IPaymentSchedule schedule)
        {
            var lastInterestPayment = schedule.Installments
                .Where(i => i.Interest > 0.0)
                .LastOrDefault();

            var start = lastInterestPayment != null ? InterestPaymentDateOf(lastInterestPayment) : loan.Terms.OriginationDate;

            var installments = schedule.Installments
                .Where(i => InterestPaymentDateOf(i) >= start);

            var periods = new List<InterestAccrualPeriod>();

            if (lastInterestPayment == null)
                periods.Add(new InterestAccrualPeriod { Principal = loan.Terms.LoanAmount, Start = loan.Terms.OriginationDate });

            foreach (var installment in installments)
                periods.Add(new InterestAccrualPeriod { Principal = installment.EndingBalance, Start = PrincipalPaymentDateOf(installment) });

            foreach (var latePayment in installments.Where(IsLatePayment))
                periods.Add(new InterestAccrualPeriod { Principal = latePayment.OpeningBalance, Start = InterestPaymentDateOf(latePayment) });

            periods = periods.OrderBy(p => p.Start).ThenByDescending(p => p.Principal).ToList();

            periods.Last().End = GetValidUntilDate(payoffDate);

            return AmortizationService.GetSimpleInterest(ToAmortizationTerms(loan.Terms), periods);
        }

        private Amortization.ILoanTerms ToAmortizationTerms(ILoanTerms terms)
        {
            return new Amortization.LoanTerms
            {
                LoanAmount = terms.LoanAmount,
                OriginationDate = terms.OriginationDate,
                Term = terms.Term,
                Rate = terms.Rate
            };
        }

        private DateTimeOffset InterestPaymentDateOf(IInstallment installment)
        {
            if (IsEarlyPayment(installment))
                return installment.PaymentDate.Value;

            return (installment.AnniversaryDate ?? installment.DueDate).Value;
        }

        private DateTimeOffset PrincipalPaymentDateOf(IInstallment installment)
        {
            if (IsEarlyPayment(installment) || IsLatePayment(installment))
                return installment.PaymentDate.Value;

            return (installment.AnniversaryDate ?? installment.DueDate).Value;
        }

        private bool IsLatePayment(IInstallment installment)
        {
            return
                installment.AnniversaryDate != null &&
                installment.PaymentDate != null &&
                installment.PaymentDate > installment.DueDate.Value.AddDays(10);
        }

        private static bool IsEarlyPayment(IInstallment installment)
        {
            return
                installment.AnniversaryDate != null &&
                installment.PaymentDate != null &&
                installment.PaymentDate < installment.AnniversaryDate;
        }

        private DateTimeOffset GetValidUntilDate(DateTimeOffset payoffDate)
        {
            return payoffDate.AddDays(Configuration.PayoffPeriod);
        }

        private static void AssertDateIsBeforeLastInstallment(DateTimeOffset payoffDate, IPaymentSchedule schedule)
        {
            var isAfterLastInstallment = schedule.Installments
                .Where(i => i.EndingBalance <= 0)
                .Any(i => i.DueDate <= payoffDate);

            if (isAfterLastInstallment)
                throw new InvalidArgumentException("Payoff must occur before last installment");
        }

        private static void AssertDateIsPastLastCompletedPayment(DateTimeOffset payoffDate, IPaymentSchedule schedule)
        {
            var isPayoffBeforeSomePayment = schedule.Installments
                .Where(i => i.Status == InstallmentStatus.Completed)
                .Any(i => i.PaymentDate >= payoffDate);

            if (isPayoffBeforeSomePayment)
                throw new InvalidArgumentException("Payoff date must be after the last payment date");
        }

        private static void AssertDateIsPastOriginationDate(DateTimeOffset payoffDate, ILoan loan)
        {
            if (payoffDate <= loan.Terms.OriginationDate)
                throw new InvalidArgumentException("Payoff date must be after origination date");
        }
    }
}
