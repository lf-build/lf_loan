﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Schedule.Calculators
{
    public class DaysPastDueCalculatorFactory : IDaysPastDueCalculatorFactory
    {
        public DaysPastDueCalculatorFactory(ICurrentDueCalculator currentDueCalculator)
        {
			CurrentDueCalculator = currentDueCalculator;
        }

        private ICurrentDueCalculator CurrentDueCalculator { get; }

        public IDaysPastDueCalculator Create(ITokenReader reader)
        {
            return new DaysPastDueCalculator(CurrentDueCalculator);
        }
    }
}
