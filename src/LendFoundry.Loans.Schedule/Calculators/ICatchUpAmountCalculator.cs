﻿using System;

namespace LendFoundry.Loans.Schedule.Calculators
{
    public interface ICatchUpAmountCalculator
    {
        double GetCatchUpAmount(DateTimeOffset date, IPaymentSchedule schedule, ILoanTerms loanTerms);
    }
}
