﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Schedule.Calculators
{
    public interface ICurrentDueCalculatorFactory
    {
        ICurrentDueCalculator Create(ITokenReader reader);
    }
}
