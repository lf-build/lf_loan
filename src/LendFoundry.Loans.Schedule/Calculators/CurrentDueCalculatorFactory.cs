﻿using LendFoundry.Amortization.Client;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Schedule.Calculators
{
    public class CurrentDueCalculatorFactory : ICurrentDueCalculatorFactory
    {
        public CurrentDueCalculatorFactory(IAmortizationServiceFactory amortizationServiceFactory)
        {
            AmortizationServiceFactory = amortizationServiceFactory;
        }

        private IAmortizationServiceFactory AmortizationServiceFactory { get; }

        public ICurrentDueCalculator Create(ITokenReader reader)
        {
            return new CurrentDueCalculator(AmortizationServiceFactory.Create(reader));
        }
    }
}
