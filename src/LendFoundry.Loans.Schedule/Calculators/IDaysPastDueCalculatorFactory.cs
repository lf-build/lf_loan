﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Schedule.Calculators
{
    public interface IDaysPastDueCalculatorFactory
    {
        IDaysPastDueCalculator Create(ITokenReader reader);
    }
}
