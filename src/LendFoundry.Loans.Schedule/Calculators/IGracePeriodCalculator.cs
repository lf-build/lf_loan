﻿using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Schedule.Calculators
{
    public interface IGracePeriodCalculator
    {
        bool IsBeforeGracePeriod(DateTimeOffset date, IPaymentSchedule schedule);
        bool IsInGracePeriod(DateTimeOffset date, IPaymentSchedule schedule);
        bool IsInGracePeriod(DateTimeOffset date, IEnumerable<IInstallment> installments);
        bool IsPastGracePeriod(DateTimeOffset date, IPaymentSchedule schedule);
		
        Interval GetGracePeriod(IPaymentSchedule schedule);
        Interval GetGracePeriod(IInstallment installment);
        Interval GetGracePeriod(DateTimeOffset date);
    }
}
