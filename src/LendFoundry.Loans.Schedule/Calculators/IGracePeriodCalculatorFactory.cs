﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Schedule.Calculators
{
    public interface IGracePeriodCalculatorFactory
    {
        IGracePeriodCalculator Create(ITokenReader reader);
    }
}
