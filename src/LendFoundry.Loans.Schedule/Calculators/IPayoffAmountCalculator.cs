﻿using System;

namespace LendFoundry.Loans.Schedule.Calculators
{
    public interface IPayoffAmountCalculator
    {
        IPayoffAmount GetPayoffAmount(DateTimeOffset date, ILoan loan, IPaymentSchedule schedule);
    }
}
