﻿using LendFoundry.Configuration.Client;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Schedule.Calculators
{
    public class GracePeriodCalculatorFactory : IGracePeriodCalculatorFactory
    {
        public GracePeriodCalculatorFactory(IConfigurationServiceFactory<LoanConfiguration> configurationFactory)
        {
            ConfigurationFactory = configurationFactory;
        }

        private IConfigurationServiceFactory<LoanConfiguration> ConfigurationFactory { get; }

        public IGracePeriodCalculator Create(ITokenReader reader)
        {
            var configuration = ConfigurationFactory.Create(reader).Get();
            return new GracePeriodCalculator(new GracePeriod(configuration.Schedule));
        }
    }
}
