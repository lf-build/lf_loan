﻿using System;
using System.Collections.Generic;
using LendFoundry.Loans.Payment;

namespace LendFoundry.Loans.Schedule
{
    public interface IPaymentScheduleService
    {
        /// <summary>
        /// Creates the payment schedule of a loan
        /// </summary>
        void Create(ILoan loan);

        /// <summary>
        /// Deletes the payment schedule of a loan
        /// </summary>
        void Delete(string loanReferenceNumber);

        /// <summary>
        /// Gets the payment schedule of a given loan
        /// </summary>
        IPaymentSchedule Get(string loanReferenceNumber);

        /// <summary>
        /// Gets the summary of a loan's payment schedule
        /// </summary>
        IPaymentScheduleSummary GetSummary(string loanReferenceNumber);

        /// <summary>
        /// Finds loans with a minimal number of due days
        /// </summary>
        /// <returns>A list of loan reference numbers</returns>
        //TODO: add unit tests for GetLoans(int minDaysDue)
        IEnumerable<string> GetLoans(int minDaysDue);

        /// <summary>
        /// Finds loans with due dates within an interval of days relative to the current date
        /// </summary>
        /// <returns>A list of loan reference numbers</returns>
        IEnumerable<string> GetLoans(int minDaysDue, int maxDaysDue);

        IEnumerable<string> GetLoansDueIn(DateTimeOffset dueDate);

        IEnumerable<IPaymentSchedule> GetInstallmentsDueIn(DateTimeOffset dueDate, PaymentMethod paymentMethod);

        IEnumerable<IPaymentSchedule> GetInstallmentsDueIn(DateTimeOffset date, PaymentMethod paymentMethod, InstallmentType type);

        IEnumerable<IPaymentSchedule> GetInstallmentsDueInByAnniversaryDate
            (
                DateTimeOffset date,
                PaymentMethod paymentMethod, 
                InstallmentType type
            );

        void UpdateInstallmentStatus(string loanReferenceNumber, DateTimeOffset dueDate, InstallmentStatus newStatus);

        void AddPayment(PaymentInfo payment);

        void ConfirmPayment(string loanReferenceNumber, string paymentId, DateTimeOffset date);

        void CancelPayment(string loanReferenceNumber, string paymentId);

        IPayoffAmount GetPayoffAmount(string loanReferenceNumber, DateTimeOffset payoffDate);

        void UpdatePaymentMethodOfScheduledInstallments(string loanReferenceNumber, PaymentMethod newPaymentMethod);

        double GetCurrentDue(string loanReferenceNumber, DateTimeOffset date);
    }
}
