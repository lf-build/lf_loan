﻿using LendFoundry.Security.Tokens;
using LendFoundry.Configuration.Client;

namespace LendFoundry.Loans.Schedule
{
    public class GracePeriodFactory : IGracePeriodFactory
    {
        public GracePeriodFactory(IConfigurationServiceFactory<LoanConfiguration> loanConfigurationFactory)
        {
            LoanConfigurationFactory = loanConfigurationFactory;
        }

        private IConfigurationServiceFactory<LoanConfiguration> LoanConfigurationFactory { get; }

        public IGracePeriod Create(ITokenReader reader)
        {
            var configurationService = LoanConfigurationFactory.Create(reader);
            var configuration = configurationService.Get();
            return new GracePeriod(configuration.Schedule);
        }
    }
}
