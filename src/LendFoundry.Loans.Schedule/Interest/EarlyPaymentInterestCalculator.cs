﻿using LendFoundry.Amortization;
using LendFoundry.Amortization.Client;
using LendFoundry.Amortization.Interest;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Schedule.Interest
{
    public class EarlyPaymentInterestCalculator
    {
        public EarlyPaymentInterestCalculator(IAmortizationService amortizationService)
        {
            AmortizationService = amortizationService;
        }

        private IAmortizationService AmortizationService { get; }

        //TODO: Refactoring: interest calculation code in ReamortizationRectifier is a duplicate of this code
        public double GetInterest(IInstallment installment, DateTimeOffset paymentDate, double paymentAmount, ILoan loan, List<IInstallment> installments)
        {
            var lastPrincipalAndInterestInstallment = installments
                .Where(i => i.DueDate < installment.DueDate)
                .Where(i => i.Interest > 0.0)
                .LastOrDefault();

            var startDate = GetInterestPaymentDate(lastPrincipalAndInterestInstallment) ?? loan.Terms.OriginationDate;
            var interestAccrualPeriods = new List<InterestAccrualPeriod>();

            // First interest accrual period
            interestAccrualPeriods.Add(new InterestAccrualPeriod
            {
                Principal = lastPrincipalAndInterestInstallment?.EndingBalance ?? loan.Terms.LoanAmount,
                Start = startDate
            });

            // Interest accrual periods in between
            interestAccrualPeriods.AddRange
            (
                installments
                    .Where(i => i != lastPrincipalAndInterestInstallment) 
                    .Where(i => (i.AnniversaryDate ?? i.DueDate) > startDate)
                    .Where(i => (i.AnniversaryDate ?? i.DueDate) < paymentDate)
                    .Select(i => new InterestAccrualPeriod
                    {
                        Principal = i.EndingBalance,
                        Start = i.DueDate.Value
                    })
            );

            interestAccrualPeriods.Last().End = paymentDate;

            var loanTerms = new Amortization.LoanTerms
            {
                OriginationDate = loan.Terms.OriginationDate,
                LoanAmount = loan.Terms.LoanAmount,
                Term = loan.Terms.Term,
                Rate = loan.Terms.Rate
            };

            return AmortizationService.GetSimpleInterest(loanTerms, interestAccrualPeriods);
        }

        private static DateTimeOffset? GetInterestPaymentDate(IInstallment installment)
        {
            if (installment == null)
                return null;

            return IsEarlyPayment(installment) ? installment.PaymentDate : installment?.AnniversaryDate ?? installment?.DueDate;
        }

        private static bool IsEarlyPayment(IInstallment installment)
        {
            return
                installment?.Type == InstallmentType.Scheduled &&
                installment?.PaymentDate < installment?.AnniversaryDate;
        }
    }
}
