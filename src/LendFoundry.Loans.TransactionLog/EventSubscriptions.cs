using System;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Abstractions;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Payment;
using LendFoundry.Loans.Payment.Events;
using LendFoundry.Loans.TransactionLog.Data;
using LendFoundry.Loans.TransactionLog.Models;
using LendFoundry.Security.Tokens;
using LendFoundry.EventHub.Client;

namespace LendFoundry.Loans.TransactionLog
{
    public class EventSubscriptions : IEventSubscription<PaymentReceived>
    {
        public EventSubscriptions
        (
            ITokenReaderFactory tokenReaderFactory,
            ITransactionLogRepositoryFactory repositoryFactory,
            ILoggerFactory loggerFactory,
            IConfigurationServiceFactory configurationFactory,
            ITenantTimeFactory tenantTimeFactory,
            IEventHubClientFactory eventHubClientFactory
        )
        {
            TokenReaderFactory = tokenReaderFactory;
            RepositoryFactory = repositoryFactory;
            LoggerFactory = loggerFactory;
            ConfigurationFactory = configurationFactory;
            TenantTimeFactory = tenantTimeFactory;
            EventHubClientFactory = eventHubClientFactory;
        }

        private ITokenReaderFactory TokenReaderFactory { get; }

        public ITransactionLogRepositoryFactory RepositoryFactory { get; set; }

        private ILoggerFactory LoggerFactory { get; }

        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private ITenantTimeFactory TenantTimeFactory { get; }

        private IEventHubClientFactory EventHubClientFactory { get; }

        public void Handle(IEventInfo @event, PaymentReceived data)
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            try
            {
                logger.Info("Payment received.");
                var reader = TokenReaderFactory.Create(@event.TenantId, "transactionlog");
                var repository = RepositoryFactory.Create(reader);
                var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
                var eventHub = EventHubClientFactory.Create(reader);

                var transaction = new TransactionInfo
                {
                    LoanReference = data.LoanReferenceNumber,
                    Date = data.DueDate,
                    Code = GetTransactionCode(data),
                    Amount = data.Amount,
                    TenantId = @event.TenantId,
                    Timestamp = tenantTime.Now,
                    PaymentId = data.PaymentId
                };
                logger.Info("Adding transaction.", transaction);
                repository.Save(transaction);
                logger.Info("Transaction added with success.");
                eventHub.Publish("LoanTransactionAdded", transaction);
            }
            catch (Exception exception)
            {
                logger.Error("Problem adding transaction for received payment", exception, data);
            }
        }

        private static string GetTransactionCode(PaymentReceived data)
        {
            if (!string.IsNullOrEmpty(data.Code))
                return data.Code;

            return data.Type == PaymentType.Fee ? "140" : "124";

            //----------------------------------

            //if (!string.IsNullOrEmpty(data.Code))
            //    return int.Parse(data.Code);

            //return data.Type == PaymentType.Fee ? 140 : 124;
        }
    }
}