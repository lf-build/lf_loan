using System.Collections.Generic;

namespace LendFoundry.Loans.TransactionLog
{
    public interface ITransactionLogConfiguration
    {
        List<Models.TransactionCode> Transactions { get; set; }
    }
}