﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Payment.Events;
using LendFoundry.Loans.TransactionLog.Data;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using System;

namespace LendFoundry.Loans.TransactionLog
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            var tenantHost = Environment.GetEnvironmentVariable("LOAN_TRANSACTION_TENANT_HOST") ?? "tenant";
            var tenantPort = int.Parse(Environment.GetEnvironmentVariable("LOAN_TRANSACTION_TENANT_PORT") ?? "5000");
            var configurationHost = Environment.GetEnvironmentVariable("LOAN_TRANSACTION_CONFIG_HOST") ?? "configuration";
            var configurationPort = int.Parse(Environment.GetEnvironmentVariable("LOAN_TRANSACTION_CONFIG_PORT") ?? "5000");
            var hubHostName = Environment.GetEnvironmentVariable("LOAN_TRANSACTION_EVENTHUB_HOST") ?? "eventhub";
            var hubHostPort = Environment.GetEnvironmentVariable("LOAN_TRANSACTION_EVENTHUB_PORT") ?? "5000";

            // Database configuration
            services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration
            (
                connectionString: Environment.GetEnvironmentVariable("LOAN_TRANSACTION_MONGO_CONNECTIONSTRING") ?? "mongodb://mongo:27017",
                database: Environment.GetEnvironmentVariable("LOAN_TRANSACTION_MONGO_DATABASE") ?? "loan-transaction"
            ));

            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantService(tenantHost, tenantPort);
            services.AddEventHub(hubHostName, int.Parse(hubHostPort), Settings.ServiceName);

            // The event hub client must be created with a static token reader because
            // it publishes events asynchronously and the http context may not be available
            // by the time the publishing request is sent.
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });
            
            services.AddTransient<ITransactionLogRepository, TransactionLogRepository>();
            services.AddTransient<ITransactionLogRepositoryFactory, TransactionLogRepositoryFactory>();
            services.AddConfigurationService<TransactionLogConfiguration>(configurationHost, configurationPort, Settings.ServiceName);
            services.AddTransient<ITransactionLogConfiguration>(p => p.GetService<IConfigurationService<TransactionLogConfiguration>>().Get());

            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();

            services.AddEventSubscription<PaymentReceived, EventSubscriptions>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
            app.UseEventHub();
        }
    }
}