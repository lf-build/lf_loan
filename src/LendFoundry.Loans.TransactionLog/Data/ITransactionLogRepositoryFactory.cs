using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;

namespace LendFoundry.Loans.TransactionLog.Data
{
    public interface ITransactionLogRepositoryFactory
    {
        ITransactionLogRepository Create(ITokenReader reader);
    }

    public class TransactionLogRepositoryFactory : ITransactionLogRepositoryFactory
    {
        public TransactionLogRepositoryFactory
        (
            IMongoConfiguration repositoryConfiguration, 
            IConfigurationServiceFactory configurationServiceFactory,
            IConfigurationServiceFactory<TransactionLogConfiguration> transactionConfigurationFactory,
            ITenantServiceFactory tenantServiceFactory,
            ITenantTimeFactory tenantTimeFactory
        )
        {
            RepositoryConfiguration = repositoryConfiguration;
            ConfigurationServiceFactory = configurationServiceFactory;
            TransactionConfigurationFactory = transactionConfigurationFactory;
            TenantServiceFactory = tenantServiceFactory;
            TenantTimeFactory = tenantTimeFactory;
        }

        private IMongoConfiguration RepositoryConfiguration { get; }

        private IConfigurationServiceFactory ConfigurationServiceFactory { get; }

        private IConfigurationServiceFactory<TransactionLogConfiguration> TransactionConfigurationFactory { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }

        private ITenantTimeFactory TenantTimeFactory { get; }

        public ITransactionLogRepository Create(ITokenReader reader)
        {
            var configurationService = TransactionConfigurationFactory.Create(reader);
            var tenantTime = TenantTimeFactory.Create(ConfigurationServiceFactory, reader);
            var tenantService = TenantServiceFactory.Create(reader);
            return new TransactionLogRepository(RepositoryConfiguration, configurationService.Get(), tenantTime, tenantService);
        }
    }
}