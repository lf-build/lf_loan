﻿using LendFoundry.Loans.TransactionLog.Models;
using System.Collections.Generic;

namespace LendFoundry.Loans.TransactionLog.Data
{
    public interface ITransactionLogRepository
    {
        ITransactionInfo Save(ITransactionInfo transaction);
        List<ITransactionInfo> GetByLoanReference(string loanId);
        ITransactionInfo GetByTransactionId(string loanReferenceNumber, string transactionId);
    }
}
