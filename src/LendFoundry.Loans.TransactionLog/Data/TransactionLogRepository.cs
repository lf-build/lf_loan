﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Loans.TransactionLog.Models;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.TransactionLog.Data
{
    public class TransactionLogRepository : MongoRepository<ITransactionInfo, TransactionInfo>, ITransactionLogRepository
    {
        static TransactionLogRepository()
        {
            BsonClassMap.RegisterClassMap<TransactionInfo>(map =>
            {
                map.AutoMap();
                map.MapMember(p => p.Date).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(p => p.Timestamp).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(TransactionInfo);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public TransactionLogRepository
        (
            IMongoConfiguration mongoConfiguration,
            ITransactionLogConfiguration serviceConfiguration,
            ITenantTime tenantTime,
            ITenantService tenantService
        ) : base(tenantService, mongoConfiguration, "transactions")
        {
            if (serviceConfiguration == null)
                throw new ArgumentNullException(nameof(serviceConfiguration));

            ServiceConfiguration = serviceConfiguration;
            TenantTime = tenantTime;
        }

        private ITenantTime TenantTime { get; }

        private ITransactionLogConfiguration ServiceConfiguration { get; }

        public List<ITransactionInfo> GetByLoanReference(string loanId)
        {
            return Query.Where(q => q.LoanReference == loanId).ToList();
        }

        public ITransactionInfo GetByTransactionId(string loanReferenceNumber, string transactionId)
        {
            if (string.IsNullOrEmpty(transactionId))
                throw new ArgumentNullException(nameof(transactionId));

            if (string.IsNullOrEmpty(loanReferenceNumber))
                throw new ArgumentNullException(nameof(loanReferenceNumber));

            return Query.FirstOrDefault(q => q.LoanReference == loanReferenceNumber && q.Id == transactionId);
        }

        public ITransactionInfo Save(ITransactionInfo transaction)
        {
            if (transaction == null)
                throw new ArgumentNullException(nameof(transaction));

            transaction.Id = ObjectId.GenerateNewId().ToString();
            transaction.TenantId = TenantService.Current.Id;
            transaction.Date = TenantTime.FromDate(transaction.Date.DateTime);
            transaction.Description = ServiceConfiguration.Transactions.FirstOrDefault(t => t.Code == transaction.Code)?.Description;

            Collection.InsertOneAsync(transaction);
            return transaction;
        }
    }
}