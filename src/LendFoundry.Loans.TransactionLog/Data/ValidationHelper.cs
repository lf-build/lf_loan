﻿using Microsoft.AspNet.Mvc;
using System;

namespace LendFoundry.Loans.TransactionLog.Data
{
    public static class ValidationHelper
    {
        public static ObjectResult ReturnServerError(this Exception ex)
        {
            return new ObjectResult(new { message = $"Unhandled server exception: {ex.Message}" }) { StatusCode = 500 };
        }
    }
}
