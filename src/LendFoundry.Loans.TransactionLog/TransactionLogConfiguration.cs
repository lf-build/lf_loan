using System.Collections.Generic;

namespace LendFoundry.Loans.TransactionLog
{
    public class TransactionLogConfiguration : ITransactionLogConfiguration
    {
        public List<Models.TransactionCode> Transactions { get; set; }
    }
}