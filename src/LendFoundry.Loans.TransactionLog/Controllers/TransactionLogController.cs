﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.TransactionLog.Data;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Mvc;
using System;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.TransactionLog.Models;

namespace LendFoundry.Loans.TransactionLog.Controllers
{
    [Route("/")]
    public class TransactionLogController : Controller
    {
        public TransactionLogController
            (
                ILogger logger, 
                ITransactionLogRepository repository, 
                ITenantService tenant, 
                IEventHubClient eventHub,
                ITenantTime tenantTime
            )
        {
            if (logger == null)
                throw new ArgumentException($"Missing parameter {nameof(logger)}");

            if (repository == null)
                throw new ArgumentException($"Missing parameter {nameof(repository)}");

            if (tenant == null)
                throw new ArgumentException($"Missing parameter {nameof(tenant)}");

            if (eventHub == null)
                throw new ArgumentException($"Missing parameter {nameof(eventHub)}");

            if (tenantTime == null)
                throw new ArgumentException($"Missing parameter {nameof(tenantTime)}");

            Logger = logger;
            Repository = repository;
            Tenant = tenant;
            EventHub = eventHub;
            TenantTime = tenantTime;
        }

        private ILogger Logger { get; }

        private ITransactionLogRepository Repository { get; }

        private ITenantService Tenant { get; }

        private IEventHubClient EventHub { get; }

        public ITenantTime TenantTime { get; }

        [HttpPost]
        public IActionResult AddTransaction([FromBody] TransactionInfo transaction)
        {
            if (transaction == null)
                return ErrorResult.BadRequest("Transaction must to be informed.");

            transaction.TenantId = Tenant.Current.Id;
            transaction.Timestamp = TenantTime.Now;
            Repository.Save(transaction);
            EventHub.Publish("LoanTransactionAdded", transaction);
            Logger.Info("Loan transaction log created successfuly", transaction);
            return new HttpOkObjectResult(transaction);
        }

        [HttpGet("{loanReferenceNumber}")]
        public IActionResult GetTransactions(string loanReferenceNumber)
        {
            return new HttpOkObjectResult(Repository.GetByLoanReference(loanReferenceNumber));
        }
        
        [HttpGet("{loanReferenceNumber}/{transactionId}")]
        public IActionResult GetTransactionByLoanAndTransaction(string loanReferenceNumber, string transactionId)
        {
            var transaction = Repository.GetByTransactionId(loanReferenceNumber, transactionId);

            if (transaction == null)
                return new HttpNotFoundResult();

            return new HttpOkObjectResult(transaction);
        }
    }
}