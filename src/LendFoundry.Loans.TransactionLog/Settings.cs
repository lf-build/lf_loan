﻿namespace LendFoundry.Loans.TransactionLog
{
    internal static class Settings
    {
        public static string ServiceName { get; } = "loans-transactionslog";
    }
}