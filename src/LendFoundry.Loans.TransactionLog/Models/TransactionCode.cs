﻿namespace LendFoundry.Loans.TransactionLog.Models
{
    public class TransactionCode
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}