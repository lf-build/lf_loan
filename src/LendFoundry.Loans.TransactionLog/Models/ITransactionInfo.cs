﻿using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.Loans.TransactionLog.Models
{
    public interface ITransactionInfo : IAggregate
    {        
        string Code { get; set; }

        string Description { get; set; }

        DateTimeOffset Date { get; set; }

        string LoanReference { get; set; }

        double Amount { get; set; }

        string Notes { get; set; }

        string PaymentId { get; set; }

        DateTimeOffset Timestamp { get; set; }
    }
}