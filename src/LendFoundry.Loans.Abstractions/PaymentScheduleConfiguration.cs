﻿namespace LendFoundry.Loans
{
    public class PaymentScheduleConfiguration
    {
        public int GracePeriod { get; set; }
        public int PayoffPeriod { get; set; }
        public int AchCoolingPeriod { get; set; }
        public int CheckCoolingPeriod { get; set; }
    }
}
