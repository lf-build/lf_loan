using System;

namespace LendFoundry.Loans
{
    public static class DoubleExtensions
    {
        private const double Epsilon = 0.0000000001;

        public static bool IsEqualTo(this double first, double second)
        {
            return Math.Abs(first - second) <= Epsilon;
        }
    }
}