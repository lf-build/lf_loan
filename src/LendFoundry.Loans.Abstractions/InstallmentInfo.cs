﻿namespace LendFoundry.Loans
{
    public class InstallmentInfo : IInstallmentInfo
    {
        public ILoan Loan { get; set; }
        public IInstallment Installment { get; set; }
    }
}
