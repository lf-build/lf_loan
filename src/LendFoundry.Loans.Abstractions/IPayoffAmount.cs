﻿using System;

namespace LendFoundry.Loans
{
    public interface IPayoffAmount
    {
        double Amount { get; }
        DateTimeOffset ValidUntil { get; }
    }
}
