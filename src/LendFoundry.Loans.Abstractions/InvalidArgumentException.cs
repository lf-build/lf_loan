﻿using System;

namespace LendFoundry.Loans
{
    public class InvalidArgumentException : ArgumentException
    {
        public InvalidArgumentException(string message) : base(message)
        {
        }

        public InvalidArgumentException(string paramName, string message) : base(message, paramName)
        {
        }

        public InvalidArgumentException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public InvalidArgumentException(string paramName, string message, Exception innerException) : base(message, paramName, innerException)
        {
        }
    }
}
