﻿using System;

namespace LendFoundry.Loans
{
    public interface IPaymentMethodEngine
    {
        void Change(string referenceNumber, PaymentMethod newPaymentMethod, Action<string> changed);
    }
}