﻿using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.Loans
{
    public interface IBorrower : IAggregate
    {
        string ReferenceNumber { get; set; }
        string FirstName { get; set; }
        string MiddleInitial { get; set; }
        string LastName { get; set; }
        DateTimeOffset DateOfBirth { get; set; }
        string SSN { get; set; }
        string Email { get; set; }
        bool IsPrimary { get; set; }
        Address[] Addresses { get; set; }
        Phone[] Phones { get; set; }
        string LoanReferenceNumber { get; set; }
    }
}
