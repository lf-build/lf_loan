﻿using System;

namespace LendFoundry.Loans
{
    public interface ITransaction
    {
        string LoanReferenceNumber { get; }
        string Id { get; }
        string Code { get; }
        string Description { get; }
        DateTimeOffset Date { get; }
        double Amount { get; }
        string Notes { get; }
        string PaymentId { get; }
        DateTimeOffset Timestamp { get; }
    }
}