﻿using System;

namespace LendFoundry.Loans
{
    public interface IInstallment
    {
        PaymentMethod PaymentMethod { get; set; }
        DateTimeOffset? AnniversaryDate { get; set; }
        DateTimeOffset? DueDate { get; set; }
        DateTimeOffset? PaymentDate { get; set; }
        string PaymentId { get; set; }
        double OpeningBalance { get; set; }
        double PaymentAmount { get; set; }
        double Principal { get; set; }
        double Interest { get; set; }
        double EndingBalance { get; set; }
        InstallmentStatus Status { get; set; }
        InstallmentType Type { get; set; }
    }
}