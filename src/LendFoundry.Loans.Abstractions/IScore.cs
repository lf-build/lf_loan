﻿using System;

namespace LendFoundry.Loans
{
    public interface IScore
    {
        string Name { get; set; }
        string InitialScore { get; set; }
        DateTimeOffset InitialDate { get; set; }
        string UpdatedScore { get; set; }
        DateTimeOffset? UpdatedDate { get; set; }
    }
}
