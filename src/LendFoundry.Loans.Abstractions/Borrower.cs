﻿using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;
using System;

namespace LendFoundry.Loans
{
    public class Borrower : Aggregate, IBorrower
    {
        [JsonIgnore]
        public override string Id { get; set; }

        [JsonIgnore]
        public string LoanReferenceNumber { get; set; }

        public string ReferenceNumber { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public DateTimeOffset DateOfBirth { get; set; }
        public string SSN { get; set; }
        public string Email { get; set; }
        public bool IsPrimary { get; set; }
        public Address[] Addresses { get; set; }
        public Phone[] Phones { get; set; }
    }
}
