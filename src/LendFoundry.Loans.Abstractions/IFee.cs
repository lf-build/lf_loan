﻿namespace LendFoundry.Loans
{
    public interface IFee
    {        
        FeeType Type { get; }
        double Amount { get; }
        string Code { get; }
        IFee To(FeeType type, double loanAmount);
    }
}
