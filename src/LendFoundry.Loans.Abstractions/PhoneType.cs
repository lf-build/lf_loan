﻿namespace LendFoundry.Loans
{
    public enum PhoneType
    {
        Home,
        Work,
        Mobile,
        Other
    }
}
