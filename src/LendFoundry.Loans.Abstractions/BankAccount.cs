﻿using System;

namespace LendFoundry.Loans
{
    public class BankAccount : IBankAccount
    {
        public string RoutingNumber { get; set; }
        public string AccountNumber { get; set; }
        public BankAccountType AccountType { get; set; }
        public string BankName { get; set; }
        public bool IsPrimary { get; set; }
        public DateTimeOffset EffectiveDate { get; set; }
    }
}