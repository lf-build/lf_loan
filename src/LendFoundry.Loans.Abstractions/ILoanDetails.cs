using System.Collections.Generic;

namespace LendFoundry.Loans
{
    public interface ILoanDetails
    {
        ILoanInfo LoanInfo { get; }
        IEnumerable<ITransaction> Transactions { get; }
        IPaymentSchedule PaymentSchedule { get; }
    }
}
