﻿using System;

namespace LendFoundry.Loans
{
    public class Fee : IFee
    {
        public string Code { get; set; }
        public FeeType Type { get; set; }
        public double Amount { get; set; }

        public IFee To(FeeType newType, double loanAmount)
        {
            if (newType == Type)
                return this;

            return new Fee
            {
                Code = Code,
                Amount = ConvertAmountTo(newType, loanAmount),
                Type = newType
            };
        }

        private double ConvertAmountTo(FeeType newType, double loanAmount)
        {
            if (newType == FeeType.Percent)
            {
                var percent = Amount / loanAmount;
                return Math.Round(percent * 100.0, 2, MidpointRounding.AwayFromZero);
            }

            if (newType == FeeType.Fixed)
            {
                var percent = Amount / 100.0;
                return Math.Round(percent * loanAmount, 2, MidpointRounding.AwayFromZero);
            }

            throw new Exception($"Unsupported fee type: {newType}");
        }
    }
}
