﻿using LendFoundry.Loans.Status;
using System;

namespace LendFoundry.Loans
{
    public class LoanStatus
    {
        public static readonly LoanStatus Onboarding = new LoanStatus("100.01", "Onboarding");
        public static readonly LoanStatus Onboarded = new LoanStatus("100.02", "Onboarded");
        public static readonly LoanStatus InService = new LoanStatus("100.03", "In service");

        LoanStatus() { }

        public LoanStatus(string code, string name)
        {
            Code = code;
            Name = name;
        }

        public string Code { get; set; }
        public string Name { get; set; }
        public DateTimeOffset? StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }
        public string LabelName { get; set; }
        public string LabelStyle { get; set; }
        public Actions[] Actions { get; set; }
        public string[] Transitions { get; set; }
    }
}
