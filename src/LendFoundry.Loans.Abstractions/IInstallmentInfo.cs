﻿namespace LendFoundry.Loans
{
    public interface IInstallmentInfo
    {
        ILoan Loan { get; }
        IInstallment Installment { get; }
    }
}
