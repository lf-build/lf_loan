﻿using System;

namespace LendFoundry.Loans
{
    public class PayoffAmount : IPayoffAmount
    {
        public double Amount { get; set; }
        public DateTimeOffset ValidUntil { get; set; }
    }
}
