﻿namespace LendFoundry.Loans
{
    public enum FeeType
    {
        Undefined,
        Fixed,
        Percent
    }
}
