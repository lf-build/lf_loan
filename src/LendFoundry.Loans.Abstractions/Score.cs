﻿using System;

namespace LendFoundry.Loans
{
    public class Score : IScore
    {
        public string Name { get; set; }
        public string InitialScore { get; set; }
        public DateTimeOffset InitialDate { get; set; }
        public string UpdatedScore { get; set; }
        public DateTimeOffset? UpdatedDate { get; set; }
    }
}
