﻿using System.Collections.Generic;

namespace LendFoundry.Loans
{
    public class LoanDetails : ILoanDetails
    {
        public ILoanInfo LoanInfo { get; set; }
        public IEnumerable<ITransaction> Transactions { get; set; }
        public IPaymentSchedule PaymentSchedule { get; set; }
    }
}
