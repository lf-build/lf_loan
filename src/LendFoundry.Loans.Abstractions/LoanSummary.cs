﻿namespace LendFoundry.Loans
{
    public class LoanSummary
    {
        public double MonthlyInterestEarned { get; set; }
        public double NetInterestAccrued { get; set; }
        public double PriorMonthNetInterestAccrued { get; set; }
        public string Paystring { get; set; }
    }
}
