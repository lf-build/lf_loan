﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Loans
{
    public interface ILoanStatusEngine
    {
        [Obsolete("This method is not supported anymore.")]

        LoanStatus Get(string code);

        LoanStatus GetStatus(string referenceNumber);
        void UpdateLoanStatus(string referenceNumber, string nextStatus, DateTimeOffset? startDate, DateTimeOffset? endDate);

        bool IsAllowedAction(string referenceNumber, string actionType);
    }
}
