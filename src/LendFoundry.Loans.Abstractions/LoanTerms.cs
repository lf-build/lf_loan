﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Loans
{
    public class LoanTerms : ILoanTerms
    {
        public DateTimeOffset OriginationDate { get; set; }
        public double LoanAmount { get; set; }
        public int Term { get; set; }
        public double Rate { get; set; }
        public RateType RateType { get; set; }
        public IEnumerable<IFee> Fees { get; set; }
        public double APR { get; set; }
        public DateTimeOffset ApplicationDate { get; set; }
        public DateTimeOffset FundedDate { get; set; }
        public double PaymentAmount { get; set; }
    }
}
