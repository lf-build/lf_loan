﻿using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans
{
    public class PaymentSchedule : Aggregate, IPaymentSchedule
    {
        [JsonIgnore]
        public override string Id { get; set; }

        private IEnumerable<IInstallment> installments;
        public string LoanReferenceNumber { get; set; }

        public IEnumerable<IInstallment> Installments
        {
            get
            {
                if (installments == null)
                    return new List<IInstallment>();

                return installments
                    .OrderBy(installment => installment.PaymentDate ?? installment.AnniversaryDate ?? installment.DueDate)
                    .ToList();
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException(nameof(value));

                installments = value;
            }
        }

        public IInstallment FirstInstallment
        {
            get
            {
                return Installments
                    .OrderBy(installment => installment.DueDate)
                    .First();
            }
        }

        public IInstallment LastPaidInstallment
        {
            get
            {
                return Installments
                    .OrderBy(GetEffectiveInstallmentDate)
                    .LastOrDefault(installment => installment.Status == InstallmentStatus.Completed && installment.PaymentAmount > 0);
            }
        }

        public IInstallment FirstScheduledInstallment
        {
            get
            {
                return Installments
                    .OrderBy(installment => installment.DueDate)
                    .FirstOrDefault(installment => installment.Status == InstallmentStatus.Scheduled);
            }
        }

        public IInstallment GetLatestInstallmentUpTo(DateTime date)
        {
            return Installments
                .OrderBy(GetEffectiveInstallmentDate)
                .LastOrDefault(installment => GetEffectiveInstallmentDate(installment) <= date);
        }

        private static DateTimeOffset? GetEffectiveInstallmentDate(IInstallment installment)
        {
            if (installment.PaymentDate.HasValue && installment.DueDate.HasValue)
                return installment.PaymentDate.Value > installment.DueDate.Value
                    ? installment.PaymentDate.Value
                    : installment.DueDate.Value;

            if (installment.PaymentDate.HasValue)
                return installment.PaymentDate.Value;

            return installment.DueDate.Value;
        }
    }
}
