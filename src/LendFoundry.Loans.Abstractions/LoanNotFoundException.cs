using System;

namespace LendFoundry.Loans
{
    public class LoanNotFoundException : Exception
    {
        public LoanNotFoundException(string loanReferenceNumber) : base($"Loan not found with reference number #{loanReferenceNumber}")
        {
            LoanReferenceNumber = loanReferenceNumber;
        }

        public string LoanReferenceNumber { get; set; }
    }
}