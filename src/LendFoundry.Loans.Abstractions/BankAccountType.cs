﻿namespace LendFoundry.Loans
{
    public enum BankAccountType
    {
        Undefined,
        Checking,
        Savings
    }
}
