﻿namespace LendFoundry.Loans
{
    public class Phone
    {
        public bool IsPrimary { get; set; }
        public string Number { get; set; }
        public PhoneType Type { get; set; }
    }
}
