﻿namespace LendFoundry.Loans
{
    public enum InstallmentStatus
    {
        Scheduled,
        Pending,
        Completed
    }
}
