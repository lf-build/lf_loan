﻿using System;

namespace LendFoundry.Loans
{
    public interface IBankAccount
    {
        string RoutingNumber { get; }
        string AccountNumber { get; set; }
        BankAccountType AccountType { get; }
        string BankName { get; set; }
        bool IsPrimary { get; set; }
        DateTimeOffset EffectiveDate { get; set; }
    }
}
