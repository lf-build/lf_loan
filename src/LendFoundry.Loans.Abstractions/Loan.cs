﻿using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Loans
{
    public class Loan : Aggregate, ILoan
    {
        [JsonIgnore]
        public override string Id { get; set; }
        public string ReferenceNumber { get; set; }
        public string FundingSource { get; set; }
        public string Purpose { get; set; }
        public LoanStatus Status { get; set; }
        public ILoanTerms Terms { get; set; }
        public IEnumerable<IBorrower> Borrowers { get; set; }
        public ILoanInvestor Investor { get; set; }
        public IEnumerable<IScore> Scores { get; set; }
        public string Grade { get; set; }
        public double? PreCloseDti { get; set; }
        public double? PostCloseDti { get; set; }
        public double MonthlyIncome { get; set; }
        public string HomeOwnership { get; set; }
        public string CampaignCode { get; set; }
        public IEnumerable<IBankAccount> BankAccounts { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
    }
}