﻿namespace LendFoundry.Loans.Status
{
    public enum Actions
    {
        None = 0,
        AutoACH = 1,
        AccrueInterest = 2,
        ManualACH = 3,
        NonAutoPay = 4
    }
}
