﻿namespace LendFoundry.Loans
{
    public enum RateType
    {
        Undefined,
        Percent,
        Factor
    }
}
    
