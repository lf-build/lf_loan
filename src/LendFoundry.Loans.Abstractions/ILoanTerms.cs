﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Loans
{
    public interface ILoanTerms
    {
        double LoanAmount { get; set; }
        DateTimeOffset OriginationDate { get; set; }
        int Term { get; set; }
        double Rate { get; set; }
        RateType RateType { get; set; }
        IEnumerable<IFee> Fees { get; set; }
        double APR { get; set; }
        DateTimeOffset ApplicationDate { get; set; }
        DateTimeOffset FundedDate { get; set; }
        double PaymentAmount { get; set; }
    }
}
