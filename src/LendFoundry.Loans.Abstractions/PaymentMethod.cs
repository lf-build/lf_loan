﻿namespace LendFoundry.Loans
{
    public enum PaymentMethod
    {
        Undefined,
        ACH,
        Check,
        NA
    }
}