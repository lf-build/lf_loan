﻿using System.Collections.Generic;

namespace LendFoundry.Loans
{
	public static class IListExtensions
	{
		public static T Previous<T>(this IList<T> list, T actual) where T : class
		{
			if (!list.Contains(actual))
			{
				return null;
			}

			var before = list.IndexOf(actual) - 1;

			return before == -1 ? null : list[before];
		}

		public static T Next<T>(this IList<T> list, T actual) where T : class
		{
			if (!list.Contains(actual))
			{
				return null;
			}

			var before = list.IndexOf(actual) + 1;

			return before >= list.Count ? null : list[before];
		}
	}
}