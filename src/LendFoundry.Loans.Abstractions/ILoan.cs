﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;

namespace LendFoundry.Loans
{
    public interface ILoan : IAggregate
    {
        string ReferenceNumber { get; set; }
        string FundingSource { get; set; }
        string Purpose { get; set; }
        LoanStatus Status { get; set; }
        ILoanTerms Terms { get; set; }
        IEnumerable<IBorrower> Borrowers { get; set; }
        ILoanInvestor Investor { get; set; }
        IEnumerable<IScore> Scores { get; set; }
        string Grade { get; set; }
        double? PreCloseDti { get; set; }
        double? PostCloseDti { get; set; }
        double MonthlyIncome { get; set; }
        string HomeOwnership { get; set; }
        string CampaignCode { get; set; }
        IEnumerable<IBankAccount> BankAccounts { get; set; }
        PaymentMethod PaymentMethod { get; set; }
    }
}