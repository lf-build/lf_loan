﻿namespace LendFoundry.Loans
{
    public enum InstallmentType
    {
        Scheduled,
        Additional,
        Payoff,
        ChargeOff
    }
}
