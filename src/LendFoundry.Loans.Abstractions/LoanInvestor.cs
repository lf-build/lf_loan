﻿using System;

namespace LendFoundry.Loans
{
    public class LoanInvestor : ILoanInvestor
    {
        public string Id { get; set; }
        public DateTimeOffset LoanPurchaseDate { get; set; }
    }
}
