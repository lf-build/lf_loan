﻿namespace LendFoundry.Loans
{
    public interface ILoanEngineResolver
    {
        T Resolve<T>();
    }
}
