﻿namespace LendFoundry.Loans.Events
{
    public class LoanPrimaryBankAccountChanged
    { 
        public LoanPrimaryBankAccountChanged
        (
            string referenceNumber,
            IBankAccount oldAccount,
            IBankAccount newAccount
        )
        {
            ReferenceNumber = referenceNumber;
            OldAccount = oldAccount;
            NewAccount = newAccount;
        }

        public string ReferenceNumber { get; set; }
        public IBankAccount OldAccount { get; set; }
        public IBankAccount NewAccount { get; set; }
    }
}