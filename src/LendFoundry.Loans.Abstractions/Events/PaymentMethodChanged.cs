﻿namespace LendFoundry.Loans.Events
{
    public class PaymentMethodChanged
    {
        public string ReferenceNumber { get; set; }
        public PaymentMethod OldPaymentMethod { get; set; }
        public PaymentMethod NewPaymentMethod { get; set; }
    }
}
