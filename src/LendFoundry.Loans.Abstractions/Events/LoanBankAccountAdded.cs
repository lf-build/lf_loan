﻿namespace LendFoundry.Loans.Events
{
    public class LoanBankAccountAdded
    {
        public LoanBankAccountAdded
        (
            string referenceNumber,
            IBankAccount bankAccount            
        )
        {
            ReferenceNumber = referenceNumber;
            BankAccount = bankAccount;            
        }

        public IBankAccount BankAccount { get; set; }
        public string ReferenceNumber { get; set; }
    }
}
