﻿using System;

namespace LendFoundry.Loans.Events
{
    public class LoanStatusUpdated
    {
        public LoanStatusUpdated
        (
            string referenceNumber,
            string oldStatus,
            string newStatus,
            DateTimeOffset? startDate,
            DateTimeOffset? endDate,
            string oldStatusDescription,
            string newStatusDescription
        )
        {
            ReferenceNumber = referenceNumber;
            OldStatus = oldStatus;
            NewStatus = newStatus;
            StartDate = startDate;
            EndDate = endDate;
            OldStatusDescription = oldStatusDescription;
            NewStatusDescription = newStatusDescription;
        }

        public string ReferenceNumber { get; set; }
        public string OldStatus { get; set; }
        public string NewStatus { get; set; }
        public DateTimeOffset? StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }
        public string OldStatusDescription { get; set; }
        public string NewStatusDescription { get; set; }
    }
}
