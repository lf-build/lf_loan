﻿namespace LendFoundry.Loans.Events
{
    public class LoanUpdated
    {
        public LoanUpdated(string referenceNumber)
        {
            ReferenceNumber = referenceNumber;
        }

        public string ReferenceNumber { get; set; }
    }
}
