﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace LendFoundry.Loans
{
    public interface IPaymentSchedule: IAggregate
    {
        string LoanReferenceNumber { get; set; }
        IEnumerable<IInstallment> Installments { get; set; }

        /// <summary>
        /// Gets the first installment of the payment schedule of a loan
        /// </summary>
        IInstallment FirstInstallment { get; }

        /// <summary>
        /// Finds the last paid installment
        /// </summary>
        /// <returns>The last paid installment, or null if no paid installment is found</returns>
        IInstallment LastPaidInstallment { get; }

        /// <summary>
        /// Finds the first scheduled installment
        /// </summary>
        /// <returns>The last paid installment, or null if no scheduled installment is found</returns>
        IInstallment FirstScheduledInstallment { get; }

        /// <summary>
        /// Finds the latest installment up to (and including) a given date.
        /// The transaction date takes precedence over the due date.
        /// </summary>
        /// <returns>The latest installment up to the given date, or null if no such installment is found.</returns>
        /// <remarks>
        /// Suppose that we have the following installments:
        ///   
        ///   #  DueDate     TransactionDate
        ///   -----------------------------
        ///   1  2015-09-25       2015-09-25
        ///   2  2015-10-25       2015-10-27
        ///   3  2015-11-25       null
        ///   4  2015-12-25       null
        /// 
        /// If date is 2015-09-25, #1 is returned.
        /// 
        /// If date is 2015-09-30, #1 is returned, because that's the latest installment
        /// relative to that date.
        /// 
        /// If date is 2015-10-26, #1 is returned, because that's the latest installment
        /// relative to that date. Installment #2 is not returned here because the
        /// TransactionDate precedes the DueDate.
        /// 
        /// If date is 2015-10-28, #2 is returned, because that's the latest installment
        /// relative to that date.
        /// 
        /// If date is 2015-11-25, #3 is returned.
        /// 
        /// </remarks>
        IInstallment GetLatestInstallmentUpTo(DateTime date);
    }
}
