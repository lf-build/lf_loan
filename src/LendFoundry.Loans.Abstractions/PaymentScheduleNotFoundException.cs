﻿using System;

namespace LendFoundry.Loans
{
    public class PaymentScheduleNotFoundException : Exception
    {
        public PaymentScheduleNotFoundException(string loanReferenceNumber) : base($"Payment schedule not found for loan #{loanReferenceNumber}")
        {
            LoanReferenceNumber = loanReferenceNumber;
        }

        public string LoanReferenceNumber { get; set; }
    }
}
