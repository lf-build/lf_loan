﻿namespace LendFoundry.Loans
{
    public class Investor : IInvestor
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public BankAccount BankAccount { get; set; }
    }
}
