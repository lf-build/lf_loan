﻿namespace LendFoundry.Loans
{
    public interface IBankAccountEngine
    {
        void Create(string loanReferenceNumber, IBankAccount bankAccount);
        void SetAsPrimary(string loanReferenceNumber, string accountNumber);
    }
}
