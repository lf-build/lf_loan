﻿namespace LendFoundry.Loans
{
    public interface IInvestor
    {
        string Id { get; set; }
        string Name { get; set; }
        BankAccount BankAccount { get; set; }
    }
}
