using LendFoundry.Loans.Schedule;

namespace LendFoundry.Loans
{
    public interface ILoanInfo
    {
        ILoan Loan { get; }
        IPaymentScheduleSummary Summary { get; }
    }
}
