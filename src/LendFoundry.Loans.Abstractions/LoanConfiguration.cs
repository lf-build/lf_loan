﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Loans
{
    public class LoanConfiguration
    {        
        public List<LoanStatus> LoanStatus { get; set; }     
           
        public PaymentScheduleConfiguration Schedule { get; set; }

        [Obsolete("Moved to payment schedule configuration")]
        public int PayoffPeriod { get; set; }
    }
}
