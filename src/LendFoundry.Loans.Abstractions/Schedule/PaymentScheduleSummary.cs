﻿using System;

namespace LendFoundry.Loans.Schedule
{
    public class PaymentScheduleSummary : IPaymentScheduleSummary
    {
        public DateTimeOffset? LastPaymentDate { get; set; }
        public DateTimeOffset? NextDueDate { get; set; }
        public double? LastPaymentAmount { get; set; }
        public double RemainingBalance { get; set; }
        public double CurrentDue { get; set; }
        public int DaysPastDue { get; set; }
        public double? NextInstallmentAmount { get; set; }
        public bool IsInGracePeriod { get; set; }
    }
}
