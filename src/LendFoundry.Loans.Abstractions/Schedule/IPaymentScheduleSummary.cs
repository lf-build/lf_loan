using System;

namespace LendFoundry.Loans.Schedule
{
    public interface IPaymentScheduleSummary
    {
        DateTimeOffset? LastPaymentDate { get; }
        DateTimeOffset? NextDueDate { get; }
        double? LastPaymentAmount { get; }
        double RemainingBalance { get; }
        double CurrentDue { get; }
        int DaysPastDue { get; }
        double? NextInstallmentAmount { get; }
        bool IsInGracePeriod { get; set; }
    }
}
