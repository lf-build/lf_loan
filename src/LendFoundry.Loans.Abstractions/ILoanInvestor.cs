﻿using System;

namespace LendFoundry.Loans
{
    public interface ILoanInvestor
    {
        string Id { get; set; }
        DateTimeOffset LoanPurchaseDate { get; set; }
    }
}
