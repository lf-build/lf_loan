﻿using System;

namespace LendFoundry.Loans.Service
{
    public class Command
    {
        public Command(Action execute, Action rollback)
        {
            DoExecute = execute;
            DoRollback = rollback;
        }

        private bool Executed { get; set; }
        private Action DoExecute { get; }
        private Action DoRollback { get; }

        public void Execute()
        {
            DoExecute();
            Executed = true;
        }

        public void Rollback()
        {
            if (Executed)
                DoRollback();
        }
    }
}
