﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Events;
using LendFoundry.Loans.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Service
{
    public class BankAccountEngine : IBankAccountEngine
    {
        public BankAccountEngine
        (
            ILoanRepository loanRepository,
            IEventHubClient eventHubClient,
            ITenantTime tenantTime,
            ILogger logger
        )
        {
            EventHubClient = eventHubClient;
            Logger = logger;
            LoanRepository = loanRepository;
            TenantTime = tenantTime;
        }

        private IEventHubClient EventHubClient { get; }

        private ILogger Logger { get; }

        private ILoanRepository LoanRepository { get; }

        private ITenantTime TenantTime { get; }

        public void Create(string loanReferenceNumber, IBankAccount bankAccount)
        {
            if (string.IsNullOrWhiteSpace(loanReferenceNumber))
                throw new InvalidArgumentException($"{nameof(loanReferenceNumber)} cannot be null");

            // Validate bank account information
            ValidateBankAccount(bankAccount);
            Logger.Info("Received BankAccount information validated properly");

            // Gets loan information
            var loan = GetLoan(loanReferenceNumber);

            var loanBankAccounts = GetBankAccounts(loan);

            Logger.Info($"Loan #{loanReferenceNumber} already has #{loanBankAccounts.Count()} account(s)");

            AssertBankAccountIsNotDuplicate(bankAccount, loan);

            IBankAccount currentPrimaryAccount = null;
            bankAccount.EffectiveDate = TenantTime.Now;

            if (!loanBankAccounts.Any())
            {
                // When does not have bank account
                // If loan has no bank account set the new one as [Primary]
                bankAccount.IsPrimary = true;
                loanBankAccounts.Add(bankAccount);
                Logger.Info($"As loan #{loanReferenceNumber} has no accounts, BankAccount #{bankAccount.AccountNumber} was set as primary");
            }
            else if (loanBankAccounts.Any() && bankAccount.IsPrimary)
            {
                // When already have a bank account and the new one is primary
                // Gets the current primary account
                currentPrimaryAccount = loanBankAccounts.Single(a => a.IsPrimary);

                // If new account is [Primary] the current primary account should have [IsPrimary=false]
                loanBankAccounts = loanBankAccounts.Select(s =>
                {
                    s.IsPrimary = false;
                    return s;
                }).ToList();
                loanBankAccounts.Add(bankAccount);
                Logger.Info($"Actual primary BankAccount #{currentPrimaryAccount.AccountNumber} found, new BankAccount #{bankAccount.AccountNumber} set as primary");
            }
            else if (loanBankAccounts.Any() && !bankAccount.IsPrimary)
            {
                // When already have bank account and the new one is not primary
                // If new account is not [Primary] so only add it as a new account option
                loanBankAccounts.Add(bankAccount);
                Logger.Info($"New BankAccount #{bankAccount.AccountNumber} added to account list");
            }

            // Loan can be only one primary account always
            if (loanBankAccounts.Count(a => a.IsPrimary) != 1)
            {
                Logger.Error($"Loan #{loanReferenceNumber} has more then one primary BankAccount");
                throw new Exception();
            }

            // Update loan accounts
            LoanRepository.UpdateLoanBankAccounts(loanReferenceNumber, loanBankAccounts);

            // When a BankAccount is updated an event must to be published to update the payments
            if (bankAccount.IsPrimary)
            {
                EventHubClient.Publish(new LoanPrimaryBankAccountChanged(loanReferenceNumber, currentPrimaryAccount, bankAccount));
                Logger.Info($"Event #{nameof(LoanPrimaryBankAccountChanged)} rised");
            }

            EventHubClient.Publish(new LoanBankAccountAdded(loanReferenceNumber, bankAccount));
            Logger.Info($"Event #{nameof(LoanBankAccountAdded)} rised");
        }

        private static IList<IBankAccount> GetBankAccounts(ILoan loan)
        {
            return loan.BankAccounts != null ? loan.BankAccounts.ToList() : new List<IBankAccount>();
        }

        private static void AssertBankAccountIsNotDuplicate(IBankAccount bankAccount, ILoan loan)
        {
            if (GetBankAccounts(loan).Any(a => a.AccountNumber == bankAccount.AccountNumber && a.RoutingNumber == bankAccount.RoutingNumber))
                throw new InvalidArgumentException($"AccountNumber #{bankAccount.AccountNumber} for Loan #{loan.ReferenceNumber} already exists");
        }

        public void SetAsPrimary(string loanReferenceNumber, string accountNumber)
        {
            if (string.IsNullOrWhiteSpace(loanReferenceNumber))
                throw new InvalidArgumentException($"{nameof(loanReferenceNumber)} cannot be null");

            if (string.IsNullOrWhiteSpace(accountNumber))
                throw new InvalidArgumentException($"{nameof(accountNumber)} cannot be null");

            // Get all bank account from loan
            // Add or Update BankAccount information must be performed only for already created loan
            var loan = GetLoan(loanReferenceNumber);

            if (loan.BankAccounts != null)
            {
                Logger.Info($"Loan #{loanReferenceNumber} already has #{loan.BankAccounts.Count()} account(s)");

                // Gets the current primary account
                var currentPrimaryAccount = loan.BankAccounts.FirstOrDefault(a => a.IsPrimary == true);

                // When informed account is the already ​*primary*​ account, should return a badrequest​ with messaging which 
                // says "Account already set as Primary"
                if (currentPrimaryAccount.AccountNumber == accountNumber)
                    throw new InvalidArgumentException($"Account #{accountNumber} already set as Primary");

                // Find an account with informed accountNumber              
                var filteredAccount = loan.BankAccounts.FirstOrDefault(a => a.AccountNumber == accountNumber);
                if (filteredAccount == null)
                    throw new InvalidArgumentException($"AccountNumber #{accountNumber} for Loan #{loanReferenceNumber} cannot be found");

                // Get the matched account and set to IsPrimary
                // Set all the rest accounts to [IsPrimary=false]            
                loan.BankAccounts = loan.BankAccounts.ToList().Select(s =>
                {
                    s.IsPrimary = s.AccountNumber == filteredAccount.AccountNumber;
                    return s;
                }).ToList();

                Logger.Info($"Actual primary BankAccount #{currentPrimaryAccount.AccountNumber} found, new BankAccount #{accountNumber} set as primary");

                // Loan can be only one primary account always
                if (loan.BankAccounts.Count(a => a.IsPrimary) != 1)
                {
                    Logger.Error($"Loan #{loanReferenceNumber} has more then one primary BankAccount");
                    throw new Exception();
                }

                // Update loan accounts
                LoanRepository.UpdateLoanBankAccounts(loanReferenceNumber, loan.BankAccounts);

                // When a BankAccount is updated an event must to be published to update the payments
                EventHubClient.Publish(new LoanPrimaryBankAccountChanged(loanReferenceNumber, currentPrimaryAccount, filteredAccount));
                Logger.Info($"Event #{nameof(LoanPrimaryBankAccountChanged)} rised");
            }
            else
                throw new NotFoundException($"Loan #{loanReferenceNumber} has no BankAccount configured");
        }

        private ILoan GetLoan(string loanReferenceNumber)
        {
            var loan = LoanRepository.Get(loanReferenceNumber);
            if (loan == null)
                throw new LoanNotFoundException(loanReferenceNumber);

            return loan;
        }

        private static void ValidateBankAccount(IBankAccount bankAccount)
        {
            if (bankAccount == null)
                throw new InvalidArgumentException("BankAccount cannot be null");

            if (string.IsNullOrWhiteSpace(bankAccount.RoutingNumber))
                throw new InvalidArgumentException("RoutingNumber is mandatory");

            if (string.IsNullOrWhiteSpace(bankAccount.AccountNumber))
                throw new InvalidArgumentException("AccountNumber is mandatory");

            if (bankAccount.AccountType == BankAccountType.Undefined)
                throw new InvalidArgumentException("AccountType is mandatory");
        }
    }
}
