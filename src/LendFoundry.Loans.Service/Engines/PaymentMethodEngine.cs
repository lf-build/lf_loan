﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Events;
using LendFoundry.Loans.Repository;
using LendFoundry.Loans.Schedule;
using System;
using System.Linq;

namespace LendFoundry.Loans.Service
{
    public class PaymentMethodEngine : IPaymentMethodEngine
    {
        public PaymentMethodEngine
        (
            ILoanRepository loanRepository,
            IEventHubClient eventHubClient,
            ILogger logger,
            IPaymentScheduleService scheduleService,
            ITenantTime tenantTime
        )
        {
            LoanRepository = loanRepository;
            EventHubClient = eventHubClient;
            Logger = logger;
            ScheduleService = scheduleService;
            TenantTime = tenantTime;
        }

        private ITenantTime TenantTime { get; }

        private IEventHubClient EventHubClient { get; }

        private ILoanRepository LoanRepository { get; }

        private ILogger Logger { get; }

        private IPaymentScheduleService ScheduleService { get; }

        private ILoan Loan { get; set; }

        public void Change(string referenceNumber, PaymentMethod newPaymentMethod, Action<string> changed)
        {
            if (string.IsNullOrWhiteSpace(referenceNumber))
                throw new InvalidArgumentException($"{nameof(referenceNumber)} cannot be null");

            if (!new[] { PaymentMethod.ACH, PaymentMethod.Check }.Any(p => p == newPaymentMethod))
                throw new InvalidArgumentException($"{newPaymentMethod} is not a valid payment method");

            // Get loan information
            Loan = GetLoan(referenceNumber);
            var actualPaymentMethod = Loan.PaymentMethod;

            if (actualPaymentMethod == newPaymentMethod)
                throw new InvalidOperationException($"{newPaymentMethod} is already the active payment method");

            // Validate payment transition, save new paymentMethod to informed
            // loan in database and rise a new eventhub message.
            var operationMessage = ValidatePaymentMethodTransition(actualPaymentMethod, newPaymentMethod);
            LoanRepository.UpdatePaymentMethod(referenceNumber, newPaymentMethod);
            EventHubClient.Publish(new PaymentMethodChanged
            {
                NewPaymentMethod = newPaymentMethod,
                OldPaymentMethod = actualPaymentMethod,
                ReferenceNumber = referenceNumber
            });
            Logger.Info($"Event {nameof(PaymentMethodChanged)} rised");
            changed?.Invoke(operationMessage);
        }

        private string ValidatePaymentMethodTransition(PaymentMethod @old, PaymentMethod @new)
        {
            Logger.Info($"Validating transition from payment method {@old} to {@new}");

            if (@old == PaymentMethod.ACH && @new == PaymentMethod.Check)
            {
                // Get loan payment schedule information
                var paymentSchedule = ScheduleService.Get(Loan.ReferenceNumber);

                // Payment method change should be cancelled in the following scenarios:
                // if there is any ExtraPayment
                if (paymentSchedule.Installments.Any(i => i.Type == InstallmentType.Additional && i.Status != InstallmentStatus.Completed))
                    throw new InvalidOperationException($"Payment method cannot be switched from ACH to Check because it has an ExtraPayment");

                // If it has Payoff scheduled
                if (paymentSchedule.Installments.Any(i => i.Type == InstallmentType.Payoff))
                    throw new InvalidOperationException($"Payment method cannot be switched from ACH to Check because it has a Payoff scheduled");

                // Within the ACH window (3 business day prior to InstallmentDate)
                if (TenantTime.Now.BusinessDaysTo(paymentSchedule.FirstScheduledInstallment.DueDate.Value) <= 3)
                    throw new InvalidOperationException($"Payment method cannot be switched from ACH to Check because it does not respect ACH Window");

                Logger.Info("Payment method transition from ACH to Check validated");
                return "Payment Method switched from ACH to Check";
            }

            if (@old == PaymentMethod.Check && @new == PaymentMethod.ACH)
            {
                var bankAccounts = Loan.BankAccounts;
                if (bankAccounts != null && bankAccounts.Any())
                {
                    var primaryAccount = bankAccounts.FirstOrDefault(a => a.IsPrimary);
                    return $"Payment Method switched from Check to ACH. Primary account and routing numbers are {primaryAccount.AccountNumber} and {primaryAccount.RoutingNumber}";
                }
                else
                    throw new InvalidOperationException("No bank account found, please create a new bank account to proceed");
            }
            return default(string);
        }

        private ILoan GetLoan(string referenceNumber)
        {
            if (string.IsNullOrWhiteSpace(referenceNumber))
                throw new InvalidArgumentException($"{nameof(referenceNumber)} cannot be null");

            var loan = LoanRepository.Get(referenceNumber);

            if (loan == null)
                throw new LoanNotFoundException(referenceNumber);

            Logger.Info($"Loan #{referenceNumber} found");
            return loan;
        }
    }
}
