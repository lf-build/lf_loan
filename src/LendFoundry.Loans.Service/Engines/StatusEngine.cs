﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Events;
using LendFoundry.Loans.Repository;
using System;
using System.Linq;
using System.Collections.Generic;

namespace LendFoundry.Loans.Service
{
    public class StatusEngine : ILoanStatusEngine
    {
        public StatusEngine
        (
            ILoanRepository loanRepository,
            IEventHubClient eventHubClient,
            ILogger logger,
            LoanConfiguration loanConfiguration
        )
        {
            LoanRepository = loanRepository;
            EventHubClient = eventHubClient;
            Logger = logger;
            LoanConfiguration = loanConfiguration;
        }

        private LoanConfiguration LoanConfiguration { get; }

        private IEventHubClient EventHubClient { get; }

        private ILogger Logger { get; }

        private ILoanRepository LoanRepository { get; }

        public LoanStatus Get(string code)
        {
            var status = LoanConfiguration.LoanStatus.FirstOrDefault(s => s.Code == code);

            if (status == null)
                throw new InvalidArgumentException($"Loan status not found: {code}");

            return status;
        }

        public LoanStatus GetStatus(string referenceNumber)
        {
            // loan level information
            var loanStatus = GetLoan(referenceNumber).Status;

            if (LoanConfiguration.LoanStatus == null)
            {
                Logger.Error($"Loan status configuration is empty");
                throw new Exception();
            }

            // configuration level information
            var configStatus = LoanConfiguration.LoanStatus.FirstOrDefault(s => s.Code == loanStatus.Code);
            if (configStatus == null)
                throw new NotFoundException($"Status code #{loanStatus.Code} not found");

            loanStatus.LabelName = configStatus.LabelName;
            loanStatus.LabelStyle = configStatus.LabelStyle;
            loanStatus.Actions = configStatus.Actions;
            loanStatus.Transitions = configStatus.Transitions;
            return loanStatus;
        }

        public void UpdateLoanStatus(string referenceNumber, string nextStatus, DateTimeOffset? startDate, DateTimeOffset? endDate)
        {
            Logger.Info($"New status #{nextStatus} requested for loan #{referenceNumber}");

            if (string.IsNullOrWhiteSpace(nextStatus))
                throw new ArgumentNullException($"{nameof(nextStatus)} cannot be null");

            // range dates validation
            if (startDate.HasValue && endDate.HasValue)
                if (startDate.Value >= endDate.Value)
                    throw new InvalidOperationException($"StartDate should not be greater than EndDate");

            // Check loan information.
            var loan = GetLoan(referenceNumber);

            // Check new status information.
            if (LoanConfiguration.LoanStatus == null)
                throw new ArgumentNullException("Loan status configuration cannot be found");

            var nextStatusInfo = LoanConfiguration.LoanStatus.FirstOrDefault(x => x.Code == nextStatus);
            if (nextStatusInfo == null)
                throw new ArgumentNullException($"Next transition, status code #{nextStatus} cannot be found");

            Logger.Info($"Next transition, status code #{nextStatusInfo.Code} name #{nextStatusInfo.Name} found");

            // Set informed date period if informed.
            if (startDate.HasValue) nextStatusInfo.StartDate = startDate.Value;
            if (endDate.HasValue) nextStatusInfo.EndDate = endDate.Value;

            ValidateTransitionMove(loan.Status.Code, nextStatusInfo.Code);
            LoanRepository.UpdateLoanStatus(referenceNumber, nextStatusInfo);
            Logger.Info($"Loan #{referenceNumber} updated in database");

            // Event when loan has changes of any nature, keep this event only
            // to avoid side effects to old modules that is using it instead of
            // new LoanStatusUpdated.
            EventHubClient.Publish(new LoanUpdated(referenceNumber));
            
            // Event when loan has status changes
            EventHubClient.Publish(new LoanStatusUpdated(
                referenceNumber: referenceNumber,
                oldStatus: loan.Status.Code,
                newStatus: nextStatusInfo.Code,
                startDate: startDate,
                endDate: endDate,
                oldStatusDescription: loan.Status.Name,
                newStatusDescription: nextStatusInfo.Name
            ));
            Logger.Info($"Event #{nameof(LoanStatusUpdated)} rised");
        }

        private void ValidateTransitionMove(string actualStatus, string nextStatus)
        {
            // Get defition of current status.
            var statusConfig = LoanConfiguration.LoanStatus.FirstOrDefault(s => s.Code == actualStatus);

            // Verify if next status is configured as a valid transition from current status.
            if (statusConfig.Transitions == null || !statusConfig.Transitions.Any(t => t == nextStatus))
                throw new InvalidOperationException($"Status #{nextStatus} is not a legal transition");

            Logger.Info($"Valid transition from #{actualStatus} to #{nextStatus}");
        }

        private ILoan GetLoan(string referenceNumber)
        {
            if (string.IsNullOrWhiteSpace(referenceNumber))
                throw new InvalidArgumentException($"{nameof(referenceNumber)} cannot be null");

            var loan = LoanRepository.Get(referenceNumber);

            if (loan == null)
                throw new LoanNotFoundException(referenceNumber);

            Logger.Info($"Loan #{referenceNumber} found");
            return loan;
        }

        public bool IsAllowedAction(string referenceNumber, string actionType)
        {
            if (string.IsNullOrWhiteSpace(referenceNumber))
                throw new ArgumentException($"{nameof(referenceNumber)} cannot be empty");

            if (string.IsNullOrWhiteSpace(actionType))
                throw new ArgumentException($"{nameof(actionType)} cannot be empty");

            var loanStatus = GetLoan(referenceNumber).Status;
            var transitionDetail = LoanConfiguration
                                    .LoanStatus
                                    .FirstOrDefault(c => c.Code == loanStatus.Code);
            
            if (transitionDetail == null || transitionDetail.Actions == null)
                return false;

            return transitionDetail
                        .Actions
                        .Any(t => t.ToString().Equals(actionType, StringComparison.OrdinalIgnoreCase));
        }
    }
}
