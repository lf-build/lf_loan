﻿using LendFoundry.Loans.Schedule;

namespace LendFoundry.Loans.Service
{
    public class LoanInfo : ILoanInfo
    {
        public ILoan Loan { get; set; }
        public IPaymentScheduleSummary Summary { get; set; }
    }
}
