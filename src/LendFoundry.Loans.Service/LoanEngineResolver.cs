﻿using LendFoundry.Foundation.Logging;
using Microsoft.Framework.DependencyInjection;
using System;

namespace LendFoundry.Loans.Service
{
    public class LoanEngineResolver : ILoanEngineResolver
    {
        public LoanEngineResolver(IServiceProvider provider, ILogger logger)
        {
            Provider = provider;
            Logger = logger;
        }

        private ILogger Logger { get; }

        private IServiceProvider Provider { get; }

        public T Resolve<T>() => Provider.GetService<T>();
    }
}
