﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Service
{
    public class OnboardingRequest : IOnboardingRequest
    {
        public string ReferenceNumber { get; set; }
        public string FundingSource { get; set; }
        public string Purpose { get; set; }
        public ILoanInvestor Investor { get; set; }
        public IEnumerable<IScore> Scores { get; set; }
        public string Grade { get; set; }
        public double? PreCloseDti { get; set; }
        public double? PostCloseDti { get; set; }
        public double MonthlyIncome { get; set; }
        public string HomeOwnership { get; set; }
        public string CampaignCode { get; set; }
        public IEnumerable<IBorrower> Borrowers { get; set; }
        public ILoanTerms Terms { get; set; }
        public IBankAccount BankInfo { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
    }
}
