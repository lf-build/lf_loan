﻿namespace LendFoundry.Loans.Service.Events
{
    public class LoanOnboarded
    {
        public LoanOnboarded(string loanReferenceNumber)
        {
            ReferenceNumber = loanReferenceNumber;
        }

        public string ReferenceNumber { get; }
    }
}
