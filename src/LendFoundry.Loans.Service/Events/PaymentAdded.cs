﻿namespace LendFoundry.Loans.Service.Events
{
    public class PaymentAdded
    {
        public string AccountNumber { get; set; }
        public double Amount { get; set; }
        public string CheckNumber { get; set; }
        public string Date { get; set; }
        public string Method { get; set; }
        public string RoutingNumber { get; set; }
    }
}
