﻿using LendFoundry.Foundation.Logging;
using System;

namespace LendFoundry.Loans.Service
{
    public class CommandExecutor
    {
        private ILogger Logger { get; }

        public CommandExecutor(ILogger logger)
        {
            Logger = logger;
        }

        public void Execute(params Command[] commands)
        {
            try
            {
                foreach (var command in commands)
                    command.Execute();
            }
            catch (Exception exception)
            {
                Logger.Error("Failed to execute a step of the onboarding process", exception);
                Rollback(commands);
                throw;
            }
        }

        private void Rollback(Command[] commands)
        {
            foreach (var command in commands)
            {
                try
                {
                    command.Rollback();
                }
                catch (Exception exception)
                {
                    Logger.Error("Failed to rollback a step of the onboarding process", exception);
                    // We don't rethrow the exception because all commands must be rolledback
                }
            }
        }
    }
}