﻿using LendFoundry.Foundation.Date;
using System;

namespace LendFoundry.Loans.Service.Summary
{
    public interface ILoanSummaryBuilder
    {
        LoanSummary Build(ILoanTerms terms, IPaymentSchedule schedule);
    }

    public class LoanSummaryBuilder : ILoanSummaryBuilder
    {
        public LoanSummaryBuilder(
            IInterestAccruedFunction interestAccruedFunction,
            INetInterestAccruedFunction netInterestAccruedFunction,
            IPaystring paystring,
            ITenantTime tenantTime
        ) {
            InterestAccruedFunction = interestAccruedFunction;
            NetInterestAccruedFunction = netInterestAccruedFunction;
            Paystring = paystring;
            TenantTime = tenantTime;
        }

        private IInterestAccruedFunction InterestAccruedFunction { get; }
        private INetInterestAccruedFunction NetInterestAccruedFunction { get; }
        private IPaystring Paystring { get; }
        private ITenantTime TenantTime { get; }

        public LoanSummary Build(ILoanTerms terms, IPaymentSchedule schedule)
        {
            var today = TenantTime.Today;
            var previousMonth = TenantTime.EndOfPreviousMonth(today);
            return new LoanSummary
            {
                Paystring = Paystring.Build(today, schedule),
                MonthlyInterestEarned = InterestAccruedFunction.Apply(today, terms, schedule, false),
                NetInterestAccrued = NetInterestAccruedFunction.Apply(today, terms, schedule, false),
                PriorMonthNetInterestAccrued = NetInterestAccruedFunction.Apply(previousMonth, terms, schedule, true)
            };
        }
    }
}
