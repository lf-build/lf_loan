﻿using LendFoundry.Loans.Schedule.Calculators;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Service.Summary
{
    public interface IPaystring
    {
        string Build(DateTimeOffset date, IPaymentSchedule schedule);
    }

    public class Paystring : IPaystring
    {
        public Paystring(IGracePeriodCalculator gracePeriodCalculator)
        {
            GracePeriodCalculator = gracePeriodCalculator;
        }

        private IGracePeriodCalculator GracePeriodCalculator { get; }

        public string Build(DateTimeOffset date, IPaymentSchedule schedule)
        {
            var installments = schedule.Installments
                .Where(i => i.AnniversaryDate <= date && i.Type == InstallmentType.Scheduled)
                .OrderBy(i => i.AnniversaryDate);

            var delinquencyCount = 0;
            var paystring = "";

            foreach (var installment in installments)
            {
                if (InGracePeriod(date, installment))
                    break;

                var periodStart = installment.AnniversaryDate.Value;
                var periodEnd = Min(periodStart.AddMonths(1).AddDays(-1), date);

                if (IsUnpaid(installment) || IsPaidLate(installment))
                    delinquencyCount = Math.Min(9, delinquencyCount + 1);

                else if (PaymentHasBeenMade(periodStart, periodEnd, installments))
                    delinquencyCount = 0;

                paystring += delinquencyCount;
            }

            if (IsLastInstallmentDelinquent(date, installments))
            {
                ForEachPeriodAfterLastInstallment(installments, date, () => paystring += delinquencyCount);
            }

            paystring += '0';

            return Format(paystring);
        }

        private bool PaymentHasBeenMade(DateTimeOffset periodStart, DateTimeOffset periodEnd, IEnumerable<IInstallment> installments)
        {
            return installments
                .Where(i => i.PaymentDate >= periodStart && i.PaymentDate <= periodEnd && i.Status == InstallmentStatus.Completed)
                .Any();
        }

        private bool InGracePeriod(DateTimeOffset date, IInstallment installment)
        {
            var graceDate = GraceDateOf(installment);
            return date >= installment.AnniversaryDate && date <= graceDate;
        }

        private bool IsPaidLate(IInstallment installment)
        {
            return installment.PaymentDate > GraceDateOf(installment);
        }

        private DateTimeOffset GraceDateOf(IInstallment installment)
        {
            return GracePeriodCalculator.GetGracePeriod(installment).End;
        }

        private bool IsUnpaid(IInstallment installment)
        {
            return installment.PaymentDate == null;
        }

        private DateTimeOffset Min(DateTimeOffset a, DateTimeOffset b)
        {
            return a < b ? a : b;
        }

        private bool HasBeenPaidAfterNextAnniversary(IInstallment installment, IEnumerable<IInstallment> installments)
        {
            var nextInstallment = installments
                .Where(i => i.AnniversaryDate > installment.AnniversaryDate)
                .OrderBy(i => i.AnniversaryDate)
                .FirstOrDefault();

            var nextAnniversary = nextInstallment?.AnniversaryDate ?? installment.DueDate.Value.AddMonths(1);
            var paymentDate = installment.PaymentDate;
            return paymentDate >= nextAnniversary;
        }

        private void ForEachPeriodAfterLastInstallment(IEnumerable<IInstallment> installments, DateTimeOffset date, Func<string> fn)
        {
            var anniversary = installments.LastOrDefault()?.AnniversaryDate.Value ?? date;
            while (date > GracePeriodCalculator.GetGracePeriod(anniversary = anniversary.AddMonths(1)).End)
                fn();
        }

        private string Format(string paystring)
        {
            var padded = paystring.PadLeft(12, '0');
            return padded.Substring(Math.Max(0, padded.Length - 12));
        }

        private bool IsLastInstallmentDelinquent(DateTimeOffset date, IEnumerable<IInstallment> installments)
        {
            var lastInstallment = installments.LastOrDefault();
            return lastInstallment != null
                && PastGracePeriod(date, lastInstallment)
                && (IsUnpaid(lastInstallment, date) || HasBeenPaidAfterNextAnniversary(lastInstallment, installments));
        }

        private bool IsUnpaid(IInstallment installment, DateTimeOffset date)
        {
            return installment.Status == InstallmentStatus.Scheduled
                || installment.PaymentDate > date;
        }

        private bool PastGracePeriod(DateTimeOffset date, IInstallment installment)
        {
            return date > GraceDateOf(installment);
        }
    }
}
