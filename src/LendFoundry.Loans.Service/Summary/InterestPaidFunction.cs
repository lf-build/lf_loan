﻿using LendFoundry.Foundation.Date;
using System;
using System.Linq;

namespace LendFoundry.Loans.Service.Summary
{
    public interface IInterestPaidFunction
    {
        double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule);
    }

    public class InterestPaidFunction : IInterestPaidFunction
    {
        public InterestPaidFunction(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
        }

        private ITenantTime TenantTime { get; }

        public double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule)
        {
            var start = TenantTime.BeginningOfMonth(referenceDate);
            var end = TenantTime.EndOfMonth(referenceDate);

            var interestPaid = schedule.Installments
                .Where(i => i.Status == InstallmentStatus.Completed)
                .Where(i => i.Type != InstallmentType.ChargeOff)
                .Where(i => i.PaymentDate >= start)
                .Where(i => i.PaymentDate <= end)
                .Sum(i => i.Interest);

            return Math.Round(interestPaid, 2, MidpointRounding.AwayFromZero);
        }
    }
}
