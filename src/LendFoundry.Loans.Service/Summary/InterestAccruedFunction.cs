﻿using System;

namespace LendFoundry.Loans.Service.Summary
{
    public interface IInterestAccruedFunction
    {
        double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule);
        double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule, bool fullPeriod);
    }

    public class InterestAccruedFunction : IInterestAccruedFunction
    {
        public InterestAccruedFunction(IAverageDailyBalanceFunction averageDailyBalanceFormula)
        {
            AverageDailyBalanceFormula = averageDailyBalanceFormula;
        }

        private IAverageDailyBalanceFunction AverageDailyBalanceFormula { get; }

        public double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule)
        {
            return Apply(referenceDate, terms, schedule, true);
        }

        public double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule, bool fullPeriod)
        {
            var adb = AverageDailyBalanceFormula.Apply(referenceDate, terms, schedule, fullPeriod);
            var rate = terms.Rate / 100;
            var monthlyRate = rate / 12;
            return Math.Round(adb * monthlyRate, 2, MidpointRounding.AwayFromZero);
        }
    }
}
