﻿using LendFoundry.Foundation.Date;
using System;

namespace LendFoundry.Loans.Service.Summary
{
    public interface INetInterestAccruedFunction
    {
        double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule);
        double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule, bool fullPeriod);
    }

    public class NetInterestAccruedFunction : INetInterestAccruedFunction
    {
        public NetInterestAccruedFunction(IInterestAccruedFunction interestAccruedFormula, IInterestPaidFunction interestPaidFormula, ITenantTime tenantTime)
        {
            InterestAccruedFormula = interestAccruedFormula;
            InterestPaidFormula = interestPaidFormula;
            TenantTime = tenantTime;
        }

        private IInterestAccruedFunction InterestAccruedFormula { get; }
        private IInterestPaidFunction InterestPaidFormula { get; }
        private ITenantTime TenantTime { get; }

        public double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule)
        {
            return Apply(referenceDate, terms, schedule, true);
        }

        public double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule, bool fullPeriod)
        {
            var interestAccrued = InterestAccruedFormula.Apply(referenceDate, terms, schedule, fullPeriod);
            var netInterestAccruedPreviousMonth = NetInterestAccruedPreviousMonth(referenceDate, terms, schedule);
            var interestPaid = InterestPaidFormula.Apply(referenceDate, terms, schedule);
            var netInterestAccrued = Math.Round(interestAccrued + netInterestAccruedPreviousMonth - interestPaid, 2, MidpointRounding.AwayFromZero);
            return Math.Max(0.0, netInterestAccrued);
        }

        private double NetInterestAccruedPreviousMonth(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule)
        {
            if (IsBeforeOriginationMonth(referenceDate, terms))
                return 0.0;

            return Apply(TenantTime.EndOfPreviousMonth(referenceDate), terms, schedule);
        }

        private bool IsBeforeOriginationMonth(DateTimeOffset date, ILoanTerms terms)
        {
            var month = TenantTime.BeginningOfMonth(date);
            var originationMonth = TenantTime.BeginningOfMonth(terms.OriginationDate);
            return month < originationMonth;
        }
    }
}
