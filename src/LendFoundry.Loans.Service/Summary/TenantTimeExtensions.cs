﻿using LendFoundry.Foundation.Date;
using System;

namespace LendFoundry.Loans.Service.Summary
{
    public static class TenantTimeExtensions
    {
        public static DateTimeOffset EndOfPreviousMonth(this ITenantTime tenantTime, DateTimeOffset date)
        {
            var previousMonth = date.AddMonths(-1);
            var year = previousMonth.Year;
            var month = previousMonth.Month;
            var lastDayOfMonth = DateTime.DaysInMonth(year, month);
            return tenantTime.Create(year, month, lastDayOfMonth);
        }

        public static Interval Period(this ITenantTime tenantTime, DateTimeOffset referenceDate)
        {
            return new Interval
            (
                start: tenantTime.BeginningOfMonth(referenceDate),
                end  : tenantTime.EndOfMonth(referenceDate)
            );
        }

        public static DateTimeOffset BeginningOfMonth(this ITenantTime tenantTime, DateTimeOffset date)
        {
            return tenantTime.Create(date.Year, date.Month, 1);
        }

        public static DateTimeOffset EndOfMonth(this ITenantTime tenantTime, DateTimeOffset date)
        {
            return tenantTime.Create(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
        }
    }
}
