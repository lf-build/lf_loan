﻿using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Service.Summary
{
    public interface IAverageDailyBalanceFunction
    {
        double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule);
        double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule, bool fullPeriod);
    }

    public class AverageDailyBalanceFunction : IAverageDailyBalanceFunction
    {
        internal const int MaxDaysInMonth = 30; //TODO: should be provided by the tenant's calendar type

        public AverageDailyBalanceFunction(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
        }

        private ITenantTime TenantTime { get; }

        public double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule)
        {
            return Apply(referenceDate, terms, schedule, true);
        }

        public double Apply(DateTimeOffset referenceDate, ILoanTerms terms, IPaymentSchedule schedule, bool fullPeriod)
        {
            var year = referenceDate.Year;
            var month = referenceDate.Month;
            var periodStart = TenantTime.Create(year, month, 1);
            var periodEnd = TenantTime.Create(year, month, DateTime.DaysInMonth(year, month));
            var lastDayOfPeriod = fullPeriod ? MaxDaysInMonth : referenceDate.Day;
            var balanceChanges = GetBalanceChanges(schedule, terms, periodStart, periodEnd).ToList();
            var intervals = GetIntervals(balanceChanges, lastDayOfPeriod);

            var adb = 0.0;
            foreach(var interval in intervals)
            {
                adb += interval.Balance * interval.Days / MaxDaysInMonth;
            }
            return Math.Round(adb, 2, MidpointRounding.AwayFromZero);
        }

        private IEnumerable<BalanceChange> GetBalanceChanges(IPaymentSchedule schedule, ILoanTerms terms, DateTimeOffset periodStart, DateTimeOffset periodEnd)
        {
            var changes = new List<BalanceChange>();

            changes.Add(new BalanceChange(periodStart, GetOpeningBalance(schedule, terms, periodStart)));

            if (terms.OriginationDate > periodStart && terms.OriginationDate <= periodEnd)
                changes.Add(new BalanceChange(terms.OriginationDate, terms.LoanAmount));

            changes.AddRange(GetInstallments(schedule, periodStart, periodEnd).Select(i => new BalanceChange(BalanceChangeDateOf(i, terms), i.EndingBalance)));

            return changes
                .OrderByDescending(c => c.Balance)
                .OrderBy(c => c.Date);
        }

        private int BalanceChangeDateOf(IInstallment installment, ILoanTerms terms)
        {
            var shouldBeAnniversaryDate =
                installment.Type == InstallmentType.Scheduled &&
                !IsEarlyPayment(installment) &&
                !IsLatePayment(installment);

            return shouldBeAnniversaryDate ?
                terms.OriginationDate.Day :
                DateOf(installment).Day;
        }

        private IEnumerable<IInstallment> GetInstallments(IPaymentSchedule schedule, DateTimeOffset periodStart, DateTimeOffset periodEnd)
        {
            var all = schedule.Installments
                .Where(i => i.Status == InstallmentStatus.Completed)
                .Where(i => DateOf(i) >= periodStart)
                .Where(i => DateOf(i) <= periodEnd);

            var byDate = all.GroupBy(DateOf);

            return byDate.Select(g => g.OrderBy(i => i.EndingBalance).First());
        }

        private static DateTimeOffset DateOf(IInstallment installment)
        {
            if (IsEarlyPayment(installment) || IsLatePayment(installment))
                return installment.PaymentDate.Value;

            return (installment.AnniversaryDate ?? installment.PaymentDate).Value;
        }

        private static bool IsEarlyPayment(IInstallment installment)
        {
            return 
                installment.PaymentDate != null &&
                installment.AnniversaryDate != null &&
                installment.PaymentDate < installment.AnniversaryDate;
        }

        private static bool IsLatePayment(IInstallment installment)
        {
            return 
                installment.PaymentDate != null &&
                installment.AnniversaryDate != null &&
                    
                //TODO: use a proper method to determine when it's a late payment

                //As of now we don't have any means to know when a payment was made late,
                //so we had to check if the payment date is past the grace period using
                //a hardcoded grace period of 10 days. This should be fixed as soon as
                //we add late payment information to the installment.
                installment.PaymentDate > installment.DueDate.Value.AddDays(10);
        }

        public double GetOpeningBalance(IPaymentSchedule schedule, ILoanTerms terms, DateTimeOffset periodStart)
        {
            var lastPayment = LastPaymentBefore(periodStart, schedule);

            if (lastPayment != null)
                return lastPayment.EndingBalance;

            if (terms.OriginationDate <= periodStart)
                return terms.LoanAmount;

            return 0.0;
        }

        private IInstallment LastPaymentBefore(DateTimeOffset date, IPaymentSchedule schedule)
        {
            return schedule.Installments
                .Where(i => DateOf(i) < date)
                .Where(i => i.Status == InstallmentStatus.Completed)
                .OrderBy(i => i.EndingBalance)
                .FirstOrDefault();
        }

        private List<BalanceInterval> GetIntervals(List<BalanceChange> balanceChanges, int lastDayOfPeriod)
        {
            var intervals = new List<BalanceInterval>();
            for(int index = 0; index < balanceChanges.Count; index++)
            {
                var current = balanceChanges[index];
                var next = index < balanceChanges.Count - 1 ? balanceChanges[index + 1] : null;
                var startDay = Math.Min(30, current.Date);
                var endDay = next != null ? Math.Min(30, next.Date) : lastDayOfPeriod + 1;
                var days = endDay - startDay;

                intervals.Add(new BalanceInterval { Balance = current.Balance, Days = days });
            }
            return intervals;
        }

        internal class BalanceInterval
        {
            public double Balance { get; set; }
            public int Days { get; set; }
        }

        internal class BalanceChange
        {
            public BalanceChange(DateTimeOffset date, double balance) : this(date.Day, balance)
            {
            }

            public BalanceChange(int date, double balance)
            {
                Date = date;
                Balance = balance;
            }

            public int Date { get; }
            public double Balance { get; }
        }
    }
}
