using LendFoundry.Loans.Service.Summary;
using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Service
{
    public interface ILoanService
    {
        void Onboard(IOnboardingRequest onboardingRequest);

        ILoan GetLoan(string loanReferenceNumber);

        ILoanInfo GetLoanInfo(string loanReferenceNumber);

        ILoanDetails GetLoanDetails(string loanReferenceNumber);

        IEnumerable<ITransaction> GetTransactions(string loanReferenceNumber);

        IPaymentSchedule GetPaymentSchedule(string loanRefereceNumber);

        //TODO: add unit tests for GetLoans(int minDaysDue)
        IEnumerable<ILoanInfo> GetLoans(int minDaysDue);

        IEnumerable<ILoanInfo> GetLoans(int minDaysDue, int maxDaysDue);

        IEnumerable<ILoanInfo> GetLoans(LoanStatus status);

        IPayoffAmount GetPayoffAmount(string loanReferenceNumber, DateTime payoffDate);

        IEnumerable<ILoanInfo> GetLoansDueIn(DateTime dueDate);

        IEnumerable<IInstallmentInfo> GetInstallmentsDueIn(DateTime dueDate, PaymentMethod paymentMethod);

        IEnumerable<IInstallmentInfo> GetInstallmentsDueIn(DateTime dueDate, PaymentMethod paymentMethod, InstallmentType type);

        IEnumerable<IInstallmentInfo> GetInstallmentsDueInByAnniversaryDate(DateTime dueDate, PaymentMethod paymentMethod, InstallmentType type);

        [Obsolete("Not being used anywhere")]
        void UpdateInstallmentStatus(string loanReferenceNumber, DateTime dueDate, InstallmentStatus newStatus);

        void AddBankAccount(string loanReferenceNumber, IBankAccount bankAccount);

        void SetBankAccountAsPrimary(string loanReferenceNumber, string accountNumber);

        LoanStatus GetLoanStatus(string referenceNumber);

        void UpdateLoanStatus(string referenceNumber, string nextStatus, DateTimeOffset? startDate, DateTimeOffset? endDate);

        void ChangePaymentMethod(string referenceNumber, PaymentMethod newPaymentMethod, Action<string> changed);

        bool IsAllowedAction(string referenceNumber, string actionType);

        LoanSummary GetSummary(string loanReferenceNumber);
    }
}
