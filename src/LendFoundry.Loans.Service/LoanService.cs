﻿using LendFoundry.Amortization.Client;
using LendFoundry.Borrowers.Repository;
using LendFoundry.Calendar.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Investors;
using LendFoundry.Loans.Repository;
using LendFoundry.Loans.Schedule;
using LendFoundry.Loans.Service.Events;
using LendFoundry.Loans.Service.Summary;
using LendFoundry.Loans.TransactionLog.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using AmortizationFee = LendFoundry.Amortization.Client.Fee;
using AmortizationFeeType = LendFoundry.Amortization.Client.FeeType;
using AmortizationLoanInfo = LendFoundry.Amortization.Client.LoanInfo;

namespace LendFoundry.Loans.Service
{
    public class LoanService : ILoanService
    {
        public LoanService
        (
            ILoanRepository loanRepository,
            IBorrowerRepository borrowerRepository,
            IPaymentScheduleService scheduleService,
            IEventHubClient eventHubClient,
            ITransactionLogService transactionLog,
            IAmortizationService amortizationService,
            ITenantTime tenantTime,
            ILogger logger,
            ICalendarService calendarService,
            IInvestorService investorService,
            ILoanEngineResolver engineResolver,
            ILoanSummaryBuilder loanSummaryBuilder,
            LoanConfiguration configuration
        )
        {
            LoanRepository = loanRepository;
            BorrowerRepository = borrowerRepository;
            ScheduleService = scheduleService;
            EventHubClient = eventHubClient;
            TransactionLog = transactionLog;
            AmortizationService = amortizationService;
            TenantTime = tenantTime;
            Calendar = calendarService;
            InvestorService = investorService;
            CommandExecutor = new CommandExecutor(logger);
            Configuration = configuration;
            Logger = logger;
            EngineResolver = engineResolver;
            LoanSummaryBuilder = loanSummaryBuilder;
        }

        private ILoanEngineResolver EngineResolver;

        private ILogger Logger { get; }

        private CommandExecutor CommandExecutor { get; }

        private ILoanRepository LoanRepository { get; }

        private IBorrowerRepository BorrowerRepository { get; }

        private IPaymentScheduleService ScheduleService { get; }

        private IEventHubClient EventHubClient { get; }

        private ITransactionLogService TransactionLog { get; }

        private IAmortizationService AmortizationService { get; }

        private ITenantTime TenantTime { get; }

        private ICalendarService Calendar { get; }

        private IInvestorService InvestorService { get; }

        private LoanConfiguration Configuration { get; }

        private ILoanSummaryBuilder LoanSummaryBuilder { get; } 

        public void Onboard(IOnboardingRequest onboardingRequest)
        {
            Validate(onboardingRequest);

            var loan = CreateLoan(onboardingRequest);

            var insertLoan = new Command
            (
                () =>
                {
                    try
                    {
                        LoanRepository.Insert(loan);
                    }
                    catch (DuplicateReferenceNumberException cause)
                    {
                        throw new InvalidArgumentException($"A loan already exists with reference number: {onboardingRequest.ReferenceNumber}", cause);
                    }
                },
                () => LoanRepository.Delete(loan.ReferenceNumber)
            );

            var storeBorrowers = new Command
            (
                () => BorrowerRepository.Store(onboardingRequest.Borrowers, loan.ReferenceNumber),
                () => BorrowerRepository.Delete(onboardingRequest.Borrowers, loan.ReferenceNumber)
            );

            var createPaymentSchedule = new Command
            (
                () => ScheduleService.Create(loan),
                () => ScheduleService.Delete(loan.ReferenceNumber)
            );

            var changeLoanStatusToOnboarded = new Command
            (
                () => LoanRepository.UpdateLoanStatus(loan.ReferenceNumber, LoanStatus.Onboarded),
                () => { /* nothing to rollback because the loan should be deleted in case of failure */ }
            );

            CommandExecutor.Execute(
                insertLoan,
                storeBorrowers,
                createPaymentSchedule,
                changeLoanStatusToOnboarded
            );

            EventHubClient.Publish(nameof(LoanOnboarded), new LoanOnboarded(onboardingRequest.ReferenceNumber));
            Logger.Info($"The {nameof(LoanOnboarded)} event was published");
        }

        public ILoan GetLoan(string loanReferenceNumber)
        {
            var loan = LoanRepository.Get(loanReferenceNumber);
            if (loan == null)
                throw new LoanNotFoundException(loanReferenceNumber);

            return WithBorrowers(loan);
        }

        public IEnumerable<ILoanInfo> GetLoans(LoanStatus status)
        {
            if (status == null) throw new InvalidArgumentException($"{nameof(status)} cannot be null");
            if (string.IsNullOrWhiteSpace(status.Code)) throw new InvalidArgumentException($"{nameof(status)}.Code cannot be null");

            var loans = WithBorrowers(LoanRepository.Get(status));
            return loans.Select(GetLoanInfo);
        }

        public IEnumerable<ILoanInfo> GetLoans(int minDaysDue)
        {
            if (minDaysDue < 0) throw new InvalidArgumentException($"{nameof(minDaysDue)} cannot be negative");

            var loanReferenceNumbers = ScheduleService.GetLoans(minDaysDue);
            var loans = GetLoans(loanReferenceNumbers);
            return loans.Select(GetLoanInfo);
        }

        public IEnumerable<ILoanInfo> GetLoans(int minDaysDue, int maxDaysDue)
        {
            if (minDaysDue < 0) throw new InvalidArgumentException($"{nameof(minDaysDue)} cannot be negative");
            if (minDaysDue > maxDaysDue) throw new InvalidArgumentException($"{nameof(minDaysDue)} cannot be greater than {nameof(maxDaysDue)}");

            var loanReferenceNumbers = ScheduleService.GetLoans(minDaysDue, maxDaysDue);
            var loans = GetLoans(loanReferenceNumbers);
            return loans.Select(GetLoanInfo);
        }

        public IEnumerable<ILoanInfo> GetLoansDueIn(DateTime dueDate)
        {
            var date = TenantTime.FromDate(dueDate);
            var referenceNumbers = ScheduleService.GetLoansDueIn(date);
            var loans = GetLoans(referenceNumbers);
            return loans.Select(GetLoanInfo);
        }

        //TODO: add unit tests for this method
        public IEnumerable<IInstallmentInfo> GetInstallmentsDueIn(DateTime dueDate, PaymentMethod paymentMethod)
        {
            var date = TenantTime.FromDate(dueDate);
            var schedules = ScheduleService.GetInstallmentsDueIn(date, paymentMethod);
            return ToInstallmentInfo(date, schedules);
        }

        public IEnumerable<IInstallmentInfo> GetInstallmentsDueIn(DateTime dueDate, PaymentMethod paymentMethod, InstallmentType type)
        {
            var date = TenantTime.FromDate(dueDate);
            var schedules = ScheduleService.GetInstallmentsDueIn(date, paymentMethod, type);
            return ToInstallmentInfo(date, schedules);
        }

        public IEnumerable<IInstallmentInfo> GetInstallmentsDueInByAnniversaryDate(DateTime dueDate, PaymentMethod paymentMethod, InstallmentType type)
        {
            var date = TenantTime.FromDate(dueDate);
            var schedules = ScheduleService.GetInstallmentsDueInByAnniversaryDate(date, paymentMethod, type);
            return ToInstallmentInfo(date, schedules, true);
        }

        private IEnumerable<IInstallmentInfo> ToInstallmentInfo(DateTimeOffset date, IEnumerable<IPaymentSchedule> schedules, bool? useAnniversaryDate = null)
        {
            var loanReferenceNumbers = schedules.Select(schedule => schedule.LoanReferenceNumber);
            var loans = WithBorrowers(LoanRepository.Get(loanReferenceNumbers));

            if (useAnniversaryDate.HasValue && useAnniversaryDate.Value)
            {
                return schedules.Select(schedule =>
                {
                    var installment = schedule.Installments.Where(i =>i.AnniversaryDate.HasValue)
                        .SingleOrDefault(candidate => candidate.Status == InstallmentStatus.Scheduled && candidate.AnniversaryDate == date);

                    if (installment == null)
                    {
                        installment = schedule.Installments
                            .Single(candidate => candidate.Status == InstallmentStatus.Scheduled && candidate.DueDate == date);
                    }

                    var loan = loans.Single(candidate => candidate.ReferenceNumber == schedule.LoanReferenceNumber);
                    return new InstallmentInfo
                    {
                        Loan = loan,
                        Installment = installment
                    };
                });
            }

            return schedules.Select(schedule =>
            {
                var installment = schedule.Installments
                    .Single(candidate => candidate.Status == InstallmentStatus.Scheduled && candidate.DueDate == date);

                var loan = loans.Single(candidate => candidate.ReferenceNumber == schedule.LoanReferenceNumber);

                return new InstallmentInfo
                {
                    Loan = loan,
                    Installment = installment
                };
            });
        }

        public void UpdateInstallmentStatus(string loanReferenceNumber, DateTime dueDate, InstallmentStatus newStatus)
        {
            ScheduleService.UpdateInstallmentStatus(loanReferenceNumber, TenantTime.FromDate(dueDate), newStatus);
        }

        private IEnumerable<ILoan> GetLoans(IEnumerable<string> loanReferenceNumbers)
        {
            var loans = LoanRepository.Get(loanReferenceNumbers);
            return WithBorrowers(loans);
        }

        private ILoanInfo GetLoanInfo(ILoan loan)
        {
            return new LoanInfo
            {
                Loan = loan,
                Summary = ScheduleService.GetSummary(loan.ReferenceNumber)
            };
        }

        public ILoanInfo GetLoanInfo(string loanReferenceNumber)
        {
            return GetLoanInfo(GetLoan(loanReferenceNumber));
        }

        public ILoanDetails GetLoanDetails(string loanReferenceNumber)
        {
            return new LoanDetails
            {
                LoanInfo = GetLoanInfo(loanReferenceNumber),
                Transactions = GetTransactions(loanReferenceNumber),
                PaymentSchedule = GetPaymentSchedule(loanReferenceNumber)
            };
        }

        public IEnumerable<ITransaction> GetTransactions(string loanReferenceNumber)
        {
            var transactions = TransactionLog.GetTransactions(loanReferenceNumber);

            return transactions.Select(info => new Transaction
            {
                LoanReferenceNumber = info.LoanReference,
                Id = info.Id,
                Code = info.Code,
                Description = info.Description,
                Date = TenantTime.Convert(info.Date),
                Amount = info.Amount,
                Notes = info.Notes,
                PaymentId = info.PaymentId,
                Timestamp = TenantTime.Convert(info.Timestamp)
            });
        }

        public IPaymentSchedule GetPaymentSchedule(string loanRefereceNumber)
        {
            return ScheduleService.Get(loanRefereceNumber);
        }

        public IPayoffAmount GetPayoffAmount(string loanReferenceNumber, DateTime payoffDate)
        {
            if (payoffDate == null)
                throw new InvalidArgumentException($"{nameof(payoffDate)} is invalid");

            return GetPayoffAmount(loanReferenceNumber, TenantTime.FromDate(payoffDate));
        }

        private IPayoffAmount GetPayoffAmount(string loanReferenceNumber, DateTimeOffset payoffDate)
        {
            return ScheduleService.GetPayoffAmount(loanReferenceNumber, payoffDate);
        }

        private ILoan CreateLoan(IOnboardingRequest onboardingRequest)
        {
            var loanInfo = new AmortizationLoanInfo
            {
                NoteDate = onboardingRequest.Terms.OriginationDate.Date,
                Amount = onboardingRequest.Terms.LoanAmount,
                Term = onboardingRequest.Terms.Term,
                Rate = onboardingRequest.Terms.Rate,
                Fees = onboardingRequest.Terms.Fees.Select(fee => new AmortizationFee
                {
                    Amount = fee.Amount,
                    Type = fee.Type == FeeType.Fixed ? AmortizationFeeType.Fixed : AmortizationFeeType.Percent
                }).ToList()
            };

            var apr = AmortizationService.GetAPR(loanInfo);
            var paymentAmount = AmortizationService.GetPaymentAmount(loanInfo);

            var bankAccount = onboardingRequest.BankInfo;
            if (bankAccount != null)
            {
                bankAccount.IsPrimary = true;
                bankAccount.EffectiveDate = TenantTime.Now;
            }

            return new Loan
            {
                Status = LoanStatus.Onboarding,
                ReferenceNumber = onboardingRequest.ReferenceNumber,
                FundingSource = onboardingRequest.FundingSource,
                Purpose = onboardingRequest.Purpose,
                Terms = new LoanTerms
                {
                    OriginationDate = TenantTime.FromDate(onboardingRequest.Terms.OriginationDate.Date),
                    FundedDate = TenantTime.FromDate(onboardingRequest.Terms.FundedDate.Date),
                    ApplicationDate = TenantTime.FromDate(onboardingRequest.Terms.ApplicationDate.Date),
                    LoanAmount = onboardingRequest.Terms.LoanAmount,
                    Term = onboardingRequest.Terms.Term,
                    Rate = onboardingRequest.Terms.Rate,
                    RateType = onboardingRequest.Terms.RateType,
                    Fees = onboardingRequest.Terms.Fees,
                    APR = apr,
                    PaymentAmount = paymentAmount
                },
                PaymentMethod = onboardingRequest.PaymentMethod,
                BankAccounts = bankAccount != null ? new[] { bankAccount } : null,
                Borrowers = onboardingRequest.Borrowers,
                CampaignCode = onboardingRequest.CampaignCode,
                Grade = onboardingRequest.Grade,
                HomeOwnership = onboardingRequest.HomeOwnership,
                Investor = new LoanInvestor
                {
                    Id = onboardingRequest.Investor.Id,
                    LoanPurchaseDate = TenantTime.FromDate(onboardingRequest.Investor.LoanPurchaseDate.Date)
                },
                MonthlyIncome = onboardingRequest.MonthlyIncome,
                PostCloseDti = onboardingRequest.PostCloseDti,
                PreCloseDti = onboardingRequest.PreCloseDti,
                Scores = onboardingRequest.Scores.Select(score => new Score
                {
                    Name = score.Name,
                    InitialScore = score.InitialScore,
                    InitialDate = TenantTime.FromDate(score.InitialDate.Date),
                    UpdatedScore = score.UpdatedScore,
                    UpdatedDate = score.UpdatedDate == null ? (DateTimeOffset?)null : TenantTime.FromDate(score.UpdatedDate.Value.Date)
                })
            };
        }

        private IEnumerable<ILoan> WithBorrowers(IEnumerable<ILoan> loans)
        {
            foreach (var loan in loans)
                loan.Borrowers = BorrowerRepository.All(loan.ReferenceNumber);
            return loans;
        }

        private ILoan WithBorrowers(ILoan loan)
        {
            loan.Borrowers = BorrowerRepository.All(loan.ReferenceNumber);
            return loan;
        }

        private void Validate(IOnboardingRequest onboardingRequest)
        {
            if (onboardingRequest == null)
                throw new InvalidArgumentException($"Argument cannot be null: {nameof(onboardingRequest)}");

            if (string.IsNullOrWhiteSpace(onboardingRequest.ReferenceNumber))
                throw new InvalidArgumentException("Loan reference number is mandatory");

            if (string.IsNullOrWhiteSpace(onboardingRequest.FundingSource))
                throw new InvalidArgumentException("Funding source is mandatory");

            Validate(onboardingRequest.Investor);

            if (string.IsNullOrWhiteSpace(onboardingRequest.Purpose))
                throw new InvalidArgumentException("Loan purpose is mandatory");

            if (onboardingRequest.Scores == null || !onboardingRequest.Scores.Any())
                throw new InvalidArgumentException("At least one score is required");

            onboardingRequest.Scores.ToList().ForEach(Validate);

            if (string.IsNullOrWhiteSpace(onboardingRequest.Grade))
                throw new InvalidArgumentException("Grade is mandatory");

            if (onboardingRequest.MonthlyIncome <= 0.0)
                throw new InvalidArgumentException("Monthly income is mandatory");

            if (string.IsNullOrWhiteSpace(onboardingRequest.HomeOwnership))
                throw new InvalidArgumentException("Home ownership is mandatory");

            if (onboardingRequest.Terms.ApplicationDate.Ticks == 0)
                throw new InvalidArgumentException("Application date is invalid or missing");

            if (onboardingRequest.Terms.OriginationDate.Ticks == 0)
                throw new InvalidArgumentException("Origination date is invalid or missing");

            if (onboardingRequest.Terms.FundedDate.Ticks == 0)
                throw new InvalidArgumentException("Funded date is invalid or missing");

            if (onboardingRequest.Terms.LoanAmount <= 0.0)
                throw new InvalidArgumentException("Loan amount must be greater than zero");

            if (onboardingRequest.Terms.Term <= 0)
                throw new InvalidArgumentException("Loan term must be greater than zero");

            if (onboardingRequest.Terms.Rate <= 0.0 || onboardingRequest.Terms.Rate > 100.0)
                throw new InvalidArgumentException("Loan rate must be in the interval (0, 100]");

            if (onboardingRequest.Terms.RateType == RateType.Undefined)
                throw new InvalidArgumentException("Rate type is mandatory");

            if (onboardingRequest.Terms.Fees == null)
                throw new InvalidArgumentException("Fee list cannot be null");

            if (onboardingRequest.PaymentMethod == PaymentMethod.Undefined)
                throw new InvalidArgumentException("Payment method is mandatory");

            if (onboardingRequest.PaymentMethod == PaymentMethod.ACH)
                Validate(onboardingRequest.BankInfo, $"when payment method is {PaymentMethod.ACH}");
            else if (onboardingRequest.BankInfo != null)
                Validate(onboardingRequest.BankInfo);

            Validate(onboardingRequest.Borrowers);
        }

        private void Validate(ILoanInvestor investor)
        {
            if (investor == null)
                throw new InvalidArgumentException("Investor is mandatory");

            if (string.IsNullOrWhiteSpace(investor.Id))
                throw new InvalidArgumentException("Investor's id is mandatory");

            if (investor.LoanPurchaseDate.Ticks == 0)
                throw new InvalidArgumentException("Investor's loan purchase date is mandatory");

            if (!InvestorService.Exists(investor.Id))
                throw new InvalidArgumentException($"Investor not found with id {investor.Id}");
        }

        private void Validate(IScore score)
        {
            if (score == null)
                throw new InvalidArgumentException("Score cannot be null");

            if (string.IsNullOrWhiteSpace(score.Name))
                throw new InvalidArgumentException("Score name is mandatory");

            if (string.IsNullOrWhiteSpace(score.InitialScore))
                throw new InvalidArgumentException("Initial score is mandatory");

            if (score.InitialDate.Ticks == 0)
                throw new InvalidArgumentException("Invalid initial score date");
        }

        private static void Validate(IBankAccount bankInfo, string supplementaryMessage = null)
        {
            if (bankInfo == null)
                throw new InvalidArgumentException($"Bank info is mandatory {supplementaryMessage}".Trim());

            if (string.IsNullOrWhiteSpace(bankInfo.RoutingNumber))
                throw new InvalidArgumentException($"Bank routing number is mandatory {supplementaryMessage}".Trim());

            if (string.IsNullOrWhiteSpace(bankInfo.AccountNumber))
                throw new InvalidArgumentException($"Bank account number is mandatory {supplementaryMessage}".Trim());

            if (bankInfo.AccountType == BankAccountType.Undefined)
                throw new InvalidArgumentException($"Bank account type is mandatory {supplementaryMessage}".Trim());
        }

        private static void Validate(IEnumerable<IBorrower> borrowers)
        {
            if (borrowers == null)
                throw new InvalidArgumentException("Borrower list cannot be null");

            if (borrowers.Count() == 0)
                throw new InvalidArgumentException("Borrower list cannot be empty");

            foreach (var borrower in borrowers)
                Validate(borrower);

            if (!borrowers.Any(borrower => borrower.IsPrimary))
                throw new InvalidArgumentException("At least one borrower must be primary");

            if (borrowers.Count(borrower => borrower.IsPrimary) > 1)
                throw new InvalidArgumentException("At most one borrower must be primary");
        }

        private static void Validate(IBorrower borrower)
        {
            if (borrower == null)
                throw new InvalidArgumentException("Borrower cannot be null");

            if (string.IsNullOrWhiteSpace(borrower.ReferenceNumber))
                throw new InvalidArgumentException("Borrower reference number name is mandatory");

            if (string.IsNullOrWhiteSpace(borrower.FirstName))
                throw new InvalidArgumentException("Borrower first name is mandatory");

            if (string.IsNullOrWhiteSpace(borrower.LastName))
                throw new InvalidArgumentException("Borrower last name is mandatory");

            if (string.IsNullOrWhiteSpace(borrower.SSN))
                throw new InvalidArgumentException("Borrower SSN is mandatory");

            if (string.IsNullOrWhiteSpace(borrower.Email))
                throw new InvalidArgumentException("Borrower email is mandatory");

            Validate(borrower.Addresses);
            Validate(borrower.Phones);
        }

        private static void Validate(IEnumerable<Address> addresses)
        {
            if (addresses == null)
                throw new InvalidArgumentException("Borrower address list cannot be null");

            if (!addresses.Any())
                throw new InvalidArgumentException("Borrower address list cannot be empty");

            foreach (var address in addresses)
                Validate(address);

            if (!addresses.Any(address => address.IsPrimary))
                throw new InvalidArgumentException("Borrower must have at least one primary address");

            if (addresses.Count(address => address.IsPrimary) > 1)
                throw new InvalidArgumentException("Borrower cannot have more than two primary addresses");
        }

        private static void Validate(Address address)
        {
            if (address == null)
                throw new InvalidArgumentException("Borrower address cannot be null");

            if (string.IsNullOrWhiteSpace(address.Line1))
                throw new InvalidArgumentException("Invalid borrower address: line 1 is mandatory");

            if (string.IsNullOrWhiteSpace(address.City))
                throw new InvalidArgumentException("Invalid borrower address: city is mandatory");

            if (string.IsNullOrWhiteSpace(address.State))
                throw new InvalidArgumentException("Invalid borrower address: state is mandatory");

            if (string.IsNullOrWhiteSpace(address.ZipCode))
                throw new InvalidArgumentException("Invalid borrower address: zip code is mandatory");
        }

        private static void Validate(IEnumerable<Phone> phones)
        {
            if (phones == null)
                throw new InvalidArgumentException("Borrower phone list cannot be null");

            if (!phones.Any())
                throw new InvalidArgumentException("Borrower phone list cannot be empty");

            foreach (var phone in phones)
                Validate(phone);

            if (!phones.Any(phone => phone.IsPrimary))
                throw new InvalidArgumentException("Borrower must have at least one primary phone");

            if (phones.Count(phone => phone.IsPrimary) > 1)
                throw new InvalidArgumentException("Borrower must have at most one primary phone");
        }

        private static void Validate(Phone phone)
        {
            if (phone == null)
                throw new InvalidArgumentException("Borrower phone cannot be null");

            if (string.IsNullOrWhiteSpace(phone.Number))
                throw new InvalidArgumentException("Borrower phone number is mandatory");
        }

        public void AddBankAccount(string loanReferenceNumber, IBankAccount bankAccount)
        {
            EngineResolver.Resolve<IBankAccountEngine>()
                          .Create(loanReferenceNumber, bankAccount);
        }

        public void SetBankAccountAsPrimary(string loanReferenceNumber, string accountNumber)
        {
            EngineResolver.Resolve<IBankAccountEngine>()
                          .SetAsPrimary(loanReferenceNumber, accountNumber);
        }

        public LoanStatus GetLoanStatus(string referenceNumber)
        {
            return EngineResolver.Resolve<ILoanStatusEngine>()
                                 .GetStatus(referenceNumber);
        }

        public void UpdateLoanStatus(string referenceNumber, string nextStatus, DateTimeOffset? startDate, DateTimeOffset? endDate)
        {
            EngineResolver.Resolve<ILoanStatusEngine>()
                          .UpdateLoanStatus(referenceNumber, nextStatus, startDate, endDate);
        }

        public void ChangePaymentMethod(string referenceNumber, PaymentMethod newPaymentMethod, Action<string> changed)
        {
            EngineResolver.Resolve<IPaymentMethodEngine>()
                          .Change(referenceNumber, newPaymentMethod, changed);
        }

        public bool IsAllowedAction(string referenceNumber, string actionType)
        {
            return EngineResolver.Resolve<ILoanStatusEngine>()
                    .IsAllowedAction(referenceNumber, actionType);
        }

        public LoanSummary GetSummary(string loanReferenceNumber)
        {
            var loan = GetLoan(loanReferenceNumber);
            var schedule = GetPaymentSchedule(loanReferenceNumber);
            return LoanSummaryBuilder.Build(loan.Terms, schedule);
        }
    }
}