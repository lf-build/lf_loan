﻿using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Loans.TransactionLog.Client
{
    public static class TransactionLogServiceExtension
    {
        public static IServiceCollection AddTransactionLog(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<ITransactionLogServiceFactory>(p => new TransactionLogServiceFactory(p, endpoint, port));
            services.AddTransient(p=>p.GetService<ITransactionLogServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
