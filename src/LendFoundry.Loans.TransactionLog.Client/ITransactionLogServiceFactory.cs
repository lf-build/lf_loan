﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.TransactionLog.Client
{
    public interface ITransactionLogServiceFactory
    {
        ITransactionLogService Create(ITokenReader reader);
    }
}