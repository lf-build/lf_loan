﻿using System.Collections.Generic;

namespace LendFoundry.Loans.TransactionLog.Client
{
    public interface ITransactionLogService
    {
        TransactionInfo AddTransaction(TransactionInfo transaction);
        List<TransactionInfo> GetTransactions(string loanId);
        TransactionInfo GetTransactionByLoanAndTransaction(string loanReferenceNumber, string transactionId);
    }
}
