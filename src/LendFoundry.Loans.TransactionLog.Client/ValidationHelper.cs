﻿using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.TransactionLog.Client
{
    internal static class ValidationHelper
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable == null)
            {
                return true;
            }
            var collection = enumerable as ICollection<T>;
            if (collection != null)
            {
                return collection.Count < 1;
            }
            return !enumerable.Any();
        }

        public static bool IsNegativeOrZero(this int value)
        {
            return value <= 0;
        }
    }
}
