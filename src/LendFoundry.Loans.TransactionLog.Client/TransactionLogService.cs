﻿using LendFoundry.Foundation.Services;
using RestSharp;
using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.TransactionLog.Client
{
    public class TransactionLogService : ITransactionLogService
    {
        public TransactionLogService(IServiceClient client)
        {
            if (client == null)
                throw new ArgumentException($"Missing argument {nameof(client)}");

            Client = client;
        }

        private IServiceClient Client { get; }

        public TransactionInfo AddTransaction(TransactionInfo transaction)
        {
            if (transaction == null)
                throw new ArgumentException($"Missing argument {nameof(transaction)}");

            var request = new RestRequest(Method.POST)
            {
                RequestFormat = DataFormat.Json
            };
            request.AddBody(transaction);
            return Client.Execute<TransactionInfo>(request);
        }

        public TransactionInfo GetTransactionByLoanAndTransaction(string loanReferenceNumber, string transactionId)
        {
            if (loanReferenceNumber.IsNullOrEmpty())
                throw new ArgumentException($"Missing argument {nameof(loanReferenceNumber)}");

            if (transactionId.IsNullOrEmpty())
                throw new ArgumentException($"Missing argument {nameof(transactionId)}");

            var request = new RestRequest("{loanReferenceNumber}/{transactionId}", Method.GET)
            {
                RequestFormat = DataFormat.Json
            };
            request.AddUrlSegment(nameof(loanReferenceNumber), loanReferenceNumber);
            request.AddUrlSegment(nameof(transactionId), transactionId);
            return Client.Execute<TransactionInfo>(request);
        }

        public List<TransactionInfo> GetTransactions(string loanReferenceNumber)
        {
            if (loanReferenceNumber.IsNullOrEmpty())
                throw new ArgumentException($"Missing argument {nameof(loanReferenceNumber)}");

            var request = new RestRequest("{loanId}", Method.GET)
            {
                RequestFormat = DataFormat.Json
            };
            request.AddUrlSegment("loanId", loanReferenceNumber);
            return Client.Execute<List<TransactionInfo>>(request);
        }
    }
}