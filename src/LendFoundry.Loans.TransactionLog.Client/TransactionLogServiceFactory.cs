using System;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.TransactionLog.Client
{
    public class TransactionLogServiceFactory : ITransactionLogServiceFactory
    {
        public TransactionLogServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public ITransactionLogService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new TransactionLogService(client);
        }
    }
}