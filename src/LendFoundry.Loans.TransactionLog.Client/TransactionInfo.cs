﻿using System;

namespace LendFoundry.Loans.TransactionLog.Client
{
    public class TransactionInfo
    {
        public string LoanReference { get; set; }

        public string Id { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public DateTimeOffset Date { get; set; }

        public double Amount { get; set; }

        public string PaymentId { get; set; }

        public string Notes { get; set; }

        public DateTimeOffset Timestamp { get; set; }
    }
}