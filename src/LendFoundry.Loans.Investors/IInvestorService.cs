﻿namespace LendFoundry.Loans.Investors
{
    public interface IInvestorService
    {
        bool Exists(string id);
    }
}
