﻿using System.Collections.Generic;

namespace LendFoundry.Loans.Investors
{
    public class InvestorConfiguration
    {
        public List<Investor> Investors { get; set; }
    }
}
