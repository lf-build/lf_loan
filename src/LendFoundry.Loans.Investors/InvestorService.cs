﻿using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System;
using System.Linq;

namespace LendFoundry.Loans.Investors
{
    public class InvestorService : IInvestorService
    {
        public InvestorService(IConfigurationServiceFactory configurationFactory, ITokenReader tokenReader, ILogger logger)
        {
            ConfigurationFactory = configurationFactory;
            TokenReader = tokenReader;
            Logger = logger;
        }

        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITokenReader TokenReader { get; }
        private ILogger Logger { get; }

        public bool Exists(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException(nameof(id));

            var configuration = ConfigurationFactory
                .Create<InvestorConfiguration>("investors", TokenReader)
                .Get();

            if (configuration == null)
                throw new Exception("Investor configuration is null");

            if (configuration.Investors == null)
                throw new Exception("The list of investors is null");

            if (!configuration.Investors.Any())
                Logger.Warn("The list of investors is empty");

            return configuration.Investors.Any(investor => investor.Id == id);
        }
    }
}
