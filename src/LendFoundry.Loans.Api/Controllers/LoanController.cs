﻿using AutoMapper;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Api.Models;
using LendFoundry.Loans.Service;
using LoanInfo = LendFoundry.Loans.Api.Models.LoanInfo;
using Microsoft.AspNet.Mvc;
using System.Collections.Generic;
using System;
using LendFoundry.EventHub.Client;
using LendFoundry.Loans.Api.Events;

namespace LendFoundry.Loans.Api.Controllers
{
    [Route("/")]
    public class LoanController : BaseController
    {
        public LoanController(ILogger logger, ILoanService loanService, IEventHubClient eventHub)
        {
            if (logger == null)
                throw new ArgumentException(nameof(logger));

            if (loanService == null)
                throw new ArgumentException(nameof(loanService));

            Logger = logger;
            LoanService = loanService;
            EventHub = eventHub;
        }

        private ILogger Logger { get; }
        private ILoanService LoanService { get; }
        private IEventHubClient EventHub { get; }

        [HttpPost("onboard")]
        public IActionResult Onboard([FromBody] List<OnboardingRequest> loans)
        {
            return Execute(() =>
            {
                if (loans == null)
                    return ErrorResult.BadRequest("At least a loan should be provided.");

                loans.ForEach(loan =>
                {
                    EventHub.Publish(new LoanOnboardingRequested(loan));
                    LoanService.Onboard(loan);
                });

                return new HttpStatusCodeResult(200);
            });
        }

        [HttpGet("{referenceNumber}")]
        public IActionResult GetLoanDetails(string referenceNumber)
        {
            return Execute(() =>
            {
                ValidateReferenceNumber(referenceNumber);

                var details = LoanService.GetLoanDetails(referenceNumber);
                var viewModel = Mapper.Map<CompleteLoanInfo>(details);

                return new HttpOkObjectResult(viewModel);
            });
        }

        [HttpGet("{referenceNumber}/info")]
        public IActionResult GetLoanInfo(string referenceNumber)
        {
            return Execute(() =>
            {
                ValidateReferenceNumber(referenceNumber);

                var info = LoanService.GetLoanInfo(referenceNumber);
                var viewModel = Mapper.Map<LoanInfo>(info);

                return new HttpOkObjectResult(viewModel);
            });
        }

        [HttpGet("{loanReferenceNumber}/transactions")]
        public IActionResult GetTransactions(string loanReferenceNumber)
        {
            return Execute(() =>
            {
                ValidateReferenceNumber(loanReferenceNumber);

                var transactions = LoanService.GetTransactions(loanReferenceNumber);
                var viewModel = Mapper.Map<List<Models.Transaction>>(transactions);
                return new HttpOkObjectResult(viewModel);
            });
        }

        [HttpGet("{loanReferenceNumber}/schedule")]
        public IActionResult GetPaymentSchedule(string loanReferenceNumber)
        {
            return Execute(() =>
            {
                ValidateReferenceNumber(loanReferenceNumber);
                var payments = LoanService.GetPaymentSchedule(loanReferenceNumber);

                if (payments == null)
                    return new HttpNotFoundResult();

                var viewModel = Mapper.Map<List<Models.Installment>>(payments.Installments);
                return new HttpOkObjectResult(viewModel);
            });
        }

        [HttpGet("{loanReferenceNumber}/summary")]
        public IActionResult GetLoanSummary(string loanReferenceNumber)
        {
            return Execute(() =>
            {
                ValidateReferenceNumber(loanReferenceNumber);

                var summary = LoanService.GetSummary(loanReferenceNumber);

                if (summary == null)
                    return new HttpNotFoundResult();

                return new HttpOkObjectResult(summary);
            });
        }

        [HttpGet("{referenceNumber}/payoff/amount/{requestDate}")]
        public IActionResult GetPayoffAmount(string referenceNumber, DateTime requestDate)
        {
            return Execute(() =>
            {
                var payoffAmount = LoanService.GetPayoffAmount(referenceNumber, requestDate);
                return new HttpOkObjectResult(new
                {
                    payoffAmount.Amount,
                    payoffAmount.ValidUntil
                });
            });
        }

        [HttpPut("{loanReferenceNumber}/installment/{dueDate}/status")]
        public IActionResult UpdateInstallmentStatus(string loanReferenceNumber, DateTime dueDate, [FromBody] InstallmentStatus newStatus)
        {
            return Execute(() =>
            {
                ValidateReferenceNumber(loanReferenceNumber);
                LoanService.UpdateInstallmentStatus(loanReferenceNumber, dueDate, newStatus);
                return new HttpStatusCodeResult(204);
            });
        }

        [HttpPut("/{referenceNumber}/bank")]
        public IActionResult AddBank(string referenceNumber, [FromBody] BankAccount bankAccount)
        {
            return Execute(() =>
            {
                ValidateReferenceNumber(referenceNumber);
                LoanService.AddBankAccount(referenceNumber, bankAccount);
                return new HttpStatusCodeResult(204);
            });
        }

        [HttpPost("/{referenceNumber}/bank/{accountNumber}/primary")]
        public IActionResult SetBankAsPrimary(string referenceNumber, string accountNumber)
        {
            return Execute(() =>
            {
                ValidateReferenceNumber(referenceNumber);
                LoanService.SetBankAccountAsPrimary(referenceNumber, accountNumber);
                return new HttpStatusCodeResult(204);
            });
        }

        [HttpGet("/{referenceNumber}/status")]
        public IActionResult GetStatus(string referenceNumber)
        {
            return Execute(() =>
            {
                ValidateReferenceNumber(referenceNumber);
                var statusInfo = LoanService.GetLoanStatus(referenceNumber);
                return new HttpOkObjectResult(new
                {
                    statusInfo.Code,
                    statusInfo.Name
                });
            });
        }

        [HttpGet("/{referenceNumber}/status/attributes")]
        public IActionResult GetStatusAttributes(string referenceNumber)
        {
            return Execute(() =>
            {
                ValidateReferenceNumber(referenceNumber);
                return new HttpOkObjectResult(LoanService.GetLoanStatus(referenceNumber));
            });
        }

        [HttpPost("/{referenceNumber}/status/{nextStatus}/{startDate?}/{endDate?}")]
        public IActionResult SetNextStatus(string referenceNumber, string nextStatus, DateTimeOffset? startDate, DateTimeOffset? endDate)
        {
            return Execute(() =>
            {
                ValidateReferenceNumber(referenceNumber);
                LoanService.UpdateLoanStatus(referenceNumber, nextStatus, startDate, endDate);
                return new HttpStatusCodeResult(204);
            });
        }

        [HttpPost("/{referenceNumber}/change/{paymentMethod}")]
        public IActionResult UpdatePaymentMethod(string referenceNumber, PaymentMethod paymentMethod)
        {
            return Execute(() =>
            {
                HttpOkObjectResult result = null;
                ValidateReferenceNumber(referenceNumber);
                LoanService.ChangePaymentMethod(referenceNumber, paymentMethod, message =>
                {
                    result = new HttpOkObjectResult(new
                    {
                        message
                    });
                });
                return result;
            });
        }

        [HttpGet("{referenceNumber}/is-allowed/{actionType}")]
        public IActionResult IsAllowedAction(string referenceNumber, string actionType)
        {
            return Execute(() =>
            {
                var isAllowed = LoanService.IsAllowedAction(referenceNumber, actionType);
                return new HttpStatusCodeResult(isAllowed ? 204 : 403);
            });
        }

        private static void ValidateReferenceNumber(string referenceNumber)
        {
            if (string.IsNullOrEmpty(referenceNumber))
                throw new InvalidArgumentException("Invalid Reference Number");
        }

        private IActionResult Execute(Func<IActionResult> expression)
        {
            try
            {
                return expression();
            }
            catch (InvalidArgumentException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (LoanNotFoundException ex)
            {
                return ErrorResult.NotFound(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return ErrorResult.NotFound(ex.Message);
            }
            catch (ClientException ex)
            {
                return new ErrorResult(ex.Error.Code, ex.Error.Message);
            }
            catch (Exception ex)
            {
                Logger.Error("Error when handling request in LoanController", ex);
                return ErrorResult.InternalServerError("We couldn't process your request");
            }
        }
    }
}