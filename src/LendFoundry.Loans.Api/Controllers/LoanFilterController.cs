﻿using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Service;
using Microsoft.AspNet.Mvc;
using System;
using System.Collections.Generic;
using AutoMapper;

namespace LendFoundry.Loans.Api.Controllers
{
    [Route("/filters")]
    public class LoanFilterController : BaseController
    {
        public LoanFilterController(ILoanService loanService)
        {
            LoanService = loanService;
        }

        private ILoanService LoanService { get; }
                  
        [HttpGet("due-in/{dueDate}")]
        public IActionResult GetLoansDueIn([FromRoute] DateTime dueDate)
        {
            try
            {
                var loans = LoanService.GetLoansDueIn(dueDate);
                var models = Mapper.Map<List<Models.LoanInfo>>(loans);
                return new HttpOkObjectResult(models);
            }
            catch (InvalidArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }
        
        [HttpGet("due-in/{dueDate}/{paymentMethod}/installments")]
        public IActionResult GetInstallmentsDueIn([FromRoute] DateTime dueDate, PaymentMethod paymentMethod)
        {
            try
            {
                var installments = LoanService.GetInstallmentsDueIn(dueDate, paymentMethod);
                var models = Mapper.Map<List<Models.InstallmentInfo>>(installments);
                return new HttpOkObjectResult(models);
            }
            catch (InvalidArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpGet("due-in/{dueDate}/{paymentMethod}/installments/{type}")]
        public IActionResult GetInstallmentsDueIn([FromRoute] DateTime dueDate, PaymentMethod paymentMethod, InstallmentType type)
        {
            try
            {
                var installments = LoanService.GetInstallmentsDueIn(dueDate, paymentMethod, type);
                var models = Mapper.Map<List<Models.InstallmentInfo>>(installments);
                return new HttpOkObjectResult(models);
            }
            catch (InvalidArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpGet("anniversaryDate/{dueDate}/{paymentMethod}/installments/{type}")]
        public IActionResult GetInstallmentsDueInByAnniversaryDate([FromRoute] DateTime dueDate, PaymentMethod paymentMethod, InstallmentType type)
        {
            try
            {
                var installments = LoanService.GetInstallmentsDueInByAnniversaryDate(dueDate, paymentMethod, type);
                var models = Mapper.Map<List<Models.InstallmentInfo>>(installments);
                return new HttpOkObjectResult(models);
            }
            catch (InvalidArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }
    }
}
