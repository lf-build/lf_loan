using System.Linq;
using LendFoundry.Borrowers.Repository;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;
using LendFoundry.EventHub.Client;
using LendFoundry.Loans.Events;

namespace LendFoundry.Loans.Api.Controllers
{
    [Route("/")]
    public class BorrowerController : BaseController
    {
        public BorrowerController(IBorrowerRepository repository, IEventHubClient eventHub)
        {
            Repository = repository;
            EventHub = eventHub;
        }

        private IBorrowerRepository  Repository { get; }

        private IEventHubClient EventHub { get; }

        [HttpPost("/{loanReferenceNumber}/{borrowerReferenceNumber}")]
        public IActionResult ModifyBorrower([FromRoute] string loanReferenceNumber, [FromRoute] string borrowerReferenceNumber, [FromBody] Models.ModifyBorrowerRequest request)
        {
            if (string.IsNullOrEmpty(loanReferenceNumber))
                return ErrorResult.BadRequest("Loan Reference Number cannot be empty.");

            if (string.IsNullOrEmpty(borrowerReferenceNumber))
                return ErrorResult.BadRequest("Borrower Reference Number cannot be empty.");

            if (request == null)
                return ErrorResult.BadRequest("No Borrower information is invalid or has not been provided.");

            if (request.Addresses != null && request.Addresses.Count(b => b.IsPrimary) != 1)
                return ErrorResult.BadRequest("Exactly one address must be the primary address");

            if (request.Phones != null && request.Phones.Count(b => b.IsPrimary) != 1)
                return ErrorResult.BadRequest("Exactly one phone number must be the primary phone number");

            var borrower = Repository.Get(loanReferenceNumber, borrowerReferenceNumber);

            if (borrower == null)
                return new ErrorResult(404, "Borrower not found");

            if (!string.IsNullOrWhiteSpace(request.Email))
                borrower.Email = request.Email;

            if (request.Addresses != null && request.Addresses.Any())
                borrower.Addresses = request.Addresses.ToArray();

            if (request.Phones != null && request.Phones.Any())
                borrower.Phones = request.Phones.ToArray();

            Repository.Update(borrower);
            EventHub.Publish("LoanUpdated", new LoanUpdated(loanReferenceNumber));

            return new HttpOkObjectResult(borrower);
        }
    }
}