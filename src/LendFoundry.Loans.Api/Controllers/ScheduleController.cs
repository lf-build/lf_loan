using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Schedule;
using LendFoundry.Loans.Service;
using LendFoundry.Security.Tokens;
using Microsoft.AspNet.Mvc;
using System;

namespace LendFoundry.Loans.Api.Controllers
{
    [Route("/")]
    public class ScheduleController : BaseController
    {
        public ScheduleController
        (
            ILogger logger,
            IPaymentScheduleServiceFactory paymentServiceFactory,
            ITokenReader tokenReader,
            ILoanService loanService,
            IPaymentScheduleService scheduleService,
            ITenantTime tenantTime
        )
        {
            if (loanService == null)
                throw new ArgumentException(nameof(loanService));

            Logger = logger;
            PaymentServiceFactory = paymentServiceFactory;
            TokenReader = tokenReader;
            LoanService = loanService;
            ScheduleService = scheduleService;
            TenantTime = tenantTime;
        }

        private ILogger Logger { get; }

        private IPaymentScheduleServiceFactory PaymentServiceFactory { get; }

        private ILoanService LoanService { get; }

        private IPaymentScheduleService ScheduleService { get; }

        private ITokenReader TokenReader { get; }

        private ITenantTime TenantTime { get; }

        [HttpGet("schedule/{loanReferenceNumber}/current-due")]
        public IActionResult GetCurrentDue(string loanReferenceNumber)
        {
            return GetCurrentDue(loanReferenceNumber, TenantTime.Today);
        }

        [HttpGet("schedule/{loanReferenceNumber}/current-due/{date}")]
        public IActionResult GetCurrentDue(string loanReferenceNumber, DateTime date)
        {
            return GetCurrentDue(loanReferenceNumber, TenantTime.FromDate(date));
        }

        private IActionResult GetCurrentDue(string loanReferenceNumber, DateTimeOffset date)
        {
            return Execute(() =>
            {
                var currentDue = ScheduleService.GetCurrentDue(loanReferenceNumber, date);
                return new HttpOkObjectResult(currentDue);
            });
        }

        private IActionResult Execute(Func<IActionResult> expression)
        {
            try
            {
                return expression();
            }
            catch (InvalidArgumentException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (LoanNotFoundException ex)
            {
                return ErrorResult.NotFound(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return ErrorResult.NotFound(ex.Message);
            }
            catch (ClientException ex)
            {
                return new ErrorResult(ex.Error.Code, ex.Error.Message);
            }
            catch (Exception ex)
            {
                Logger.Error("Error when handling request in LoanController", ex);
                return ErrorResult.InternalServerError("We couldn't process your request");
            }
        }
    }
}