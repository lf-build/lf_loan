﻿using AutoMapper;
using LendFoundry.Loans.Service;
using Microsoft.AspNet.Mvc;

namespace LendFoundry.Loans.Api.Controllers
{
    public abstract class BaseController : Controller
    {
        static BaseController()
        {
            Mapper.CreateMap<ILoanDetails, Models.CompleteLoanInfo>()
                .ForMember(dest => dest.Info, options => options.MapFrom(src => src.LoanInfo))
                .ForMember(dest => dest.Transactions, options => options.MapFrom(src => src.Transactions))
                .ForMember(dest => dest.Schedule, options => options.MapFrom(src => src.PaymentSchedule.Installments));

            Mapper.CreateMap<ILoanInfo, Models.LoanInfo>()
                .ForMember(dest => dest.ReferenceNumber, options => options.MapFrom(src => src.Loan.ReferenceNumber))
                .ForMember(dest => dest.FundingSource, options => options.MapFrom(src => src.Loan.FundingSource))
                .ForMember(dest => dest.Purpose, options => options.MapFrom(src => src.Loan.Purpose))
                .ForMember(dest => dest.Status, options => options.MapFrom(src => src.Loan.Status))
                .ForMember(dest => dest.Terms, options => options.MapFrom(src => src.Loan.Terms))
                .ForMember(dest => dest.Borrowers, options => options.MapFrom(src => src.Loan.Borrowers))
                .ForMember(dest => dest.Investor, options => options.MapFrom(src => src.Loan.Investor))
                .ForMember(dest => dest.Scores, options => options.MapFrom(src => src.Loan.Scores))
                .ForMember(dest => dest.Grade, options => options.MapFrom(src => src.Loan.Grade))
                .ForMember(dest => dest.PreCloseDti, options => options.MapFrom(src => src.Loan.PreCloseDti))
                .ForMember(dest => dest.PostCloseDti, options => options.MapFrom(src => src.Loan.PostCloseDti))
                .ForMember(dest => dest.MonthlyIncome, options => options.MapFrom(src => src.Loan.MonthlyIncome))
                .ForMember(dest => dest.HomeOwnership, options => options.MapFrom(src => src.Loan.HomeOwnership))
                .ForMember(dest => dest.CampaignCode, options => options.MapFrom(src => src.Loan.CampaignCode))
                .ForMember(dest => dest.BankInfo, options => options.MapFrom(src => src.Loan.BankAccounts))
                .ForMember(dest => dest.PaymentMethod, options => options.MapFrom(src => src.Loan.PaymentMethod))
                .ForMember(dest => dest.LastPaymentDate, options => options.MapFrom(src => src.Summary.LastPaymentDate))
                .ForMember(dest => dest.NextDueDate, options => options.MapFrom(src => src.Summary.NextDueDate))
                .ForMember(dest => dest.LastPaymentAmount, options => options.MapFrom(src => src.Summary.LastPaymentAmount))
                .ForMember(dest => dest.RemainingBalance, options => options.MapFrom(src => src.Summary.RemainingBalance))
                .ForMember(dest => dest.CurrentDue, options => options.MapFrom(src => src.Summary.CurrentDue))
                .ForMember(dest => dest.DaysPastDue, options => options.MapFrom(src => src.Summary.DaysPastDue))
                .ForMember(dest => dest.IsInGracePeriod, options => options.MapFrom(src => src.Summary.IsInGracePeriod))
                .ForMember(dest => dest.NextInstallmentAmount, options => options.MapFrom(src => src.Summary.NextInstallmentAmount));

            Mapper.CreateMap<IScore, Score>();

            Mapper.CreateMap<LoanStatus, Models.Status>()
                .AfterMap((src, dest) => dest.Description = src.Name);
            
            Mapper.CreateMap<ILoanTerms, LoanTerms>();
            Mapper.CreateMap<ILoanInvestor, LoanInvestor>();
            Mapper.CreateMap<IScore, Score>();
            Mapper.CreateMap<IBankAccount, BankAccount>();
            Mapper.CreateMap<IBorrower, Borrower>();


            Mapper.CreateMap<IInstallment, Models.Installment>()
                .ForMember(dest => dest.DueDate, options => options.MapFrom(src => src.DueDate))
                .ForMember(dest => dest.AnniversaryDate, options => options.MapFrom(src => src.AnniversaryDate))
                .ForMember(dest => dest.EndingBalance, options => options.MapFrom(src => src.EndingBalance))
                .ForMember(dest => dest.Interest, options => options.MapFrom(src => src.Interest))
                .ForMember(dest => dest.OpeningBalance, options => options.MapFrom(src => src.OpeningBalance))
                .ForMember(dest => dest.PaymentAmount, options => options.MapFrom(src => src.PaymentAmount))
                .ForMember(dest => dest.PaymentDate, options => options.MapFrom(src => src.PaymentDate))
                .ForMember(dest => dest.PaymentId, options => options.MapFrom(src => src.PaymentId))
                .ForMember(dest => dest.PaymentMethod, options => options.MapFrom(src => src.PaymentMethod))
                .ForMember(dest => dest.Principal, options => options.MapFrom(src => src.Principal))
                .ForMember(dest => dest.Status, options => options.MapFrom(src => src.Status))
                .ForMember(dest => dest.Type, options => options.MapFrom(src => src.Type));

            Mapper.CreateMap<ITransaction, Models.Transaction>()
                .ForMember(dest => dest.LoanReference, options => options.MapFrom(src => src.LoanReferenceNumber));

            Mapper.CreateMap<IInstallmentInfo, Models.InstallmentInfo>();            
            Mapper.CreateMap<ILoan, Models.Loan>()
                  .ForMember(dest => dest.BankInfo, options => options.MapFrom(src => src.BankAccounts));
        }
    }
}