﻿using LendFoundry.Loans.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Api.Controllers
{
    public static class ValidationHelper
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable == null)
            {
                return true;
            }
            var collection = enumerable as ICollection<T>;
            if (collection != null)
            {
                return collection.Count < 1;
            }
            return !enumerable.Any();
        }
        public static bool IsNegativeOrZero(this int value)
        {
            return value <= 0;
        }
        public static bool HasInvalidLoan(this List<OnboardingRequest> list)
        {
            foreach (var lo in list)
            {
                if (string.IsNullOrEmpty(lo.ReferenceNumber) ||
                    string.IsNullOrEmpty(lo.FundingSource) ||
                    lo.Borrowers.IsNullOrEmpty() ||
                    lo.Terms == null)
                {
                    return true;
                }
            }
            return false;
        }
    }
}