﻿namespace LendFoundry.Loans.Api.Models
{
    public class InstallmentInfo
    {
        public Loan Loan { get; set; }
        public Installment Installment { get; set; }
    }
}
