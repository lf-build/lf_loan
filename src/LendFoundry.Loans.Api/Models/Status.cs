﻿namespace LendFoundry.Loans.Api.Models
{
    public class Status
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
