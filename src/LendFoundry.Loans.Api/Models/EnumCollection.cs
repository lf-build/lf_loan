﻿namespace LendFoundry.Loans.Api.Models
{
    public enum FeeType
    {
        Fixed,
        Percent
    }
    public enum TypeAccount
    {
        Checking,
        Savings
    }
    public enum TypePayment
    {
        ACH,
        Check
    }
    public enum TypeRate
    {
        Percent,
        Factor
    }
    public enum PhoneType
    {
        Home,
        Work,
        Mobile,
        Other
    }
}
