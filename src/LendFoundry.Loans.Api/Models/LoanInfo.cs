﻿using System;

namespace LendFoundry.Loans.Api.Models
{
    public class LoanInfo
    {
        public string ReferenceNumber { get; set; }
        public Status Status { get; set; }
        public string FundingSource { get; set; }
        public string Purpose { get; set; }
        public LoanInvestor Investor { get; set; }
        public Score[] Scores { get; set; }
        public string Grade { get; set; }
        public double? PreCloseDti { get; set; }
        public double? PostCloseDti { get; set; }
        public double MonthlyIncome { get; set; }
        public string HomeOwnership { get; set; }
        public string CampaignCode { get; set; }
        public IBorrower[] Borrowers { get; set; }
        public LoanTerms Terms { get; set; }
        public BankAccount[] BankInfo { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public DateTimeOffset? NextDueDate { get; set; }
        public double NextInstallmentAmount { get; set; }
        public double CurrentDue { get; set; }
        public int DaysPastDue { get; set; }
        public DateTimeOffset? LastPaymentDate { get; set; }
        public double LastPaymentAmount { get; set; }
        public double RemainingBalance { get; set; }     
        public bool IsInGracePeriod { get; set; }
    }
}