﻿namespace LendFoundry.Loans.Api.Models
{
    public class CompleteLoanInfo
    {
        public LoanInfo Info { get; set; }
        public Transaction[] Transactions { get; set; }
        public Installment[] Schedule { get; set; }
    }
}
