﻿using System.Collections.Generic;

namespace LendFoundry.Loans.Api.Models
{
    public class ModifyBorrowerRequest
    {
        public string Email { get; set; }

        public IList<Loans.Address> Addresses { get; set; }

        public IList<Loans.Phone> Phones { get; set; }
    }
}