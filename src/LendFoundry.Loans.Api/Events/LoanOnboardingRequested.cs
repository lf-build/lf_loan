﻿using LendFoundry.Loans.Service;

namespace LendFoundry.Loans.Api.Events
{
    public class LoanOnboardingRequested
    {
        public LoanOnboardingRequested(OnboardingRequest request)
        {
            LoanReferenceNumber = request.ReferenceNumber;
            OriginationDate = request.Terms.OriginationDate.ToString("o");
            Request = request;
        }

        public string LoanReferenceNumber { get; set; }
        public string OriginationDate { get; set; }
        public OnboardingRequest Request { get; set; }
    }
}
