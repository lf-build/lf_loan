﻿using LendFoundry.Amortization.Client;
using LendFoundry.Borrowers.Repository;
using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Events;
using LendFoundry.Loans.Investors;
using LendFoundry.Loans.Payment.Events;
using LendFoundry.Loans.Repository;
using LendFoundry.Loans.Schedule;
using LendFoundry.Loans.Schedule.Calculators;
using LendFoundry.Loans.Schedule.Data;
using LendFoundry.Loans.Service;
using LendFoundry.Loans.Service.Summary;
using LendFoundry.Loans.TransactionLog.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using System;

namespace LendFoundry.Loans.Api
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureLoanService(services);

            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddCors();

            services.AddMvc().AddLendFoundryJsonOptions((options) =>
            {
                options
                    .AddInterfaceConverter<IBorrower, Borrower>()
                    .AddInterfaceConverter<IFee, Fee>()
                    .AddInterfaceConverter<ILoanInvestor, LoanInvestor>()
                    .AddInterfaceConverter<ILoanTerms, LoanTerms>()
                    .AddInterfaceConverter<IBankAccount, BankAccount>()
                    .AddInterfaceConverter<IScore, Score>();
            });

            services
                .AddEventSubscription<PaymentAdded, EventSubscriptions>()
                .AddEventSubscription<PaymentReceived, EventSubscriptions>()
                .AddEventSubscription<PaymentFailed, EventSubscriptions>()
                .AddEventSubscription<PaymentCancelled, EventSubscriptions>()
                .AddEventSubscription<PaymentMethodChanged, EventSubscriptions>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
            app.UseEventHub();
        }

        private static void ConfigureLoanService(IServiceCollection services)
        {
            services.AddTransient<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));

            // Loan config
            var configurationHost = Environment.GetEnvironmentVariable("LOAN_CONFIG_HOST") ?? "configuration";
            var configurationPort = int.Parse(Environment.GetEnvironmentVariable("LOAN_CONFIG_PORT") ?? "5000");
            services.AddConfigurationService<LoanConfiguration>(configurationHost, configurationPort, Settings.ServiceName);

            // Payment schedule service config
            services.AddTransient(provider =>
            {
                return provider
                    .GetService<IConfigurationService<LoanConfiguration>>()
                    .Get();
            });

            // Payment schedule service config
            services.AddTransient(provider =>
            {
                return provider.GetService<LoanConfiguration>().Schedule;
            });

            // Investors
            services.AddTransient<IInvestorService, InvestorService>();

            // Amortization service
            var amortizationHost = Environment.GetEnvironmentVariable("LOAN_AMORTIZATION_HOST") ?? "amortization";
            var amortizationPort = int.Parse(Environment.GetEnvironmentVariable("LOAN_AMORTIZATION_PORT") ?? "5000");
            services.AddAmortizationService(amortizationHost, amortizationPort);

            // Engines
            services.AddTransient<ILoanStatusEngine, StatusEngine>();
            services.AddTransient<IBankAccountEngine, BankAccountEngine>();
            services.AddTransient<IPaymentMethodEngine, PaymentMethodEngine>();
            services.AddTransient<ILoanEngineResolver, LoanEngineResolver>();

            // Calendar
            var calendarHost = Environment.GetEnvironmentVariable("LOAN_CALENDAR_HOST") ?? "calendar";
            var calendarPort = int.Parse(Environment.GetEnvironmentVariable("LOAN_CALENDAR_PORT") ?? "5000");
            services.AddCalendarService(calendarHost, calendarPort);

            // Transation log
            services.AddTransactionLog
            (Environment.GetEnvironmentVariable("LOAN_TRANSACTIONLOG_HOST") ?? "transactionlog", int.Parse(Environment.GetEnvironmentVariable("LOAN_TRANSACTIONLOG_PORT") ?? "5000")
            );

            // Tenant service
            services.AddTenantService
            (Environment.GetEnvironmentVariable("LOAN_TENANT_HOST") ?? "tenant", int.Parse(Environment.GetEnvironmentVariable("LOAN_TENANT_PORT") ?? "5000")
            );

            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.ServiceName);

            // The event hub client must be created with a static token reader because
            // it publishes events asynchronously and the http context may not be available
            // by the time the publishing request is sent.
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });

            // Others
            services.AddTransient<IGracePeriod, GracePeriod>();
            services.AddTransient<IGracePeriodFactory, GracePeriodFactory>();
            services.AddTransient<IGracePeriodCalculator, GracePeriodCalculator>();
            services.AddTransient<IGracePeriodCalculatorFactory, GracePeriodCalculatorFactory>();
            services.AddTransient<IDaysPastDueCalculator, DaysPastDueCalculator>();
            services.AddTransient<IDaysPastDueCalculatorFactory, DaysPastDueCalculatorFactory>();
            services.AddTransient<ICurrentDueCalculator, CurrentDueCalculator>();
            services.AddTransient<ICurrentDueCalculatorFactory, CurrentDueCalculatorFactory>();
            services.AddTransient<IPaymentScheduleService, PaymentScheduleService>();
            services.AddTransient<IPaymentScheduleRepository, PaymentScheduleRepository>();
            services.AddTransient<ILoanService, LoanService>();            
            services.AddTransient<IBorrowerRepository, BorrowerRepository>();
            services.AddTransient<ILoanRepository, LoanRepository>();
            services.AddTransient<IPaymentScheduleRepositoryFactory, PaymentScheduleRepositoryFactory>();
            services.AddTransient<IPaymentScheduleServiceFactory, PaymentScheduleServiceFactory>();
            services.AddTransient<ILoanRepositoryFactory, LoanRepositoryFactory>();
            services.AddTransient<IPayoffAmountCalculator, PayoffAmountCalculator>();
            services.AddTransient<IPayoffAmountCalculatorFactory, PayoffAmountCalculatorFactory>();

            services.AddTransient<IPaystring, Paystring>();
            services.AddTransient<IInterestAccruedFunction, InterestAccruedFunction>();
            services.AddTransient<INetInterestAccruedFunction, NetInterestAccruedFunction>();
            services.AddTransient<IAverageDailyBalanceFunction, AverageDailyBalanceFunction>();
            services.AddTransient<IInterestPaidFunction, InterestPaidFunction>();
            services.AddTransient<ILoanSummaryBuilder, LoanSummaryBuilder>();
        }
    }
}
