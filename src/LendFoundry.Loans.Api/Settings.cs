﻿using LendFoundry.Foundation.Services.Settings;

namespace LendFoundry.Loans.Api
{
    internal static class Settings
    {
        public static string ServiceName { get; } = "loan";

        public static ServiceSettings EventHub { get; } = new ServiceSettings("LOAN_EVENTHUB", "eventhub");

        public static ServiceSettings Payment { get; } = new ServiceSettings("LOAN_PAYMENT", "loan-payments");

        public static DatabaseSettings Mongo { get; } = new DatabaseSettings("LOAN_MONGO_CONNECTIONSTRING", "mongodb://mongo:27017", "LOAN_MONGO_DATABASE", "loans");
    }
}