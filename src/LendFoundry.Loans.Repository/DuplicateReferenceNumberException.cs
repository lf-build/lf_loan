﻿using System;

namespace LendFoundry.Loans.Repository
{
    public class DuplicateReferenceNumberException : Exception
    {
        public DuplicateReferenceNumberException(string message) : base(message)
        {
        }
    }
}
