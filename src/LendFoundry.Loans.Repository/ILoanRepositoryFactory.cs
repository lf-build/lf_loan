﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Repository
{
    public interface ILoanRepositoryFactory
    {
        ILoanRepository Create(ITokenReader reader);
    }
}
