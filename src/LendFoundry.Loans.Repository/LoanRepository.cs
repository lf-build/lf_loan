﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Repository
{
    public class LoanRepository : MongoRepository<ILoan, Loan>, ILoanRepository
    {
        static LoanRepository()
        {
            BsonClassMap.RegisterClassMap<Loan>(map =>
            {
                map.AutoMap();

                //Borrowers are stored separately in their own repository
                map.UnmapProperty(loan => loan.Borrowers);

                var type = typeof(Loan);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<LoanTerms>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.ApplicationDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(m => m.FundedDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(m => m.OriginationDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(LoanTerms);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<LoanInvestor>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.LoanPurchaseDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(LoanInvestor);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<Score>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.InitialDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(m => m.UpdatedDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));

                var type = typeof(Score);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<BankAccount>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.EffectiveDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(m => m.AccountType).SetSerializer(new EnumSerializer<BankAccountType>(BsonType.String));

                var type = typeof(BankAccount);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<LoanStatus>(map =>
            {                
                map.MapMember(m => m.Code);
                map.MapMember(m => m.Name);
                map.MapMember(m => m.StartDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));
                map.MapMember(m => m.EndDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));                                

                var type = typeof(LoanStatus);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<ILoanTerms, LoanTerms>());
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IFee, Fee>());
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<ILoanInvestor, LoanInvestor>());
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IScore, Score>());
            BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IBankAccount, BankAccount>());
        }

        public LoanRepository(IMongoConfiguration configuration, ITenantService tenantService)
            : base(tenantService, configuration, "loans")
        {
            CreateIndexIfNotExists
            (
               indexName: "reference-number",
               index: Builders<ILoan>.IndexKeys.Ascending(i => i.ReferenceNumber)
            );

            CreateIndexIfNotExists
            (
               indexName: "status-code",
               index: Builders<ILoan>.IndexKeys.Ascending(i => i.Status.Code)
            );
        }

        public new ILoan Get(string loanReferenceNumber)
        {
            if (loanReferenceNumber.IsNullOrEmpty())
                throw new ArgumentException(LocalResources.MsgBadLoanRefNumber);

            return Query.FirstOrDefault(q => q.ReferenceNumber == loanReferenceNumber);
        }

        public void Insert(ILoan loan)
        {
            if (loan == null)
                throw new ArgumentException(LocalResources.MsgBadLoan);

            if (loan.Borrowers.IsNullOrEmpty())
                throw new ArgumentException(LocalResources.MsgBagBorrower);

            if (ReferenceNumberAlreadyExists(loan.ReferenceNumber))
                throw new DuplicateReferenceNumberException($"Loan with reference number {loan.ReferenceNumber} already exists.");

            Add(loan);
        }

        private bool ReferenceNumberAlreadyExists(string loanReferenceNumber)
        {
            return Query.Where(q => q.ReferenceNumber == loanReferenceNumber).Any();
        }

        public void Delete(string loanReferenceNumber)
        {
            if (loanReferenceNumber.IsNullOrEmpty())
                throw new ArgumentException(LocalResources.MsgBadLoanRefNumber);

            Collection.FindOneAndDeleteAsync(q =>
                q.TenantId == TenantService.Current.Id &&
                q.ReferenceNumber == loanReferenceNumber);
        }

        public IEnumerable<ILoan> Get(IEnumerable<string> loanReferenceNumbers)
        {
            if (loanReferenceNumbers == null)
                throw new ArgumentNullException(nameof(loanReferenceNumbers));

            var builder = new FilterDefinitionBuilder<ILoan>();
            var tenant = builder.Eq(candidate => candidate.TenantId, TenantService.Current.Id);
            var @in = builder.In(candidate => candidate.ReferenceNumber, loanReferenceNumbers);
            var filter = builder.And(tenant, @in);
            return Collection.Find(filter).ToListAsync().Result;
        }

        public IEnumerable<ILoan> Get(LoanStatus status)
        {
            if (status == null)
                throw new ArgumentNullException(nameof(status));

            return Query.Where(q => q.Status.Code == status.Code).ToList();
        }

        public void UpdateLoanBankAccounts(string referenceNumber, IEnumerable<IBankAccount> bankAccounts)
        {
            var loanToBeChanged = Get(referenceNumber);
            if (loanToBeChanged == null)
                throw new ArgumentException($"Loan with reference number #{referenceNumber} could not be found.");

            var update = Builders<ILoan>.Update.Set(s => s.BankAccounts, bankAccounts);
            Collection.FindOneAndUpdateAsync(q =>
                q.TenantId == TenantService.Current.Id &&
                q.ReferenceNumber == referenceNumber, update);
        }

        public void UpdateLoanStatus(string referenceNumber, LoanStatus loanStatus)
        {
            var loanToBeChanged = Get(referenceNumber);
            if (loanToBeChanged == null)
                throw new ArgumentException($"Loan with reference number #{referenceNumber} could not be found.");

            var update = Builders<ILoan>.Update.Set(s => s.Status, loanStatus);
            Collection.FindOneAndUpdateAsync(q =>
                q.TenantId == TenantService.Current.Id &&
                q.ReferenceNumber == referenceNumber, update);
        }

        public void UpdatePaymentMethod(string referenceNumber, PaymentMethod paymentMethod)
        {
            var loanToBeChanged = Get(referenceNumber);
            if (loanToBeChanged == null)
                throw new ArgumentException($"Loan with reference number #{referenceNumber} could not be found.");

            var update = Builders<ILoan>.Update.Set(s => s.PaymentMethod, paymentMethod);
            Collection.FindOneAndUpdateAsync(q =>
                q.TenantId == TenantService.Current.Id &&
                q.ReferenceNumber == referenceNumber, update);
        }

        private static class LocalResources
        {
            public static string MsgBadLoanRefNumber => "Loan Reference Number must to be informed.";
            public static string MsgBadLoan => "Loan must to be informed.";
            public static string MsgBagBorrower => "At least one borrower must to be informed.";

        }
    }
}
