﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;

namespace LendFoundry.Loans.Repository
{
    public class LoanRepositoryFactory : ILoanRepositoryFactory
    {
        public LoanRepositoryFactory(IMongoConfiguration configuration, ITenantServiceFactory tenantServiceFactory)
        {
            Configuration = configuration;
            TenantServiceFactory = tenantServiceFactory;
        }

        private IMongoConfiguration Configuration { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }

        public ILoanRepository Create(ITokenReader reader)
        {
            return new LoanRepository(Configuration, TenantServiceFactory.Create(reader));
        }
    }
}
