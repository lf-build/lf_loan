﻿using System.Collections.Generic;

namespace LendFoundry.Loans.Repository
{
    public interface ILoanRepository
    {
        void Insert(ILoan loan);
        ILoan Get(string loanReferenceNumber);
        IEnumerable<ILoan> Get(IEnumerable<string> loanReferenceNumbers);
        IEnumerable<ILoan> Get(LoanStatus status);
        void Delete(string loanReferenceNumber);
        void UpdateLoanStatus(string referenceNumber, LoanStatus loanStatus);
        void UpdateLoanBankAccounts(string referenceNumber, IEnumerable<IBankAccount> bankAccounts);
        void UpdatePaymentMethod(string referenceNumber, PaymentMethod paymentMethod);
    }
}
