﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Loans;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace LendFoundry.Borrowers.Repository
{
    public class BorrowerRepository : MongoRepository<IBorrower, Borrower>, IBorrowerRepository
    {
        static BorrowerRepository()
        {
            BsonClassMap.RegisterClassMap<Borrower>(map =>
            {
                map.AutoMap();
                map.MapProperty(p => p.DateOfBirth).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(Borrower);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public BorrowerRepository(IMongoConfiguration configuration, ITenantService tenantService, ITenantTime tenantTime)
            : base(tenantService, configuration, "borrowers")
        {
            TenantTime = tenantTime;

            CreateIndexIfNotExists
            (
               indexName: "reference-number",
               index: Builders<IBorrower>.IndexKeys.Ascending(i => i.ReferenceNumber)
            );
        }

        private ITenantTime TenantTime { get; }

        public IEnumerable<IBorrower> All(string loanReferenceNumber)
        {
            if (string.IsNullOrEmpty(loanReferenceNumber))
                throw new ArgumentException(LocalResources.MsgBadLoan);
            return Query.Where(q => q.LoanReferenceNumber == loanReferenceNumber).ToList();
        }

        public IBorrower Get(string loanReferenceNumber, string borrowerReferenceNumber)
        {
            return Query.FirstOrDefault(FindByLoanAndReferenceNumber(loanReferenceNumber, borrowerReferenceNumber));
        }

        private Expression<Func<IBorrower, bool>> FindByLoanAndReferenceNumber(string loanReferenceNumber, string borrowerReferenceNumber)
        {
            return e => e.TenantId == TenantService.Current.Id &&
                        e.LoanReferenceNumber == loanReferenceNumber &&
                        e.ReferenceNumber == borrowerReferenceNumber;
        }

        public void Store(IEnumerable<IBorrower> borrowers, string loanReferenceNumber)
        {
            if (borrowers.IsNullOrEmpty())
                throw new ArgumentException(LocalResources.MsgBadBorrower);

            if (loanReferenceNumber.IsNullOrEmpty())
                throw new ArgumentException(LocalResources.MsgBadLoan);

            borrowers = borrowers.Select(b =>
            {
                b.LoanReferenceNumber = loanReferenceNumber;
                b.DateOfBirth = TenantTime.FromDate(b.DateOfBirth.Date);
                b.TenantId = TenantService.Current.Id;
                b.Id = ObjectId.GenerateNewId().ToString();
                return b;
            }).ToList();
            Collection.InsertManyAsync(borrowers);
        }

        public void Delete(IEnumerable<IBorrower> borrowers, string loanReferenceNumber)
        {
            if (borrowers.IsNullOrEmpty())
                throw new ArgumentException(LocalResources.MsgBadBorrower);

            if (loanReferenceNumber.IsNullOrEmpty())
                throw new ArgumentException(LocalResources.MsgBadLoan);

            borrowers.ToList()
                     .ForEach(b =>
                     {
                         Collection.FindOneAndDeleteAsync(d =>
                            d.LoanReferenceNumber == loanReferenceNumber &&
                            d.ReferenceNumber == b.ReferenceNumber &&
                            d.TenantId == TenantService.Current.Id);
                     });
        }

        private static class LocalResources
        {
            public static string MsgBadLoan => "Loan must to be informed.";
            public static string MsgBadBorrower => "At least one borrower must to be informed.";
        }
    }
}
