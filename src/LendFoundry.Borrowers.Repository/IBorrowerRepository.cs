﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Loans;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Borrowers.Repository
{
    public interface IBorrowerRepository : IRepository<IBorrower>
    {
        void Store(IEnumerable<IBorrower> borrowers, string loanReferenceNumber);

        void Delete(IEnumerable<IBorrower> borrowers, string loanReferenceNumber);

        IEnumerable<IBorrower> All(string loanReferenceNumber);

        IBorrower Get(string loanReferenceNumber, string borrowerReferenceNumber);
    }
}
