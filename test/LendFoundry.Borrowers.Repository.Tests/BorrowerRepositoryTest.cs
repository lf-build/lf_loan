﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Testing.Mongo;
using LendFoundry.Loans;
using LendFoundry.Tenant.Client;
using Moq;
using System;
using System.Linq;
using Xunit;

namespace LendFoundry.Borrowers.Repository.Tests
{
    public class BorrowerRepositoryTest
    {
        private Mock<ITenantService> TenentService { get; set; }

        private IBorrowerRepository Repository(IMongoConfiguration config)
        {
            TenentService = new Mock<ITenantService>();
            TenentService.SetupGet(s => s.Current).Returns(new TenantInfo { Id = "my-tenant" });
            var mockTenantTime = new UtcTenantTime();
            return new BorrowerRepository
            (
                configuration: config,
                tenantService: TenentService.Object,
                tenantTime: mockTenantTime
            );
        }

        private Borrower Borrower
        {
            get
            {
                return new Borrower
                {
                    ReferenceNumber = string.Empty,
                    IsPrimary = true,
                    FirstName = "John",
                    MiddleInitial = "Z",
                    LastName = "Doe",
                    DateOfBirth = new DateTime(1977, 7, 20),
                    SSN = "721-07-4426",
                    Email = "john@gmail.com",
                    Addresses = new[]
                    {
                        new Address{IsPrimary = true,Line1 = "196 25th Avenue",City = "San Francisco",State = "CA",ZipCode = "94121"}
                    },
                    Phones = new[]
                    {
                        new Phone{IsPrimary = true,Type = PhoneType.Home,Number = "65202108"}
                    }
                };
            }
        }

        [MongoFact]
        public void GetByLoanReference()
        {
            MongoTest.Run((config) =>
            {
                // arrange
                string loanReferenceNumber = "Loan001";
                var repository = Repository(config);
                repository.Store(GenerateEntry(), loanReferenceNumber);

                // act
                var result = repository.All(loanReferenceNumber);

                // assert
                Assert.NotNull(result);
                Assert.True(result.All(x => x.Id != string.Empty));
                Assert.True(result.All(x => x.LoanReferenceNumber == loanReferenceNumber));
                Assert.True(result.All(x => x.TenantId == TenentService.Object.Current.Id));
            });
        }

        [MongoFact]
        public void GetByLoanAndBorrowerReference()
        {
            MongoTest.Run((config) =>
            {
                // arrange
                string loanReferenceNumber = "Loan001";
                var repository = Repository(config);
                var listBorrowers = GenerateEntry();
                repository.Store(listBorrowers, loanReferenceNumber);

                // act
                var result = repository.Get(loanReferenceNumber, listBorrowers.First().ReferenceNumber);

                // assert
                Assert.NotNull(result);
                Assert.Equal(loanReferenceNumber, result.LoanReferenceNumber);
                Assert.False(string.IsNullOrEmpty(result.Id));
            });
        }

        [MongoFact]
        public void Update()
        {
            MongoTest.Run((config) =>
            {
                // arrange
                string loanReferenceNumber = "Loan001";
                var repository = Repository(config);
                var listBorrowers = GenerateEntry();
                repository.Store(listBorrowers, loanReferenceNumber);

                // act
                var borrowerToUpdate = repository.All(loanReferenceNumber).First();
                borrowerToUpdate.Email = "updated@mail.com";
                borrowerToUpdate.DateOfBirth = new DateTimeOffset(new DateTime(1984, 8, 20));
                repository.Update(borrowerToUpdate);
                var result = repository.All(loanReferenceNumber);
                var updatedBorrower = result.FirstOrDefault(x => x.ReferenceNumber == borrowerToUpdate.ReferenceNumber);

                // assert
                Assert.NotNull(updatedBorrower);
                Assert.Equal(updatedBorrower.ReferenceNumber, borrowerToUpdate.ReferenceNumber);
                Assert.Equal(loanReferenceNumber, borrowerToUpdate.LoanReferenceNumber);
                Assert.Equal("updated@mail.com", updatedBorrower.Email);
                Assert.Equal(new DateTimeOffset(new DateTime(1984, 8, 20)), updatedBorrower.DateOfBirth);
            });
        }

        [MongoFact]
        public void Store()
        {
            MongoTest.Run((config) =>
            {
                // arrange
                string loanReferenceNumber = "Loan001";
                var repository = Repository(config);
                var listBorrowers = GenerateEntry();

                // act
                repository.Store(listBorrowers, loanReferenceNumber);

                // assert
                var result = repository.All(loanReferenceNumber);
                Assert.NotNull(result);
                Assert.Equal(listBorrowers.Count(), result.Count());
                Assert.True(result.All(x => x.Id != string.Empty));
                Assert.True(result.All(x => x.LoanReferenceNumber == loanReferenceNumber));
                Assert.True(result.All(x => x.TenantId == TenentService.Object.Current.Id));
            });
        }

        [MongoFact]
        public void Delete()
        {
            MongoTest.Run((config) =>
            {
                // arrange
                string loanReferenceNumber = "Loan001";
                var repository = Repository(config);
                var listBorrowers = GenerateEntry();
                repository.Store(listBorrowers, loanReferenceNumber);
                var objToDelete = repository.All(loanReferenceNumber);

                // act
                repository.Delete(objToDelete.Take(2), loanReferenceNumber);
                var result1 = repository.Get(loanReferenceNumber, objToDelete.ToList()[0].ReferenceNumber);
                var result2 = repository.Get(loanReferenceNumber, objToDelete.ToList()[1].ReferenceNumber);
                var result3 = repository.Get(loanReferenceNumber, objToDelete.ToList()[2].ReferenceNumber);

                // assert                
                Assert.Null(result1);
                Assert.Null(result2);
                Assert.NotNull(result3);
            });
        }

        private IBorrower[] GenerateEntry()
        {
            var listBorrowers = new[] { Borrower, Borrower, Borrower };
            listBorrowers = listBorrowers.Select((b, i) =>
            {
                b.ReferenceNumber = $"Borrower{i}";
                return b;
            }).ToArray();
            return listBorrowers;
        }
    }
}
