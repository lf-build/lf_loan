﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LendFoundry.Loans.Abstractions.Tests
{
    public class PaymentScheduleTest
    {
        [Fact]
        public void FirstInstallment_OrderByDueDate()
        {
            var installment1 = new Installment { DueDate = DateTime.Today };
            var installment2 = new Installment { DueDate = DateTime.Today.AddDays(10) };
            var installment3 = new Installment { DueDate = DateTime.Today.AddDays(20) };

            IPaymentSchedule schedule = new PaymentSchedule
            {
                Installments = new List<IInstallment>
                {
                    installment2,
                    installment1,
                    installment3
                }
            };

            Assert.Same(installment1, schedule.FirstInstallment);
        }

        [Fact]
        public void LastPaidInstallment_OrderByDueDate()
        {
            var installment1 = new Installment { Status = InstallmentStatus.Completed, DueDate = DateTime.Today };
            var installment2 = new Installment { Status = InstallmentStatus.Completed, DueDate = DateTime.Today.AddDays(10) };
            var installment3 = new Installment { Status = InstallmentStatus.Completed, DueDate = DateTime.Today.AddDays(20), PaymentAmount = 100 };
            var installment4 = new Installment { Status = InstallmentStatus.Scheduled, DueDate = DateTime.Today.AddDays(30) };
            var installment5 = new Installment { Status = InstallmentStatus.Scheduled, DueDate = DateTime.Today.AddDays(40) };

            IPaymentSchedule schedule = new PaymentSchedule
            {
                Installments = new List<IInstallment>
                {
                    installment2,
                    installment3,
                    installment1,
                    installment5,
                    installment4
                }
            };

            Assert.Same(installment3, schedule.LastPaidInstallment);
        }

        [Fact]
        public void LastPaidInstallment_OrderByTransactionDateWhenDueDateIsBeforeTransactionDate()
        {
            var installment1 = new Installment { Status = InstallmentStatus.Completed, DueDate = DateTime.Today };
            var installment2 = new Installment { Status = InstallmentStatus.Completed, DueDate = DateTime.Today.AddDays(10) };
            var installment3 = new Installment
            {
                Status = InstallmentStatus.Completed,
                DueDate = DateTime.Today.AddDays(5),
                PaymentDate = DateTime.Today.AddDays(20),
                PaymentAmount = 100
            };
            var installment4 = new Installment { Status = InstallmentStatus.Scheduled, DueDate = DateTime.Today.AddDays(30) };
            var installment5 = new Installment { Status = InstallmentStatus.Scheduled, DueDate = DateTime.Today.AddDays(40) };

            IPaymentSchedule schedule = new PaymentSchedule
            {
                Installments = new List<IInstallment>
                {
                    installment2,
                    installment3,
                    installment1,
                    installment5,
                    installment4
                }
            };

            Assert.Same(installment3, schedule.LastPaidInstallment);
        }

        [Fact]
        public void LastPaidInstallmentIsNullWhenNoPaidInstallmentIsFound()
        {
            var installment1 = new Installment { Status = InstallmentStatus.Scheduled, DueDate = DateTime.Today };
            var installment2 = new Installment { Status = InstallmentStatus.Scheduled, DueDate = DateTime.Today.AddMonths(1) };
            var installment3 = new Installment { Status = InstallmentStatus.Scheduled, DueDate = DateTime.Today.AddMonths(2) };

            IPaymentSchedule schedule = new PaymentSchedule
            {
                Installments = new List<IInstallment>
                {
                    installment1,
                    installment2,
                    installment3
                }
            };

            Assert.Null(schedule.LastPaidInstallment);
        }

        [Fact]
        public void FirstScheduledInstallment_OrderByDueDate()
        {
            var installment1 = new Installment { Status = InstallmentStatus.Completed, DueDate = DateTime.Today };
            var installment2 = new Installment { Status = InstallmentStatus.Completed, DueDate = DateTime.Today.AddDays(10) };
            var installment3 = new Installment { Status = InstallmentStatus.Completed, DueDate = DateTime.Today.AddDays(20) };
            var installment4 = new Installment { Status = InstallmentStatus.Scheduled, DueDate = DateTime.Today.AddDays(30) };
            var installment5 = new Installment { Status = InstallmentStatus.Scheduled, DueDate = DateTime.Today.AddDays(40) };

            IPaymentSchedule schedule = new PaymentSchedule
            {
                Installments = new List<IInstallment>
                {
                    installment2,
                    installment3,
                    installment1,
                    installment5,
                    installment4
                }
            };

            Assert.Same(installment4, schedule.FirstScheduledInstallment);
        }

        [Fact]
        public void GetLatestInstallmentUpToSomeDate_OrderByDueDate()
        {
            var installment1 = new Installment { DueDate = DateTime.Today };
            var installment2 = new Installment { DueDate = DateTime.Today.AddDays(10) };
            var installment3 = new Installment { DueDate = DateTime.Today.AddDays(20) };

            IPaymentSchedule schedule = new PaymentSchedule
            {
                Installments = new List<IInstallment>
                {
                    installment2,
                    installment3,
                    installment1
                }
            };

            Assert.Null(schedule.GetLatestInstallmentUpTo(DateTime.Today.AddDays(-1)));

            Assert.Same(installment1, schedule.GetLatestInstallmentUpTo(DateTime.Today));
            Assert.Same(installment1, schedule.GetLatestInstallmentUpTo(DateTime.Today.AddDays(5)));
            Assert.Same(installment2, schedule.GetLatestInstallmentUpTo(DateTime.Today.AddDays(10)));
            Assert.Same(installment2, schedule.GetLatestInstallmentUpTo(DateTime.Today.AddDays(15)));
            Assert.Same(installment3, schedule.GetLatestInstallmentUpTo(DateTime.Today.AddDays(20)));
            Assert.Same(installment3, schedule.GetLatestInstallmentUpTo(DateTime.Today.AddDays(25)));
        }

        [Fact]
        public void InstallmentsAreSortedByDueDate()
        {
            var installment1 = new Installment { DueDate = DateTime.Today };
            var installment2 = new Installment { DueDate = DateTime.Today.AddDays(10) };
            var installment3 = new Installment { DueDate = DateTime.Today.AddDays(20) };
            var installment4 = new Installment { DueDate = DateTime.Today.AddDays(30) };
            var installment5 = new Installment { DueDate = DateTime.Today.AddDays(40) };

            IPaymentSchedule schedule = new PaymentSchedule
            {
                Installments = new List<IInstallment>
                {
                    installment3,
                    installment2,
                    installment1,
                    installment5,
                    installment4
                }
            };

            Assert.NotNull(schedule.Installments);
            var installments = schedule.Installments.ToList();
            Assert.Equal(5, installments.Count);
            Assert.Same(installment1, installments[0]);
            Assert.Same(installment2, installments[1]);
            Assert.Same(installment3, installments[2]);
            Assert.Same(installment4, installments[3]);
            Assert.Same(installment5, installments[4]);
        }

        [Fact]
        public void InstallmentsAreSortedByTransactionDateWhenDueDateIsZero()
        {
            var installment1 = new Installment { DueDate = DateTime.Today };
            var installment2 = new Installment { DueDate = DateTime.Today.AddDays(10) };
            var installment3 = new Installment { PaymentDate = DateTime.Today.AddDays(20) };
            var installment4 = new Installment { DueDate = DateTime.Today.AddDays(30) };
            var installment5 = new Installment { PaymentDate = DateTime.Today.AddDays(40) };

            IPaymentSchedule schedule = new PaymentSchedule
            {
                Installments = new List<IInstallment>
                {
                    installment3,
                    installment2,
                    installment1,
                    installment5,
                    installment4
                }
            };

            Assert.NotNull(schedule.Installments);
            var installments = schedule.Installments.ToList();
            Assert.Equal(5, installments.Count);
            Assert.Same(installment1, installments[0]);
            Assert.Same(installment2, installments[1]);
            Assert.Same(installment3, installments[2]);
            Assert.Same(installment4, installments[3]);
            Assert.Same(installment5, installments[4]);
        }

        [Fact]
        public void InstallmentsListCannotBeNull()
        {
            Assert.Throws<ArgumentNullException>("value", () =>
                new PaymentSchedule
                {
                    Installments = null
                }
            );
        }
    }
}
