﻿using Xunit;

namespace LendFoundry.Loans.Abstractions.Tests
{
    public class FeeTest
    {
        private IFee fixedAmountFee = new Fee
        {
            Code = "origination",
            Amount = 25.0,
            Type = FeeType.Fixed
        };

        private IFee percentFee = new Fee
        {
            Code = "foo",
            Amount = 11.97,
            Type = FeeType.Percent
        };

        [Fact]
        public void ConvertFromFixedToPercent()
        {
            IFee percentFee = fixedAmountFee.To(FeeType.Percent, 3000.0);
            Assert.Equal(0.83, percentFee.Amount);
            Assert.Equal(FeeType.Percent, percentFee.Type);
            Assert.Equal("origination", percentFee.Code);
        }

        [Fact]
        public void ConvertFromPercentToFixed()
        {
            IFee fixedAmountFee = percentFee.To(FeeType.Fixed, 9019.0);
            Assert.Equal(1079.57, fixedAmountFee.Amount);
            Assert.Equal(FeeType.Fixed, fixedAmountFee.Type);
            Assert.Equal("foo", fixedAmountFee.Code);
        }

        [Fact]
        public void ConvertingToSameTypeResultsInSameFee()
        {
            Assert.Same(percentFee, percentFee.To(FeeType.Percent, -1));
            Assert.Same(fixedAmountFee, fixedAmountFee.To(FeeType.Fixed, -1));
        }
    }
}
