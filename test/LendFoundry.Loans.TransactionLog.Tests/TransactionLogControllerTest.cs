﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.TransactionLog.Controllers;
using LendFoundry.Loans.TransactionLog.Data;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Date;
using Xunit;

namespace LendFoundry.Loans.TransactionLog.Tests
{
    public class TransactionLogControllerTest
    {
        private Mock<ITransactionLogRepository> Repository;
        private Mock<ILogger> Logger;
        private Mock<IEventHubClient> EventHub;
        private Mock<ITenantService> TenantService;
        private Mock<ITenantTime> TenantTime;

        private TransactionLogController Controller
        {
            get
            {
                return new TransactionLogController
                    (
                        Logger.Object, 
                        Repository.Object, 
                        TenantService.Object, 
                        EventHub.Object,
                        TenantTime.Object
                    );
            }
        }

        public TransactionLogControllerTest()
        {
            Repository = new Mock<ITransactionLogRepository>();
            Logger = new Mock<ILogger>();
            TenantService = new Mock<ITenantService>();
            EventHub = new Mock<IEventHubClient>();
            TenantTime = new Mock<ITenantTime>();
        }

        [Fact]
        public void AddTransaction_HasInvalidEntryInfo_ReturnBadRequest()
        {         
            // act   
            var result = (ObjectResult)Controller.AddTransaction(null);

            // arrange
            Assert.Equal(400, result.StatusCode);
        }

        [Fact]
        public void AddTransaction_ValidEntryInfo_SaveItAndPublishEventHub_ReturnOk()
        {
            // arrange
            var transactionInfo = new Models.TransactionInfo();            
            TenantService.Setup(s => s.Current).Returns(new TenantInfo { Id = "tn01" });
            TenantTime.Setup(s => s.Now).Returns(new DateTimeOffset(DateTime.Now));
            Repository.Setup(s => s.Save(transactionInfo));
            EventHub.Setup(s => s.Publish("LoanTransactionAdded", transactionInfo));
            
            // act
            var result = (ObjectResult)Controller.AddTransaction(transactionInfo);

            // arrange
            Assert.Equal(200, result.StatusCode);
            Repository.Verify(v => v.Save(transactionInfo));
            TenantTime.Verify(t => t.Now);
            EventHub.Verify(v => v.Publish("LoanTransactionAdded", transactionInfo));
        }

        [Fact]
        public void GetTransactions_HasSearchMatched_ReturnListOfTransactions()
        {
            // arrange
            Repository.Setup(s => s.GetByLoanReference("ln01")).Returns(new List<Models.ITransactionInfo>
            {
                new Models.TransactionInfo()
            });

            // act
            var result = (ObjectResult)Controller.GetTransactions("ln01");

            // assert
            Assert.Equal(200, result.StatusCode);
            Assert.NotEmpty((List<Models.ITransactionInfo>)result.Value);
        }

        [Fact]
        public void GetTransactions_HasUnmatchedSearch_ReturnEmptyList()
        {
            // arrange
            Repository.Setup(s => s.GetByLoanReference("ln01")).Returns(new List<Models.ITransactionInfo>());

            // act
            var result = (ObjectResult)Controller.GetTransactions("ln01");

            // assert
            Assert.Equal(200, result.StatusCode);
            Assert.Empty((List<Models.ITransactionInfo>)result.Value);
        }

        [Fact]
        public void GetTransactionByLoanAndTransaction_HasSearchMatched_ReturnSingleTransaction()
        {
            // arrange
            Repository.Setup(s => s.GetByTransactionId("ln01", "tx01"))
                      .Returns(new Models.TransactionInfo());

            // act
            var result = (ObjectResult)Controller.GetTransactionByLoanAndTransaction("ln01", "tx01");

            // assert
            Assert.Equal(200, result.StatusCode);
            Assert.NotNull(result.Value is Models.TransactionInfo);
        }

        [Fact]
        public void GetTransactionByLoanAndTransaction_HasUnmatchedSearch_ReturnNotFound()
        {
            // arrange
            Repository.Setup(s => s.GetByTransactionId("ln01", "tx01"));

            // act
            var result = (HttpNotFoundResult)Controller.GetTransactionByLoanAndTransaction("ln01", "tx01");

            // assert
            Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public void WhenController_HasNoEventHub_ReturnArgumentException()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                using (new TransactionLogController(Logger.Object, Repository.Object, TenantService.Object, null, TenantTime.Object)) { }
            });
        }

        [Fact]
        public void WhenController_HasNoLogger_ReturnArgumentException()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                using (new TransactionLogController(null, Repository.Object, TenantService.Object, EventHub.Object, TenantTime.Object)) { }
            });
        }

        [Fact]
        public void WhenController_HasNoRepository_ReturnArgumentException()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                using (new TransactionLogController(Logger.Object, null, TenantService.Object, EventHub.Object, TenantTime.Object)) { }
            });
        }

        [Fact]
        public void WhenController_HasNoTenant_ReturnArgumentException()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                using (new TransactionLogController(Logger.Object, Repository.Object, null, EventHub.Object, TenantTime.Object)) { }
            });
        }

        [Fact]
        public void WhenController_HasNoTenantTime_ReturnArgumentException()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                using (new TransactionLogController(Logger.Object, Repository.Object, TenantService.Object, EventHub.Object, null)) { }
            });
        }
    }
}