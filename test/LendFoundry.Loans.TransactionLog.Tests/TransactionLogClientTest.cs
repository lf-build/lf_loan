﻿using LendFoundry.Foundation.Services;
using LendFoundry.Loans.TransactionLog.Client;
using Moq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LendFoundry.Loans.TransactionLog.Tests
{
    public class TransactionLogClientTest
    {
        private Mock<IServiceClient> Service;

        public static TransactionInfo Transaction
        {
            get
            {
                return new TransactionInfo
                {
                    Amount = 2.500,
                    LoanReference = "ln01",
                    Notes = "Notes",
                    Code = "400",
                    Date = DateTimeOffset.Now,
                    Description = "Description"
                };
            }
        }

        public TransactionLogClientTest()
        {
            Service = new Mock<IServiceClient>();
        }

        [Fact]
        public void WhenClient_HasNoServiceClient_ReturnArgumentException()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                new TransactionLogService(null);
            });
        }

        [Fact]
        public void WhenAddTransaction_HasNoTransaction_ReturnArgumentException()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = Mock.Of<IServiceClient>();
                var client = new TransactionLogService(service);
                client.AddTransaction(null);
            });
        }

        [Fact]
        public void WhenAddTransaction_HasInfoOk_ReturnTransactionSavedWithId()
        {
            // arrange
            Service.Setup(s => s.Execute<TransactionInfo>(It.IsAny<RestRequest>()))
                   .Returns(new TransactionInfo { Id = "tx01" });

            // act
            var client = new TransactionLogService(Service.Object);
            var result = client.AddTransaction(Transaction);

            // assert
            Assert.NotNull(result);
            Assert.Equal("tx01", result.Id);
        }

        [Fact]
        public void WhenGetTransactionByLoanAndTransaction_HasNoLoan_ReturnArgumentException()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = Mock.Of<IServiceClient>();
                var client = new TransactionLogService(service);
                client.GetTransactionByLoanAndTransaction(string.Empty, "tx01");
            });
        }

        [Fact]
        public void WheGetTransactionByLoanAndTransaction_HasNoTransaction_ReturnArgumentException()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = Mock.Of<IServiceClient>();
                var client = new TransactionLogService(service);
                client.GetTransactionByLoanAndTransaction("ln01", string.Empty);
            });
        }

        [Fact]
        public void WhenAddTransaction_HasInfoOk_SaveItAndReturnTransctionWithId()
        {
            // arrange
            Service.Setup(s => s.Execute<TransactionInfo>(It.IsAny<RestRequest>()))
                   .Returns(new TransactionInfo
                   {
                       Id = "tx01",
                       LoanReference = "ln01"
                   });

            // act
            var client = new TransactionLogService(Service.Object);
            var result = client.AddTransaction(Transaction);

            // assert
            Assert.NotNull(result);
            Assert.Equal("tx01", result.Id);
            Assert.Equal("ln01", result.LoanReference);
            Service.Verify(v => v.Execute<TransactionInfo>(It.IsAny<RestRequest>()));
        }

        [Fact]
        public void WhenGetTransactions_HasNoLoan_ReturnArgumentException()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var service = Mock.Of<IServiceClient>();
                var client = new TransactionLogService(service);
                client.GetTransactions(string.Empty);
            });
        }

        [Fact]
        public void WhenGetTransactions_HasMatchedSearch_ReturnListOfTransaction()
        {
            // arrange
            Service.Setup(s => s.Execute<List<TransactionInfo>>(It.IsAny<RestRequest>()))
                   .Returns(new List<TransactionInfo>
                   {
                       new TransactionInfo { LoanReference = "ln01" },
                       new TransactionInfo { LoanReference = "ln01" },
                       new TransactionInfo { LoanReference = "ln02" },
                       new TransactionInfo { LoanReference = "ln02" },
                   });

            // act
            var client = new TransactionLogService(Service.Object);
            var result = client.GetTransactions("ln01");

            // assert
            Assert.NotNull(result);
            Assert.NotEmpty(result);            
            Assert.True(result.Count(d => d.LoanReference == "ln01") == 2);
            Assert.True(result.Count(d => d.LoanReference == "ln02") == 2);
        }
    }
}
