﻿using LendFoundry.Foundation.Date;
using System;

namespace LendFoundry.Loans.Schedule.Tests
{
    public class StaticTodayTenantTime : ITenantTime
    {
        private readonly ITenantTime tenantTime;
        private DateTimeOffset? today;

        public StaticTodayTenantTime(ITenantTime tenantTime)
        {
            this.tenantTime = tenantTime;
        }

        public DateTimeOffset Create(int year, int month, int day)
        {
            return tenantTime.Create(year, month, day);
        }

        public DateTimeOffset FromDate(DateTime dateTime)
        {
            return tenantTime.FromDate(dateTime);
        }

        public DateTimeOffset Convert(DateTimeOffset dateTime)
        {
            return tenantTime.Convert(dateTime);
        }

        public DateTimeOffset Now => Today;

        public DateTimeOffset Today
        {
            get
            {
                if (today == null)
                    throw new Exception("Today's value is not set");

                return FromDate(today.Value.Date);
            }
            set
            {
                today = value;
            }
        }

        public TimeZoneInfo TimeZone => TimeZoneInfo.Utc;
    }
}
