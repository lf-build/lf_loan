﻿using System;
using Xunit;

namespace LendFoundry.Loans.Schedule.Tests
{
    public class ConfirmPaymentTest : PaymentTestBase
    {
        [Fact]
        public void StatusIsChangedToCompleted()
        {
            Schedule = @"
                DueDate    | AnniversaryDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Status    | Type      | PaymentMethod | PaymentDate | PaymentId 
                2015-01-15 | 2015-01-14      |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | Scheduled | Scheduled | ACH           | 2015-01-15  | PMT001    
                2015-02-15 | 2015-02-14      |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | Scheduled | Scheduled | ACH           |             |           
                2015-03-15 | 2015-03-14      |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | Scheduled | Scheduled | ACH           |             |           ";

            Service.ConfirmPayment
            (
                loanReferenceNumber: "loan001",
                paymentId: "PMT001",
                date: TenantTime.Create(2015, 1, 15)
            );

            AssertSchedule(@"
                DueDate    | AnniversaryDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Status    | Type      | PaymentMethod | PaymentDate | PaymentId 
                2015-01-15 | 2015-01-14      |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | Completed | Scheduled | ACH           | 2015-01-15  | PMT001    
                2015-02-15 | 2015-02-14      |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | Scheduled | Scheduled | ACH           |             |           
                2015-03-15 | 2015-03-14      |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | Scheduled | Scheduled | ACH           |             |           "
            );
        }

        [Fact]
        public void PaymentDateGetsUpdated()
        {
            Schedule = @"
                DueDate    | AnniversaryDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Status    | Type      | PaymentMethod | PaymentDate | PaymentId 
                2015-01-15 | 2015-01-14      |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | Scheduled | Scheduled | ACH           | 2015-01-15  | PMT001    
                2015-02-15 | 2015-02-14      |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | Scheduled | Scheduled | ACH           |             |           
                2015-03-15 | 2015-03-14      |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | Scheduled | Scheduled | ACH           |             |           ";

            Service.ConfirmPayment
            (
                loanReferenceNumber: "loan001",
                paymentId: "PMT001",
                date: TenantTime.Create(2015, 1, 20)
            );

            AssertSchedule(@"
                DueDate    | AnniversaryDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Status    | Type      | PaymentMethod | PaymentDate | PaymentId 
                2015-01-15 | 2015-01-14      |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | Completed | Scheduled | ACH           | 2015-01-20  | PMT001    
                2015-02-15 | 2015-02-14      |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | Scheduled | Scheduled | ACH           |             |           
                2015-03-15 | 2015-03-14      |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | Scheduled | Scheduled | ACH           |             |           "
            );
        }

        [Fact]
        public void PaymentDateIsCorrectlyConvertedToTenantTime()
        {
            Schedule = @"
                DueDate    | AnniversaryDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Status    | Type      | PaymentMethod | PaymentDate | PaymentId 
                2015-01-15 | 2015-01-14      |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | Scheduled | Scheduled | ACH           | 2015-01-15  | PMT001    
                2015-02-15 | 2015-02-14      |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | Scheduled | Scheduled | ACH           |             |           
                2015-03-15 | 2015-03-14      |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | Scheduled | Scheduled | ACH           |             |           ";

            Service.ConfirmPayment
            (
                loanReferenceNumber: "loan001",
                paymentId: "PMT001",
                date: new DateTimeOffset(2015, 1, 20, 0, 0, 0, new TimeSpan(5, 0, 0))
            );

            AssertSchedule(@"
                DueDate    | AnniversaryDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Status    | Type      | PaymentMethod | PaymentDate | PaymentId 
                2015-01-15 | 2015-01-14      |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | Completed | Scheduled | ACH           | 2015-01-20  | PMT001    
                2015-02-15 | 2015-02-14      |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | Scheduled | Scheduled | ACH           |             |           
                2015-03-15 | 2015-03-14      |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | Scheduled | Scheduled | ACH           |             |           "
            );
        }

        [Fact(Skip = "TODO")]
        public void LatePaymentOfFirstInstallment()
        {
        }

        [Fact(Skip = "TODO")]
        public void LatePaymentOfThirdInstallment()
        {
        }

        [Fact(Skip = "TODO")]
        public void LatePaymentOfLastInstallment()
        {
        }

        [Fact(Skip = "TODO")]
        public void LatePaymentOfTwoFirstInstallments()
        {
        }

        [Fact(Skip = "TODO")]
        public void LatePaymentOfThreeInstallments()
        {
        }

        [Fact(Skip = "TODO")]
        public void LatePaymentOfLastTwoInstallments()
        {
        }
    }
}
