﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Testing;
using LendFoundry.Foundation.Testing.Mongo;
using LendFoundry.Loans.Schedule.Data;
using LendFoundry.Tenant.Client;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LendFoundry.Loans.Schedule.Tests.Data
{
    public class PaymentScheduleRepositoryTest
    {
        private TenantInfo tenantInfo;
        private Mock<ITenantService> tenantServiceMock;

        private IPaymentSchedule PaymentScheduleFake
        {
            get
            {
                return new PaymentSchedule
                {
                    LoanReferenceNumber = string.Empty,
                    Installments = new List<IInstallment>()
                };
            }
        }

        private IInstallment InstallmentFake
        {
            get
            {
                return new Installment
                {
                    DueDate = new DateTime(2015, 2, 22),
                    OpeningBalance = 3000,
                    PaymentAmount = 1000,
                    Interest = 30,
                    Principal = 900,
                    EndingBalance = 2000,
                    Type = InstallmentType.Additional,
                    Status = InstallmentStatus.Completed,
                    PaymentMethod = PaymentMethod.Check,
                    PaymentDate = DateTime.Now,
                    PaymentId = "PaymentId001"
                };
            }
        }

        public PaymentScheduleRepositoryTest()
        {
            tenantInfo = new TenantInfo() { Id = "my-tenant" };

            tenantServiceMock = new Mock<ITenantService>();
            tenantServiceMock.Setup(tenantService => tenantService.Current)
                             .Returns(tenantInfo);
        }

        private IPaymentScheduleRepository Repository(IMongoConfiguration configuration)
        {
            return new PaymentScheduleRepository
            (
                configuration: configuration,
                tenantService: tenantServiceMock.Object
            );
        }

        [MongoFact]
        public void CreatePaymentSchedule()
        {
            MongoTest.Run((config) =>
            {
                // arrange
                var loanReferenceNumber = "loan001";
                var paymentSchedule = PaymentScheduleFake;
                paymentSchedule.LoanReferenceNumber = loanReferenceNumber;
                paymentSchedule.Installments = GetListInstallment();
                var repository = Repository(config);

                // act
                repository.Create(paymentSchedule);

                var schedule = repository.Get(loanReferenceNumber);

                Assert.NotNull(schedule);
                Assert.Equal(loanReferenceNumber, schedule.LoanReferenceNumber);

                var installments = schedule.Installments.OrderBy(installment => installment.DueDate).ToList();
                Assert.Equal(2, installments.Count);

                var installment1 = installments[0];
                var installment2 = installments[1];

                Assert.Equal(new DateTime(2016, 2, 21), installment1.DueDate);
                Assert.Equal(5000, installment1.OpeningBalance);
                Assert.Equal(1694.52, installment1.PaymentAmount);
                Assert.Equal(41.67, installment1.Interest);
                Assert.Equal(1652.85, installment1.Principal);
                Assert.Equal(3347.15, installment1.EndingBalance);
                Assert.Equal(InstallmentType.Scheduled, installment1.Type);
                Assert.Equal(InstallmentStatus.Scheduled, installment1.Status);
                Assert.Equal(PaymentMethod.ACH, installment1.PaymentMethod);
                Assert.NotNull(installment1.PaymentId);

                Assert.Equal(new DateTime(2016, 2, 22), installment2.DueDate);
                Assert.Equal(3000, installment2.OpeningBalance);
                Assert.Equal(1000, installment2.PaymentAmount);
                Assert.Equal(30, installment2.Interest);
                Assert.Equal(900, installment2.Principal);
                Assert.Equal(2000, installment2.EndingBalance);
                Assert.Equal(InstallmentType.Additional, installment2.Type);
                Assert.Equal(InstallmentStatus.Completed, installment2.Status);
                Assert.Equal(PaymentMethod.Check, installment2.PaymentMethod);
                Assert.Equal("PaymentId002", installment2.PaymentId);

            });
        }

        [MongoFact]
        public void DeletePaymentSchedule()
        {
            MongoTest.Run((config) =>
            {
                // arrange
                var loanReferenceNumber = "loan001";
                var paymentSchedule = PaymentScheduleFake;
                paymentSchedule.LoanReferenceNumber = loanReferenceNumber;
                paymentSchedule.Installments = new List<IInstallment> { InstallmentFake };
                var repository = Repository(config);

                repository.Create(new PaymentSchedule
                {
                    LoanReferenceNumber = "loan001",
                    Installments = new List<IInstallment> { InstallmentFake }
                });

                repository.Create(new PaymentSchedule
                {
                    LoanReferenceNumber = "loan002",
                    Installments = new List<IInstallment> { InstallmentFake }
                });

                repository.Create(new PaymentSchedule
                {
                    LoanReferenceNumber = "loan003",
                    Installments = new List<IInstallment> { InstallmentFake }
                });

                repository.Delete("loan002").Wait(TimeSpan.FromMilliseconds(500));
                Assert.Null(repository.Get("loan002"));
                Assert.NotNull(repository.Get("loan001"));
                Assert.NotNull(repository.Get("loan003"));
            });
        }

        [MongoFact]
        public void GetLoansByStatusAndDueDate()
        {
            MongoTest.Run((config) =>
            {
                var repository = Repository(config);

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                //CREATE THREE SCHEDULES
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                repository.Create(new PaymentSchedule
                {
                    LoanReferenceNumber = "loan001",
                    Installments = new List<IInstallment>
                    {
                        new Installment { DueDate = new DateTime(2015, 1, 15), PaymentDate = DateTime.Now },
                        new Installment { DueDate = new DateTime(2015, 2, 15), PaymentDate = DateTime.Now },
                        new Installment { DueDate = new DateTime(2015, 3, 15), PaymentDate = DateTime.Now }
                    }
                });

                repository.Create(new PaymentSchedule
                {
                    LoanReferenceNumber = "loan002",
                    Installments = new List<IInstallment>
                    {
                        new Installment { DueDate = new DateTime(2015, 1, 20), PaymentDate = DateTime.Now },
                        new Installment { DueDate = new DateTime(2015, 2, 20), PaymentDate = DateTime.Now },
                        new Installment { DueDate = new DateTime(2015, 3, 20), PaymentDate = DateTime.Now }
                    }
                });

                repository.Create(new PaymentSchedule
                {
                    LoanReferenceNumber = "loan003",
                    Installments = new List<IInstallment>
                    {
                        new Installment { DueDate = new DateTime(2015, 1, 25), PaymentDate = DateTime.Now },
                        new Installment { DueDate = new DateTime(2015, 2, 25), PaymentDate = DateTime.Now },
                        new Installment { DueDate = new DateTime(2015, 3, 25), PaymentDate = DateTime.Now }
                    }
                });


                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                //GET LOANS WITH DUE DATES FROM 3 / 15 UP TO 3 / 24
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                var loanRefNumbers = repository.GetLoans
                (
                    installmentStatus: InstallmentStatus.Scheduled,
                    minDueDate: new DateTime(2015, 3, 15),
                    maxDueDate: new DateTime(2015, 3, 24)
                ).ToList();

                Assert.NotNull(loanRefNumbers);
                Assert.Equal(2, loanRefNumbers.Count());
                Assert.True(loanRefNumbers.Contains("loan001"));
                Assert.True(loanRefNumbers.Contains("loan002"));


                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                //GET LOANS WITH DUE DATES FROM 1 / 16 UP TO 1 / 25
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                loanRefNumbers = null;
                loanRefNumbers = repository.GetLoans
                (
                    installmentStatus: InstallmentStatus.Scheduled,
                    minDueDate: new DateTime(2015, 1, 16),
                    maxDueDate: new DateTime(2015, 1, 25)
                ).ToList();

                Assert.NotNull(loanRefNumbers);
                Assert.Equal(2, loanRefNumbers.Count());
                Assert.True(loanRefNumbers.Contains("loan002"));
                Assert.True(loanRefNumbers.Contains("loan003"));
            });
        }

        [MongoFact]
        public void GetLoansByStatusAndDueDateAndPaymentMethod()
        {
            MongoTest.Run((config) =>
            {
                var repository = Repository(config);

                repository.Create(new PaymentSchedule
                {
                    LoanReferenceNumber = "loanRef001",
                    Installments = new List<IInstallment>
                    {
                        new Installment { DueDate = new DateTime(2015, 1, 15), PaymentMethod = PaymentMethod.ACH, Status = InstallmentStatus.Completed },
                        new Installment { DueDate = new DateTime(2015, 2, 15), PaymentMethod = PaymentMethod.ACH, Status = InstallmentStatus.Completed },
                        new Installment { DueDate = new DateTime(2015, 3, 15), PaymentMethod = PaymentMethod.ACH, Status = InstallmentStatus.Scheduled }
                    }
                });

                repository.Create(new PaymentSchedule
                {
                    LoanReferenceNumber = "loanRef002",
                    Installments = new List<IInstallment>
                    {
                        new Installment { DueDate = new DateTime(2015, 1, 20), PaymentMethod = PaymentMethod.Check, Status = InstallmentStatus.Completed },
                        new Installment { DueDate = new DateTime(2015, 2, 20), PaymentMethod = PaymentMethod.Check, Status = InstallmentStatus.Completed },
                        new Installment { DueDate = new DateTime(2015, 3, 20), PaymentMethod = PaymentMethod.Check, Status = InstallmentStatus.Scheduled }
                    }
                });

                repository.Create(new PaymentSchedule
                {
                    LoanReferenceNumber = "loanRef003",
                    Installments = new List<IInstallment>
                    {
                        new Installment { DueDate = new DateTime(2015, 1, 22), PaymentMethod = PaymentMethod.Check, Status = InstallmentStatus.Completed },
                        new Installment { DueDate = new DateTime(2015, 2, 22), PaymentMethod = PaymentMethod.ACH, Status = InstallmentStatus.Scheduled },
                        new Installment { DueDate = new DateTime(2015, 3, 22), PaymentMethod = PaymentMethod.Check, Status = InstallmentStatus.Scheduled }
                    }
                });

                repository.Create(new PaymentSchedule
                {
                    LoanReferenceNumber = "loanRef004",
                    Installments = new List<IInstallment>
                    {
                        new Installment { DueDate = new DateTime(2015, 1, 25), PaymentMethod = PaymentMethod.Check, Status = InstallmentStatus.Completed },
                        new Installment { DueDate = new DateTime(2015, 2, 25), PaymentMethod = PaymentMethod.ACH, Status = InstallmentStatus.Completed },
                        new Installment { DueDate = new DateTime(2015, 3, 25), PaymentMethod = PaymentMethod.Check, Status = InstallmentStatus.Scheduled }
                    }
                });


                var loanRefNumbers = repository.GetLoans
                (
                    installmentStatus: InstallmentStatus.Completed,
                    minDueDate: new DateTime(2015, 2, 15),
                    maxDueDate: new DateTime(2015, 2, 25),
                    paymentMethod: PaymentMethod.ACH
                ).ToList();

                Assert.NotNull(loanRefNumbers);
                Assert.Equal(2, loanRefNumbers.Count());
                Assert.True(loanRefNumbers.Contains("loanRef001"));
                Assert.True(loanRefNumbers.Contains("loanRef004"));

                loanRefNumbers = null;
                loanRefNumbers = repository.GetLoans
                (
                    installmentStatus: InstallmentStatus.Completed,
                    minDueDate: new DateTime(2015, 2, 15),
                    maxDueDate: new DateTime(2015, 2, 25),
                    paymentMethod: PaymentMethod.Check
                ).ToList();

                Assert.NotNull(loanRefNumbers);
                Assert.Equal(1, loanRefNumbers.Count());
                Assert.True(loanRefNumbers.Contains("loanRef002"));

                loanRefNumbers = null;
                loanRefNumbers = repository.GetLoans
                (
                    installmentStatus: InstallmentStatus.Scheduled,
                    minDueDate: new DateTime(2015, 2, 15),
                    maxDueDate: new DateTime(2015, 2, 25),
                    paymentMethod: PaymentMethod.ACH
                ).ToList();

                Assert.NotNull(loanRefNumbers);
                Assert.Equal(1, loanRefNumbers.Count());
                Assert.True(loanRefNumbers.Contains("loanRef003"));
            });
        }

        [MongoFact]
        public void MarkInstallmentAsCompleted()
        {
            MongoTest.Run((config) =>
            {
                var loanReferenceNumber = "loanReferenceNumber-001";
                var repository = Repository(config);

                repository.Create(new PaymentSchedule
                {
                    LoanReferenceNumber = loanReferenceNumber,
                    Installments = new List<IInstallment>
                    {
                        new Installment { PaymentId = "paymentId001", PaymentDate = new DateTime(2015, 1, 15), DueDate = new DateTime(2015, 1, 15), Status = InstallmentStatus.Scheduled },
                        new Installment { PaymentId = "paymentId002", PaymentDate = new DateTime(2015, 2, 15), DueDate = new DateTime(2015, 2, 15), Status = InstallmentStatus.Scheduled },
                        new Installment { PaymentId = "paymentId003", PaymentDate = null, DueDate = new DateTime(2015, 3, 15), Status = InstallmentStatus.Scheduled }
                    }
                });

                repository.MarkInstallmentAsCompleted(
                    loanReferenceNumber: loanReferenceNumber,
                    paymentId: "paymentId002",
                    paymentDate: new DateTime(2015, 2, 18)
                );

                var schedule = repository.Get(loanReferenceNumber);

                var installments = schedule.Installments.OrderBy(installment => installment.DueDate).ToList();
                Assert.Equal(3, installments.Count);

                var installment1 = installments[0];
                var installment2 = installments[1];
                var installment3 = installments[2];

                Assert.Equal(new DateTime(2015, 1, 15), installment1.DueDate);
                Assert.Equal(new DateTime(2015, 1, 15), installment1.PaymentDate);
                Assert.Equal("paymentId001", installment1.PaymentId);

                Assert.Equal(new DateTime(2015, 2, 15), installment2.DueDate);
                Assert.Equal(new DateTime(2015, 2, 18), installment2.PaymentDate);
                Assert.Equal("paymentId002", installment2.PaymentId);

                Assert.Equal(new DateTime(2015, 3, 15), installment3.DueDate);
                Assert.Equal(null, installment3.PaymentDate);
                Assert.Equal("paymentId003", installment3.PaymentId);
            });
        }

        [MongoFact]
        public void GetLoansByDueIn()
        {
            MongoTest.Run((config) =>
            {
                var repository = Repository(config);

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                //CREATE THREE SCHEDULES
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                repository.Create(new PaymentSchedule
                {
                    LoanReferenceNumber = "loan001",
                    Installments = new List<IInstallment>
                    {
                        new Installment { DueDate = new DateTime(2015, 1, 15), PaymentDate = DateTimeOffset.Now },
                        new Installment { DueDate = new DateTime(2015, 1, 15), PaymentDate = DateTimeOffset.Now },
                        new Installment { DueDate = new DateTime(2015, 1, 15), PaymentDate = DateTimeOffset.Now }
                    }
                });

                repository.Create(new PaymentSchedule
                {
                    LoanReferenceNumber = "loan002",
                    Installments = new List<IInstallment>
                    {
                        new Installment { DueDate = new DateTime(2015, 1, 20), PaymentDate = DateTimeOffset.Now },
                        new Installment { DueDate = new DateTime(2015, 1, 20), PaymentDate = DateTimeOffset.Now },
                        new Installment { DueDate = new DateTime(2015, 1, 20), PaymentDate = DateTimeOffset.Now }
                    }
                });

                repository.Create(new PaymentSchedule
                {
                    LoanReferenceNumber = "loan003",
                    Installments = new List<IInstallment>
                    {
                        new Installment { DueDate = new DateTime(2015, 1, 15), PaymentDate = DateTimeOffset.Now },
                        new Installment { DueDate = new DateTime(2015, 1, 15), PaymentDate = DateTimeOffset.Now },
                        new Installment { DueDate = new DateTime(2015, 1, 15), PaymentDate = DateTimeOffset.Now }
                    }
                });

                var loanRefNumbers = repository.GetLoansDueIn
                (
                    dueDate: new DateTime(2015, 1, 15)
                ).ToList();

                Assert.NotNull(loanRefNumbers);
                Assert.Equal(2, loanRefNumbers.Count());
                Assert.True(loanRefNumbers.Contains("loan001"));
                Assert.True(loanRefNumbers.Contains("loan003"));
            });
        }

        [MongoFact]
        public void GetInstallmentsDueIn()
        {
            MongoTest.Run((config) =>
            {
                var repository = Repository(config);

                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                //CREATE THREE SCHEDULES
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                repository.Create(new PaymentSchedule
                {
                    LoanReferenceNumber = "loan001",
                    Installments = new List<IInstallment>
                    {
                        new Installment
                        {
                            DueDate = new DateTime(2015, 2, 22),
                            OpeningBalance = 3000,
                            PaymentAmount = 1000,
                            Interest = 30,
                            Principal = 900,
                            EndingBalance = 2000,
                            Type = InstallmentType.Additional,
                            Status = InstallmentStatus.Scheduled,
                            PaymentMethod = PaymentMethod.Check,
                            PaymentDate = DateTime.Now,
                            PaymentId = "PaymentId001"
                        },
                    }
                });

                repository.Create(new PaymentSchedule
                {
                    LoanReferenceNumber = "loan002",
                    Installments = new List<IInstallment>
                    {
                        new Installment { DueDate = new DateTime(2015, 1, 20), PaymentDate = DateTimeOffset.Now },
                        new Installment { DueDate = new DateTime(2015, 1, 20), PaymentDate = DateTimeOffset.Now },
                        new Installment { DueDate = new DateTime(2015, 1, 20), PaymentDate = DateTimeOffset.Now }
                    }
                });

                repository.Create(new PaymentSchedule
                {
                    LoanReferenceNumber = "loan003",
                    Installments = new List<IInstallment>
                    {
                        new Installment
                        {
                            DueDate = new DateTime(2015, 2, 22),
                            OpeningBalance = 3000,
                            PaymentAmount = 1000,
                            Interest = 30,
                            Principal = 900,
                            EndingBalance = 2000,
                            Type = InstallmentType.Additional,
                            Status = InstallmentStatus.Scheduled,
                            PaymentMethod = PaymentMethod.Check,
                            PaymentDate = DateTime.Now,
                            PaymentId = "PaymentId001"
                        },
                    }
                });

                var paymentSchedules = repository.GetInstallmentsDueIn
                (
                    dueDate: new DateTime(2015, 2, 22),
                    paymentMethod: PaymentMethod.Check
                ).ToList();

                Assert.NotNull(paymentSchedules);
                Assert.Equal(2, paymentSchedules.Count());
                Assert.True(paymentSchedules.All(i => i.FirstInstallment.PaymentMethod == PaymentMethod.Check));
            });
        }

        [MongoFact]
        public void UpdateInstallmentStatus()
        {
            MongoTest.Run((config) =>
            {
                var loanReferenceNumber = "loan001";
                var repository = Repository(config);

                repository.Create(new PaymentSchedule
                {
                    LoanReferenceNumber = loanReferenceNumber,
                    Installments = new List<IInstallment>
                    {
                        new Installment
                        {
                            DueDate = new DateTime(2015, 2, 22),
                            OpeningBalance = 3000,
                            PaymentAmount = 1000,
                            Interest = 30,
                            Principal = 900,
                            EndingBalance = 2000,
                            Type = InstallmentType.Additional,
                            Status = InstallmentStatus.Scheduled,
                            PaymentMethod = PaymentMethod.Check,
                            PaymentDate = DateTime.Now,
                            PaymentId = "PaymentId001"
                        },
                    }
                });

                repository.UpdateInstallmentStatus(loanReferenceNumber, new DateTime(2015, 2, 22), InstallmentStatus.Completed);

                var scheduleUpdated = repository.Get(loanReferenceNumber);

                Assert.NotNull(scheduleUpdated);
                Assert.Equal(loanReferenceNumber, scheduleUpdated.LoanReferenceNumber);
                Assert.Equal(new DateTime(2015, 2, 22), scheduleUpdated.FirstInstallment.DueDate);
                Assert.Equal(InstallmentStatus.Completed, scheduleUpdated.FirstInstallment.Status);
            });
        }

        [MongoFact]
        public void AssignPaymentToInstallment()
        {
            MongoTest.Run((config) =>
            {
                var loanReferenceNumber = "loan001";
                var repository = Repository(config);

                repository.Create(new PaymentSchedule
                {
                    LoanReferenceNumber = loanReferenceNumber,
                    Installments = new List<IInstallment>
                    {
                        new Installment
                        {
                            DueDate = new DateTime(2015, 2, 22),
                            OpeningBalance = 3000,
                            PaymentAmount = 1000,
                            Interest = 30,
                            Principal = 900,
                            EndingBalance = 2000,
                            Type = InstallmentType.Additional,
                            Status = InstallmentStatus.Scheduled,
                            PaymentMethod = PaymentMethod.Check,
                            PaymentDate = DateTimeOffset.Now,
                            PaymentId = "PaymentId001"
                        },
                    }
                });

                repository.AssignPaymentToInstallment(
                    loanReferenceNumber: loanReferenceNumber,
                    dueDate: new DateTime(2015, 2, 22),
                    paymentId: "PaymentIdAssigned",
                    paymentDate: new DateTime(2015, 2, 22));

                var scheduleAssigned = repository.Get(loanReferenceNumber);

                Assert.NotNull(scheduleAssigned);
                Assert.Equal(loanReferenceNumber, scheduleAssigned.LoanReferenceNumber);
                Assert.Equal(new DateTime(2015, 2, 22), scheduleAssigned.FirstInstallment.DueDate);
                Assert.Equal("PaymentIdAssigned", scheduleAssigned.FirstInstallment.PaymentId);
            });
        }

        [MongoFact]
        public void PaymentSchedulesAreCreatedForTheRightTenants()
        {
            MongoTest.Run((config) =>
            {
                var repository = Repository(config);

                tenantInfo.Id = "tenantA";
                repository.Create(AnyValidSchedule("loan001"));

                tenantInfo.Id = "tenantB";
                repository.Create(AnyValidSchedule("loan002"));

                tenantInfo.Id = "tenantA";
                repository.Create(AnyValidSchedule("loan003"));

                tenantInfo.Id = "tenantB";
                Assert.NotNull(repository.Get("loan002"));
                Assert.Null(repository.Get("loan001"));
                Assert.Null(repository.Get("loan003"));

                tenantInfo.Id = "tenantA";
                Assert.NotNull(repository.Get("loan001"));
                Assert.NotNull(repository.Get("loan003"));
                Assert.Null(repository.Get("loan002"));
            });
        }

        [MongoFact]
        public void WhenCreateSchedule_HasNoPaymentSchedule_ReturnArgumentException()
        {
            MongoTest.Run((config) =>
            {
                var repository = Repository(config);

                AssertException.Throws<ArgumentException>("Payment Schedule must be informed", () =>
                    repository.Create(It.IsAny<PaymentSchedule>())
                );
            });
        }

        [MongoFact]
        public void WhenCreateSchedule_HasNoLoan_ReturnArgumentException()
        {
            MongoTest.Run((config) =>
            {
                var repository = Repository(config);

                AssertException.Throws<ArgumentException>("Loan Reference must be informed", () =>
                    repository.Create(new PaymentSchedule
                    {
                        LoanReferenceNumber = string.Empty
                    })
                );
            });
        }

        [MongoFact]
        public void WhenCreateSchedule_HasNoInstallment_ReturnArgumentException()
        {
            MongoTest.Run((config) =>
            {
                var repository = Repository(config);

                AssertException.Throws<ArgumentException>("At least one Installment must be informed", () =>
                {
                    var paymentScheduleMock = new Mock<IPaymentSchedule>();

                    paymentScheduleMock.Setup(schedule => schedule.LoanReferenceNumber).Returns("123456789");
                    paymentScheduleMock.Setup(schedule => schedule.Installments).Returns((IList<IInstallment>)null);

                    repository.Create(paymentScheduleMock.Object);
                });
            });
        }

        [MongoFact]
        public void WhenDeleteSchedule_HasNoLoan_ReturnArgumentException()
        {
            MongoTest.Run(async (config) =>
            {
                var repository = Repository(config);

                await Assert.ThrowsAsync<ArgumentNullException>(() => repository.Delete(string.Empty));
            });
        }

        [MongoFact]
        public void WhenGetSchedule_HasNoLoan_ReturnArgumentException()
        {
            MongoTest.Run((config) =>
            {
                var repository = Repository(config);

                Assert.Throws<ArgumentNullException>(() => repository.Get(string.Empty));
            });
        }

        [MongoFact]
        public void WhenMarkInstallmentAsCompleted_HasLoanInvalid_ReturnException()
        {
            MongoTest.Run((config) =>
            {
                var repository = Repository(config);

                Assert.Throws<Exception>(() =>
                {
                    repository.MarkInstallmentAsCompleted(
                        loanReferenceNumber: "nonono",
                        paymentId: "paymentId002",
                        paymentDate: new DateTime(2015, 2, 18)
                    );
                });
            });
        }

        private static PaymentSchedule AnyValidSchedule(string loanReferenceNumber)
        {
            return new PaymentSchedule
            {
                LoanReferenceNumber = loanReferenceNumber,
                Installments = new List<IInstallment>
                {
                    new Installment { DueDate = DateTime.Today }
                }
            };
        }

        private List<IInstallment> GetListInstallment()
        {
            return new List<IInstallment>
            {
                new Installment
                {
                    DueDate = new DateTime(2016, 2, 21),
                    OpeningBalance = 5000,
                    PaymentAmount = 1694.52,
                    Interest = 41.67,
                    Principal = 1652.85,
                    EndingBalance = 3347.15,
                    Type = InstallmentType.Scheduled,
                    Status = InstallmentStatus.Scheduled,
                    PaymentMethod = PaymentMethod.ACH,
                    PaymentDate = DateTime.Now,
                    PaymentId = "PaymentId001"
                },
                new Installment
                {
                    DueDate = new DateTime(2016, 2, 22),
                    OpeningBalance = 3000,
                    PaymentAmount = 1000,
                    Interest = 30,
                    Principal = 900,
                    EndingBalance = 2000,
                    Type = InstallmentType.Additional,
                    Status = InstallmentStatus.Completed,
                    PaymentMethod = PaymentMethod.Check,
                    PaymentDate = DateTime.Now,
                    PaymentId = "PaymentId002"
                }
            };
        }
    }
}