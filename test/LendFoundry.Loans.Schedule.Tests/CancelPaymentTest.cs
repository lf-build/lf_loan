﻿using LendFoundry.Amortization;
using LendFoundry.Amortization.Interest;
using LendFoundry.Foundation.Testing;
using Xunit;

namespace LendFoundry.Loans.Schedule.Tests
{
    public class CancelPaymentTest : PaymentTestBase
    {
        [Fact]
        public void ThrowExceptionWhenPaymentIsNotFound()
        {
            AssertException.Throws<InstallmentNotFoundException>("Installment not found with payment id PMT999", () =>
            {
                Schedule = @"
                    DueDate    | AnniversaryDate | PaymentDate | PaymentId  | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Status    | Type       | PaymentMethod
                    2015-01-15 | 2015-01-14      | 2015-01-15  | PMT001     |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | Completed | Scheduled  | ACH          
                    2015-02-15 | 2015-02-14      | 2015-02-15  | PMT002     |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | Completed | Scheduled  | ACH          
                    2015-03-01 |                 | 2015-03-01  | PMT003     |        6068.05 |       1000.00 |    32.42 |    967.58 |       5100.47 | Scheduled | Additional | ACH          
                    2015-03-15 | 2015-03-14      |             |            |        5100.47 |       1560.68 |    27.25 |   1533.43 |       3567.04 | Scheduled | Scheduled  | ACH          
                    2015-04-15 | 2015-04-14      |             |            |        3567.04 |       1560.68 |    40.84 |   1519.84 |       2047.20 | Scheduled | Scheduled  | ACH          
                    2015-05-15 | 2015-05-14      |             |            |        2047.20 |       1560.68 |    23.44 |   1537.24 |        509.96 | Scheduled | Scheduled  | ACH          
                    2015-06-15 | 2015-06-14      |             |            |         509.96 |        515.80 |     5.84 |    509.96 |          0.00 | Scheduled | Scheduled  | ACH          ";

                Service.CancelPayment("loan001", "PMT999");
            });
        }

        [Fact]
        public void CancelFirstScheduledPayment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Scheduled |        9000.00 |        999.99 |   103.05 |    896.94 |       8103.06
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        8103.06 |       1560.68 |    92.78 |   1467.90 |       6635.16
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6635.16 |       1560.68 |    75.97 |   1484.71 |       5150.45
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        5150.45 |       1560.68 |    58.97 |   1501.71 |       3648.74
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3648.74 |       1560.68 |    41.78 |   1518.90 |       2129.84
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        2129.84 |       2154.23 |    24.39 |   2129.84 |          0.00";

            AmortizationService.OnAmortize(new AmortizationRequest())
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |        9000.00 | 1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |        7542.37 | 1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |        6068.05 | 1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt001",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00"
            );
        }

        [Fact]
        public void CancelThirdScheduledPayment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled | Scheduled |        6068.05 |        999.99 |    69.48 |    930.51 |       5137.54
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        5137.54 |       1560.68 |    58.82 |   1501.86 |       3635.68
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3635.68 |       1560.68 |    41.63 |   1519.05 |       2116.63
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        2116.63 |       2140.87 |    24.24 |   2116.63 |          0.00";

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 6068.05,
                ReferenceDate = Date(2015, 2, 17)
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-03-15      | 2015-03-17 |        6068.05 | 1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt003",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00"
            );
        }

        [Fact]
        public void CancelLastScheduledPayment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 | 2015-04-17  | pmt004    | Scheduled | Completed |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 | 2015-05-17  | pmt005    | Scheduled | Completed |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 | 2015-06-17  | pmt006    | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00";

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 1543.03,
                ReferenceDate = Date(2015, 5, 17)
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt006",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 | 2015-04-17  | pmt004    | Scheduled | Completed |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 | 2015-05-17  | pmt005    | Scheduled | Completed |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00"
            );
        }

        [Fact]
        public void CancelFirstCompletedInstallment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00";

            AmortizationService.OnAmortize(new AmortizationRequest())
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |        9000.00 | 1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |        7542.37 | 1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |        6068.05 | 1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt001",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00"
            );
        }

        [Fact]
        public void CancelThirdCompletedInstallment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00";

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 6068.05,
                ReferenceDate = Date(2015, 2, 17)
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-03-15      | 2015-03-17 |        6068.05 | 1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt003",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00"
            );
        }

        [Fact]
        public void CancelLastCompletedInstallment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 | 2015-04-17  | pmt004    | Scheduled | Completed |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 | 2015-05-17  | pmt005    | Scheduled | Completed |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 | 2015-06-17  | pmt006    | Scheduled | Completed |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00";

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 1543.03,
                ReferenceDate = Date(2015, 5, 17)
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt006",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 | 2015-04-17  | pmt004    | Scheduled | Completed |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 | 2015-05-17  | pmt005    | Scheduled | Completed |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00"
            );
        }

        [Fact]
        public void CancelExtraPaymentBeforeFirstInstallment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                                | 2014-12-30 | 2014-12-30  | pmt001    | Additional | Scheduled |        9000.00 |        111.11 |     0.00 |    111.11 |       8888.89
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        8888.89 |       1560.68 |   102.41 |   1458.27 |       7430.62
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        7430.62 |       1560.68 |    85.08 |   1475.60 |       5955.02
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        5955.02 |       1560.68 |    68.19 |   1492.49 |       4462.53
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4462.53 |       1560.68 |    51.10 |   1509.58 |       2952.95
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2952.95 |       1560.68 |    33.81 |   1526.87 |       1426.08
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1426.08 |       1442.41 |    16.33 |   1426.08 |          0.00";

            AmortizationService.OnAmortize(new AmortizationRequest())
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |        9000.00 | 1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |        7542.37 | 1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |        6068.05 | 1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt001",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00"
            );
        }

        [Fact]
        public void CancelOneExtraPaymentOutOfTwoBeforeFirstInstallment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                                | 2014-12-28 | 2014-12-28  | pmt001    | Additional | Scheduled |        9000.00 |        111.11 |     0.00 |    111.11 |       8888.89
                                | 2015-01-06 | 2015-01-06  | pmt002    | Additional | Scheduled |        8888.89 |        222.22 |     0.00 |    222.22 |       8666.67
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        8666.67 |       1560.68 |   101.57 |   1459.11 |       7207.56
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        7207.56 |       1560.68 |    82.53 |   1478.15 |       5729.41
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        5729.41 |       1560.68 |    65.60 |   1495.08 |       4234.33
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4234.33 |       1560.68 |    48.48 |   1512.20 |       2722.13
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2722.13 |       1560.68 |    31.17 |   1529.51 |       1192.62
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1192.62 |       1206.28 |    13.66 |   1192.62 |          0.00";

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 9000.00, Start = Loan.Terms.OriginationDate },
                new InterestAccrualPeriod { Principal = 8777.78, Start = Date(2015, 1, 6) }
            })
            .Returns(102.29);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 8777.78,
                ReferenceDate = Date(2015, 1, 6),
                OpeningInterest = 102.29
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |        8777.78 | 1560.68 |   102.29 |   1458.39 |       7319.39
                2015-02-15      | 2015-02-17 |        7319.39 | 1560.68 |    83.81 |   1476.87 |       5842.52
                2015-03-15      | 2015-03-17 |        5842.52 | 1560.68 |    66.90 |   1493.78 |       4348.74
                2015-04-15      | 2015-04-17 |        4348.74 | 1560.68 |    49.79 |   1510.89 |       2837.85
                2015-05-15      | 2015-05-17 |        2837.85 | 1560.68 |    32.49 |   1528.19 |       1309.66
                2015-06-15      | 2015-06-17 |        1309.66 | 1324.66 |    15.00 |   1309.66 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt001",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                                | 2015-01-06 | 2015-01-06  | pmt002    | Additional | Scheduled |        9000.00 |        222.22 |     0.00 |    222.22 |       8777.78
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        8777.78 |       1560.68 |   102.29 |   1458.39 |       7319.39
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        7319.39 |       1560.68 |    83.81 |   1476.87 |       5842.52
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        5842.52 |       1560.68 |    66.90 |   1493.78 |       4348.74
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4348.74 |       1560.68 |    49.79 |   1510.89 |       2837.85
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2837.85 |       1560.68 |    32.49 |   1528.19 |       1309.66
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1309.66 |       1324.66 |    15.00 |   1309.66 |          0.00"
            );
        }

        [Fact]
        public void CancelOneExtraPaymentOutOfThreeBeforeFirstInstallment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                                | 2014-12-23 | 2014-12-23  | pmt001    | Additional | Scheduled |        9000.00 |        111.11 |     0.00 |    111.11 |       8888.89
                                | 2014-12-29 | 2014-12-29  | pmt002    | Additional | Scheduled |        8888.89 |        222.22 |     0.00 |    222.22 |       8666.67
                                | 2015-01-05 | 2015-01-05  | pmt003    | Additional | Scheduled |        8666.67 |        333.33 |     0.00 |    333.33 |       8333.34
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        8333.34 |       1560.68 |    99.49 |   1461.19 |       6872.15
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        6872.15 |       1560.68 |    78.69 |   1481.99 |       5390.16
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        5390.16 |       1560.68 |    61.72 |   1498.96 |       3891.20
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        3891.20 |       1560.68 |    44.55 |   1516.13 |       2375.07
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2375.07 |       1560.68 |    27.19 |   1533.49 |        841.58
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |         841.58 |        851.22 |     9.64 |    841.58 |          0.00";

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 9000.00, Start = Loan.Terms.OriginationDate },
                new InterestAccrualPeriod { Principal = 8888.89, Start = Date(2014, 12, 23) },
                new InterestAccrualPeriod { Principal = 8555.56, Start = Date(2015, 1, 5) }
            })
            .Returns(100.84);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 8555.56,
                ReferenceDate = Date(2015, 1, 5),
                OpeningInterest = 100.84
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |        8555.56 | 1560.68 |   100.84 |   1459.84 |       7095.72
                2015-02-15      | 2015-02-17 |        7095.72 | 1560.68 |    81.25 |   1479.43 |       5616.29
                2015-03-15      | 2015-03-17 |        5616.29 | 1560.68 |    64.31 |   1496.37 |       4119.92
                2015-04-15      | 2015-04-17 |        4119.92 | 1560.68 |    47.17 |   1513.51 |       2606.41
                2015-05-15      | 2015-05-17 |        2606.41 | 1560.68 |    29.84 |   1530.84 |       1075.57
                2015-06-15      | 2015-06-17 |        1075.57 | 1087.89 |    12.32 |   1075.57 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt002",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                                | 2014-12-23 | 2014-12-23  | pmt001    | Additional | Scheduled |        9000.00 |        111.11 |     0.00 |    111.11 |       8888.89
                                | 2015-01-05 | 2015-01-05  | pmt003    | Additional | Scheduled |        8888.89 |        333.33 |     0.00 |    333.33 |       8555.56
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        8555.56 |       1560.68 |   100.84 |   1459.84 |       7095.72
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        7095.72 |       1560.68 |    81.25 |   1479.43 |       5616.29
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        5616.29 |       1560.68 |    64.31 |   1496.37 |       4119.92
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4119.92 |       1560.68 |    47.17 |   1513.51 |       2606.41
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2606.41 |       1560.68 |    29.84 |   1530.84 |       1075.57
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1075.57 |       1087.89 |    12.32 |   1075.57 |          0.00"
            );
        }

        [Fact]
        public void CancelOneExtraPaymentOutOfThree()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                                | 2015-03-22 | 2015-03-22  | pmt001    | Additional | Scheduled |        4576.85 |        111.11 |     0.00 |    111.11 |       4465.74
                                | 2015-03-30 | 2015-03-30  | pmt002    | Additional | Scheduled |        4465.74 |        222.22 |     0.00 |    222.22 |       4243.52
                                | 2015-04-11 | 2015-04-11  | pmt003    | Additional | Scheduled |        4243.52 |        333.33 |     0.00 |    333.33 |       3910.19
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        3910.19 |       1560.68 |    49.65 |   1511.03 |       2399.16
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2399.16 |       1560.68 |    27.47 |   1533.21 |        865.95
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |         865.95 |        875.87 |     9.92 |    865.95 |          0.00";

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 4576.85, Start = Date(2015, 3, 15) },
                new InterestAccrualPeriod { Principal = 4465.74, Start = Date(2015, 3, 22) },
                new InterestAccrualPeriod { Principal = 4132.41, Start = Date(2015, 4, 11) }
            })
            .Returns(50.92);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 4132.41,
                ReferenceDate = Date(2015, 4, 11),
                OpeningInterest = 50.92
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        4132.41 | 1560.68 |    50.92 |   1509.76 |       2622.65
                2015-05-15      | 2015-05-17 |        2622.65 | 1560.68 |    30.03 |   1530.65 |       1092.00
                2015-06-15      | 2015-06-17 |        1092.00 | 1104.50 |    12.50 |   1092.00 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt002",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                                | 2015-03-22 | 2015-03-22  | pmt001    | Additional | Scheduled |        4576.85 |        111.11 |     0.00 |    111.11 |       4465.74
                                | 2015-04-11 | 2015-04-11  | pmt003    | Additional | Scheduled |        4465.74 |        333.33 |     0.00 |    333.33 |       4132.41
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4132.41 |       1560.68 |    50.92 |   1509.76 |       2622.65
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2622.65 |       1560.68 |    30.03 |   1530.65 |       1092.00
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1092.00 |       1104.50 |    12.50 |   1092.00 |          0.00"
            );
        }

        [Fact]
        public void CancelExtraPaymentAndPreserveFutureExtraPayments()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                                | 2015-01-22 | 2015-01-22  | pmt001    | Additional | Scheduled |        7542.37 |         55.05 |     0.00 |     55.05 |       7487.32
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        7487.32 |       1560.68 |    85.88 |   1474.80 |       6012.52
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        6012.52 |       1560.68 |    68.84 |   1491.84 |       4520.68
                                | 2015-03-22 | 2015-03-22  | pmt002    | Additional | Scheduled |        4520.68 |        111.11 |     0.00 |    111.11 |       4409.57
                                | 2015-03-30 | 2015-03-30  | pmt003    | Additional | Scheduled |        4409.57 |        222.22 |     0.00 |    222.22 |       4187.35
                                | 2015-04-11 | 2015-04-11  | pmt004    | Additional | Scheduled |        4187.35 |        333.33 |     0.00 |    333.33 |       3854.02
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        3854.02 |       1560.68 |    49.01 |   1511.67 |       2342.34
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2342.34 |       1560.68 |    26.82 |   1533.86 |        808.48
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |         808.48 |        817.74 |     9.26 |    808.48 |          0.00";

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 7542.37,
                ReferenceDate = Date(2015, 1, 17)
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-02-15      | 2015-02-17 |        7542.37 | 1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |        6068.05 | 1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 4576.85, Start = Date(2015, 3, 15) },
                new InterestAccrualPeriod { Principal = 4465.74, Start = Date(2015, 3, 22) },
                new InterestAccrualPeriod { Principal = 4243.52, Start = Date(2015, 3, 30) },
                new InterestAccrualPeriod { Principal = 3910.19, Start = Date(2015, 4, 11) }
            })
            .Returns(49.65);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 3910.19,
                ReferenceDate = Date(2015, 4, 11),
                OpeningInterest = 49.65
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        3910.19 | 1560.68 |    49.65 |   1511.03 |       2399.16
                2015-05-15      | 2015-05-17 |        2399.16 | 1560.68 |    27.47 |   1533.21 |        865.95
                2015-06-15      | 2015-06-17 |         865.95 |  875.87 |     9.92 |    865.95 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt001",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                                | 2015-03-22 | 2015-03-22  | pmt002    | Additional | Scheduled |        4576.85 |        111.11 |     0.00 |    111.11 |       4465.74
                                | 2015-03-30 | 2015-03-30  | pmt003    | Additional | Scheduled |        4465.74 |        222.22 |     0.00 |    222.22 |       4243.52
                                | 2015-04-11 | 2015-04-11  | pmt004    | Additional | Scheduled |        4243.52 |        333.33 |     0.00 |    333.33 |       3910.19
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        3910.19 |       1560.68 |    49.65 |   1511.03 |       2399.16
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2399.16 |       1560.68 |    27.47 |   1533.21 |        865.95
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |         865.95 |        875.87 |     9.92 |    865.95 |          0.00"
            );
        }

        [Fact]
        public void CancelFirstPaymentScheduledAfterOneExtraPayment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                                | 2015-01-03 | 2015-01-03  | pmt001    | Additional | Scheduled |        9000.00 |        100.01 |     0.00 |    100.01 |       8899.99
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt002    | Scheduled  | Scheduled |        8899.99 |        999.09 |   102.59 |    896.50 |       8003.49
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        8003.49 |       1560.68 |    91.64 |   1469.04 |       6534.45
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        6534.45 |       1560.68 |    74.82 |   1485.86 |       5048.59
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        5048.59 |       1560.68 |    57.81 |   1502.87 |       3545.72
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        3545.72 |       1560.68 |    40.60 |   1520.08 |       2025.64
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        2025.64 |       2048.83 |    23.19 |   2025.64 |          0.00";

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 9000.00, Start = Loan.Terms.OriginationDate },
                new InterestAccrualPeriod { Principal = 8899.99, Start = Date(2015, 1, 3) }
            })
            .Returns(102.59);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 8899.99,
                ReferenceDate = Date(2015, 1, 3),
                OpeningInterest = 102.59
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |        8899.99 | 1560.68 |   102.59 |   1458.09 |       7441.90
                2015-02-15      | 2015-02-17 |        7441.90 | 1560.68 |    85.21 |   1475.47 |       5966.43
                2015-03-15      | 2015-03-17 |        5966.43 | 1560.68 |    68.32 |   1492.36 |       4474.07
                2015-04-15      | 2015-04-17 |        4474.07 | 1560.68 |    51.23 |   1509.45 |       2964.62
                2015-05-15      | 2015-05-17 |        2964.62 | 1560.68 |    33.94 |   1526.74 |       1437.88
                2015-06-15      | 2015-06-17 |        1437.88 | 1454.34 |    16.46 |   1437.88 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt002",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                                | 2015-01-03 | 2015-01-03  | pmt001    | Additional | Scheduled |        9000.00 |        100.01 |     0.00 |    100.01 |       8899.99
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        8899.99 |       1560.68 |   102.59 |   1458.09 |       7441.90
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        7441.90 |       1560.68 |    85.21 |   1475.47 |       5966.43
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        5966.43 |       1560.68 |    68.32 |   1492.36 |       4474.07
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4474.07 |       1560.68 |    51.23 |   1509.45 |       2964.62
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2964.62 |       1560.68 |    33.94 |   1526.74 |       1437.88
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1437.88 |       1454.34 |    16.46 |   1437.88 |          0.00"
            );
        }

        [Fact]
        public void CancelFirstPaymentScheduledAfterTwoExtraPayments()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                                | 2014-12-26 | 2014-12-26  | pmt001    | Additional | Scheduled |        9000.00 |        100.01 |     0.00 |    100.01 |       8899.99
                                | 2015-01-08 | 2015-01-08  | pmt002    | Additional | Scheduled |        8899.99 |        200.02 |     0.00 |    200.02 |       8699.97
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt003    | Scheduled  | Scheduled |        8699.97 |        999.09 |   101.79 |    897.30 |       7802.67
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        7802.67 |       1560.68 |    89.34 |   1471.34 |       6331.33
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        6331.33 |       1560.68 |    72.49 |   1488.19 |       4843.14
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4843.14 |       1560.68 |    55.45 |   1505.23 |       3337.91
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        3337.91 |       1560.68 |    38.22 |   1522.46 |       1815.45
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1815.45 |       1836.24 |    20.79 |   1815.45 |          0.00";

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 9000.00, Start = Loan.Terms.OriginationDate },
                new InterestAccrualPeriod { Principal = 8899.99, Start = Date(2014, 12, 26) },
                new InterestAccrualPeriod { Principal = 8699.97, Start = Date(2015, 1, 8) }
            })
            .Returns(101.79);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 8699.97,
                ReferenceDate = Date(2015, 1, 8),
                OpeningInterest = 101.79
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |        8699.97 | 1560.68 |   101.79 |   1458.89 |       7241.08
                2015-02-15      | 2015-02-17 |        7241.08 | 1560.68 |    82.91 |   1477.77 |       5763.31
                2015-03-15      | 2015-03-17 |        5763.31 | 1560.68 |    65.99 |   1494.69 |       4268.62
                2015-04-15      | 2015-04-17 |        4268.62 | 1560.68 |    48.88 |   1511.80 |       2756.82
                2015-05-15      | 2015-05-17 |        2756.82 | 1560.68 |    31.57 |   1529.11 |       1227.71
                2015-06-15      | 2015-06-17 |        1227.71 | 1241.77 |    14.06 |   1227.71 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt003",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                                | 2014-12-26 | 2014-12-26  | pmt001    | Additional | Scheduled |        9000.00 |        100.01 |     0.00 |    100.01 |       8899.99
                                | 2015-01-08 | 2015-01-08  | pmt002    | Additional | Scheduled |        8899.99 |        200.02 |     0.00 |    200.02 |       8699.97
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        8699.97 |       1560.68 |   101.79 |   1458.89 |       7241.08
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        7241.08 |       1560.68 |    82.91 |   1477.77 |       5763.31
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        5763.31 |       1560.68 |    65.99 |   1494.69 |       4268.62
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4268.62 |       1560.68 |    48.88 |   1511.80 |       2756.82
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2756.82 |       1560.68 |    31.57 |   1529.11 |       1227.71
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1227.71 |       1241.77 |    14.06 |   1227.71 |          0.00"
            );
        }

        [Fact]
        public void CancelScheduledPaymentScheduledBetweenTwoExtraPayments()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                                | 2015-01-30 | 2015-01-30  | pmt001    | Additional | Scheduled |        7542.37 |        101.01 |     0.00 |    101.01 |       7441.36
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Scheduled |        7441.36 |        909.09 |    85.78 |    823.31 |       6618.05
                                | 2015-02-23 | 2015-02-23  | pmt003    | Additional | Scheduled |        6618.05 |        101.01 |     0.00 |    101.01 |       6517.04
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        6517.04 |       1560.68 |    74.93 |   1485.75 |       5031.29
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        5031.29 |       1560.68 |    57.61 |   1503.07 |       3528.22
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        3528.22 |       1560.68 |    40.40 |   1520.28 |       2007.94
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        2007.94 |       2030.93 |    22.99 |   2007.94 |          0.00";

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 7542.37, Start = Date(2015, 1, 15) },
                new InterestAccrualPeriod { Principal = 7441.36, Start = Date(2015, 1, 30) },
            })
            .Returns(85.78);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 7441.36,
                ReferenceDate = Date(2015, 1, 30),
                OpeningInterest = 85.78
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance | Amount  | Interest | Principal | EndingBalance
                2015-02-15      | 2015-02-17 |        7441.36 | 1560.68 |    85.78 |   1474.90 |       5966.46
                2015-03-15      | 2015-03-17 |        5966.46 | 1560.68 |    68.32 |   1492.36 |       4474.10
                2015-04-15      | 2015-04-17 |        4474.10 | 1560.68 |    51.23 |   1509.45 |       2964.65
                2015-05-15      | 2015-05-17 |        2964.65 | 1560.68 |    33.95 |   1526.73 |       1437.92
                2015-06-15      | 2015-06-17 |        1437.92 | 1454.38 |    16.46 |   1437.92 |          0.00");

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 5966.46, Start = Date(2015, 2, 15) },
                new InterestAccrualPeriod { Principal = 5865.45, Start = Date(2015, 2, 23) },
            })
            .Returns(67.47);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 5865.45,
                ReferenceDate = Date(2015, 2, 23),
                OpeningInterest = 67.47
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance | Amount  | Interest | Principal | EndingBalance
                2015-03-15      | 2015-03-17 |        5865.45 | 1560.68 |    67.47 |   1493.21 |       4372.24
                2015-04-15      | 2015-04-17 |        4372.24 | 1560.68 |    50.06 |   1510.62 |       2861.62
                2015-05-15      | 2015-05-17 |        2861.62 | 1560.68 |    32.77 |   1527.91 |       1333.71
                2015-06-15      | 2015-06-17 |        1333.71 | 1348.98 |    15.27 |   1333.71 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt002",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                                | 2015-01-30 | 2015-01-30  | pmt001    | Additional | Scheduled |        7542.37 |        101.01 |     0.00 |    101.01 |       7441.36
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        7441.36 |       1560.68 |    85.78 |   1474.90 |       5966.46
                                | 2015-02-23 | 2015-02-23  | pmt003    | Additional | Scheduled |        5966.46 |        101.01 |     0.00 |    101.01 |       5865.45
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        5865.45 |       1560.68 |    67.47 |   1493.21 |       4372.24
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4372.24 |       1560.68 |    50.06 |   1510.62 |       2861.62
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2861.62 |       1560.68 |    32.77 |   1527.91 |       1333.71
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1333.71 |       1348.98 |    15.27 |   1333.71 |          0.00"
            );
        }

        [Fact(Skip = "TODO")]
        public void CancelScheduledPaymentBeforeCompletedScheduledPayment()
        {
        }

        [Fact]
        public void CancelScheduledPaymentBeforeCompletedExtraPayment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled  | Scheduled |        6068.05 |       1444.04 |    69.48 |   1374.56 |       4693.49
                                | 2015-03-19 | 2015-03-19  | pmt004    | Additional | Completed |        4693.49 |        999.99 |     0.00 |    999.99 |       3693.50
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        3693.50 |       1560.68 |    43.82 |   1516.86 |       2176.64
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2176.64 |       1560.68 |    24.92 |   1535.76 |        640.88
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |         640.88 |        648.22 |     7.34 |    640.88 |          0.00";

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 6068.05,
                ReferenceDate = Date(2015, 2, 17)
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance | Amount  | Interest | Principal | EndingBalance
                2015-03-15      | 2015-03-17 |        6068.05 | 1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 4576.85, Start = Date(2015, 3, 15) },
                new InterestAccrualPeriod { Principal = 3576.86, Start = Date(2015, 3, 19) }
            })
            .Returns(42.48);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 3576.86,
                ReferenceDate = Date(2015, 3, 19),
                OpeningInterest = 42.48
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        3576.86 | 1560.68 |    42.48 |   1518.20 |       2058.66
                2015-05-15      | 2015-05-17 |        2058.66 | 1560.68 |    23.57 |   1537.11 |        521.55
                2015-06-15      | 2015-06-17 |         521.55 |  527.52 |     5.97 |    521.55 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt003",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                                | 2015-03-19 | 2015-03-19  | pmt004    | Additional | Completed |        4576.85 |        999.99 |     0.00 |    999.99 |       3576.86
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        3576.86 |       1560.68 |    42.48 |   1518.20 |       2058.66
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2058.66 |       1560.68 |    23.57 |   1537.11 |        521.55
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |         521.55 |        527.52 |     5.97 |    521.55 |          0.00"
            );
        }

        [Fact]
        public void CancelExtraPaymentBeforeCompletedScheduledPayment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                                | 2015-03-13 | 2015-03-13  | pmt003    | Additional | Scheduled |        6068.05 |        999.99 |     0.00 |    999.99 |       5068.06
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt004    | Scheduled  | Completed |        5068.06 |       1444.04 |    68.72 |   1375.32 |       3692.74
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        3692.74 |       1560.68 |    42.28 |   1518.40 |       2174.34
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2174.34 |       1560.68 |    24.90 |   1535.78 |        638.56
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |         638.56 |        645.87 |     7.31 |    638.56 |          0.00";

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 6068.05,
                ReferenceDate = Date(2015, 2, 17)
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance | Amount  | Interest | Principal | EndingBalance
                2015-03-15      | 2015-03-17 |        6068.05 | 1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 4693.49,
                ReferenceDate = Date(2015, 3, 17)
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        4693.49 | 1560.68 |    53.74 |   1506.94 |       3186.55
                2015-05-15      | 2015-05-17 |        3186.55 | 1560.68 |    36.49 |   1524.19 |       1662.36
                2015-06-15      | 2015-06-17 |        1662.36 | 1681.39 |    19.03 |   1662.36 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt003",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt004    | Scheduled | Completed |        6068.05 |       1444.04 |    69.48 |   1374.56 |       4693.49
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4693.49 |       1560.68 |    53.74 |   1506.94 |       3186.55
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3186.55 |       1560.68 |    36.49 |   1524.19 |       1662.36
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1662.36 |       1681.39 |    19.03 |   1662.36 |          0.00"
            );
        }

        [Fact]
        public void CancelExtraPaymentBeforeCompletedExtraPayment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                                | 2015-02-26 | 2015-02-26  | pmt003    | Additional | Scheduled |        6068.05 |        303.03 |     0.00 |    303.03 |       5765.02
                                | 2015-03-11 | 2015-03-11  | pmt004    | Additional | Completed |        5765.02 |        404.04 |     0.00 |    404.04 |       5360.98
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        5360.98 |       1444.04 |    66.66 |   1377.38 |       3983.60
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        3983.60 |       1560.68 |    45.61 |   1515.07 |       2468.53
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2468.53 |       1560.68 |    28.26 |   1532.42 |        936.11
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |         936.11 |        946.83 |    10.72 |    936.11 |          0.00";

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 6068.05, Start = Date(2015, 2, 15) },
                new InterestAccrualPeriod { Principal = 5664.01, Start = Date(2015, 3, 11) }
            })
            .Returns(68.86);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 5664.01,
                ReferenceDate = Date(2015, 3, 11),
                OpeningInterest = 68.86
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-03-15      | 2015-03-17 |        5664.01 | 1444.04 |    68.86 |   1375.18 |       4288.83
                2015-04-15      | 2015-04-17 |        4288.83 | 1560.68 |    49.11 |   1511.57 |       2777.26
                2015-05-15      | 2015-05-17 |        2777.26 | 1560.68 |    31.80 |   1528.88 |       1248.38
                2015-06-15      | 2015-06-17 |        1248.38 | 1262.67 |    14.29 |   1248.38 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt003",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                                | 2015-03-11 | 2015-03-11  | pmt004    | Additional | Completed |        6068.05 |        404.04 |     0.00 |    404.04 |       5664.01
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        5664.01 |       1444.04 |    68.86 |   1375.18 |       4288.83
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4288.83 |       1560.68 |    49.11 |   1511.57 |       2777.26
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2777.26 |       1560.68 |    31.80 |   1528.88 |       1248.38
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1248.38 |       1262.67 |    14.29 |   1248.38 |          0.00"
            );
        }

        [Fact]
        public void CancelExtraPaymentScheduledAfterCompletedExtraPayment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                                | 2015-02-26 | 2015-02-26  | pmt003    | Additional | Completed |        6068.05 |        303.03 |     0.00 |    303.03 |       5765.02
                                | 2015-03-09 | 2015-03-09  | pmt004    | Additional | Scheduled |        5765.02 |        404.04 |     0.00 |    404.04 |       5360.98
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        5360.98 |       1560.68 |    66.36 |   1494.32 |       3866.66
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        3866.66 |       1560.68 |    44.27 |   1516.41 |       2350.25
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2350.25 |       1560.68 |    26.91 |   1533.77 |        816.48
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |         816.48 |        825.83 |     9.35 |    816.48 |          0.00";

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 6068.05, Start = Date(2015, 2, 15) },
                new InterestAccrualPeriod { Principal = 5765.02, Start = Date(2015, 2, 26) }
            })
            .Returns(67.28);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 5765.02,
                ReferenceDate = Date(2015, 2, 26),
                OpeningInterest = 67.28
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-03-15      | 2015-03-17 |        5765.02 | 1560.68 |    67.28 |   1493.40 |       4271.62
                2015-04-15      | 2015-04-17 |        4271.62 | 1560.68 |    48.91 |   1511.77 |       2759.85
                2015-05-15      | 2015-05-17 |        2759.85 | 1560.68 |    31.60 |   1529.08 |       1230.77
                2015-06-15      | 2015-06-17 |        1230.77 | 1244.86 |    14.09 |   1230.77 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt004",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                                | 2015-02-26 | 2015-02-26  | pmt003    | Additional | Completed |        6068.05 |        303.03 |     0.00 |    303.03 |       5765.02
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        5765.02 |       1560.68 |    67.28 |   1493.40 |       4271.62
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4271.62 |       1560.68 |    48.91 |   1511.77 |       2759.85
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2759.85 |       1560.68 |    31.60 |   1529.08 |       1230.77
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1230.77 |       1244.86 |    14.09 |   1230.77 |          0.00"
            );
        }

        [Fact]
        public void CancelCompletedExtraPayment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled  | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                                | 2015-03-26 | 2015-03-26  | pmt004    | Additional | Completed |        4576.85 |        404.04 |     0.00 |    404.04 |       4172.81
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4172.81 |       1560.68 |    49.47 |   1511.21 |       2661.60
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2661.60 |       1560.68 |    30.48 |   1530.20 |       1131.40
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1131.40 |       1144.35 |    12.95 |   1131.40 |          0.00";

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 4576.85,
                ReferenceDate = Date(2015, 3, 17)
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt004",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00"
            );
        }

        [Fact]
        public void CancelCompletedExtraPaymentMadeAfterAnotherCompletedExtraPayment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled  | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                                | 2015-03-26 | 2015-03-26  | pmt004    | Additional | Completed |        4576.85 |        404.04 |     0.00 |    404.04 |       4172.81
                                | 2015-04-08 | 2015-04-08  | pmt005    | Additional | Completed |        4172.81 |        505.05 |     0.00 |    505.05 |       3667.76
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        3667.76 |       1560.68 |    48.13 |   1512.55 |       2155.21
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2155.21 |       1560.68 |    24.68 |   1536.00 |        619.21
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |         619.21 |        626.30 |     7.09 |    619.21 |          0.00";

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 4576.85, Start = Date(2015, 3, 15) },
                new InterestAccrualPeriod { Principal = 4172.81, Start = Date(2015, 3, 26) }
            })
            .Returns(49.47);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 4172.81,
                ReferenceDate = Date(2015, 3, 26),
                OpeningInterest = 49.47
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        4172.81 | 1560.68 |    49.47 |   1511.21 |       2661.60
                2015-05-15      | 2015-05-17 |        2661.60 | 1560.68 |    30.48 |   1530.20 |       1131.40
                2015-06-15      | 2015-06-17 |        1131.40 | 1144.35 |    12.95 |   1131.40 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt005",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled  | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                                | 2015-03-26 | 2015-03-26  | pmt004    | Additional | Completed |        4576.85 |        404.04 |     0.00 |    404.04 |       4172.81
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4172.81 |       1560.68 |    49.47 |   1511.21 |       2661.60
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2661.60 |       1560.68 |    30.48 |   1530.20 |       1131.40
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1131.40 |       1144.35 |    12.95 |   1131.40 |          0.00"
            );
        }

        [Fact]
        public void CancelCompletedExtraPaymentMadeBetweenTwoOtherCompletedExtraPayments()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled  | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                                | 2015-03-26 | 2015-03-26  | pmt004    | Additional | Completed |        4576.85 |        404.04 |     0.00 |    404.04 |       4172.81
                                | 2015-04-02 | 2015-04-02  | pmt005    | Additional | Completed |        4172.81 |        505.05 |     0.00 |    505.05 |       3667.76
                                | 2015-04-11 | 2015-04-11  | pmt006    | Additional | Completed |        3667.76 |        606.06 |     0.00 |    606.06 |       3061.70
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        3061.70 |       1560.68 |    46.04 |   1514.64 |       1547.06
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        1547.06 |       1560.68 |    17.71 |   1542.97 |          4.09
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |           4.09 |          4.14 |     0.05 |      4.09 |          0.00";

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 4576.85, Start = Date(2015, 3, 15) },
                new InterestAccrualPeriod { Principal = 4172.81, Start = Date(2015, 3, 26) },
                new InterestAccrualPeriod { Principal = 3566.75, Start = Date(2015, 4, 11) }
            })
            .Returns(48.55);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 3566.75,
                ReferenceDate = Date(2015, 4, 11),
                OpeningInterest = 48.55
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        3566.75 | 1560.68 |    48.55 |   1512.13 |       2054.62
                2015-05-15      | 2015-05-17 |        2054.62 | 1560.68 |    23.53 |   1537.15 |        517.47
                2015-06-15      | 2015-06-17 |         517.47 |  523.40 |     5.93 |    517.47 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt005",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled  | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                                | 2015-03-26 | 2015-03-26  | pmt004    | Additional | Completed |        4576.85 |        404.04 |     0.00 |    404.04 |       4172.81
                                | 2015-04-11 | 2015-04-11  | pmt006    | Additional | Completed |        4172.81 |        606.06 |     0.00 |    606.06 |       3566.75
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        3566.75 |       1560.68 |    48.55 |   1512.13 |       2054.62
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2054.62 |       1560.68 |    23.53 |   1537.15 |        517.47
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |         517.47 |        523.40 |     5.93 |    517.47 |          0.00"
            );
        }

        [Fact]
        public void Cancel1stEarlyCheckPayment()
        {
            Loan.PaymentMethod = PaymentMethod.Check;

            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-10  | pmt001    | Scheduled | Completed | Check         |        9000.00 |       1560.68 |   100.07 |   1460.61 |       7539.39
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled | Check         |        7539.39 |       1560.68 |    86.33 |   1474.35 |       6065.04
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled | Check         |        6065.04 |       1560.68 |    69.44 |   1491.24 |       4573.80
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled | Check         |        4573.80 |       1560.68 |    52.37 |   1508.31 |       3065.49
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled | Check         |        3065.49 |       1560.68 |    35.10 |   1525.58 |       1539.91
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled | Check         |        1539.91 |       1557.54 |    17.63 |   1539.91 |          0.00";

            AmortizationService.OnAmortize(new AmortizationRequest())
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |        9000.00 | 1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |        7542.37 | 1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |        6068.05 | 1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt001",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled | Check         |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled | Check         |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled | Check         |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled | Check         |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00"
            );
        }

        [Fact]
        public void Cancel3rdEarlyCheckPayment()
        {
            Loan.PaymentMethod = PaymentMethod.Check;

            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-11  | pmt003    | Scheduled | Completed | Check         |        6068.05 |       1560.68 |    67.10 |   1493.58 |       4574.47
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled | Check         |        4574.47 |       1560.68 |    52.38 |   1508.30 |       3066.17
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled | Check         |        3066.17 |       1560.68 |    35.11 |   1525.57 |       1540.60
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled | Check         |        1540.60 |       1558.24 |    17.64 |   1540.60 |          0.00";

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 6068.05,
                ReferenceDate = Date(2015, 2, 17)
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-03-15      | 2015-03-17 |        6068.05 | 1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt003",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled | Check         |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled | Check         |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled | Check         |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled | Check         |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00"
            );
        }

        [Fact]
        public void CancelLastEarlyCheckPayment()
        {
            Loan.PaymentMethod = PaymentMethod.Check;

            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-15  | pmt003    | Scheduled | Completed | Check         |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 | 2015-04-15  | pmt004    | Scheduled | Completed | Check         |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 | 2015-05-15  | pmt005    | Scheduled | Completed | Check         |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 | 2015-06-09  | pmt006    | Scheduled | Completed | Check         |        1543.03 |       1557.13 |    14.10 |   1543.03 |          0.00";

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 1543.03,
                ReferenceDate = Date(2015, 5, 17)
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt006",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-15  | pmt003    | Scheduled | Completed | Check         |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 | 2015-04-15  | pmt004    | Scheduled | Completed | Check         |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 | 2015-05-15  | pmt005    | Scheduled | Completed | Check         |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled | Check         |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00"
            );
        }

        [Fact]
        public void CancelExtraPaymentMadeBeforeEarlyCheckPayment()
        {
            Loan.PaymentMethod = PaymentMethod.Check;

            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled  | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled  | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                                | 2015-03-01 | 2015-03-01  | pmt003    | Additional | Completed | Check         |        6068.05 |        303.03 |     0.00 |    303.03 |       5765.02
                2015-03-15      | 2015-03-17 | 2015-03-11  | pmt004    | Scheduled  | Completed | Check         |        5765.02 |       1560.68 |    59.06 |   1501.62 |       4263.40
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled | Check         |        4263.40 |       1560.68 |    55.32 |   1505.36 |       2758.04
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled | Check         |        2758.04 |       1560.68 |    31.58 |   1529.10 |       1228.94
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled | Check         |        1228.94 |       1243.01 |    14.07 |   1228.94 |          0.00";

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 6068.05, Start = Date(2015, 2, 15), End = Date(2015, 3, 11) }
            })
            .Returns(60.22);

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 4567.59, Start = Date(2015, 3, 11) },
                new InterestAccrualPeriod { Principal = 4567.59, Start = Date(2015, 3, 15) }
            })
            .Returns(59.27);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 4567.59,
                ReferenceDate = Date(2015, 3, 17),
                OpeningInterest = 59.27
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        4567.59 | 1560.68 |    59.27 |   1501.41 |       3066.18
                2015-05-15      | 2015-05-17 |        3066.18 | 1560.68 |    35.11 |   1525.57 |       1540.61
                2015-06-15      | 2015-06-17 |        1540.61 | 1558.25 |    17.64 |   1540.61 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt003",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-11  | pmt004    | Scheduled | Completed | Check         |        6068.05 |       1560.68 |    60.22 |   1500.46 |       4567.59
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled | Check         |        4567.59 |       1560.68 |    59.27 |   1501.41 |       3066.18
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled | Check         |        3066.18 |       1560.68 |    35.11 |   1525.57 |       1540.61
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled | Check         |        1540.61 |       1558.25 |    17.64 |   1540.61 |          0.00"
            );
        }

        [Fact]
        public void CancelExtraPaymentMadeBeforeExtraAndEarlyCheckPayments()
        {
            Loan.PaymentMethod = PaymentMethod.Check;

            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled  | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled  | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                                | 2015-02-26 | 2015-02-26  | pmt003    | Additional | Completed | Check         |        6068.05 |        300.03 |     0.00 |    300.03 |       5768.02
                                | 2015-03-03 | 2015-03-03  | pmt004    | Additional | Completed | Check         |        5768.02 |        400.04 |     0.00 |    400.04 |       5367.98
                2015-03-15      | 2015-03-17 | 2015-03-11  | pmt005    | Scheduled  | Completed | Check         |        5367.98 |       1560.68 |    57.28 |   1503.40 |       3864.58
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled | Check         |        3864.58 |       1560.68 |    50.15 |   1510.53 |       2354.05
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled | Check         |        2354.05 |       1560.68 |    26.95 |   1533.73 |        820.32
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled | Check         |         820.32 |        829.71 |     9.39 |    820.32 |          0.00";

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 6068.05, Start = Date(2015, 2, 15) },
                new InterestAccrualPeriod { Principal = 5668.01, Start = Date(2015, 3,  3), End = Date(2015, 3, 11) }
            })
            .Returns(58.99); //early payment's interest

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 4166.32, Start = Date(2015, 3, 11) },
                new InterestAccrualPeriod { Principal = 4166.32, Start = Date(2015, 3, 15) },
            })
            .Returns(54.06); //next installment's interest;

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 4166.32,
                ReferenceDate = Date(2015, 3, 17),
                OpeningInterest = 54.06
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        4166.32 | 1560.68 |    54.06 |   1506.62 |       2659.70
                2015-05-15      | 2015-05-17 |        2659.70 | 1560.68 |    30.45 |   1530.23 |       1129.47
                2015-06-15      | 2015-06-17 |        1129.47 | 1142.40 |    12.93 |   1129.47 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt003",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled  | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled  | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                                | 2015-03-03 | 2015-03-03  | pmt004    | Additional | Completed | Check         |        6068.05 |        400.04 |     0.00 |    400.04 |       5668.01
                2015-03-15      | 2015-03-17 | 2015-03-11  | pmt005    | Scheduled  | Completed | Check         |        5668.01 |       1560.68 |    58.99 |   1501.69 |       4166.32
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled | Check         |        4166.32 |       1560.68 |    54.06 |   1506.62 |       2659.70
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled | Check         |        2659.70 |       1560.68 |    30.45 |   1530.23 |       1129.47
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled | Check         |        1129.47 |       1142.40 |    12.93 |   1129.47 |          0.00"
            );
        }

        [Fact(Skip = "TODO")]
        public void CancelLatePaymentScheduledAfterOneOpenInstallment()
        {
        }

        [Fact(Skip = "TODO")]
        public void CancelLatePaymentScheduledAfterTwoOpenInstallments()
        {
        }

        [Fact]
        public void CancelPayoffScheduledBeforeFirstInstallment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type   | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                                | 2014-12-30 | 2014-12-30  | pmt001    | Payoff | Scheduled |        9000.00 |       9051.53 |    51.53 |   9000.00 |          0.00";

            AmortizationService.OnAmortize(new AmortizationRequest())
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |        9000.00 | 1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |        7542.37 | 1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |        6068.05 | 1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt001",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00"
            );
        }

        [Fact]
        public void CancelPayoffScheduledAfterFirstInstallment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                                | 2015-01-22 | 2015-01-22  | pmt001    | Payoff    | Scheduled |        7542.37 |       7562.52 |    20.15 |   7542.37 |          0.00";

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 7542.37,
                ReferenceDate = Date(2015, 1, 17)
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-02-15      | 2015-02-17 |        7542.37 | 1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |        6068.05 | 1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt001",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00"
            );
        }

        [Fact]
        public void CancelPayoffScheduledAfterThirdInstallment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                                | 2015-03-24 | 2015-03-24  | pmt001    | Payoff    | Scheduled |        4576.85 |       4592.57 |    15.72 |   4576.85 |          0.00";

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 4576.85,
                ReferenceDate = Date(2015, 3, 17)
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt001",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00"
            );
        }

        [Fact]
        public void CancelCompletedPayoffMadeAfterThirdInstallment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                                | 2015-03-24 | 2015-03-24  | pmt004    | Payoff    | Completed |        4576.85 |       4592.57 |    15.72 |   4576.85 |          0.00";

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 4576.85,
                ReferenceDate = Date(2015, 3, 17)
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt004",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00"
            );
        }

        [Fact]
        public void CancelPayoffScheduledAfterThirdPayment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                                | 2015-03-24 | 2015-03-24  | pmt004    | Payoff    | Scheduled |        4576.85 |       4592.57 |    15.72 |   4576.85 |          0.00";

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 4576.85,
                ReferenceDate = Date(2015, 3, 17)
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt004",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00"
            );
        }

        [Fact]
        public void CancelPayoffScheduledAfterTwoExtraPayments()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled  | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                                | 2015-03-23 | 2015-03-23  | pmt004    | Additional | Completed |        4576.85 |        101.01 |     0.00 |    101.01 |       4475.84
                                | 2015-04-02 | 2015-04-02  | pmt005    | Additional | Scheduled |        4475.84 |        202.02 |     0.00 |    202.02 |       4273.82
                                | 2015-04-10 | 2015-04-10  | pmt006    | Payoff     | Scheduled |        4273.82 |       4316.22 |    42.40 |   4273.82 |          0.00";

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 4576.85, Start = Date(2015, 3, 15) },
                new InterestAccrualPeriod { Principal = 4475.84, Start = Date(2015, 3, 23) },
                new InterestAccrualPeriod { Principal = 4273.82, Start = Date(2015, 4, 2) }
            })
            .Returns(50.55);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 4273.82,
                ReferenceDate = Date(2015, 4, 2),
                OpeningInterest = 50.55
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        4273.82 | 1560.68 |    50.55 |   1510.13 |       2763.69
                2015-05-15      | 2015-05-17 |        2763.69 | 1560.68 |    31.64 |   1529.04 |       1234.65
                2015-06-15      | 2015-06-17 |        1234.65 | 1248.79 |    14.14 |   1234.65 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt006",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled  | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                                | 2015-03-23 | 2015-03-23  | pmt004    | Additional | Completed |        4576.85 |        101.01 |     0.00 |    101.01 |       4475.84
                                | 2015-04-02 | 2015-04-02  | pmt005    | Additional | Scheduled |        4475.84 |        202.02 |     0.00 |    202.02 |       4273.82
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4273.82 |       1560.68 |    50.55 |   1510.13 |       2763.69
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2763.69 |       1560.68 |    31.64 |   1529.04 |       1234.65
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1234.65 |       1248.79 |    14.14 |   1234.65 |          0.00"
            );
        }

        [Fact]
        public void CancelPayoffScheduledAfterExtraPaymentBeforeFirstInstallment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                                | 2014-12-30 | 2014-12-30  | pmt001    | Additional | Scheduled |        9000.00 |        101.01 |     0.00 |    101.01 |       8898.99
                                | 2015-01-07 | 2015-01-07  | pmt002    | Payoff     | Scheduled |        8898.99 |       8974.29 |    75.30 |   8898.99 |          0.00";

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 9000.00, Start = Loan.Terms.OriginationDate },
                new InterestAccrualPeriod { Principal = 8898.99, Start = Date(2014, 12, 30) }
            })
            .Returns(102.47);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 8898.99,
                ReferenceDate = Date(2014, 12, 30),
                OpeningInterest = 102.47
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |        8898.99 | 1560.68 |   102.47 |   1458.21 |       7440.78
                2015-02-15      | 2015-02-17 |        7440.78 | 1560.68 |    85.20 |   1475.48 |       5965.30
                2015-03-15      | 2015-03-17 |        5965.30 | 1560.68 |    68.30 |   1492.38 |       4472.92
                2015-04-15      | 2015-04-17 |        4472.92 | 1560.68 |    51.21 |   1509.47 |       2963.45
                2015-05-15      | 2015-05-17 |        2963.45 | 1560.68 |    33.93 |   1526.75 |       1436.70
                2015-06-15      | 2015-06-17 |        1436.70 | 1453.15 |    16.45 |   1436.70 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt002",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                                | 2014-12-30 | 2014-12-30  | pmt001    | Additional | Scheduled |        9000.00 |        101.01 |     0.00 |    101.01 |       8898.99
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        8898.99 |       1560.68 |   102.47 |   1458.21 |       7440.78
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        7440.78 |       1560.68 |    85.20 |   1475.48 |       5965.30
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        5965.30 |       1560.68 |    68.30 |   1492.38 |       4472.92
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4472.92 |       1560.68 |    51.21 |   1509.47 |       2963.45
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2963.45 |       1560.68 |    33.93 |   1526.75 |       1436.70
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1436.70 |       1453.15 |    16.45 |   1436.70 |          0.00"
            );
        }

        [Fact]
        public void CancelPayoffScheduledAfterTwoExtraPaymentsBeforeFirstInstallment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                                | 2014-12-22 | 2014-12-22  | pmt001    | Additional | Scheduled |        9000.00 |        101.01 |     0.00 |    101.01 |       8898.99
                                | 2015-01-04 | 2015-01-04  | pmt002    | Additional | Scheduled |        8898.99 |        202.02 |     0.00 |    202.02 |       8696.97
                                | 2015-01-09 | 2015-01-09  | pmt003    | Payoff     | Scheduled |        8696.97 |       8778.37 |    81.40 |   8696.97 |          0.00";

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 9000.00, Start = Loan.Terms.OriginationDate },
                new InterestAccrualPeriod { Principal = 8898.99, Start = Date(2014, 12, 22) },
                new InterestAccrualPeriod { Principal = 8696.97, Start = Date(2015, 1, 4) },
            })
            .Returns(101.32);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 8696.97,
                ReferenceDate = Date(2015, 1, 4),
                OpeningInterest = 101.32
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |        8696.97 | 1560.68 |   101.32 |   1459.36 |       7237.61
                2015-02-15      | 2015-02-17 |        7237.61 | 1560.68 |    82.87 |   1477.81 |       5759.80
                2015-03-15      | 2015-03-17 |        5759.80 | 1560.68 |    65.95 |   1494.73 |       4265.07
                2015-04-15      | 2015-04-17 |        4265.07 | 1560.68 |    48.83 |   1511.85 |       2753.22
                2015-05-15      | 2015-05-17 |        2753.22 | 1560.68 |    31.52 |   1529.16 |       1224.06
                2015-06-15      | 2015-06-17 |        1224.06 | 1238.08 |    14.02 |   1224.06 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt003",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                                | 2014-12-22 | 2014-12-22  | pmt001    | Additional | Scheduled |        9000.00 |        101.01 |     0.00 |    101.01 |       8898.99
                                | 2015-01-04 | 2015-01-04  | pmt002    | Additional | Scheduled |        8898.99 |        202.02 |     0.00 |    202.02 |       8696.97
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        8696.97 |       1560.68 |   101.32 |   1459.36 |       7237.61
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        7237.61 |       1560.68 |    82.87 |   1477.81 |       5759.80
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        5759.80 |       1560.68 |    65.95 |   1494.73 |       4265.07
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4265.07 |       1560.68 |    48.83 |   1511.85 |       2753.22
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2753.22 |       1560.68 |    31.52 |   1529.16 |       1224.06
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1224.06 |       1238.08 |    14.02 |   1224.06 |          0.00"
            );
        }

        [Fact]
        public void PreservePayoffWhenCancellingScheduledPaymentAndItsAmountDoesNotChangeAfterReamortization()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                                | 2015-03-24 | 2015-03-24  | pmt004    | Payoff    | Scheduled |        4576.85 |       4592.57 |    15.72 |   4576.85 |          0.00";

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 6068.05,
                ReferenceDate = Date(2015, 2, 17)
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-03-15      | 2015-03-17 |        6068.05 | 1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt003",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                                | 2015-03-24 | 2015-03-24  | pmt004    | Payoff    | Scheduled |        4576.85 |       4592.57 |    15.72 |   4576.85 |          0.00"
            );
        }

        [Fact]
        public void RemovePayoffWhenCancellingScheduledPaymentAndItsAmountChangesAfterReamortization()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled | Scheduled |        6068.05 |        999.99 |    69.48 |    930.51 |       5137.54
                                | 2015-03-24 | 2015-03-24  | pmt004    | Payoff    | Scheduled |        5137.54 |       5155.19 |    17.65 |   5137.54 |          0.00";

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 6068.05,
                ReferenceDate = Date(2015, 2, 17)
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-03-15      | 2015-03-17 |        6068.05 | 1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt003",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00"
            );
        }

        [Fact]
        public void RemovePayoffWhenCancellingAnExtraPayment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                                | 2015-01-23 | 2015-01-23  | pmt001    | Additional | Scheduled |        7542.37 |        101.01 |     0.00 |    101.01 |       7441.36
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        7441.36 |       1560.68 |    85.51 |   1475.17 |       5966.19
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        5966.19 |        999.99 |    68.31 |    931.68 |       5034.51
                                | 2015-03-24 | 2015-03-24  | pmt002    | Payoff     | Scheduled |        5034.51 |       5051.81 |    17.29 |   5034.51 |          0.00";

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                PrincipalBalance = 7542.37,
                ReferenceDate = Date(2015, 1, 17)
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-02-15      | 2015-02-17 |        7542.37 | 1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |        6068.05 | 1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt001",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00"
            );
        }

        [Fact]
        public void RemovePayoffWhenCancellingAnExtraPaymentScheduledBeforeTheFirstInstallment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                                | 2014-12-30 | 2014-12-30  | pmt001    | Additional | Scheduled |        9000.00 |        101.01 |     0.00 |    101.01 |       8898.99
                                | 2015-01-06 | 2015-01-06  | pmt002    | Payoff     | Scheduled |        8898.99 |       9001.46 |   102.47 |   8898.99 |          0.00";

            AmortizationService.OnAmortize(new AmortizationRequest())
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |        9000.00 | 1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |        7542.37 | 1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |        6068.05 | 1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt001",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00"
            );
        }

        [Fact]
        public void RemovePayoffWhenCancellingAnExtraPaymentScheduledBetweenTwoOtherExtraPaymentsAllBeforeTheFirstInstallment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                                | 2014-12-23 | 2014-12-23  | pmt001    | Additional | Scheduled |        9000.00 |        101.01 |     0.00 |    101.01 |       8898.99
                                | 2014-12-30 | 2014-12-30  | pmt002    | Additional | Scheduled |        8898.99 |        202.02 |     0.00 |    202.02 |       8696.97
                                | 2015-01-05 | 2015-01-05  | pmt003    | Additional | Scheduled |        8696.97 |        303.03 |     0.00 |    303.03 |       8393.94
                                | 2015-01-12 | 2015-01-12  | pmt004    | Payoff     | Scheduled |        8393.94 |       8493.83 |    99.89 |   8393.94 |          0.00";

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 9000.00, Start = Loan.Terms.OriginationDate },
                new InterestAccrualPeriod { Principal = 8898.99, Start = Date(2014, 12, 23) },
                new InterestAccrualPeriod { Principal = 8595.96, Start = Date(2015, 1, 5) },
            })
            .Returns(101.05);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 1, 5),
                PrincipalBalance = 8595.96,
                OpeningInterest = 101.05
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |        8595.96 | 1560.68 |   101.05 |   1459.63 |       7136.33
                2015-02-15      | 2015-02-17 |        7136.33 | 1560.68 |    81.71 |   1478.97 |       5657.36
                2015-03-15      | 2015-03-17 |        5657.36 | 1560.68 |    64.78 |   1495.90 |       4161.46
                2015-04-15      | 2015-04-17 |        4161.46 | 1560.68 |    47.65 |   1513.03 |       2648.43
                2015-05-15      | 2015-05-17 |        2648.43 | 1560.68 |    30.32 |   1530.36 |       1118.07
                2015-06-15      | 2015-06-17 |        1118.07 | 1130.87 |    12.80 |   1118.07 |          0.00");

            Service.CancelPayment
            (
                paymentId: "pmt002",
                loanReferenceNumber: ReferenceNumber
            );

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                                | 2014-12-23 | 2014-12-23  | pmt001    | Additional | Scheduled |        9000.00 |        101.01 |     0.00 |    101.01 |       8898.99
                                | 2015-01-05 | 2015-01-05  | pmt003    | Additional | Scheduled |        8898.99 |        303.03 |     0.00 |    303.03 |       8595.96
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        8595.96 |       1560.68 |   101.05 |   1459.63 |       7136.33
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        7136.33 |       1560.68 |    81.71 |   1478.97 |       5657.36
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        5657.36 |       1560.68 |    64.78 |   1495.90 |       4161.46
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4161.46 |       1560.68 |    47.65 |   1513.03 |       2648.43
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2648.43 |       1560.68 |    30.32 |   1530.36 |       1118.07
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1118.07 |       1130.87 |    12.80 |   1118.07 |          0.00"
            );
        }
    }
}
