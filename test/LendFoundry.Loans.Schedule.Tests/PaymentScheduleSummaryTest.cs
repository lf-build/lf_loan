﻿using LendFoundry.Amortization.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Testing;
using LendFoundry.Loans.Repository;
using LendFoundry.Loans.Schedule.Calculators;
using LendFoundry.Loans.Schedule.Tests.Fake;
using Moq;
using Xunit;

namespace LendFoundry.Loans.Schedule.Tests
{
    public class PaymentScheduleSummaryTest
    {
        private const string LoanReferenceNumber = "loan001";
        private ILoanTerms Terms { get; } = new LoanTerms();
        private IPaymentScheduleService PaymentScheduleService { get; }
        private PaymentScheduleConfiguration Configuration { get; } = new PaymentScheduleConfiguration();
        private Mock<IAmortizationService> AmortizationServiceMock { get; } = new Mock<IAmortizationService>();
        private FakePaymentScheduleRepository PaymentScheduleRepository { get; } = new FakePaymentScheduleRepository();
        private StaticTodayTenantTime TenantTime { get; } = new StaticTodayTenantTime(new UtcTenantTime());

        public PaymentScheduleSummaryTest()
        {
            var loan = new Loan { Terms = Terms };

            PaymentScheduleService = new PaymentScheduleService
            (
                Configuration,
                AmortizationServiceMock.Object,
                PaymentScheduleRepository,
                Mock.Of<ILoanRepository>(r => r.Get(LoanReferenceNumber) == loan),
                Mock.Of<IPayoffAmountCalculator>(),
                Mock.Of<IGracePeriodCalculator>(),
                Mock.Of<IDaysPastDueCalculator>(),
                Mock.Of<ICurrentDueCalculator>(),
                Mock.Of<IEventHubClient>(),
                TenantTime,
                Mock.Of<ILogger>()
            );
        }

        private string Schedule
        {
            set
            {
                var installments = ListParser.Parse<Installment>(value);
                PaymentScheduleRepository.Create(Mock.Of<IPaymentSchedule>(s =>
                    s.LoanReferenceNumber == LoanReferenceNumber &&
                    s.Installments == installments
                ));
            }
        }

        private IPaymentScheduleSummary Summary => PaymentScheduleService.GetSummary(LoanReferenceNumber);

        private void TodayIs(int year, int month, int day)
        {
            TenantTime.Today = TenantTime.Create(year, month, day);
        }

        [Fact]
        public void LastPaymentDateIsThePaymentDateOfTheLastPaidInstallment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentAmount | Status   
                2015-01-19      | 2015-01-20 | 2015-01-21  |        100.00 | Completed
                2015-02-19      | 2015-02-20 | 2015-02-21  |        100.00 | Completed
                2015-03-19      | 2015-03-20 | 2015-03-21  |        100.00 | Scheduled";

            TodayIs(2015, 4, 1);

            Assert.Equal(TenantTime.Create(2015, 2, 21), Summary.LastPaymentDate);
        }

        [Fact]
        public void LastPaymentDateIsThePaymentDateOfTheLastPaidInstallmentWherePaymentAmountIsGreaterThanZero()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentAmount | Status   
                2015-01-19      | 2015-01-20 | 2015-01-21  |        100.00 | Completed
                2015-02-19      | 2015-02-20 | 2015-02-21  |          0.00 | Completed
                2015-03-19      | 2015-03-20 | 2015-03-21  |        100.00 | Scheduled";

            TodayIs(2015, 4, 1);

            Assert.Equal(TenantTime.Create(2015, 1, 21), Summary.LastPaymentDate);
        }

        [Fact]
        public void LastPaymentAmountIsTakenFromTheLastPaidInstallment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentAmount | Status   
                2015-01-19      | 2015-01-20 |        100.00 | Completed
                2015-02-19      | 2015-02-20 |        200.00 | Completed
                2015-03-19      | 2015-03-20 |        300.00 | Scheduled";

            TodayIs(2015, 4, 1);

            Assert.Equal(200.00, Summary.LastPaymentAmount);
        }

        [Fact]
        public void LastPaymentAmountIsTakenFromTheLastPaidInstallmentWherePaymentAmountIsGreaterThanZero()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentAmount | Status   
                2015-01-19      | 2015-01-20 |        100.00 | Completed
                2015-02-19      | 2015-02-20 |          0.00 | Completed
                2015-03-19      | 2015-03-20 |        300.00 | Scheduled";

            TodayIs(2015, 4, 1);

            Assert.Equal(100.00, Summary.LastPaymentAmount);
        }

        [Fact]
        public void LastPaymentDateIsNullWhenNoPaidInstallmentIsFound()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | Status   
                2015-01-19      | 2015-01-20 | 2015-01-21  | Scheduled
                2015-02-19      | 2015-02-20 | 2015-02-21  | Scheduled
                2015-03-19      | 2015-03-20 | 2015-03-21  | Scheduled";

            TodayIs(2015, 4, 1);

            Assert.Null(Summary.LastPaymentDate);
        }

        [Fact]
        public void LastPaymentAmountIsNullWhenNoPaidInstallmentIsFound()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | Status   
                2015-01-19      | 2015-01-20 | Scheduled
                2015-02-19      | 2015-02-20 | Scheduled
                2015-03-19      | 2015-03-20 | Scheduled";

            TodayIs(2015, 4, 1);

            Assert.Null(Summary.LastPaymentAmount);
        }

        [Fact]
        public void NextDueDateIsTakenFromTheFirstScheduledInstallment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | Status   
                2015-01-19      | 2015-01-20 | 2015-01-21  | Completed
                2015-02-19      | 2015-02-20 |             | Scheduled
                2015-03-19      | 2015-03-20 |             | Scheduled";

            TodayIs(2015, 4, 1);

            Assert.Equal(TenantTime.Create(2015, 2, 20), Summary.NextDueDate);
        }

        [Fact]
        public void NextDueDateIsNullWhenNoScheduledInstallmentIsFound()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | Status   
                2015-01-19      | 2015-01-20 | 2015-01-21  | Completed
                2015-02-19      | 2015-02-20 | 2015-02-21  | Completed
                2015-03-19      | 2015-03-20 | 2015-03-21  | Completed";

            TodayIs(2015, 4, 1);

            Assert.Null(Summary.NextDueDate);
        }

        [Fact]
        public void RemainingBalanceIsTheLoanAmountMinusTheSumOfAllPrincipalPaid()
        {
            TodayIs(2015, 6, 15);

            Terms.LoanAmount = 10000.0;

            Schedule = @"
                Principal | Status   
                   153.49 | Completed
                   120.08 | Completed
                   115.33 | Completed
                   112.99 | Scheduled
                   107.41 | Scheduled";

            var principalPaid = 153.49 + 120.08 + 115.33;
            var expectedRemainingBalance = Terms.LoanAmount - principalPaid;

            Assert.Equal(expectedRemainingBalance, Summary.RemainingBalance);
        }

        [Fact(Skip = "To do")]
        public void NextPaymentAmount()
        {
        }
    }
}
