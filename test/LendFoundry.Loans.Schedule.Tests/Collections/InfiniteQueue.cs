﻿using System.Collections.Generic;

namespace LendFoundry.Loans.Schedule.Tests.Collections
{
    internal class InfiniteQueue<T>
    {
        private Queue<T> InternalQueue { get; } = new Queue<T>();
        private T LastElement { get; set; }

        public void Enqueue(T e)
        {
            InternalQueue.Enqueue(e);
            LastElement = e;
        }

        public T Dequeue()
        {
            return InternalQueue.Count > 0 ? InternalQueue.Dequeue() : LastElement;
        }
    }
}
