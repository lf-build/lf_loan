﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Testing;
using System;
using System.Linq;

namespace LendFoundry.Loans.Schedule.Tests
{
    public abstract class BaseScheduleTest
    {
        protected IPaymentSchedule Schedule { get; } = new PaymentSchedule();
        protected ITenantTime TenantTime { get; } = new UtcTenantTime();
        protected DateTimeOffset Date(int year, int month, int day) => TenantTime.Create(year, month, day);
        protected void GivenThatScheduleIs(string table) => Schedule.Installments = ListParser.Parse<Installment>(table);
        protected IInstallment InstallmentWithAnniversaryOn(DateTimeOffset anniversaryDate) => Schedule.Installments.Single(i => i.AnniversaryDate == anniversaryDate);
        protected IInstallment InstallmentDueOn(DateTimeOffset dueDate) => Schedule.Installments.Single(i => i.DueDate == dueDate);
    }
}
