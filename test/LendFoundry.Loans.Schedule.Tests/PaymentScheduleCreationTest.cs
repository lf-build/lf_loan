﻿using LendFoundry.Amortization;
using LendFoundry.Amortization.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Testing;
using LendFoundry.Loans.Repository;
using LendFoundry.Loans.Schedule.Calculators;
using LendFoundry.Loans.Schedule.Tests.Fake;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Foundation.Logging;
using Xunit;

namespace LendFoundry.Loans.Schedule.Tests
{
    public class PaymentScheduleCreationTest
    {
        private IPaymentScheduleService service;
        private Mock<IAmortizationService> amortizationServiceMock;
        
        public PaymentScheduleCreationTest()
        {
            var scheduleRepository = new FakePaymentScheduleRepository();
            amortizationServiceMock = new Mock<IAmortizationService>();
            service = new PaymentScheduleService
            (
                new PaymentScheduleConfiguration(),
                amortizationServiceMock.Object,
                scheduleRepository,
                Mock.Of<ILoanRepository>(),
                Mock.Of<IPayoffAmountCalculator>(),
                Mock.Of<IGracePeriodCalculator>(),
                Mock.Of<IDaysPastDueCalculator>(),
                Mock.Of<ICurrentDueCalculator>(),
                Mock.Of<IEventHubClient>(),
                new UtcTenantTime(),
                Mock.Of<ILogger>()
            );
        }

        [Fact]
        public void CreatePaymentScheduleWithACHAsPaymentMethod()
        {
            var loan = new Loan
            {
                PaymentMethod = PaymentMethod.ACH,
                ReferenceNumber = "loan001",
                Terms = new LoanTerms
                {
                    OriginationDate = new DateTime(2014, 12, 15),
                    LoanAmount          = 9000.0,
                    Term            = 6,
                    Rate            = 13.74,
                    Fees            = new List<IFee>
                    {
                        new Fee { Type = FeeType.Fixed,   Amount = 100.0 },
                        new Fee { Type = FeeType.Percent, Amount = 2.7 }
                    },                    
                    ApplicationDate = new DateTime(2014, 12, 15),
                    APR = 12.00,
                    FundedDate = new DateTime(2014, 12, 15),
                    PaymentAmount = 50.00,
                    RateType = RateType.Factor
                }
            };

            OnAmortize(
                request: new AmortizationRequest
                {
                    LoanTerms = new Amortization.LoanTerms
                    {
                        OriginationDate = new DateTime(2014, 12, 15),
                        LoanAmount = 9000.0,
                        Term = 6,
                        Rate = 13.74,
                    }
                },
                result: ListParser.Parse<Amortization.Installment>(@"
                    # | DueDate    | AnniversaryDate | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                    1 | 2015-01-15 | 2015-01-14      |        9000.00 | 1560.68 |   103.05 |   1457.63 |       7542.37
                    2 | 2015-02-15 | 2015-02-14      |        7542.37 | 1560.68 |    86.36 |   1474.32 |       6068.05
                    3 | 2015-03-15 | 2015-03-14      |        6068.05 | 1560.68 |    69.48 |   1491.20 |       4576.85
                    4 | 2015-04-15 | 2015-04-14      |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                    5 | 2015-05-15 | 2015-05-14      |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                    6 | 2015-06-15 | 2015-06-14      |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00"
                )
            );

            service.Create(loan);

            var schedule = service.Get("loan001");

            Assert.NotNull(schedule);
            Assert.Equal("loan001", schedule.LoanReferenceNumber);

            AssertScheduleHasInstallments(schedule, @"
                # | DueDate    | AnniversaryDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Status    | Type      | PaymentMethod | PaymentDate | PaymentId 
                1 | 2015-01-15 | 2015-01-14      |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | Scheduled | Scheduled | ACH           |             |           
                2 | 2015-02-15 | 2015-02-14      |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | Scheduled | Scheduled | ACH           |             |           
                3 | 2015-03-15 | 2015-03-14      |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | Scheduled | Scheduled | ACH           |             |           
                4 | 2015-04-15 | 2015-04-14      |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | Scheduled | Scheduled | ACH           |             |           
                5 | 2015-05-15 | 2015-05-14      |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | Scheduled | Scheduled | ACH           |             |           
                6 | 2015-06-15 | 2015-06-14      |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | Scheduled | Scheduled | ACH           |             |           "
            );
        }

        [Fact]
        public void CreatePaymentScheduleWithCheckAsPaymentMethod()
        {
            var loan = new Loan
            {
                PaymentMethod = PaymentMethod.Check,
                ReferenceNumber = "loan002",
                Terms = new LoanTerms
                {
                    OriginationDate = new DateTime(2016, 1, 1),
                    LoanAmount          = 5000.0,
                    Term            = 3,
                    Rate            = 10.0,
                    Fees            = new List<IFee>
                    {
                        new Fee { Type = FeeType.Percent, Amount = 3.5 }
                    },                    
                }
            };

            OnAmortize(
                request: new AmortizationRequest
                {
                    LoanTerms = new Amortization.LoanTerms
                    {
                        OriginationDate = new DateTime(2016, 1, 1),
                        LoanAmount = 5000.0,
                        Term = 3,
                        Rate = 10.0
                    }
                },
                result: ListParser.Parse<Amortization.Installment>(@"
                    # | DueDate    | AnniversaryDate | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                    1 | 2016-02-02 | 2016-02-01      |        5000.00 | 1694.52 |    41.67 |   1652.85 |       3347.15
                    2 | 2016-03-02 | 2016-03-01      |        3347.15 | 1694.52 |    27.89 |   1666.63 |       1680.52
                    3 | 2016-04-02 | 2016-04-01      |        1680.52 | 1694.52 |    14.00 |   1680.52 |          0.00"
                )
            );

            service.Create(loan);

            var schedule = service.Get("loan002");

            Assert.NotNull(schedule);
            Assert.Equal("loan002", schedule.LoanReferenceNumber);

            AssertScheduleHasInstallments(schedule, @"
                # | DueDate    | AnniversaryDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Status    | Type      | PaymentMethod | PaymentDate | PaymentId 
                1 | 2016-02-02 | 2016-02-01      |        5000.00 |       1694.52 |    41.67 |   1652.85 |       3347.15 | Scheduled | Scheduled | Check         |             |           
                2 | 2016-03-02 | 2016-03-01      |        3347.15 |       1694.52 |    27.89 |   1666.63 |       1680.52 | Scheduled | Scheduled | Check         |             |            
                3 | 2016-04-02 | 2016-04-01      |        1680.52 |       1694.52 |    14.00 |   1680.52 |          0.00 | Scheduled | Scheduled | Check         |             |           "
            );
        }

        private void AssertScheduleHasInstallments(IPaymentSchedule schedule, string expectedInstallmentsTable)
        {
            AssertTable.Equal(expectedInstallmentsTable, schedule.Installments);
        }

        protected void OnAmortize(AmortizationRequest request, IEnumerable<Amortization.Installment> result)
        {
            amortizationServiceMock
                .Setup(service => service.Amortize(It.IsAny<AmortizationRequest>()))
                .Callback((AmortizationRequest actualRequest) => AssertEquals(request, actualRequest))
                .Returns(() => result);
        }

        private void AssertEquals(AmortizationRequest expected, AmortizationRequest actual)
        {
            Assert.NotNull(actual);
            Assert.Equal(expected.LoanTerms.OriginationDate, actual.LoanTerms.OriginationDate);
            Assert.Equal(expected.LoanTerms.LoanAmount, actual.LoanTerms.LoanAmount);
            Assert.Equal(expected.LoanTerms.Term, actual.LoanTerms.Term);
            Assert.Equal(expected.LoanTerms.Rate, actual.LoanTerms.Rate);

            Assert.Null(actual.LoanTerms.Fees);
            Assert.Null(actual.ReferenceDate);
            Assert.Null(actual.PrincipalBalance);
            Assert.Null(actual.OpeningInterest);
        }
    }
}