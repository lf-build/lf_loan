﻿using LendFoundry.Amortization.Client;
using LendFoundry.Loans.Repository;
using LendFoundry.Loans.Schedule.Data;
using Moq;
using System;
using LendFoundry.Foundation.Date;
using Xunit;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Schedule.Calculators;

namespace LendFoundry.Loans.Schedule.Tests
{
    public class GetLoansTest
    {
        private IPaymentScheduleService service;
        private Mock<IPaymentScheduleRepository> repositoryMock;
        private PaymentScheduleConfiguration config;
        private ITenantTime TenantTime = new UtcTenantTime();

        public GetLoansTest()
        {
            config = new PaymentScheduleConfiguration();
            repositoryMock = new Mock<IPaymentScheduleRepository>();
            service = new PaymentScheduleService
            (
                config,
                Mock.Of<IAmortizationService>(),
                repositoryMock.Object,
                Mock.Of<ILoanRepository>(),
                Mock.Of<IPayoffAmountCalculator>(),
                Mock.Of<IGracePeriodCalculator>(),
                Mock.Of<IDaysPastDueCalculator>(),
                Mock.Of<ICurrentDueCalculator>(),
                Mock.Of<IEventHubClient>(),
                new UtcTenantTime(),
                Mock.Of<ILogger>()
            );
        }

        [Fact]
        public void GetLoansDueWithinIntervalOfDays()
        {
            var minDaysDue = 16;
            var maxDaysDue = 30;

            var maxDueDate = TenantTime.Today.AddDays(-minDaysDue);
            var minDueDate = TenantTime.Today.AddDays(-maxDaysDue);

            var expectedLoanRefNumbers = new[] { "loan001", "loan002" };

            repositoryMock
                .Setup(repository => repository.GetLoans(InstallmentStatus.Scheduled, minDueDate, maxDueDate))
                .Returns(expectedLoanRefNumbers);

            Assert.Same(expectedLoanRefNumbers, service.GetLoans(minDaysDue, maxDaysDue));
        }
    }
}
