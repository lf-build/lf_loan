﻿using LendFoundry.Amortization;
using LendFoundry.Amortization.Interest;
using LendFoundry.Foundation.Testing;
using System;
using Xunit;

namespace LendFoundry.Loans.Schedule.Tests
{
    public abstract class AddPaymentTest : PaymentTestBase
    {
        public AddPaymentTest()
        {
            Loan.PaymentMethod = PaymentMethod;
        }

        protected abstract PaymentMethod PaymentMethod { get; }

        [Fact]
        public void OnTimeFirstPayment()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Status    | Type      | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 1, 17),
                PrincipalBalance = 7542.37
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance | Amount  | Interest | Principal | EndingBalance
                2015-02-15      | 2015-02-17 |        7542.37 | 1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |        6068.05 | 1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00
            ");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 1560.68,
                DueDate = Date(2015, 1, 15),
                PaymentId = "pmt001",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Status    | Type      | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void OnTimeThirdPayment()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 3, 17),
                PrincipalBalance = 4576.85
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00
            ");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 1560.68,
                DueDate = Date(2015, 3, 15),
                PaymentId = "pmt003",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 | 2015-03-15  | pmt003    | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void OnTimeLastPayment()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 | 2015-04-17  | pmt004    | Scheduled | Completed |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 | 2015-05-17  | pmt005    | Scheduled | Completed |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 6, 17),
                PrincipalBalance = 0.0
            })
            .Returns("");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 1560.70,
                DueDate = Date(2015, 6, 15),
                PaymentId = "pmt006",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 | 2015-04-17  | pmt004    | Scheduled | Completed |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 | 2015-05-17  | pmt005    | Scheduled | Completed |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 | 2015-06-15  | pmt006    | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void FirstPaymentOnDueDate()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Status    | Type      | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            var paymentDate = Date(2015, 1, 17);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = paymentDate,
                PrincipalBalance = 7542.37
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance | Amount  | Interest | Principal | EndingBalance
                2015-02-15      | 2015-02-17 |        7542.37 | 1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |        6068.05 | 1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00
            ");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 1560.68,
                DueDate = paymentDate,
                PaymentId = "pmt001",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Status    | Type      | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void ThirdPaymentOnDueDate()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            var paymentDate = Date(2015, 3, 17);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = paymentDate,
                PrincipalBalance = 4576.85
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 1560.68,
                DueDate = paymentDate,
                PaymentId = "pmt003",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void LastPaymentOnDueDate()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 | 2015-04-17  | pmt004    | Scheduled | Completed |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 | 2015-05-17  | pmt005    | Scheduled | Completed |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            var paymentDate = Date(2015, 6, 17);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = paymentDate,
                PrincipalBalance = 0.0
            })
            .Returns("");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 1560.70,
                DueDate = paymentDate,
                PaymentId = "pmt006",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 | 2015-04-17  | pmt004    | Scheduled | Completed |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 | 2015-05-17  | pmt005    | Scheduled | Completed |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 | 2015-06-17  | pmt006    | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void OnTimeUnderpaymentOf1stInstallment()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            var paymentDate = Date(2015, 1, 17);
            var paymentAmount = 999.99;
            var openingBalance = 9000.00;
            var interest = 103.05;
            var expectedPrincipalPaid = paymentAmount - interest;
            var expectedEndingBalance = openingBalance - expectedPrincipalPaid;

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = paymentDate,
                PrincipalBalance = expectedEndingBalance
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-02-15      | 2015-02-17 |        8103.06 | 1560.68 |    92.78 |   1467.90 |       6635.16
                2015-03-15      | 2015-03-17 |        6635.16 | 1560.68 |    75.97 |   1484.71 |       5150.45
                2015-04-15      | 2015-04-17 |        5150.45 | 1560.68 |    58.97 |   1501.71 |       3648.74
                2015-05-15      | 2015-05-17 |        3648.74 | 1560.68 |    41.78 |   1518.90 |       2129.84
                2015-06-15      | 2015-06-17 |        2129.84 | 2154.23 |    24.39 |   2129.84 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                Amount = paymentAmount,
                DueDate = paymentDate,
                PaymentId = "pmt001",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Scheduled |        9000.00 |        999.99 |   103.05 |    896.94 |       8103.06 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        8103.06 |       1560.68 |    92.78 |   1467.90 |       6635.16 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6635.16 |       1560.68 |    75.97 |   1484.71 |       5150.45 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        5150.45 |       1560.68 |    58.97 |   1501.71 |       3648.74 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3648.74 |       1560.68 |    41.78 |   1518.90 |       2129.84 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        2129.84 |       2154.23 |    24.39 |   2129.84 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void OnTimeUnderpaymentOf3rdInstallment()
        {
            Schedule = $@"
                DueDate    | AnniversaryDate | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-17 | 2015-01-15      | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-17 | 2015-02-15      | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-17 | 2015-03-15      |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-17 | 2015-04-15      |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-17 | 2015-05-15      |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-17 | 2015-06-15      |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            var paymentDate = Date(2015, 3, 17);
            var paymentAmount = 1000.0;
            var openingBalance = 6068.05;
            var interest = 69.48;
            var expectedPrincipalPaid = paymentAmount - interest;
            var expectedEndingBalance = openingBalance - expectedPrincipalPaid;

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = paymentDate,
                PrincipalBalance = expectedEndingBalance
            })
            .Returns(@"
                DueDate    | AnniversaryDate | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-04-17 | 2015-04-15      |        5137.53 | 1560.68 |    58.82 |   1501.86 |       3635.67
                2015-05-17 | 2015-05-15      |        3635.67 | 1560.68 |    41.63 |   1519.05 |       2116.62
                2015-06-17 | 2015-06-15      |        2116.62 | 2140.86 |    24.24 |   2116.62 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                Amount = paymentAmount,
                DueDate = paymentDate,
                PaymentId = "pmt003",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                DueDate    | AnniversaryDate | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-17 | 2015-01-15      | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-17 | 2015-02-15      | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-17 | 2015-03-15      | 2015-03-17  | pmt003    | Scheduled | Scheduled |        6068.05 |       1000.00 |    69.48 |    930.52 |       5137.53 | {PaymentMethod}
                2015-04-17 | 2015-04-15      |             |           | Scheduled | Scheduled |        5137.53 |       1560.68 |    58.82 |   1501.86 |       3635.67 | {PaymentMethod}
                2015-05-17 | 2015-05-15      |             |           | Scheduled | Scheduled |        3635.67 |       1560.68 |    41.63 |   1519.05 |       2116.62 | {PaymentMethod}
                2015-06-17 | 2015-06-15      |             |           | Scheduled | Scheduled |        2116.62 |       2140.86 |    24.24 |   2116.62 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void OnTimeUnderpaymentOfLastInstallment()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 | 2015-04-17  | pmt004    | Scheduled | Completed |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 | 2015-05-17  | pmt005    | Scheduled | Completed |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            var paymentDate = Date(2015, 6, 17);
            var paymentAmount = 999.99;
            var openingBalance = 1543.03;
            var interest = 17.67;
            var expectedPrincipalPaid = paymentAmount - interest;
            var expectedEndingBalance = openingBalance - expectedPrincipalPaid;

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = paymentDate,
                PrincipalBalance = expectedEndingBalance
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance | Amount | Interest | Principal | EndingBalance
                2015-07-15      | 2015-07-17 |         560.71 | 567.13 |     6.42 |    560.71 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                Amount = paymentAmount,
                DueDate = paymentDate,
                PaymentId = "pmt006",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 | 2015-04-17  | pmt004    | Scheduled | Completed |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 | 2015-05-17  | pmt005    | Scheduled | Completed |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 | 2015-06-17  | pmt006    | Scheduled | Scheduled |        1543.03 |        999.99 |    17.67 |    982.32 |        560.71 | {PaymentMethod}
                2015-07-15      | 2015-07-17 |             |           | Scheduled | Scheduled |         560.71 |        567.13 |     6.42 |    560.71 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void PreserveAndUpdateFutureExtraPaymentsOnReamortization()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt008    | Scheduled  | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                                | 2015-02-26 | 2015-02-26  | pmt002    | Additional | Scheduled |        6068.05 |         22.22 |     0.00 |     22.22 |       6045.83 | {PaymentMethod}
                                | 2015-03-01 | 2015-03-01  | pmt003    | Additional | Scheduled |        6045.83 |         33.33 |     0.00 |     33.33 |       6012.50 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        6012.50 |       1560.68 |    69.14 |   1491.54 |       4520.96 | {PaymentMethod}
                                | 2015-04-01 | 2015-04-01  | pmt004    | Additional | Scheduled |        4520.96 |         44.44 |     0.00 |     44.44 |       4476.52 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4476.52 |       1560.68 |    51.53 |   1509.15 |       2967.37 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2967.37 |       1560.68 |    33.98 |   1526.70 |       1440.67 | {PaymentMethod}
                                | 2015-05-20 | 2015-05-20  | pmt005    | Additional | Scheduled |        1440.67 |         55.55 |     0.00 |     55.55 |       1385.12 | {PaymentMethod}
                                | 2015-05-30 | 2015-05-30  | pmt006    | Additional | Scheduled |        1385.12 |         66.66 |     0.00 |     66.66 |       1318.46 | {PaymentMethod}
                                | 2015-06-10 | 2015-06-10  | pmt007    | Additional | Scheduled |        1318.46 |         77.77 |     0.00 |     77.77 |       1240.69 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1240.69 |       1256.12 |    15.44 |   1240.69 |          0.00 | {PaymentMethod}";

            AmortizationService.OnGetSimpleInterest(new []
            {
                new InterestAccrualPeriod { Principal = 6394.17, Start = Date(2015, 2, 15) },
                new InterestAccrualPeriod { Principal = 6371.95, Start = Date(2015, 2, 26) },
                new InterestAccrualPeriod { Principal = 6338.62, Start = Date(2015, 3, 01) }
            })
            .Returns(72.87);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 3, 1),
                PrincipalBalance = 6338.62,
                OpeningInterest = 72.87
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-03-15      | 2015-03-17 |        6338.62 | 1560.68 |    72.87 |   1487.81 |       4850.81
                2015-04-15      | 2015-04-17 |        4850.81 | 1560.68 |    55.54 |   1505.14 |       3345.67
                2015-05-15      | 2015-05-17 |        3345.67 | 1560.68 |    38.31 |   1522.37 |       1823.30
                2015-06-15      | 2015-06-17 |        1823.30 | 1844.18 |    20.88 |   1823.30 |          0.00");

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 4850.81, Start = Date(2015, 3, 15) },
                new InterestAccrualPeriod { Principal = 4806.37, Start = Date(2015, 4, 01) }
            })
            .Returns(55.30);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 4, 1),
                PrincipalBalance = 4806.37,
                OpeningInterest = 55.30
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        4806.37 | 1560.68 |    55.30 |   1505.38 |       3301.00
                2015-05-15      | 2015-05-17 |        3301.00 | 1560.68 |    37.80 |   1522.88 |       1778.12
                2015-06-15      | 2015-06-17 |        1778.12 | 1798.48 |    20.36 |   1778.12 |          0.00");

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 1778.12, Start = Date(2015, 5, 15) },
                new InterestAccrualPeriod { Principal = 1722.57, Start = Date(2015, 5, 20) },
                new InterestAccrualPeriod { Principal = 1655.91, Start = Date(2015, 5, 30) },
                new InterestAccrualPeriod { Principal = 1578.14, Start = Date(2015, 6, 10) }
            })
            .Returns(19.30);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 6, 10),
                PrincipalBalance = 1578.14,
                OpeningInterest = 19.30
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-06-15      | 2015-06-17 |        1578.14 | 1597.44 |    19.30 |   1578.14 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 1234.56,
                DueDate = Date(2015, 2, 17),
                PaymentId = "pmt008",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt008    | Scheduled  | Scheduled |        7542.37 |       1234.56 |    86.36 |   1148.20 |       6394.17 | {PaymentMethod}
                                | 2015-02-26 | 2015-02-26  | pmt002    | Additional | Scheduled |        6394.17 |         22.22 |     0.00 |     22.22 |       6371.95 | {PaymentMethod}
                                | 2015-03-01 | 2015-03-01  | pmt003    | Additional | Scheduled |        6371.95 |         33.33 |     0.00 |     33.33 |       6338.62 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        6338.62 |       1560.68 |    72.87 |   1487.81 |       4850.81 | {PaymentMethod}
                                | 2015-04-01 | 2015-04-01  | pmt004    | Additional | Scheduled |        4850.81 |         44.44 |     0.00 |     44.44 |       4806.37 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4806.37 |       1560.68 |    55.30 |   1505.38 |       3301.00 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        3301.00 |       1560.68 |    37.80 |   1522.88 |       1778.12 | {PaymentMethod}
                                | 2015-05-20 | 2015-05-20  | pmt005    | Additional | Scheduled |        1778.12 |         55.55 |     0.00 |     55.55 |       1722.57 | {PaymentMethod}
                                | 2015-05-30 | 2015-05-30  | pmt006    | Additional | Scheduled |        1722.57 |         66.66 |     0.00 |     66.66 |       1655.91 | {PaymentMethod}
                                | 2015-06-10 | 2015-06-10  | pmt007    | Additional | Scheduled |        1655.91 |         77.77 |     0.00 |     77.77 |       1578.14 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1578.14 |       1597.44 |    19.30 |   1578.14 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void PaymentInGracePeriod()
        {
            Schedule = $@"
                DueDate    | AnniversaryDate | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-17 | 2015-01-15      | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-17 | 2015-02-15      | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-17 | 2015-03-15      |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-17 | 2015-04-15      |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-17 | 2015-05-15      |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-17 | 2015-06-15      |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            GracePeriod.Duration = 10;

            var paymentDate = Date(2015, 3, 20);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 3, 17),
                PrincipalBalance = 4576.85
            })
            .Returns(@"
                DueDate    | AnniversaryDate | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-04-17 | 2015-04-15      |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-17 | 2015-05-15      |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-17 | 2015-06-15      |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 1560.68,
                DueDate = paymentDate,
                PaymentId = "pmt003",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                DueDate    | AnniversaryDate | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-17 | 2015-01-15      | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-17 | 2015-02-15      | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-17 | 2015-03-15      | 2015-03-20  | pmt003    | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-17 | 2015-04-15      |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-17 | 2015-05-15      |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-17 | 2015-06-15      |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void PaymentOnLastDayOfGracePeriod()
        {
            Schedule = $@"
                DueDate    | AnniversaryDate | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-17 | 2015-01-15      | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-17 | 2015-02-15      | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-17 | 2015-03-15      |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-17 | 2015-04-15      |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-17 | 2015-05-15      |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-17 | 2015-06-15      |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            GracePeriod.Duration = 10;

            var paymentDate = Date(2015, 3, 25);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 3, 17),
                PrincipalBalance = 4576.85
            })
            .Returns(@"
                DueDate    | AnniversaryDate | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-04-17 | 2015-04-15      |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-17 | 2015-05-15      |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-17 | 2015-06-15      |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                DueDate = paymentDate,
                Amount = 1560.68,
                PaymentId = "pmt003",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                DueDate    | AnniversaryDate | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-17 | 2015-01-15      | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-17 | 2015-02-15      | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-17 | 2015-03-15      | 2015-03-25  | pmt003    | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-17 | 2015-04-15      |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-17 | 2015-05-15      |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-17 | 2015-06-15      |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void ScheduledPaymentPastGracePeriodIsNotAllowed()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            GracePeriod.Duration = 8;

            AssertException.Throws<ArgumentException>("Cannot add a scheduled payment past grace period", () =>
            {
                Service.AddPayment(new PaymentInfo
                {
                    DueDate = Date(2015, 3, 26),
                    Amount = 1560.68,
                    PaymentId = "pmt003",
                    Type = InstallmentType.Scheduled,
                    LoanReferenceNumber = ReferenceNumber,
                    PaymentMethod = PaymentMethod
                });
            });

            AssertException.Throws<ArgumentException>("Cannot add a scheduled payment past grace period", () =>
            {
                Service.AddPayment(new PaymentInfo
                {
                    DueDate = Date(2015, 4, 18),
                    Amount = 1560.68,
                    PaymentId = "pmt003",
                    Type = InstallmentType.Scheduled,
                    LoanReferenceNumber = ReferenceNumber,
                    PaymentMethod = PaymentMethod
                });
            });
        }

        [Fact]
        public void ExtraPaymentAfterScheduledInstallment()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            var paymentDate = Date(2015, 2, 23);
            var paymentAmount = 111.11;
            var openingBalance = 6068.05;
            var principalPaid = paymentAmount;
            var endingBalance = openingBalance - principalPaid;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = openingBalance, Start = Date(2015, 2, 15) },
                new InterestAccrualPeriod { Principal = endingBalance,  Start = paymentDate }
            })
            .Returns(68.55);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = paymentDate,
                PrincipalBalance = endingBalance,
                OpeningInterest = 68.55
            })
            .Returns(@"
                DueDate    | AnniversaryDate | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-03-17 | 2015-03-15      |        5956.94 | 1560.68 |    68.55 |   1492.13 |       4464.81
                2015-04-17 | 2015-04-15      |        4464.81 | 1560.68 |    51.12 |   1509.56 |       2955.25
                2015-05-17 | 2015-05-15      |        2955.25 | 1560.68 |    33.84 |   1526.84 |       1428.41
                2015-06-17 | 2015-06-15      |        1428.41 | 1444.77 |    16.36 |   1428.41 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                DueDate = paymentDate,
                Amount = paymentAmount,
                PaymentId = "pmt001",
                Type = InstallmentType.Additional,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                                | 2015-02-23 | 2015-02-23  | pmt001    | Additional | Scheduled |        6068.05 |        111.11 |     0.00 |    111.11 |       5956.94 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        5956.94 |       1560.68 |    68.55 |   1492.13 |       4464.81 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4464.81 |       1560.68 |    51.12 |   1509.56 |       2955.25 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2955.25 |       1560.68 |    33.84 |   1526.84 |       1428.41 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1428.41 |       1444.77 |    16.36 |   1428.41 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void ExtraPaymentAfterCompletedInstallment()
        {
            Schedule = $@"
                DueDate    | AnniversaryDate | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-17 | 2015-01-15      | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-17 | 2015-02-15      | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-17 | 2015-03-15      |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-17 | 2015-04-15      |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-17 | 2015-05-15      |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-17 | 2015-06-15      |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            var paymentDate = Date(2015, 3, 1);
            var paymentAmount = 999.0;
            var openingBalance = 6068.05;
            var principalPaid = paymentAmount;
            var endingBalance = openingBalance - principalPaid;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = openingBalance, Start = Date(2015, 2, 15) },
                new InterestAccrualPeriod { Principal = endingBalance,  Start = paymentDate }
            })
            .Returns(64.14);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = paymentDate,
                PrincipalBalance = endingBalance,
                OpeningInterest = 64.14
            })
            .Returns(@"
                DueDate    | AnniversaryDate | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-03-17 | 2015-03-15      |        5069.05 | 1560.68 |    64.14 |   1496.54 |       3572.51
                2015-04-17 | 2015-04-15      |        3572.51 | 1560.68 |    40.91 |   1519.77 |       2052.74
                2015-05-17 | 2015-05-15      |        2052.74 | 1560.68 |    23.50 |   1537.18 |        515.56
                2015-06-17 | 2015-06-15      |         515.56 |  521.46 |     5.90 |    515.56 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                DueDate = paymentDate,
                Amount = paymentAmount,
                PaymentId = "pmt003",
                Type = InstallmentType.Additional,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                DueDate    | AnniversaryDate | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-17 | 2015-01-15      | 2015-01-17  | pmt001    | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-17 | 2015-02-15      | 2015-02-17  | pmt002    | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-01 |                 | 2015-03-01  | pmt003    | Additional | Scheduled |        6068.05 |        999.00 |     0.00 |    999.00 |       5069.05 | {PaymentMethod}
                2015-03-17 | 2015-03-15      |             |           | Scheduled  | Scheduled |        5069.05 |       1560.68 |    64.14 |   1496.54 |       3572.51 | {PaymentMethod}
                2015-04-17 | 2015-04-15      |             |           | Scheduled  | Scheduled |        3572.51 |       1560.68 |    40.91 |   1519.77 |       2052.74 | {PaymentMethod}
                2015-05-17 | 2015-05-15      |             |           | Scheduled  | Scheduled |        2052.74 |       1560.68 |    23.50 |   1537.18 |        515.56 | {PaymentMethod}
                2015-06-17 | 2015-06-15      |             |           | Scheduled  | Scheduled |         515.56 |        521.46 |     5.90 |    515.56 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void ExtraPaymentAfterExtraPayment()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                                | 2015-02-28 | 2015-02-28  | pmt003    | Additional | Scheduled |        6068.05 |        999.00 |     0.00 |    999.00 |       5069.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        5069.05 |       1560.68 |    59.13 |   1501.55 |       3567.50 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        3567.50 |       1560.68 |    40.85 |   1519.83 |       2047.67 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2047.67 |       1560.68 |    23.45 |   1537.23 |        510.44 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |         510.44 |        516.28 |     5.84 |    510.44 |          0.00 | {PaymentMethod}";

            var paymentDate = Date(2015, 3, 4);
            var paymentAmount = 333.0;
            var openingBalance = 5069.05;
            var principalPaid = paymentAmount;
            var endingBalance = openingBalance - principalPaid;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 6068.05, Start = Date(2015, 2, 15) },
                new InterestAccrualPeriod { Principal = 5069.05, Start = Date(2015, 2, 28) },
                new InterestAccrualPeriod { Principal = endingBalance, Start = paymentDate }
            })
            .Returns(57.73);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = paymentDate,
                PrincipalBalance = endingBalance,
                OpeningInterest = 57.73
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-03-15      | 2015-03-17 |        4736.05 | 1560.68 |    57.73 |   1502.95 |       3233.10
                2015-04-15      | 2015-04-17 |        3233.10 | 1560.68 |    37.02 |   1523.66 |       1709.44
                2015-05-15      | 2015-05-17 |        1709.44 | 1560.68 |    19.57 |   1541.11 |        168.33
                2015-06-15      | 2015-06-17 |         168.33 |  170.26 |     1.93 |    168.33 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                DueDate = paymentDate,
                Amount = paymentAmount,
                PaymentId = "pmt004",
                Type = InstallmentType.Additional,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                                | 2015-02-28 | 2015-02-28  | pmt003    | Additional | Scheduled |        6068.05 |        999.00 |     0.00 |    999.00 |       5069.05 | {PaymentMethod}
                                | 2015-03-04 | 2015-03-04  | pmt004    | Additional | Scheduled |        5069.05 |        333.00 |     0.00 |    333.00 |       4736.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        4736.05 |       1560.68 |    57.73 |   1502.95 |       3233.10 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        3233.10 |       1560.68 |    37.02 |   1523.66 |       1709.44 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        1709.44 |       1560.68 |    19.57 |   1541.11 |        168.33 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |         168.33 |        170.26 |     1.93 |    168.33 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void ExtraPaymentBeforeExtraPayment()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                                | 2015-03-04 | 2015-03-04  | pmt003    | Additional | Scheduled |        6068.05 |        444.00 |     0.00 |    444.00 |       5624.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        5624.05 |       1560.68 |    67.62 |   1493.06 |       4130.99 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4130.99 |       1560.68 |    47.30 |   1513.38 |       2617.61 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2617.61 |       1560.68 |    29.97 |   1530.71 |       1086.90 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1086.90 |       1099.34 |    12.44 |   1086.90 |          0.00 | {PaymentMethod}";

            var paymentDate = Date(2015, 2, 28);
            var paymentAmount = 888.0;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 6068.05, Start = Date(2015, 2, 15) },
                new InterestAccrualPeriod { Principal = 5180.05, Start = Date(2015, 2, 28) },
                new InterestAccrualPeriod { Principal = 4736.05, Start = Date(2015, 3, 04) }
            })
            .Returns(57.90);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 3, 4),
                PrincipalBalance = 4736.05,
                OpeningInterest = 57.90
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-03-15      | 2015-03-17 |        4736.05 | 1560.68 |    57.90 |   1502.78 |       3233.27
                2015-04-15      | 2015-04-17 |        3233.27 | 1560.68 |    37.02 |   1523.66 |       1709.61
                2015-05-15      | 2015-05-17 |        1709.61 | 1560.68 |    19.58 |   1541.10 |        168.51
                2015-06-15      | 2015-06-17 |         168.51 |  170.44 |     1.93 |    168.51 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                DueDate = paymentDate,
                Amount = paymentAmount,
                PaymentId = "pmt004",
                Type = InstallmentType.Additional,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                                | 2015-02-28 | 2015-02-28  | pmt004    | Additional | Scheduled |        6068.05 |        888.00 |     0.00 |    888.00 |       5180.05 | {PaymentMethod}
                                | 2015-03-04 | 2015-03-04  | pmt003    | Additional | Scheduled |        5180.05 |        444.00 |     0.00 |    444.00 |       4736.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        4736.05 |       1560.68 |    57.90 |   1502.78 |       3233.27 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        3233.27 |       1560.68 |    37.02 |   1523.66 |       1709.61 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        1709.61 |       1560.68 |    19.58 |   1541.10 |        168.51 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |         168.51 |        170.44 |     1.93 |    168.51 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void ExtraPaymentInBetweenTwoExtraPayments()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled  | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                                | 2015-03-25 | 2015-03-25  | pmt004    | Additional | Scheduled |        4576.85 |        111.11 |     0.00 |    111.11 |       4465.74 | {PaymentMethod}
                                | 2015-04-09 | 2015-04-09  | pmt005    | Additional | Scheduled |        4465.74 |        222.22 |     0.00 |    222.22 |       4243.52 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4243.52 |       1560.68 |    51.05 |   1509.63 |       2733.89 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2733.89 |       1560.68 |    31.30 |   1529.38 |       1204.51 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1204.51 |       1218.30 |    13.79 |   1204.51 |          0.00 | {PaymentMethod}";

            var paymentDate = Date(2015, 3, 30);
            var paymentAmount = 333.33;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 4576.85, Start = Date(2015, 3, 15) },
                new InterestAccrualPeriod { Principal = 4465.74, Start = Date(2015, 3, 25) },
                new InterestAccrualPeriod { Principal = 4132.41, Start = Date(2015, 3, 30) },
                new InterestAccrualPeriod { Principal = 3910.19, Start = Date(2015, 4, 09) },
            })
            .Returns(49.14);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 4, 9),
                PrincipalBalance = 3910.19,
                OpeningInterest = 49.14
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        3910.19 | 1560.68 |    49.14 |   1511.54 |       2398.65
                2015-05-15      | 2015-05-17 |        2398.65 | 1560.68 |    27.46 |   1533.22 |        865.43
                2015-06-15      | 2015-06-17 |         865.43 |  875.34 |     9.91 |    865.43 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                DueDate = paymentDate,
                Amount = paymentAmount,
                PaymentId = "pmt006",
                Type = InstallmentType.Additional,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 | 2015-03-17  | pmt003    | Scheduled  | Completed |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                                | 2015-03-25 | 2015-03-25  | pmt004    | Additional | Scheduled |        4576.85 |        111.11 |     0.00 |    111.11 |       4465.74 | {PaymentMethod}
                                | 2015-03-30 | 2015-03-30  | pmt006    | Additional | Scheduled |        4465.74 |        333.33 |     0.00 |    333.33 |       4132.41 | {PaymentMethod}
                                | 2015-04-09 | 2015-04-09  | pmt005    | Additional | Scheduled |        4132.41 |        222.22 |     0.00 |    222.22 |       3910.19 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        3910.19 |       1560.68 |    49.14 |   1511.54 |       2398.65 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2398.65 |       1560.68 |    27.46 |   1533.22 |        865.43 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |         865.43 |        875.34 |     9.91 |    865.43 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact(Skip = "TODO")]
        public void ExtraPaymentOnSameDateAsAnotherExtraPayment()
        {
        }

        [Fact(Skip = "TODO")]
        public void ExtraPaymentOnSameDateAsOtherTwoExtraPayments()
        {
        }

        [Fact(Skip = "TODO")]
        public void ExtraPaymentOnSameDateAsScheduledPayment()
        {
        }

        [Fact(Skip = "TODO")]
        public void ThreeExtraPaymentsOnSameDateAsScheduledPayment()
        {
        }

        [Fact]
        public void AnticipatedFirstPayment()
        {
            Schedule = $@"
                DueDate    | AnniversaryDate | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-17 | 2015-01-15      |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-17 | 2015-02-15      |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-17 | 2015-03-15      |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-17 | 2015-04-15      |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-17 | 2015-05-15      |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-17 | 2015-06-15      |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            var paymentDate = Date(2015, 1, 1);
            var paymentAmount = 1560.68;
            var openingBalance = Loan.Terms.LoanAmount;
            var endingBalance = openingBalance - paymentAmount;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = openingBalance, Start = Loan.Terms.OriginationDate },
                new InterestAccrualPeriod { Principal = endingBalance, Start = paymentDate }
            })
            .Returns(94.71);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = paymentDate,
                PrincipalBalance = endingBalance,
                OpeningInterest = 94.71
            })
            .Returns(@"
                DueDate    | AnniversaryDate | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-01-17 | 2015-01-15      |        7439.32 | 1560.68 |    94.71 |   1465.97 |       5973.35
                2015-02-17 | 2015-02-15      |        5973.35 | 1560.68 |    68.39 |   1492.29 |       4481.06
                2015-03-17 | 2015-03-15      |        4481.06 | 1560.68 |    51.31 |   1509.37 |       2971.69
                2015-04-17 | 2015-04-15      |        2971.69 | 1560.68 |    34.03 |   1526.65 |       1445.04
                2015-05-17 | 2015-05-15      |        1445.04 | 1461.59 |    16.55 |   1445.04 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                Amount = paymentAmount,
                DueDate = paymentDate,
                PaymentId = "pmt001",
                Type = InstallmentType.Additional,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                DueDate    | AnniversaryDate | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-01 |                 | 2015-01-01  | pmt001    | Additional | Scheduled |        9000.00 |       1560.68 |     0.00 |   1560.68 |       7439.32 | {PaymentMethod}
                2015-01-17 | 2015-01-15      |             |           | Scheduled  | Scheduled |        7439.32 |       1560.68 |    94.71 |   1465.97 |       5973.35 | {PaymentMethod}
                2015-02-17 | 2015-02-15      |             |           | Scheduled  | Scheduled |        5973.35 |       1560.68 |    68.39 |   1492.29 |       4481.06 | {PaymentMethod}
                2015-03-17 | 2015-03-15      |             |           | Scheduled  | Scheduled |        4481.06 |       1560.68 |    51.31 |   1509.37 |       2971.69 | {PaymentMethod}
                2015-04-17 | 2015-04-15      |             |           | Scheduled  | Scheduled |        2971.69 |       1560.68 |    34.03 |   1526.65 |       1445.04 | {PaymentMethod}
                2015-05-17 | 2015-05-15      |             |           | Scheduled  | Scheduled |        1445.04 |       1461.59 |    16.55 |   1445.04 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void ExtraPaymentAfterAnticipatedFirstPayment()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                                | 2014-12-27 | 2014-12-27  | pmt001    | Additional | Scheduled |        9000.00 |       1560.68 |     0.00 |   1560.68 |       7439.32 | {PaymentMethod}
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        7439.32 |       1560.68 |    92.33 |   1468.35 |       5970.97 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        5970.97 |       1560.68 |    68.37 |   1492.31 |       4478.66 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        4478.66 |       1560.68 |    51.28 |   1509.40 |       2969.26 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        2969.26 |       1560.68 |    34.00 |   1526.68 |       1442.58 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        1442.58 |       1459.10 |    16.52 |   1442.58 |          0.00 | {PaymentMethod}";

            var paymentDate = Date(2015, 1,4);
            var paymentAmount = 473.00;
            var openingBalance = 7439.32;
            var endingBalance = openingBalance - paymentAmount;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 9000.00, Start = Loan.Terms.OriginationDate },
                new InterestAccrualPeriod { Principal = 7439.32, Start = Date(2014, 12, 27) },
                new InterestAccrualPeriod { Principal = endingBalance, Start = paymentDate },
            })
            .Returns(90.34);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = paymentDate,
                PrincipalBalance = endingBalance,
                OpeningInterest = 90.34
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |        6966.32 | 1560.68 |    90.34 |   1470.34 |       5495.98
                2015-02-15      | 2015-02-17 |        5495.98 | 1560.68 |    62.93 |   1497.75 |       3998.23
                2015-03-15      | 2015-03-17 |        3998.23 | 1560.68 |    45.78 |   1514.90 |       2483.33
                2015-04-15      | 2015-04-17 |        2483.33 | 1560.68 |    28.43 |   1532.25 |        951.08
                2015-05-15      | 2015-05-17 |         951.08 |  961.97 |    10.89 |    951.08 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                Amount = paymentAmount,
                DueDate = paymentDate,
                PaymentId = "pmt002",
                Type = InstallmentType.Additional,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                                | 2014-12-27 | 2014-12-27  | pmt001    | Additional | Scheduled |        9000.00 |       1560.68 |     0.00 |   1560.68 |       7439.32 | {PaymentMethod}
                                | 2015-01-04 | 2015-01-04  | pmt002    | Additional | Scheduled |        7439.32 |        473.00 |     0.00 |    473.00 |       6966.32 | {PaymentMethod}
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        6966.32 |       1560.68 |    90.34 |   1470.34 |       5495.98 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        5495.98 |       1560.68 |    62.93 |   1497.75 |       3998.23 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        3998.23 |       1560.68 |    45.78 |   1514.90 |       2483.33 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        2483.33 |       1560.68 |    28.43 |   1532.25 |        951.08 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |         951.08 |        961.97 |    10.89 |    951.08 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void ExtraPaymentBeforeAnticipatedFirstPayment()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                                | 2015-01-04 | 2015-01-04  | pmt001    | Additional | Scheduled |        9000.00 |       1560.68 |     0.00 |   1560.68 |       7439.32 | {PaymentMethod}
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        7439.32 |       1560.68 |    96.50 |   1464.18 |       5975.14 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        5975.14 |       1560.68 |    68.42 |   1492.26 |       4482.88 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        4482.88 |       1560.68 |    51.33 |   1509.35 |       2973.53 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        2973.53 |       1560.68 |    34.05 |   1526.63 |       1446.90 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        1446.90 |       1463.47 |    16.57 |   1446.90 |          0.00 | {PaymentMethod}";

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 9000.00, Start = Loan.Terms.OriginationDate },
                new InterestAccrualPeriod { Principal = 8527.00, Start = Date(2014, 12, 27) },
                new InterestAccrualPeriod { Principal = 6966.32, Start = Date(2015, 1, 4) },
            })
            .Returns(93.25);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 1, 4),
                PrincipalBalance = 6966.32,
                OpeningInterest = 93.25
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |        6966.32 | 1560.68 |    93.25 |   1467.43 |       5498.89
                2015-02-15      | 2015-02-17 |        5498.89 | 1560.68 |    62.96 |   1497.72 |       4001.17
                2015-03-15      | 2015-03-17 |        4001.17 | 1560.68 |    45.81 |   1514.87 |       2486.30
                2015-04-15      | 2015-04-17 |        2486.30 | 1560.68 |    28.47 |   1532.21 |        954.09
                2015-05-15      | 2015-05-17 |         954.09 |  965.01 |    10.92 |    954.09 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 473.00,
                DueDate = Date(2014, 12, 27),
                PaymentId = "pmt002",
                Type = InstallmentType.Additional,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                                | 2014-12-27 | 2014-12-27  | pmt002    | Additional | Scheduled |        9000.00 |        473.00 |     0.00 |    473.00 |       8527.00 | {PaymentMethod}
                                | 2015-01-04 | 2015-01-04  | pmt001    | Additional | Scheduled |        8527.00 |       1560.68 |     0.00 |   1560.68 |       6966.32 | {PaymentMethod}
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        6966.32 |       1560.68 |    93.25 |   1467.43 |       5498.89 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        5498.89 |       1560.68 |    62.96 |   1497.72 |       4001.17 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        4001.17 |       1560.68 |    45.81 |   1514.87 |       2486.30 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        2486.30 |       1560.68 |    28.47 |   1532.21 |        954.09 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |         954.09 |        965.01 |    10.92 |    954.09 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void ExtraPaymentInBetweenTwoExtraPaymentsBeforeFirstPayment()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                                | 2014-12-23 | 2014-12-23  | pmt002    | Additional | Scheduled |        9000.00 |        111.11 |     0.00 |    111.11 |       8888.89 | {PaymentMethod}
                                | 2015-01-04 | 2015-01-04  | pmt001    | Additional | Scheduled |        8888.89 |        222.22 |     0.00 |    222.22 |       8666.67 | {PaymentMethod}
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        8666.67 |       1560.68 |   101.18 |   1459.50 |       7207.17 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        7207.17 |       1560.68 |    82.52 |   1478.16 |       5729.01 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        5729.01 |       1560.68 |    65.60 |   1495.08 |       4233.93 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4233.93 |       1560.68 |    48.48 |   1512.20 |       2721.73 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2721.73 |       2752.89 |    31.16 |   2721.73 |          0.00 | {PaymentMethod}";

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 9000.00, Start = Loan.Terms.OriginationDate },
                new InterestAccrualPeriod { Principal = 8888.89, Start = Date(2014, 12, 23) },
                new InterestAccrualPeriod { Principal = 8555.56, Start = Date(2014, 12, 27) },
                new InterestAccrualPeriod { Principal = 8333.34, Start = Date(2015, 01, 04) },
            })
            .Returns(98.89);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 1, 4),
                PrincipalBalance = 8333.34,
                OpeningInterest = 98.89
            })
            .Returns(@"
                    AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                    2015-01-15      | 2015-01-17 |        8333.34 | 1560.68 |    98.89 |   1461.79 |       6871.55
                    2015-02-15      | 2015-02-17 |        6871.55 | 1560.68 |    78.68 |   1482.00 |       5389.55
                    2015-03-15      | 2015-03-17 |        5389.55 | 1560.68 |    61.71 |   1498.97 |       3890.58
                    2015-04-15      | 2015-04-17 |        3890.58 | 1560.68 |    44.55 |   1516.13 |       2374.45
                    2015-05-15      | 2015-05-17 |        2374.45 | 2401.64 |    27.19 |   2374.45 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 333.33,
                DueDate = Date(2014, 12, 27),
                PaymentId = "pmt003",
                Type = InstallmentType.Additional,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                                | 2014-12-23 | 2014-12-23  | pmt002    | Additional | Scheduled |        9000.00 |        111.11 |     0.00 |    111.11 |       8888.89 | {PaymentMethod}
                                | 2014-12-27 | 2014-12-27  | pmt003    | Additional | Scheduled |        8888.89 |        333.33 |     0.00 |    333.33 |       8555.56 | {PaymentMethod}
                                | 2015-01-04 | 2015-01-04  | pmt001    | Additional | Scheduled |        8555.56 |        222.22 |     0.00 |    222.22 |       8333.34 | {PaymentMethod}
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        8333.34 |       1560.68 |    98.89 |   1461.79 |       6871.55 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        6871.55 |       1560.68 |    78.68 |   1482.00 |       5389.55 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        5389.55 |       1560.68 |    61.71 |   1498.97 |       3890.58 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        3890.58 |       1560.68 |    44.55 |   1516.13 |       2374.45 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2374.45 |       2401.64 |    27.19 |   2374.45 |          0.00 | {PaymentMethod}");
        }

        [Fact]
        public void PayoffBeforeFirstInstallment()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            var paymentDate = Date(2014, 12, 30);

            AmortizationService.FailOnAmortize();

            Service.AddPayment(new PaymentInfo
            {
                DueDate = paymentDate,
                Amount = 9051.53,
                PaymentId = "pmt001",
                Type = InstallmentType.Payoff,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type   | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                                | 2014-12-30 | 2014-12-30  | pmt001    | Payoff | Scheduled |        9000.00 |       9051.53 |    51.53 |   9000.00 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void PayoffOnFirstAnniversaryDate()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            var paymentDate = Date(2015, 1, 15);

            AmortizationService.FailOnAmortize();

            Service.AddPayment(new PaymentInfo
            {
                DueDate = paymentDate,
                Amount = 9103.05,
                PaymentId = "pmt001",
                Type = InstallmentType.Payoff,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type   | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                                | 2015-01-15 | 2015-01-15  | pmt001    | Payoff | Scheduled |        9000.00 |       9103.05 |   103.05 |   9000.00 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void PayoffOnFirstDueDate()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            var paymentDate = Date(2015, 1, 17);

            AmortizationService.FailOnAmortize();

            Service.AddPayment(new PaymentInfo
            {
                DueDate = paymentDate,
                Amount = 9109.92,
                PaymentId = "pmt001",
                Type = InstallmentType.Payoff,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type   | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                                | 2015-01-17 | 2015-01-17  | pmt001    | Payoff | Scheduled |        9000.00 |       9109.92 |   109.92 |   9000.00 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void PayoffAfterFirstDueDate()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            TenantTime.Today = Date(2015, 1, 1);

            var paymentDate = Date(2015, 1, 23);

            AmortizationService.FailOnAmortize();

            Service.AddPayment(new PaymentInfo
            {
                DueDate = paymentDate,
                Amount = 7565.40,
                PaymentId = "pmt001",
                Type = InstallmentType.Payoff,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                                | 2015-01-23 | 2015-01-23  | pmt001    | Payoff    | Scheduled |        7542.37 |       7565.40 |    23.03 |   7542.37 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void PayoffAfterFirstPayment()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            var paymentDate = Date(2015, 1, 23);

            AmortizationService.FailOnAmortize();

            Service.AddPayment(new PaymentInfo
            {
                DueDate = paymentDate,
                Amount = 7565.40,
                PaymentId = "pmt002",
                Type = InstallmentType.Payoff,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                                | 2015-01-23 | 2015-01-23  | pmt002    | Payoff    | Scheduled |        7542.37 |       7565.40 |    23.03 |   7542.37 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void PayoffOnThirdAnniversaryDate()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            TenantTime.Today = Date(2015, 1, 1);

            var payoffDate = Date(2015, 3, 15);

            Service.AddPayment(new PaymentInfo
            {
                DueDate = payoffDate,
                Amount = 6137.53,
                PaymentId = "pmt001",
                Type = InstallmentType.Payoff,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                                | 2015-03-15 | 2015-03-15  | pmt001    | Payoff    | Scheduled |        6068.05 |       6137.53 |    69.48 |   6068.05 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void PayoffOnThirdDueDate()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            TenantTime.Today = Date(2015, 1, 1);

            var payoffDate = Date(2015, 3, 17);

            Service.AddPayment(new PaymentInfo
            {
                DueDate = payoffDate,
                Amount = 6142.16,
                PaymentId = "pmt001",
                Type = InstallmentType.Payoff,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                                | 2015-03-17 | 2015-03-17  | pmt001    | Payoff    | Scheduled |        6068.05 |       6142.16 |    74.11 |   6068.05 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void PayoffAfterThirdDueDate()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            TenantTime.Today = Date(2015, 1, 1);

            var payoffDate = Date(2015, 3, 28);

            Service.AddPayment(new PaymentInfo
            {
                DueDate = payoffDate,
                Amount = 4599.56,
                PaymentId = "pmt001",
                Type = InstallmentType.Payoff,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                                | 2015-03-28 | 2015-03-28  | pmt001    | Payoff    | Scheduled |        4576.85 |       4599.56 |    22.71 |   4576.85 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void PayoffAfterExtraPayment()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                                | 2015-02-20 | 2015-02-20  | pmt001    | Additional | Scheduled |        6068.05 |        100.00 |     0.00 |    100.00 |       5968.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        5968.05 |       1560.68 |    68.53 |   1492.15 |       4475.90 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4475.90 |       1560.68 |    51.25 |   1509.43 |       2966.47 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2966.47 |       1560.68 |    33.97 |   1526.71 |       1439.76 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1439.76 |       1456.25 |    16.49 |   1439.76 |          0.00 | {PaymentMethod}";

            var payoffDate = Date(2015, 2, 28);

            Service.AddPayment(new PaymentInfo
            {
                DueDate = payoffDate,
                Amount = 5997.85,
                PaymentId = "pmt002",
                Type = InstallmentType.Payoff,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                                | 2015-02-20 | 2015-02-20  | pmt001    | Additional | Scheduled |        6068.05 |        100.00 |     0.00 |    100.00 |       5968.05 | {PaymentMethod}
                                | 2015-02-28 | 2015-02-28  | pmt002    | Payoff     | Scheduled |        5968.05 |       5997.85 |    29.80 |   5968.05 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void PayoffAfterTwoExtraPayments()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                                | 2015-02-21 | 2015-02-21  | pmt001    | Additional | Scheduled |        6068.05 |        111.11 |     0.00 |    111.11 |       5956.94 | {PaymentMethod}
                                | 2015-03-01 | 2015-03-01  | pmt002    | Additional | Scheduled |        5956.94 |        222.22 |     0.00 |    222.22 |       5734.72 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            var payoffDate = Date(2015, 3, 9);

            Service.AddPayment(new PaymentInfo
            {
                DueDate = payoffDate,
                Amount = 5788.86,
                PaymentId = "pmt003",
                Type = InstallmentType.Payoff,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                                | 2015-02-21 | 2015-02-21  | pmt001    | Additional | Scheduled |        6068.05 |        111.11 |     0.00 |    111.11 |       5956.94 | {PaymentMethod}
                                | 2015-03-01 | 2015-03-01  | pmt002    | Additional | Scheduled |        5956.94 |        222.22 |     0.00 |    222.22 |       5734.72 | {PaymentMethod}
                                | 2015-03-09 | 2015-03-09  | pmt003    | Payoff     | Scheduled |        5734.72 |       5788.86 |    54.14 |   5734.72 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void PayoffAfterPastDueInstallment()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt1      | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00";

            TenantTime.Today = Date(2015, 2, 20);

            var payoffDate = Date(2015, 2, 28);

            Service.AddPayment(new PaymentInfo
            {
                DueDate = payoffDate,
                Amount = 7666.15,
                PaymentId = "pmt2",
                Type = InstallmentType.Payoff,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt1      | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Completed |        7542.37 |          0.00 |     0.00 |      0.00 |       7542.37
                                | 2015-02-28 | 2015-02-28  | pmt2      | Payoff    | Scheduled |        7542.37 |       7666.15 |   123.78 |   7542.37 |          0.00"
            );
        }

        [Fact]
        public void PayoffRemovesExtraPaymentsPastThePayoffDate()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                                | 2015-04-28 | 2015-04-28  | pmt001    | Additional | Scheduled |        3068.57 |        275.00 |     0.00 |    275.00 |       2793.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2793.57 |       1560.68 |    33.35 |   1527.33 |       1266.24 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1266.24 |       1280.74 |    14.50 |   1266.24 |          0.00 | {PaymentMethod}";

            TenantTime.Today = Date(2015, 1, 1);

            var payoffDate = Date(2015, 3, 28);

            Service.AddPayment(new PaymentInfo
            {
                DueDate = payoffDate,
                Amount = 4599.56,
                PaymentId = "pmt002",
                Type = InstallmentType.Payoff,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                                | 2015-03-28 | 2015-03-28  | pmt002    | Payoff    | Scheduled |        4576.85 |       4599.56 |    22.71 |   4576.85 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void PreservePayoffWhenScheduledPaymentHasExpectedPaymentAmount()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                                | 2015-03-24 | 2015-03-24  | pmt001    | Payoff    | Scheduled |        4576.85 |       4592.57 |    15.72 |   4576.85 |          0.00 | {PaymentMethod}";

            var paymentDate = Date(2015, 1, 17);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 1, 17),
                PrincipalBalance = 7542.37
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance | Amount  | Interest | Principal | EndingBalance
                2015-02-15      | 2015-02-17 |        7542.37 | 1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |        6068.05 | 1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |        4576.85 | 1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |        3068.57 | 1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |        1543.03 | 1560.70 |    17.67 |   1543.03 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                DueDate = paymentDate,
                Amount = 1560.68,
                PaymentId = "pmt002",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt002    | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                                | 2015-03-24 | 2015-03-24  | pmt001    | Payoff    | Scheduled |        4576.85 |       4592.57 |    15.72 |   4576.85 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void RemovePayoffWhenScheduledPaymentDoesNotHaveTheExpectedAmount()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                                | 2015-03-24 | 2015-03-24  | pmt001    | Payoff    | Scheduled |        4576.85 |       4592.57 |    15.72 |   4576.85 |          0.00 | {PaymentMethod}";

            var paymentDate = Date(2015, 1, 17);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 1, 17),
                PrincipalBalance = 8103.06
            })
            .Returns(@"
                DueDate    | AnniversaryDate | OpeningBalance | Amount  | Interest | Principal | EndingBalance
                2015-02-17 | 2015-02-15      |        8103.06 | 1560.68 |    92.78 |   1467.90 |       6635.16
                2015-03-17 | 2015-03-15      |        6635.16 | 1560.68 |    75.97 |   1484.71 |       5150.45
                2015-04-17 | 2015-04-15      |        5150.45 | 1560.68 |    58.97 |   1501.71 |       3648.74
                2015-05-17 | 2015-05-15      |        3648.74 | 1560.68 |    41.78 |   1518.90 |       2129.84
                2015-06-17 | 2015-06-15      |        2129.84 | 2154.23 |    24.39 |   2129.84 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                DueDate = paymentDate,
                Amount = 999.99,
                PaymentId = "pmt002",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt002    | Scheduled | Scheduled |        9000.00 |        999.99 |   103.05 |    896.94 |       8103.06 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        8103.06 |       1560.68 |    92.78 |   1467.90 |       6635.16 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6635.16 |       1560.68 |    75.97 |   1484.71 |       5150.45 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        5150.45 |       1560.68 |    58.97 |   1501.71 |       3648.74 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3648.74 |       1560.68 |    41.78 |   1518.90 |       2129.84 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        2129.84 |       2154.23 |    24.39 |   2129.84 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void RemovePayoffWhenSchedulingAnExtraPayment()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                                | 2015-03-24 | 2015-03-24  | pmt001    | Payoff    | Scheduled |        4576.85 |       4592.57 |    15.72 |   4576.85 |          0.00 | {PaymentMethod}";

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 6068.05, Start = Date(2015, 2, 15) },
                new InterestAccrualPeriod { Principal = 5967.04, Start = Date(2015, 3, 2) },
            })
            .Returns(68.98);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 3, 2),
                PrincipalBalance = 5967.04,
                OpeningInterest = 68.98
            })
            .Returns(@"
                    AnniversaryDate | DueDate    | OpeningBalance | Amount  | Interest | Principal | EndingBalance
                    2015-03-15      | 2015-03-17 |        5967.04 | 1560.68 |    68.98 |   1491.70 |       4475.34
                    2015-04-15      | 2015-04-17 |        4475.34 | 1560.68 |    51.24 |   1509.44 |       2965.90
                    2015-05-15      | 2015-05-17 |        2965.90 | 1560.68 |    33.96 |   1526.72 |       1439.18
                    2015-06-15      | 2015-06-17 |        1439.18 | 1455.66 |    16.48 |   1439.18 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                DueDate = Date(2015, 3, 2),
                Amount = 101.01,
                PaymentId = "pmt002",
                Type = InstallmentType.Additional,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod
            });

            AssertSchedule($@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                                | 2015-03-02 | 2015-03-02  | pmt002    | Additional | Scheduled |        6068.05 |        101.01 |     0.00 |    101.01 |       5967.04 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled |        5967.04 |       1560.68 |    68.98 |   1491.70 |       4475.34 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled |        4475.34 |       1560.68 |    51.24 |   1509.44 |       2965.90 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled |        2965.90 |       1560.68 |    33.96 |   1526.72 |       1439.18 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled |        1439.18 |       1455.66 |    16.48 |   1439.18 |          0.00 | {PaymentMethod}"
            );
        }

        [Fact]
        public void CannotSchedulePayoffWhenPayoffIsAlreadyScheduled()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                                | 2015-03-24 | 2015-03-24  | pmt001    | Payoff    | Scheduled |        4576.85 |       4592.57 |    15.72 |   4576.85 |          0.00 | {PaymentMethod}";

            AssertException.Throws<ArgumentException>("Payoff cannot be scheduled because a payoff is already scheduled", () =>
            {
                Service.AddPayment(new PaymentInfo
                {
                    DueDate = Date(2015, 2, 1),
                    Amount = 100.0,
                    PaymentId = "pmt002",
                    Type = InstallmentType.Payoff,
                    LoanReferenceNumber = ReferenceNumber,
                    PaymentMethod = PaymentMethod
                });
            });
        }

        [Fact]
        public void CannotScheduleExtraPaymentAfterLastInstallment()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            AssertException.Throws<ArgumentException>("Payment cannot be scheduled after or on same date as a the last installment", () =>
            {
                Service.AddPayment(new PaymentInfo
                {
                    DueDate = Date(2015, 6, 17),
                    Amount = 100.0,
                    PaymentId = "pmt003",
                    Type = InstallmentType.Additional,
                    LoanReferenceNumber = ReferenceNumber,
                    PaymentMethod = PaymentMethod
                });
            });

            AssertException.Throws<ArgumentException>("Payment cannot be scheduled after or on same date as a the last installment", () =>
            {
                Service.AddPayment(new PaymentInfo
                {
                    DueDate = Date(2015, 6, 18),
                    Amount = 100.0,
                    PaymentId = "pmt003",
                    Type = InstallmentType.Additional,
                    LoanReferenceNumber = ReferenceNumber,
                    PaymentMethod = PaymentMethod
                });
            });
        }

        [Fact]
        public void CannotScheduleExtraPaymentBeforeOriginationDate()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            AssertException.Throws<ArgumentException>("Payment cannot be scheduled before or on origination date", () =>
            {
                Service.AddPayment(new PaymentInfo
                {
                    DueDate = Loan.Terms.OriginationDate,
                    Amount = 100.0,
                    PaymentId = "pmt003",
                    Type = InstallmentType.Additional,
                    LoanReferenceNumber = ReferenceNumber,
                    PaymentMethod = PaymentMethod
                });
            });

            AssertException.Throws<ArgumentException>("Payment cannot be scheduled before or on origination date", () =>
            {
                Service.AddPayment(new PaymentInfo
                {
                    DueDate = Loan.Terms.OriginationDate.AddDays(-1),
                    Amount = 100.0,
                    PaymentId = "pmt003",
                    Type = InstallmentType.Additional,
                    LoanReferenceNumber = ReferenceNumber,
                    PaymentMethod = PaymentMethod
                });
            });
        }

        [Fact]
        public void CannotSchedulePayoffBeforeCompletedInstallment()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            AssertException.Throws<ArgumentException>("Payment cannot be scheduled before or on same date as a completed installment", () =>
            {
                Service.AddPayment(new PaymentInfo
                {
                    DueDate = Date(2015, 2, 16),
                    Amount = 100.0,
                    PaymentId = "pmt003",
                    Type = InstallmentType.Payoff,
                    LoanReferenceNumber = ReferenceNumber,
                    PaymentMethod = PaymentMethod
                });
            });

            AssertException.Throws<ArgumentException>("Payment cannot be scheduled before or on same date as a completed installment", () =>
            {
                Service.AddPayment(new PaymentInfo
                {
                    DueDate = Date(2015, 2, 17),
                    Amount = 100.0,
                    PaymentId = "pmt003",
                    Type = InstallmentType.Payoff,
                    LoanReferenceNumber = ReferenceNumber,
                    PaymentMethod = PaymentMethod
                });
            });
        }

        [Fact]
        public void CannotSchedulePayoffAfterLastInstallment()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            AssertException.Throws<ArgumentException>("Payment cannot be scheduled after or on same date as a the last installment", () =>
            {
                Service.AddPayment(new PaymentInfo
                {
                    DueDate = Date(2015, 6, 17),
                    Amount = 100.0,
                    PaymentId = "pmt003",
                    Type = InstallmentType.Payoff,
                    LoanReferenceNumber = ReferenceNumber,
                    PaymentMethod = PaymentMethod
                });
            });

            AssertException.Throws<ArgumentException>("Payment cannot be scheduled after or on same date as a the last installment", () =>
            {
                Service.AddPayment(new PaymentInfo
                {
                    DueDate = Date(2015, 6, 18),
                    Amount = 100.0,
                    PaymentId = "pmt003",
                    Type = InstallmentType.Payoff,
                    LoanReferenceNumber = ReferenceNumber,
                    PaymentMethod = PaymentMethod
                });
            });
        }

        [Fact]
        public void CannotSchedulePayoffBeforeOriginationDate()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | {PaymentMethod}
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | {PaymentMethod}
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | {PaymentMethod}
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | {PaymentMethod}
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | {PaymentMethod}
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | {PaymentMethod}";

            AssertException.Throws<ArgumentException>("Payment cannot be scheduled before or on origination date", () =>
            {
                Service.AddPayment(new PaymentInfo
                {
                    DueDate = Loan.Terms.OriginationDate,
                    Amount = 100.0,
                    PaymentId = "pmt003",
                    Type = InstallmentType.Payoff,
                    LoanReferenceNumber = ReferenceNumber,
                    PaymentMethod = PaymentMethod
                });
            });

            AssertException.Throws<ArgumentException>("Payment cannot be scheduled before or on origination date", () =>
            {
                Service.AddPayment(new PaymentInfo
                {
                    DueDate = Loan.Terms.OriginationDate.AddDays(-1),
                    Amount = 100.0,
                    PaymentId = "pmt003",
                    Type = InstallmentType.Payoff,
                    LoanReferenceNumber = ReferenceNumber,
                    PaymentMethod = PaymentMethod
                });
            });
        }

        [Fact(Skip = "TODO")]
        public void CannotAddExtraPaymentWithAmountGreaterThanThePrincipalBalanceOfTheInstallmentPrecedingIt()
        {
            Schedule = $@"
                DueDate    | AnniversaryDate | PaymentDate | PaymentId  | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Status    | Type      | PaymentMethod  
                2015-01-15 | 2015-01-14      |             |            |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | Scheduled | Scheduled | {PaymentMethod} 
                2015-02-15 | 2015-02-14      |             |            |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | Scheduled | Scheduled | {PaymentMethod} 
                2015-03-15 | 2015-03-14      |             |            |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | Scheduled | Scheduled | {PaymentMethod} 
                2015-04-15 | 2015-04-14      |             |            |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | Scheduled | Scheduled | {PaymentMethod}
                2015-05-15 | 2015-05-14      |             |            |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | Scheduled | Scheduled | {PaymentMethod}
                2015-06-15 | 2015-06-14      |             |            |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | Scheduled | Scheduled | {PaymentMethod}";

            AssertException.Throws<InvalidArgumentException>("Extra payment amount must be less than the ending balance of the preceding installment", () =>
            {
                Service.AddPayment(new PaymentInfo
                {
                    // Day before 1st installment
                    DueDate = Date(2015, 1, 14),
                    Amount = Loan.Terms.LoanAmount,
                    PaymentId = "PMT001",
                    Type = InstallmentType.Additional,
                    LoanReferenceNumber = ReferenceNumber,
                    PaymentMethod = PaymentMethod
                });
            });

            AssertException.Throws<InvalidArgumentException>("Extra payment amount must be less than the ending balance of the preceding installment", () =>
            {
                Service.AddPayment(new PaymentInfo
                {
                    // Same day as 1st installment
                    DueDate = Date(2015, 1, 15),
                    Amount = 7542.37,
                    PaymentId = "PMT001",
                    Type = InstallmentType.Additional,
                    LoanReferenceNumber = ReferenceNumber,
                    PaymentMethod = PaymentMethod
                });
            });

            AssertException.Throws<InvalidArgumentException>("Extra payment amount must be less than the ending balance of the preceding installment", () =>
            {
                Service.AddPayment(new PaymentInfo
                {
                    // Day after 1st installment
                    DueDate = Date(2015, 1, 16),
                    Amount = 7542.37,
                    PaymentId = "PMT001",
                    Type = InstallmentType.Additional,
                    LoanReferenceNumber = ReferenceNumber,
                    PaymentMethod = PaymentMethod
                });
            });

            AssertException.Throws<InvalidArgumentException>("Extra payment amount must be less than the ending balance of the preceding installment", () =>
            {
                Service.AddPayment(new PaymentInfo
                {
                    // Day after 2nd installment
                    DueDate = Date(2015, 2, 16),
                    Amount = 6068.05,
                    PaymentId = "PMT001",
                    Type = InstallmentType.Additional,
                    LoanReferenceNumber = ReferenceNumber,
                    PaymentMethod = PaymentMethod
                });
            });

            Schedule = $@"
                DueDate    | AnniversaryDate | PaymentDate | PaymentId | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Status    | Type       | PaymentMethod  
                2015-01-15 | 2015-01-14      | 2015-01-15  | PMT001    |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | Completed | Scheduled  | {PaymentMethod}
                2015-02-15 | 2015-02-14      | 2015-02-15  | PMT002    |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | Completed | Scheduled  | {PaymentMethod}
                2015-03-01 |                 | 2015-03-03  | PMT003    |        6068.05 |       1000.00 |    32.42 |    967.58 |       5100.47 | Scheduled | Additional | {PaymentMethod}
                2015-03-15 | 2015-03-14      |             |           |        5100.47 |       1560.68 |    27.25 |   1533.43 |       3567.04 | Scheduled | Scheduled  | {PaymentMethod}
                2015-04-15 | 2015-04-14      |             |           |        3567.04 |       1560.68 |    40.84 |   1519.84 |       2047.20 | Scheduled | Scheduled  | {PaymentMethod}
                2015-05-15 | 2015-05-14      |             |           |        2047.20 |       1560.68 |    23.44 |   1537.24 |        509.96 | Scheduled | Scheduled  | {PaymentMethod}
                2015-06-15 | 2015-06-14      |             |           |         509.96 |        515.80 |     5.84 |    509.96 |          0.00 | Scheduled | Scheduled  | {PaymentMethod}";

            AssertException.Throws<InvalidArgumentException>("Extra payment amount must be less than the ending balance of the preceding installment", () =>
            {
                Service.AddPayment(new PaymentInfo
                {
                    // Day after 2nd payment. It should consider the
                    // payment date instead of the due date when the former
                    // is not null
                    DueDate = Date(2015, 3, 2),
                    Amount = 6068.05,
                    PaymentId = "PMT004",
                    Type = InstallmentType.Additional,
                    LoanReferenceNumber = ReferenceNumber,
                    PaymentMethod = PaymentMethod
                });
            });

            AssertException.Throws<InvalidArgumentException>("Extra payment amount must be less than the ending balance of the preceding installment", () =>
            {
                Service.AddPayment(new PaymentInfo
                {
                    // Day after 3rd payment
                    DueDate = Date(2015, 3, 4),
                    Amount = 5100.47,
                    PaymentId = "PMT004",
                    Type = InstallmentType.Additional,
                    LoanReferenceNumber = ReferenceNumber,
                    PaymentMethod = PaymentMethod
                });
            });
        }
    }

    public class AddAchPaymentTest : AddPaymentTest
    {
        protected override PaymentMethod PaymentMethod => PaymentMethod.ACH;

        [Fact]
        public void ScheduledPaymentBeforeAnniversaryDateIsNotAllowed()
        {
            Schedule = $@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentMethod  
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37 | ACH
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05 | ACH
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85 | ACH
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57 | ACH
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03 | ACH
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00 | ACH";

            GracePeriod.Duration = 10;

            AssertException.Throws<ArgumentException>("Installment not found due on 2015-03-14", () =>
            {
                Service.AddPayment(new PaymentInfo
                {
                    DueDate = Date(2015, 3, 14),
                    Amount = 1560.68,
                    PaymentId = "pmt003",
                    Type = InstallmentType.Scheduled,
                    LoanReferenceNumber = ReferenceNumber,
                    PaymentMethod = PaymentMethod.ACH
                });
            });
        }
    }

    public class AddCheckPaymentTest : AddPaymentTest
    {
        protected override PaymentMethod PaymentMethod => PaymentMethod.Check;

        public AddCheckPaymentTest()
        {
            Loan.PaymentMethod = PaymentMethod.Check;
        }

        [Fact]
        public void Early1stPayment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled | Check         |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled | Check         |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled | Check         |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled | Check         |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00";

            GracePeriod.Duration = 10;

            /* Early payment's interest */
            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 9000.00, Start = Loan.Terms.OriginationDate, End = Date(2015, 1, 9) }
            })
            .Returns(82.44);

            /* Next installment's interest */
            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 7521.76, Start = Date(2015, 1,  9) }, // From the early payment date
                new InterestAccrualPeriod { Principal = 7521.76, Start = Date(2015, 1, 15) }  // to the early payment anniversary date, then onwards to the next anniversary date
            })
            .Returns(103.35);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 1, 17),
                PrincipalBalance = 7521.76,
                OpeningInterest = 103.35
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance | Amount  | Interest | Principal | EndingBalance
                2015-02-15      | 2015-02-17 |        7521.76 | 1560.68 |   103.35 |   1457.33 |       6064.43
                2015-03-15      | 2015-03-17 |        6064.43 | 1560.68 |    69.44 |   1491.24 |       4573.19
                2015-04-15      | 2015-04-17 |        4573.19 | 1560.68 |    52.36 |   1508.32 |       3064.87
                2015-05-15      | 2015-05-17 |        3064.87 | 1560.68 |    35.09 |   1525.59 |       1539.28
                2015-06-15      | 2015-06-17 |        1539.28 | 1556.90 |    17.62 |   1539.28 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 1560.68,
                DueDate = Date(2015, 1, 9),
                PaymentId = "pmt001",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod.Check
            });

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-09  | pmt001    | Scheduled | Scheduled | Check         |        9000.00 |       1560.68 |    82.44 |   1478.24 |       7521.76
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled | Check         |        7521.76 |       1560.68 |   103.35 |   1457.33 |       6064.43
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled | Check         |        6064.43 |       1560.68 |    69.44 |   1491.24 |       4573.19
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled | Check         |        4573.19 |       1560.68 |    52.36 |   1508.32 |       3064.87
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled | Check         |        3064.87 |       1560.68 |    35.09 |   1525.59 |       1539.28
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled | Check         |        1539.28 |       1556.90 |    17.62 |   1539.28 |          0.00"
            );
        }

        [Fact]
        public void Early3rdPayment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled | Check         |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled | Check         |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled | Check         |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled | Check         |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00";

            GracePeriod.Duration = 10;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 6068.05, Start = Date(2015, 2, 15), End = Date(2015, 3, 10) }
            })
            .Returns(57.90);

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 4565.27, Start = Date(2015, 3, 10) },
                new InterestAccrualPeriod { Principal = 4565.27, Start = Date(2015, 3, 15) }
            })
            .Returns(60.98);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 3, 17),
                PrincipalBalance = 4565.27,
                OpeningInterest = 60.98
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance | Amount  | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        4565.27 | 1560.68 |    60.98 |   1499.70 |       3065.57
                2015-05-15      | 2015-05-17 |        3065.57 | 1560.68 |    35.10 |   1525.58 |       1539.99
                2015-06-15      | 2015-06-17 |        1539.99 | 1557.62 |    17.63 |   1539.99 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 1560.68,
                DueDate = Date(2015, 3, 10),
                PaymentId = "pmt003",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod.Check
            });

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-10  | pmt003    | Scheduled | Scheduled | Check         |        6068.05 |       1560.68 |    57.90 |   1502.78 |       4565.27
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled | Check         |        4565.27 |       1560.68 |    60.98 |   1499.70 |       3065.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled | Check         |        3065.57 |       1560.68 |    35.10 |   1525.58 |       1539.99
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled | Check         |        1539.99 |       1557.62 |    17.63 |   1539.99 |          0.00"
            );
        }

        [Fact]
        public void EarlyLastPayment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-15  | pmt003    | Scheduled | Completed | Check         |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 | 2015-04-15  | pmt004    | Scheduled | Completed | Check         |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 | 2015-05-15  | pmt005    | Scheduled | Completed | Check         |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled | Check         |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00";

            GracePeriod.Duration = 10;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 1543.03, Start = Date(2015, 5, 15), End = Date(2015, 6, 11) }
            })
            .Returns(15.31);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 6, 17),
                PrincipalBalance = 0.0
            })
            .Returns("");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 1558.34,
                DueDate = Date(2015, 6, 11),
                PaymentId = "pmt006",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod.Check
            });

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-15  | pmt003    | Scheduled | Completed | Check         |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 | 2015-04-15  | pmt004    | Scheduled | Completed | Check         |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 | 2015-05-15  | pmt005    | Scheduled | Completed | Check         |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 | 2015-06-11  | pmt006    | Scheduled | Scheduled | Check         |        1543.03 |       1558.34 |    15.31 |   1543.03 |          0.00"
            );
        }

        [Fact]
        public void Early1stPaymentAddedAfterAnExtraPayment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                                | 2014-12-25 | 2014-12-25  | pmt001    | Additional | Completed | Check         |        9000.00 |        101.01 |     0.00 |    101.01 |       8898.99
                2015-01-15      | 2015-01-17 |             |           | Scheduled  | Scheduled | Check         |        8898.99 |       1560.68 |   102.28 |   1458.40 |       7440.59
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled | Check         |        7440.59 |       1560.68 |    85.19 |   1475.49 |       5965.10
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled | Check         |        5965.10 |       1560.68 |    68.30 |   1492.38 |       4472.72
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled | Check         |        4472.72 |       1560.68 |    51.21 |   1509.47 |       2963.25
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled | Check         |        2963.25 |       1560.68 |    33.93 |   1526.75 |       1436.50
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled | Check         |        1436.50 |       1452.95 |    16.45 |   1436.50 |          0.00";

            GracePeriod.Duration = 10;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 9000.00, Start = Loan.Terms.OriginationDate },
                new InterestAccrualPeriod { Principal = 8898.99, Start = Date(2014, 12, 25), End = Date(2015, 1, 6) }
            })
            .Returns(71.71); //early payment's interest

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 7410.02, Start = Date(2015, 1, 6) },
                new InterestAccrualPeriod { Principal = 7410.02, Start = Date(2015, 1, 15) }
            })
            .Returns(110.30); //next installment's interest

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 1, 17),
                PrincipalBalance = 7410.02,
                OpeningInterest = 110.30
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-02-15      | 2015-02-17 |        7410.02 | 1560.68 |   110.30 |   1450.38 |       5959.64
                2015-03-15      | 2015-03-17 |        5959.64 | 1560.68 |    68.24 |   1492.44 |       4467.20
                2015-04-15      | 2015-04-17 |        4467.20 | 1560.68 |    51.15 |   1509.53 |       2957.67
                2015-05-15      | 2015-05-17 |        2957.67 | 1560.68 |    33.87 |   1526.81 |       1430.86
                2015-06-15      | 2015-06-17 |        1430.86 | 1447.24 |    16.38 |   1430.86 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 1560.68,
                DueDate = Date(2015, 1, 6),
                PaymentId = "pmt002",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod.Check
            });

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                                | 2014-12-25 | 2014-12-25  | pmt001    | Additional | Completed | Check         |        9000.00 |        101.01 |     0.00 |    101.01 |       8898.99
                2015-01-15      | 2015-01-17 | 2015-01-06  | pmt002    | Scheduled  | Scheduled | Check         |        8898.99 |       1560.68 |    71.71 |   1488.97 |       7410.02
                2015-02-15      | 2015-02-17 |             |           | Scheduled  | Scheduled | Check         |        7410.02 |       1560.68 |   110.30 |   1450.38 |       5959.64
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled | Check         |        5959.64 |       1560.68 |    68.24 |   1492.44 |       4467.20
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled | Check         |        4467.20 |       1560.68 |    51.15 |   1509.53 |       2957.67
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled | Check         |        2957.67 |       1560.68 |    33.87 |   1526.81 |       1430.86
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled | Check         |        1430.86 |       1447.24 |    16.38 |   1430.86 |          0.00"
            );
        }

        [Fact]
        public void Early3rdPaymentAddedAfterExtraPayment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled  | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled  | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                                | 2015-03-01 | 2015-03-01  | pmt003    | Additional | Completed | Check         |        6068.05 |        303.03 |     0.00 |    303.03 |       5765.02
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled | Check         |        5765.02 |       1560.68 |    67.86 |   1492.82 |       4272.20
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled | Check         |        4272.20 |       1560.68 |    48.92 |   1511.76 |       2760.44
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled | Check         |        2760.44 |       1560.68 |    31.61 |   1529.07 |       1231.37
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled | Check         |        1231.37 |       1245.47 |    14.10 |   1231.37 |          0.00";

            GracePeriod.Duration = 10;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 6068.05, Start = Date(2015, 2, 15) },
                new InterestAccrualPeriod { Principal = 5765.02, Start = Date(2015, 3, 01), End = Date(2015, 3, 12) }
            })
            .Returns(61.26); //early payment interest

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 4265.60, Start = Date(2015, 3, 12) },
                new InterestAccrualPeriod { Principal = 4265.60, Start = Date(2015, 3, 15) }
            })
            .Returns(53.73); //next installment's interest

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 3, 17),
                PrincipalBalance = 4265.60,
                OpeningInterest = 53.73
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance | Amount  | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        4265.60 | 1560.68 |    53.73 |   1506.95 |       2758.65
                2015-05-15      | 2015-05-17 |        2758.65 | 1560.68 |    31.59 |   1529.09 |       1229.56
                2015-06-15      | 2015-06-17 |        1229.56 | 1243.64 |    14.08 |   1229.56 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 1560.68,
                DueDate = Date(2015, 3, 12),
                PaymentId = "pmt004",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod.Check
            });

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled  | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled  | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                                | 2015-03-01 | 2015-03-01  | pmt003    | Additional | Completed | Check         |        6068.05 |        303.03 |     0.00 |    303.03 |       5765.02
                2015-03-15      | 2015-03-17 | 2015-03-12  | pmt004    | Scheduled  | Scheduled | Check         |        5765.02 |       1560.68 |    61.26 |   1499.42 |       4265.60
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled | Check         |        4265.60 |       1560.68 |    53.73 |   1506.95 |       2758.65
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled | Check         |        2758.65 |       1560.68 |    31.59 |   1529.09 |       1229.56
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled | Check         |        1229.56 |       1243.64 |    14.08 |   1229.56 |          0.00"
            );
        }

        [Fact]
        public void Early3rdPaymentAddedAfterTwoExtraPayments()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled  | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled  | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                                | 2015-02-23 | 2015-02-23  | pmt003    | Additional | Completed | Check         |        6068.05 |        303.03 |     0.00 |    303.03 |       5765.02
                                | 2015-03-04 | 2015-03-04  | pmt004    | Additional | Completed | Check         |        5765.02 |        404.04 |     0.00 |    404.04 |       5360.98
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled | Check         |        5360.98 |       1560.68 |    65.24 |   1495.44 |       3865.54
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled | Check         |        3865.54 |       1560.68 |    44.26 |   1516.42 |       2349.12
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled | Check         |        2349.12 |       1560.68 |    26.90 |   1533.78 |        815.34
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled | Check         |         815.34 |        824.68 |     9.34 |    815.34 |          0.00";

            GracePeriod.Duration = 10;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 6068.05, Start = Date(2015, 2, 15) },
                new InterestAccrualPeriod { Principal = 5765.02, Start = Date(2015, 2, 23) },
                new InterestAccrualPeriod { Principal = 5360.98, Start = Date(2015, 3, 04), End = Date(2015, 3, 11) }
            })
            .Returns(57.05); //early payment's interest

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 3857.35, Start = Date(2015, 3, 11) },
                new InterestAccrualPeriod { Principal = 3857.35, Start = Date(2015, 3, 15) }
            })
            .Returns(50.06); //next interest's interest

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 3, 17),
                PrincipalBalance = 3857.35,
                OpeningInterest = 50.06
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance | Amount  | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        3857.35 | 1560.68 |    50.06 |   1510.62 |       2346.73
                2015-05-15      | 2015-05-17 |        2346.73 | 1560.68 |    26.87 |   1533.81 |        812.92
                2015-06-15      | 2015-06-17 |         812.92 |  822.23 |     9.31 |    812.92 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 1560.68,
                DueDate = Date(2015, 3, 11),
                PaymentId = "pmt005",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod.Check
            });

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled  | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled  | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                                | 2015-02-23 | 2015-02-23  | pmt003    | Additional | Completed | Check         |        6068.05 |        303.03 |     0.00 |    303.03 |       5765.02
                                | 2015-03-04 | 2015-03-04  | pmt004    | Additional | Completed | Check         |        5765.02 |        404.04 |     0.00 |    404.04 |       5360.98
                2015-03-15      | 2015-03-17 | 2015-03-11  | pmt005    | Scheduled  | Scheduled | Check         |        5360.98 |       1560.68 |    57.05 |   1503.63 |       3857.35
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled | Check         |        3857.35 |       1560.68 |    50.06 |   1510.62 |       2346.73
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled | Check         |        2346.73 |       1560.68 |    26.87 |   1533.81 |        812.92
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled | Check         |         812.92 |        822.23 |     9.31 |    812.92 |          0.00"
            );
        }

        [Fact]
        public void EarlyLastPaymentAddedAfterAnExtraPayment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled  | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled  | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-15  | pmt003    | Scheduled  | Completed | Check         |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 | 2015-04-15  | pmt004    | Scheduled  | Completed | Check         |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 | 2015-05-15  | pmt005    | Scheduled  | Completed | Check         |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                                | 2015-05-26 | 2015-05-26  | pmt006    | Additional | Completed | Check         |        1543.03 |        606.06 |     0.00 |    606.06 |        936.97
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled | Check         |         936.97 |        950.24 |    13.27 |    936.97 |          0.00";

            GracePeriod.Duration = 10;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 1543.03, Start = Date(2015, 5, 15) },
                new InterestAccrualPeriod { Principal =  936.97, Start = Date(2015, 5, 26), End = Date(2015, 6, 11) }
            })
            .Returns(11.84); //early payment's interest

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 6, 17),
                PrincipalBalance = 0.0
            })
            .Returns("");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 948.81,
                DueDate = Date(2015, 6, 11),
                PaymentId = "pmt007",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod.Check
            });

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled  | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled  | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-15  | pmt003    | Scheduled  | Completed | Check         |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 | 2015-04-15  | pmt004    | Scheduled  | Completed | Check         |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 | 2015-05-15  | pmt005    | Scheduled  | Completed | Check         |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                                | 2015-05-26 | 2015-05-26  | pmt006    | Additional | Completed | Check         |        1543.03 |        606.06 |     0.00 |    606.06 |        936.97
                2015-06-15      | 2015-06-17 | 2015-06-11  | pmt007    | Scheduled  | Scheduled | Check         |         936.97 |        948.81 |    11.84 |    936.97 |          0.00"
            );
        }

        [Fact]
        public void EarlyPaymentAfterEarlyPayment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-08  | pmt002    | Scheduled | Completed | Check         |        7542.37 |       1560.68 |    66.21 |   1494.47 |       6047.90
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled | Check         |        6047.90 |       1560.68 |    85.41 |   1475.27 |       4572.63
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled | Check         |        4572.63 |       1560.68 |    52.36 |   1508.32 |       3064.31
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled | Check         |        3064.31 |       1560.68 |    35.09 |   1525.59 |       1538.72
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled | Check         |        1538.72 |       1556.34 |    17.62 |   1538.72 |          0.00";

            GracePeriod.Duration = 10;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod
                {
                    Principal = 6047.90,
                    Start = Date(2015, 2,  8), // the previous early payment date
                    End   = Date(2015, 3, 10)  // the actual early payment date
                }
            })
            .Returns(73.87); //early payment's interest

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 4561.09, Start = Date(2015, 3, 10) },
                new InterestAccrualPeriod { Principal = 4561.09, Start = Date(2015, 3, 15) }
            })
            .Returns(60.93); //next installment's interest

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 3, 17),
                PrincipalBalance = 4561.09,
                OpeningInterest = 60.93
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        4561.09 | 1560.68 |    60.93 |   1499.75 |       3061.34
                2015-05-15      | 2015-05-17 |        3061.34 | 1560.68 |    35.05 |   1525.63 |       1535.71
                2015-06-15      | 2015-06-17 |        1535.71 | 1553.29 |    17.58 |   1535.71 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 1560.68,
                DueDate = Date(2015, 3, 10),
                PaymentId = "pmt003",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod.Check
            });

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-08  | pmt002    | Scheduled | Completed | Check         |        7542.37 |       1560.68 |    66.21 |   1494.47 |       6047.90
                2015-03-15      | 2015-03-17 | 2015-03-10  | pmt003    | Scheduled | Scheduled | Check         |        6047.90 |       1560.68 |    73.87 |   1486.81 |       4561.09
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled | Check         |        4561.09 |       1560.68 |    60.93 |   1499.75 |       3061.34
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled | Check         |        3061.34 |       1560.68 |    35.05 |   1525.63 |       1535.71
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled | Check         |        1535.71 |       1553.29 |    17.58 |   1535.71 |          0.00"
            );
        }

        [Fact]
        public void EarlyPaymentAfterEarlyAndExtraPayments()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-10  | pmt002    | Scheduled  | Completed | Check         |        7542.37 |       1560.68 |    71.97 |   1488.71 |       6053.66
                                | 2015-02-27 | 2015-02-27  | pmt003    | Additional | Completed | Check         |        6053.66 |        303.03 |     0.00 |    303.03 |       5750.63
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled | Check         |        5750.63 |       1560.68 |    78.78 |   1481.90 |       4268.73
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled | Check         |        4268.73 |       1560.68 |    48.88 |   1511.80 |       2756.93
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled | Check         |        2756.93 |       1560.68 |    31.57 |   1529.11 |       1227.82
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled | Check         |        1227.82 |       1241.88 |    14.06 |   1227.82 |          0.00";

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 6053.66, Start = Date(2015, 2, 10) },                         //1st early payment
                new InterestAccrualPeriod { Principal = 5750.63, Start = Date(2015, 2, 27), End = Date(2015, 3, 9) }  //extra payment
            })
            .Returns(65.62); //new early payment's interest

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 4255.57, Start = Date(2015, 3,  9) }, //new early payment
                new InterestAccrualPeriod { Principal = 4255.57, Start = Date(2015, 3, 15) }  //anniversary date
            })
            .Returns(58.47); //next installment's interest

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 3, 17),
                PrincipalBalance = 4255.57,
                OpeningInterest = 58.47
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        4255.57 | 1560.68 |    58.47 |   1502.21 |       2753.36
                2015-05-15      | 2015-05-17 |        2753.36 | 1560.68 |    31.53 |   1529.15 |       1224.21
                2015-06-15      | 2015-06-17 |        1224.21 | 1238.23 |    14.02 |   1224.21 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 1560.68,
                DueDate = Date(2015, 3, 9),
                PaymentId = "pmt004",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod.Check
            });

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-10  | pmt002    | Scheduled  | Completed | Check         |        7542.37 |       1560.68 |    71.97 |   1488.71 |       6053.66
                                | 2015-02-27 | 2015-02-27  | pmt003    | Additional | Completed | Check         |        6053.66 |        303.03 |     0.00 |    303.03 |       5750.63
                2015-03-15      | 2015-03-17 | 2015-03-09  | pmt004    | Scheduled  | Scheduled | Check         |        5750.63 |       1560.68 |    65.62 |   1495.06 |       4255.57
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled | Check         |        4255.57 |       1560.68 |    58.47 |   1502.21 |       2753.36
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled | Check         |        2753.36 |       1560.68 |    31.53 |   1529.15 |       1224.21
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled | Check         |        1224.21 |       1238.23 |    14.02 |   1224.21 |          0.00"
            );
        }

        [Fact]
        public void ExtraPaymentAfterEarlyPayment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-08  | pmt002    | Scheduled | Completed | Check         |        7542.37 |       1560.68 |    66.21 |   1494.47 |       6047.90
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled | Check         |        6047.90 |       1560.68 |    85.41 |   1475.27 |       4572.63
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled | Check         |        4572.63 |       1560.68 |    52.36 |   1508.32 |       3064.31
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled | Check         |        3064.31 |       1560.68 |    35.09 |   1525.59 |       1538.72
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled | Check         |        1538.72 |       1556.34 |    17.62 |   1538.72 |          0.00";

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 6047.90, Start = Date(2015, 2,  8) },
                new InterestAccrualPeriod { Principal = 6047.90, Start = Date(2015, 2, 15) },
                new InterestAccrualPeriod { Principal = 5744.87, Start = Date(2015, 3,  4) }
            })
            .Returns(84.13);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 3, 4),
                PrincipalBalance = 5744.87,
                OpeningInterest = 84.13
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                2015-03-15      | 2015-03-17 |        5744.87 | 1560.68 |    84.13 |   1476.55 |       4268.32
                2015-04-15      | 2015-04-17 |        4268.32 | 1560.68 |    48.87 |   1511.81 |       2756.51
                2015-05-15      | 2015-05-17 |        2756.51 | 1560.68 |    31.56 |   1529.12 |       1227.39
                2015-06-15      | 2015-06-17 |        1227.39 | 1241.44 |    14.05 |   1227.39 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 303.03,
                DueDate = Date(2015, 3, 4),
                PaymentId = "pmt003",
                Type = InstallmentType.Additional,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod.Check
            });

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-08  | pmt002    | Scheduled  | Completed | Check         |        7542.37 |       1560.68 |    66.21 |   1494.47 |       6047.90
                                | 2015-03-04 | 2015-03-04  | pmt003    | Additional | Scheduled | Check         |        6047.90 |        303.03 |     0.00 |    303.03 |       5744.87
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled | Check         |        5744.87 |       1560.68 |    84.13 |   1476.55 |       4268.32
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled | Check         |        4268.32 |       1560.68 |    48.87 |   1511.81 |       2756.51
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled | Check         |        2756.51 |       1560.68 |    31.56 |   1529.12 |       1227.39
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled | Check         |        1227.39 |       1241.44 |    14.05 |   1227.39 |          0.00");
        }

        [Fact]
        public void ExtraPaymentBetweenEarlyPaymentDateAndEarlyPaymentAnniversaryDate()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-07  | pmt003    | Scheduled | Completed | Check         |        6068.05 |       1560.68 |    50.95 |   1509.73 |       4558.32
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled | Check         |        4558.32 |       1560.68 |    66.11 |   1494.57 |       3063.75
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled | Check         |        3063.75 |       1560.68 |    35.08 |   1525.60 |       1538.15
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled | Check         |        1538.15 |       1555.76 |    17.61 |   1538.15 |          0.00";

            GracePeriod.Duration = 10;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 4558.32, Start = Date(2015, 3,  7) },
                new InterestAccrualPeriod { Principal = 4154.28, Start = Date(2015, 3, 11) },
                new InterestAccrualPeriod { Principal = 4154.28, Start = Date(2015, 3, 15) }
            })
            .Returns(60.87);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                // In this case the reference date should be the early payment's anniversary date,
                // otherwise the reamortization would start on the early payment date and we'd end up
                // having a duplicate scheduled installment due in March 15th.
                ReferenceDate = Date(2015, 3, 15),
                PrincipalBalance = 4154.28,
                OpeningInterest = 60.87
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance | Amount  | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        4154.28 | 1560.68 |    60.87 |   1499.81 |       2654.47
                2015-05-15      | 2015-05-17 |        2654.47 | 1560.68 |    30.39 |   1530.29 |       1124.18
                2015-06-15      | 2015-06-17 |        1124.18 | 1137.05 |    12.87 |   1124.18 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 404.04,
                DueDate = Date(2015, 3, 11),
                PaymentId = "pmt004",
                Type = InstallmentType.Additional,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod.Check
            });

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled  | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled  | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-07  | pmt003    | Scheduled  | Completed | Check         |        6068.05 |       1560.68 |    50.95 |   1509.73 |       4558.32
                                | 2015-03-11 | 2015-03-11  | pmt004    | Additional | Scheduled | Check         |        4558.32 |        404.04 |     0.00 |    404.04 |       4154.28
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled | Check         |        4154.28 |       1560.68 |    60.87 |   1499.81 |       2654.47
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled | Check         |        2654.47 |       1560.68 |    30.39 |   1530.29 |       1124.18
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled | Check         |        1124.18 |       1137.05 |    12.87 |   1124.18 |          0.00"
            );
        }

        [Fact]
        public void TwoExtraPaymentsBetweenEarlyPaymentDateAndEarlyPaymentAnniversaryDate()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled  | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled  | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-07  | pmt003    | Scheduled  | Completed | Check         |        6068.05 |       1560.68 |    50.95 |   1509.73 |       4558.32
                                | 2015-03-11 | 2015-03-11  | pmt004    | Additional | Completed | Check         |        4558.32 |        404.04 |     0.00 |    404.04 |       4154.28
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled | Check         |        4154.28 |       1560.68 |    60.87 |   1499.81 |       2654.47
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled | Check         |        2654.47 |       1560.68 |    30.39 |   1530.29 |       1124.18
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled | Check         |        1124.18 |       1137.05 |    12.87 |   1124.18 |          0.00";

            GracePeriod.Duration = 10;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 4558.32, Start = Date(2015, 3,  7) },
                new InterestAccrualPeriod { Principal = 4053.27, Start = Date(2015, 3,  9) },
                new InterestAccrualPeriod { Principal = 3649.23, Start = Date(2015, 3, 11) },
                new InterestAccrualPeriod { Principal = 3649.23, Start = Date(2015, 3, 15) }
            })
            .Returns(53.62);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                // In this case the reference date should be the early payment's anniversary date,
                // otherwise the reamortization would start on the early payment date and we'd end up
                // having a duplicate scheduled installment due in March 15th.
                ReferenceDate = Date(2015, 3, 15),
                PrincipalBalance = 3649.23,
                OpeningInterest = 53.62
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance | Amount  | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        3649.23 | 1560.68 |    53.62 |   1507.06 |       2142.17
                2015-05-15      | 2015-05-17 |        2142.17 | 1560.68 |    24.53 |   1536.15 |        606.02
                2015-06-15      | 2015-06-17 |         606.02 |  612.96 |     6.94 |    606.02 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 505.05,
                DueDate = Date(2015, 3, 9),
                PaymentId = "pmt005",
                Type = InstallmentType.Additional,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod.Check
            });

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled  | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled  | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-07  | pmt003    | Scheduled  | Completed | Check         |        6068.05 |       1560.68 |    50.95 |   1509.73 |       4558.32
                                | 2015-03-09 | 2015-03-09  | pmt005    | Additional | Scheduled | Check         |        4558.32 |        505.05 |     0.00 |    505.05 |       4053.27
                                | 2015-03-11 | 2015-03-11  | pmt004    | Additional | Completed | Check         |        4053.27 |        404.04 |     0.00 |    404.04 |       3649.23
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled | Check         |        3649.23 |       1560.68 |    53.62 |   1507.06 |       2142.17
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled | Check         |        2142.17 |       1560.68 |    24.53 |   1536.15 |        606.02
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled | Check         |         606.02 |        612.96 |     6.94 |    606.02 |          0.00"
            );
        }

        [Fact]
        public void EarlyPaymentBeforeExtraPayment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                                | 2015-03-12 | 2015-03-12  | pmt003    | Additional | Completed | Check         |        6068.05 |        303.03 |     0.00 |    303.03 |       5765.02
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled | Check         |        5765.02 |       1560.68 |    69.13 |   1491.55 |       4273.47
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled | Check         |        4273.47 |       1560.68 |    48.93 |   1511.75 |       2761.72
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled | Check         |        2761.72 |       1560.68 |    31.62 |   1529.06 |       1232.66
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled | Check         |        1232.66 |       1246.77 |    14.11 |   1232.66 |          0.00";

            GracePeriod.Duration = 10;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 6068.05, Start = Date(2015, 2, 15), End = Date(2015, 3, 10) }
            })
            .Returns(57.90);

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 4565.27, Start = Date(2015, 3, 10) },
                new InterestAccrualPeriod { Principal = 4262.24, Start = Date(2015, 3, 12) },
                new InterestAccrualPeriod { Principal = 4262.24, Start = Date(2015, 3, 15) }
            })
            .Returns(57.17);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                // In this case the reference date should be the early payment's anniversary date,
                // otherwise the reamortization would start on the early payment date and we'd end up
                // having a duplicate scheduled installment due in March 15th.
                ReferenceDate = Date(2015, 3, 15),
                PrincipalBalance = 4262.24,
                OpeningInterest = 57.17
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance | Amount  | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        4262.24 | 1560.68 |    57.17 |   1503.51 |       2758.73
                2015-05-15      | 2015-05-17 |        2758.73 | 1560.68 |    31.59 |   1529.09 |       1229.64
                2015-06-15      | 2015-06-17 |        1229.64 | 1243.72 |    14.08 |   1229.64 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 1560.68,
                DueDate = Date(2015, 3, 10),
                PaymentId = "pmt004",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod.Check
            });

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-10  | pmt004    | Scheduled  | Scheduled | Check         |        6068.05 |       1560.68 |    57.90 |   1502.78 |       4565.27
                                | 2015-03-12 | 2015-03-12  | pmt003    | Additional | Completed | Check         |        4565.27 |        303.03 |     0.00 |    303.03 |       4262.24
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled | Check         |        4262.24 |       1560.68 |    57.17 |   1503.51 |       2758.73
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled | Check         |        2758.73 |       1560.68 |    31.59 |   1529.09 |       1229.64
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled | Check         |        1229.64 |       1243.72 |    14.08 |   1229.64 |          0.00");
        }

        [Fact]
        public void ExtraPaymentOnSameDateAsEarlyPayment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-11  | pmt003    | Scheduled | Completed | Check         |        6068.05 |       1560.68 |    60.22 |   1500.46 |       4567.59
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled | Check         |        4567.59 |       1560.68 |    59.27 |   1501.41 |       3066.18
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled | Check         |        3066.18 |       1560.68 |    35.11 |   1525.57 |       1540.61
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled | Check         |        1540.61 |       1558.25 |    17.64 |   1540.61 |          0.00";

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 4567.59, Start = Date(2015, 3, 11) },
                new InterestAccrualPeriod { Principal = 4163.55, Start = Date(2015, 3, 11) },
                new InterestAccrualPeriod { Principal = 4163.55, Start = Date(2015, 3, 15) }
            })
            .Returns(54.03);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 3, 15),
                PrincipalBalance = 4163.55,
                OpeningInterest = 54.03
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance | Amount  | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        4163.55 | 1560.68 |    54.03 |   1506.65 |       2656.89
                2015-05-15      | 2015-05-17 |        2656.89 | 1560.68 |    30.42 |   1530.26 |       1126.63
                2015-06-15      | 2015-06-17 |        1126.63 | 1139.53 |    12.90 |   1126.63 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 404.04,
                DueDate = Date(2015, 3, 11),
                PaymentId = "pmt004",
                Type = InstallmentType.Additional,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod.Check
            });

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-11  | pmt003    | Scheduled  | Completed | Check         |        6068.05 |       1560.68 |    60.22 |   1500.46 |       4567.59
                                | 2015-03-11 | 2015-03-11  | pmt004    | Additional | Scheduled | Check         |        4567.59 |        404.04 |     0.00 |    404.04 |       4163.55
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled | Check         |        4163.55 |       1560.68 |    54.03 |   1506.65 |       2656.89
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled | Check         |        2656.89 |       1560.68 |    30.42 |   1530.26 |       1126.63
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled | Check         |        1126.63 |       1139.53 |    12.90 |   1126.63 |          0.00");
        }

        [Fact]
        public void EarlyPaymentOnSameDateAsExtraPayment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                                | 2015-03-11 | 2015-03-11  | pmt003    | Additional | Completed | Check         |        6068.05 |        404.04 |     0.00 |    404.04 |       5664.01
                2015-03-15      | 2015-03-17 |             |           | Scheduled  | Scheduled | Check         |        5664.01 |       1560.68 |    68.86 |   1491.82 |       4172.19
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled | Check         |        4172.19 |       1560.68 |    47.77 |   1512.91 |       2659.28
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled | Check         |        2659.28 |       1560.68 |    30.45 |   1530.23 |       1129.05
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled | Check         |        1129.05 |       1141.98 |    12.93 |   1129.05 |          0.00";

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 6068.05, Start = Date(2015, 2, 15), End = Date(2015, 3, 11) }
            })
            .Returns(60.22);

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 4567.59, Start = Date(2015, 3, 11) },
                new InterestAccrualPeriod { Principal = 4163.55, Start = Date(2015, 3, 11) },
                new InterestAccrualPeriod { Principal = 4163.55, Start = Date(2015, 3, 15) }
            })
            .Returns(54.03);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 3, 15),
                PrincipalBalance = 4163.55,
                OpeningInterest = 54.03
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance | Amount  | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        4163.55 | 1560.68 |    54.03 |   1506.65 |       2656.89
                2015-05-15      | 2015-05-17 |        2656.89 | 1560.68 |    30.42 |   1530.26 |       1126.63
                2015-06-15      | 2015-06-17 |        1126.63 | 1139.53 |    12.90 |   1126.63 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 1560.68,
                DueDate = Date(2015, 3, 11),
                PaymentId = "pmt004",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod.Check
            });

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type       | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | pmt001    | Scheduled  | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | pmt002    | Scheduled  | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-11  | pmt004    | Scheduled  | Scheduled | Check         |        6068.05 |       1560.68 |    60.22 |   1500.46 |       4567.59
                                | 2015-03-11 | 2015-03-11  | pmt003    | Additional | Completed | Check         |        4567.59 |        404.04 |     0.00 |    404.04 |       4163.55
                2015-04-15      | 2015-04-17 |             |           | Scheduled  | Scheduled | Check         |        4163.55 |       1560.68 |    54.03 |   1506.65 |       2656.89
                2015-05-15      | 2015-05-17 |             |           | Scheduled  | Scheduled | Check         |        2656.89 |       1560.68 |    30.42 |   1530.26 |       1126.63
                2015-06-15      | 2015-06-17 |             |           | Scheduled  | Scheduled | Check         |        1126.63 |       1139.53 |    12.90 |   1126.63 |          0.00");
        }

        [Fact]
        public void PayAchInstallmentEarlyWithCheck()
        {
            Loan.PaymentMethod = PaymentMethod.ACH;

            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled | Completed | ACH           |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled | Completed | ACH           |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled | ACH           |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled | ACH           |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled | ACH           |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled | ACH           |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00";

            GracePeriod.Duration = 10;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 6068.05, Start = Date(2015, 2, 15), End = Date(2015, 3, 10) }
            })
            .Returns(57.90);

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 4565.27, Start = Date(2015, 3, 10) },
                new InterestAccrualPeriod { Principal = 4565.27, Start = Date(2015, 3, 15) }
            })
            .Returns(60.98);

            AmortizationService.OnAmortize(new AmortizationRequest
            {
                ReferenceDate = Date(2015, 3, 17),
                PrincipalBalance = 4565.27,
                OpeningInterest = 60.98
            })
            .Returns(@"
                AnniversaryDate | DueDate    | OpeningBalance | Amount  | Interest | Principal | EndingBalance
                2015-04-15      | 2015-04-17 |        4565.27 | 1560.68 |    60.98 |   1499.70 |       3065.57
                2015-05-15      | 2015-05-17 |        3065.57 | 1560.68 |    35.10 |   1525.58 |       1539.99
                2015-06-15      | 2015-06-17 |        1539.99 | 1557.62 |    17.63 |   1539.99 |          0.00");

            Service.AddPayment(new PaymentInfo
            {
                Amount = 1560.68,
                DueDate = Date(2015, 3, 10),
                PaymentId = "pmt003",
                Type = InstallmentType.Scheduled,
                LoanReferenceNumber = ReferenceNumber,
                PaymentMethod = PaymentMethod.Check
            });

            AssertSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled | Completed | ACH           |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled | Completed | ACH           |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-10  | pmt003    | Scheduled | Scheduled | Check         |        6068.05 |       1560.68 |    57.90 |   1502.78 |       4565.27
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled | ACH           |        4565.27 |       1560.68 |    60.98 |   1499.70 |       3065.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled | ACH           |        3065.57 |       1560.68 |    35.10 |   1525.58 |       1539.99
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled | ACH           |        1539.99 |       1557.62 |    17.63 |   1539.99 |          0.00"
            );
        }

        [Fact]
        public void CannotAddEarlyUnderpayment_1stInstallment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             |           | Scheduled | Scheduled | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |             |           | Scheduled | Scheduled | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled | Check         |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled | Check         |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled | Check         |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled | Check         |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00";

            GracePeriod.Duration = 10;

            AssertException.Throws<ArgumentException>("Installment not found due on 2015-01-09", () =>
            {
                Service.AddPayment(new PaymentInfo
                {
                    Amount = 1560.67, //one cent less than expected
                    DueDate = Date(2015, 1, 9),
                    PaymentId = "pmt001",
                    Type = InstallmentType.Scheduled,
                    LoanReferenceNumber = ReferenceNumber,
                    PaymentMethod = PaymentMethod.Check
                });
            });
        }

        [Fact]
        public void CannotAddEarlyUnderpayment_3rdInstallment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             |           | Scheduled | Scheduled | Check         |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             |           | Scheduled | Scheduled | Check         |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             |           | Scheduled | Scheduled | Check         |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled | Check         |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00";

            GracePeriod.Duration = 10;

            AssertException.Throws<ArgumentException>("Installment not found due on 2015-03-10", () =>
            {
                Service.AddPayment(new PaymentInfo
                {
                    Amount = 1560.67, //one cent less than expected
                    DueDate = Date(2015, 3, 10),
                    PaymentId = "pmt003",
                    Type = InstallmentType.Scheduled,
                    LoanReferenceNumber = ReferenceNumber,
                    PaymentMethod = PaymentMethod.Check
                });
            });
        }

        [Fact]
        public void CannotAddEarlyUnderpayment_LastInstallment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | PaymentId | Type      | Status    | PaymentMethod | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-15  | pmt001    | Scheduled | Completed | Check         |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-15  | pmt002    | Scheduled | Completed | Check         |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 | 2015-03-15  | pmt003    | Scheduled | Completed | Check         |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 | 2015-04-15  | pmt004    | Scheduled | Completed | Check         |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 | 2015-05-15  | pmt005    | Scheduled | Completed | Check         |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             |           | Scheduled | Scheduled | Check         |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00";

            GracePeriod.Duration = 10;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 1543.03, Start = Date(2015, 5, 15), End = Date(2015, 6, 10) }
            })
            .Returns(14.72);

            AssertException.Throws<ArgumentException>("Installment not found due on 2015-06-10", () =>
            {
                Service.AddPayment(new PaymentInfo
                {
                    Amount = 1557.74, //one cent less than expected
                    DueDate = Date(2015, 6, 10),
                    PaymentId = "pmt006",
                    Type = InstallmentType.Scheduled,
                    LoanReferenceNumber = ReferenceNumber,
                    PaymentMethod = PaymentMethod.Check
                });
            });
        }
    }
}
