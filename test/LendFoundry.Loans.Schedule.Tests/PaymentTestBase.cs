﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Testing;
using LendFoundry.Loans.Repository;
using LendFoundry.Loans.Schedule.Calculators;
using LendFoundry.Loans.Schedule.Data;
using LendFoundry.Loans.Schedule.Tests.Fake;
using LendFoundry.Loans.Schedule.Tests.Mocks;
using Moq;
using System;
using System.Linq;

namespace LendFoundry.Loans.Schedule.Tests
{
    public abstract class PaymentTestBase
    {
        protected const string ReferenceNumber = "loan001";

        protected IPaymentScheduleService Service { get; }
        protected StaticTodayTenantTime TenantTime { get; } = new StaticTodayTenantTime(new UtcTenantTime());
        protected AmortizationServiceMock AmortizationService { get; }
        protected DueDateBasedGracePeriod GracePeriod { get; } = new DueDateBasedGracePeriod();
        private Mock<ILoanRepository> LoanRepositoryMock { get; } = new Mock<ILoanRepository>();
        private IPaymentScheduleRepository ScheduleRepository { get; } = new FakePaymentScheduleRepository();

        protected ILoan Loan { get; }

        public PaymentTestBase()
        {
            Loan = new Loan
            {
                ReferenceNumber = ReferenceNumber,
                Terms = new LoanTerms
                {
                    OriginationDate = Date(2014, 12, 15),
                    LoanAmount = 9000.0,
                    Term = 6,
                    Rate = 13.74,
                    PaymentAmount = 1560.68
                }
            };

            AmortizationService = new AmortizationServiceMock(Loan.Terms);

            Service = new PaymentScheduleService(
                Mock.Of<PaymentScheduleConfiguration>(),
                AmortizationService.Object,
                ScheduleRepository,
                LoanRepositoryMock.Object,
                Mock.Of<IPayoffAmountCalculator>(),
                new GracePeriodCalculator(GracePeriod),
                Mock.Of<IDaysPastDueCalculator>(),
                Mock.Of<ICurrentDueCalculator>(),
                Mock.Of<IEventHubClient>(),
                TenantTime,
                Mock.Of<ILogger>()
            );

            LoanRepositoryMock
                .Setup(repository => repository.Get(ReferenceNumber))
                .Returns(Loan);
        }

        protected DateTimeOffset Today
        {
            get { return TenantTime.Today; }
            set { TenantTime.Today = value; }
        }

        protected DateTimeOffset Date(int year, int month, int day) => TenantTime.Create(year, month, day);

        protected string Schedule
        {
            set
            {
                var installments = ListParser
                    .Parse<Installment>(value)
                    .Select(installment => (IInstallment)installment)
                    .ToList();

                ScheduleRepository.Create(new PaymentSchedule
                {
                    LoanReferenceNumber = ReferenceNumber,
                    Installments = installments
                });
            }
        }

        protected void AssertSchedule(string expectedSchedule)
        {
            var paymentSchedule = ScheduleRepository.Get(ReferenceNumber);
            AssertTable.Equal(expectedSchedule, paymentSchedule.Installments);
        }
    }
}
