﻿using LendFoundry.Amortization;
using LendFoundry.Amortization.Client;
using LendFoundry.Amortization.Interest;
using LendFoundry.Foundation.Testing;
using LendFoundry.Loans.Schedule.Tests.Collections;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Xunit.Sdk;
using AmortizationInstallment = LendFoundry.Amortization.Installment;

namespace LendFoundry.Loans.Schedule.Tests.Mocks
{
    public class Outcome<T>
    {
        public Outcome(Action<T> returns) { Returns = returns; }
        public Action<T> Returns { get; }
    }

    public class AmortizationServiceMock
    {
        public AmortizationServiceMock(ILoanTerms loanTerms)
        {
            LoanTerms = loanTerms;
        }

        public IAmortizationService Object => AmortizationService.Object;

        private ILoanTerms LoanTerms { get; }
        private Mock<IAmortizationService> AmortizationService { get; } = new Mock<IAmortizationService>(MockBehavior.Strict);
        private InfiniteQueue<AmortizationRequest> AmortizationRequestQueue { get; } = new InfiniteQueue<AmortizationRequest>();
        private InfiniteQueue<IEnumerable<AmortizationInstallment>> AmortizationResultQueue { get; } = new InfiniteQueue<IEnumerable<AmortizationInstallment>>();
        private List<GetSimpleInterestExpectation> GetSimpleInterestExpectations = new List<GetSimpleInterestExpectation>();
        private Queue<double> SimpleInterestResultQueue { get; } = new Queue<double>();

        public Outcome<string> OnAmortize(AmortizationRequest request)
        {
            return new Outcome<string>((amortizationTable) =>
            {
                AmortizationRequestQueue.Enqueue(request);
                AmortizationResultQueue.Enqueue(amortizationTable?.Trim().Length > 0 ? ListParser.Parse<AmortizationInstallment>(amortizationTable) : new List<AmortizationInstallment>());

                AmortizationService
                    .Setup(service => service.Amortize(It.IsAny<AmortizationRequest>()))
                    .Callback((AmortizationRequest actualRequest) => AssertEquals(AmortizationRequestQueue.Dequeue(), actualRequest))
                    .Returns(() => AmortizationResultQueue.Dequeue());
            });
        }

        public Outcome<double> OnGetSimpleInterest(double principal, DateTimeOffset start, DateTimeOffset end)
        {
            return OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod
                {
                    Principal = principal,
                    Start = start,
                    End = end
                }
            });
        }

        public Outcome<double> OnGetSimpleInterest(double principal, DateTimeOffset start, DateTimeOffset end, InterestAccrualOptions options)
        {
            return OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod
                {
                    Principal = principal,
                    Start = start,
                    End = end
                }
            }, options);
        }

        public Outcome<double> OnGetSimpleInterest(IEnumerable<InterestAccrualPeriod> periods)
        {
            return new Outcome<double>((returnValue) =>
            {
                GetSimpleInterestExpectations.Add(new GetSimpleInterestExpectation
                {
                    ExpectedTerms = ToAmortizationTerms(LoanTerms),
                    ExpectedInterestAccrualPeriods = periods,
                    ReturnValue = returnValue
                });

                AmortizationService
                    .Setup(a => a.GetSimpleInterest(It.IsAny<Amortization.ILoanTerms>(), It.IsAny<IEnumerable<InterestAccrualPeriod>>()))
                    .Callback((Amortization.ILoanTerms actualLoanTerms, IEnumerable<InterestAccrualPeriod> actualPeriods) =>
                    {
                        var expectations = GetSimpleInterestExpectations.SingleOrDefault(e =>
                            AreEqual(e.ExpectedTerms, actualLoanTerms) && 
                            AreEqual(e.ExpectedInterestAccrualPeriods, actualPeriods)
                        );

                        if (expectations == null)
                            throw new XunitException("Unexpected parameters");

                        SimpleInterestResultQueue.Enqueue(expectations.ReturnValue);
                    })
                    .Returns(() => SimpleInterestResultQueue.Dequeue());
            });
        }

        public Outcome<double> OnGetSimpleInterest(IEnumerable<InterestAccrualPeriod> periods, InterestAccrualOptions options)
        {
            return new Outcome<double>((returnValue) =>
            {
                GetSimpleInterestExpectations.Add(new GetSimpleInterestExpectation
                {
                    ExpectedTerms = ToAmortizationTerms(LoanTerms),
                    ExpectedInterestAccrualPeriods = periods,
                    ExpectedOptions = options,
                    ReturnValue = returnValue
                });

                AmortizationService
                    .Setup(a => a.GetSimpleInterest(It.IsAny<Amortization.ILoanTerms>(), It.IsAny<IEnumerable<InterestAccrualPeriod>>(), It.IsAny<InterestAccrualOptions>()))
                    .Callback((Amortization.ILoanTerms actualLoanTerms, IEnumerable<InterestAccrualPeriod> actualPeriods, InterestAccrualOptions actualOptions) =>
                    {
                        var expectations = GetSimpleInterestExpectations.SingleOrDefault(e =>
                            AreEqual(e.ExpectedTerms, actualLoanTerms) && 
                            AreEqual(e.ExpectedInterestAccrualPeriods, actualPeriods) &&
                            AreEqual(e.ExpectedOptions, actualOptions)
                        );

                        if (expectations == null)
                            throw new XunitException("Unexpected parameters");

                        SimpleInterestResultQueue.Enqueue(expectations.ReturnValue);
                    })
                    .Returns(() => SimpleInterestResultQueue.Dequeue());
            });
        }

        private bool AreEqual(InterestAccrualOptions expected, InterestAccrualOptions actual)
        {
            return expected.EffectiveEndDate == actual.EffectiveEndDate;
        }

        private Amortization.ILoanTerms ToAmortizationTerms(ILoanTerms loanTerms)
        {
            return new Amortization.LoanTerms
            {
                OriginationDate = loanTerms.OriginationDate,
                LoanAmount = loanTerms.LoanAmount,
                Term = loanTerms.Term,
                Rate = loanTerms.Rate,
                Fees = ToAmortizationFees(loanTerms.Fees)
            };
        }

        private IEnumerable<Amortization.Fee> ToAmortizationFees(IEnumerable<IFee> fees)
        {
            if (fees == null) return null;
            return fees.Select(f => new Amortization.Fee
            {
                Amount = f.Amount,
                Type = f.Type == FeeType.Fixed ? Amortization.FeeType.Fixed : Amortization.FeeType.Percent
            });
        }

        private bool AreEqual(IEnumerable<InterestAccrualPeriod> expected, IEnumerable<InterestAccrualPeriod> actual)
        {
            if (expected == null) return actual == null;
            if (actual == null) return expected == null;

            if (!HaveSameCount(expected, actual))
                return false;

            var expectedList = expected.ToList();
            var actualList = actual.ToList();
            for (int index = 0; index < expectedList.Count; index++)
                if (!AreEqual(expectedList[index], actualList[index]))
                    return false;

            return true;
        }

        private bool AreEqual(InterestAccrualPeriod expected, InterestAccrualPeriod actual)
        {
            return
                AreEqual(expected.Principal, actual.Principal) &&
                expected.Start == actual.Start &&
                expected.End == actual.End;
        }

        private bool AreEqual(Amortization.ILoanTerms expected, Amortization.ILoanTerms actual)
        {
            return
                expected.OriginationDate == actual.OriginationDate &&
                AreEqual(expected.LoanAmount, actual.LoanAmount) &&
                expected.Term == actual.Term &&
                AreEqual(expected.Rate, actual.Rate) &&
                AreEqual(expected.Fees, actual.Fees);
        }

        private bool AreEqual(IEnumerable<Amortization.Fee> expected, IEnumerable<Amortization.Fee> actual)
        {
            if (expected == null) return actual == null;
            if (actual == null) return expected == null;

            if (!HaveSameCount(expected, actual))
                return false;

            var expectedList = expected.ToList();
            var actualList = actual.ToList();
            for (int index = 0; index < expectedList.Count; index++)
                if (!AreEqual(expectedList[index], actualList[index]))
                    return false;

            return true;
        }

        private bool HaveSameCount<T>(IEnumerable<T> a, IEnumerable<T> b)
        {
            return a.Count() == b.Count();
        }

        private bool AreEqual(Amortization.Fee expected, Amortization.Fee actual)
        {
            return
                AreEqual(expected.Amount, actual.Amount) &&
                expected.Type == actual.Type;
        }

        public void FailOnAmortize()
        {
            AmortizationService
                .Setup(s => s.Amortize(It.IsAny<AmortizationRequest>()))
                .Throws(new Exception("Unexpected call to amortization service"));
        }

        private void AssertEquals(AmortizationRequest expected, AmortizationRequest actual)
        {
            Assert.NotNull(actual);

            AssertEquals(LoanTerms, actual.LoanTerms);
            Assert.Equal(expected.ReferenceDate, actual.ReferenceDate);
            AssertEquals(expected.PrincipalBalance, actual.PrincipalBalance);
            AssertEquals(expected.OpeningInterest, actual.OpeningInterest);
        }

        private static void AssertEquals(ILoanTerms expectedLoanTerms, Amortization.ILoanTerms actualLoanTerms)
        {
            Assert.Equal(expectedLoanTerms.OriginationDate, actualLoanTerms.OriginationDate);
            Assert.Equal(expectedLoanTerms.LoanAmount, actualLoanTerms.LoanAmount);
            Assert.Equal(expectedLoanTerms.Term, actualLoanTerms.Term);
            Assert.Equal(expectedLoanTerms.Rate, actualLoanTerms.Rate);
        }

        private void AssertEquals(IEnumerable<InterestAccrualPeriod> expected, IEnumerable<InterestAccrualPeriod> actual)
        {
            if (expected == null)
                Assert.Null(actual);
            else
            {
                Assert.NotNull(actual);
                Assert.Equal(expected.Count(), actual.Count());
                for (var index = 0; index < expected.Count(); index++)
                    AssertEquals(expected.ToList()[index], actual.ToList()[index]);
            }
        }

        private void AssertEquals(InterestAccrualPeriod expected, InterestAccrualPeriod actual)
        {
            if (expected == null)
                Assert.Null(actual);
            else
            {
                Assert.NotNull(actual);
                Assert.Equal(expected.Principal, actual.Principal, 2);
                Assert.Equal(expected.Start, actual.Start);
                Assert.Equal(expected.End, actual.End);
            }
        }

        private static void AssertEquals(double? expectedValue, double? actualValue)
        {
            if (expectedValue == null)
                Assert.Null(actualValue);
            else
                Assert.Equal(expectedValue.Value, actualValue.Value, 2);
        }

        private bool AreEqual(double? a, double? b)
        {
            if (a == null) return b == null;
            if (b == null) return a == null;
            return AreEqual(a.Value, b.Value);
        }

        private bool AreEqual(double a, double b)
        {
            return Math.Abs(a - b) <= 0.00000001;
        }

        private class GetSimpleInterestExpectation
        {
            public Amortization.ILoanTerms ExpectedTerms { get; set; }
            public IEnumerable<InterestAccrualPeriod> ExpectedInterestAccrualPeriods { get; set; }
            public double ReturnValue { get; set; }
            public InterestAccrualOptions ExpectedOptions { get; set; }
        }
    }
}
