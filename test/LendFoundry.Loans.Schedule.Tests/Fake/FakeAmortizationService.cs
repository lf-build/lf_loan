﻿using System;
using System.Collections.Generic;
using LendFoundry.Amortization;
using LendFoundry.Amortization.Client;
using LendFoundry.Amortization.Interest;

namespace LendFoundry.Loans.Schedule.Tests.Fake
{
	public class FakeAmortizationService : IAmortizationService
	{
		public IList<Amortization.IInstallment> InMemoryData = new List<Amortization.IInstallment>(); 

		public IEnumerable<Amortization.IInstallment> Amortize(AmortizationRequest request)
		{
			return InMemoryData;
		}

		public double GetAPR(LoanInfo loanInfo)
		{
			throw new NotImplementedException();
		}

		public double GetPaymentAmount(LoanInfo loanInfo)
		{
			throw new NotImplementedException();
		}

		public double GetSimpleInterest(double amount, double rate, DateTimeOffset start, DateTimeOffset end)
		{
			throw new NotImplementedException();
		}

		public double GetSimpleInterest(Amortization.ILoanTerms loanTerms, IEnumerable<InterestAccrualPeriod> interestAccrualPeriods)
		{
			throw new NotImplementedException();
		}

		public double GetSimpleInterest(Amortization.ILoanTerms loanTerms, IEnumerable<InterestAccrualPeriod> interestAccrualPeriods, InterestAccrualOptions options)
		{
			throw new NotImplementedException();
		}
	}
}