﻿using System;

namespace LendFoundry.Loans.Schedule.Tests.Fake
{
    public class DueDateBasedGracePeriod : IGracePeriod
    {
        public int Duration { get; set; }
        public DateTimeOffset GetStartDate(IInstallment installment) => installment.AnniversaryDate.Value;
        public DateTimeOffset GetEndDateReference(IInstallment installment) => installment.DueDate.Value;
    }
}
