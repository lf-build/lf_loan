﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Loans.Schedule.Data;

namespace LendFoundry.Loans.Schedule.Tests.Fake
{
    public class FakePaymentScheduleRepository : IPaymentScheduleRepository
    {
        private IDictionary<string, IPaymentSchedule> schedules = new Dictionary<string, IPaymentSchedule>();

        private IInstallment Clone(IInstallment installment)
        {
            return new Installment
            {
                AnniversaryDate = installment.AnniversaryDate,
                DueDate = installment.DueDate,
                EndingBalance = installment.EndingBalance,
                Interest = installment.Interest,
                OpeningBalance = installment.OpeningBalance,
                PaymentAmount = installment.PaymentAmount,
                PaymentDate = installment.PaymentDate,
                PaymentId = installment.PaymentId,
                PaymentMethod = installment.PaymentMethod,
                Principal = installment.Principal,
                Status = installment.Status,
                Type = installment.Type
            };
        }

        public Task Create(IPaymentSchedule paymentSchedule)
        {
            schedules[paymentSchedule.LoanReferenceNumber] = paymentSchedule;
            return Task.Run(() => { });
        }

        void IPaymentScheduleRepository.Create(IPaymentSchedule paymentSchedule)
        {
            schedules[paymentSchedule.LoanReferenceNumber] = paymentSchedule;
        }

        public Task Delete(string loanReferenceNumber)
        {
            schedules.Remove(loanReferenceNumber);
            return Task.Run(() => { });
        }

        public IPaymentSchedule Get(string loanReferenceNumber)
        {
            var schedule = schedules[loanReferenceNumber];
            return new PaymentSchedule
            {
                Id = schedule.Id,
                TenantId = schedule.TenantId,
                LoanReferenceNumber = schedule.LoanReferenceNumber,
                Installments = schedule.Installments.Select(i => Clone(i))
            };
        }

        public IEnumerable<string> GetLoans(InstallmentStatus installmentStatus, DateTimeOffset minDueDate, DateTimeOffset maxDueDate)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetLoans(InstallmentStatus installmentStatus, DateTimeOffset minDueDate, DateTimeOffset maxDueDate, PaymentMethod paymentMethod)
        {
            throw new NotImplementedException();
        }

        public void MarkInstallmentAsCompleted(string loanReferenceNumber, DateTimeOffset dueDate, string paymentId, DateTimeOffset paymentDate)
        {
            throw new Exception("Obsolete");
        }

        public void AssignPaymentToInstallment(string loanReferenceNumber, DateTimeOffset dueDate, string paymentId, DateTimeOffset? paymentDate)
        {
            var schedule = schedules[loanReferenceNumber];

            var installment = schedule
                .Installments
                .First(candidate => candidate.DueDate == dueDate);

            installment.PaymentId = paymentId;
            installment.PaymentDate = paymentDate;
        }

        public void ReplaceInstallments(string loanReferenceNumber, DateTimeOffset startDate, IEnumerable<IInstallment> newInstallments)
        {
            var schedule = schedules[loanReferenceNumber];

            var installments = new List<IInstallment>();
            installments.AddRange(schedule.Installments.Where(installment => installment.DueDate < startDate));
            installments.AddRange(newInstallments);

            Create(new PaymentSchedule
            {
                LoanReferenceNumber = loanReferenceNumber,
                Installments = installments
            });
        }

        public void ReplaceInstallments(string loanReferenceNumber, IEnumerable<IInstallment> installments)
        {
            Create(new PaymentSchedule
            {
                LoanReferenceNumber = loanReferenceNumber,
                Installments = installments
            });
        }

        public IEnumerable<string> GetLoansDueIn(DateTimeOffset dueDate)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IPaymentSchedule> GetInstallmentsDueIn(DateTimeOffset dueDate, PaymentMethod paymentMethod)
        {
            throw new NotImplementedException();
        }

        public void UpdateInstallmentStatus(string loanReferenceNumber, DateTimeOffset dueDate, InstallmentStatus newStatus)
        {
            throw new NotImplementedException();
        }

        public void MarkInstallmentAsCompleted(string loanReferenceNumber, string paymentId, DateTimeOffset paymentDate)
        {
            if (!schedules.ContainsKey(loanReferenceNumber))
                throw new PaymentScheduleNotFoundException(loanReferenceNumber);

            var schedule = schedules[loanReferenceNumber];

            var installment = schedule.Installments.SingleOrDefault(i => i.PaymentId == paymentId);
            if (installment == null)
                throw new InstallmentNotFoundException(paymentId, loanReferenceNumber);

            installment.Status = InstallmentStatus.Completed;
            installment.PaymentDate = paymentDate;
        }

        public IEnumerable<IPaymentSchedule> GetInstallmentsDueIn(DateTimeOffset date, PaymentMethod paymentMethod, InstallmentType type)
        {
            throw new NotImplementedException();
        }

        public void UpdatePaymentMethodOfScheduledInstallments(string loanReferenceNumber, PaymentMethod newPaymentMethod)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IPaymentSchedule> GetInstallmentsDueInByAnniversaryDate(DateTimeOffset date, PaymentMethod paymentMethod, InstallmentType type)
        {
            throw new NotImplementedException();
        }
    }
}
