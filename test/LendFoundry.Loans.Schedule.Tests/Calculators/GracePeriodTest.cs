﻿using LendFoundry.Foundation.Testing;
using LendFoundry.Loans.Schedule.Calculators;
using LendFoundry.Loans.Schedule.Tests.Fake;
using Moq;
using System;
using Xunit;

namespace LendFoundry.Loans.Schedule.Tests.Calculators
{
    public class GracePeriodTest : BaseScheduleTest
    {
        private DueDateBasedGracePeriod GracePeriod { get; } = new DueDateBasedGracePeriod();
        private IGracePeriodCalculator GracePeriodCalculator { get; }

        public GracePeriodTest()
        {
            GracePeriodCalculator = new GracePeriodCalculator(GracePeriod);
        }

        [Fact]
        public void ItIsBeforeGracePeriodBeforeAnniversaryDate()
        {
            GracePeriod.Duration = 10;

            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status   
                2015-01-15      | 2015-01-17 | Scheduled | Completed
                2015-02-15      | 2015-02-17 | Scheduled | Scheduled
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled");

            Assert.True(GracePeriodCalculator.IsBeforeGracePeriod(Date(2015, 2, 14), Schedule));
        }

        [Fact]
        public void NotBeforeGracePeriodOnAnniversaryDate()
        {
            GracePeriod.Duration = 10;

            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status   
                2015-01-15      | 2015-01-17 | Scheduled | Completed
                2015-02-15      | 2015-02-17 | Scheduled | Scheduled
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled");

            Assert.False(GracePeriodCalculator.IsBeforeGracePeriod(Date(2015, 2, 15), Schedule));
        }

        [Fact]
        public void NotBeforeGracePeriodPastAnniversaryDate()
        {
            GracePeriod.Duration = 10;

            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status   
                2015-01-15      | 2015-01-17 | Scheduled | Completed
                2015-02-15      | 2015-02-17 | Scheduled | Scheduled
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled");

            Assert.False(GracePeriodCalculator.IsBeforeGracePeriod(Date(2015, 2, 16), Schedule));
        }

        [Fact]
        public void NotBeforeGracePeriodWhenLoanIsPaidOff()
        {
            GracePeriod.Duration = 10;

            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status   
                2015-01-15      | 2015-01-17 | Scheduled | Completed
                2015-02-15      | 2015-02-17 | Scheduled | Completed
                2015-03-15      | 2015-03-17 | Scheduled | Completed");

            Assert.False(GracePeriodCalculator.IsBeforeGracePeriod(Date(2015, 2, 14), Schedule));
        }

        [Fact]
        public void NotInGracePeriodBeforeAnniversaryDate()
        {
            GracePeriod.Duration = 10;

            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status   
                2015-01-15      | 2015-01-17 | Scheduled | Scheduled
                2015-02-15      | 2015-02-17 | Scheduled | Scheduled
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled");

            Assert.False(GracePeriodCalculator.IsInGracePeriod(Date(2015, 1, 14), Schedule));
        }

        [Fact]
        public void InGracePeriodOnAnniversaryDate()
        {
            GracePeriod.Duration = 10;

            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status   
                2015-01-15      | 2015-01-17 | Scheduled | Scheduled
                2015-02-15      | 2015-02-17 | Scheduled | Scheduled
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled");

            Assert.True(GracePeriodCalculator.IsInGracePeriod(Date(2015, 1, 15), Schedule));
        }

        [Fact]
        public void InGracePeriodBeforeEndOfGracePeriod()
        {
            GracePeriod.Duration = 10;

            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status   
                2015-01-15      | 2015-01-17 | Scheduled | Scheduled
                2015-02-15      | 2015-02-17 | Scheduled | Scheduled
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled");

            Assert.True(GracePeriodCalculator.IsInGracePeriod(Date(2015, 1, 20), Schedule));
        }

        [Fact]
        public void InGracePeriodOnLastDayOfGracePeriod()
        {
            GracePeriod.Duration = 10;

            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status   
                2015-01-15      | 2015-01-17 | Scheduled | Scheduled
                2015-02-15      | 2015-02-17 | Scheduled | Scheduled
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled");

            Assert.True(GracePeriodCalculator.IsInGracePeriod(Date(2015, 1, 27), Schedule));
        }

        [Fact]
        public void NotInGracePeriodWhenPastLastDayOfGracePeriod()
        {
            GracePeriod.Duration = 10;

            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status   
                2015-01-15      | 2015-01-17 | Scheduled | Scheduled
                2015-02-15      | 2015-02-17 | Scheduled | Scheduled
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled");

            Assert.False(GracePeriodCalculator.IsInGracePeriod(Date(2015, 1, 28), Schedule));
        }

        [Fact]
        public void NotPastGracePeriodBeforeAnniversaryDate()
        {
            GracePeriod.Duration = 10;

            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status   
                2015-01-15      | 2015-01-17 | Scheduled | Completed
                2015-02-15      | 2015-02-17 | Scheduled | Scheduled
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled");

            Assert.False(GracePeriodCalculator.IsPastGracePeriod(Date(2015, 2, 14), Schedule));
        }

        [Fact]
        public void NotPastGracePeriodOnAnniversaryDate()
        {
            GracePeriod.Duration = 10;

            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status   
                2015-01-15      | 2015-01-17 | Scheduled | Completed
                2015-02-15      | 2015-02-17 | Scheduled | Scheduled
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled");

            Assert.False(GracePeriodCalculator.IsPastGracePeriod(Date(2015, 2, 15), Schedule));
        }

        [Fact]
        public void NotPastGracePeriodBeforeEndOfGracePeriod()
        {
            GracePeriod.Duration = 10;

            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status   
                2015-01-15      | 2015-01-17 | Scheduled | Completed
                2015-02-15      | 2015-02-17 | Scheduled | Scheduled
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled");

            Assert.False(GracePeriodCalculator.IsPastGracePeriod(Date(2015, 2, 23), Schedule));
        }

        [Fact]
        public void NotPastGracePeriodOnLastDayOfGracePeriod()
        {
            GracePeriod.Duration = 10;

            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status   
                2015-01-15      | 2015-01-17 | Scheduled | Completed
                2015-02-15      | 2015-02-17 | Scheduled | Scheduled
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled");

            Assert.False(GracePeriodCalculator.IsPastGracePeriod(Date(2015, 2, 27), Schedule));
        }

        [Fact]
        public void ItIsPastGracePeriodPastTheLastDayOfGracePeriod()
        {
            GracePeriod.Duration = 10;

            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status   
                2015-01-15      | 2015-01-17 | Scheduled | Completed
                2015-02-15      | 2015-02-17 | Scheduled | Scheduled
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled");

            Assert.True(GracePeriodCalculator.IsPastGracePeriod(Date(2015, 2, 28), Schedule));
        }

        [Fact]
        public void NotPastGracePeriodWhenLoanIsPaidOff()
        {
            GracePeriod.Duration = 10;

            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status   
                2015-01-15      | 2015-01-17 | Scheduled | Completed
                2015-02-15      | 2015-02-17 | Scheduled | Completed
                2015-03-15      | 2015-03-17 | Scheduled | Completed");

            Assert.False(GracePeriodCalculator.IsPastGracePeriod(Date(2015, 3, 28), Schedule));
        }

        [Fact]
        public void IgnoreExtraPayments()
        {
            GracePeriod.Duration = 10;

            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type       | Status   
                                | 2015-01-12 | Additional | Scheduled
                2015-01-15      | 2015-01-17 | Scheduled  | Scheduled
                                | 2015-01-28 | Additional | Scheduled
                2015-02-15      | 2015-02-17 | Scheduled  | Scheduled
                2015-03-15      | 2015-03-17 | Scheduled  | Scheduled");

            Assert.False(GracePeriodCalculator.IsInGracePeriod(Date(2015, 1, 12), Schedule));
            Assert.False(GracePeriodCalculator.IsInGracePeriod(Date(2015, 1, 14), Schedule));
            Assert.False(GracePeriodCalculator.IsInGracePeriod(Date(2015, 1, 28), Schedule));
            Assert.False(GracePeriodCalculator.IsInGracePeriod(Date(2015, 1, 30), Schedule));
        }

        [Fact]
        public void IgnorePaidInstallments()
        {
            GracePeriod.Duration = 10;

            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status   
                2015-01-15      | 2015-01-17 | Scheduled | Completed
                2015-02-15      | 2015-02-17 | Scheduled | Scheduled
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled");

            Assert.False(GracePeriodCalculator.IsInGracePeriod(Date(2015, 1, 17), Schedule));
            Assert.False(GracePeriodCalculator.IsInGracePeriod(Date(2015, 1, 27), Schedule));
            Assert.False(GracePeriodCalculator.IsInGracePeriod(Date(2015, 2, 14), Schedule));

            Assert.True(GracePeriodCalculator.IsInGracePeriod(Date(2015, 2, 15), Schedule));
            Assert.True(GracePeriodCalculator.IsInGracePeriod(Date(2015, 2, 23), Schedule));
            Assert.True(GracePeriodCalculator.IsInGracePeriod(Date(2015, 2, 27), Schedule));

            Assert.False(GracePeriodCalculator.IsInGracePeriod(Date(2015, 2, 28), Schedule));
            Assert.False(GracePeriodCalculator.IsInGracePeriod(Date(2015, 3, 17), Schedule));
        }

        [Fact]
        public void PaidOffLoanIsNeverIsGracePeriod()
        {
            GracePeriod.Duration = 10;

            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status   
                2015-01-15      | 2015-01-17 | Scheduled | Completed
                2015-02-15      | 2015-02-17 | Scheduled | Completed
                2015-03-15      | 2015-03-17 | Scheduled | Completed");

            Assert.False(GracePeriodCalculator.IsInGracePeriod(Date(2015, 1, 17), Schedule));
            Assert.False(GracePeriodCalculator.IsInGracePeriod(Date(2015, 2, 17), Schedule));
            Assert.False(GracePeriodCalculator.IsInGracePeriod(Date(2015, 3, 17), Schedule));
        }

        [Fact]
        public void GracePeriodStartsOnAnniversaryDateAndEndsDaysAfterTheDueDate()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status   
                2015-01-15      | 2015-01-17 | Scheduled | Scheduled
                2015-02-15      | 2015-02-17 | Scheduled | Scheduled
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled");

            GracePeriod.Duration = 5;

            var interval = GracePeriodCalculator.GetGracePeriod(Schedule);
            Assert.NotNull(interval);
            Assert.Equal(Date(2015, 1, 15), interval.Start);
            Assert.Equal(Date(2015, 1, 22), interval.End);
        }

        [Fact]
        public void GracePeriodStartingOnSecondInstallment()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status   
                2015-01-15      | 2015-01-17 | Scheduled | Completed
                2015-02-15      | 2015-02-17 | Scheduled | Scheduled
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled");

            GracePeriod.Duration = 10;

            var interval = GracePeriodCalculator.GetGracePeriod(Schedule);
            Assert.NotNull(interval);
            Assert.Equal(Date(2015, 2, 15), interval.Start);
            Assert.Equal(Date(2015, 2, 27), interval.End);
        }

        [Fact]
        public void GracePeriodStartingOnThirdInstallment()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status   
                2015-01-15      | 2015-01-17 | Scheduled | Completed
                2015-02-15      | 2015-02-17 | Scheduled | Completed
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled");

            GracePeriod.Duration = 10;

            var interval = GracePeriodCalculator.GetGracePeriod(Schedule);
            Assert.NotNull(interval);
            Assert.Equal(Date(2015, 3, 15), interval.Start);
            Assert.Equal(Date(2015, 3, 27), interval.End);
        }

        [Fact]
        public void GracePeriodIsNullWhenLoanIsPaidOff()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status   
                2015-01-15      | 2015-01-17 | Scheduled | Completed
                2015-02-15      | 2015-02-17 | Scheduled | Completed
                2015-03-15      | 2015-03-17 | Scheduled | Completed");

            GracePeriod.Duration = 10;

            var interval = GracePeriodCalculator.GetGracePeriod(Schedule);
            Assert.Null(interval);
        }

        [Fact]
        public void GetGracePeriodForGivenInstallment()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type       | Status   
                2015-01-15      | 2015-01-17 | Scheduled  | Completed
                2015-02-15      | 2015-02-17 | Scheduled  | Scheduled
                2015-03-15      | 2015-03-17 | Scheduled  | Scheduled");

            GracePeriod.Duration = 7;

            var interval = GracePeriodCalculator.GetGracePeriod(InstallmentWithAnniversaryOn(Date(2015, 1, 15)));
            Assert.NotNull(interval);
            Assert.Equal(Date(2015, 1, 15), interval.Start);
            Assert.Equal(Date(2015, 1, 24), interval.End);

            interval = GracePeriodCalculator.GetGracePeriod(InstallmentWithAnniversaryOn(Date(2015, 2, 15)));
            Assert.NotNull(interval);
            Assert.Equal(Date(2015, 2, 15), interval.Start);
            Assert.Equal(Date(2015, 2, 24), interval.End);

            interval = GracePeriodCalculator.GetGracePeriod(InstallmentWithAnniversaryOn(Date(2015, 3, 15)));
            Assert.NotNull(interval);
            Assert.Equal(Date(2015, 3, 15), interval.Start);
            Assert.Equal(Date(2015, 3, 24), interval.End);
        }

        [Fact]
        public void AnniversaryDateIsRequiredForGracePeriodCalculation()
        {
            var installment = Mock.Of<IInstallment>(i =>
                i.AnniversaryDate == null &&
                i.DueDate == Date(2015, 1, 1)
            );
            AssertException.Throws<ArgumentException>("Installment must have anniversary date", () =>
                GracePeriodCalculator.GetGracePeriod(installment)
            );
        }
    }
}
