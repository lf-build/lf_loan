﻿using LendFoundry.Amortization.Interest;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Testing;
using LendFoundry.Loans.Repository;
using LendFoundry.Loans.Schedule.Calculators;
using LendFoundry.Loans.Schedule.Data;
using LendFoundry.Loans.Schedule.Tests.Mocks;
using Moq;
using System;
using System.Linq;
using Xunit;

namespace LendFoundry.Loans.Schedule.Tests.Calculators
{
    public class PayoffAmountTest
    {
        private const string LoanReferenceNumber = "loan001";

        private IPaymentScheduleService Service { get; }
        private AmortizationServiceMock AmortizationService { get; }
        private Mock<ILoanRepository> LoanRepositoryMock { get; } = new Mock<ILoanRepository>(MockBehavior.Strict);
        private Mock<IPaymentScheduleRepository> ScheduleRepositoryMock { get; } = new Mock<IPaymentScheduleRepository>(MockBehavior.Strict);
        private PaymentScheduleConfiguration Configuration { get; } = new PaymentScheduleConfiguration();
        private StaticTodayTenantTime TenantTime { get; } = new StaticTodayTenantTime(new UtcTenantTime());
        private ILoan Loan { get; }
        private ILoanTerms Terms { get; }

        public PayoffAmountTest()
        {
            Terms = new LoanTerms();
            Loan = new Loan { Terms = Terms };

            AmortizationService = new AmortizationServiceMock(Terms);

            LoanRepositoryMock
                .Setup(repository => repository.Get(LoanReferenceNumber))
                .Returns(Loan);

            Service = new PaymentScheduleService
            (
                Configuration,
                AmortizationService.Object,
                ScheduleRepositoryMock.Object,
                LoanRepositoryMock.Object,
                new PayoffAmountCalculator(Configuration, AmortizationService.Object, TenantTime),
                Mock.Of<IGracePeriodCalculator>(),
                Mock.Of<IDaysPastDueCalculator>(),
                Mock.Of<ICurrentDueCalculator>(),
                Mock.Of<IEventHubClient>(),
                TenantTime,
                Mock.Of<ILogger>()
            );
        }

        protected string Schedule
        {
            set
            {
                var installments = ListParser
                    .Parse<Installment>(value)
                    .Select(installment => (IInstallment)installment)
                    .ToList();

                ScheduleRepositoryMock
                    .Setup(s => s.Get(LoanReferenceNumber))
                    .Returns(new PaymentSchedule
                    {
                        LoanReferenceNumber = LoanReferenceNumber,
                        Installments = installments
                    });
            }
        }

        private DateTimeOffset Date(int year, int month, int day) => TenantTime.Create(year, month, day);

        [Fact]
        public void BeforeFirstInstallmentAnniversaryDate()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Terms.LoanAmount = 9000.0;
            Terms.Term = 6;
            Terms.Rate = 13.74;

            Configuration.PayoffPeriod = 7;

            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |             | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00";

            TenantTime.Today = Date(2015, 1, 1);

            var payoffDate     = Date(2015, 1, 14);
            var validUntilDate = Date(2015, 1, 21);
            var daysOfInterestAccrual = (validUntilDate - Terms.OriginationDate).Days;
            var dailyRate = Terms.Rate / 100 / 360;
            var interest = Math.Round(Terms.LoanAmount * dailyRate * daysOfInterestAccrual, 2, MidpointRounding.AwayFromZero);
            var expectedPayoffAmount = Math.Round(Terms.LoanAmount + interest, 2, MidpointRounding.AwayFromZero);

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = Terms.LoanAmount, Start = Terms.OriginationDate, End = validUntilDate }
            })
            .Returns(interest);

            var payoffAmount = Service.GetPayoffAmount(LoanReferenceNumber, payoffDate);

            Assert.NotNull(payoffAmount);
            Assert.Equal(expectedPayoffAmount, payoffAmount.Amount);
            Assert.Equal(validUntilDate, payoffAmount.ValidUntil);
        }

        [Fact]
        public void OnFirstInstallmentAnniversaryDate()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Terms.LoanAmount = 9000.0;
            Terms.Term = 6;
            Terms.Rate = 13.74;

            Configuration.PayoffPeriod = 7;

            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |             | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00";

            TenantTime.Today = Date(2015, 1, 1);

            var payoffDate = Date(2015, 1, 15);
            var validUntilDate = Date(2015, 1, 22);
            var daysOfInterestAccrual = (validUntilDate - Terms.OriginationDate).Days;
            var dailyRate = Terms.Rate / 100 / 360;
            var interest = Math.Round(Terms.LoanAmount * dailyRate * daysOfInterestAccrual, 2, MidpointRounding.AwayFromZero);
            var expectedPayoffAmount = Math.Round(Terms.LoanAmount + interest, 2, MidpointRounding.AwayFromZero);

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = Terms.LoanAmount, Start = Terms.OriginationDate, End = validUntilDate }
            })
            .Returns(interest);

            var payoffAmount = Service.GetPayoffAmount(LoanReferenceNumber, payoffDate);

            Assert.NotNull(payoffAmount);
            Assert.Equal(expectedPayoffAmount, payoffAmount.Amount);
            Assert.Equal(validUntilDate, payoffAmount.ValidUntil);
        }

        [Fact]
        public void ExcludeInstallmentsDueOnPayoffDateFromThePayoffAmountCalculation_1stInstallment()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Terms.LoanAmount = 9000.0;
            Terms.Term = 6;
            Terms.Rate = 13.74;

            Configuration.PayoffPeriod = 7;

            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |             | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00";

            TenantTime.Today = Date(2015, 1, 1);

            var principalBalance = 9000.0;
            var interestAccrualStart = Terms.OriginationDate;
            var payoffDate = Date(2015, 1, 17);
            var validUntilDate = Date(2015, 1, 24);
            var daysOfInterestAccrual = (validUntilDate - interestAccrualStart).Days;
            var dailyRate = Terms.Rate / 100 / 360;
            var interest = Math.Round(principalBalance * dailyRate * daysOfInterestAccrual, 2, MidpointRounding.AwayFromZero);
            var expectedPayoffAmount = Math.Round(principalBalance + interest, 2, MidpointRounding.AwayFromZero);

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = principalBalance, Start = interestAccrualStart, End = validUntilDate }
            })
            .Returns(interest);

            var payoffAmount = Service.GetPayoffAmount(LoanReferenceNumber, payoffDate);

            Assert.NotNull(payoffAmount);
            Assert.Equal(expectedPayoffAmount, payoffAmount.Amount);
            Assert.Equal(validUntilDate, payoffAmount.ValidUntil);
        }
        
        [Fact]
        public void ExcludeInstallmentsDueOnPayoffDateFromThePayoffAmountCalculation_3rdInstallment()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Terms.LoanAmount = 9000.0;
            Terms.Term = 6;
            Terms.Rate = 13.74;

            Configuration.PayoffPeriod = 7;

            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00";

            TenantTime.Today = Date(2015, 3, 5);
            var payoffDate = Date(2015, 3, 17);

            var principalBalance = 6068.05;
            var interestAccrualStart = Date(2015, 2, 15);
            var validUntilDate = Date(2015, 3, 24);
            var daysOfInterestAccrual = (validUntilDate - interestAccrualStart).Days;
            var dailyRate = Terms.Rate / 100 / 360;
            var interest = Math.Round(principalBalance * dailyRate * daysOfInterestAccrual, 2, MidpointRounding.AwayFromZero);
            var expectedPayoffAmount = Math.Round(principalBalance + interest, 2, MidpointRounding.AwayFromZero);

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = principalBalance, Start = interestAccrualStart, End = validUntilDate }
            })
            .Returns(interest);

            var payoffAmount = Service.GetPayoffAmount(LoanReferenceNumber, payoffDate);

            Assert.NotNull(payoffAmount);
            Assert.Equal(expectedPayoffAmount, payoffAmount.Amount);
            Assert.Equal(validUntilDate, payoffAmount.ValidUntil);
        }
        [Fact]
        public void AfterFirstInstallmentDueDate()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Terms.LoanAmount = 9000.0;
            Terms.Term = 6;
            Terms.Rate = 13.74;

            Configuration.PayoffPeriod = 7;

            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |             | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00";

            TenantTime.Today = Date(2015, 1, 1);

            var anniversaryDate = Date(2015, 01, 15);
            var payoffDate = Date(2015, 1, 18);
            var validUntilDate = Date(2015, 1, 25);
            var daysOfInterestAccrual = (validUntilDate - anniversaryDate).Days;
            var dailyRate = Terms.Rate / 100 / 360;
            var principalBalance = 7542.37;
            var interest = Math.Round(principalBalance * dailyRate * daysOfInterestAccrual, 2, MidpointRounding.AwayFromZero);
            var expectedPayoffAmount = Math.Round(principalBalance + interest, 2, MidpointRounding.AwayFromZero);

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = principalBalance, Start = anniversaryDate, End = validUntilDate }
            })
            .Returns(interest);

            var payoffAmount = Service.GetPayoffAmount(LoanReferenceNumber, payoffDate);

            Assert.NotNull(payoffAmount);
            Assert.Equal(expectedPayoffAmount, payoffAmount.Amount);
            Assert.Equal(validUntilDate, payoffAmount.ValidUntil);
        }

        [Fact]
        public void OnThirdInstallmentAnniversaryDate()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Terms.LoanAmount = 9000.0;
            Terms.Term = 6;
            Terms.Rate = 13.74;

            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             | Scheduled | Scheduled |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |             | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00";

            TenantTime.Today = Date(2015, 1, 1);

            Configuration.PayoffPeriod  = 7;

            var referenceDate = Date(2015, 2, 15);
            var payoffDate = Date(2015, 3, 15);
            var validUntil = Date(2015, 3, 22);
            var principalBalance = 6068.05;
            var dailyRate = Terms.Rate / 100 / 360;
            var daysOfInterestAccrual = (validUntil - referenceDate).Days;
            var interest = Math.Round(dailyRate * principalBalance * daysOfInterestAccrual, 2, MidpointRounding.AwayFromZero);
            var expectedPayoffAmount = Math.Round(principalBalance + interest, 2, MidpointRounding.AwayFromZero);

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = principalBalance, Start = referenceDate, End = validUntil }
            })
            .Returns(interest);

            var payoffAmount = Service.GetPayoffAmount(LoanReferenceNumber, payoffDate);

            Assert.NotNull(payoffAmount);
            Assert.Equal(expectedPayoffAmount, payoffAmount.Amount);
            Assert.Equal(validUntil, payoffAmount.ValidUntil);
        }

        [Fact]
        public void AfterExtraPayment()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Terms.LoanAmount = 9000.0;
            Terms.Term = 6;
            Terms.Rate = 13.74;

            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                                | 2015-02-26 | 2015-02-26  | Additional | Completed |        6068.05 |        500.00 |     0.00 |    500.00 |       5568.05
                2015-03-15      | 2015-03-17 |             | Scheduled  | Scheduled |        5568.05 |       1560.68 |    65.85 |   1494.83 |       4073.22
                2015-04-15      | 2015-04-17 |             | Scheduled  | Scheduled |        4073.22 |       1560.68 |    46.64 |   1514.04 |       2559.18
                2015-05-15      | 2015-05-17 |             | Scheduled  | Scheduled |        2559.18 |       1560.68 |    29.30 |   1531.38 |       1027.80
                2015-06-15      | 2015-06-17 |             | Scheduled  | Scheduled |        1027.80 |       1039.57 |    11.77 |   1027.80 |          0.00";

            TenantTime.Today = Date(2015, 2, 26);

            Configuration.PayoffPeriod = 7;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 6068.05, Start = Date(2015, 02, 15) },
                new InterestAccrualPeriod { Principal = 5568.05, Start = Date(2015, 02, 26), End = Date(2015, 03, 10) }
            })
            .Returns(55.23);

            var payoffAmount = Service.GetPayoffAmount(LoanReferenceNumber, Date(2015, 03, 03));

            Assert.NotNull(payoffAmount);
            Assert.Equal(5623.28, payoffAmount.Amount);
            Assert.Equal(Date(2015, 03, 10), payoffAmount.ValidUntil);
        }

        [Fact]
        public void AfterTwoExtraPayments()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Terms.LoanAmount = 9000.0;
            Terms.Term = 6;
            Terms.Rate = 13.74;

            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                                | 2015-02-26 | 2015-02-26  | Additional | Completed |        6068.05 |        500.00 |     0.00 |    500.00 |       5568.05
                                | 2015-03-01 | 2015-03-01  | Additional | Completed |        5568.05 |        600.00 |     0.00 |    600.00 |       4968.05
                2015-03-15      | 2015-03-17 |             | Scheduled  | Scheduled |        4968.05 |       1560.68 |    62.65 |   1498.03 |       3470.02
                2015-04-15      | 2015-04-17 |             | Scheduled  | Scheduled |        3470.02 |       1560.68 |    39.73 |   1520.95 |       1949.07
                2015-05-15      | 2015-05-17 |             | Scheduled  | Scheduled |        1949.07 |       1560.68 |    22.32 |   1538.36 |        410.71
                2015-06-15      | 2015-06-17 |             | Scheduled  | Scheduled |         410.71 |        415.41 |     4.70 |    410.71 |          0.00";

            TenantTime.Today = Date(2015, 3, 1);

            Configuration.PayoffPeriod = 7;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 6068.05, Start = Date(2015, 02, 15) },
                new InterestAccrualPeriod { Principal = 5568.05, Start = Date(2015, 02, 26) },
                new InterestAccrualPeriod { Principal = 4968.05, Start = Date(2015, 03, 01), End = Date(2015, 03, 11) }
            })
            .Returns(55.07);

            var payoffAmount = Service.GetPayoffAmount(LoanReferenceNumber, Date(2015, 03, 04));

            Assert.NotNull(payoffAmount);
            Assert.Equal(5023.12, payoffAmount.Amount);
            Assert.Equal(Date(2015, 03, 11), payoffAmount.ValidUntil);
        }

        [Fact]
        public void AfterTwoExtraPaymentsOnTheSameDay()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Terms.LoanAmount = 9000.0;
            Terms.Term = 6;
            Terms.Rate = 13.74;

            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                                | 2015-02-17 | 2015-02-17  | Additional | Completed |        6068.05 |        101.01 |     0.00 |    101.01 |       5967.04
                                | 2015-02-17 | 2015-02-17  | Additional | Completed |        5967.04 |        202.02 |     0.00 |    202.02 |       5765.02
                2015-03-15      | 2015-03-17 |             | Scheduled  | Scheduled |        5765.02 |       1560.68 |    66.24 |   1494.44 |       4270.58
                2015-04-15      | 2015-04-17 |             | Scheduled  | Scheduled |        4270.58 |       1560.68 |    48.90 |   1511.78 |       2758.80
                2015-05-15      | 2015-05-17 |             | Scheduled  | Scheduled |        2758.80 |       1560.68 |    31.59 |   1529.09 |       1229.71
                2015-06-15      | 2015-06-17 |             | Scheduled  | Scheduled |        1229.71 |       1243.79 |    14.08 |   1229.71 |          0.00";

            TenantTime.Today = Date(2015, 2, 17);

            Configuration.PayoffPeriod = 7;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 6068.05, Start = Date(2015, 02, 15) },
                new InterestAccrualPeriod { Principal = 5967.04, Start = Date(2015, 02, 17) },
                new InterestAccrualPeriod { Principal = 5765.02, Start = Date(2015, 02, 17), End = Date(2015, 03, 06) }
            })
            .Returns(46.44);

            var payoffAmount = Service.GetPayoffAmount(LoanReferenceNumber, Date(2015, 02, 27));

            Assert.NotNull(payoffAmount);
            Assert.Equal(5811.46, payoffAmount.Amount);
            Assert.Equal(Date(2015, 03, 06), payoffAmount.ValidUntil);
        }

        [Fact]
        public void AfterTwoExtraPaymentsOnTheSameDayAsScheduledInstallment()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Terms.LoanAmount = 9000.0;
            Terms.Term = 6;
            Terms.Rate = 13.74;

            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-15 | 2015-02-15  | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                                | 2015-02-15 | 2015-02-15  | Additional | Completed |        6068.05 |        101.01 |     0.00 |    101.01 |       5967.04
                                | 2015-02-15 | 2015-02-15  | Additional | Completed |        5967.04 |        202.02 |     0.00 |    202.02 |       5765.02
                2015-03-15      | 2015-03-17 |             | Scheduled  | Scheduled |        5765.02 |       1560.68 |    66.01 |   1494.67 |       4270.35
                2015-04-15      | 2015-04-17 |             | Scheduled  | Scheduled |        4270.35 |       1560.68 |    48.90 |   1511.78 |       2758.57
                2015-05-15      | 2015-05-17 |             | Scheduled  | Scheduled |        2758.57 |       1560.68 |    31.59 |   1529.09 |       1229.48
                2015-06-15      | 2015-06-17 |             | Scheduled  | Scheduled |        1229.48 |       1243.56 |    14.08 |   1229.48 |          0.00";

            TenantTime.Today = Date(2015, 2, 15);

            Configuration.PayoffPeriod = 7;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 6068.05, Start = Date(2015, 02, 15) },
                new InterestAccrualPeriod { Principal = 5967.04, Start = Date(2015, 02, 15) },
                new InterestAccrualPeriod { Principal = 5765.02, Start = Date(2015, 02, 15), End = Date(2015, 03, 06) }
            })
            .Returns(46.21);

            var payoffAmount = Service.GetPayoffAmount(LoanReferenceNumber, Date(2015, 02, 27));

            Assert.NotNull(payoffAmount);
            Assert.Equal(5811.23, payoffAmount.Amount);
            Assert.Equal(Date(2015, 03, 06), payoffAmount.ValidUntil);
        }

        [Fact]
        public void AfterExtraPaymentBeforeFirstInstallment()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Terms.LoanAmount = 9000.0;
            Terms.Term = 6;
            Terms.Rate = 13.74;

            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                                | 2015-01-02 | 2015-01-02  | Additional | Scheduled |        9000.00 |        999.09 |     0.00 |    999.09 |       8000.91
                2015-01-15      | 2015-01-17 |             | Scheduled  | Scheduled |        8000.91 |       1560.68 |    98.09 |   1462.59 |       6538.32
                2015-02-15      | 2015-02-17 |             | Scheduled  | Scheduled |        6538.32 |       1560.68 |    74.86 |   1485.82 |       5052.50
                2015-03-15      | 2015-03-17 |             | Scheduled  | Scheduled |        5052.50 |       1560.68 |    57.85 |   1502.83 |       3549.67
                2015-04-15      | 2015-04-17 |             | Scheduled  | Scheduled |        3549.67 |       1560.68 |    40.64 |   1520.04 |       2029.63
                2015-05-15      | 2015-05-17 |             | Scheduled  | Scheduled |        2029.63 |       1560.68 |    23.24 |   1537.44 |        492.19
                2015-06-15      | 2015-06-17 |             | Scheduled  | Scheduled |         492.19 |        497.83 |     5.64 |    492.19 |          0.00";

            TenantTime.Today = Date(2015, 1, 1);

            Configuration.PayoffPeriod  = 7;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = Terms.LoanAmount, Start = Terms.OriginationDate },
                new InterestAccrualPeriod { Principal = 8000.91, Start = Date(2015, 01, 02), End = Date(2015, 01, 12) }
            })
            .Returns(88.94);

            var payoffAmount = Service.GetPayoffAmount(LoanReferenceNumber, Date(2015, 01, 05));

            Assert.NotNull(payoffAmount);
            Assert.Equal(8089.85, payoffAmount.Amount);
            Assert.Equal(Date(2015, 01, 12), payoffAmount.ValidUntil);
        }

        [Fact]
        public void AfterTwoExtraPaymentsBeforeFirstInstallment()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Terms.LoanAmount = 9000.0;
            Terms.Term = 6;
            Terms.Rate = 13.74;

            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                                | 2014-12-26 | 2014-12-26  | Additional | Scheduled |        9000.00 |        101.01 |     0.00 |    101.01 |       8898.99
                                | 2015-01-03 | 2015-01-03  | Additional | Scheduled |        8898.99 |        202.02 |     0.00 |    202.02 |       8696.97
                2015-01-15      | 2015-01-17 |             | Scheduled  | Scheduled |        8696.97 |       1560.68 |   101.39 |   1459.29 |       7237.68
                2015-02-15      | 2015-02-17 |             | Scheduled  | Scheduled |        7237.68 |       1560.68 |    82.87 |   1477.81 |       5759.87
                2015-03-15      | 2015-03-17 |             | Scheduled  | Scheduled |        5759.87 |       1560.68 |    65.95 |   1494.73 |       4265.14
                2015-04-15      | 2015-04-17 |             | Scheduled  | Scheduled |        4265.14 |       1560.68 |    48.84 |   1511.84 |       2753.30
                2015-05-15      | 2015-05-17 |             | Scheduled  | Scheduled |        2753.30 |       1560.68 |    31.53 |   1529.15 |       1224.15
                2015-06-15      | 2015-06-17 |             | Scheduled  | Scheduled |        1224.15 |       1238.17 |    14.02 |   1224.15 |          0.00";

            TenantTime.Today = Date(2014, 12, 20);

            Configuration.PayoffPeriod = 7;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = Terms.LoanAmount, Start = Terms.OriginationDate },
                new InterestAccrualPeriod { Principal = 8898.99, Start = Date(2014, 12, 26) },
                new InterestAccrualPeriod { Principal = 8696.97, Start = Date(2015, 01, 03), End = Date(2015, 01, 13) }
            })
            .Returns(94.76);

            var payoffAmount = Service.GetPayoffAmount(LoanReferenceNumber, Date(2015, 01, 06));

            Assert.NotNull(payoffAmount);
            Assert.Equal(8791.73, payoffAmount.Amount);
            Assert.Equal(Date(2015, 01, 13), payoffAmount.ValidUntil);
        }

        [Fact]
        public void AfterEarlyPaymentDueDate()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Terms.LoanAmount = 9000.0;
            Terms.Term = 6;
            Terms.Rate = 13.74;

            Configuration.PayoffPeriod = 7;

            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-10  | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00";

            TenantTime.Today = Date(2015, 2, 10);

            var earlyPaymentDate = Date(2015, 2, 10);
            var payoffDate = Date(2015, 2, 18);
            var validUntil = Date(2015, 2, 25);

            var dailyRate = Terms.Rate / 100 / 360;
            var principalBalance = 6068.05;
            var daysOfInterestAccrual = (validUntil - earlyPaymentDate).Days;
            var interest = Math.Round(principalBalance * dailyRate * daysOfInterestAccrual, 2, MidpointRounding.AwayFromZero);
            var expectedPayoffAmount = Math.Round(principalBalance + interest, 2, MidpointRounding.AwayFromZero);

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = principalBalance, Start = earlyPaymentDate, End = validUntil }
            })
            .Returns(interest);

            var payoffAmount = Service.GetPayoffAmount(LoanReferenceNumber, payoffDate);

            Assert.NotNull(payoffAmount);
            Assert.Equal(validUntil, payoffAmount.ValidUntil);
            Assert.Equal(expectedPayoffAmount, payoffAmount.Amount);
        }

        [Fact]
        public void AfterEarlyPaymentAnniversaryDate()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Terms.LoanAmount = 9000.0;
            Terms.Term = 6;
            Terms.Rate = 13.74;

            Configuration.PayoffPeriod = 7;

            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-10  | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00";

            TenantTime.Today = Date(2015, 2, 10);

            var earlyPaymentDate = Date(2015, 2, 10);
            var payoffDate = Date(2015, 2, 16);
            var validUntil = Date(2015, 2, 23);

            var dailyRate = Terms.Rate / 100 / 360;
            var principalBalance = 6068.05;
            var daysOfInterestAccrual = (validUntil - earlyPaymentDate).Days;
            var interest = Math.Round(principalBalance * dailyRate * daysOfInterestAccrual, 2, MidpointRounding.AwayFromZero);
            var expectedPayoffAmount = Math.Round(principalBalance + interest, 2, MidpointRounding.AwayFromZero);

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = principalBalance, Start = earlyPaymentDate, End = validUntil }
            })
            .Returns(interest);

            var payoffAmount = Service.GetPayoffAmount(LoanReferenceNumber, payoffDate);

            Assert.NotNull(payoffAmount);
            Assert.Equal(validUntil, payoffAmount.ValidUntil);
            Assert.Equal(expectedPayoffAmount, payoffAmount.Amount);
        }

        [Fact]
        public void AfterEarlyPaymentActualDate()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Terms.LoanAmount = 9000.0;
            Terms.Term = 6;
            Terms.Rate = 13.74;

            Configuration.PayoffPeriod = 7;

            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-10  | Scheduled | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00";

            TenantTime.Today = Date(2015, 2, 10);

            var earlyPaymentDate = Date(2015, 2, 10);
            var payoffDate = Date(2015, 2, 14);
            var validUntil = Date(2015, 2, 21);

            var dailyRate = Terms.Rate / 100 / 360;
            var principalBalance = 6068.05;
            var daysOfInterestAccrual = (validUntil - earlyPaymentDate).Days;
            var interest = Math.Round(principalBalance * dailyRate * daysOfInterestAccrual, 2, MidpointRounding.AwayFromZero);
            var expectedPayoffAmount = Math.Round(principalBalance + interest, 2, MidpointRounding.AwayFromZero);

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = principalBalance, Start = earlyPaymentDate, End = validUntil }
            })
            .Returns(interest);

            var payoffAmount = Service.GetPayoffAmount(LoanReferenceNumber, payoffDate);

            Assert.NotNull(payoffAmount);
            Assert.Equal(validUntil, payoffAmount.ValidUntil);
            Assert.Equal(expectedPayoffAmount, payoffAmount.Amount);
        }

        [Fact]
        public void AfterEarlyPaymentFollowedByExtraPayment()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Terms.LoanAmount = 9000.0;
            Terms.Term = 6;
            Terms.Rate = 13.74;

            Configuration.PayoffPeriod = 7;

            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled  | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 | 2015-02-10  | Scheduled  | Completed |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                                | 2015-02-18 | 2015-02-18  | Additional | Scheduled |        6068.05 |        999.09 |     0.00 |    999.09 |       5068.96
                2015-03-15      | 2015-03-17 |             | Scheduled  | Scheduled |        5068.96 |       1560.68 |    70.76 |   1489.92 |       3579.04
                2015-04-15      | 2015-04-17 |             | Scheduled  | Scheduled |        3579.04 |       1560.68 |    40.98 |   1519.70 |       2059.34
                2015-05-15      | 2015-05-17 |             | Scheduled  | Scheduled |        2059.34 |       1560.68 |    23.58 |   1537.10 |        522.24
                2015-06-15      | 2015-06-17 |             | Scheduled  | Scheduled |         522.24 |        528.22 |     5.98 |    522.24 |          0.00";

            TenantTime.Today = Date(2015, 2, 10);

            var earlyPaymentDate = Date(2015, 2, 10);
            var extraPaymentDate = Date(2015, 2, 18);
            var payoffDate = Date(2015, 2, 21);
            var validUntil = Date(2015, 2, 28);

            var balanceAfterEarlyPayment = 6068.05;
            var balanceAfterExtraPayment = 5068.96;
            var interest = 37.88;
            var expectedPayoffAmount = balanceAfterExtraPayment + interest;

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = balanceAfterEarlyPayment, Start = earlyPaymentDate },
                new InterestAccrualPeriod { Principal = balanceAfterExtraPayment, Start = extraPaymentDate, End = validUntil }
            })
            .Returns(interest);

            var payoffAmount = Service.GetPayoffAmount(LoanReferenceNumber, payoffDate);

            Assert.NotNull(payoffAmount);
            Assert.Equal(validUntil, payoffAmount.ValidUntil);
            Assert.Equal(expectedPayoffAmount, payoffAmount.Amount);
        }

        [Fact]
        public void PastDueInstallmentsShouldBeConsideredAsUnpaid()
        {
            Terms.OriginationDate = Date(2014, 12, 15);
            Terms.LoanAmount = 9000.0;
            Terms.Term = 6;
            Terms.Rate = 13.74;

            Configuration.PayoffPeriod = 7;

            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled | Completed |        9000.00 |       1560.68 |   103.05 |   1457.63 |       7542.37
                2015-02-15      | 2015-02-17 |             | Scheduled | Scheduled |        7542.37 |       1560.68 |    86.36 |   1474.32 |       6068.05
                2015-03-15      | 2015-03-17 |             | Scheduled | Scheduled |        6068.05 |       1560.68 |    69.48 |   1491.20 |       4576.85
                2015-04-15      | 2015-04-17 |             | Scheduled | Scheduled |        4576.85 |       1560.68 |    52.40 |   1508.28 |       3068.57
                2015-05-15      | 2015-05-17 |             | Scheduled | Scheduled |        3068.57 |       1560.68 |    35.14 |   1525.54 |       1543.03
                2015-06-15      | 2015-06-17 |             | Scheduled | Scheduled |        1543.03 |       1560.70 |    17.67 |   1543.03 |          0.00";

            TenantTime.Today = Date(2015, 2, 20);

            var payoffDate = Date(2015, 2, 21);
            var validUntil = Date(2015, 2, 28);

            AmortizationService.OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 7542.37, Start = Date(2015, 1, 15), End = validUntil }
            })
            .Returns(123.78);

            var payoffAmount = Service.GetPayoffAmount(LoanReferenceNumber, payoffDate);

            Assert.NotNull(payoffAmount);
            Assert.Equal(validUntil, payoffAmount.ValidUntil);
            Assert.Equal(7666.15, payoffAmount.Amount);
        }

        [Fact]
        public void CannotCalculatePayoffBeforeOrOnSameDateOfExistingPayment()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | PaymentDate | Status
                2015-01-15      | 2015-01-16 | 2015-01-19  | Completed
                2015-02-15      | 2015-02-16 | 2015-02-18  | Completed
                2015-03-15      | 2015-03-16 |             | Scheduled";

            AssertException.Throws<InvalidArgumentException>("Payoff date must be after the last payment date", () =>
                Service.GetPayoffAmount(LoanReferenceNumber, Date(2015, 02, 17))
            );

            AssertException.Throws<InvalidArgumentException>("Payoff date must be after the last payment date", () =>
                Service.GetPayoffAmount(LoanReferenceNumber, Date(2015, 02, 18))
            );
        }

        [Fact]
        public void CannotCalculatePayoffAfterEndingBalanceIsZero()
        {
            Schedule = @"
                AnniversaryDate | DueDate    | EndingBalance | Type      | Status
                2015-01-15      | 2015-01-16 |        200.00 | Scheduled | Scheduled
                2015-02-15      | 2015-02-16 |        100.00 | Scheduled | Scheduled
                2015-03-15      | 2015-03-16 |          0.00 | Scheduled | Scheduled";

            AssertException.Throws<InvalidArgumentException>("Payoff must occur before last installment", () =>
                Service.GetPayoffAmount(LoanReferenceNumber, Date(2015, 03, 17))
            );

            AssertException.Throws<InvalidArgumentException>("Payoff must occur before last installment", () =>
                Service.GetPayoffAmount(LoanReferenceNumber, Date(2015, 03, 16))
            );
        }

        [Fact]
        public void CannotCalculatePayoffBeforeOriginationDate()
        {
            Terms.OriginationDate = Date(2014, 12, 15);

            Schedule = @"
                AnniversaryDate | DueDate    | Type      | Status
                2015-01-15      | 2015-01-16 | Scheduled | Scheduled
                2015-02-15      | 2015-02-16 | Scheduled | Scheduled
                2015-03-15      | 2015-03-16 | Scheduled | Scheduled";

            AssertException.Throws<InvalidArgumentException>("Payoff date must be after origination date", () =>
                Service.GetPayoffAmount(LoanReferenceNumber, Terms.OriginationDate)
            );

            AssertException.Throws<InvalidArgumentException>("Payoff date must be after origination date", () =>
                Service.GetPayoffAmount(LoanReferenceNumber, Terms.OriginationDate.AddDays(-1))
            );
        }
    }
}
