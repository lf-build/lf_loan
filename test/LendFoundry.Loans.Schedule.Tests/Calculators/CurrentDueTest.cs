﻿using LendFoundry.Amortization;
using LendFoundry.Loans.Schedule.Calculators;
using LendFoundry.Loans.Schedule.Tests.Mocks;
using Moq;
using System;
using Xunit;

namespace LendFoundry.Loans.Schedule.Tests.Calculators
{
    public class CurrentDueTest : BaseScheduleTest
    {
        private ICurrentDueCalculator Calculator { get; }
        private ILoanTerms LoanTerms { get; }
        public AmortizationServiceMock AmortizationService { get; }

        public CurrentDueTest()
        {
            LoanTerms = new LoanTerms
            {
                OriginationDate = Date(2014, 12, 15),
                LoanAmount = 10000.0,
                Term = 7,
                Rate = 11.111
            };

            AmortizationService = new AmortizationServiceMock(LoanTerms);

            AmortizationService
                .OnAmortize(new AmortizationRequest())
                .Returns(@"
                    AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                    2015-01-15      | 2015-01-17 |       10000.00 | 1481.97 |    92.59 |   1389.38 |       8610.62
                    2015-02-15      | 2015-02-17 |        8610.62 | 1481.97 |    79.73 |   1402.24 |       7208.38
                    2015-03-15      | 2015-03-17 |        7208.38 | 1481.97 |    66.74 |   1415.23 |       5793.15
                    2015-04-15      | 2015-04-17 |        5793.15 | 1481.97 |    53.64 |   1428.33 |       4364.82
                    2015-05-15      | 2015-05-17 |        4364.82 | 1481.97 |    40.41 |   1441.56 |       2923.26
                    2015-06-15      | 2015-06-17 |        2923.26 | 1481.97 |    27.07 |   1454.90 |       1468.36
                    2015-07-15      | 2015-07-17 |        1468.36 | 1481.96 |    13.60 |   1468.36 |          0.00");

            Calculator = new CurrentDueCalculator(AmortizationService.Object);
        }

        private void AssertCurrentDue(double expectedAmount, DateTimeOffset date)
        {
            var actualAmount = Calculator.GetCurrentDue(date, Schedule, LoanTerms);
            Assert.Equal(Math.Round(expectedAmount, 2, MidpointRounding.AwayFromZero), actualAmount);
        }
        
        [Fact]
        public void NoPayments()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             | Scheduled | Scheduled |       10000.00 |       1481.97 |    92.59 |   1389.38 |       8610.62
                2015-02-15      | 2015-02-17 |             | Scheduled | Scheduled |        8610.62 |       1481.97 |    79.73 |   1402.24 |       7208.38
                2015-03-15      | 2015-03-17 |             | Scheduled | Scheduled |        7208.38 |       1481.97 |    66.74 |   1415.23 |       5793.15
                2015-04-15      | 2015-04-17 |             | Scheduled | Scheduled |        5793.15 |       1481.97 |    53.64 |   1428.33 |       4364.82
                2015-05-15      | 2015-05-17 |             | Scheduled | Scheduled |        4364.82 |       1481.97 |    40.41 |   1441.56 |       2923.26
                2015-06-15      | 2015-06-17 |             | Scheduled | Scheduled |        2923.26 |       1481.97 |    27.07 |   1454.90 |       1468.36
                2015-07-15      | 2015-07-17 |             | Scheduled | Scheduled |        1468.36 |       1481.96 |    13.60 |   1468.36 |          0.00");

            AssertCurrentDue(       0.00          , Date(2014, 12, 14));
            AssertCurrentDue(       0.00          , Date(2014, 12, 15));
            AssertCurrentDue(       0.00          , Date(2015,  1, 14));
            AssertCurrentDue(1481.97 * 1          , Date(2015,  1, 15));
            AssertCurrentDue(1481.97 * 1          , Date(2015,  2, 14));
            AssertCurrentDue(1481.97 * 2          , Date(2015,  2, 15));
            AssertCurrentDue(1481.97 * 2          , Date(2015,  3, 14));
            AssertCurrentDue(1481.97 * 3          , Date(2015,  3, 15));
            AssertCurrentDue(1481.97 * 3          , Date(2015,  4, 14));
            AssertCurrentDue(1481.97 * 4          , Date(2015,  4, 15));
            AssertCurrentDue(1481.97 * 4          , Date(2015,  5, 14));
            AssertCurrentDue(1481.97 * 5          , Date(2015,  5, 15));
            AssertCurrentDue(1481.97 * 5          , Date(2015,  6, 14));
            AssertCurrentDue(1481.97 * 6          , Date(2015,  6, 15));
            AssertCurrentDue(1481.97 * 6          , Date(2015,  7, 14));
            AssertCurrentDue(1481.97 * 6 + 1481.96, Date(2015,  7, 15));
        }

        [Fact]
        public void PerfectPay()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled | Completed |       10000.00 |       1481.97 |    92.59 |   1389.38 |       8610.62
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled | Completed |        8610.62 |       1481.97 |    79.73 |   1402.24 |       7208.38
                2015-03-15      | 2015-03-17 | 2015-03-17  | Scheduled | Completed |        7208.38 |       1481.97 |    66.74 |   1415.23 |       5793.15
                2015-04-15      | 2015-04-17 | 2015-04-17  | Scheduled | Completed |        5793.15 |       1481.97 |    53.64 |   1428.33 |       4364.82
                2015-05-15      | 2015-05-17 | 2015-05-17  | Scheduled | Completed |        4364.82 |       1481.97 |    40.41 |   1441.56 |       2923.26
                2015-06-15      | 2015-06-17 | 2015-06-17  | Scheduled | Completed |        2923.26 |       1481.97 |    27.07 |   1454.90 |       1468.36
                2015-07-15      | 2015-07-17 | 2015-07-17  | Scheduled | Completed |        1468.36 |       1481.96 |    13.60 |   1468.36 |          0.00");

            AssertCurrentDue(0.00, Date(2015, 1, 14));
            AssertCurrentDue(0.00, Date(2015, 1, 15));
            AssertCurrentDue(0.00, Date(2015, 2, 14));
            AssertCurrentDue(0.00, Date(2015, 2, 15));
            AssertCurrentDue(0.00, Date(2015, 3, 14));
            AssertCurrentDue(0.00, Date(2015, 3, 15));
            AssertCurrentDue(0.00, Date(2015, 4, 14));
            AssertCurrentDue(0.00, Date(2015, 4, 15));
            AssertCurrentDue(0.00, Date(2015, 5, 14));
            AssertCurrentDue(0.00, Date(2015, 5, 15));
            AssertCurrentDue(0.00, Date(2015, 6, 14));
            AssertCurrentDue(0.00, Date(2015, 6, 15));
            AssertCurrentDue(0.00, Date(2015, 7, 14));
            AssertCurrentDue(0.00, Date(2015, 7, 15));
            AssertCurrentDue(0.00, Date(2015, 8, 14));
            AssertCurrentDue(0.00, Date(2015, 8, 15));
        }

        [Fact]
        public void HalfPaidLoan()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled | Completed |       10000.00 |       1481.97 |    92.59 |   1389.38 |       8610.62
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled | Completed |        8610.62 |       1481.97 |    79.73 |   1402.24 |       7208.38
                2015-03-15      | 2015-03-17 | 2015-03-17  | Scheduled | Completed |        7208.38 |       1481.97 |    66.74 |   1415.23 |       5793.15
                2015-04-15      | 2015-04-17 |             | Scheduled | Scheduled |        5793.15 |       1481.97 |    53.64 |   1428.33 |       4364.82
                2015-05-15      | 2015-05-17 |             | Scheduled | Scheduled |        4364.82 |       1481.97 |    40.41 |   1441.56 |       2923.26
                2015-06-15      | 2015-06-17 |             | Scheduled | Scheduled |        2923.26 |       1481.97 |    27.07 |   1454.90 |       1468.36
                2015-07-15      | 2015-07-17 |             | Scheduled | Scheduled |        1468.36 |       1481.96 |    13.60 |   1468.36 |          0.00");

            AssertCurrentDue(   0.00, Date(2015, 1, 14));
            AssertCurrentDue(   0.00, Date(2015, 1, 15));
            AssertCurrentDue(   0.00, Date(2015, 2, 14));
            AssertCurrentDue(   0.00, Date(2015, 2, 15));
            AssertCurrentDue(   0.00, Date(2015, 3, 14));
            AssertCurrentDue(   0.00, Date(2015, 3, 15));
            AssertCurrentDue(   0.00, Date(2015, 4, 14));
            AssertCurrentDue(1481.97, Date(2015, 4, 15));
            AssertCurrentDue(1481.97, Date(2015, 5, 14));
            AssertCurrentDue(2963.94, Date(2015, 5, 15));
            AssertCurrentDue(2963.94, Date(2015, 6, 14));
            AssertCurrentDue(4445.91, Date(2015, 6, 15));
            AssertCurrentDue(4445.91, Date(2015, 7, 14));
            AssertCurrentDue(5927.87, Date(2015, 7, 15));
        }

        [Fact]
        public void MissedPayment()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled | Completed |       10000.00 |       1481.97 |    92.59 |   1389.38 |       8610.62
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled | Completed |        8610.62 |       1481.97 |    79.73 |   1402.24 |       7208.38
                2015-03-15      | 2015-03-17 |             | Scheduled | Completed |        7208.38 |          0.00 |     0.00 |      0.00 |       7208.38
                2015-04-15      | 2015-04-17 |             | Scheduled | Scheduled |        7208.38 |       1481.97 |   133.49 |   1348.48 |       5859.90
                2015-05-15      | 2015-05-17 |             | Scheduled | Scheduled |        5859.90 |       1481.97 |    54.26 |   1427.71 |       4432.19
                2015-06-15      | 2015-06-17 |             | Scheduled | Scheduled |        4432.19 |       1481.97 |    41.04 |   1440.93 |       2991.26
                2015-07-15      | 2015-07-17 |             | Scheduled | Scheduled |        2991.26 |       3018.96 |    27.70 |   2991.26 |          0.00");

            AssertCurrentDue(   0.00, Date(2015, 1, 14));
            AssertCurrentDue(   0.00, Date(2015, 1, 15));
            AssertCurrentDue(   0.00, Date(2015, 2, 14));
            AssertCurrentDue(   0.00, Date(2015, 2, 15));
            AssertCurrentDue(   0.00, Date(2015, 3, 14));
            AssertCurrentDue(1481.97, Date(2015, 3, 15));
            AssertCurrentDue(1481.97, Date(2015, 4, 14));
            AssertCurrentDue(2963.94, Date(2015, 4, 15));
            AssertCurrentDue(2963.94, Date(2015, 5, 14));
            AssertCurrentDue(4445.91, Date(2015, 5, 15));
            AssertCurrentDue(4445.91, Date(2015, 6, 14));
            AssertCurrentDue(5927.88, Date(2015, 6, 15));
            AssertCurrentDue(5927.88, Date(2015, 7, 14));
            AssertCurrentDue(7464.87, Date(2015, 7, 15));
        }

        [Fact]
        public void MissedPayments_WhereLastInstallmentIsOpen()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled | Completed |       10000.00 |       1481.97 |    92.59 |   1389.38 |       8610.62
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled | Completed |        8610.62 |       1481.97 |    79.73 |   1402.24 |       7208.38
                2015-03-15      | 2015-03-17 |             | Scheduled | Completed |        7208.38 |          0.00 |     0.00 |      0.00 |       7208.38
                2015-04-15      | 2015-04-17 |             | Scheduled | Completed |        7208.38 |          0.00 |     0.00 |      0.00 |       7208.38
                2015-05-15      | 2015-05-17 |             | Scheduled | Completed |        7208.38 |          0.00 |     0.00 |      0.00 |       7208.38
                2015-06-15      | 2015-06-17 |             | Scheduled | Completed |        7208.38 |          0.00 |     0.00 |      0.00 |       7208.38
                2015-07-15      | 2015-07-17 |             | Scheduled | Scheduled |        7208.38 |       7542.10 |   333.72 |   7208.38 |          0.00");

            AssertCurrentDue(   0.00, Date(2015, 1, 14));
            AssertCurrentDue(   0.00, Date(2015, 1, 15));
            AssertCurrentDue(   0.00, Date(2015, 2, 14));
            AssertCurrentDue(   0.00, Date(2015, 2, 15));
            AssertCurrentDue(   0.00, Date(2015, 3, 14));
            AssertCurrentDue(1481.97, Date(2015, 3, 15));
            AssertCurrentDue(1481.97, Date(2015, 4, 14));
            AssertCurrentDue(2963.94, Date(2015, 4, 15));
            AssertCurrentDue(2963.94, Date(2015, 5, 14));
            AssertCurrentDue(4445.91, Date(2015, 5, 15));
            AssertCurrentDue(4445.91, Date(2015, 6, 14));
            AssertCurrentDue(5927.88, Date(2015, 6, 15));
            AssertCurrentDue(5927.88, Date(2015, 7, 14));
            AssertCurrentDue(7542.10, Date(2015, 7, 15));
        }

        [Fact]
        public void LatePayment_WhereLoanIsPaidOff()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled | Completed |       10000.00 |       1481.97 |    92.59 |   1389.38 |       8610.62
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled | Completed |        8610.62 |       1481.97 |    79.73 |   1402.24 |       7208.38
                2015-03-15      | 2015-03-17 |             | Scheduled | Completed |        7208.38 |          0.00 |     0.00 |      0.00 |       7208.38
                                | 2015-03-30 | 2015-03-30  | Scheduled | Completed |        7208.38 |       1481.97 |    66.74 |   1415.23 |       5793.15
                2015-04-15      | 2015-04-17 | 2015-04-17  | Scheduled | Completed |        5793.15 |       1481.97 |    60.19 |   1421.78 |       4371.37
                2015-05-15      | 2015-05-17 | 2015-05-17  | Scheduled | Completed |        4371.37 |       1481.97 |    40.48 |   1441.49 |       2929.88
                2015-06-15      | 2015-06-17 | 2015-06-17  | Scheduled | Completed |        2929.88 |       1481.97 |    27.13 |   1454.84 |       1475.04
                2015-07-15      | 2015-07-17 | 2015-07-17  | Scheduled | Completed |        1475.04 |       1488.70 |    13.66 |   1475.04 |          0.00");

            AssertCurrentDue(   0.00, Date(2015, 1, 14));
            AssertCurrentDue(   0.00, Date(2015, 1, 15));
            AssertCurrentDue(   0.00, Date(2015, 2, 14));
            AssertCurrentDue(   0.00, Date(2015, 2, 15));
            AssertCurrentDue(   0.00, Date(2015, 3, 14));
            AssertCurrentDue(1481.97, Date(2015, 3, 15));
            AssertCurrentDue(1481.97, Date(2015, 3, 29));
            AssertCurrentDue(   0.00, Date(2015, 3, 30));
            AssertCurrentDue(   0.00, Date(2015, 4, 14));
            AssertCurrentDue(   0.00, Date(2015, 4, 15));
            AssertCurrentDue(   0.00, Date(2015, 5, 14));
            AssertCurrentDue(   0.00, Date(2015, 5, 15));
            AssertCurrentDue(   0.00, Date(2015, 6, 14));
            AssertCurrentDue(   0.00, Date(2015, 6, 15));
            AssertCurrentDue(   0.00, Date(2015, 7, 14));
            AssertCurrentDue(   0.00, Date(2015, 7, 15));
            AssertCurrentDue(   0.00, Date(2015, 8, 14));
            AssertCurrentDue(   0.00, Date(2015, 8, 15));
        }

        [Fact]
        public void LatePayment_WhereLastInstallmentIsOpen()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled | Completed |       10000.00 |       1481.97 |    92.59 |   1389.38 |       8610.62
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled | Completed |        8610.62 |       1481.97 |    79.73 |   1402.24 |       7208.38
                2015-03-15      | 2015-03-17 |             | Scheduled | Completed |        7208.38 |          0.00 |     0.00 |      0.00 |       7208.38
                                | 2015-03-30 | 2015-03-30  | Scheduled | Completed |        7208.38 |       1481.97 |    66.74 |   1415.23 |       5793.15
                2015-04-15      | 2015-04-17 | 2015-04-17  | Scheduled | Completed |        5793.15 |       1481.97 |    60.19 |   1421.78 |       4371.37
                2015-05-15      | 2015-05-17 | 2015-05-17  | Scheduled | Completed |        4371.37 |       1481.97 |    40.48 |   1441.49 |       2929.88
                2015-06-15      | 2015-06-17 | 2015-06-17  | Scheduled | Completed |        2929.88 |       1481.97 |    27.13 |   1454.84 |       1475.04
                2015-07-15      | 2015-07-17 | 2015-07-17  | Scheduled | Scheduled |        1475.04 |       1488.70 |    13.66 |   1475.04 |          0.00");

            AssertCurrentDue(   0.00, Date(2015, 1, 14));
            AssertCurrentDue(   0.00, Date(2015, 1, 15));
            AssertCurrentDue(   0.00, Date(2015, 2, 14));
            AssertCurrentDue(   0.00, Date(2015, 2, 15));
            AssertCurrentDue(   0.00, Date(2015, 3, 14));
            AssertCurrentDue(1481.97, Date(2015, 3, 15));
            AssertCurrentDue(1481.97, Date(2015, 3, 29));
            AssertCurrentDue(   0.00, Date(2015, 3, 30));
            AssertCurrentDue(   0.00, Date(2015, 4, 14));
            AssertCurrentDue(   0.00, Date(2015, 4, 15));
            AssertCurrentDue(   0.00, Date(2015, 5, 14));
            AssertCurrentDue(   0.00, Date(2015, 5, 15));
            AssertCurrentDue(   0.00, Date(2015, 6, 14));
            AssertCurrentDue(   0.00, Date(2015, 6, 15));
            AssertCurrentDue(   0.00, Date(2015, 7, 14));
            AssertCurrentDue(1488.70, Date(2015, 7, 15));
        }

        [Fact]
        public void LatePayment_WhereLastInstallmentIsOpenAndPrecedingInstallmentIsMissed()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled | Completed |       10000.00 |       1481.97 |    92.59 |   1389.38 |       8610.62
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled | Completed |        8610.62 |       1481.97 |    79.73 |   1402.24 |       7208.38
                2015-03-15      | 2015-03-17 |             | Scheduled | Completed |        7208.38 |          0.00 |     0.00 |      0.00 |       7208.38
                                | 2015-03-30 | 2015-03-30  | Scheduled | Completed |        7208.38 |       1481.97 |    66.74 |   1415.23 |       5793.15
                2015-04-15      | 2015-04-17 | 2015-04-17  | Scheduled | Completed |        5793.15 |       1481.97 |    60.19 |   1421.78 |       4371.37
                2015-05-15      | 2015-05-17 | 2015-05-17  | Scheduled | Completed |        4371.37 |       1481.97 |    40.48 |   1441.49 |       2929.88
                2015-06-15      | 2015-06-17 | 2015-06-17  | Scheduled | Completed |        2929.88 |          0.00 |     0.00 |      0.00 |       2929.88
                2015-07-15      | 2015-07-17 | 2015-07-17  | Scheduled | Scheduled |        2929.88 |       2984.14 |    54.26 |   2929.88 |          0.00");

            AssertCurrentDue(   0.00, Date(2015, 1, 14));
            AssertCurrentDue(   0.00, Date(2015, 1, 15));
            AssertCurrentDue(   0.00, Date(2015, 2, 14));
            AssertCurrentDue(   0.00, Date(2015, 2, 15));
            AssertCurrentDue(   0.00, Date(2015, 3, 14));
            AssertCurrentDue(1481.97, Date(2015, 3, 15));
            AssertCurrentDue(1481.97, Date(2015, 3, 29));
            AssertCurrentDue(   0.00, Date(2015, 3, 30));
            AssertCurrentDue(   0.00, Date(2015, 4, 14));
            AssertCurrentDue(   0.00, Date(2015, 4, 15));
            AssertCurrentDue(   0.00, Date(2015, 5, 14));
            AssertCurrentDue(   0.00, Date(2015, 5, 15));
            AssertCurrentDue(   0.00, Date(2015, 6, 14));
            AssertCurrentDue(1481.97, Date(2015, 6, 15));
            AssertCurrentDue(1481.97, Date(2015, 7, 14));
            AssertCurrentDue(2984.14, Date(2015, 7, 15));
        }

        [Fact]
        public void EarlyPayment()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled | Completed |       10000.00 |       1481.97 |    92.59 |   1389.38 |       8610.62
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled | Completed |        8610.62 |       1481.97 |    79.73 |   1402.24 |       7208.38
                2015-03-15      | 2015-03-17 | 2015-03-09  | Scheduled | Completed |        7208.38 |       1481.97 |    53.39 |   1428.58 |       5779.80
                2015-04-15      | 2015-04-17 | 2015-04-17  | Scheduled | Scheduled |        5779.80 |       1481.97 |    64.22 |   1417.75 |       4362.05
                2015-05-15      | 2015-05-17 | 2015-05-17  | Scheduled | Scheduled |        4362.05 |       1481.97 |    40.39 |   1441.58 |       2920.47
                2015-06-15      | 2015-06-17 | 2015-06-17  | Scheduled | Scheduled |        2920.47 |       1481.97 |    27.04 |   1454.93 |       1465.54
                2015-07-15      | 2015-07-17 | 2015-07-17  | Scheduled | Scheduled |        1465.54 |       1479.11 |    13.57 |   1465.54 |          0.00");

            AssertCurrentDue(   0.00, Date(2015, 1, 14));
            AssertCurrentDue(   0.00, Date(2015, 1, 15));
            AssertCurrentDue(   0.00, Date(2015, 2, 14));
            AssertCurrentDue(   0.00, Date(2015, 2, 15));
            AssertCurrentDue(   0.00, Date(2015, 3, 14));
            AssertCurrentDue(   0.00, Date(2015, 3, 15));
            AssertCurrentDue(   0.00, Date(2015, 4, 14));
            AssertCurrentDue(1481.97, Date(2015, 4, 15));
            AssertCurrentDue(1481.97, Date(2015, 5, 14));
            AssertCurrentDue(2963.94, Date(2015, 5, 15));
            AssertCurrentDue(2963.94, Date(2015, 6, 14));
            AssertCurrentDue(4445.91, Date(2015, 6, 15));
            AssertCurrentDue(4445.91, Date(2015, 7, 14));
            AssertCurrentDue(5925.02, Date(2015, 7, 15));
        }

        [Fact]
        public void EarlyPayment_WhereLoanIsPaidOff()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled | Completed |       10000.00 |       1481.97 |    92.59 |   1389.38 |       8610.62
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled | Completed |        8610.62 |       1481.97 |    79.73 |   1402.24 |       7208.38
                2015-03-15      | 2015-03-17 | 2015-03-09  | Scheduled | Completed |        7208.38 |       1481.97 |    53.39 |   1428.58 |       5779.80
                2015-04-15      | 2015-04-17 | 2015-04-17  | Scheduled | Completed |        5779.80 |       1481.97 |    64.22 |   1417.75 |       4362.05
                2015-05-15      | 2015-05-17 | 2015-05-17  | Scheduled | Completed |        4362.05 |       1481.97 |    40.39 |   1441.58 |       2920.47
                2015-06-15      | 2015-06-17 | 2015-06-17  | Scheduled | Completed |        2920.47 |       1481.97 |    27.04 |   1454.93 |       1465.54
                2015-07-15      | 2015-07-17 | 2015-07-17  | Scheduled | Completed |        1465.54 |       1479.11 |    13.57 |   1465.54 |          0.00");

            AssertCurrentDue(0.00, Date(2015, 1, 14));
            AssertCurrentDue(0.00, Date(2015, 1, 15));
            AssertCurrentDue(0.00, Date(2015, 2, 14));
            AssertCurrentDue(0.00, Date(2015, 2, 15));
            AssertCurrentDue(0.00, Date(2015, 3, 14));
            AssertCurrentDue(0.00, Date(2015, 3, 15));
            AssertCurrentDue(0.00, Date(2015, 4, 14));
            AssertCurrentDue(0.00, Date(2015, 4, 15));
            AssertCurrentDue(0.00, Date(2015, 5, 14));
            AssertCurrentDue(0.00, Date(2015, 5, 15));
            AssertCurrentDue(0.00, Date(2015, 6, 14));
            AssertCurrentDue(0.00, Date(2015, 6, 15));
            AssertCurrentDue(0.00, Date(2015, 7, 14));
            AssertCurrentDue(0.00, Date(2015, 7, 15));
            AssertCurrentDue(0.00, Date(2015, 8, 14));
            AssertCurrentDue(0.00, Date(2015, 8, 15));
        }

        [Fact]
        public void Underpayment()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled | Completed |       10000.00 |       1481.97 |    92.59 |   1389.38 |       8610.62
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled | Completed |        8610.62 |       1481.97 |    79.73 |   1402.24 |       7208.38
                2015-03-15      | 2015-03-17 | 2015-03-17  | Scheduled | Completed |        7208.38 |        999.09 |    66.74 |    932.35 |       6276.03
                2015-04-15      | 2015-04-17 |             | Scheduled | Scheduled |        6276.03 |       1481.97 |    58.11 |   1423.86 |       4852.17
                2015-05-15      | 2015-05-17 |             | Scheduled | Scheduled |        4852.17 |       1481.97 |    44.93 |   1437.04 |       3415.13
                2015-06-15      | 2015-06-17 |             | Scheduled | Scheduled |        3415.13 |       1481.97 |    31.62 |   1450.35 |       1964.78
                2015-07-15      | 2015-07-17 |             | Scheduled | Scheduled |        1964.78 |       1982.97 |    18.19 |   1964.78 |          0.00");

            AssertCurrentDue(   0.00, Date(2015, 1, 14));
            AssertCurrentDue(   0.00, Date(2015, 1, 15));
            AssertCurrentDue(   0.00, Date(2015, 2, 14));
            AssertCurrentDue(   0.00, Date(2015, 2, 15));
            AssertCurrentDue(   0.00, Date(2015, 3, 14));
            AssertCurrentDue( 482.88, Date(2015, 3, 15));
            AssertCurrentDue( 482.88, Date(2015, 4, 14));
            AssertCurrentDue(1964.85, Date(2015, 4, 15));
            AssertCurrentDue(1964.85, Date(2015, 5, 14));
            AssertCurrentDue(3446.82, Date(2015, 5, 15));
            AssertCurrentDue(3446.82, Date(2015, 6, 14));
            AssertCurrentDue(4928.79, Date(2015, 6, 15));
            AssertCurrentDue(4928.79, Date(2015, 7, 14));
            AssertCurrentDue(6428.88, Date(2015, 7, 15));
        }

        [Fact]
        public void Underpayments_WhereLoanIsPaidOff()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled | Completed |       10000.00 |       1481.97 |    92.59 |   1389.38 |       8610.62
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled | Completed |        8610.62 |       1300.00 |    79.73 |   1220.27 |       7390.35
                2015-03-15      | 2015-03-17 | 2015-03-17  | Scheduled | Completed |        7390.35 |       1481.97 |    68.43 |   1413.54 |       5976.81
                2015-04-15      | 2015-04-17 | 2015-04-17  | Scheduled | Completed |        5976.81 |       1250.00 |    55.34 |   1194.66 |       4782.15
                2015-05-15      | 2015-05-17 | 2015-05-17  | Scheduled | Completed |        4782.15 |       1481.97 |    44.28 |   1437.69 |       3344.46
                2015-06-15      | 2015-06-17 | 2015-06-17  | Scheduled | Completed |        3344.46 |       1481.97 |    30.97 |   1451.00 |       1893.46
                2015-07-15      | 2015-07-17 | 2015-07-17  | Scheduled | Completed |        1893.46 |       1910.99 |    17.53 |   1893.46 |          0.00");

            AssertCurrentDue(  0.00, Date(2015, 1, 14));
            AssertCurrentDue(  0.00, Date(2015, 1, 15));
            AssertCurrentDue(  0.00, Date(2015, 2, 14));
            AssertCurrentDue(181.97, Date(2015, 2, 15));
            AssertCurrentDue(181.97, Date(2015, 3, 14));
            AssertCurrentDue(181.97, Date(2015, 3, 15));
            AssertCurrentDue(181.97, Date(2015, 4, 14));
            AssertCurrentDue(413.94, Date(2015, 4, 15));
            AssertCurrentDue(413.94, Date(2015, 5, 14));
            AssertCurrentDue(413.94, Date(2015, 5, 15));
            AssertCurrentDue(413.94, Date(2015, 6, 14));
            AssertCurrentDue(413.94, Date(2015, 6, 15));
            AssertCurrentDue(413.94, Date(2015, 7, 14));
            AssertCurrentDue(  0.00, Date(2015, 7, 15));
            AssertCurrentDue(  0.00, Date(2015, 8, 14));
            AssertCurrentDue(  0.00, Date(2015, 8, 15));
        }

        [Fact]
        public void UnderpaymentAndCatchUp()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled | Completed |       10000.00 |       1481.97 |    92.59 |   1389.38 |       8610.62
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled | Completed |        8610.62 |        999.09 |    79.73 |    919.36 |       7691.26
                2015-03-15      | 2015-03-17 | 2015-03-17  | Scheduled | Completed |        7691.26 |       1481.97 |    71.21 |   1410.76 |       6280.50
                2015-04-15      | 2015-04-17 | 2015-04-17  | Scheduled | Completed |        6280.50 |       1964.85 |    58.15 |   1906.70 |       4373.80
                2015-05-15      | 2015-05-17 | 2015-05-17  | Scheduled | Completed |        4373.80 |       1481.97 |    40.50 |   1441.47 |       2932.33
                2015-06-15      | 2015-06-17 | 2015-06-17  | Scheduled | Completed |        2932.33 |       1481.97 |    27.15 |   1454.82 |       1477.51
                2015-07-15      | 2015-07-17 | 2015-07-17  | Scheduled | Completed |        1477.51 |       1491.19 |    13.68 |   1477.51 |          0.00");

            AssertCurrentDue(  0.00, Date(2015, 1, 14));
            AssertCurrentDue(  0.00, Date(2015, 1, 15));
            AssertCurrentDue(  0.00, Date(2015, 2, 14));
            AssertCurrentDue(482.88, Date(2015, 2, 15));
            AssertCurrentDue(482.88, Date(2015, 3, 14));
            AssertCurrentDue(482.88, Date(2015, 3, 15));
            AssertCurrentDue(482.88, Date(2015, 4, 14));
            AssertCurrentDue(  0.00, Date(2015, 4, 15));
            AssertCurrentDue(  0.00, Date(2015, 5, 14));
            AssertCurrentDue(  0.00, Date(2015, 5, 15));
            AssertCurrentDue(  0.00, Date(2015, 6, 14));
            AssertCurrentDue(  0.00, Date(2015, 6, 15));
            AssertCurrentDue(  0.00, Date(2015, 7, 14));
            AssertCurrentDue(  0.00, Date(2015, 7, 15));
            AssertCurrentDue(  0.00, Date(2015, 8, 14));
            AssertCurrentDue(  0.00, Date(2015, 8, 15));
        }

        [Fact]
        public void ExtraPayment()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled  | Completed |       10000.00 |       1481.97 |    92.59 |   1389.38 |       8610.62
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled  | Completed |        8610.62 |       1481.97 |    79.73 |   1402.24 |       7208.38
                2015-03-15      | 2015-03-17 | 2015-03-17  | Scheduled  | Completed |        7208.38 |       1481.97 |    66.74 |   1415.23 |       5793.15
                                | 2015-03-30 | 2015-03-30  | Additional | Completed |        5793.15 |        234.56 |     0.00 |    234.56 |       5558.59
                2015-04-15      | 2015-04-17 |             | Scheduled  | Scheduled |        5558.59 |       1481.97 |    52.55 |   1429.42 |       4129.17
                2015-05-15      | 2015-05-17 |             | Scheduled  | Scheduled |        4129.17 |       1481.97 |    38.23 |   1443.74 |       2685.43
                2015-06-15      | 2015-06-17 |             | Scheduled  | Scheduled |        2685.43 |       1481.97 |    24.86 |   1457.11 |       1228.32
                2015-07-15      | 2015-07-17 |             | Scheduled  | Scheduled |        1228.32 |       1239.69 |    11.37 |   1228.32 |          0.00");

            AssertCurrentDue(   0.00, Date(2015, 1, 14));
            AssertCurrentDue(   0.00, Date(2015, 1, 15));
            AssertCurrentDue(   0.00, Date(2015, 2, 14));
            AssertCurrentDue(   0.00, Date(2015, 2, 15));
            AssertCurrentDue(   0.00, Date(2015, 3, 14));
            AssertCurrentDue(   0.00, Date(2015, 3, 15));
            AssertCurrentDue(   0.00, Date(2015, 3, 29));
            AssertCurrentDue(   0.00, Date(2015, 3, 30));
            AssertCurrentDue(   0.00, Date(2015, 4, 14));
            AssertCurrentDue(1481.97, Date(2015, 4, 15));
            AssertCurrentDue(1481.97, Date(2015, 5, 14));
            AssertCurrentDue(2963.94, Date(2015, 5, 15));
            AssertCurrentDue(2963.94, Date(2015, 6, 14));
            AssertCurrentDue(4445.91, Date(2015, 6, 15));
            AssertCurrentDue(4445.91, Date(2015, 7, 14));
            AssertCurrentDue(5685.60, Date(2015, 7, 15));
        }

        [Fact]
        public void ExtraPayment_WhereLoanIsPaidOff()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled  | Completed |       10000.00 |       1481.97 |    92.59 |   1389.38 |       8610.62
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled  | Completed |        8610.62 |       1481.97 |    79.73 |   1402.24 |       7208.38
                2015-03-15      | 2015-03-17 | 2015-03-17  | Scheduled  | Completed |        7208.38 |       1481.97 |    66.74 |   1415.23 |       5793.15
                                | 2015-03-30 | 2015-03-30  | Additional | Completed |        5793.15 |        234.56 |     0.00 |    234.56 |       5558.59
                2015-04-15      | 2015-04-17 | 2015-04-17  | Scheduled  | Completed |        5558.59 |       1481.97 |    52.55 |   1429.42 |       4129.17
                2015-05-15      | 2015-05-17 | 2015-05-17  | Scheduled  | Completed |        4129.17 |       1481.97 |    38.23 |   1443.74 |       2685.43
                2015-06-15      | 2015-06-17 | 2015-06-17  | Scheduled  | Completed |        2685.43 |       1481.97 |    24.86 |   1457.11 |       1228.32
                2015-07-15      | 2015-07-17 | 2015-07-17  | Scheduled  | Completed |        1228.32 |       1239.69 |    11.37 |   1228.32 |          0.00");

            AssertCurrentDue(0.00, Date(2015, 1, 14));
            AssertCurrentDue(0.00, Date(2015, 1, 15));
            AssertCurrentDue(0.00, Date(2015, 2, 14));
            AssertCurrentDue(0.00, Date(2015, 2, 15));
            AssertCurrentDue(0.00, Date(2015, 3, 14));
            AssertCurrentDue(0.00, Date(2015, 3, 15));
            AssertCurrentDue(0.00, Date(2015, 3, 29));
            AssertCurrentDue(0.00, Date(2015, 3, 30));
            AssertCurrentDue(0.00, Date(2015, 4, 14));
            AssertCurrentDue(0.00, Date(2015, 4, 15));
            AssertCurrentDue(0.00, Date(2015, 5, 14));
            AssertCurrentDue(0.00, Date(2015, 5, 15));
            AssertCurrentDue(0.00, Date(2015, 6, 14));
            AssertCurrentDue(0.00, Date(2015, 6, 15));
            AssertCurrentDue(0.00, Date(2015, 7, 14));
            AssertCurrentDue(0.00, Date(2015, 7, 15));
            AssertCurrentDue(0.00, Date(2015, 8, 14));
            AssertCurrentDue(0.00, Date(2015, 8, 15));
        }

        [Fact]
        public void ExtraPayment_WhereLastInstallmentIsOpen()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled  | Completed |       10000.00 |       1481.97 |    92.59 |   1389.38 |       8610.62
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled  | Completed |        8610.62 |       1481.97 |    79.73 |   1402.24 |       7208.38
                2015-03-15      | 2015-03-17 | 2015-03-17  | Scheduled  | Completed |        7208.38 |       1481.97 |    66.74 |   1415.23 |       5793.15
                                | 2015-03-30 | 2015-03-30  | Additional | Completed |        5793.15 |        234.56 |     0.00 |    234.56 |       5558.59
                2015-04-15      | 2015-04-17 | 2015-04-17  | Scheduled  | Completed |        5558.59 |       1481.97 |    52.55 |   1429.42 |       4129.17
                2015-05-15      | 2015-05-17 | 2015-05-17  | Scheduled  | Completed |        4129.17 |       1481.97 |    38.23 |   1443.74 |       2685.43
                2015-06-15      | 2015-06-17 | 2015-06-17  | Scheduled  | Completed |        2685.43 |       1481.97 |    24.86 |   1457.11 |       1228.32
                2015-07-15      | 2015-07-17 |             | Scheduled  | Scheduled |        1228.32 |       1239.69 |    11.37 |   1228.32 |          0.00");

            AssertCurrentDue(   0.00, Date(2015, 1, 14));
            AssertCurrentDue(   0.00, Date(2015, 1, 15));
            AssertCurrentDue(   0.00, Date(2015, 2, 14));
            AssertCurrentDue(   0.00, Date(2015, 2, 15));
            AssertCurrentDue(   0.00, Date(2015, 3, 14));
            AssertCurrentDue(   0.00, Date(2015, 3, 15));
            AssertCurrentDue(   0.00, Date(2015, 3, 29));
            AssertCurrentDue(   0.00, Date(2015, 3, 30));
            AssertCurrentDue(   0.00, Date(2015, 4, 14));
            AssertCurrentDue(   0.00, Date(2015, 4, 15));
            AssertCurrentDue(   0.00, Date(2015, 5, 14));
            AssertCurrentDue(   0.00, Date(2015, 5, 15));
            AssertCurrentDue(   0.00, Date(2015, 6, 14));
            AssertCurrentDue(   0.00, Date(2015, 6, 15));
            AssertCurrentDue(   0.00, Date(2015, 7, 14));
            AssertCurrentDue(1239.69, Date(2015, 7, 15));
        }

        [Fact]
        public void BigExtraPayment()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled  | Completed |       10000.00 |       1481.97 |    92.59 |   1389.38 |       8610.62
                                | 2015-01-25 | 2015-01-25  | Additional | Completed |        8610.62 |       6500.00 |     0.00 |   6500.00 |       2110.62
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled  | Completed |        2110.62 |       1481.97 |    19.54 |   1462.43 |        648.19
                2015-03-15      | 2015-03-17 |             | Scheduled  | Scheduled |         648.19 |        654.19 |     6.00 |    648.19 |          0.00");

            AssertCurrentDue(   0.00, Date(2015, 1, 14));
            AssertCurrentDue(   0.00, Date(2015, 1, 15));
            AssertCurrentDue(   0.00, Date(2015, 2, 14));
            AssertCurrentDue(   0.00, Date(2015, 2, 15));
            AssertCurrentDue(   0.00, Date(2015, 3, 14));
            AssertCurrentDue( 654.19, Date(2015, 3, 15));
            AssertCurrentDue( 654.19, Date(2015, 4, 15));
            AssertCurrentDue( 654.19, Date(2015, 5, 15));
        }

        [Fact]
        public void LateUnderAndExtraPayments()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled  | Completed |       10000.00 |          0.00 |     0.00 |      0.00 |      10000.00
                                | 2015-01-29 | 2015-01-29  | Scheduled  | Completed |       10000.00 |       1481.97 |    92.59 |   1389.38 |       8610.62
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled  | Completed |        8610.62 |       1481.97 |    85.73 |   1396.24 |       7214.38
                2015-03-15      | 2015-03-17 | 2015-03-17  | Scheduled  | Completed |        7214.38 |        999.09 |    66.80 |    932.29 |       6282.09
                2015-04-15      | 2015-04-17 | 2015-04-17  | Scheduled  | Completed |        6282.09 |       1481.97 |    58.17 |   1423.80 |       4858.29
                                | 2015-04-30 | 2015-04-30  | Additional | Completed |        4858.29 |        303.03 |     0.00 |    303.03 |       4555.26
                2015-05-15      | 2015-05-17 |             | Scheduled  | Scheduled |        4555.26 |       1481.97 |    43.58 |   1438.39 |       3116.87
                2015-06-15      | 2015-06-17 |             | Scheduled  | Scheduled |        3116.87 |       1481.97 |    28.86 |   1453.11 |       1663.76
                2015-07-15      | 2015-07-17 |             | Scheduled  | Scheduled |        1663.76 |       1679.17 |    15.41 |   1663.76 |          0.00");
            
            AssertCurrentDue(   0.00, Date(2015, 1, 14));
            AssertCurrentDue(1481.97, Date(2015, 1, 15));
            AssertCurrentDue(1481.97, Date(2015, 1, 28));
            AssertCurrentDue(   0.00, Date(2015, 1, 29));
            AssertCurrentDue(   0.00, Date(2015, 2, 14));
            AssertCurrentDue(   0.00, Date(2015, 2, 15));
            AssertCurrentDue(   0.00, Date(2015, 3, 14));
            AssertCurrentDue( 482.88, Date(2015, 3, 15));
            AssertCurrentDue( 482.88, Date(2015, 4, 14));
            AssertCurrentDue( 482.88, Date(2015, 4, 15));
            AssertCurrentDue( 482.88, Date(2015, 4, 29));
            AssertCurrentDue( 482.88, Date(2015, 4, 30));
            AssertCurrentDue( 482.88, Date(2015, 5, 14));
            AssertCurrentDue(1964.85, Date(2015, 5, 15));
            AssertCurrentDue(1964.85, Date(2015, 6, 14));
            AssertCurrentDue(3446.82, Date(2015, 6, 15));
            AssertCurrentDue(3446.82, Date(2015, 7, 14));
            AssertCurrentDue(4643.11, Date(2015, 7, 15));
        }

        [Fact]
        public void LateUnderAndExtraPayments_WhereLoanIsPaidOff()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled  | Completed |       10000.00 |          0.00 |     0.00 |      0.00 |      10000.00
                                | 2015-01-29 | 2015-01-29  | Scheduled  | Completed |       10000.00 |       1481.97 |    92.59 |   1389.38 |       8610.62
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled  | Completed |        8610.62 |       1481.97 |    85.73 |   1396.24 |       7214.38
                2015-03-15      | 2015-03-17 | 2015-03-17  | Scheduled  | Completed |        7214.38 |        999.09 |    66.80 |    932.29 |       6282.09
                2015-04-15      | 2015-04-17 | 2015-04-17  | Scheduled  | Completed |        6282.09 |       1481.97 |    58.17 |   1423.80 |       4858.29
                                | 2015-04-30 | 2015-04-30  | Additional | Completed |        4858.29 |        303.03 |     0.00 |    303.03 |       4555.26
                2015-05-15      | 2015-05-17 | 2015-05-17  | Scheduled  | Completed |        4555.26 |       1481.97 |    43.58 |   1438.39 |       3116.87
                2015-06-15      | 2015-06-17 | 2015-06-17  | Scheduled  | Completed |        3116.87 |       1481.97 |    28.86 |   1453.11 |       1663.76
                2015-07-15      | 2015-07-17 | 2015-07-17  | Scheduled  | Completed |        1663.76 |       1679.17 |    15.41 |   1663.76 |          0.00");
            
            AssertCurrentDue(   0.00, Date(2015, 1, 14));
            AssertCurrentDue(1481.97, Date(2015, 1, 15));
            AssertCurrentDue(1481.97, Date(2015, 1, 28));
            AssertCurrentDue(   0.00, Date(2015, 1, 29));
            AssertCurrentDue(   0.00, Date(2015, 2, 14));
            AssertCurrentDue(   0.00, Date(2015, 2, 15));
            AssertCurrentDue(   0.00, Date(2015, 3, 14));
            AssertCurrentDue( 482.88, Date(2015, 3, 15));
            AssertCurrentDue( 482.88, Date(2015, 4, 14));
            AssertCurrentDue( 482.88, Date(2015, 4, 15));
            AssertCurrentDue( 482.88, Date(2015, 4, 29));
            AssertCurrentDue( 482.88, Date(2015, 4, 30));
            AssertCurrentDue( 482.88, Date(2015, 5, 14));
            AssertCurrentDue( 482.88, Date(2015, 5, 15));
            AssertCurrentDue( 482.88, Date(2015, 6, 14));
            AssertCurrentDue( 482.88, Date(2015, 6, 15));
            AssertCurrentDue( 482.88, Date(2015, 7, 14));
            AssertCurrentDue(   0.00, Date(2015, 7, 15));
            AssertCurrentDue(   0.00, Date(2015, 8, 14));
            AssertCurrentDue(   0.00, Date(2015, 8, 15));
        }

        [Fact]
        public void LateUnderAndExtraPayments_WhereLastInstallmentIsOpen()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled  | Completed |       10000.00 |          0.00 |     0.00 |      0.00 |      10000.00
                                | 2015-01-29 | 2015-01-29  | Scheduled  | Completed |       10000.00 |       1481.97 |    92.59 |   1389.38 |       8610.62
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled  | Completed |        8610.62 |       1481.97 |    85.73 |   1396.24 |       7214.38
                2015-03-15      | 2015-03-17 | 2015-03-17  | Scheduled  | Completed |        7214.38 |        999.09 |    66.80 |    932.29 |       6282.09
                2015-04-15      | 2015-04-17 | 2015-04-17  | Scheduled  | Completed |        6282.09 |       1481.97 |    58.17 |   1423.80 |       4858.29
                                | 2015-04-30 | 2015-04-30  | Additional | Completed |        4858.29 |        303.03 |     0.00 |    303.03 |       4555.26
                2015-05-15      | 2015-05-17 | 2015-05-17  | Scheduled  | Completed |        4555.26 |       1481.97 |    43.58 |   1438.39 |       3116.87
                2015-06-15      | 2015-06-17 | 2015-06-17  | Scheduled  | Completed |        3116.87 |       1481.97 |    28.86 |   1453.11 |       1663.76
                2015-07-15      | 2015-07-17 |             | Scheduled  | Scheduled |        1663.76 |       1679.17 |    15.41 |   1663.76 |          0.00");
            
            AssertCurrentDue(   0.00, Date(2015, 1, 14));
            AssertCurrentDue(1481.97, Date(2015, 1, 15));
            AssertCurrentDue(1481.97, Date(2015, 1, 28));
            AssertCurrentDue(   0.00, Date(2015, 1, 29));
            AssertCurrentDue(   0.00, Date(2015, 2, 14));
            AssertCurrentDue(   0.00, Date(2015, 2, 15));
            AssertCurrentDue(   0.00, Date(2015, 3, 14));
            AssertCurrentDue( 482.88, Date(2015, 3, 15));
            AssertCurrentDue( 482.88, Date(2015, 4, 14));
            AssertCurrentDue( 482.88, Date(2015, 4, 15));
            AssertCurrentDue( 482.88, Date(2015, 4, 29));
            AssertCurrentDue( 482.88, Date(2015, 4, 30));
            AssertCurrentDue( 482.88, Date(2015, 5, 14));
            AssertCurrentDue( 482.88, Date(2015, 5, 15));
            AssertCurrentDue( 482.88, Date(2015, 6, 14));
            AssertCurrentDue( 482.88, Date(2015, 6, 15));
            AssertCurrentDue( 482.88, Date(2015, 7, 14));
            AssertCurrentDue(1679.17, Date(2015, 7, 15));
        }

        [Fact]
        public void Payoff()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled | Completed |       10000.00 |       1481.97 |    92.59 |   1389.38 |       8610.62
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled | Completed |        8610.62 |       1481.97 |    79.73 |   1402.24 |       7208.38
                2015-03-15      | 2015-03-17 | 2015-03-17  | Scheduled | Completed |        7208.38 |       1481.97 |    66.74 |   1415.23 |       5793.15
                2015-04-15      | 2015-04-17 | 2015-04-17  | Scheduled | Completed |        5793.15 |       1481.97 |    53.64 |   1428.33 |       4364.82
                                | 2015-04-29 | 2015-04-29  | Payoff    | Completed |        4364.82 |       4383.68 |    18.86 |   4364.82 |          0.00");

            AssertCurrentDue(0.00, Date(2015, 1, 14));
            AssertCurrentDue(0.00, Date(2015, 1, 15));
            AssertCurrentDue(0.00, Date(2015, 2, 14));
            AssertCurrentDue(0.00, Date(2015, 2, 15));
            AssertCurrentDue(0.00, Date(2015, 3, 14));
            AssertCurrentDue(0.00, Date(2015, 3, 15));
            AssertCurrentDue(0.00, Date(2015, 4, 14));
            AssertCurrentDue(0.00, Date(2015, 4, 15));
            AssertCurrentDue(0.00, Date(2015, 4, 28));
            AssertCurrentDue(0.00, Date(2015, 4, 29));
            AssertCurrentDue(0.00, Date(2015, 5, 15));
        }

        [Fact]
        public void Payoff_WhereThePayoffInstallmentIsScheduled()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled | Completed |       10000.00 |       1481.97 |    92.59 |   1389.38 |       8610.62
                2015-02-15      | 2015-02-17 | 2015-02-17  | Scheduled | Completed |        8610.62 |       1481.97 |    79.73 |   1402.24 |       7208.38
                2015-03-15      | 2015-03-17 | 2015-03-17  | Scheduled | Completed |        7208.38 |       1481.97 |    66.74 |   1415.23 |       5793.15
                2015-04-15      | 2015-04-17 | 2015-04-17  | Scheduled | Completed |        5793.15 |       1481.97 |    53.64 |   1428.33 |       4364.82
                                | 2015-04-29 | 2015-04-29  | Payoff    | Scheduled |        4364.82 |       4383.68 |    18.86 |   4364.82 |          0.00");

            AssertCurrentDue(0.00, Date(2015, 1, 14));
            AssertCurrentDue(0.00, Date(2015, 1, 15));
            AssertCurrentDue(0.00, Date(2015, 2, 14));
            AssertCurrentDue(0.00, Date(2015, 2, 15));
            AssertCurrentDue(0.00, Date(2015, 3, 14));
            AssertCurrentDue(0.00, Date(2015, 3, 15));
            AssertCurrentDue(0.00, Date(2015, 4, 14));
            AssertCurrentDue(0.00, Date(2015, 4, 15));
            AssertCurrentDue(0.00, Date(2015, 4, 28));
            AssertCurrentDue(0.00, Date(2015, 4, 29));
            AssertCurrentDue(0.00, Date(2015, 5, 14));
            AssertCurrentDue(0.00, Date(2015, 5, 15));
        }

        [Fact]
        public void EarlyExtraAndPayoff()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-17  | Scheduled  | Completed |       10000.00 |       1481.97 |    92.59 |   1389.38 |       8610.62
                2015-02-15      | 2015-02-17 | 2015-02-07  | Scheduled  | Completed |        8610.62 |       1481.97 |    58.47 |   1423.50 |       7187.12
                2015-03-15      | 2015-03-17 | 2015-03-17  | Scheduled  | Completed |        7187.12 |       1481.97 |    84.29 |   1397.68 |       5789.44
                                | 2015-03-30 | 2015-03-30  | Additional | Completed |        5789.44 |        543.21 |     0.00 |    543.21 |       5246.23
                2015-04-15      | 2015-04-17 | 2015-04-17  | Scheduled  | Completed |        5246.23 |       1481.97 |    51.09 |   1430.88 |       3815.35
                                | 2015-04-29 | 2015-04-29  | Payoff     | Completed |        3815.35 |       3831.84 |    16.49 |   3815.35 |          0.00");

            AssertCurrentDue(0.00, Date(2015, 1, 14));
            AssertCurrentDue(0.00, Date(2015, 1, 15));
            AssertCurrentDue(0.00, Date(2015, 2, 14));
            AssertCurrentDue(0.00, Date(2015, 2, 15));
            AssertCurrentDue(0.00, Date(2015, 3, 14));
            AssertCurrentDue(0.00, Date(2015, 3, 15));
            AssertCurrentDue(0.00, Date(2015, 3, 29));
            AssertCurrentDue(0.00, Date(2015, 3, 30));
            AssertCurrentDue(0.00, Date(2015, 4, 14));
            AssertCurrentDue(0.00, Date(2015, 4, 15));
            AssertCurrentDue(0.00, Date(2015, 4, 28));
            AssertCurrentDue(0.00, Date(2015, 4, 29));
            AssertCurrentDue(0.00, Date(2015, 5, 15));
        }

        [Fact]
        public void EarlyPaymentOf1stInstallmentFollowedByPayoffBefore1stAnniversary()
        {
            Schedule.Installments = new[]
            {
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2015, 1, 15) && i.DueDate == Date(2015, 1, 15) && i.PaymentDate == Date(2015, 1, 06) && i.PaymentAmount == 1481.97 && i.EndingBalance == 8582.84 && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == null              && i.DueDate == Date(2015, 1, 10) && i.PaymentDate == Date(2015, 1, 10) && i.PaymentAmount == 8593.44 && i.EndingBalance ==    0.00 && i.Type == InstallmentType.Payoff && i.Status == InstallmentStatus.Completed)
            };
            
            AssertCurrentDue(0.0, Date(2015, 1,  5));
            AssertCurrentDue(0.0, Date(2015, 1,  6));
            AssertCurrentDue(0.0, Date(2015, 1,  9));
            AssertCurrentDue(0.0, Date(2015, 1,  10));
            AssertCurrentDue(0.0, Date(2015, 1,  15));
            AssertCurrentDue(0.0, Date(2015, 2,  15));
            AssertCurrentDue(0.0, Date(2015, 3,  15));
        }
    }
}
