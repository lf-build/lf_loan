﻿using LendFoundry.Loans.Schedule.Calculators;
using LendFoundry.Loans.Schedule.Tests.Fake;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LendFoundry.Loans.Schedule.Tests.Calculators
{
    public class DaysPastDueTest : BaseScheduleTest
    {
        private IDaysPastDueCalculator Calculator { get; }
		private readonly FakeAmortizationService FakeAmortization = new FakeAmortizationService();
		private ILoanTerms LoanTerms { get; set; }

		private Mock<ICurrentDueCalculator> CurrentDueCalculator { get; } = new Mock<ICurrentDueCalculator>();

		public DaysPastDueTest()
		{
			Calculator = new DaysPastDueCalculator(CurrentDueCalculator.Object);
		}

		[Fact]
		public void ZeroBeforeAnniversaryDate()
		{
			LoanTerms = new LoanTerms
			{
				OriginationDate = Date(2015, 1, 15),
				LoanAmount = 10000.0,
				Term = 3,
				Rate = 0.1374
			};

			GivenThatScheduleIs(@"
		              AnniversaryDate | DueDate    | Type      | Status   
		              2015-01-15      | 2015-01-17 | Scheduled | Scheduled
		              2015-02-15      | 2015-02-17 | Scheduled | Scheduled
		              2015-03-15      | 2015-03-17 | Scheduled | Scheduled");

			Assert.Equal(0, Calculator.GetDaysPastDue(Date(2015, 1, 14), Schedule, LoanTerms ));
		}

		[Fact]
		public void ZeroOnFirstDayOfGracePeriod()
		{
			GivenThatScheduleIs(@"
		              AnniversaryDate | DueDate    | Type      | Status   
		              2015-01-15      | 2015-01-17 | Scheduled | Scheduled
		              2015-02-15      | 2015-02-17 | Scheduled | Scheduled
		              2015-03-15      | 2015-03-17 | Scheduled | Scheduled");

			Assert.Equal(0, Calculator.GetDaysPastDue(Date(2015, 1, 15), Schedule, LoanTerms));
		}
		
		[Fact]
		public void ActualNumberOfDaysFromDueDateWhenPastGracePeriod()
		{
			GivenThatScheduleIs(@"
		              AnniversaryDate | DueDate    | Type      | Status   
		              2015-01-15      | 2015-01-17 | Scheduled | Scheduled
		              2015-02-15      | 2015-02-17 | Scheduled | Scheduled
		              2015-03-15      | 2015-03-17 | Scheduled | Scheduled");

			CurrentDueCalculator.SetupSequence(a => a.GetCurrentDue(It.IsAny<DateTimeOffset>(), 
													It.IsAny<IPaymentSchedule>(), It.IsAny<ILoanTerms>()))
								.Returns(1)
								.Returns(1)
								.Returns(1)
								.Returns(0);
			var daysPastDue = Calculator.GetDaysPastDue(Date(2015, 1, 28), Schedule, LoanTerms);

			Assert.Equal(11, daysPastDue);
		}

		[Fact]
		public void SeveralDaysFromDueDatePastGracePeriod()
		{
			GivenThatScheduleIs(@"
		              AnniversaryDate | DueDate    | Type      | Status   
		              2015-01-15      | 2015-01-17 | Scheduled | Scheduled
		              2015-02-15      | 2015-02-17 | Scheduled | Scheduled
		              2015-03-15      | 2015-03-17 | Scheduled | Scheduled");

			CurrentDueCalculator.SetupSequence(a => a.GetCurrentDue(It.IsAny<DateTimeOffset>(),
													It.IsAny<IPaymentSchedule>(), It.IsAny<ILoanTerms>()))
								.Returns(1)
								.Returns(1)
								.Returns(1)
								.Returns(1);

			Assert.Equal(696, Calculator.GetDaysPastDue(Date(2016, 12, 13), Schedule, LoanTerms));
		}

		[Fact]
		public void CurrentDue_zero()
		{
			LoanTerms = new LoanTerms
			{
				OriginationDate = Date(2015, 1, 15),
				LoanAmount = 10000.0,
				Term = 5,
				Rate = 0.1374
			};

			var date = Date(2016, 04, 19);

			GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentDate
                2016-02-15      | 2016-02-17 | Scheduled | Completed |        1000.00 |        206.92 |    11.45 |    195.47 |        804.53 | 2016-02-15 
                2016-03-15      | 2016-03-17 | Scheduled | Completed |         804.53 |        206.92 |     9.21 |    197.71 |        606.82 | 2016-03-15 
                2016-04-15      | 2016-04-17 | Scheduled | Completed |         606.82 |        206.92 |     6.95 |    199.97 |        406.85 | 2016-04-15 
                2016-05-15      | 2016-05-17 | Scheduled | Scheduled |         406.85 |        206.92 |     4.66 |    202.26 |        204.59 |            
                2016-06-15      | 2016-06-17 | Scheduled | Scheduled |         204.59 |        206.93 |     2.34 |    204.59 |          0.00 |            ");

			var list = Schedule.Installments.Select(a => new Amortization.Installment
			{
				AnniversaryDate = (DateTimeOffset)a.AnniversaryDate,
				DueDate = (DateTimeOffset)a.DueDate,
				Amount = 206.92
			}).Cast<Amortization.IInstallment>().ToList();

			CurrentDueCalculator.SetupSequence(a => a.GetCurrentDue(It.IsAny<DateTimeOffset>(), It.IsAny<IPaymentSchedule>(), It.IsAny<ILoanTerms>())).Returns(0);
			FakeAmortization.InMemoryData = list;

			Assert.Equal(0, Calculator.GetDaysPastDue(date, Schedule, LoanTerms));
		}

		[Fact]
		public void Completed_but_paymentAmount_less_original()
		{
			LoanTerms = new LoanTerms
			{
				OriginationDate = Date(2015, 1, 15),
				LoanAmount = 10000.0,
				Term = 5,
				Rate = 0.1374
			};

			var date = Date(2016, 06, 29);

			GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentDate
                2016-02-15      | 2016-02-17 | Scheduled | Completed |        1000.00 |        206.92 |    11.45 |    195.47 |        804.53 | 2016-02-15 
                2016-03-15      | 2016-03-17 | Scheduled | Completed |         804.53 |         95.00 |     9.21 |     85.79 |        718.74 | 2016-03-15 
                2016-04-15      | 2016-04-17 | Scheduled | Completed |         718.74 |        206.92 |     8.23 |    198.69 |        520.05 | 2016-04-15 
                2016-05-15      | 2016-05-17 | Scheduled | Scheduled |         520.05 |        206.92 |     5.95 |    200.97 |        319.08 |            
                2016-06-15      | 2016-06-17 | Scheduled | Scheduled |         319.08 |        322.73 |     3.65 |    319.08 |          0.00 |            ");

			IList<Amortization.IInstallment> list = Schedule.Installments.Select(a => new Amortization.Installment
			{
				AnniversaryDate = (DateTimeOffset)a.AnniversaryDate,
				DueDate = (DateTimeOffset)a.DueDate,
				Amount = 206.92
			}).Cast<Amortization.IInstallment>().ToList();

			CurrentDueCalculator.SetupSequence(a => a.GetCurrentDue(It.IsAny<DateTimeOffset>(), It.IsAny<IPaymentSchedule>(), It.IsAny<ILoanTerms>()))
								.Returns(1)
								.Returns(1)
								.Returns(1)
								.Returns(1)
								.Returns(1)
								.Returns(0);

			FakeAmortization.InMemoryData = list;

			Assert.Equal(104, Calculator.GetDaysPastDue(date, Schedule, LoanTerms));
		}

		[Fact]
		public void Installment_due_out_gracePeriod()
		{
			LoanTerms = new LoanTerms
			{
				OriginationDate = Date(2015, 1, 15),
				LoanAmount = 10000.0,
				Term = 5,
				Rate = 0.1374
			};

			var date = Date(2016, 06, 30);

			GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentDate
                2016-03-15      | 2016-03-17 | Scheduled | Completed |        1000.00 |        206.92 |    11.45 |    195.47 |        804.53 | 2016-03-15 
                2016-04-15      | 2016-04-17 | Scheduled | Completed |         804.53 |        206.92 |     9.21 |    197.71 |        606.82 | 2016-04-15 
                2016-05-15      | 2016-05-17 | Scheduled | Completed |         606.82 |        206.92 |     6.95 |    199.97 |        406.85 | 2016-05-15 
                2016-06-15      | 2016-06-17 | Scheduled | Scheduled |         406.85 |        206.92 |     4.66 |    202.26 |        204.59 |            
                2016-07-15      | 2016-07-17 | Scheduled | Scheduled |         204.59 |        206.93 |     2.34 |    204.59 |          0.00 |            ");

			IList<Amortization.IInstallment> list = Schedule.Installments.Select(a => new Amortization.Installment
			{
				AnniversaryDate = (DateTimeOffset)a.AnniversaryDate,
				DueDate = (DateTimeOffset)a.DueDate,
				Amount = 206.92
			}).Cast<Amortization.IInstallment>().ToList();

			CurrentDueCalculator.SetupSequence(a => a.GetCurrentDue(It.IsAny<DateTimeOffset>(), It.IsAny<IPaymentSchedule>(), It.IsAny<ILoanTerms>()))
					.Returns(1)
					.Returns(1)
					.Returns(0);

			FakeAmortization.InMemoryData = list;

			Assert.Equal(13, Calculator.GetDaysPastDue(date, Schedule, LoanTerms));
		}

		[Fact]
		public void Installment_due_in_gracePeriod()
		{
			LoanTerms = new LoanTerms
			{
				OriginationDate = Date(2015, 1, 15),
				LoanAmount = 10000.0,
				Term = 5,
				Rate = 0.1374
			};

			var date = Date(2016, 06, 29);

			GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | PaymentDate
                2016-03-15      | 2016-03-17 | Scheduled | Completed |        1000.00 |        206.92 |    11.45 |    195.47 |        804.53 | 2016-03-15 
                2016-04-15      | 2016-04-17 | Scheduled | Completed |         804.53 |        206.92 |     9.21 |    197.71 |        606.82 | 2016-04-15 
                2016-05-15      | 2016-05-17 | Scheduled | Completed |         606.82 |        206.92 |     6.95 |    199.97 |        406.85 | 2016-05-15 
                2016-06-15      | 2016-06-17 | Scheduled | Scheduled |         406.85 |        206.92 |     4.66 |    202.26 |        204.59 |            
                2016-07-15      | 2016-07-17 | Scheduled | Scheduled |         204.59 |        206.93 |     2.34 |    204.59 |          0.00 |            ");

			IList<Amortization.IInstallment> list = Schedule.Installments.Select(a => new Amortization.Installment
			{
				AnniversaryDate = (DateTimeOffset)a.AnniversaryDate,
				DueDate = (DateTimeOffset)a.DueDate,
				Amount = 206.92
			}).Cast<Amortization.IInstallment>().ToList();

			CurrentDueCalculator.SetupSequence(a => a.GetCurrentDue(It.IsAny<DateTimeOffset>(), It.IsAny<IPaymentSchedule>(), It.IsAny<ILoanTerms>()))
				.Returns(0);

			FakeAmortization.InMemoryData = list;

			Assert.Equal(0, Calculator.GetDaysPastDue(date, Schedule, LoanTerms));
		}
	}
}
