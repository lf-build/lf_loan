﻿using LendFoundry.Amortization;
using LendFoundry.Amortization.Interest;
using LendFoundry.Loans.Schedule.Calculators;
using LendFoundry.Loans.Schedule.Tests.Fake;
using LendFoundry.Loans.Schedule.Tests.Mocks;
using System;
using System.Collections.Generic;
using Xunit;

namespace LendFoundry.Loans.Schedule.Tests.Calculators
{
    public class CatchUpAmountTest : BaseScheduleTest
    {
        private ICatchUpAmountCalculator Calculator { get; }
        private ILoanTerms LoanTerms { get; }
        public AmortizationServiceMock AmortizationService { get; }

        public CatchUpAmountTest()
        {
            LoanTerms = new LoanTerms
            {
                OriginationDate = Date(2014, 12, 15),
                LoanAmount = 10000.0,
                Term = 5,
                Rate = 11.111
            };

            AmortizationService = new AmortizationServiceMock(LoanTerms);

            AmortizationService
                .OnAmortize(new AmortizationRequest())
                .Returns(@"
                    AnniversaryDate | DueDate    | OpeningBalance |  Amount | Interest | Principal | EndingBalance
                    2015-01-15      | 2015-01-17 |       10000.00 | 2055.90 |    92.59 |   1963.31 |       8036.69
                    2015-02-15      | 2015-02-17 |        8036.69 | 2055.90 |    74.41 |   1981.49 |       6055.20
                    2015-03-15      | 2015-03-17 |        6055.20 | 2055.90 |    56.07 |   1999.83 |       4055.37
                    2015-04-15      | 2015-04-17 |        4055.37 | 2055.90 |    37.55 |   2018.35 |       2037.02
                    2015-05-15      | 2015-05-17 |        2037.02 | 2055.88 |    18.86 |   2037.02 |          0.00");

            Calculator = new CatchUpAmountCalculator(new DueDateBasedGracePeriod(), AmortizationService.Object);
        }

        private Outcome<double> OnGetSimpleInterest(double principal, DateTimeOffset start, DateTimeOffset end) =>
            AmortizationService.OnGetSimpleInterest(principal, start, end, new InterestAccrualOptions
            {
                EffectiveEndDate = EffectiveEndDate.PreviousAnniversary
            });

        private Outcome<double> OnGetSimpleInterest(IEnumerable<InterestAccrualPeriod> periods) =>
            AmortizationService.OnGetSimpleInterest(periods, new InterestAccrualOptions
            {
                EffectiveEndDate = EffectiveEndDate.PreviousAnniversary
            });

        [Fact]
        public void WhenNoPaymentHasBeenMade()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | Scheduled | Scheduled |       10000.00 |       2055.90 |    92.59 |   1963.31 |       8036.69
                2015-02-15      | 2015-02-17 | Scheduled | Scheduled |        8036.69 |       2055.90 |    74.41 |   1981.49 |       6055.20
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled |        6055.20 |       2055.90 |    56.07 |   1999.83 |       4055.37
                2015-04-15      | 2015-04-17 | Scheduled | Scheduled |        4055.37 |       2055.90 |    37.55 |   2018.35 |       2037.02
                2015-05-15      | 2015-05-17 | Scheduled | Scheduled |        2037.02 |       2055.88 |    18.86 |   2037.02 |          0.00");

            OnGetSimpleInterest(10000.00, Date(2014, 12, 15), Date(2015, 01, 15)).Returns(92.59);
            OnGetSimpleInterest(10000.00, Date(2014, 12, 15), Date(2015, 02, 14)).Returns(92.59);
            OnGetSimpleInterest(10000.00, Date(2014, 12, 15), Date(2015, 02, 15)).Returns(185.18);
            OnGetSimpleInterest(10000.00, Date(2014, 12, 15), Date(2015, 03, 14)).Returns(185.18);
            OnGetSimpleInterest(10000.00, Date(2014, 12, 15), Date(2015, 03, 15)).Returns(277.78);
            OnGetSimpleInterest(10000.00, Date(2014, 12, 15), Date(2015, 04, 14)).Returns(277.78);
            OnGetSimpleInterest(10000.00, Date(2014, 12, 15), Date(2015, 04, 15)).Returns(370.37);
            OnGetSimpleInterest(10000.00, Date(2014, 12, 15), Date(2015, 05, 14)).Returns(370.37);
            OnGetSimpleInterest(10000.00, Date(2014, 12, 15), Date(2015, 05, 15)).Returns(462.96);
            OnGetSimpleInterest(10000.00, Date(2014, 12, 15), Date(2015, 06, 14)).Returns(462.96);
            OnGetSimpleInterest(10000.00, Date(2014, 12, 15), Date(2015, 06, 15)).Returns(555.55);
            OnGetSimpleInterest(10000.00, Date(2014, 12, 15), Date(2015, 07, 14)).Returns(555.55);
            OnGetSimpleInterest(10000.00, Date(2014, 12, 15), Date(2015, 07, 15)).Returns(648.14);

            Assert.Equal(    0.00, Calculator.GetCatchUpAmount(Date(2014, 12, 14), Schedule, LoanTerms));
            Assert.Equal(    0.00, Calculator.GetCatchUpAmount(Date(2014, 12, 15), Schedule, LoanTerms));
            Assert.Equal(    0.00, Calculator.GetCatchUpAmount(Date(2015,  1, 14), Schedule, LoanTerms));
            Assert.Equal( 2055.90, Calculator.GetCatchUpAmount(Date(2015,  1, 15), Schedule, LoanTerms));
            Assert.Equal( 2055.90, Calculator.GetCatchUpAmount(Date(2015,  2, 14), Schedule, LoanTerms));
            Assert.Equal( 4129.98, Calculator.GetCatchUpAmount(Date(2015,  2, 15), Schedule, LoanTerms));
            Assert.Equal( 4129.98, Calculator.GetCatchUpAmount(Date(2015,  3, 14), Schedule, LoanTerms));
            Assert.Equal( 6222.41, Calculator.GetCatchUpAmount(Date(2015,  3, 15), Schedule, LoanTerms));
            Assert.Equal( 6222.41, Calculator.GetCatchUpAmount(Date(2015,  4, 14), Schedule, LoanTerms));
            Assert.Equal( 8333.35, Calculator.GetCatchUpAmount(Date(2015,  4, 15), Schedule, LoanTerms));
            Assert.Equal( 8333.35, Calculator.GetCatchUpAmount(Date(2015,  5, 14), Schedule, LoanTerms));
            Assert.Equal(10462.96, Calculator.GetCatchUpAmount(Date(2015,  5, 15), Schedule, LoanTerms));
            Assert.Equal(10462.96, Calculator.GetCatchUpAmount(Date(2015,  6, 14), Schedule, LoanTerms));
            Assert.Equal(10555.55, Calculator.GetCatchUpAmount(Date(2015,  6, 15), Schedule, LoanTerms));
            Assert.Equal(10555.55, Calculator.GetCatchUpAmount(Date(2015,  7, 14), Schedule, LoanTerms));
            Assert.Equal(10648.14, Calculator.GetCatchUpAmount(Date(2015,  7, 15), Schedule, LoanTerms));
        }

        [Fact]
        public void WhenAllInstallmentsArePaid()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | Scheduled | Completed |       10000.00 |       2055.90 |    92.59 |   1963.31 |       8036.69
                2015-02-15      | 2015-02-17 | Scheduled | Completed |        8036.69 |       2055.90 |    74.41 |   1981.49 |       6055.20
                2015-03-15      | 2015-03-17 | Scheduled | Completed |        6055.20 |       2055.90 |    56.07 |   1999.83 |       4055.37
                2015-04-15      | 2015-04-17 | Scheduled | Completed |        4055.37 |       2055.90 |    37.55 |   2018.35 |       2037.02
                2015-05-15      | 2015-05-17 | Scheduled | Completed |        2037.02 |       2055.88 |    18.86 |   2037.02 |          0.00");

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 01, 15) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 02, 14) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 02, 15) }
            })
            .Returns(92.59 + 74.41);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 03, 14) }
            })
            .Returns(92.59 + 74.41);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  6055.20, Start = Date(2015, 02, 15), End = Date(2015, 03, 15) }
            })
            .Returns(92.59 + 74.41 + 56.07);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  6055.20, Start = Date(2015, 02, 15), End = Date(2015, 04, 14) }
            })
            .Returns(92.59 + 74.41 + 56.07);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  6055.20, Start = Date(2015, 02, 15) },
                new InterestAccrualPeriod { Principal =  4055.37, Start = Date(2015, 03, 15), End = Date(2015, 04, 15) }
            })
            .Returns(92.59 + 74.41 + 56.07 + 37.55);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  6055.20, Start = Date(2015, 02, 15) },
                new InterestAccrualPeriod { Principal =  4055.37, Start = Date(2015, 03, 15), End = Date(2015, 05, 14) }
            })
            .Returns(92.59 + 74.41 + 56.07 + 37.55);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  6055.20, Start = Date(2015, 02, 15) },
                new InterestAccrualPeriod { Principal =  4055.37, Start = Date(2015, 03, 15) },
                new InterestAccrualPeriod { Principal =  2037.02, Start = Date(2015, 04, 15), End = Date(2015, 05, 15) }
            })
            .Returns(92.59 + 74.41 + 56.07 + 37.55 + 18.86);

            Assert.Equal(0.00, Calculator.GetCatchUpAmount(Date(2015, 1, 14), Schedule, LoanTerms));
            Assert.Equal(0.00, Calculator.GetCatchUpAmount(Date(2015, 1, 15), Schedule, LoanTerms));
            Assert.Equal(0.00, Calculator.GetCatchUpAmount(Date(2015, 2, 14), Schedule, LoanTerms));
            Assert.Equal(0.00, Calculator.GetCatchUpAmount(Date(2015, 2, 15), Schedule, LoanTerms));
            Assert.Equal(0.00, Calculator.GetCatchUpAmount(Date(2015, 3, 14), Schedule, LoanTerms));
            Assert.Equal(0.00, Calculator.GetCatchUpAmount(Date(2015, 3, 15), Schedule, LoanTerms));
            Assert.Equal(0.00, Calculator.GetCatchUpAmount(Date(2015, 4, 14), Schedule, LoanTerms));
            Assert.Equal(0.00, Calculator.GetCatchUpAmount(Date(2015, 4, 15), Schedule, LoanTerms));
            Assert.Equal(0.00, Calculator.GetCatchUpAmount(Date(2015, 5, 14), Schedule, LoanTerms));
            Assert.Equal(0.00, Calculator.GetCatchUpAmount(Date(2015, 5, 15), Schedule, LoanTerms));
        }

        [Fact]
        public void When1stInstallmentIsPaid()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | Scheduled | Completed |       10000.00 |       2055.90 |    92.59 |   1963.31 |       8036.69
                2015-02-15      | 2015-02-17 | Scheduled | Scheduled |        8036.69 |       2055.90 |    74.41 |   1981.49 |       6055.20
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled |        6055.20 |       2055.90 |    56.07 |   1999.83 |       4055.37
                2015-04-15      | 2015-04-17 | Scheduled | Scheduled |        4055.37 |       2055.90 |    37.55 |   2018.35 |       2037.02
                2015-05-15      | 2015-05-17 | Scheduled | Scheduled |        2037.02 |       2055.88 |    18.86 |   2037.02 |          0.00");

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 01, 15) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 02, 14) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 02, 15) }
            })
            .Returns(167.00);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 03, 14) }
            })
            .Returns(167.00);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 03, 15) }
            })
            .Returns(241.42);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 04, 14) }
            })
            .Returns(241.42);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 04, 15) }
            })
            .Returns(315.83);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 05, 14) }
            })
            .Returns(315.83);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 05, 15) }
            })
            .Returns(390.24);

            OnGetSimpleInterest(principal: 10000.0, start: Date(2015, 1, 15), end: Date(2015, 2, 15)).Returns( 74.41);
            OnGetSimpleInterest(principal: 10000.0, start: Date(2015, 1, 15), end: Date(2015, 3, 15)).Returns(148.83);
            OnGetSimpleInterest(principal: 10000.0, start: Date(2015, 1, 15), end: Date(2015, 4, 15)).Returns(223.24);
            OnGetSimpleInterest(principal: 10000.0, start: Date(2015, 1, 15), end: Date(2015, 5, 15)).Returns(297.65);

            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015, 1, 14), Schedule, LoanTerms));
            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015, 1, 15), Schedule, LoanTerms));
            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015, 2, 14), Schedule, LoanTerms));
            Assert.Equal(2055.90, Calculator.GetCatchUpAmount(Date(2015, 2, 15), Schedule, LoanTerms));
            Assert.Equal(2055.90, Calculator.GetCatchUpAmount(Date(2015, 3, 14), Schedule, LoanTerms));
            Assert.Equal(4130.15, Calculator.GetCatchUpAmount(Date(2015, 3, 15), Schedule, LoanTerms));
            Assert.Equal(4130.15, Calculator.GetCatchUpAmount(Date(2015, 4, 14), Schedule, LoanTerms));
            Assert.Equal(6222.91, Calculator.GetCatchUpAmount(Date(2015, 4, 15), Schedule, LoanTerms));
            Assert.Equal(6222.91, Calculator.GetCatchUpAmount(Date(2015, 5, 14), Schedule, LoanTerms));
            Assert.Equal(8334.34, Calculator.GetCatchUpAmount(Date(2015, 5, 15), Schedule, LoanTerms));
        }

        [Fact]
        public void When1stAnd2ndInstallmentsArePaid()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | Scheduled | Completed |       10000.00 |       2055.90 |    92.59 |   1963.31 |       8036.69
                2015-02-15      | 2015-02-17 | Scheduled | Completed |        8036.69 |       2055.90 |    74.41 |   1981.49 |       6055.20
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled |        6055.20 |       2055.90 |    56.07 |   1999.83 |       4055.37
                2015-04-15      | 2015-04-17 | Scheduled | Scheduled |        4055.37 |       2055.90 |    37.55 |   2018.35 |       2037.02
                2015-05-15      | 2015-05-17 | Scheduled | Scheduled |        2037.02 |       2055.88 |    18.86 |   2037.02 |          0.00");

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 01, 15) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 02, 14) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 02, 15) }
            })
            .Returns(167.00);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 03, 14) }
            })
            .Returns(167.00);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  6055.20, Start = Date(2015, 02, 15), End = Date(2015, 03, 15) }
            })
            .Returns(223.07);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  6055.20, Start = Date(2015, 02, 15), End = Date(2015, 04, 14) }
            })
            .Returns(223.07);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  6055.20, Start = Date(2015, 02, 15), End = Date(2015, 04, 15) }
            })
            .Returns(279.13);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  6055.20, Start = Date(2015, 02, 15), End = Date(2015, 05, 14) }
            })
            .Returns(279.13);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  6055.20, Start = Date(2015, 02, 15), End = Date(2015, 05, 15) }
            })
            .Returns(335.20);

            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015, 1, 14), Schedule, LoanTerms));
            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015, 1, 15), Schedule, LoanTerms));
            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015, 2, 14), Schedule, LoanTerms));
            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015, 2, 15), Schedule, LoanTerms));
            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015, 3, 14), Schedule, LoanTerms));
            Assert.Equal(2055.90, Calculator.GetCatchUpAmount(Date(2015, 3, 15), Schedule, LoanTerms));
            Assert.Equal(2055.90, Calculator.GetCatchUpAmount(Date(2015, 4, 14), Schedule, LoanTerms));
            Assert.Equal(4130.31, Calculator.GetCatchUpAmount(Date(2015, 4, 15), Schedule, LoanTerms));
            Assert.Equal(4130.31, Calculator.GetCatchUpAmount(Date(2015, 5, 14), Schedule, LoanTerms));
            Assert.Equal(6223.40, Calculator.GetCatchUpAmount(Date(2015, 5, 15), Schedule, LoanTerms));
        }

        [Fact]
        public void When1stInstallmentIsUnderpaid()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | Scheduled | Completed |       10000.00 |        999.00 |    92.59 |    906.41 |       9093.59
                2015-02-15      | 2015-02-17 | Scheduled | Scheduled |        9093.59 |       2055.90 |    84.20 |   1971.70 |       7121.89
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled |        7121.89 |       2055.90 |    65.94 |   1989.96 |       5131.93
                2015-04-15      | 2015-04-17 | Scheduled | Scheduled |        5131.93 |       2055.90 |    47.52 |   2008.38 |       3123.55
                2015-05-15      | 2015-05-17 | Scheduled | Scheduled |        3123.55 |       3152.47 |    28.92 |   3123.55 |          0.00");

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 01, 15) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 02, 14) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  9093.59, Start = Date(2015, 01, 15), End = Date(2015, 02, 15) }
            })
            .Returns(176.79);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  9093.59, Start = Date(2015, 01, 15), End = Date(2015, 03, 14) }
            })
            .Returns(176.79);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  9093.59, Start = Date(2015, 01, 15), End = Date(2015, 03, 15) }
            })
            .Returns(260.99);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  9093.59, Start = Date(2015, 01, 15), End = Date(2015, 04, 14) }
            })
            .Returns(260.99);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  9093.59, Start = Date(2015, 01, 15), End = Date(2015, 04, 15) }
            })
            .Returns(345.19);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  9093.59, Start = Date(2015, 01, 15), End = Date(2015, 05, 14) }
            })
            .Returns(345.19);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  9093.59, Start = Date(2015, 01, 15), End = Date(2015, 05, 15) }
            })
            .Returns(429.39);

            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015, 1, 14), Schedule, LoanTerms));
            Assert.Equal(1056.90, Calculator.GetCatchUpAmount(Date(2015, 1, 15), Schedule, LoanTerms));
            Assert.Equal(1056.90, Calculator.GetCatchUpAmount(Date(2015, 2, 14), Schedule, LoanTerms));
            Assert.Equal(3122.59, Calculator.GetCatchUpAmount(Date(2015, 2, 15), Schedule, LoanTerms));
            Assert.Equal(3122.59, Calculator.GetCatchUpAmount(Date(2015, 3, 14), Schedule, LoanTerms));
            Assert.Equal(5206.62, Calculator.GetCatchUpAmount(Date(2015, 3, 15), Schedule, LoanTerms));
            Assert.Equal(5206.62, Calculator.GetCatchUpAmount(Date(2015, 4, 14), Schedule, LoanTerms));
            Assert.Equal(7309.17, Calculator.GetCatchUpAmount(Date(2015, 4, 15), Schedule, LoanTerms));
            Assert.Equal(7309.17, Calculator.GetCatchUpAmount(Date(2015, 5, 14), Schedule, LoanTerms));
            Assert.Equal(9430.39, Calculator.GetCatchUpAmount(Date(2015, 5, 15), Schedule, LoanTerms));
        }

        [Fact]
        public void When1stAnd2ndInstallmentsAreUnderpaid()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | Scheduled | Completed |       10000.00 |        888.00 |    92.59 |    795.41 |       9204.59
                2015-02-15      | 2015-02-17 | Scheduled | Completed |        9204.59 |        999.00 |    85.23 |    913.77 |       8290.82
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled |        8290.82 |       2055.90 |    76.77 |   1979.13 |       6311.69
                2015-04-15      | 2015-04-17 | Scheduled | Scheduled |        6311.69 |       2055.90 |    58.44 |   1997.46 |       4314.23
                2015-05-15      | 2015-05-17 | Scheduled | Scheduled |        4314.23 |       4354.18 |    39.95 |   4314.23 |          0.00");

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 01, 15) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 02, 14) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  9204.59, Start = Date(2015, 01, 15), End = Date(2015, 02, 15) }
            })
            .Returns(177.82);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  9204.59, Start = Date(2015, 01, 15), End = Date(2015, 03, 14) }
            })
            .Returns(177.82);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  9204.59, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  8290.82, Start = Date(2015, 02, 15), End = Date(2015, 03, 15) }
            })
            .Returns(254.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  9204.59, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  8290.82, Start = Date(2015, 02, 15), End = Date(2015, 04, 14) }
            })
            .Returns(254.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  9204.59, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  8290.82, Start = Date(2015, 02, 15), End = Date(2015, 04, 15) }
            })
            .Returns(331.35);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  9204.59, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  8290.82, Start = Date(2015, 02, 15), End = Date(2015, 05, 14) }
            })
            .Returns(331.35);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  9204.59, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  8290.82, Start = Date(2015, 02, 15), End = Date(2015, 05, 15) }
            })
            .Returns(408.12);

            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015, 1, 14), Schedule, LoanTerms));
            Assert.Equal(1167.90, Calculator.GetCatchUpAmount(Date(2015, 1, 15), Schedule, LoanTerms));
            Assert.Equal(1167.90, Calculator.GetCatchUpAmount(Date(2015, 2, 14), Schedule, LoanTerms));
            Assert.Equal(2235.62, Calculator.GetCatchUpAmount(Date(2015, 2, 15), Schedule, LoanTerms));
            Assert.Equal(2235.62, Calculator.GetCatchUpAmount(Date(2015, 3, 14), Schedule, LoanTerms));
            Assert.Equal(4312.22, Calculator.GetCatchUpAmount(Date(2015, 3, 15), Schedule, LoanTerms));
            Assert.Equal(4312.22, Calculator.GetCatchUpAmount(Date(2015, 4, 14), Schedule, LoanTerms));
            Assert.Equal(6407.33, Calculator.GetCatchUpAmount(Date(2015, 4, 15), Schedule, LoanTerms));
            Assert.Equal(6407.33, Calculator.GetCatchUpAmount(Date(2015, 5, 14), Schedule, LoanTerms));
            Assert.Equal(8521.12, Calculator.GetCatchUpAmount(Date(2015, 5, 15), Schedule, LoanTerms));
        }

        [Fact]
        public void MixedUnderpaymentsAndExtraPayments()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | Scheduled  | Completed |       10000.00 |        888.00 |    92.59 |    795.41 |       9204.59
                                | 2015-01-31 | Additional | Completed |        9204.59 |         50.05 |     0.00 |     50.05 |       9154.54
                2015-02-15      | 2015-02-17 | Scheduled  | Completed |        9154.54 |        999.00 |    87.84 |    911.16 |       8243.38
                                | 2015-02-28 | Additional | Completed |        8243.38 |         60.06 |     0.00 |     60.06 |       8183.32
                2015-03-15      | 2015-03-17 | Scheduled  | Scheduled |        8183.32 |       2055.90 |    70.96 |   1984.94 |       6198.38
                2015-04-15      | 2015-04-17 | Scheduled  | Scheduled |        6198.38 |       2055.90 |    57.39 |   1998.51 |       4199.87
                2015-05-15      | 2015-05-17 | Scheduled  | Scheduled |        4199.87 |       4238.76 |    38.89 |   4199.87 |          0.00");

            OnGetSimpleInterest(new []
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 01, 15) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new []
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 01, 30) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new []
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 01, 31) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new []
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 02, 14) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  9204.59, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  9154.54, Start = Date(2015, 01, 31), End = Date(2015, 02, 15) }
            })
            .Returns(180.43);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  9204.59, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  9154.54, Start = Date(2015, 01, 31), End = Date(2015, 02, 27) }
            })
            .Returns(180.43);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  9204.59, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  9154.54, Start = Date(2015, 01, 31), End = Date(2015, 02, 28) }
            })
            .Returns(180.43);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  9204.59, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  9154.54, Start = Date(2015, 01, 31), End = Date(2015, 03, 14) }
            })
            .Returns(180.43);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  9204.59, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  9154.54, Start = Date(2015, 01, 31) },
                new InterestAccrualPeriod { Principal =  8243.38, Start = Date(2015, 02, 15) },
                new InterestAccrualPeriod { Principal =  8183.32, Start = Date(2015, 02, 28), End = Date(2015, 03, 15) }
            })
            .Returns(251.39);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  9204.59, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  9154.54, Start = Date(2015, 01, 31) },
                new InterestAccrualPeriod { Principal =  8243.38, Start = Date(2015, 02, 15) },
                new InterestAccrualPeriod { Principal =  8183.32, Start = Date(2015, 02, 28), End = Date(2015, 04, 14) }
            })
            .Returns(251.39);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  9204.59, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  9154.54, Start = Date(2015, 01, 31) },
                new InterestAccrualPeriod { Principal =  8243.38, Start = Date(2015, 02, 15) },
                new InterestAccrualPeriod { Principal =  8183.32, Start = Date(2015, 02, 28), End = Date(2015, 04, 15) }
            })
            .Returns(327.16);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  9204.59, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  9154.54, Start = Date(2015, 01, 31) },
                new InterestAccrualPeriod { Principal =  8243.38, Start = Date(2015, 02, 15) },
                new InterestAccrualPeriod { Principal =  8183.32, Start = Date(2015, 02, 28), End = Date(2015, 05, 14) }
            })
            .Returns(327.16);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  9204.59, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  9154.54, Start = Date(2015, 01, 31) },
                new InterestAccrualPeriod { Principal =  8243.38, Start = Date(2015, 02, 15) },
                new InterestAccrualPeriod { Principal =  8183.32, Start = Date(2015, 02, 28), End = Date(2015, 05, 15) }
            })
            .Returns(402.93);

            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015, 1, 14), Schedule, LoanTerms));
            Assert.Equal(1167.90, Calculator.GetCatchUpAmount(Date(2015, 1, 15), Schedule, LoanTerms));
            Assert.Equal(1167.90, Calculator.GetCatchUpAmount(Date(2015, 1, 30), Schedule, LoanTerms));
            Assert.Equal(1117.85, Calculator.GetCatchUpAmount(Date(2015, 1, 31), Schedule, LoanTerms));
            Assert.Equal(1117.85, Calculator.GetCatchUpAmount(Date(2015, 2, 14), Schedule, LoanTerms));
            Assert.Equal(2188.18, Calculator.GetCatchUpAmount(Date(2015, 2, 15), Schedule, LoanTerms));
            Assert.Equal(2188.18, Calculator.GetCatchUpAmount(Date(2015, 2, 27), Schedule, LoanTerms));
            Assert.Equal(2128.12, Calculator.GetCatchUpAmount(Date(2015, 2, 28), Schedule, LoanTerms));
            Assert.Equal(2128.12, Calculator.GetCatchUpAmount(Date(2015, 3, 14), Schedule, LoanTerms));
            Assert.Equal(4198.91, Calculator.GetCatchUpAmount(Date(2015, 3, 15), Schedule, LoanTerms));
            Assert.Equal(4198.91, Calculator.GetCatchUpAmount(Date(2015, 4, 14), Schedule, LoanTerms));
            Assert.Equal(6293.03, Calculator.GetCatchUpAmount(Date(2015, 4, 15), Schedule, LoanTerms));
            Assert.Equal(6293.03, Calculator.GetCatchUpAmount(Date(2015, 5, 14), Schedule, LoanTerms));
            Assert.Equal(8405.82, Calculator.GetCatchUpAmount(Date(2015, 5, 15), Schedule, LoanTerms));
        }

        [Fact]
        public void Missed1stInstallment()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | Scheduled | Completed |       10000.00 |          0.00 |     0.00 |      0.00 |      10000.00
                2015-02-15      | 2015-02-17 | Scheduled | Scheduled |       10000.00 |       2055.90 |   185.18 |   1870.72 |       8129.28
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled |        8129.28 |       2055.90 |    75.27 |   1980.63 |       6148.65
                2015-04-15      | 2015-04-17 | Scheduled | Scheduled |        6148.65 |       2055.90 |    56.93 |   1998.97 |       4149.68
                2015-05-15      | 2015-05-17 | Scheduled | Scheduled |        4149.68 |       4188.10 |    38.42 |   4149.68 |          0.00");

            OnGetSimpleInterest(principal: 10000.00, start: Date(2014, 12, 15), end: Date(2015, 01, 15)).Returns( 92.59);
            OnGetSimpleInterest(principal: 10000.00, start: Date(2014, 12, 15), end: Date(2015, 02, 14)).Returns( 92.59);
            OnGetSimpleInterest(principal: 10000.00, start: Date(2014, 12, 15), end: Date(2015, 02, 15)).Returns(185.18);
            OnGetSimpleInterest(principal: 10000.00, start: Date(2014, 12, 15), end: Date(2015, 03, 14)).Returns(185.18);
            OnGetSimpleInterest(principal: 10000.00, start: Date(2014, 12, 15), end: Date(2015, 03, 15)).Returns(277.78);
            OnGetSimpleInterest(principal: 10000.00, start: Date(2014, 12, 15), end: Date(2015, 04, 14)).Returns(277.78);
            OnGetSimpleInterest(principal: 10000.00, start: Date(2014, 12, 15), end: Date(2015, 04, 15)).Returns(370.37);
            OnGetSimpleInterest(principal: 10000.00, start: Date(2014, 12, 15), end: Date(2015, 05, 14)).Returns(370.37);
            OnGetSimpleInterest(principal: 10000.00, start: Date(2014, 12, 15), end: Date(2015, 05, 15)).Returns(462.96);

            Assert.Equal(    0.00, Calculator.GetCatchUpAmount(Date(2015, 1, 14), Schedule, LoanTerms));
            Assert.Equal( 2055.90, Calculator.GetCatchUpAmount(Date(2015, 1, 15), Schedule, LoanTerms));
            Assert.Equal( 2055.90, Calculator.GetCatchUpAmount(Date(2015, 2, 14), Schedule, LoanTerms));
            Assert.Equal( 4129.98, Calculator.GetCatchUpAmount(Date(2015, 2, 15), Schedule, LoanTerms));
            Assert.Equal( 4129.98, Calculator.GetCatchUpAmount(Date(2015, 3, 14), Schedule, LoanTerms));
            Assert.Equal( 6222.41, Calculator.GetCatchUpAmount(Date(2015, 3, 15), Schedule, LoanTerms));
            Assert.Equal( 6222.41, Calculator.GetCatchUpAmount(Date(2015, 4, 14), Schedule, LoanTerms));
            Assert.Equal( 8333.35, Calculator.GetCatchUpAmount(Date(2015, 4, 15), Schedule, LoanTerms));
            Assert.Equal( 8333.35, Calculator.GetCatchUpAmount(Date(2015, 5, 14), Schedule, LoanTerms));
            Assert.Equal(10462.96, Calculator.GetCatchUpAmount(Date(2015, 5, 15), Schedule, LoanTerms));
        }

        [Fact]
        public void Missed2ndInstallment()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | Scheduled | Completed |       10000.00 |       2055.90 |    92.59 |   1963.31 |       8036.69
                2015-02-15      | 2015-02-17 | Scheduled | Completed |        8036.69 |          0.00 |     0.00 |      0.00 |       8036.69
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled |        8036.69 |       2055.90 |   148.83 |   1907.07 |       6129.62
                2015-04-15      | 2015-04-17 | Scheduled | Scheduled |        6129.62 |       2055.90 |    56.76 |   1999.14 |       4130.48
                2015-05-15      | 2015-05-17 | Scheduled | Scheduled |        4130.48 |       4168.72 |    38.24 |   4130.48 |          0.00");

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 01, 15) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 02, 14) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 02, 15) }
            })
            .Returns(167.00);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 03, 14) }
            })
            .Returns(167.00);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 03, 15) }
            })
            .Returns(241.42);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 04, 14) }
            })
            .Returns(241.42);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 04, 15) }
            })
            .Returns(315.83);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 05, 14) }
            })
            .Returns(315.83);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 05, 15) }
            })
            .Returns(390.24);

            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015, 1, 14), Schedule, LoanTerms));
            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015, 1, 15), Schedule, LoanTerms));
            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015, 2, 14), Schedule, LoanTerms));
            Assert.Equal(2055.90, Calculator.GetCatchUpAmount(Date(2015, 2, 15), Schedule, LoanTerms));
            Assert.Equal(2055.90, Calculator.GetCatchUpAmount(Date(2015, 3, 14), Schedule, LoanTerms));
            Assert.Equal(4130.15, Calculator.GetCatchUpAmount(Date(2015, 3, 15), Schedule, LoanTerms));
            Assert.Equal(4130.15, Calculator.GetCatchUpAmount(Date(2015, 4, 14), Schedule, LoanTerms));
            Assert.Equal(6222.91, Calculator.GetCatchUpAmount(Date(2015, 4, 15), Schedule, LoanTerms));
            Assert.Equal(6222.91, Calculator.GetCatchUpAmount(Date(2015, 5, 14), Schedule, LoanTerms));
            Assert.Equal(8334.34, Calculator.GetCatchUpAmount(Date(2015, 5, 15), Schedule, LoanTerms));
        }

        [Fact]
        public void Missed2ndInstallmentFollowedByExtraPayment()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | Scheduled  | Completed |       10000.00 |       2055.90 |    92.59 |   1963.31 |       8036.69
                2015-02-15      | 2015-02-17 | Scheduled  | Completed |        8036.69 |          0.00 |     0.00 |      0.00 |       8036.69
                                | 2015-03-01 | Additional | Completed |        8036.69 |        199.19 |     0.00 |    199.19 |       7837.50
                2015-03-15      | 2015-03-17 | Scheduled  | Scheduled |        7837.50 |       2055.90 |   147.97 |   1907.93 |       5929.57
                2015-04-15      | 2015-04-17 | Scheduled  | Scheduled |        5929.57 |       2055.90 |    54.90 |   2001.00 |       3928.57
                2015-05-15      | 2015-05-17 | Scheduled  | Scheduled |        3928.57 |       3964.95 |    36.38 |   3928.57 |          0.00");

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 01, 15) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 02, 14) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 02, 15) }
            })
            .Returns(167.00);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 02, 28) }
            })
            .Returns(167.00);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 03, 01) }
            })
            .Returns(167.00);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 03, 14) }
            })
            .Returns(167.00);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  7837.50, Start = Date(2015, 03, 01), End = Date(2015, 03, 15) }
            })
            .Returns(240.56);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  7837.50, Start = Date(2015, 03, 01), End = Date(2015, 04, 14) }
            })
            .Returns(240.56);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  7837.50, Start = Date(2015, 03, 01), End = Date(2015, 04, 15) }
            })
            .Returns(313.13);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  7837.50, Start = Date(2015, 03, 01), End = Date(2015, 05, 14) }
            })
            .Returns(313.13);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  7837.50, Start = Date(2015, 03, 01), End = Date(2015, 05, 15) }
            })
            .Returns(385.70);

            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015, 1, 14), Schedule, LoanTerms));
            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015, 1, 15), Schedule, LoanTerms));
            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015, 2, 14), Schedule, LoanTerms));
            Assert.Equal(2055.90, Calculator.GetCatchUpAmount(Date(2015, 2, 15), Schedule, LoanTerms));
            Assert.Equal(2055.90, Calculator.GetCatchUpAmount(Date(2015, 2, 28), Schedule, LoanTerms));
            Assert.Equal(1856.71, Calculator.GetCatchUpAmount(Date(2015, 3,  1), Schedule, LoanTerms));
            Assert.Equal(1856.71, Calculator.GetCatchUpAmount(Date(2015, 3, 14), Schedule, LoanTerms));
            Assert.Equal(3930.10, Calculator.GetCatchUpAmount(Date(2015, 3, 15), Schedule, LoanTerms));
            Assert.Equal(3930.10, Calculator.GetCatchUpAmount(Date(2015, 4, 14), Schedule, LoanTerms));
            Assert.Equal(6021.02, Calculator.GetCatchUpAmount(Date(2015, 4, 15), Schedule, LoanTerms));
            Assert.Equal(6021.02, Calculator.GetCatchUpAmount(Date(2015, 5, 14), Schedule, LoanTerms));
            Assert.Equal(8130.61, Calculator.GetCatchUpAmount(Date(2015, 5, 15), Schedule, LoanTerms));
        }

        [Fact]
        public void Missed2ndPaid3rd()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | Scheduled | Completed |       10000.00 |       2055.90 |    92.59 |   1963.31 |       8036.69
                2015-02-15      | 2015-02-17 | Scheduled | Completed |        8036.69 |          0.00 |     0.00 |      0.00 |       8036.69
                2015-03-15      | 2015-03-17 | Scheduled | Completed |        8036.69 |       2055.90 |   148.83 |   1907.07 |       6129.62
                2015-04-15      | 2015-04-17 | Scheduled | Scheduled |        6129.62 |       2055.90 |    56.76 |   1999.14 |       4130.48
                2015-05-15      | 2015-05-17 | Scheduled | Scheduled |        4130.48 |       4168.72 |    38.24 |   4130.48 |          0.00");

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 01, 15) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 02, 14) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 02, 15) }
            })
            .Returns(167.00);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 03, 14) }
            })
            .Returns(167.00);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 03, 15) }
            })
            .Returns(241.42);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 04, 14) }
            })
            .Returns(241.42);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  6129.62, Start = Date(2015, 03, 15), End = Date(2015, 04, 15) }
            })
            .Returns(298.17);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  6129.62, Start = Date(2015, 03, 15), End = Date(2015, 05, 14) }
            })
            .Returns(298.17);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  6129.62, Start = Date(2015, 03, 15), End = Date(2015, 05, 15) }
            })
            .Returns(354.93);

            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015, 1, 14), Schedule, LoanTerms));
            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015, 1, 15), Schedule, LoanTerms));
            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015, 2, 14), Schedule, LoanTerms));
            Assert.Equal(2055.90, Calculator.GetCatchUpAmount(Date(2015, 2, 15), Schedule, LoanTerms));
            Assert.Equal(2055.90, Calculator.GetCatchUpAmount(Date(2015, 3, 14), Schedule, LoanTerms));
            Assert.Equal(2074.25, Calculator.GetCatchUpAmount(Date(2015, 3, 15), Schedule, LoanTerms));
            Assert.Equal(2074.25, Calculator.GetCatchUpAmount(Date(2015, 4, 14), Schedule, LoanTerms));
            Assert.Equal(4149.35, Calculator.GetCatchUpAmount(Date(2015, 4, 15), Schedule, LoanTerms));
            Assert.Equal(4149.35, Calculator.GetCatchUpAmount(Date(2015, 5, 14), Schedule, LoanTerms));
            Assert.Equal(6243.13, Calculator.GetCatchUpAmount(Date(2015, 5, 15), Schedule, LoanTerms));
        }

        [Fact]
        public void ScheduledExtraPaymentsDoNotAffectTheCatchUpAmount()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 |             | Scheduled  | Scheduled |       10000.00 |       2055.90 |    92.59 |   1963.31 |       8036.69
                                | 2015-01-26 | 2015-01-26  | Additional | Scheduled |        8036.69 |        101.01 |     0.00 |    101.01 |       7935.68
                2015-02-15      | 2015-02-17 |             | Scheduled  | Scheduled |        7935.68 |       2055.90 |    73.82 |   1982.08 |       5953.60
                                | 2015-02-28 | 2015-02-28  | Additional | Scheduled |        5953.60 |        102.02 |     0.00 |    102.02 |       5851.58
                2015-03-15      | 2015-03-17 |             | Scheduled  | Scheduled |        5851.58 |       2055.90 |    50.98 |   2004.92 |       3846.66
                2015-04-15      | 2015-04-17 |             | Scheduled  | Scheduled |        3846.66 |       2055.90 |    35.62 |   2020.28 |       1826.38
                2015-05-15      | 2015-05-17 |             | Scheduled  | Scheduled |        1826.38 |       1843.29 |    16.91 |   1826.38 |          0.00");

            OnGetSimpleInterest(principal: 10000.00, start: Date(2014, 12, 15), end: Date(2015, 01, 15)).Returns(92.59);
            OnGetSimpleInterest(principal: 10000.00, start: Date(2014, 12, 15), end: Date(2015, 02, 14)).Returns(92.59);
            OnGetSimpleInterest(principal: 10000.00, start: Date(2014, 12, 15), end: Date(2015, 02, 15)).Returns(185.18);
            OnGetSimpleInterest(principal: 10000.00, start: Date(2014, 12, 15), end: Date(2015, 03, 14)).Returns(185.18);
            OnGetSimpleInterest(principal: 10000.00, start: Date(2014, 12, 15), end: Date(2015, 03, 15)).Returns(277.78);
            OnGetSimpleInterest(principal: 10000.00, start: Date(2014, 12, 15), end: Date(2015, 04, 14)).Returns(277.78);
            OnGetSimpleInterest(principal: 10000.00, start: Date(2014, 12, 15), end: Date(2015, 04, 15)).Returns(370.37);
            OnGetSimpleInterest(principal: 10000.00, start: Date(2014, 12, 15), end: Date(2015, 05, 14)).Returns(370.37);
            OnGetSimpleInterest(principal: 10000.00, start: Date(2014, 12, 15), end: Date(2015, 05, 15)).Returns(462.96);

            Assert.Equal(    0.00, Calculator.GetCatchUpAmount(Date(2015, 1, 14), Schedule, LoanTerms));
            Assert.Equal( 2055.90, Calculator.GetCatchUpAmount(Date(2015, 1, 15), Schedule, LoanTerms));
            Assert.Equal( 2055.90, Calculator.GetCatchUpAmount(Date(2015, 2, 14), Schedule, LoanTerms));
            Assert.Equal( 4129.98, Calculator.GetCatchUpAmount(Date(2015, 2, 15), Schedule, LoanTerms));
            Assert.Equal( 4129.98, Calculator.GetCatchUpAmount(Date(2015, 3, 14), Schedule, LoanTerms));
            Assert.Equal( 6222.41, Calculator.GetCatchUpAmount(Date(2015, 3, 15), Schedule, LoanTerms));
            Assert.Equal( 6222.41, Calculator.GetCatchUpAmount(Date(2015, 4, 14), Schedule, LoanTerms));
            Assert.Equal( 8333.35, Calculator.GetCatchUpAmount(Date(2015, 4, 15), Schedule, LoanTerms));
            Assert.Equal( 8333.35, Calculator.GetCatchUpAmount(Date(2015, 5, 14), Schedule, LoanTerms));
            Assert.Equal(10462.96, Calculator.GetCatchUpAmount(Date(2015, 5, 15), Schedule, LoanTerms));
        }

        [Fact]
        public void EarlyPaymentFollowedByExtraPaymentFollowedByUnderpayment()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | PaymentDate | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | 2015-01-10  | Scheduled  | Completed |       10000.00 |       2055.90 |    77.16 |   1978.74 |       8021.26
                                | 2015-01-28 | 2015-01-28  | Additional | Completed |        8021.26 |        333.03 |     0.00 |    333.03 |       7688.23
                2015-02-15      | 2015-02-17 |             | Scheduled  | Completed |        7688.23 |       1001.01 |    84.90 |    916.11 |       6772.12
                2015-03-15      | 2015-03-17 |             | Scheduled  | Scheduled |        6772.12 |       2055.90 |    62.70 |   1993.20 |       4778.92
                2015-04-15      | 2015-04-17 |             | Scheduled  | Scheduled |        4778.92 |       2055.90 |    44.25 |   2011.65 |       2767.27
                2015-05-15      | 2015-05-17 |             | Scheduled  | Scheduled |        2767.27 |       2792.89 |    25.62 |   2767.27 |          0.00");

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 01, 10) }
            })
            .Returns(0.0);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 01, 15) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 01, 27) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 01, 28) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 02, 14) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8021.26, Start = Date(2015, 01, 10) },
                new InterestAccrualPeriod { Principal =  7688.23, Start = Date(2015, 01, 28), End = Date(2015, 02, 15) }
            })
            .Returns(162.06);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8021.26, Start = Date(2015, 01, 10) },
                new InterestAccrualPeriod { Principal =  7688.23, Start = Date(2015, 01, 28), End = Date(2015, 03, 14) }
            })
            .Returns(162.06);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8021.26, Start = Date(2015, 01, 10) },
                new InterestAccrualPeriod { Principal =  7688.23, Start = Date(2015, 01, 28) },
                new InterestAccrualPeriod { Principal =  6772.12, Start = Date(2015, 02, 15), End = Date(2015, 03, 15) }
            })
            .Returns(224.76);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8021.26, Start = Date(2015, 01, 10) },
                new InterestAccrualPeriod { Principal =  7688.23, Start = Date(2015, 01, 28) },
                new InterestAccrualPeriod { Principal =  6772.12, Start = Date(2015, 02, 15), End = Date(2015, 04, 14) }
            })
            .Returns(224.76);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8021.26, Start = Date(2015, 01, 10) },
                new InterestAccrualPeriod { Principal =  7688.23, Start = Date(2015, 01, 28) },
                new InterestAccrualPeriod { Principal =  6772.12, Start = Date(2015, 02, 15), End = Date(2015, 04, 15) }
            })
            .Returns(287.46);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8021.26, Start = Date(2015, 01, 10) },
                new InterestAccrualPeriod { Principal =  7688.23, Start = Date(2015, 01, 28) },
                new InterestAccrualPeriod { Principal =  6772.12, Start = Date(2015, 02, 15), End = Date(2015, 05, 14) }
            })
            .Returns(287.46);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8021.26, Start = Date(2015, 01, 10) },
                new InterestAccrualPeriod { Principal =  7688.23, Start = Date(2015, 01, 28) },
                new InterestAccrualPeriod { Principal =  6772.12, Start = Date(2015, 02, 15), End = Date(2015, 05, 15) }
            })
            .Returns(350.16);

            Assert.Equal(    0.00, Calculator.GetCatchUpAmount(Date(2015, 1,  9), Schedule, LoanTerms));
            Assert.Equal(    0.00, Calculator.GetCatchUpAmount(Date(2015, 1, 10), Schedule, LoanTerms));
            Assert.Equal(    0.00, Calculator.GetCatchUpAmount(Date(2015, 1, 15), Schedule, LoanTerms));
            Assert.Equal(    0.00, Calculator.GetCatchUpAmount(Date(2015, 1, 27), Schedule, LoanTerms));
            Assert.Equal(    0.00, Calculator.GetCatchUpAmount(Date(2015, 1, 28), Schedule, LoanTerms));
            Assert.Equal(    0.00, Calculator.GetCatchUpAmount(Date(2015, 2, 14), Schedule, LoanTerms));
            Assert.Equal(  716.92, Calculator.GetCatchUpAmount(Date(2015, 2, 15), Schedule, LoanTerms));
            Assert.Equal(  716.92, Calculator.GetCatchUpAmount(Date(2015, 3, 14), Schedule, LoanTerms));
            Assert.Equal( 2779.45, Calculator.GetCatchUpAmount(Date(2015, 3, 15), Schedule, LoanTerms));
            Assert.Equal( 2779.45, Calculator.GetCatchUpAmount(Date(2015, 4, 14), Schedule, LoanTerms));
            Assert.Equal( 4860.50, Calculator.GetCatchUpAmount(Date(2015, 4, 15), Schedule, LoanTerms));
            Assert.Equal( 4860.50, Calculator.GetCatchUpAmount(Date(2015, 5, 14), Schedule, LoanTerms));
            Assert.Equal( 6960.22, Calculator.GetCatchUpAmount(Date(2015, 5, 15), Schedule, LoanTerms));
        }

        [Fact]
        public void InsufficientInterestPayment()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type      | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | Scheduled | Completed |       10000.00 |       2055.90 |    92.59 |   1963.31 |       8036.69
                2015-02-15      | 2015-02-17 | Scheduled | Completed |        8036.69 |         33.03 |    33.03 |      0.00 |       8036.69
                2015-03-15      | 2015-03-17 | Scheduled | Scheduled |        8036.69 |       2055.90 |    74.41 |   1981.49 |       6055.20
                2015-04-15      | 2015-04-17 | Scheduled | Scheduled |        6055.20 |       2055.90 |    56.07 |   1999.83 |       4055.37
                2015-05-15      | 2015-05-17 | Scheduled | Scheduled |        4055.37 |       4092.92 |    37.55 |   4055.37 |          0.00");

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 01, 15) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 02, 14) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 02, 15) }
            })
            .Returns(167.00);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 03, 14) }
            })
            .Returns(167.00);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 03, 15) }
            })
            .Returns(241.42);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 04, 14) }
            })
            .Returns(241.42);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 04, 15) }
            })
            .Returns(315.83);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 05, 14) }
            })
            .Returns(315.83);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15), End = Date(2015, 05, 15) }
            })
            .Returns(390.24);

            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2014, 12, 14), Schedule, LoanTerms));
            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2014, 12, 15), Schedule, LoanTerms));
            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015,  1, 14), Schedule, LoanTerms));
            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015,  1, 15), Schedule, LoanTerms));
            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015,  2, 14), Schedule, LoanTerms));
            Assert.Equal(2022.87, Calculator.GetCatchUpAmount(Date(2015,  2, 15), Schedule, LoanTerms));
            Assert.Equal(2022.87, Calculator.GetCatchUpAmount(Date(2015,  3, 14), Schedule, LoanTerms));
            Assert.Equal(4097.12, Calculator.GetCatchUpAmount(Date(2015,  3, 15), Schedule, LoanTerms));
            Assert.Equal(4097.12, Calculator.GetCatchUpAmount(Date(2015,  4, 14), Schedule, LoanTerms));
            Assert.Equal(6189.88, Calculator.GetCatchUpAmount(Date(2015,  4, 15), Schedule, LoanTerms));
            Assert.Equal(6189.88, Calculator.GetCatchUpAmount(Date(2015,  5, 14), Schedule, LoanTerms));
            Assert.Equal(8301.31, Calculator.GetCatchUpAmount(Date(2015,  5, 15), Schedule, LoanTerms));
        }

        [Fact]
        public void TwoExtraPaymentsFollowedByMissedInstallment()
        {
            GivenThatScheduleIs(@"
                AnniversaryDate | DueDate    | Type       | Status    | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance
                2015-01-15      | 2015-01-17 | Scheduled  | Completed |       10000.00 |       2055.90 |    92.59 |   1963.31 |       8036.69
                                | 2015-01-23 | Additional | Completed |        8036.69 |        101.01 |     0.00 |    101.01 |       7935.68
                                | 2015-02-04 | Additional | Completed |        7935.68 |        202.02 |     0.00 |    202.02 |       7733.66
                2015-02-15      | 2015-02-17 | Scheduled  | Completed |        7733.66 |          0.00 |     0.00 |      0.00 |       7733.66
                2015-03-15      | 2015-03-17 | Scheduled  | Scheduled |        7733.66 |       2055.90 |   144.65 |   1911.25 |       5822.41
                2015-04-15      | 2015-04-17 | Scheduled  | Scheduled |        5822.41 |       2055.90 |    53.91 |   2001.99 |       3820.42
                2015-05-15      | 2015-05-17 | Scheduled  | Scheduled |        3820.42 |       3855.79 |    35.37 |   3820.42 |          0.00");

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 01, 15) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 01, 23) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 02, 04) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15), End = Date(2015, 02, 14) }
            })
            .Returns(92.59);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  7935.68, Start = Date(2015, 01, 23) },
                new InterestAccrualPeriod { Principal =  7733.66, Start = Date(2015, 02, 04), End = Date(2015, 02, 15) }
            })
            .Returns(165.63);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  7935.68, Start = Date(2015, 01, 23) },
                new InterestAccrualPeriod { Principal =  7733.66, Start = Date(2015, 02, 04), End = Date(2015, 03, 14) }
            })
            .Returns(165.63);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  7935.68, Start = Date(2015, 01, 23) },
                new InterestAccrualPeriod { Principal =  7733.66, Start = Date(2015, 02, 04), End = Date(2015, 03, 15) },
            })
            .Returns(237.24);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  7935.68, Start = Date(2015, 01, 23) },
                new InterestAccrualPeriod { Principal =  7733.66, Start = Date(2015, 02, 04), End = Date(2015, 04, 14) },
            })
            .Returns(237.24);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  7935.68, Start = Date(2015, 01, 23) },
                new InterestAccrualPeriod { Principal =  7733.66, Start = Date(2015, 02, 04), End = Date(2015, 04, 15) },
            })
            .Returns(308.85);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  7935.68, Start = Date(2015, 01, 23) },
                new InterestAccrualPeriod { Principal =  7733.66, Start = Date(2015, 02, 04), End = Date(2015, 05, 14) },
            })
            .Returns(308.85);

            OnGetSimpleInterest(new[]
            {
                new InterestAccrualPeriod { Principal = 10000.00, Start = Date(2014, 12, 15) },
                new InterestAccrualPeriod { Principal =  8036.69, Start = Date(2015, 01, 15) },
                new InterestAccrualPeriod { Principal =  7935.68, Start = Date(2015, 01, 23) },
                new InterestAccrualPeriod { Principal =  7733.66, Start = Date(2015, 02, 04), End = Date(2015, 05, 15) },
            })
            .Returns(380.45);

            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015,  1, 14), Schedule, LoanTerms));
            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015,  1, 15), Schedule, LoanTerms));
            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015,  1, 23), Schedule, LoanTerms));
            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015,  2,  4), Schedule, LoanTerms));
            Assert.Equal(   0.00, Calculator.GetCatchUpAmount(Date(2015,  2, 14), Schedule, LoanTerms));
            Assert.Equal(1751.50, Calculator.GetCatchUpAmount(Date(2015,  2, 15), Schedule, LoanTerms));
            Assert.Equal(1751.50, Calculator.GetCatchUpAmount(Date(2015,  3, 14), Schedule, LoanTerms));
            Assert.Equal(3822.94, Calculator.GetCatchUpAmount(Date(2015,  3, 15), Schedule, LoanTerms));
            Assert.Equal(3822.94, Calculator.GetCatchUpAmount(Date(2015,  4, 14), Schedule, LoanTerms));
            Assert.Equal(5912.90, Calculator.GetCatchUpAmount(Date(2015,  4, 15), Schedule, LoanTerms));
            Assert.Equal(5912.90, Calculator.GetCatchUpAmount(Date(2015,  5, 14), Schedule, LoanTerms));
            Assert.Equal(8021.52, Calculator.GetCatchUpAmount(Date(2015,  5, 15), Schedule, LoanTerms));
        }
    }
}
