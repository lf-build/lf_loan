﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Testing.Mongo;
using LendFoundry.Loans.TransactionLog.Data;
using LendFoundry.Loans.TransactionLog.Models;
using LendFoundry.Tenant.Client;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LendFoundry.Loans.TransactionLog.Repository.Tests
{
    public class TransactionLogRepositoryTest
    {
        private ITenantTime tenantTime = new UtcTenantTime();
        private Mock<ITenantService> tenantServiceMock = new Mock<ITenantService>();

        private TransactionLogRepository Repository(IMongoConfiguration repositoryConfig)
        {
            var serviceConfigMock = new Mock<ITransactionLogConfiguration>();
            serviceConfigMock.Setup(config => config.Transactions).Returns(new List<TransactionCode>());
            return new TransactionLogRepository(repositoryConfig, serviceConfigMock.Object, tenantTime, tenantServiceMock.Object);
        }

        public string TenantId
        {
            set
            {
                tenantServiceMock.Setup(tenantService => tenantService.Current).Returns(new TenantInfo
                {
                    Id = value
                });
            }
        }

        [MongoFact]
        public void SaveTransaction()
        {
            MongoTest.Run((config) =>
            {
                TenantId = "tenant307";

                var repository = Repository(config);
                var tx = new TransactionInfo
                {
                    TenantId = "tenant307",
                    LoanReference = "loan001",
                    Code = "124",
                    Description = "Interest and principal billing",
                    Date = new DateTimeOffset(2015, 12, 31, 1, 2, 3, 4, new TimeSpan(9, 0, 0)),
                    PaymentId = "pmt6093",
                    Amount = 100,
                    Notes = "Lorem ipsum",
                    Timestamp = new DateTimeOffset(DateTime.Now)
                };

                repository.Save(tx);

                Assert.False(string.IsNullOrWhiteSpace(tx.Id));
            });
        }

        [MongoFact]
        public void GetByLoanReference()
        {
            MongoTest.Run(config =>
            {
                var repository = Repository(config);

                TenantId = "tn01";
                repository.Save(new TransactionInfo { LoanReference = "ln01", PaymentId = "pmt1" });
                repository.Save(new TransactionInfo { LoanReference = "ln01", PaymentId = "pmt2" });
                repository.Save(new TransactionInfo { LoanReference = "ln02", PaymentId = "pmt3" });
                repository.Save(new TransactionInfo { LoanReference = "ln02", PaymentId = "pmt4" });

                TenantId = "tn02";
                repository.Save(new TransactionInfo { LoanReference = "ln01", PaymentId = "pmt5" });
                repository.Save(new TransactionInfo { LoanReference = "ln01", PaymentId = "pmt6" });
                repository.Save(new TransactionInfo { LoanReference = "ln02", PaymentId = "pmt7" });
                repository.Save(new TransactionInfo { LoanReference = "ln02", PaymentId = "pmt8" });


                TenantId = "tn01";
                var txs = repository.GetByLoanReference("ln01");
                Assert.NotNull(txs);
                Assert.Equal(2, txs.Count);
                Assert.True(txs.Any(tx => tx.PaymentId == "pmt1"));
                Assert.True(txs.Any(tx => tx.PaymentId == "pmt2"));

                txs = repository.GetByLoanReference("ln02");
                Assert.NotNull(txs);
                Assert.Equal(2, txs.Count);
                Assert.True(txs.Any(tx => tx.PaymentId == "pmt3"));
                Assert.True(txs.Any(tx => tx.PaymentId == "pmt4"));

                TenantId = "tn02";
                txs = repository.GetByLoanReference("ln01");
                Assert.NotNull(txs);
                Assert.Equal(2, txs.Count);
                Assert.True(txs.Any(tx => tx.PaymentId == "pmt5"));
                Assert.True(txs.Any(tx => tx.PaymentId == "pmt6"));

                txs = repository.GetByLoanReference("ln02");
                Assert.NotNull(txs);
                Assert.Equal(2, txs.Count);
                Assert.True(txs.Any(tx => tx.PaymentId == "pmt7"));
                Assert.True(txs.Any(tx => tx.PaymentId == "pmt8"));
            });
        }

        [MongoFact]
        public void GetByTransactionId()
        {
            MongoTest.Run((config) =>
            {
                var transactionDate = new DateTimeOffset(2015, 12, 31, 0, 0, 0, 0, new TimeSpan(7, 0, 0));
                var tx = new TransactionInfo
                {
                    LoanReference = "loan001",
                    Code = "124",
                    Description = "Interest and principal billing",
                    Date = transactionDate,
                    PaymentId = "pmt6093",
                    Amount = 100,
                    Notes = "Lorem ipsum",
                    Timestamp = new DateTimeOffset(DateTime.Now)
                };

                TenantId = "tenant307";

                var repository = Repository(config);
                repository.Save(tx);

                var recoveredTx = repository.GetByTransactionId("loan001", tx.Id);

                Assert.NotNull(recoveredTx);
                Assert.Equal("tenant307", recoveredTx.TenantId);
                Assert.Equal("loan001", recoveredTx.LoanReference);
                Assert.Equal("124", recoveredTx.Code);
                //Assert.Equal("Interest and principal billing", recoveredTx.Description);
                Assert.Equal(tenantTime.FromDate(transactionDate.DateTime), recoveredTx.Date);
                Assert.Equal("pmt6093", recoveredTx.PaymentId);
                Assert.Equal(100, recoveredTx.Amount);
                Assert.Equal("Lorem ipsum", recoveredTx.Notes);
            });
        }
    }
}
