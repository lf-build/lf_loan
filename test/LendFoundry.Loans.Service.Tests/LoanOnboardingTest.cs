﻿using System;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Amortization.Client;
using LendFoundry.Calendar.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Payment;
using LendFoundry.Loans.Service.Tests.Fake;
using LendFoundry.Loans.TransactionLog.Client;
using Moq;
using Xunit;
using LendFoundry.Loans.Investors;
using LendFoundry.Loans.Service.Summary;

namespace LendFoundry.Loans.Service.Tests
{
    public class LoanOnboardingTest
    {
        string expectedLoanReferenceNumber = "loan001";
        string expectedFundingSource = "wayne";
        string expectedLoanPurpose = "Home improvement";
        double expectedLoanAmount = 5000.0;
        int expectedLoanTerm = 36;
        double expectedRate = 8.24;
        RateType expectedRateType = RateType.Percent;

        // Fee #1
        string expectedFee1Name = "Origination fee";
        FeeType expectedFee1Type = FeeType.Fixed;
        double expectedFee1Amount = 190.0;

        // Fee #2
        string expectedFee2Name = "Some other fee";
        FeeType expectedFee2Type = FeeType.Percent;
        double expectedFee2Amount = 2.5;

        // Bank info
        string expectedBankRoutingNumber = "routing9051";
        string expectedBankAccountNumber = "account7310";
        BankAccountType expectedBankAccountType = BankAccountType.Checking;

        IBorrower borrower1 = new Borrower
        {
            ReferenceNumber = "borrower001",
            IsPrimary = true,
            FirstName = "John",
            MiddleInitial = "Z",
            LastName = "Doe",
            DateOfBirth = new DateTime(1977, 7, 20),
            SSN = "721-07-4426",
            Email = "john@gmail.com",
            Addresses = new[]
            {
                new Address
                {
                    IsPrimary = true,
                    Line1 = "196 25th Avenue",
                    City = "San Francisco",
                    State = "CA",
                    ZipCode = "94121"
                }
            },
            Phones = new[]
            {
                new Phone
                {
                    IsPrimary = true,
                    Type = PhoneType.Home,
                    Number = "65202108"
                }
            }
        };

        IBorrower borrower2 = new Borrower
        {
            ReferenceNumber = "borrower002",
            IsPrimary = false,
            FirstName = "Mary",
            MiddleInitial = "Y",
            LastName = "Jane",
            DateOfBirth = new DateTime(1980, 11, 30),
            SSN = "078-05-1120",
            Email = "mary@outlook.com",
            Addresses = new[]
            {
                new Address
                {
                    IsPrimary = true,
                    Line1 = "1062 Walnut St",
                    City = "Irvine",
                    State = "CA",
                    ZipCode = "92780"
                }
            },
            Phones = new[]
            {
                new Phone
                {
                    IsPrimary = true,
                    Type = PhoneType.Mobile,
                    Number = "2014074907"
                }
            }
        };

        private ILoanService loanService;
        private FakePaymentScheduleService scheduleService;
        private FakeLoanRepository loanRepository;
        private FakeBorrowerRepository borrowerRepository;
        private ITenantTime tenantTime;

        public LoanOnboardingTest()
        {
            loanRepository = new FakeLoanRepository();
            borrowerRepository = new FakeBorrowerRepository();
            scheduleService = new FakePaymentScheduleService();
            tenantTime = new UtcTenantTime();
            loanService = new LoanService(
                loanRepository,
                borrowerRepository,
                scheduleService,
                new FakeEventHub(),
                Mock.Of<ITransactionLogService>(),
                Mock.Of<IAmortizationService>(),
                tenantTime,
                Mock.Of<ILogger>(),
                Mock.Of<ICalendarService>(),
                InvestorServiceMock(),
                Mock.Of<ILoanEngineResolver>(),
                Mock.Of<ILoanSummaryBuilder>(),
                new LoanConfiguration()
            );
        }

        private static IInvestorService InvestorServiceMock()
        {
            var mock = new Mock<IInvestorService>();
            mock.Setup(s => s.Exists(It.IsAny<string>())).Returns(true);
            return mock.Object;
        }

        private ILoan Onboard(PaymentMethod paymentMethod = PaymentMethod.ACH)
        {
            IOnboardingRequest onboardingRequest = new OnboardingRequest
            {
                ReferenceNumber = expectedLoanReferenceNumber,
                FundingSource = expectedFundingSource,
                Purpose = expectedLoanPurpose,
                Terms = new LoanTerms
                {
                    LoanAmount = expectedLoanAmount,
                    Term = expectedLoanTerm,
                    Rate = expectedRate,
                    RateType = expectedRateType,
                    Fees = new List<Fee>
                    {
                        new Fee
                        {
                            Code = expectedFee1Name,
                            Type = expectedFee1Type,
                            Amount = expectedFee1Amount
                        },
                        new Fee
                        {
                            Code = expectedFee2Name,
                            Type = expectedFee2Type,
                            Amount = expectedFee2Amount
                        }
                    },
                    OriginationDate = new DateTimeOffset(2015, 10, 21, 0, 0, 0, new TimeSpan(-5, 0, 0)),
                    ApplicationDate = DateTimeOffset.Now,
                    FundedDate = DateTimeOffset.Now
                },
                PaymentMethod = paymentMethod,
                BankInfo = new BankAccount
                {
                    RoutingNumber = expectedBankRoutingNumber,
                    AccountNumber = expectedBankAccountNumber,
                    AccountType = expectedBankAccountType
                },
                Borrowers = new List<IBorrower>(),
                Investor = new LoanInvestor { Id = "inv001", LoanPurchaseDate = new DateTimeOffset(2015, 12, 31, 0, 0, 0, new TimeSpan(-5, 0, 0)) },
                Scores = new List<IScore> { new Score { Name = "FICO", InitialScore = "500", InitialDate = DateTimeOffset.Now } },
                Grade = "ABC",
                MonthlyIncome = 10000,
                PreCloseDti = 5,
                PostCloseDti = 9,
                HomeOwnership = "Owner"
            };

            onboardingRequest.Borrowers = new List<IBorrower> { borrower1, borrower2 };

            loanService.Onboard(onboardingRequest);

            return loanService.GetLoan(expectedLoanReferenceNumber);
        }

        [Fact]
        public void LoanIsNotNull()
        {
            var loan = Onboard();
            Assert.NotNull(loan);
        }

        [Fact]
        public void LoanReferenceNumber()
        {
            var loan = Onboard();
            Assert.Equal(expectedLoanReferenceNumber, loan.ReferenceNumber);
        }

        [Fact]
        public void FundingSource()
        {
            var loan = Onboard();
            Assert.Equal(expectedFundingSource, loan.FundingSource);
        }

        [Fact]
        public void LoanPurpose()
        {
            var loan = Onboard();
            Assert.Equal(expectedLoanPurpose, loan.Purpose);
        }

        [Fact]
        public void LoanTermsIsNotNull()
        {
            var loan = Onboard();
            ILoanTerms terms = loan.Terms;
            Assert.NotNull(terms);
        }

        [Fact]
        public void LoanOriginationDate()
        {
            var loan = Onboard();
            Assert.Equal(tenantTime.Create(2015, 10, 21), loan.Terms.OriginationDate);
        }

        [Fact]
        public void LoanAmount()
        {
            var loan = Onboard();
            Assert.Equal(expectedLoanAmount, loan.Terms.LoanAmount);
        }

        [Fact]
        public void LoanTerm()
        {
            var loan = Onboard();
            Assert.Equal(expectedLoanTerm, loan.Terms.Term);
        }

        [Fact]
        public void LoanRate()
        {
            var loan = Onboard();
            Assert.Equal(expectedRate, loan.Terms.Rate);
        }

        [Fact]
        public void LoanRateType()
        {
            var loan = Onboard();
            Assert.Equal(expectedRateType, loan.Terms.RateType);
        }

        [Fact]
        public void LoanPaymentMethod()
        {
            var loan = Onboard(PaymentMethod.Check);
            Assert.Equal(PaymentMethod.Check, loan.PaymentMethod);
        }

        [Fact]
        public void LoanHasTwoFees()
        {
            var loan = Onboard();
            var fees = loan.Terms.Fees;
            Assert.NotNull(fees);
            Assert.Equal(2, fees.Count());
        }

        [Fact]
        public void LoanIsInsertedWithTheBorrowers()
        {
            var loan = Onboard();

            var borrowers = loan.Borrowers;
            Assert.NotNull(borrowers);
            Assert.Equal(2, borrowers.Count());
            Assert.Same(borrower1, borrowers.First());
            Assert.Same(borrower2, borrowers.Last());
        }

        [Fact]
        public void Fee1()
        {
            var loan = Onboard();
            IFee fee1 = loan.Terms.Fees.First();
            Assert.NotNull(fee1);
            Assert.Equal(expectedFee1Name, fee1.Code);
            Assert.Equal(expectedFee1Type, fee1.Type);
            Assert.Equal(expectedFee1Amount, fee1.Amount);
        }

        [Fact]
        public void Fee2()
        {
            var loan = Onboard();
            IFee fee2 = loan.Terms.Fees.ElementAt(1);
            Assert.NotNull(fee2);
            Assert.Equal(expectedFee2Name, fee2.Code);
            Assert.Equal(expectedFee2Type, fee2.Type);
            Assert.Equal(expectedFee2Amount, fee2.Amount);
        }

        [Fact]
        public void BankInfo()
        {
            ILoan loan = Onboard(PaymentMethod.ACH);
            Assert.NotNull(loan.BankAccounts);
            Assert.Equal(1, loan.BankAccounts.Count());

            IBankAccount bankAccount = loan.BankAccounts.First();
            Assert.NotNull(bankAccount);
            Assert.Equal(expectedBankRoutingNumber, bankAccount.RoutingNumber);
            Assert.Equal(expectedBankAccountNumber, bankAccount.AccountNumber);
            Assert.Equal(expectedBankAccountType, bankAccount.AccountType);
        }

        [Fact]
        public void KeepBankInfoEvenWhenPaymentMethodIsCheck()
        {
            ILoan loan = Onboard(PaymentMethod.Check);
            Assert.NotNull(loan.BankAccounts);
            Assert.Equal(1, loan.BankAccounts.Count());

            IBankAccount bankAccount = loan.BankAccounts.First();
            Assert.NotNull(bankAccount);
            Assert.Equal(expectedBankRoutingNumber, bankAccount.RoutingNumber);
            Assert.Equal(expectedBankAccountNumber, bankAccount.AccountNumber);
            Assert.Equal(expectedBankAccountType, bankAccount.AccountType);
        }

        [Fact]
        public void Investor()
        {
            var loan = Onboard();
            Assert.NotNull(loan.Investor);
            Assert.Equal("inv001", loan.Investor.Id);
            Assert.Equal(tenantTime.Create(2015, 12, 31), loan.Investor.LoanPurchaseDate);
        }

        [Fact]
        public void TheBorrowersAreAddedToTheRegistry()
        {
            var loan = Onboard();
            var borrowers = new List<IBorrower>(borrowerRepository.All(loan.ReferenceNumber));
            Assert.NotNull(borrowers);
            Assert.Equal(2, borrowers.Count);
            Assert.Same(borrower1, borrowers[0]);
            Assert.Same(borrower2, borrowers[1]);
        }

        [Fact]
        public void PaymentScheduleIsCreated()
        {
            var loan = Onboard();
            var schedule = scheduleService.Get(expectedLoanReferenceNumber);
            Assert.NotNull(schedule);
            Assert.Equal(expectedLoanReferenceNumber, schedule.LoanReferenceNumber);
        }
    }
}
