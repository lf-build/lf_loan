﻿using LendFoundry.Loans.Service.Summary;
using Xunit;

namespace LendFoundry.Loans.Service.Tests
{
    public class AverageDailyBalanceFormulaTest : FormulaTestBase
    {
        private IAverageDailyBalanceFunction AverageDailyBalance { get; }

        public AverageDailyBalanceFormulaTest()
        {
            AverageDailyBalance = new AverageDailyBalanceFunction(TenantTime);
        }

        [Fact]
        public void MonthBeforeOriginationAndNoPaymentsMade()
        {
            var loan = Loan().Of(10000).OriginatedOn(Date(2015, 12, 10)).Object;

            var schedule = Schedule(
                Installment().Open().AnniversaryOn(Date(2016, 01, 10)).DueOn(Date(2016, 01, 11)).EndingBalance(8032.63).Object,
                Installment().Open().AnniversaryOn(Date(2016, 02, 10)).DueOn(Date(2016, 02, 12)).EndingBalance(6049.08).Object,
                Installment().Open().AnniversaryOn(Date(2016, 03, 10)).DueOn(Date(2016, 03, 13)).EndingBalance(4049.21).Object,
                Installment().Open().AnniversaryOn(Date(2016, 04, 10)).DueOn(Date(2016, 04, 12)).EndingBalance(2032.89).Object,
                Installment().Open().AnniversaryOn(Date(2016, 05, 10)).DueOn(Date(2016, 05, 11)).EndingBalance(   0.00).Object
            );

            Assert.Equal(0.0, AverageDailyBalance.Apply(Date(2015, 11, 30), loan.Terms, schedule));
        }

        [Fact]
        public void SameMonthAsOriginationAndNoPaymentsMade()
        {
            var loan = Loan().Of(10000).OriginatedOn(Date(2015, 12, 10)).Object;

            var schedule = Schedule(
                Installment().Open().AnniversaryOn(Date(2016, 01, 10)).DueOn(Date(2016, 01, 11)).EndingBalance(8032.63).Object,
                Installment().Open().AnniversaryOn(Date(2016, 02, 10)).DueOn(Date(2016, 02, 12)).EndingBalance(6049.08).Object,
                Installment().Open().AnniversaryOn(Date(2016, 03, 10)).DueOn(Date(2016, 03, 13)).EndingBalance(4049.21).Object,
                Installment().Open().AnniversaryOn(Date(2016, 04, 10)).DueOn(Date(2016, 04, 12)).EndingBalance(2032.89).Object,
                Installment().Open().AnniversaryOn(Date(2016, 05, 10)).DueOn(Date(2016, 05, 11)).EndingBalance(   0.00).Object
            );

            Assert.Equal(7000.0, AverageDailyBalance.Apply(Date(2015, 12, 31), loan.Terms, schedule));
        }

        [Fact]
        public void SameMonthAsOriginationOnDecember1st()
        {
            var loan = Loan().Of(10000).OriginatedOn(Date(2015, 12, 1)).Object;

            var schedule = Schedule(
                Installment().Open().AnniversaryOn(Date(2016, 01, 01)).DueOn(Date(2016, 01, 11)).EndingBalance(8032.63).Object,
                Installment().Open().AnniversaryOn(Date(2016, 02, 01)).DueOn(Date(2016, 02, 12)).EndingBalance(6049.08).Object,
                Installment().Open().AnniversaryOn(Date(2016, 03, 01)).DueOn(Date(2016, 03, 13)).EndingBalance(4049.21).Object,
                Installment().Open().AnniversaryOn(Date(2016, 04, 01)).DueOn(Date(2016, 04, 12)).EndingBalance(2032.89).Object,
                Installment().Open().AnniversaryOn(Date(2016, 05, 01)).DueOn(Date(2016, 05, 11)).EndingBalance(   0.00).Object
            );

            Assert.Equal(10000.0, AverageDailyBalance.Apply(Date(2015, 12, 31), loan.Terms, schedule));
        }

        [Fact]
        public void SameMonthAsOriginationOnNovember30th()
        {
            var loan = Loan().Of(10000).OriginatedOn(Date(2015, 11, 30)).Object;

            var schedule = Schedule(
                Installment().Open().AnniversaryOn(Date(2015, 12, 31)).DueOn(Date(2015, 12, 31)).EndingBalance(8745.15).Object,
                Installment().Open().AnniversaryOn(Date(2016, 01, 31)).DueOn(Date(2016, 01, 31)).EndingBalance(8032.63).Object,
                Installment().Open().AnniversaryOn(Date(2016, 02, 29)).DueOn(Date(2016, 02, 29)).EndingBalance(6049.08).Object,
                Installment().Open().AnniversaryOn(Date(2016, 03, 31)).DueOn(Date(2016, 03, 31)).EndingBalance(4049.21).Object,
                Installment().Open().AnniversaryOn(Date(2016, 04, 30)).DueOn(Date(2016, 04, 30)).EndingBalance(2032.89).Object
            );

            Assert.Equal(Round(10000.0 / 30), AverageDailyBalance.Apply(Date(2015, 11, 30), loan.Terms, schedule));
        }

        [Fact]
        public void SameMonthAsOriginationOnFebruary28th()
        {
            var loan = Loan().Of(10000).OriginatedOn(Date(2015, 2, 28)).Object;

            var schedule = Schedule(
                Installment().Open().AnniversaryOn(Date(2015, 03, 31)).DueOn(Date(2015, 03, 31)).EndingBalance(8745.15).Object,
                Installment().Open().AnniversaryOn(Date(2015, 04, 30)).DueOn(Date(2015, 04, 30)).EndingBalance(8032.63).Object,
                Installment().Open().AnniversaryOn(Date(2015, 05, 31)).DueOn(Date(2015, 05, 31)).EndingBalance(6049.08).Object,
                Installment().Open().AnniversaryOn(Date(2015, 06, 30)).DueOn(Date(2015, 06, 30)).EndingBalance(4049.21).Object,
                Installment().Open().AnniversaryOn(Date(2015, 07, 31)).DueOn(Date(2015, 07, 31)).EndingBalance(2032.89).Object
            );

            Assert.Equal(Round(10000.0 / 30 * 3), AverageDailyBalance.Apply(Date(2015, 2, 28), loan.Terms, schedule));
        }

        [Fact]
        public void OneMonthAfterOriginationAndNoPaymentsMade()
        {
            var loan = Loan().Of(10000).OriginatedOn(Date(2015, 12, 10)).Object;

            var schedule = Schedule(
                Installment().Open().AnniversaryOn(Date(2016, 01, 10)).DueOn(Date(2016, 01, 11)).EndingBalance(8032.63).Object,
                Installment().Open().AnniversaryOn(Date(2016, 02, 10)).DueOn(Date(2016, 02, 12)).EndingBalance(6049.08).Object,
                Installment().Open().AnniversaryOn(Date(2016, 03, 10)).DueOn(Date(2016, 03, 13)).EndingBalance(4049.21).Object,
                Installment().Open().AnniversaryOn(Date(2016, 04, 10)).DueOn(Date(2016, 04, 12)).EndingBalance(2032.89).Object,
                Installment().Open().AnniversaryOn(Date(2016, 05, 10)).DueOn(Date(2016, 05, 11)).EndingBalance(   0.00).Object
            );

            Assert.Equal(loan.Terms.LoanAmount, AverageDailyBalance.Apply(Date(2016, 1, 31), loan.Terms, schedule));
        }

        [Fact]
        public void TwoMonthsAfterOriginationAndNoPaymentsMade()
        {
            var loan = Loan().Of(10000).OriginatedOn(Date(2015, 12, 10)).Object;

            var schedule = Schedule(
                Installment().Open().AnniversaryOn(Date(2016, 01, 10)).DueOn(Date(2016, 01, 11)).EndingBalance(8032.63).Object,
                Installment().Open().AnniversaryOn(Date(2016, 02, 10)).DueOn(Date(2016, 02, 12)).EndingBalance(6049.08).Object,
                Installment().Open().AnniversaryOn(Date(2016, 03, 10)).DueOn(Date(2016, 03, 13)).EndingBalance(4049.21).Object,
                Installment().Open().AnniversaryOn(Date(2016, 04, 10)).DueOn(Date(2016, 04, 12)).EndingBalance(2032.89).Object,
                Installment().Open().AnniversaryOn(Date(2016, 05, 10)).DueOn(Date(2016, 05, 11)).EndingBalance(   0.00).Object
            );

            Assert.Equal(loan.Terms.LoanAmount, AverageDailyBalance.Apply(Date(2016, 2, 29), loan.Terms, schedule));
        }

        [Fact]
        public void OneMonthAfterMaturityDateAndNoPaymentsMade()
        {
            var loan = Loan().Of(10000).OriginatedOn(Date(2015, 12, 10)).Object;

            var schedule = Schedule(
                Installment().Open().AnniversaryOn(Date(2016, 01, 10)).DueOn(Date(2016, 01, 11)).EndingBalance(8032.63).Object,
                Installment().Open().AnniversaryOn(Date(2016, 02, 10)).DueOn(Date(2016, 02, 12)).EndingBalance(6049.08).Object,
                Installment().Open().AnniversaryOn(Date(2016, 03, 10)).DueOn(Date(2016, 03, 13)).EndingBalance(4049.21).Object,
                Installment().Open().AnniversaryOn(Date(2016, 04, 10)).DueOn(Date(2016, 04, 12)).EndingBalance(2032.89).Object,
                Installment().Open().AnniversaryOn(Date(2016, 05, 10)).DueOn(Date(2016, 05, 11)).EndingBalance(   0.00).Object
            );

            Assert.Equal(loan.Terms.LoanAmount, AverageDailyBalance.Apply(Date(2016, 6, 30), loan.Terms, schedule));
        }

        [Fact]
        public void MonthBeforeOriginationAndOnePaymentMade()
        {
            var loan = Loan().Of(10000).OriginatedOn(Date(2015, 12, 10)).Object;

            var schedule = Schedule(
                Installment().Paid().EndingBalance(8032.63).AnniversaryOn(Date(2016, 01, 10)).DueOn(Date(2016, 01, 11)).PaidOn(Date(2016, 01, 12)).Object,
                Installment().Open().EndingBalance(6049.08).AnniversaryOn(Date(2016, 02, 10)).DueOn(Date(2016, 02, 12)).Object,
                Installment().Open().EndingBalance(4049.21).AnniversaryOn(Date(2016, 03, 10)).DueOn(Date(2016, 03, 13)).Object,
                Installment().Open().EndingBalance(2032.89).AnniversaryOn(Date(2016, 04, 10)).DueOn(Date(2016, 04, 12)).Object,
                Installment().Open().EndingBalance(   0.00).AnniversaryOn(Date(2016, 05, 10)).DueOn(Date(2016, 05, 11)).Object
            );

            Assert.Equal(0.0, AverageDailyBalance.Apply(Date(2015, 11, 30), loan.Terms, schedule));
        }

        [Fact]
        public void SameMonthAsOriginationDateAndOneExtraPaymentMade()
        {
            var loan = Loan().Of(10000).OriginatedOn(Date(2015, 12, 10)).Object;

            var schedule = Schedule(
                ExtraPayment().Paid().EndingBalance(9500.00).DueOn(Date(2015, 12, 21)).PaidOn(Date(2015, 12, 21)).Object,
                Installment ().Open().EndingBalance(7530.03).AnniversaryOn(Date(2016, 01, 10)).DueOn(Date(2016, 01, 11)).Object,
                Installment ().Open().EndingBalance(5542.34).AnniversaryOn(Date(2016, 02, 10)).DueOn(Date(2016, 02, 12)).Object,
                Installment ().Open().EndingBalance(3538.31).AnniversaryOn(Date(2016, 03, 10)).DueOn(Date(2016, 03, 13)).Object,
                Installment ().Open().EndingBalance(1517.79).AnniversaryOn(Date(2016, 04, 10)).DueOn(Date(2016, 04, 12)).Object,
                Installment ().Open().EndingBalance(   0.00).AnniversaryOn(Date(2016, 05, 10)).DueOn(Date(2016, 05, 11)).Object
            );

            Assert.Equal(6833.33, AverageDailyBalance.Apply(Date(2015, 12, 31), loan.Terms, schedule));
        }

        [Fact]
        public void OnePaymentPerMonth()
        {
            var loan = Loan().Of(10000).OriginatedOn(Date(2015, 12, 10)).Object;

            var schedule = Schedule(
                Installment().Paid().EndingBalance(8032.63).AnniversaryOn(Date(2016, 01, 10)).DueOn(Date(2016, 01, 11)).Object,
                Installment().Paid().EndingBalance(6049.08).AnniversaryOn(Date(2016, 02, 10)).DueOn(Date(2016, 02, 12)).Object,
                Installment().Paid().EndingBalance(4049.21).AnniversaryOn(Date(2016, 03, 10)).DueOn(Date(2016, 03, 13)).Object,
                Installment().Paid().EndingBalance(2032.89).AnniversaryOn(Date(2016, 04, 10)).DueOn(Date(2016, 04, 12)).Object,
                Installment().Paid().EndingBalance(   0.00).AnniversaryOn(Date(2016, 05, 10)).DueOn(Date(2016, 05, 11)).Object
            );

            Assert.Equal(   0.00, AverageDailyBalance.Apply(Date(2015, 11, 30), loan.Terms, schedule));
            Assert.Equal(7000.00, AverageDailyBalance.Apply(Date(2015, 12, 31), loan.Terms, schedule));
            Assert.Equal(8622.84, AverageDailyBalance.Apply(Date(2016,  1, 31), loan.Terms, schedule));
            Assert.Equal(6644.15, AverageDailyBalance.Apply(Date(2016,  2, 28), loan.Terms, schedule));
            Assert.Equal(4649.17, AverageDailyBalance.Apply(Date(2016,  3, 31), loan.Terms, schedule));
            Assert.Equal(2637.79, AverageDailyBalance.Apply(Date(2016,  4, 30), loan.Terms, schedule));
            Assert.Equal( 609.87, AverageDailyBalance.Apply(Date(2016,  5, 31), loan.Terms, schedule));
        }

        [Fact]
        public void TwoPaymentsSameMonth()
        {
            var loan = Loan().Of(10000).OriginatedOn(Date(2015, 12, 10)).Object;

            var schedule = Schedule(
                Installment ().Paid().EndingBalance(8032.63).AnniversaryOn(Date(2016, 01, 10)).DueOn(Date(2016, 01, 11)).PaidOn(Date(2016, 01, 11)).Object,
                Installment ().Paid().EndingBalance(6049.08).AnniversaryOn(Date(2016, 02, 10)).DueOn(Date(2016, 02, 12)).PaidOn(Date(2016, 02, 12)).Object,
                ExtraPayment().Paid().EndingBalance(5149.08)                                  .DueOn(Date(2016, 02, 22)).PaidOn(Date(2016, 02, 22)).Object,
                Installment ().Open().EndingBalance(3144.77).AnniversaryOn(Date(2016, 03, 10)).DueOn(Date(2016, 03, 13)).Object,
                Installment ().Open().EndingBalance(1121.02).AnniversaryOn(Date(2016, 04, 10)).DueOn(Date(2016, 04, 12)).Object,
                Installment ().Open().EndingBalance(   0.00).AnniversaryOn(Date(2016, 05, 10)).DueOn(Date(2016, 05, 11)).Object
            );

            Assert.Equal(6374.15, AverageDailyBalance.Apply(Date(2016, 2, 29), loan.Terms, schedule));
        }

        [Fact]
        public void ThreePaymentsSameMonth()
        {
            var loan = Loan().Of(10000).OriginatedOn(Date(2015, 12, 10)).Object;

            var schedule = Schedule(
                Installment ().Paid().EndingBalance(8032.63).AnniversaryOn(Date(2016, 01, 10)).DueOn(Date(2016, 01, 11)).PaidOn(Date(2016, 01, 11)).Object,
                Installment ().Paid().EndingBalance(6049.08).AnniversaryOn(Date(2016, 02, 10)).DueOn(Date(2016, 02, 12)).PaidOn(Date(2016, 02, 12)).Object,
                ExtraPayment().Paid().EndingBalance(5149.08)                                  .DueOn(Date(2016, 02, 14)).PaidOn(Date(2016, 02, 14)).Object,
                ExtraPayment().Paid().EndingBalance(4449.08)                                  .DueOn(Date(2016, 02, 21)).PaidOn(Date(2016, 02, 21)).Object,
                Installment ().Open().EndingBalance(2439.15).AnniversaryOn(Date(2016, 03, 10)).DueOn(Date(2016, 03, 13)).Object,
                Installment ().Open().EndingBalance( 409.59).AnniversaryOn(Date(2016, 04, 10)).DueOn(Date(2016, 04, 12)).Object,
                Installment ().Open().EndingBalance(   0.00).AnniversaryOn(Date(2016, 05, 10)).DueOn(Date(2016, 05, 11)).Object
            );

            Assert.Equal(5900.81, AverageDailyBalance.Apply(Date(2016, 2, 29), loan.Terms, schedule));
        }

        [Fact]
        public void ThreePaymentsSameMonth_NotFullPeriod()
        {
            var loan = Loan().Of(10000).OriginatedOn(Date(2015, 12, 10)).Object;

            var schedule = Schedule(
                Installment ().Paid().EndingBalance(8032.63).AnniversaryOn(Date(2016, 01, 10)).DueOn(Date(2016, 01, 11)).PaidOn(Date(2016, 01, 11)).Object,
                Installment ().Paid().EndingBalance(6049.08).AnniversaryOn(Date(2016, 02, 10)).DueOn(Date(2016, 02, 12)).PaidOn(Date(2016, 02, 12)).Object,
                ExtraPayment().Paid().EndingBalance(5149.08)                                  .DueOn(Date(2016, 02, 14)).PaidOn(Date(2016, 02, 14)).Object,
                ExtraPayment().Paid().EndingBalance(4449.08)                                  .DueOn(Date(2016, 02, 21)).PaidOn(Date(2016, 02, 21)).Object,
                Installment ().Open().EndingBalance(2439.15).AnniversaryOn(Date(2016, 03, 10)).DueOn(Date(2016, 03, 13)).Object,
                Installment ().Open().EndingBalance( 409.59).AnniversaryOn(Date(2016, 04, 10)).DueOn(Date(2016, 04, 12)).Object,
                Installment ().Open().EndingBalance(   0.00).AnniversaryOn(Date(2016, 05, 10)).DueOn(Date(2016, 05, 11)).Object
            );

            Assert.Equal(5307.60, AverageDailyBalance.Apply(Date(2016, 2, 26), loan.Terms, schedule, false));
        }

        [Fact]
        public void ExtraPaymentOnThe1st()
        {
            var loan = Loan().Of(10000).OriginatedOn(Date(2014, 10, 10)).Object;

            var schedule = Schedule(
                Installment ().Paid().EndingBalance(8650.26).AnniversaryOn(Date(2014, 11, 10)).DueOn(Date(2014, 11, 12)).PaidOn(Date(2014, 11, 12)).Object,
                ExtraPayment().Paid().EndingBalance(8218.07)                                  .DueOn(Date(2014, 12, 01)).PaidOn(Date(2014, 12, 01)).Object,
                Installment ().Paid().EndingBalance(7868.33).AnniversaryOn(Date(2014, 12, 10)).DueOn(Date(2014, 12, 12)).PaidOn(Date(2014, 12, 12)).Object,
                Installment ().Paid().EndingBalance(7518.59).AnniversaryOn(Date(2015, 01, 10)).DueOn(Date(2015, 01, 12)).Object,
                Installment ().Open().EndingBalance(7168.85).AnniversaryOn(Date(2015, 02, 10)).DueOn(Date(2015, 02, 12)).Object,
                Installment ().Open().EndingBalance(6819.11).AnniversaryOn(Date(2015, 03, 10)).DueOn(Date(2015, 03, 12)).Object,
                Installment ().Open().EndingBalance(6469.37).AnniversaryOn(Date(2015, 04, 10)).DueOn(Date(2015, 04, 12)).Object
            );

            Assert.Equal(7973.25, AverageDailyBalance.Apply(Date(2014, 12, 31), loan.Terms, schedule));
        }

        [Fact]
        public void ExtraPaymentOnThe31st()
        {
            var loan = Loan().Of(25000).OriginatedOn(Date(2014, 10, 10)).Object;

            var schedule = Schedule(
                Installment ().Paid().EndingBalance(20919.30).AnniversaryOn(Date(2014, 11, 10)).DueOn(Date(2014, 11, 12)).PaidOn(Date(2014, 11, 12)).Object,
                Installment ().Paid().EndingBalance(16804.60).AnniversaryOn(Date(2014, 12, 10)).DueOn(Date(2014, 12, 12)).PaidOn(Date(2014, 12, 12)).Object,
                Installment ().Paid().EndingBalance(12655.61).AnniversaryOn(Date(2015, 01, 10)).DueOn(Date(2015, 01, 12)).PaidOn(Date(2015, 01, 12)).Object,
                ExtraPayment().Paid().EndingBalance(10653.59)                                  .DueOn(Date(2015, 01, 31)).PaidOn(Date(2015, 01, 31)).Object,
                Installment ().Paid().EndingBalance( 6464.46).AnniversaryOn(Date(2015, 02, 10)).DueOn(Date(2015, 02, 12)).PaidOn(Date(2015, 02, 12)).Object,
                Installment ().Paid().EndingBalance( 2229.30).AnniversaryOn(Date(2015, 03, 10)).DueOn(Date(2015, 03, 12)).PaidOn(Date(2015, 03, 12)).Object,
                Installment ().Paid().EndingBalance(    0.00).AnniversaryOn(Date(2015, 04, 10)).DueOn(Date(2015, 04, 12)).PaidOn(Date(2015, 04, 12)).Object
            );

            Assert.Equal(    0.00, AverageDailyBalance.Apply(Date(2015,  9, 30), loan.Terms, schedule));
            Assert.Equal(17500.00, AverageDailyBalance.Apply(Date(2014, 10, 31), loan.Terms, schedule));
            Assert.Equal(22143.51, AverageDailyBalance.Apply(Date(2014, 11, 30), loan.Terms, schedule));
            Assert.Equal(18039.01, AverageDailyBalance.Apply(Date(2014, 12, 31), loan.Terms, schedule));
            Assert.Equal(13833.57, AverageDailyBalance.Apply(Date(2015,  1, 31), loan.Terms, schedule));
            Assert.Equal( 7721.20, AverageDailyBalance.Apply(Date(2015,  2, 28), loan.Terms, schedule));
            Assert.Equal( 3499.85, AverageDailyBalance.Apply(Date(2015,  3, 31), loan.Terms, schedule));
            Assert.Equal(  668.79, AverageDailyBalance.Apply(Date(2015,  4, 30), loan.Terms, schedule));
            Assert.Equal(    0.00, AverageDailyBalance.Apply(Date(2015,  5, 31), loan.Terms, schedule));
        }

        [Fact]
        public void ExtraPaymentOnFebruary28th()
        {
            var loan = Loan().Of(10000).OriginatedOn(Date(2014, 12, 10)).Object;

            var schedule = Schedule(
                Installment ().Paid().EndingBalance(8415.69).AnniversaryOn(Date(2015, 01, 10)).DueOn(Date(2015, 01, 12)).PaidOn(Date(2015, 01, 12)).Object,
                Installment ().Paid().EndingBalance(8065.95).AnniversaryOn(Date(2015, 02, 10)).DueOn(Date(2015, 02, 12)).PaidOn(Date(2015, 02, 12)).Object,
                ExtraPayment().Paid().EndingBalance(7633.76)                                  .DueOn(Date(2015, 02, 28)).PaidOn(Date(2015, 02, 28)).Object,
                Installment ().Open().EndingBalance(7284.02).AnniversaryOn(Date(2015, 03, 10)).DueOn(Date(2015, 03, 12)).Object,
                Installment ().Open().EndingBalance(6934.28).AnniversaryOn(Date(2015, 04, 10)).DueOn(Date(2015, 04, 12)).Object,
                Installment ().Open().EndingBalance(6584.54).AnniversaryOn(Date(2015, 05, 10)).DueOn(Date(2015, 05, 12)).Object,
                Installment ().Open().EndingBalance(6234.80).AnniversaryOn(Date(2015, 06, 10)).DueOn(Date(2015, 06, 12)).Object
            );

            Assert.Equal(8127.65, AverageDailyBalance.Apply(Date(2015, 2, 28), loan.Terms, schedule));
        }

        [Fact]
        public void ExtraPaymentOnNovember30th()
        {
            var loan = Loan().Of(10000).OriginatedOn(Date(2014, 9, 10)).Object;

            var schedule = Schedule(
                Installment ().Paid().EndingBalance(9526.80).AnniversaryOn(Date(2014, 10, 10)).DueOn(Date(2014, 10, 12)).PaidOn(Date(2014, 10, 12)).Object,
                Installment ().Paid().EndingBalance(9177.06).AnniversaryOn(Date(2014, 11, 10)).DueOn(Date(2014, 11, 12)).PaidOn(Date(2014, 11, 12)).Object,
                ExtraPayment().Paid().EndingBalance(8744.87)                                  .DueOn(Date(2014, 11, 30)).PaidOn(Date(2014, 11, 30)).Object,
                Installment ().Open().EndingBalance(8395.13).AnniversaryOn(Date(2014, 12, 10)).DueOn(Date(2014, 12, 12)).Object,
                Installment ().Open().EndingBalance(8045.39).AnniversaryOn(Date(2015, 01, 10)).DueOn(Date(2015, 01, 12)).Object,
                Installment ().Open().EndingBalance(7695.65).AnniversaryOn(Date(2015, 02, 10)).DueOn(Date(2015, 02, 12)).Object,
                Installment ().Open().EndingBalance(7345.91).AnniversaryOn(Date(2015, 03, 10)).DueOn(Date(2015, 03, 12)).Object
            );

            Assert.Equal(9267.58, AverageDailyBalance.Apply(Date(2014, 11, 30), loan.Terms, schedule));
        }

        [Fact]
        public void ExtraPaymentOnDecember30th()
        {
            var loan = Loan().Of(10000).OriginatedOn(Date(2014, 10, 10)).Object;

            var schedule = Schedule(
                Installment ().Paid().EndingBalance(8650.26).AnniversaryOn(Date(2014, 11, 10)).DueOn(Date(2014, 11, 12)).PaidOn(Date(2014, 11, 12)).Object,
                Installment ().Paid().EndingBalance(8300.52).AnniversaryOn(Date(2014, 12, 10)).DueOn(Date(2014, 12, 12)).PaidOn(Date(2014, 12, 12)).Object,
                ExtraPayment().Paid().EndingBalance(7868.33)                                  .DueOn(Date(2014, 12, 30)).PaidOn(Date(2014, 12, 30)).Object,
                Installment ().Open().EndingBalance(7518.59).AnniversaryOn(Date(2015, 01, 10)).DueOn(Date(2015, 01, 12)).Object,
                Installment ().Open().EndingBalance(7168.85).AnniversaryOn(Date(2015, 02, 10)).DueOn(Date(2015, 02, 12)).Object,
                Installment ().Open().EndingBalance(6819.11).AnniversaryOn(Date(2015, 03, 10)).DueOn(Date(2015, 03, 12)).Object,
                Installment ().Open().EndingBalance(6469.37).AnniversaryOn(Date(2015, 04, 10)).DueOn(Date(2015, 04, 12)).Object
            );

            Assert.Equal(8391.04, AverageDailyBalance.Apply(Date(2014, 12, 31), loan.Terms, schedule));
        }

        [Fact]
        public void OriginationOnThe31stShouldConsiderAllInstallmentsOnThe30th()
        {
            var loan = Loan().Of(25000).OriginatedOn(Date(2014, 12, 31)).Object;

            var schedule = Schedule(
                Installment().Paid().EndingBalance(20919.30).AnniversaryOn(Date(2015, 01, 31)).DueOn(Date(2015, 02, 02)).PaidOn(Date(2015, 02, 02)).Object,
                Installment().Paid().EndingBalance(16804.60).AnniversaryOn(Date(2015, 02, 28)).DueOn(Date(2015, 03, 02)).PaidOn(Date(2015, 03, 02)).Object,
                Installment().Paid().EndingBalance(12655.61).AnniversaryOn(Date(2015, 03, 31)).DueOn(Date(2015, 04, 02)).PaidOn(Date(2015, 04, 02)).Object,
                Installment().Paid().EndingBalance( 8472.04).AnniversaryOn(Date(2015, 04, 30)).DueOn(Date(2015, 05, 02)).PaidOn(Date(2015, 05, 02)).Object,
                Installment().Paid().EndingBalance( 4253.61).AnniversaryOn(Date(2015, 05, 31)).DueOn(Date(2015, 06, 02)).PaidOn(Date(2015, 06, 02)).Object,
                Installment().Paid().EndingBalance(    0.00).AnniversaryOn(Date(2015, 06, 30)).DueOn(Date(2015, 07, 02)).PaidOn(Date(2015, 07, 02)).Object
            );

            Assert.Equal(    0.00, AverageDailyBalance.Apply(Date(2014, 11, 30), loan.Terms, schedule));
            Assert.Equal(  833.33, AverageDailyBalance.Apply(Date(2014, 12, 31), loan.Terms, schedule));
            Assert.Equal(24863.98, AverageDailyBalance.Apply(Date(2015,  1, 31), loan.Terms, schedule));
            Assert.Equal(20782.14, AverageDailyBalance.Apply(Date(2015,  2, 28), loan.Terms, schedule));
            Assert.Equal(16666.30, AverageDailyBalance.Apply(Date(2015,  3, 31), loan.Terms, schedule));
            Assert.Equal(12516.16, AverageDailyBalance.Apply(Date(2015,  4, 30), loan.Terms, schedule));
            Assert.Equal( 8331.43, AverageDailyBalance.Apply(Date(2015,  5, 31), loan.Terms, schedule));
            Assert.Equal( 4111.82, AverageDailyBalance.Apply(Date(2015,  6, 30), loan.Terms, schedule));
            Assert.Equal(    0.00, AverageDailyBalance.Apply(Date(2014,  7, 31), loan.Terms, schedule));
        }

        [Fact]
        public void OriginationOnThe30thShouldConsiderFebruaryInstallmentsOnThe30th()
        {
            var loan = Loan().Of(25000).OriginatedOn(Date(2014, 12, 30)).Object;

            var schedule = Schedule(
                Installment().Paid().EndingBalance(20919.30).AnniversaryOn(Date(2015, 01, 30)).DueOn(Date(2015, 02, 01)).PaidOn(Date(2015, 02, 01)).Object,
                Installment().Paid().EndingBalance(16804.60).AnniversaryOn(Date(2015, 02, 28)).DueOn(Date(2015, 03, 02)).PaidOn(Date(2015, 03, 02)).Object,
                Installment().Paid().EndingBalance(12655.61).AnniversaryOn(Date(2015, 03, 30)).DueOn(Date(2015, 04, 01)).PaidOn(Date(2015, 04, 01)).Object,
                Installment().Paid().EndingBalance( 8472.04).AnniversaryOn(Date(2015, 04, 30)).DueOn(Date(2015, 05, 02)).PaidOn(Date(2015, 05, 02)).Object,
                Installment().Paid().EndingBalance( 4253.61).AnniversaryOn(Date(2015, 05, 30)).DueOn(Date(2015, 06, 01)).PaidOn(Date(2015, 06, 01)).Object,
                Installment().Paid().EndingBalance(    0.00).AnniversaryOn(Date(2015, 06, 30)).DueOn(Date(2015, 07, 02)).PaidOn(Date(2015, 07, 02)).Object
            );

            Assert.Equal(    0.00, AverageDailyBalance.Apply(Date(2014, 11, 30), loan.Terms, schedule));
            Assert.Equal(  833.33, AverageDailyBalance.Apply(Date(2014, 12, 31), loan.Terms, schedule));
            Assert.Equal(24863.98, AverageDailyBalance.Apply(Date(2015,  1, 31), loan.Terms, schedule));
            Assert.Equal(20782.14, AverageDailyBalance.Apply(Date(2015,  2, 28), loan.Terms, schedule));
            Assert.Equal(16666.30, AverageDailyBalance.Apply(Date(2015,  3, 31), loan.Terms, schedule));
            Assert.Equal(12516.16, AverageDailyBalance.Apply(Date(2015,  4, 30), loan.Terms, schedule));
            Assert.Equal( 8331.43, AverageDailyBalance.Apply(Date(2015,  5, 31), loan.Terms, schedule));
            Assert.Equal( 4111.82, AverageDailyBalance.Apply(Date(2015,  6, 30), loan.Terms, schedule));
            Assert.Equal(    0.00, AverageDailyBalance.Apply(Date(2014,  7, 31), loan.Terms, schedule));
        }

        [Fact]
        public void OriginationOnThe29thShouldConsiderFebruaryInstallmentsOnThe29th()
        {
            var loan = Loan().Of(25000).OriginatedOn(Date(2014, 12, 29)).Object;

            var schedule = Schedule(
                Installment().Paid().EndingBalance(20919.30).AnniversaryOn(Date(2015, 01, 29)).DueOn(Date(2015, 01, 31)).PaidOn(Date(2015, 01, 31)).Object,
                Installment().Paid().EndingBalance(16804.60).AnniversaryOn(Date(2015, 02, 28)).DueOn(Date(2015, 03, 02)).PaidOn(Date(2015, 03, 02)).Object,
                Installment().Paid().EndingBalance(12655.61).AnniversaryOn(Date(2015, 03, 29)).DueOn(Date(2015, 03, 31)).PaidOn(Date(2015, 03, 31)).Object,
                Installment().Paid().EndingBalance( 8472.04).AnniversaryOn(Date(2015, 04, 29)).DueOn(Date(2015, 05, 01)).PaidOn(Date(2015, 05, 01)).Object,
                Installment().Paid().EndingBalance( 4253.61).AnniversaryOn(Date(2015, 05, 29)).DueOn(Date(2015, 05, 31)).PaidOn(Date(2015, 05, 31)).Object,
                Installment().Paid().EndingBalance(    0.00).AnniversaryOn(Date(2015, 06, 29)).DueOn(Date(2015, 07, 01)).PaidOn(Date(2015, 07, 01)).Object
            );

            Assert.Equal(    0.00, AverageDailyBalance.Apply(Date(2014, 11, 30), loan.Terms, schedule));
            Assert.Equal( 1666.67, AverageDailyBalance.Apply(Date(2014, 12, 31), loan.Terms, schedule));
            Assert.Equal(24727.95, AverageDailyBalance.Apply(Date(2015,  1, 31), loan.Terms, schedule));
            Assert.Equal(20644.99, AverageDailyBalance.Apply(Date(2015,  2, 28), loan.Terms, schedule));
            Assert.Equal(16528.00, AverageDailyBalance.Apply(Date(2015,  3, 31), loan.Terms, schedule));
            Assert.Equal(12376.71, AverageDailyBalance.Apply(Date(2015,  4, 30), loan.Terms, schedule));
            Assert.Equal( 8190.81, AverageDailyBalance.Apply(Date(2015,  5, 31), loan.Terms, schedule));
            Assert.Equal( 3970.04, AverageDailyBalance.Apply(Date(2015,  6, 30), loan.Terms, schedule));
            Assert.Equal(    0.00, AverageDailyBalance.Apply(Date(2014,  7, 31), loan.Terms, schedule));
        }

        [Fact]
        public void OriginationOnThe28th()
        {
            var loan = Loan().Of(25000).OriginatedOn(Date(2014, 12, 28)).Object;

            var schedule = Schedule(
                Installment().Paid().EndingBalance(20919.30).AnniversaryOn(Date(2015, 01, 28)).DueOn(Date(2015, 01, 30)).PaidOn(Date(2015, 01, 30)).Object,
                Installment().Paid().EndingBalance(16804.60).AnniversaryOn(Date(2015, 02, 28)).DueOn(Date(2015, 03, 02)).PaidOn(Date(2015, 03, 02)).Object,
                Installment().Paid().EndingBalance(12655.61).AnniversaryOn(Date(2015, 03, 28)).DueOn(Date(2015, 03, 30)).PaidOn(Date(2015, 03, 30)).Object,
                Installment().Paid().EndingBalance( 8472.04).AnniversaryOn(Date(2015, 04, 28)).DueOn(Date(2015, 04, 30)).PaidOn(Date(2015, 04, 30)).Object,
                Installment().Paid().EndingBalance( 4253.61).AnniversaryOn(Date(2015, 05, 28)).DueOn(Date(2015, 05, 30)).PaidOn(Date(2015, 05, 30)).Object,
                Installment().Paid().EndingBalance(    0.00).AnniversaryOn(Date(2015, 06, 28)).DueOn(Date(2015, 06, 30)).PaidOn(Date(2015, 06, 30)).Object
            );

            Assert.Equal(    0.00, AverageDailyBalance.Apply(Date(2014, 11, 30), loan.Terms, schedule));
            Assert.Equal( 2500.00, AverageDailyBalance.Apply(Date(2014, 12, 31), loan.Terms, schedule));
            Assert.Equal(24591.93, AverageDailyBalance.Apply(Date(2015,  1, 31), loan.Terms, schedule));
            Assert.Equal(20507.83, AverageDailyBalance.Apply(Date(2015,  2, 28), loan.Terms, schedule));
            Assert.Equal(16389.70, AverageDailyBalance.Apply(Date(2015,  3, 31), loan.Terms, schedule));
            Assert.Equal(12237.25, AverageDailyBalance.Apply(Date(2015,  4, 30), loan.Terms, schedule));
            Assert.Equal( 8050.20, AverageDailyBalance.Apply(Date(2015,  5, 31), loan.Terms, schedule));
            Assert.Equal( 3828.25, AverageDailyBalance.Apply(Date(2015,  6, 30), loan.Terms, schedule));
            Assert.Equal(    0.00, AverageDailyBalance.Apply(Date(2014,  7, 31), loan.Terms, schedule));
        }

        [Fact]
        public void OriginationOnThe27th()
        {
            var loan = Loan().Of(25000).OriginatedOn(Date(2014, 12, 27)).Object;

            var schedule = Schedule(
                Installment().Paid().EndingBalance(20919.30).AnniversaryOn(Date(2015, 01, 27)).DueOn(Date(2015, 01, 29)).PaidOn(Date(2015, 01, 29)).Object,
                Installment().Paid().EndingBalance(16804.60).AnniversaryOn(Date(2015, 02, 27)).DueOn(Date(2015, 03, 01)).PaidOn(Date(2015, 03, 01)).Object,
                Installment().Paid().EndingBalance(12655.61).AnniversaryOn(Date(2015, 03, 27)).DueOn(Date(2015, 03, 29)).PaidOn(Date(2015, 03, 29)).Object,
                Installment().Paid().EndingBalance( 8472.04).AnniversaryOn(Date(2015, 04, 27)).DueOn(Date(2015, 04, 29)).PaidOn(Date(2015, 04, 29)).Object,
                Installment().Paid().EndingBalance( 4253.61).AnniversaryOn(Date(2015, 05, 27)).DueOn(Date(2015, 05, 29)).PaidOn(Date(2015, 05, 29)).Object,
                Installment().Paid().EndingBalance(    0.00).AnniversaryOn(Date(2015, 06, 27)).DueOn(Date(2015, 06, 29)).PaidOn(Date(2015, 06, 29)).Object
            );

            Assert.Equal(    0.00, AverageDailyBalance.Apply(Date(2014, 11, 30), loan.Terms, schedule));
            Assert.Equal( 3333.33, AverageDailyBalance.Apply(Date(2014, 12, 31), loan.Terms, schedule));
            Assert.Equal(24455.91, AverageDailyBalance.Apply(Date(2015,  1, 31), loan.Terms, schedule));
            Assert.Equal(20370.67, AverageDailyBalance.Apply(Date(2015,  2, 28), loan.Terms, schedule));
            Assert.Equal(16251.40, AverageDailyBalance.Apply(Date(2015,  3, 31), loan.Terms, schedule));
            Assert.Equal(12097.80, AverageDailyBalance.Apply(Date(2015,  4, 30), loan.Terms, schedule));
            Assert.Equal( 7909.58, AverageDailyBalance.Apply(Date(2015,  5, 31), loan.Terms, schedule));
            Assert.Equal( 3686.46, AverageDailyBalance.Apply(Date(2015,  6, 30), loan.Terms, schedule));
            Assert.Equal(    0.00, AverageDailyBalance.Apply(Date(2014,  7, 31), loan.Terms, schedule));
        }

        [Fact]
        public void MonthWithoutAnyPayments()
        {
            var loan = Loan().Of(10000).OriginatedOn(Date(2015, 12, 10)).Object;

            var schedule = Schedule(
                Installment().Paid().AnniversaryOn(Date(2016, 01, 10)).EndingBalance(8032.63).Object,
                Installment().Paid().AnniversaryOn(Date(2016, 02, 10)).EndingBalance(6049.08).Object,
                Installment().Open().AnniversaryOn(Date(2016, 03, 10)).EndingBalance(4049.21).Object,
                Installment().Open().AnniversaryOn(Date(2016, 04, 10)).EndingBalance(2032.89).Object,
                Installment().Open().AnniversaryOn(Date(2016, 05, 10)).EndingBalance(   0.00).Object
            );

            Assert.Equal(6049.08, AverageDailyBalance.Apply(Date(2016, 3, 31), loan.Terms, schedule));
            Assert.Equal(6049.08, AverageDailyBalance.Apply(Date(2016, 4, 30), loan.Terms, schedule));
            Assert.Equal(6049.08, AverageDailyBalance.Apply(Date(2016, 5, 31), loan.Terms, schedule));
            Assert.Equal(6049.08, AverageDailyBalance.Apply(Date(2016, 6, 30), loan.Terms, schedule));
        }

        [Fact]
        public void AfterPayoff()
        {
            var loan = Loan().Of(10000).OriginatedOn(Date(2015, 12, 10)).Object;

            var schedule = Schedule(
                Installment().Paid().AnniversaryOn(Date(2016, 01, 10)).EndingBalance(8032.63).Object,
                Installment().Paid().AnniversaryOn(Date(2016, 02, 10)).EndingBalance(6049.08).Object,
                Installment().Paid().AnniversaryOn(Date(2016, 03, 10)).EndingBalance(4049.21).Object,
                Installment().Paid().AnniversaryOn(Date(2016, 04, 10)).EndingBalance(2032.89).Object,
                Installment().Paid().AnniversaryOn(Date(2016, 05, 10)).EndingBalance(   0.00).Object
            );

            Assert.Equal(0.00, AverageDailyBalance.Apply(Date(2016, 6, 30), loan.Terms, schedule));
        }

        [Fact]
        public void ConsiderPaymentDateForEarlyPayment()
        {
            var loan = Loan().Of(1868.48).OriginatedOn(Date(2014, 12, 15)).Object;

            var schedule = Schedule(
                Installment().Paid().AnniversaryOn(Date(2015, 01, 15)).DueOn(Date(2015, 01, 17)).PaidOn(Date(2015, 01, 19)).EndingBalance(1420.38).Object,
                Installment().Paid().AnniversaryOn(Date(2015, 02, 15)).DueOn(Date(2015, 02, 17)).PaidOn(Date(2015, 02, 08)).EndingBalance( 950.63).Object,
                Installment().Paid().AnniversaryOn(Date(2015, 03, 15)).DueOn(Date(2015, 03, 17)).PaidOn(Date(2015, 03, 19)).EndingBalance( 483.20).Object,
                Installment().Paid().AnniversaryOn(Date(2015, 04, 15)).DueOn(Date(2015, 04, 17)).PaidOn(Date(2015, 04, 19)).EndingBalance(   0.00).Object
            );

            Assert.Equal(1060.24, AverageDailyBalance.Apply(Date(2015, 2, 15), loan.Terms, schedule));
        }

        [Fact]
        public void ConsiderPaymentDateForLatePayment()
        {
            var loan = Loan().Of(1868.48).OriginatedOn(Date(2014, 12, 15)).Object;

            var schedule = Schedule(
                Installment().Paid().AnniversaryOn(Date(2015, 01, 15)).DueOn(Date(2015, 01, 17)).PaidOn(Date(2015, 01, 19)).EndingBalance(1420.38).Object,
                Installment().Paid().AnniversaryOn(Date(2015, 02, 15)).DueOn(Date(2015, 02, 17)).PaidOn(Date(2015, 02, 28)).EndingBalance( 959.83).Object,
                Installment().Paid().AnniversaryOn(Date(2015, 03, 15)).DueOn(Date(2015, 03, 17)).PaidOn(Date(2015, 03, 19)).EndingBalance( 490.26).Object,
                Installment().Paid().AnniversaryOn(Date(2015, 04, 15)).DueOn(Date(2015, 04, 17)).PaidOn(Date(2015, 04, 19)).EndingBalance(   0.00).Object
            );

            Assert.Equal(1374.33, AverageDailyBalance.Apply(Date(2015, 2, 15), loan.Terms, schedule));
        }

        [Fact]
        public void ConsiderTheLowestEndingBalanceWhenMultiplePaymentsAreMadeOnTheSameDate()
        {
            var loan = Loan().Of(1868.48).OriginatedOn(Date(2014, 12, 15)).Object;

            var schedule = Schedule(
                Installment ().Paid().AnniversaryOn(Date(2015, 01, 15)).DueOn(Date(2015, 01, 17)).PaidOn(Date(2015, 01, 19)).EndingBalance(1420.38).Object,
                ExtraPayment().Paid()                                  .DueOn(Date(2015, 02, 15)).PaidOn(Date(2015, 02, 15)).EndingBalance( 889.83).Object,
                ExtraPayment().Paid()                                  .DueOn(Date(2015, 02, 15)).PaidOn(Date(2015, 02, 15)).EndingBalance( 809.83).Object,
                Installment ().Paid().AnniversaryOn(Date(2015, 02, 15)).DueOn(Date(2015, 02, 17)).PaidOn(Date(2015, 02, 19)).EndingBalance( 959.83).Object,
                Installment ().Paid().AnniversaryOn(Date(2015, 03, 15)).DueOn(Date(2015, 03, 17)).PaidOn(Date(2015, 03, 19)).EndingBalance( 332.33).Object,
                Installment ().Paid().AnniversaryOn(Date(2015, 04, 15)).DueOn(Date(2015, 04, 17)).PaidOn(Date(2015, 04, 19)).EndingBalance(   0.00).Object
            );

            Assert.Equal(1094.75, AverageDailyBalance.Apply(Date(2015, 2, 15), loan.Terms, schedule));
        }

        [Fact]
        public void DoNotRelyOnOrderOfInstallments()
        {
            var loan = Loan().Of(10000).OriginatedOn(Date(2015, 12, 10)).Object;

            var schedule = Schedule(
                Installment().Paid().EndingBalance(8032.63).AnniversaryOn(Date(2016, 01, 10)).DueOn(Date(2016, 01, 11)).Object,
                ExtraPayment().Paid().EndingBalance(7529.47).DueOn(Date(2016, 01, 25)).PaidOn(Date(2016, 01, 25)).Object,
                ExtraPayment().Paid().EndingBalance(7910.11).DueOn(Date(2016, 01, 25)).PaidOn(Date(2016, 01, 25)).Object,
                Installment().Paid().EndingBalance(6049.08).AnniversaryOn(Date(2016, 02, 10)).DueOn(Date(2016, 02, 12)).Object,
                Installment().Paid().EndingBalance(4049.21).AnniversaryOn(Date(2016, 03, 10)).DueOn(Date(2016, 03, 13)).Object,
                Installment().Paid().EndingBalance(2032.89).AnniversaryOn(Date(2016, 04, 10)).DueOn(Date(2016, 04, 12)).Object,
                Installment().Paid().EndingBalance(   0.00).AnniversaryOn(Date(2016, 05, 10)).DueOn(Date(2016, 05, 11)).Object
            );

            Assert.Equal(8522.21, AverageDailyBalance.Apply(Date(2016, 1, 31), loan.Terms, schedule));
            Assert.Equal(6493.20, AverageDailyBalance.Apply(Date(2016, 2, 29), loan.Terms, schedule));
        }
    }
}
