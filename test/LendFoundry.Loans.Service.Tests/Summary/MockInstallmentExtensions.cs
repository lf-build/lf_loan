﻿using Moq;
using System;

namespace LendFoundry.Loans.Service.Tests
{
    public static class MockInstallmentExtensions
    {
        public static Mock<IInstallment> Of(this Mock<IInstallment> mock, double paymentAmount)
        {
            mock.Setup(i => i.PaymentAmount).Returns(paymentAmount);
            return mock;
        }

        public static Mock<IInstallment> Type(this Mock<IInstallment> mock, InstallmentType value)
        {
            mock.Setup(i => i.Type).Returns(value);
            return mock;
        }

        public static Mock<IInstallment> AnniversaryOn(this Mock<IInstallment> mock, DateTimeOffset value)
        {
            mock.Setup(i => i.AnniversaryDate).Returns(value);
            return mock;
        }

        public static Mock<IInstallment> DueOn(this Mock<IInstallment> mock, DateTimeOffset value)
        {
            mock.Setup(i => i.DueDate).Returns(value);
            return mock;
        }

        public static Mock<IInstallment> EndingBalance(this Mock<IInstallment> mock, double value)
        {
            mock.Setup(i => i.EndingBalance).Returns(value);
            return mock;
        }

        public static Mock<IInstallment> PaidOn(this Mock<IInstallment> mock, DateTimeOffset value)
        {
            mock.Setup(i => i.PaymentDate).Returns(value);
            return mock;
        }

        public static Mock<IInstallment> Open(this Mock<IInstallment> mock)
        {
            mock.Setup(i => i.Status).Returns(InstallmentStatus.Scheduled);
            return mock;
        }

        public static Mock<IInstallment> Paid(this Mock<IInstallment> mock)
        {
            mock.Setup(i => i.Status).Returns(InstallmentStatus.Completed);
            return mock;
        }

        public static Mock<IInstallment> Principal(this Mock<IInstallment> mock, double value)
        {
            mock.Setup(i => i.Principal).Returns(value);
            return mock;
        }

        public static Mock<IInstallment> Interest(this Mock<IInstallment> mock, double value)
        {
            mock.Setup(i => i.Interest).Returns(value);
            return mock;
        }
    }
}
