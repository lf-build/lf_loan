﻿using LendFoundry.Loans.Service.Summary;
using Moq;
using System;
using Xunit;

namespace LendFoundry.Loans.Service.Tests
{
    public class InterestAccruedFormulaTest : ScheduledBasedFormulaTest
    {
        private IInterestAccruedFunction Formula { get; }
        private Mock<IAverageDailyBalanceFunction> AverageDailyBalanceFormula { get; } = new Mock<IAverageDailyBalanceFunction>();

        public InterestAccruedFormulaTest()
        {
            Formula = new InterestAccruedFunction(AverageDailyBalanceFormula.Object);
        }

        private ReturnValue<double> AverageDailyBalanceOn(DateTimeOffset referenceDate)
        {
            return new ReturnValue<double>((value) => AverageDailyBalanceFormula.Setup(f => f.Apply(referenceDate, Terms, Schedule, true)).Returns(value));
        }

        [Fact]
        public void NoPayment()
        {
            Terms.OriginationDate = TenantTime.Create(2014, 12, 22);
            Terms.LoanAmount = 25000.0;
            Terms.Rate = 10;

            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type      | Status   
                2015-01-22      | 2015-01-24 |             |       25000.00 |       4289.03 |   208.33 |   4080.70 |      20919.30 | Scheduled | Scheduled
                2015-02-22      | 2015-02-24 |             |       20919.30 |       4289.03 |   174.33 |   4114.70 |      16804.60 | Scheduled | Scheduled
                2015-03-22      | 2015-03-24 |             |       16804.60 |       4289.03 |   140.04 |   4148.99 |      12655.61 | Scheduled | Scheduled
                2015-04-22      | 2015-04-24 |             |       12655.61 |       4289.03 |   105.46 |   4183.57 |       8472.04 | Scheduled | Scheduled
                2015-05-22      | 2015-05-24 |             |        8472.04 |       4289.03 |    70.60 |   4218.43 |       4253.61 | Scheduled | Scheduled
                2015-06-22      | 2015-06-24 |             |        4253.61 |       4289.06 |    35.45 |   4253.61 |          0.00 | Scheduled | Scheduled
            ");

            AverageDailyBalanceOn(Date(2014, 12, 31)).Is( 7500.00);
            AverageDailyBalanceOn(Date(2015,  1, 31)).Is(25000.00);
            AverageDailyBalanceOn(Date(2015,  2, 28)).Is(25000.00);
            AverageDailyBalanceOn(Date(2015,  3, 31)).Is(25000.00);
            AverageDailyBalanceOn(Date(2015,  4, 30)).Is(25000.00);
            AverageDailyBalanceOn(Date(2015,  5, 31)).Is(25000.00);
            AverageDailyBalanceOn(Date(2015,  6, 30)).Is(25000.00);
            AverageDailyBalanceOn(Date(2015,  7, 31)).Is(25000.00);
            AverageDailyBalanceOn(Date(2015,  8, 31)).Is(25000.00);

            Assert.Equal(  0.00, Formula.Apply(Date(2014, 11, 30), Terms, Schedule));
            Assert.Equal( 62.50, Formula.Apply(Date(2014, 12, 31), Terms, Schedule));
            Assert.Equal(208.33, Formula.Apply(Date(2015,  1, 31), Terms, Schedule));
            Assert.Equal(208.33, Formula.Apply(Date(2015,  2, 28), Terms, Schedule));
            Assert.Equal(208.33, Formula.Apply(Date(2015,  3, 31), Terms, Schedule));
            Assert.Equal(208.33, Formula.Apply(Date(2015,  4, 30), Terms, Schedule));
            Assert.Equal(208.33, Formula.Apply(Date(2015,  5, 31), Terms, Schedule));
            Assert.Equal(208.33, Formula.Apply(Date(2015,  6, 30), Terms, Schedule));
            Assert.Equal(208.33, Formula.Apply(Date(2015,  7, 31), Terms, Schedule));
            Assert.Equal(208.33, Formula.Apply(Date(2015,  8, 31), Terms, Schedule));
        }

        [Fact]
        public void PerfectPay()
        {
            Terms.OriginationDate = TenantTime.Create(2014, 12, 22);
            Terms.LoanAmount = 25000.0;
            Terms.Rate = 10;

            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type      | Status   
                2015-01-22      | 2015-01-24 | 2015-01-24  |       25000.00 |       4289.03 |   208.33 |   4080.70 |      20919.30 | Scheduled | Completed
                2015-02-22      | 2015-02-24 | 2015-02-24  |       20919.30 |       4289.03 |   174.33 |   4114.70 |      16804.60 | Scheduled | Completed
                2015-03-22      | 2015-03-24 | 2015-03-24  |       16804.60 |       4289.03 |   140.04 |   4148.99 |      12655.61 | Scheduled | Completed
                2015-04-22      | 2015-04-24 | 2015-04-24  |       12655.61 |       4289.03 |   105.46 |   4183.57 |       8472.04 | Scheduled | Completed
                2015-05-22      | 2015-05-24 | 2015-05-24  |        8472.04 |       4289.03 |    70.60 |   4218.43 |       4253.61 | Scheduled | Completed
                2015-06-22      | 2015-06-24 | 2015-06-24  |        4253.61 |       4289.06 |    35.45 |   4253.61 |          0.00 | Scheduled | Completed
            ");

            AverageDailyBalanceOn(Date(2014, 12, 31)).Is( 7500.00);
            AverageDailyBalanceOn(Date(2015,  1, 31)).Is(23775.79);
            AverageDailyBalanceOn(Date(2015,  2, 28)).Is(19684.89);
            AverageDailyBalanceOn(Date(2015,  3, 31)).Is(15559.90);
            AverageDailyBalanceOn(Date(2015,  4, 30)).Is(11400.54);
            AverageDailyBalanceOn(Date(2015,  5, 31)).Is( 7206.51);
            AverageDailyBalanceOn(Date(2015,  6, 30)).Is( 2977.53);

            Assert.Equal(  0.00, Formula.Apply(Date(2014, 11, 30), Terms, Schedule));
            Assert.Equal( 62.50, Formula.Apply(Date(2014, 12, 31), Terms, Schedule));
            Assert.Equal(198.13, Formula.Apply(Date(2015,  1, 31), Terms, Schedule));
            Assert.Equal(164.04, Formula.Apply(Date(2015,  2, 28), Terms, Schedule));
            Assert.Equal(129.67, Formula.Apply(Date(2015,  3, 31), Terms, Schedule));
            Assert.Equal( 95.00, Formula.Apply(Date(2015,  4, 30), Terms, Schedule));
            Assert.Equal( 60.05, Formula.Apply(Date(2015,  5, 31), Terms, Schedule));
            Assert.Equal( 24.81, Formula.Apply(Date(2015,  6, 30), Terms, Schedule));
            Assert.Equal(  0.00, Formula.Apply(Date(2015,  7, 31), Terms, Schedule));
        }

        [Fact]
        public void FirstThreeInstallmentsPaid()
        {
            Terms.OriginationDate = TenantTime.Create(2014, 12, 22);
            Terms.LoanAmount = 25000.0;
            Terms.Rate = 10;

            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type      | Status   
                2015-01-22      | 2015-01-24 | 2015-01-24  |       25000.00 |       4289.03 |   208.33 |   4080.70 |      20919.30 | Scheduled | Completed
                2015-02-22      | 2015-02-24 | 2015-02-24  |       20919.30 |       4289.03 |   174.33 |   4114.70 |      16804.60 | Scheduled | Completed
                2015-03-22      | 2015-03-24 | 2015-03-24  |       16804.60 |       4289.03 |   140.04 |   4148.99 |      12655.61 | Scheduled | Completed
                2015-04-22      | 2015-04-24 |             |       12655.61 |       4289.03 |   105.46 |   4183.57 |       8472.04 | Scheduled | Scheduled
                2015-05-22      | 2015-05-24 |             |        8472.04 |       4289.03 |    70.60 |   4218.43 |       4253.61 | Scheduled | Scheduled
                2015-06-22      | 2015-06-24 |             |        4253.61 |       4289.06 |    35.45 |   4253.61 |          0.00 | Scheduled | Scheduled
            ");

            AverageDailyBalanceOn(Date(2014, 12, 31)).Is( 7500.00);
            AverageDailyBalanceOn(Date(2015,  1, 31)).Is(23775.79);
            AverageDailyBalanceOn(Date(2015,  2, 28)).Is(19684.89);
            AverageDailyBalanceOn(Date(2015,  3, 31)).Is(15559.90);
            AverageDailyBalanceOn(Date(2015,  4, 30)).Is(15559.90);
            AverageDailyBalanceOn(Date(2015,  5, 31)).Is(15559.90);
            AverageDailyBalanceOn(Date(2015,  6, 30)).Is(15559.90);
            AverageDailyBalanceOn(Date(2015,  7, 31)).Is(15559.90);
            AverageDailyBalanceOn(Date(2015,  8, 31)).Is(15559.90);

            Assert.Equal(  0.00, Formula.Apply(Date(2014, 11, 30), Terms, Schedule));
            Assert.Equal( 62.50, Formula.Apply(Date(2014, 12, 31), Terms, Schedule));
            Assert.Equal(198.13, Formula.Apply(Date(2015,  1, 31), Terms, Schedule));
            Assert.Equal(164.04, Formula.Apply(Date(2015,  2, 28), Terms, Schedule));
            Assert.Equal(129.67, Formula.Apply(Date(2015,  3, 31), Terms, Schedule));
            Assert.Equal(129.67, Formula.Apply(Date(2015,  4, 30), Terms, Schedule));
            Assert.Equal(129.67, Formula.Apply(Date(2015,  5, 31), Terms, Schedule));
            Assert.Equal(129.67, Formula.Apply(Date(2015,  6, 30), Terms, Schedule));
            Assert.Equal(129.67, Formula.Apply(Date(2015,  7, 31), Terms, Schedule));
            Assert.Equal(129.67, Formula.Apply(Date(2015,  8, 31), Terms, Schedule));
        }
    }
}
