﻿using Moq;
using System;

namespace LendFoundry.Loans.Service.Tests
{
    public static class MockLoanExtensions
    {
        public static Mock<ILoan> Of(this Mock<ILoan> mock, double amount)
        {
            GetTerms(mock).Setup(t => t.LoanAmount).Returns(amount);
            return mock;
        }

        public static Mock<ILoan> OriginatedOn(this Mock<ILoan> mock, DateTimeOffset date)
        {
            GetTerms(mock).Setup(t => t.OriginationDate).Returns(date);
            return mock;
        }

        private static Mock<ILoanTerms> GetTerms(Mock<ILoan> loan)
        {
            if (loan.Object.Terms != null)
                return Mock.Get(loan.Object.Terms);

            var terms = new Mock<ILoanTerms>();
            loan.Setup(l => l.Terms).Returns(terms.Object);
            return terms;
        }
    }
}
