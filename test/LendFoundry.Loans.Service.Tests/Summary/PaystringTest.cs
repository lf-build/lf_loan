﻿using LendFoundry.Foundation.Date;
using LendFoundry.Loans.Schedule.Calculators;
using LendFoundry.Loans.Service.Summary;
using Moq;
using System;
using Xunit;

namespace LendFoundry.Loans.Service.Tests
{
    public class PaystringTest
    {
        private const int GracePeriod = 10;

        public PaystringTest()
        {
            Paystring = new Paystring(GracePeriodCalculator.Object);

            GracePeriodCalculator
                .Setup(c => c.GetGracePeriod(It.IsAny<DateTimeOffset>()))
                .Returns((DateTimeOffset date) => new Interval(date, date.AddDays(GracePeriod)));

            GracePeriodCalculator
                .Setup(c => c.GetGracePeriod(It.IsAny<IInstallment>()))
                .Returns((IInstallment installment) => new Interval(installment.AnniversaryDate.Value, installment.DueDate.Value.AddDays(GracePeriod)));
        }

        private IPaystring Paystring { get; }
        private ITenantTime TenantTime { get; } = new UtcTenantTime();
        private Mock<IGracePeriodCalculator> GracePeriodCalculator { get; } = new Mock<IGracePeriodCalculator>();
        private DateTimeOffset Date(int year, int month, int day) => TenantTime.Create(year, month, day);

        [Fact]
        public void NoPayments()
        {
            var schedule = Mock.Of<IPaymentSchedule>(s => s.Installments == new[] {
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 01, 10) && i.DueDate == Date(2017, 01, 12) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Scheduled),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 02, 10) && i.DueDate == Date(2017, 02, 12) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Scheduled),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 03, 10) && i.DueDate == Date(2017, 03, 12) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Scheduled),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 04, 10) && i.DueDate == Date(2017, 04, 12) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Scheduled),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 05, 10) && i.DueDate == Date(2017, 05, 12) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Scheduled),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 06, 10) && i.DueDate == Date(2017, 06, 12) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Scheduled),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 07, 10) && i.DueDate == Date(2017, 07, 12) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Scheduled),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 08, 10) && i.DueDate == Date(2017, 08, 12) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Scheduled),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 09, 10) && i.DueDate == Date(2017, 09, 12) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Scheduled),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 10, 10) && i.DueDate == Date(2017, 10, 12) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Scheduled),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 11, 10) && i.DueDate == Date(2017, 11, 12) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Scheduled),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 12, 10) && i.DueDate == Date(2017, 12, 12) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Scheduled),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2018, 01, 10) && i.DueDate == Date(2018, 01, 12) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Scheduled),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2018, 02, 10) && i.DueDate == Date(2018, 02, 12) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Scheduled),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2018, 03, 10) && i.DueDate == Date(2018, 03, 12) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Scheduled),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2018, 04, 10) && i.DueDate == Date(2018, 04, 12) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Scheduled)
            });

            Assert.Equal("000000000000", Paystring.Build(Date(2016, 12, 10), schedule));

            Assert.Equal("000000000000", Paystring.Build(Date(2017, 01, 22), schedule));
            Assert.Equal("000000000010", Paystring.Build(Date(2017, 01, 23), schedule));

            Assert.Equal("000000000010", Paystring.Build(Date(2017, 02, 22), schedule));
            Assert.Equal("000000000120", Paystring.Build(Date(2017, 02, 23), schedule));

            Assert.Equal("000000000120", Paystring.Build(Date(2017, 03, 22), schedule));
            Assert.Equal("000000001230", Paystring.Build(Date(2017, 03, 23), schedule));

            Assert.Equal("000000001230", Paystring.Build(Date(2017, 04, 22), schedule));
            Assert.Equal("000000012340", Paystring.Build(Date(2017, 04, 23), schedule));

            Assert.Equal("000000012340", Paystring.Build(Date(2017, 05, 22), schedule));
            Assert.Equal("000000123450", Paystring.Build(Date(2017, 05, 23), schedule));

            Assert.Equal("000000123450", Paystring.Build(Date(2017, 06, 22), schedule));
            Assert.Equal("000001234560", Paystring.Build(Date(2017, 06, 23), schedule));

            Assert.Equal("000001234560", Paystring.Build(Date(2017, 07, 22), schedule));
            Assert.Equal("000012345670", Paystring.Build(Date(2017, 07, 23), schedule));

            Assert.Equal("000012345670", Paystring.Build(Date(2017, 08, 22), schedule));
            Assert.Equal("000123456780", Paystring.Build(Date(2017, 08, 23), schedule));

            Assert.Equal("000123456780", Paystring.Build(Date(2017, 09, 22), schedule));
            Assert.Equal("001234567890", Paystring.Build(Date(2017, 09, 23), schedule));

            Assert.Equal("001234567890", Paystring.Build(Date(2017, 10, 22), schedule));
            Assert.Equal("012345678990", Paystring.Build(Date(2017, 10, 23), schedule));

            Assert.Equal("012345678990", Paystring.Build(Date(2017, 11, 22), schedule));
            Assert.Equal("123456789990", Paystring.Build(Date(2017, 11, 23), schedule));

            Assert.Equal("123456789990", Paystring.Build(Date(2017, 12, 22), schedule));
            Assert.Equal("234567899990", Paystring.Build(Date(2017, 12, 23), schedule));

            Assert.Equal("234567899990", Paystring.Build(Date(2018, 01, 22), schedule));
            Assert.Equal("345678999990", Paystring.Build(Date(2018, 01, 23), schedule));

            Assert.Equal("345678999990", Paystring.Build(Date(2018, 02, 22), schedule));
            Assert.Equal("456789999990", Paystring.Build(Date(2018, 02, 23), schedule));

            Assert.Equal("456789999990", Paystring.Build(Date(2018, 03, 22), schedule));
            Assert.Equal("567899999990", Paystring.Build(Date(2018, 03, 23), schedule));

            Assert.Equal("567899999990", Paystring.Build(Date(2018, 04, 22), schedule));
            Assert.Equal("678999999990", Paystring.Build(Date(2018, 04, 23), schedule));

            Assert.Equal("678999999990", Paystring.Build(Date(2018, 05, 20), schedule));
            Assert.Equal("789999999990", Paystring.Build(Date(2018, 05, 21), schedule));

            Assert.Equal("789999999990", Paystring.Build(Date(2018, 06, 20), schedule));
            Assert.Equal("899999999990", Paystring.Build(Date(2018, 06, 21), schedule));
        }

        [Fact]
        public void PerfectPay()
        {
            var schedule = Mock.Of<IPaymentSchedule>(s => s.Installments == new[] {
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 01, 10) && i.DueDate == Date(2017, 01, 12) && i.PaymentDate == Date(2017, 01, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 02, 10) && i.DueDate == Date(2017, 02, 12) && i.PaymentDate == Date(2017, 02, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 03, 10) && i.DueDate == Date(2017, 03, 12) && i.PaymentDate == Date(2017, 03, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 04, 10) && i.DueDate == Date(2017, 04, 12) && i.PaymentDate == Date(2017, 04, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 05, 10) && i.DueDate == Date(2017, 05, 12) && i.PaymentDate == Date(2017, 05, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 06, 10) && i.DueDate == Date(2017, 06, 12) && i.PaymentDate == Date(2017, 06, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 07, 10) && i.DueDate == Date(2017, 07, 12) && i.PaymentDate == Date(2017, 07, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 08, 10) && i.DueDate == Date(2017, 08, 12) && i.PaymentDate == Date(2017, 08, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 09, 10) && i.DueDate == Date(2017, 09, 12) && i.PaymentDate == Date(2017, 09, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 10, 10) && i.DueDate == Date(2017, 10, 12) && i.PaymentDate == Date(2017, 10, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 11, 10) && i.DueDate == Date(2017, 11, 12) && i.PaymentDate == Date(2017, 11, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 12, 10) && i.DueDate == Date(2017, 12, 12) && i.PaymentDate == Date(2017, 12, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2018, 01, 10) && i.DueDate == Date(2018, 01, 12) && i.PaymentDate == Date(2018, 01, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2018, 02, 10) && i.DueDate == Date(2018, 02, 12) && i.PaymentDate == Date(2018, 02, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2018, 03, 10) && i.DueDate == Date(2018, 03, 12) && i.PaymentDate == Date(2018, 03, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2018, 04, 10) && i.DueDate == Date(2018, 04, 12) && i.PaymentDate == Date(2018, 04, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed)
            });
            
            Assert.Equal("000000000000", Paystring.Build(Date(2016, 12, 10), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 01, 22), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 01, 23), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 02, 22), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 02, 23), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 03, 22), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 03, 23), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 04, 22), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 04, 23), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 05, 22), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 05, 23), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 06, 22), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 06, 23), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 07, 22), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 07, 23), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 08, 22), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 08, 23), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 09, 22), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 09, 23), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 10, 22), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 10, 23), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 11, 22), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 11, 23), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 12, 22), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 12, 23), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2018, 01, 22), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2018, 01, 23), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2018, 02, 22), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2018, 02, 23), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2018, 03, 22), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2018, 03, 23), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2018, 04, 22), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2018, 04, 23), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2018, 05, 20), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2018, 05, 21), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2018, 06, 20), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2018, 06, 21), schedule));
        }

        [Fact]
        public void EarlyPayment()
        {
            var schedule = Mock.Of<IPaymentSchedule>(s => s.Installments == new[] {
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 01, 10) && i.DueDate == Date(2017, 01, 12) && i.PaymentDate == Date(2017, 01, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 02, 10) && i.DueDate == Date(2017, 02, 12) && i.PaymentDate == Date(2017, 02, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 03, 10) && i.DueDate == Date(2017, 03, 12) && i.PaymentDate == Date(2017, 03,  9) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 04, 10) && i.DueDate == Date(2017, 04, 12) && i.PaymentDate == Date(2017, 04, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 05, 10) && i.DueDate == Date(2017, 05, 12) && i.PaymentDate == Date(2017, 05, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 06, 10) && i.DueDate == Date(2017, 06, 12) && i.PaymentDate == Date(2017, 06, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 07, 10) && i.DueDate == Date(2017, 07, 12) && i.PaymentDate == Date(2017, 07, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 08, 10) && i.DueDate == Date(2017, 08, 12) && i.PaymentDate == Date(2017, 08, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 09, 10) && i.DueDate == Date(2017, 09, 12) && i.PaymentDate == Date(2017, 09, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 10, 10) && i.DueDate == Date(2017, 10, 12) && i.PaymentDate == Date(2017, 10, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 11, 10) && i.DueDate == Date(2017, 11, 12) && i.PaymentDate == Date(2017, 11, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 12, 10) && i.DueDate == Date(2017, 12, 12) && i.PaymentDate == Date(2017, 12, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2018, 01, 10) && i.DueDate == Date(2018, 01, 12) && i.PaymentDate == Date(2018, 01, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2018, 02, 10) && i.DueDate == Date(2018, 02, 12) && i.PaymentDate == Date(2018, 02, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2018, 03, 10) && i.DueDate == Date(2018, 03, 12) && i.PaymentDate == Date(2018, 03, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2018, 04, 10) && i.DueDate == Date(2018, 04, 12) && i.PaymentDate == Date(2018, 04, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed)
            });
            
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 03,  1), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 03, 10), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 03, 22), schedule));
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 03, 23), schedule));
        }

        [Fact]
        public void MissedTwoPayments()
        {
            var schedule = Mock.Of<IPaymentSchedule>(s => s.Installments == new[] {
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 01, 10) && i.DueDate == Date(2017, 01, 12) && i.PaymentDate == Date(2017, 03, 11) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 02, 10) && i.DueDate == Date(2017, 02, 12) && i.PaymentDate == Date(2017, 03, 12) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 03, 10) && i.DueDate == Date(2017, 03, 12) && i.PaymentDate == Date(2017, 03, 13) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 04, 10) && i.DueDate == Date(2017, 04, 12) && i.PaymentDate == Date(2017, 04, 22) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
            });
            
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 01, 22), schedule));
            Assert.Equal("000000000010", Paystring.Build(Date(2017, 01, 23), schedule));
            Assert.Equal("000000000010", Paystring.Build(Date(2017, 02, 22), schedule));
            Assert.Equal("000000000120", Paystring.Build(Date(2017, 02, 23), schedule));

            Assert.Equal("000000000120", Paystring.Build(Date(2017, 03, 10), schedule));
            Assert.Equal("000000000120", Paystring.Build(Date(2017, 03, 11), schedule));
            Assert.Equal("000000000120", Paystring.Build(Date(2017, 03, 12), schedule));
            Assert.Equal("000000000120", Paystring.Build(Date(2017, 03, 13), schedule));
            Assert.Equal("000000000120", Paystring.Build(Date(2017, 03, 22), schedule));
            Assert.Equal("000000001200", Paystring.Build(Date(2017, 03, 23), schedule));

            Assert.Equal("000000001200", Paystring.Build(Date(2017, 04, 22), schedule));
            Assert.Equal("000000012000", Paystring.Build(Date(2017, 04, 23), schedule));
        }

        [Fact]
        public void AllPaymentsMadeDuringNextGracePeriod()
        {
            var schedule = Mock.Of<IPaymentSchedule>(s => s.Installments == new[] {
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 01, 10) && i.DueDate == Date(2017, 01, 12) && i.PaymentDate == Date(2017, 02, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 02, 10) && i.DueDate == Date(2017, 02, 12) && i.PaymentDate == Date(2017, 03, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 03, 10) && i.DueDate == Date(2017, 03, 12) && i.PaymentDate == Date(2017, 04, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 04, 10) && i.DueDate == Date(2017, 04, 12) && i.PaymentDate == Date(2017, 05, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
            });
            
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 01, 22), schedule));
            Assert.Equal("000000000010", Paystring.Build(Date(2017, 01, 23), schedule));
            Assert.Equal("000000000010", Paystring.Build(Date(2017, 02, 22), schedule));
            Assert.Equal("000000000120", Paystring.Build(Date(2017, 02, 23), schedule));
            Assert.Equal("000000000120", Paystring.Build(Date(2017, 03, 22), schedule));
            Assert.Equal("000000001230", Paystring.Build(Date(2017, 03, 23), schedule));
            Assert.Equal("000000001230", Paystring.Build(Date(2017, 04, 22), schedule));
            Assert.Equal("000000012340", Paystring.Build(Date(2017, 04, 23), schedule));
        }
        
        [Fact]
        public void AllPaymentsMadeAfterNextGracePeriod()
        {
            var schedule = Mock.Of<IPaymentSchedule>(s => s.Installments == new[] {
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 01, 10) && i.DueDate == Date(2017, 01, 12) && i.PaymentDate == Date(2017, 02, 25) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 02, 10) && i.DueDate == Date(2017, 02, 12) && i.PaymentDate == Date(2017, 03, 25) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 03, 10) && i.DueDate == Date(2017, 03, 12) && i.PaymentDate == Date(2017, 04, 25) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 04, 10) && i.DueDate == Date(2017, 04, 12) && i.PaymentDate == Date(2017, 05, 25) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
            });
            
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 01, 22), schedule));
            Assert.Equal("000000000010", Paystring.Build(Date(2017, 01, 23), schedule));
            Assert.Equal("000000000010", Paystring.Build(Date(2017, 02, 22), schedule));
            Assert.Equal("000000000120", Paystring.Build(Date(2017, 02, 23), schedule));
            Assert.Equal("000000000120", Paystring.Build(Date(2017, 03, 22), schedule));
            Assert.Equal("000000001230", Paystring.Build(Date(2017, 03, 23), schedule));
            Assert.Equal("000000001230", Paystring.Build(Date(2017, 04, 22), schedule));
            Assert.Equal("000000012340", Paystring.Build(Date(2017, 04, 23), schedule));
        }
        
        [Fact]
        public void Missed1stAnd3rd()
        {
            var schedule = Mock.Of<IPaymentSchedule>(s => s.Installments == new[] {
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 01, 10) && i.DueDate == Date(2017, 01, 12) && i.PaymentDate == Date(2017, 02, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 02, 10) && i.DueDate == Date(2017, 02, 12) && i.PaymentDate == Date(2017, 02, 20) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 03, 10) && i.DueDate == Date(2017, 03, 12) && i.PaymentDate == Date(2017, 04, 15) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
                Mock.Of<IInstallment>(i => i.AnniversaryDate == Date(2017, 04, 10) && i.DueDate == Date(2017, 04, 12) && i.PaymentDate == Date(2017, 04, 20) && i.Type == InstallmentType.Scheduled && i.Status == InstallmentStatus.Completed),
            });
            
            Assert.Equal("000000000000", Paystring.Build(Date(2017, 01, 22), schedule));
            Assert.Equal("000000000010", Paystring.Build(Date(2017, 01, 23), schedule));
            Assert.Equal("000000000010", Paystring.Build(Date(2017, 02, 22), schedule));
            Assert.Equal("000000000100", Paystring.Build(Date(2017, 02, 23), schedule));
            Assert.Equal("000000000100", Paystring.Build(Date(2017, 03, 22), schedule));
            Assert.Equal("000000001010", Paystring.Build(Date(2017, 03, 23), schedule));
            Assert.Equal("000000001010", Paystring.Build(Date(2017, 04, 22), schedule));
            Assert.Equal("000000010100", Paystring.Build(Date(2017, 04, 23), schedule));
        }
    }
}
