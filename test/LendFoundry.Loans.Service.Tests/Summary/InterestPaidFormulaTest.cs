﻿using LendFoundry.Loans.Service.Summary;
using Moq;
using Xunit;

namespace LendFoundry.Loans.Service.Tests
{
    public class InterestPaidFormulaTest : ScheduledBasedFormulaTest
    {
        private IInterestPaidFunction Formula { get; }
        private Mock<IInstallment> Installment() => new Mock<IInstallment>();

        public InterestPaidFormulaTest()
        {
            Formula = new InterestPaidFunction(TenantTime);
        }

        [Fact]
        public void NoPayment()
        {
            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type      | Status   
                2015-01-22      | 2015-01-24 | 2015-01-24  |       25000.00 |       4289.03 |   208.33 |   4080.70 |      20919.30 | Scheduled | Scheduled
                2015-02-22      | 2015-02-24 | 2015-02-24  |       20919.30 |       4289.03 |   174.33 |   4114.70 |      16804.60 | Scheduled | Scheduled
                2015-03-22      | 2015-03-24 | 2015-03-24  |       16804.60 |       4289.03 |   140.04 |   4148.99 |      12655.61 | Scheduled | Scheduled
                2015-04-22      | 2015-04-24 | 2015-04-24  |       12655.61 |       4289.03 |   105.46 |   4183.57 |       8472.04 | Scheduled | Scheduled
                2015-05-22      | 2015-05-24 | 2015-05-24  |        8472.04 |       4289.03 |    70.60 |   4218.43 |       4253.61 | Scheduled | Scheduled
                2015-06-22      | 2015-06-24 | 2015-06-24  |        4253.61 |       4289.06 |    35.45 |   4253.61 |          0.00 | Scheduled | Scheduled
            ");

            Assert.Equal(0.00, Formula.Apply(Date(2014, 11, 30), Terms, Schedule));
            Assert.Equal(0.00, Formula.Apply(Date(2014, 12, 31), Terms, Schedule));
            Assert.Equal(0.00, Formula.Apply(Date(2015,  1, 31), Terms, Schedule));
            Assert.Equal(0.00, Formula.Apply(Date(2015,  2, 28), Terms, Schedule));
            Assert.Equal(0.00, Formula.Apply(Date(2015,  3, 31), Terms, Schedule));
            Assert.Equal(0.00, Formula.Apply(Date(2015,  4, 30), Terms, Schedule));
            Assert.Equal(0.00, Formula.Apply(Date(2015,  5, 31), Terms, Schedule));
            Assert.Equal(0.00, Formula.Apply(Date(2015,  6, 30), Terms, Schedule));
            Assert.Equal(0.00, Formula.Apply(Date(2015,  7, 31), Terms, Schedule));
        }

        [Fact]
        public void PerfectPay()
        {
            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type      | Status   
                2015-01-22      | 2015-01-24 | 2015-01-24  |       25000.00 |       4289.03 |   208.33 |   4080.70 |      20919.30 | Scheduled | Completed
                2015-02-22      | 2015-02-24 | 2015-02-24  |       20919.30 |       4289.03 |   174.33 |   4114.70 |      16804.60 | Scheduled | Completed
                2015-03-22      | 2015-03-24 | 2015-03-24  |       16804.60 |       4289.03 |   140.04 |   4148.99 |      12655.61 | Scheduled | Completed
                2015-04-22      | 2015-04-24 | 2015-04-24  |       12655.61 |       4289.03 |   105.46 |   4183.57 |       8472.04 | Scheduled | Completed
                2015-05-22      | 2015-05-24 | 2015-05-24  |        8472.04 |       4289.03 |    70.60 |   4218.43 |       4253.61 | Scheduled | Completed
                2015-06-22      | 2015-06-24 | 2015-06-24  |        4253.61 |       4289.06 |    35.45 |   4253.61 |          0.00 | Scheduled | Completed
            ");

            Assert.Equal(  0.00, Formula.Apply(Date(2014, 11, 30), Terms, Schedule));
            Assert.Equal(  0.00, Formula.Apply(Date(2014, 12, 31), Terms, Schedule));
            Assert.Equal(208.33, Formula.Apply(Date(2015,  1, 31), Terms, Schedule));
            Assert.Equal(174.33, Formula.Apply(Date(2015,  2, 28), Terms, Schedule));
            Assert.Equal(140.04, Formula.Apply(Date(2015,  3, 31), Terms, Schedule));
            Assert.Equal(105.46, Formula.Apply(Date(2015,  4, 30), Terms, Schedule));
            Assert.Equal( 70.60, Formula.Apply(Date(2015,  5, 31), Terms, Schedule));
            Assert.Equal( 35.45, Formula.Apply(Date(2015,  6, 30), Terms, Schedule));
            Assert.Equal(  0.00, Formula.Apply(Date(2015,  7, 31), Terms, Schedule));
        }

        [Fact]
        public void FirstThreeInstallmentsPaid()
        {
            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type      | Status   
                2015-01-22      | 2015-01-24 | 2015-01-24  |       25000.00 |       4289.03 |   208.33 |   4080.70 |      20919.30 | Scheduled | Completed
                2015-02-22      | 2015-02-24 | 2015-02-24  |       20919.30 |       4289.03 |   174.33 |   4114.70 |      16804.60 | Scheduled | Completed
                2015-03-22      | 2015-03-24 | 2015-03-24  |       16804.60 |       4289.03 |   140.04 |   4148.99 |      12655.61 | Scheduled | Completed
                2015-04-22      | 2015-04-24 |             |       12655.61 |       4289.03 |   105.46 |   4183.57 |       8472.04 | Scheduled | Scheduled
                2015-05-22      | 2015-05-24 |             |        8472.04 |       4289.03 |    70.60 |   4218.43 |       4253.61 | Scheduled | Scheduled
                2015-06-22      | 2015-06-24 |             |        4253.61 |       4289.06 |    35.45 |   4253.61 |          0.00 | Scheduled | Scheduled
            ");

            Assert.Equal(  0.00, Formula.Apply(Date(2014, 11, 30), Terms, Schedule));
            Assert.Equal(  0.00, Formula.Apply(Date(2014, 12, 31), Terms, Schedule));
            Assert.Equal(208.33, Formula.Apply(Date(2015,  1, 31), Terms, Schedule));
            Assert.Equal(174.33, Formula.Apply(Date(2015,  2, 28), Terms, Schedule));
            Assert.Equal(140.04, Formula.Apply(Date(2015,  3, 31), Terms, Schedule));
            Assert.Equal(  0.00, Formula.Apply(Date(2015,  4, 30), Terms, Schedule));
            Assert.Equal(  0.00, Formula.Apply(Date(2015,  5, 31), Terms, Schedule));
            Assert.Equal(  0.00, Formula.Apply(Date(2015,  6, 30), Terms, Schedule));
            Assert.Equal(  0.00, Formula.Apply(Date(2015,  7, 31), Terms, Schedule));
        }

        [Fact]
        public void ExtraPayments()
        {
            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type       | Status   
                2015-01-22      | 2015-01-24 | 2015-01-24  |       25000.00 |       4289.03 |   208.33 |   4080.70 |      20919.30 | Scheduled  | Completed
                2015-02-22      | 2015-02-24 | 2015-02-24  |       20919.30 |       4289.03 |   174.33 |   4114.70 |      16804.60 | Scheduled  | Completed
                                | 2015-03-09 | 2015-03-09  |       16804.60 |        999.00 |     0.00 |    999.00 |      15805.60 | Additional | Completed
                2015-03-22      | 2015-03-24 | 2015-03-24  |       15805.60 |       4289.03 |   136.43 |   4152.60 |      11653.00 | Scheduled  | Completed
                                | 2015-03-30 | 2015-03-30  |       11653.00 |        888.00 |     0.00 |    888.00 |      10765.00 | Additional | Completed
                2015-04-22      | 2015-04-24 | 2015-04-24  |       10765.00 |       4289.03 |    91.68 |   4197.35 |       6567.65 | Scheduled  | Completed
                2015-05-22      | 2015-05-24 | 2015-05-24  |        6567.65 |       4289.03 |    54.73 |   4234.30 |       2333.35 | Scheduled  | Completed
                2015-06-22      | 2015-06-24 | 2015-06-24  |        2333.35 |       2352.79 |    19.44 |   2333.35 |          0.00 | Scheduled  | Completed
            ");

            Assert.Equal(  0.00, Formula.Apply(Date(2014, 11, 30), Terms, Schedule));
            Assert.Equal(  0.00, Formula.Apply(Date(2014, 12, 31), Terms, Schedule));
            Assert.Equal(208.33, Formula.Apply(Date(2015,  1, 31), Terms, Schedule));
            Assert.Equal(174.33, Formula.Apply(Date(2015,  2, 28), Terms, Schedule));
            Assert.Equal(136.43, Formula.Apply(Date(2015,  3, 31), Terms, Schedule));
            Assert.Equal( 91.68, Formula.Apply(Date(2015,  4, 30), Terms, Schedule));
            Assert.Equal( 54.73, Formula.Apply(Date(2015,  5, 31), Terms, Schedule));
            Assert.Equal( 19.44, Formula.Apply(Date(2015,  6, 30), Terms, Schedule));
            Assert.Equal(  0.00, Formula.Apply(Date(2015,  7, 31), Terms, Schedule));
        }
        
        [Fact]
        public void ConsiderPaymentDatesOnly()
        {
            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type      | Status   
                2015-01-31      | 2015-01-31 | 2015-02-01  |       25000.00 |       4289.03 |   208.33 |   4080.70 |      20919.30 | Scheduled | Completed
                2015-02-28      | 2015-02-28 | 2015-02-28  |       20919.30 |       4289.03 |   174.33 |   4114.70 |      16804.60 | Scheduled | Completed
                2015-03-31      | 2015-04-02 | 2015-04-02  |       16804.60 |       4289.03 |   140.04 |   4148.99 |      12655.61 | Scheduled | Completed
                2015-04-30      | 2015-04-30 | 2015-04-30  |       12655.61 |       4289.03 |   105.46 |   4183.57 |       8472.04 | Scheduled | Completed
                2015-05-31      | 2015-06-02 | 2015-06-02  |        8472.04 |       4289.03 |    70.60 |   4218.43 |       4253.61 | Scheduled | Completed
                2015-06-30      | 2015-06-30 | 2015-06-30  |        4253.61 |       4289.06 |    35.45 |   4253.61 |          0.00 | Scheduled | Completed
            ");

            Assert.Equal(  0.00, Formula.Apply(Date(2014, 11, 30), Terms, Schedule));
            Assert.Equal(  0.00, Formula.Apply(Date(2014, 12, 31), Terms, Schedule));
            Assert.Equal(  0.00, Formula.Apply(Date(2015,  1, 31), Terms, Schedule));
            Assert.Equal(382.66, Formula.Apply(Date(2015,  2, 28), Terms, Schedule));
            Assert.Equal(  0.00, Formula.Apply(Date(2015,  3, 31), Terms, Schedule));
            Assert.Equal(245.50, Formula.Apply(Date(2015,  4, 30), Terms, Schedule));
            Assert.Equal(  0.00, Formula.Apply(Date(2015,  5, 31), Terms, Schedule));
            Assert.Equal(106.05, Formula.Apply(Date(2015,  6, 30), Terms, Schedule));
            Assert.Equal(  0.00, Formula.Apply(Date(2015,  7, 31), Terms, Schedule));
        }
        
        [Fact]
        public void PaymentDateOnThe1stOfTheMonth()
        {
            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type      | Status   
                2015-01-01      | 2015-01-01 | 2015-01-01  |       25000.00 |       4289.03 |   208.33 |   4080.70 |      20919.30 | Scheduled | Completed
                2015-02-01      | 2015-02-01 | 2015-02-01  |       20919.30 |       4289.03 |   174.33 |   4114.70 |      16804.60 | Scheduled | Completed
                2015-03-01      | 2015-03-01 | 2015-03-01  |       16804.60 |       4289.03 |   140.04 |   4148.99 |      12655.61 | Scheduled | Completed
                2015-04-01      | 2015-04-01 | 2015-04-01  |       12655.61 |       4289.03 |   105.46 |   4183.57 |       8472.04 | Scheduled | Completed
                2015-05-01      | 2015-05-01 | 2015-05-01  |        8472.04 |       4289.03 |    70.60 |   4218.43 |       4253.61 | Scheduled | Completed
                2015-06-01      | 2015-06-01 | 2015-06-01  |        4253.61 |       4289.06 |    35.45 |   4253.61 |          0.00 | Scheduled | Completed
            ");

            Assert.Equal(  0.00, Formula.Apply(Date(2014, 11, 30), Terms, Schedule));
            Assert.Equal(  0.00, Formula.Apply(Date(2014, 12, 31), Terms, Schedule));
            Assert.Equal(208.33, Formula.Apply(Date(2015,  1, 31), Terms, Schedule));
            Assert.Equal(174.33, Formula.Apply(Date(2015,  2, 28), Terms, Schedule));
            Assert.Equal(140.04, Formula.Apply(Date(2015,  3, 31), Terms, Schedule));
            Assert.Equal(105.46, Formula.Apply(Date(2015,  4, 30), Terms, Schedule));
            Assert.Equal( 70.60, Formula.Apply(Date(2015,  5, 31), Terms, Schedule));
            Assert.Equal( 35.45, Formula.Apply(Date(2015,  6, 30), Terms, Schedule));
            Assert.Equal(  0.00, Formula.Apply(Date(2015,  7, 31), Terms, Schedule));
        }

        [Fact]
        public void Payoff()
        {
            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type      | Status   
                2015-01-22      | 2015-01-24 | 2015-01-24  |       25000.00 |       4289.03 |   208.33 |   4080.70 |      20919.30 | Scheduled | Completed
                2015-02-22      | 2015-02-24 | 2015-02-24  |       20919.30 |       4289.03 |   174.33 |   4114.70 |      16804.60 | Scheduled | Completed
                2015-03-22      | 2015-03-24 | 2015-03-24  |       16804.60 |       4289.03 |   140.04 |   4148.99 |      12655.61 | Scheduled | Completed
                                | 2015-04-10 | 2015-04-10  |       12655.61 |      12718.89 |    63.28 |  12655.61 |          0.00 | Payoff    | Completed
            ");

            Assert.Equal(  0.00, Formula.Apply(Date(2014, 11, 30), Terms, Schedule));
            Assert.Equal(  0.00, Formula.Apply(Date(2014, 12, 31), Terms, Schedule));
            Assert.Equal(208.33, Formula.Apply(Date(2015,  1, 31), Terms, Schedule));
            Assert.Equal(174.33, Formula.Apply(Date(2015,  2, 28), Terms, Schedule));
            Assert.Equal(140.04, Formula.Apply(Date(2015,  3, 31), Terms, Schedule));
            Assert.Equal( 63.28, Formula.Apply(Date(2015,  4, 30), Terms, Schedule));
            Assert.Equal(  0.00, Formula.Apply(Date(2015,  5, 31), Terms, Schedule));
            Assert.Equal(  0.00, Formula.Apply(Date(2015,  6, 30), Terms, Schedule));
            Assert.Equal(  0.00, Formula.Apply(Date(2015,  7, 31), Terms, Schedule));
        }

        [Fact]
        public void ExcludeChargeOffInstallments()
        {
            Schedule = Mock.Of<IPaymentSchedule>(s => s.Installments == new[]
            {
                Installment().Interest(10)
                    .Paid()
                    .DueOn(Date(2016, 2, 8))
                    .PaidOn(Date(2016, 2, 9)).Object,

                Installment().Interest(99)
                    .Paid()
                    .Type(InstallmentType.ChargeOff)
                    .AnniversaryOn(Date(2016, 2, 25))
                    .DueOn(Date(2016, 2, 26))
                    .PaidOn(Date(2016, 2, 27)).Object,

                Installment().Interest(20)
                    .Paid()
                    .DueOn(Date(2016, 2, 27))
                    .PaidOn(Date(2016, 2, 28)).Object,
            });

            Assert.Equal(30, Formula.Apply(Date(2016, 2, 28), Terms, Schedule));
        }
    }
}
