﻿using LendFoundry.Loans.Service.Summary;
using Moq;
using System;
using Xunit;

namespace LendFoundry.Loans.Service.Tests
{
    public class NetInterestAccruedFormulaTest : ScheduledBasedFormulaTest
    {
        private INetInterestAccruedFunction Formula { get; }
        private Mock<IInterestAccruedFunction> InterestAccruedFormula { get; } = new Mock<IInterestAccruedFunction>();
        private Mock<IInterestPaidFunction> InterestPaidFormula { get; } = new Mock<IInterestPaidFunction>();

        public NetInterestAccruedFormulaTest()
        {
            Formula = new NetInterestAccruedFunction(InterestAccruedFormula.Object, InterestPaidFormula.Object, TenantTime);
        }

        private ReturnValue<double> InterestAccruedOn(DateTimeOffset referenceDate)
        {
            return new ReturnValue<double>((value) => InterestAccruedFormula.Setup(f => f.Apply(referenceDate, Terms, Schedule, true)).Returns(value));
        }

        private ReturnValue<double> InterestPaidOn(DateTimeOffset referenceDate)
        {
            return new ReturnValue<double>((value) => InterestPaidFormula.Setup(f => f.Apply(referenceDate, Terms, Schedule)).Returns(value));
        }

        [Fact]
        public void PerfectPay()
        {
            Terms.OriginationDate = TenantTime.Create(2014, 12, 22);
            Terms.LoanAmount = 25000.0;
            Terms.Rate = 10;

            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type      | Status   
                2015-01-22      | 2015-01-24 | 2015-01-24  |       25000.00 |       4289.03 |   208.33 |   4080.70 |      20919.30 | Scheduled | Completed
                2015-02-22      | 2015-02-24 | 2015-02-24  |       20919.30 |       4289.03 |   174.33 |   4114.70 |      16804.60 | Scheduled | Completed
                2015-03-22      | 2015-03-24 | 2015-03-24  |       16804.60 |       4289.03 |   140.04 |   4148.99 |      12655.61 | Scheduled | Completed
                2015-04-22      | 2015-04-24 | 2015-04-24  |       12655.61 |       4289.03 |   105.46 |   4183.57 |       8472.04 | Scheduled | Completed
                2015-05-22      | 2015-05-24 | 2015-05-24  |        8472.04 |       4289.03 |    70.60 |   4218.43 |       4253.61 | Scheduled | Completed
                2015-06-22      | 2015-06-24 | 2015-06-24  |        4253.61 |       4289.06 |    35.45 |   4253.61 |          0.00 | Scheduled | Completed
            ");

            InterestAccruedOn(Date(2014, 12, 31)).Is( 62.50);
            InterestAccruedOn(Date(2015,  1, 31)).Is(198.13);
            InterestAccruedOn(Date(2015,  2, 28)).Is(164.04);
            InterestAccruedOn(Date(2015,  3, 31)).Is(129.67);
            InterestAccruedOn(Date(2015,  4, 30)).Is( 95.00);
            InterestAccruedOn(Date(2015,  5, 31)).Is( 60.05);
            InterestAccruedOn(Date(2015,  6, 30)).Is( 24.81);

            InterestPaidOn(Date(2015,  1, 31)).Is(208.33);
            InterestPaidOn(Date(2015,  2, 28)).Is(174.33);
            InterestPaidOn(Date(2015,  3, 31)).Is(140.04);
            InterestPaidOn(Date(2015,  4, 30)).Is(105.46);
            InterestPaidOn(Date(2015,  5, 31)).Is( 70.60);
            InterestPaidOn(Date(2015,  6, 30)).Is( 35.45);

            Assert.Equal( 0.00, Formula.Apply(Date(2014, 11, 30), Terms, Schedule));
            Assert.Equal(62.50, Formula.Apply(Date(2014, 12, 31), Terms, Schedule));
            Assert.Equal(52.30, Formula.Apply(Date(2015,  1, 31), Terms, Schedule));
            Assert.Equal(42.01, Formula.Apply(Date(2015,  2, 28), Terms, Schedule));
            Assert.Equal(31.64, Formula.Apply(Date(2015,  3, 31), Terms, Schedule));
            Assert.Equal(21.18, Formula.Apply(Date(2015,  4, 30), Terms, Schedule));
            Assert.Equal(10.63, Formula.Apply(Date(2015,  5, 31), Terms, Schedule));
            Assert.Equal( 0.00, Formula.Apply(Date(2015,  6, 30), Terms, Schedule));
        }

        [Fact]
        public void NoPayment()
        {
            Terms.OriginationDate = TenantTime.Create(2014, 12, 22);
            Terms.LoanAmount = 25000.0;
            Terms.Rate = 10;

            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type      | Status   
                2015-01-22      | 2015-01-24 |             |       25000.00 |       4289.03 |   208.33 |   4080.70 |      20919.30 | Scheduled | Scheduled
                2015-02-22      | 2015-02-24 |             |       20919.30 |       4289.03 |   174.33 |   4114.70 |      16804.60 | Scheduled | Scheduled
                2015-03-22      | 2015-03-24 |             |       16804.60 |       4289.03 |   140.04 |   4148.99 |      12655.61 | Scheduled | Scheduled
                2015-04-22      | 2015-04-24 |             |       12655.61 |       4289.03 |   105.46 |   4183.57 |       8472.04 | Scheduled | Scheduled
                2015-05-22      | 2015-05-24 |             |        8472.04 |       4289.03 |    70.60 |   4218.43 |       4253.61 | Scheduled | Scheduled
                2015-06-22      | 2015-06-24 |             |        4253.61 |       4289.06 |    35.45 |   4253.61 |          0.00 | Scheduled | Scheduled
            ");

            InterestAccruedOn(Date(2014, 12, 31)).Is( 62.50);
            InterestAccruedOn(Date(2015,  1, 31)).Is(208.33);
            InterestAccruedOn(Date(2015,  2, 28)).Is(208.33);
            InterestAccruedOn(Date(2015,  3, 31)).Is(208.33);
            InterestAccruedOn(Date(2015,  4, 30)).Is(208.33);
            InterestAccruedOn(Date(2015,  5, 31)).Is(208.33);
            InterestAccruedOn(Date(2015,  6, 30)).Is(208.33);
            InterestAccruedOn(Date(2015,  7, 31)).Is(208.33);
            InterestAccruedOn(Date(2015,  8, 31)).Is(208.33);

            Assert.Equal(   0.00, Formula.Apply(Date(2014, 11, 30), Terms, Schedule));
            Assert.Equal(  62.50, Formula.Apply(Date(2014, 12, 31), Terms, Schedule));
            Assert.Equal( 270.83, Formula.Apply(Date(2015,  1, 31), Terms, Schedule));
            Assert.Equal( 479.16, Formula.Apply(Date(2015,  2, 28), Terms, Schedule));
            Assert.Equal( 687.49, Formula.Apply(Date(2015,  3, 31), Terms, Schedule));
            Assert.Equal( 895.82, Formula.Apply(Date(2015,  4, 30), Terms, Schedule));
            Assert.Equal(1104.15, Formula.Apply(Date(2015,  5, 31), Terms, Schedule));
            Assert.Equal(1312.48, Formula.Apply(Date(2015,  6, 30), Terms, Schedule));
            Assert.Equal(1520.81, Formula.Apply(Date(2015,  7, 31), Terms, Schedule));
            Assert.Equal(1729.14, Formula.Apply(Date(2015,  8, 31), Terms, Schedule));
        }

        [Fact]
        public void FirstThreeInstallmentsPaid()
        {
            Terms.OriginationDate = TenantTime.Create(2014, 12, 22);
            Terms.LoanAmount = 25000.0;
            Terms.Rate = 10;

            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type      | Status   
                2015-01-22      | 2015-01-24 | 2015-01-24  |       25000.00 |       4289.03 |   208.33 |   4080.70 |      20919.30 | Scheduled | Completed
                2015-02-22      | 2015-02-24 | 2015-02-24  |       20919.30 |       4289.03 |   174.33 |   4114.70 |      16804.60 | Scheduled | Completed
                2015-03-22      | 2015-03-24 | 2015-03-24  |       16804.60 |       4289.03 |   140.04 |   4148.99 |      12655.61 | Scheduled | Completed
                2015-04-22      | 2015-04-24 |             |       12655.61 |       4289.03 |   105.46 |   4183.57 |       8472.04 | Scheduled | Scheduled
                2015-05-22      | 2015-05-24 |             |        8472.04 |       4289.03 |    70.60 |   4218.43 |       4253.61 | Scheduled | Scheduled
                2015-06-22      | 2015-06-24 |             |        4253.61 |       4289.06 |    35.45 |   4253.61 |          0.00 | Scheduled | Scheduled
            ");

            InterestAccruedOn(Date(2014, 12, 31)).Is( 62.50);
            InterestAccruedOn(Date(2015,  1, 31)).Is(198.13);
            InterestAccruedOn(Date(2015,  2, 28)).Is(164.04);
            InterestAccruedOn(Date(2015,  3, 31)).Is(129.67);
            InterestAccruedOn(Date(2015,  4, 30)).Is(129.67);
            InterestAccruedOn(Date(2015,  5, 31)).Is(129.67);
            InterestAccruedOn(Date(2015,  6, 30)).Is(129.67);
            InterestAccruedOn(Date(2015,  7, 31)).Is(129.67);
            InterestAccruedOn(Date(2015,  8, 31)).Is(129.67);

            InterestPaidOn(Date(2015,  1, 31)).Is(208.33);
            InterestPaidOn(Date(2015,  2, 28)).Is(174.33);
            InterestPaidOn(Date(2015,  3, 31)).Is(140.04);

            Assert.Equal(  0.00, Formula.Apply(Date(2014, 11, 30), Terms, Schedule));
            Assert.Equal( 62.50, Formula.Apply(Date(2014, 12, 31), Terms, Schedule));
            Assert.Equal( 52.30, Formula.Apply(Date(2015,  1, 31), Terms, Schedule));
            Assert.Equal( 42.01, Formula.Apply(Date(2015,  2, 28), Terms, Schedule));
            Assert.Equal( 31.64, Formula.Apply(Date(2015,  3, 31), Terms, Schedule));
            Assert.Equal(161.31, Formula.Apply(Date(2015,  4, 30), Terms, Schedule));
            Assert.Equal(290.98, Formula.Apply(Date(2015,  5, 31), Terms, Schedule));
            Assert.Equal(420.65, Formula.Apply(Date(2015,  6, 30), Terms, Schedule));
            Assert.Equal(550.32, Formula.Apply(Date(2015,  7, 31), Terms, Schedule));
            Assert.Equal(679.99, Formula.Apply(Date(2015,  8, 31), Terms, Schedule));
        }

        [Fact]
        public void ExtraPaymentBeforeScheduledInstallment()
        {
            Terms.OriginationDate = TenantTime.Create(2014, 12, 22);
            Terms.LoanAmount = 25000.0;
            Terms.Rate = 10;

            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type       | Status   
                2015-01-22      | 2015-01-24 | 2015-01-24  |       25000.00 |       4289.03 |   208.33 |   4080.70 |      20919.30 | Scheduled  | Completed
                2015-02-22      | 2015-02-24 | 2015-02-24  |       20919.30 |       4289.03 |   174.33 |   4114.70 |      16804.60 | Scheduled  | Completed
                                | 2015-03-09 | 2015-03-09  |       16804.60 |        999.00 |     0.00 |    999.00 |      15805.60 | Additional | Completed
                2015-03-22      | 2015-03-24 | 2015-03-24  |       15805.60 |       4289.03 |   136.43 |   4152.60 |      11653.00 | Scheduled  | Completed
                2015-04-22      | 2015-04-24 | 2015-04-24  |       11653.00 |       4289.03 |    97.11 |   4191.92 |       7461.08 | Scheduled  | Completed
                2015-05-22      | 2015-05-24 | 2015-05-24  |        7461.08 |       4289.03 |    62.18 |   4226.85 |       3234.23 | Scheduled  | Completed
                2015-06-22      | 2015-06-24 | 2015-06-24  |        3234.23 |       3261.18 |    26.95 |   3234.23 |          0.00 | Scheduled  | Completed
            ");

            InterestAccruedOn(Date(2014, 12, 31)).Is( 62.50);
            InterestAccruedOn(Date(2015,  1, 31)).Is(198.13);
            InterestAccruedOn(Date(2015,  2, 28)).Is(164.04);
            InterestAccruedOn(Date(2015,  3, 31)).Is(123.55);
            InterestAccruedOn(Date(2015,  4, 30)).Is( 86.63);
            InterestAccruedOn(Date(2015,  5, 31)).Is( 51.61);
            InterestAccruedOn(Date(2015,  6, 30)).Is( 18.87);

            InterestPaidOn(Date(2015,  1, 31)).Is(208.33);
            InterestPaidOn(Date(2015,  2, 28)).Is(174.33);
            InterestPaidOn(Date(2015,  3, 31)).Is(136.43);
            InterestPaidOn(Date(2015,  4, 30)).Is( 97.11);
            InterestPaidOn(Date(2015,  5, 31)).Is( 62.18);
            InterestPaidOn(Date(2015,  6, 30)).Is( 26.95);

            Assert.Equal( 0.00, Formula.Apply(Date(2014, 11, 30), Terms, Schedule));
            Assert.Equal(62.50, Formula.Apply(Date(2014, 12, 31), Terms, Schedule));
            Assert.Equal(52.30, Formula.Apply(Date(2015,  1, 31), Terms, Schedule));
            Assert.Equal(42.01, Formula.Apply(Date(2015,  2, 28), Terms, Schedule));
            Assert.Equal(29.13, Formula.Apply(Date(2015,  3, 31), Terms, Schedule));
            Assert.Equal(18.65, Formula.Apply(Date(2015,  4, 30), Terms, Schedule));
            Assert.Equal( 8.08, Formula.Apply(Date(2015,  5, 31), Terms, Schedule));
            Assert.Equal( 0.00, Formula.Apply(Date(2015,  6, 30), Terms, Schedule));
            Assert.Equal( 0.00, Formula.Apply(Date(2015,  7, 30), Terms, Schedule));
        }

        [Fact]
        public void ExtraPaymentAfterScheduledInstallment()
        {
            Terms.OriginationDate = TenantTime.Create(2014, 12, 22);
            Terms.LoanAmount = 25000.0;
            Terms.Rate = 10;

            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type       | Status   
                2015-01-22      | 2015-01-24 | 2015-01-24  |       25000.00 |       4289.03 |   208.33 |   4080.70 |      20919.30 | Scheduled  | Completed
                2015-02-22      | 2015-02-24 | 2015-02-24  |       20919.30 |       4289.03 |   174.33 |   4114.70 |      16804.60 | Scheduled  | Completed
                2015-03-22      | 2015-03-24 | 2015-03-24  |       16804.60 |       4289.03 |   140.04 |   4148.99 |      12655.61 | Scheduled  | Completed
                                | 2015-03-29 | 2015-03-29  |       12655.61 |        999.00 |     0.00 |    999.00 |      11656.61 | Additional | Completed
                2015-04-22      | 2015-04-24 | 2015-04-24  |       11656.61 |       4289.03 |    99.08 |   4189.95 |       7466.66 | Scheduled  | Completed
                2015-05-22      | 2015-05-24 | 2015-05-24  |        7466.66 |       4289.03 |    62.22 |   4226.81 |       3239.85 | Scheduled  | Completed
                2015-06-22      | 2015-06-24 | 2015-06-24  |        3239.85 |       3266.85 |    27.00 |   3239.85 |          0.00 | Scheduled  | Completed
            ");

            InterestAccruedOn(Date(2014, 12, 31)).Is( 62.50);
            InterestAccruedOn(Date(2015,  1, 31)).Is(198.13);
            InterestAccruedOn(Date(2015,  2, 28)).Is(164.04);
            InterestAccruedOn(Date(2015,  3, 31)).Is(129.11);
            InterestAccruedOn(Date(2015,  4, 30)).Is( 86.66);
            InterestAccruedOn(Date(2015,  5, 31)).Is( 51.66);
            InterestAccruedOn(Date(2015,  6, 30)).Is( 18.90);

            InterestPaidOn(Date(2015,  1, 31)).Is(208.33);
            InterestPaidOn(Date(2015,  2, 28)).Is(174.33);
            InterestPaidOn(Date(2015,  3, 31)).Is(140.04);
            InterestPaidOn(Date(2015,  4, 30)).Is( 99.08);
            InterestPaidOn(Date(2015,  5, 31)).Is( 62.22);
            InterestPaidOn(Date(2015,  6, 30)).Is( 27.00);

            Assert.Equal( 0.00, Formula.Apply(Date(2014, 11, 30), Terms, Schedule));
            Assert.Equal(62.50, Formula.Apply(Date(2014, 12, 31), Terms, Schedule));
            Assert.Equal(52.30, Formula.Apply(Date(2015,  1, 31), Terms, Schedule));
            Assert.Equal(42.01, Formula.Apply(Date(2015,  2, 28), Terms, Schedule));
            Assert.Equal(31.08, Formula.Apply(Date(2015,  3, 31), Terms, Schedule));
            Assert.Equal(18.66, Formula.Apply(Date(2015,  4, 30), Terms, Schedule));
            Assert.Equal( 8.10, Formula.Apply(Date(2015,  5, 31), Terms, Schedule));
            Assert.Equal( 0.00, Formula.Apply(Date(2015,  6, 30), Terms, Schedule));
            Assert.Equal( 0.00, Formula.Apply(Date(2015,  7, 30), Terms, Schedule));
        }

        [Fact]
        public void Payoff()
        {
            Terms.OriginationDate = TenantTime.Create(2014, 12, 22);
            Terms.LoanAmount = 25000.0;
            Terms.Rate = 10;

            CreateSchedule(@"
                AnniversaryDate | DueDate    | PaymentDate | OpeningBalance | PaymentAmount | Interest | Principal | EndingBalance | Type      | Status   
                2015-01-22      | 2015-01-24 | 2015-01-24  |       25000.00 |       4289.03 |   208.33 |   4080.70 |      20919.30 | Scheduled | Completed
                2015-02-22      | 2015-02-24 | 2015-02-24  |       20919.30 |       4289.03 |   174.33 |   4114.70 |      16804.60 | Scheduled | Completed
                2015-03-22      | 2015-03-24 | 2015-03-24  |       16804.60 |       4289.03 |   140.04 |   4148.99 |      12655.61 | Scheduled | Completed
                                | 2015-04-10 | 2015-04-10  |       12655.61 |      12718.89 |    63.28 |  12655.61 |          0.00 | Payoff    | Completed
            ");

            InterestAccruedOn(Date(2014, 12, 31)).Is( 62.50);
            InterestAccruedOn(Date(2015,  1, 31)).Is(198.13);
            InterestAccruedOn(Date(2015,  2, 28)).Is(164.04);
            InterestAccruedOn(Date(2015,  3, 31)).Is(129.67);
            InterestAccruedOn(Date(2015,  4, 30)).Is( 31.64);

            InterestPaidOn(Date(2015,  1, 31)).Is(208.33);
            InterestPaidOn(Date(2015,  2, 28)).Is(174.33);
            InterestPaidOn(Date(2015,  3, 31)).Is(140.04);
            InterestPaidOn(Date(2015,  4, 30)).Is( 63.28);

            Assert.Equal( 0.00, Formula.Apply(Date(2014, 11, 30), Terms, Schedule));
            Assert.Equal(62.50, Formula.Apply(Date(2014, 12, 31), Terms, Schedule));
            Assert.Equal(52.30, Formula.Apply(Date(2015,  1, 31), Terms, Schedule));
            Assert.Equal(42.01, Formula.Apply(Date(2015,  2, 28), Terms, Schedule));
            Assert.Equal(31.64, Formula.Apply(Date(2015,  3, 31), Terms, Schedule));
            Assert.Equal( 0.00, Formula.Apply(Date(2015,  4, 30), Terms, Schedule));
        }
    }
}
