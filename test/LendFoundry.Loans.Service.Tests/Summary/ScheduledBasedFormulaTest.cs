﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Testing;
using Moq;
using System;

namespace LendFoundry.Loans.Service.Tests
{
    public abstract class ScheduledBasedFormulaTest
    {
        protected ILoanTerms Terms { get; } = new LoanTerms();
        protected ITenantTime TenantTime { get; } = new UtcTenantTime();
        protected IPaymentSchedule Schedule { get; set; }

        protected void CreateSchedule(string table)
        {
            Schedule = Mock.Of<IPaymentSchedule>(i => i.Installments == ListParser.Parse<Installment>(table));
        }

        protected DateTimeOffset Date(int year, int month, int day) { return TenantTime.Create(year, month, day); }

        protected class ReturnValue<T>
        {
            private Action<T> Callback;

            public ReturnValue(Action<T> callback) { Callback = callback; }

            public void Is(T value) { Callback(value); }
        }
    }
}
