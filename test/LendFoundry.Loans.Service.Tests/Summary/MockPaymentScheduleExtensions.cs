﻿using Moq;
using System.Collections.Generic;

namespace LendFoundry.Loans.Service.Tests
{
    public static class MockPaymentScheduleExtensions
    {
        public static Mock<IPaymentSchedule> With(this Mock<IPaymentSchedule> mock, IEnumerable<IInstallment> installments)
        {
            mock.Setup(i => i.Installments).Returns(installments);
            return mock;
        }
    }
}
