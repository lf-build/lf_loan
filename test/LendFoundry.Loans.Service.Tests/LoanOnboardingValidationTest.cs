﻿using LendFoundry.Amortization.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Service.Tests.Fake;
using LendFoundry.Loans.TransactionLog.Client;
using Moq;
using System;
using System.Collections.Generic;
using LendFoundry.Calendar.Client;
using LendFoundry.Loans.Payment;
using Xunit;
using LendFoundry.Loans.Investors;
using LendFoundry.Foundation.Testing;
using LendFoundry.Loans.Service.Summary;

namespace LendFoundry.Loans.Service.Tests
{
    public class LoanOnboardingValidationTest
    {
        private ILoanService loanService;
        private OnboardingRequest onboardingRequest;
        private BankAccount bankInfo;
        private Borrower borrower1;
        private Borrower borrower2;
        private IInvestorService investorService;

        public LoanOnboardingValidationTest()
        {
            investorService = InvestorServiceMock();

            loanService = new LoanService(
                new FakeLoanRepository(),
                new FakeBorrowerRepository(),
                new FakePaymentScheduleService(),
                new FakeEventHub(),
                Mock.Of<ITransactionLogService>(),
                Mock.Of<IAmortizationService>(),
                new UtcTenantTime(),
                Mock.Of<ILogger>(),
                Mock.Of<ICalendarService>(),
                investorService,
                Mock.Of<ILoanEngineResolver>(),
                Mock.Of<ILoanSummaryBuilder>(),
                new LoanConfiguration()
            );

            bankInfo = new BankAccount
            {
                AccountNumber = "acc007",
                AccountType = BankAccountType.Checking,
                RoutingNumber = "routing101"
            };

            borrower1 = new Borrower
            {
                ReferenceNumber = "borrower001",
                IsPrimary = true,
                FirstName = "John",
                MiddleInitial = "Z",
                LastName = "Doe",
                DateOfBirth = new DateTime(1977, 7, 20),
                SSN = "721-07-4426",
                Email = "john@gmail.com",
                Addresses = new []
                {
                    new Address
                    {
                        IsPrimary = false,
                        Line1 = "196 25th Avenue",
                        City = "San Francisco",
                        State = "CA",
                        ZipCode = "94121"
                    },
                    new Address
                    {
                        IsPrimary = true,
                        Line1 = "1062 Walnut St",
                        City = "Irvine",
                        State = "CA",
                        ZipCode = "92780"
                    },
                    new Address
                    {
                        IsPrimary = false,
                        Line1 = "386 S Olive St",
                        City = "Anaheim",
                        State = "CA",
                        ZipCode = "92805"
                    }
                },
                Phones = new []
                {
                    new Phone
                    {
                        IsPrimary = false,
                        Type = PhoneType.Home,
                        Number = "65202108"
                    },
                    new Phone
                    {
                        IsPrimary = false,
                        Type = PhoneType.Work,
                        Number = "468342093"
                    },
                    new Phone
                    {
                        IsPrimary = true,
                        Type = PhoneType.Mobile,
                        Number = "2014074907"
                    }
                }
            };

            borrower2 = new Borrower
            {
                ReferenceNumber = "borrower002",
                IsPrimary = false,
                FirstName = "Mary",
                MiddleInitial = "X",
                LastName = "Jane",
                DateOfBirth = new DateTime(1980, 12, 31),
                SSN = "502-01-9047",
                Email = "mary@outlook.com",
                Addresses = new []
                {
                    new Address
                    {
                        IsPrimary = true,
                        Line1 = "1526 Lincoln St",
                        City = "Bakersfield",
                        State = "CA",
                        ZipCode = "93305"
                    }
                        },
                Phones = new []
                {
                    new Phone
                    {
                        IsPrimary = true,
                        Type = PhoneType.Mobile,
                        Number = "305214845"
                    }
                }
            };

            onboardingRequest = new OnboardingRequest
            {
                ReferenceNumber = "loan001",
                FundingSource = "acme",
                Purpose = "Home improvement",
                Terms = new LoanTerms {
                    ApplicationDate = DateTimeOffset.Now,
                    OriginationDate = DateTimeOffset.Now,
                    FundedDate = DateTimeOffset.Now,
                    LoanAmount = 1.0,
                    Term = 1,
                    Rate = 100.0,
                    RateType = RateType.Percent,
                    Fees = new List<IFee>(),
                },
                PaymentMethod = PaymentMethod.ACH,
                BankInfo = bankInfo,
                Borrowers = new List<IBorrower> { borrower1, borrower2 },
                Investor = new LoanInvestor { Id = "inv001", LoanPurchaseDate = DateTimeOffset.Now },
                Scores = new List<IScore> { new Score { Name = "FICO", InitialScore = "500", InitialDate = DateTimeOffset.Now } },
                Grade = "ABC",
                MonthlyIncome = 10000,
                PreCloseDti = 5,
                PostCloseDti = 9,
                HomeOwnership = "Owner"
            };
        }

        private static IInvestorService InvestorServiceMock()
        {
            var mock = new Mock<IInvestorService>();
            mock.Setup(s => s.Exists(It.IsAny<string>())).Returns(true);
            return mock.Object;
        }

        [Fact]
        public void OnboardingRequestCannotBeNull()
        {
            AssertException.Throws<InvalidArgumentException>("Argument cannot be null: onboardingRequest", () => loanService.Onboard(null));
        }

        [Fact]
        public void LoanReferenceNumberCannotBeNullOrEmpty()
        {
            onboardingRequest.ReferenceNumber = null;
            AssertInvalidArgument("Loan reference number is mandatory");

            onboardingRequest.ReferenceNumber = "  ";
            AssertInvalidArgument("Loan reference number is mandatory");
        }

        [Fact]
        public void LoanReferenceNumberMustBeUnique()
        {
            // Onboard once...
            loanService.Onboard(onboardingRequest);

            // then onboard the same loan twice:
            AssertException.Throws<InvalidArgumentException>($"A loan already exists with reference number: {onboardingRequest.ReferenceNumber}", () => loanService.Onboard(onboardingRequest));
        }

        [Fact]
        public void FundingSourceIsMandatory()
        {
            onboardingRequest.FundingSource = null;
            AssertInvalidArgument("Funding source is mandatory");

            onboardingRequest.FundingSource = "  ";
            AssertInvalidArgument("Funding source is mandatory");
        }

        [Fact]
        public void LoanPurposeIsMandatory()
        {
            onboardingRequest.Purpose = null;
            AssertInvalidArgument("Loan purpose is mandatory");

            onboardingRequest.Purpose = "  ";
            AssertInvalidArgument("Loan purpose is mandatory");
        }

        [Fact]
        public void LoanAmountCannotBeLessOrEqualToZero()
        {
            onboardingRequest.Terms.LoanAmount = 0.0;
            AssertInvalidArgument("Loan amount must be greater than zero");

            onboardingRequest.Terms.LoanAmount = -1.0;
            AssertInvalidArgument("Loan amount must be greater than zero");
        }

        [Fact]
        public void LoanTermCannotBeLessOrEqualToZero()
        {
            onboardingRequest.Terms.Term = 0;
            AssertInvalidArgument("Loan term must be greater than zero");

            onboardingRequest.Terms.Term = -1;
            AssertInvalidArgument("Loan term must be greater than zero");
        }

        [Fact]
        public void LoanRateMustBeGreaterThanZero()
        {
            onboardingRequest.Terms.Rate = 0.0;
            AssertInvalidArgument("Loan rate must be in the interval (0, 100]");

            onboardingRequest.Terms.Rate = -0.1;
            AssertInvalidArgument("Loan rate must be in the interval (0, 100]");
        }

        [Fact]
        public void LoanRateMustBeLessOrEqualToOneHundred()
        {
            onboardingRequest.Terms.Rate = 100.1;
            AssertInvalidArgument("Loan rate must be in the interval (0, 100]");
        }

        [Fact]
        public void LoanRateTypeCannotBeUndefined()
        {
            onboardingRequest.Terms.RateType = RateType.Undefined;
            AssertInvalidArgument("Rate type is mandatory");
        }

        [Fact]
        public void ListOfFeesCannotBeNull()
        {
            onboardingRequest.Terms.Fees = null;
            AssertInvalidArgument("Fee list cannot be null");
        }

        [Fact]
        public void PaymentMethodCannotBeUndefined()
        {
            onboardingRequest.PaymentMethod = PaymentMethod.Undefined;
            AssertInvalidArgument("Payment method is mandatory");
        }

        [Fact]
        public void BankInfoCannotBeNullWhenPaymentMethodIsACH()
        {
            onboardingRequest.PaymentMethod = PaymentMethod.ACH;
            onboardingRequest.BankInfo = null;
            AssertInvalidArgument("Bank info is mandatory when payment method is ACH");
        }

        [Fact]
        public void BankRoutingNumberMustBeProvidedWhenPaymentMethodIsACH()
        {
            onboardingRequest.PaymentMethod = PaymentMethod.ACH;

            bankInfo.RoutingNumber = null;
            AssertInvalidArgument("Bank routing number is mandatory when payment method is ACH");

            bankInfo.RoutingNumber = "  ";
            AssertInvalidArgument("Bank routing number is mandatory when payment method is ACH");
        }

        [Fact]
        public void BankAccountNumberMustBeProvidedWhenPaymentMethodIsACH()
        {
            onboardingRequest.PaymentMethod = PaymentMethod.ACH;

            bankInfo.AccountNumber = null;
            AssertInvalidArgument("Bank account number is mandatory when payment method is ACH");

            bankInfo.AccountNumber = "  ";
            AssertInvalidArgument("Bank account number is mandatory when payment method is ACH");
        }

        [Fact]
        public void BankAccountTypeMustBeProvidedWhenPaymentMethodIsACH()
        {
            onboardingRequest.PaymentMethod = PaymentMethod.ACH;

            bankInfo.AccountType = BankAccountType.Undefined;
            AssertInvalidArgument("Bank account type is mandatory when payment method is ACH");
        }
        
        [Fact]
        public void BankInfoCanBeNullWhenPaymentMethodIsCheck()
        {
            onboardingRequest.PaymentMethod = PaymentMethod.Check;
            onboardingRequest.BankInfo = null;

            // No exception should be thrown:
            loanService.Onboard(onboardingRequest);
        }
        
        [Fact]
        public void BankRoutingNumberMustBeProvidedWheneverTheBankInfoIsProvided()
        {
            onboardingRequest.PaymentMethod = PaymentMethod.Check;

            bankInfo.RoutingNumber = null;
            AssertInvalidArgument("Bank routing number is mandatory");

            bankInfo.RoutingNumber = "  ";
            AssertInvalidArgument("Bank routing number is mandatory");
        }

        [Fact]
        public void BankAccountNumberMustBeProvidedWheneverTheBankInfoIsProvided()
        {
            onboardingRequest.PaymentMethod = PaymentMethod.Check;

            bankInfo.AccountNumber = null;
            AssertInvalidArgument("Bank account number is mandatory");

            bankInfo.AccountNumber = "  ";
            AssertInvalidArgument("Bank account number is mandatory");
        }

        [Fact]
        public void BankAccountTypeMustBeProvidedWheneverTheBankInfoIsProvided()
        {
            onboardingRequest.PaymentMethod = PaymentMethod.Check;

            bankInfo.AccountType = BankAccountType.Undefined;
            AssertInvalidArgument("Bank account type is mandatory");
        }

        [Fact]
        public void BorrowerListCannotBeNullOrEmpty()
        {
            onboardingRequest.Borrowers = null;
            AssertInvalidArgument("Borrower list cannot be null");

            onboardingRequest.Borrowers = new List<IBorrower>();
            AssertInvalidArgument("Borrower list cannot be empty");
        }

        [Fact]
        public void BorrowerCannotBeNull()
        {
            onboardingRequest.Borrowers = new List<IBorrower> { null };
            AssertInvalidArgument("Borrower cannot be null");
        }

        [Fact]
        public void BorrowerReferenceNumberCannotBeNullOrEmpty()
        {
            borrower2.ReferenceNumber = null;
            AssertInvalidArgument("Borrower reference number name is mandatory");

            borrower2.ReferenceNumber = "  ";
            AssertInvalidArgument("Borrower reference number name is mandatory");
        }

        [Fact]
        public void AtLeastOneBorrowerMustBePrimary()
        {
            borrower1.IsPrimary = false;
            borrower2.IsPrimary = false;
            AssertInvalidArgument("At least one borrower must be primary");
        }

        [Fact]
        public void AtMostOneBorrowerMustBePrimary()
        {
            borrower1.IsPrimary = true;
            borrower2.IsPrimary = true;
            AssertInvalidArgument("At most one borrower must be primary");
        }

        [Fact]
        public void BorrowerFirstNameCannotBeNullOrEmpty()
        {
            borrower1.FirstName = null;
            AssertInvalidArgument("Borrower first name is mandatory");

            borrower1.FirstName = "  ";
            AssertInvalidArgument("Borrower first name is mandatory");
        }

        [Fact]
        public void BorrowerLastNameCannotBeNullOrEmpty()
        {
            borrower2.LastName = null;
            AssertInvalidArgument("Borrower last name is mandatory");

            borrower2.LastName = "  ";
            AssertInvalidArgument("Borrower last name is mandatory");
        }

        [Fact]
        public void BorrowerSSNCannotBeNullOrEmpty()
        {
            borrower2.SSN = null;
            AssertInvalidArgument("Borrower SSN is mandatory");

            borrower2.SSN = "  ";
            AssertInvalidArgument("Borrower SSN is mandatory");
        }

        [Fact]
        public void BorrowerEmailCannotBeNullOrEmpty()
        {
            borrower1.Email = null;
            AssertInvalidArgument("Borrower email is mandatory");

            borrower1.Email = "  ";
            AssertInvalidArgument("Borrower email is mandatory");
        }

        [Fact]
        public void BorrowerAddressListCannotNotBeNullOrEmpty()
        {
            borrower2.Addresses = null;
            AssertInvalidArgument("Borrower address list cannot be null");

            borrower2.Addresses = new List<Address>().ToArray();
            AssertInvalidArgument("Borrower address list cannot be empty");
        }

        [Fact]
        public void BorrowerAddressCannotBeNull()
        {
            borrower1.Addresses[1] = null;
            AssertInvalidArgument("Borrower address cannot be null");
        }

        [Fact]
        public void Line1OfBorrowerAddressCannotBeNullOrEmpty()
        {
            borrower1.Addresses[1].Line1 = null;
            AssertInvalidArgument("Invalid borrower address: line 1 is mandatory");

            borrower1.Addresses[1].Line1 = "  ";
            AssertInvalidArgument("Invalid borrower address: line 1 is mandatory");
        }

        [Fact]
        public void BorrowerCityIsMandatory()
        {
            borrower1.Addresses[1].City = null;
            AssertInvalidArgument("Invalid borrower address: city is mandatory");

            borrower1.Addresses[1].City = "  ";
            AssertInvalidArgument("Invalid borrower address: city is mandatory");
        }

        [Fact]
        public void BorrowerStateIsMandatory()
        {
            borrower1.Addresses[1].State = null;
            AssertInvalidArgument("Invalid borrower address: state is mandatory");

            borrower1.Addresses[1].State = "  ";
            AssertInvalidArgument("Invalid borrower address: state is mandatory");
        }

        [Fact]
        public void BorrowerZipCodeIsMandatory()
        {
            borrower1.Addresses[1].ZipCode = null;
            AssertInvalidArgument("Invalid borrower address: zip code is mandatory");

            borrower1.Addresses[1].ZipCode = "  ";
            AssertInvalidArgument("Invalid borrower address: zip code is mandatory");
        }

        [Fact]
        public void BorrowerMustHaveAtLeastOnePrimaryAddress()
        {
            borrower1.Addresses[0].IsPrimary = false;
            borrower1.Addresses[1].IsPrimary = false;
            AssertInvalidArgument("Borrower must have at least one primary address");

        }

        [Fact]
        public void BorrowerCanHaveAtMostOnePrimaryAddress()
        {
            borrower1.Addresses[0].IsPrimary = true;
            borrower1.Addresses[1].IsPrimary = true;
            AssertInvalidArgument("Borrower cannot have more than two primary addresses");
        }

        [Fact]
        public void BorrowerPhoneListCannotBeNullOrEmpty()
        {
            borrower2.Phones = null;
            AssertInvalidArgument("Borrower phone list cannot be null");

            borrower2.Phones = new List<Phone>().ToArray();
            AssertInvalidArgument("Borrower phone list cannot be empty");
        }

        [Fact]
        public void BorrowerPhoneCannotBeNull()
        {
            borrower1.Phones[1] = null;
            AssertInvalidArgument("Borrower phone cannot be null");
        }

        [Fact]
        public void BorrowerMustHaveAtLeastOnePrimaryPhone()
        {
            borrower1.Phones[0].IsPrimary = false;
            borrower1.Phones[1].IsPrimary = false;
            borrower1.Phones[2].IsPrimary = false;
            AssertInvalidArgument("Borrower must have at least one primary phone");
        }

        [Fact]
        public void BorrowerMustHaveAtMostOnePrimaryPhone()
        {
            borrower1.Phones[0].IsPrimary = true;
            borrower1.Phones[1].IsPrimary = false;
            borrower1.Phones[2].IsPrimary = true;
            AssertInvalidArgument("Borrower must have at most one primary phone");
        }

        [Fact]
        public void BorrowerPhoneNumberIsMandatory()
        {
            borrower1.Phones[1].Number = null;
            AssertInvalidArgument("Borrower phone number is mandatory");

            borrower1.Phones[1].Number = "  ";
            AssertInvalidArgument("Borrower phone number is mandatory");
        }

        [Fact]
        public void InvestorIsMandatory()
        {
            onboardingRequest.Investor = null;
            AssertInvalidArgument("Investor is mandatory");
        }

        [Fact]
        public void InvestorIdIsMandatory()
        {
            onboardingRequest.Investor.Id = null;
            AssertInvalidArgument("Investor's id is mandatory");

            onboardingRequest.Investor.Id = "  ";
            AssertInvalidArgument("Investor's id is mandatory");
        }

        [Fact]
        public void LoanPurchaseDateIsMandatory()
        {
            onboardingRequest.Investor.LoanPurchaseDate = new DateTimeOffset();
            AssertInvalidArgument("Investor's loan purchase date is mandatory");
        }

        [Fact]
        public void InvestorMustExist()
        {
            onboardingRequest.Investor.Id = "inv1234";

            Mock.Get(investorService).Setup(s => s.Exists("inv1234")).Returns(false);

            AssertInvalidArgument("Investor not found with id inv1234");
        }

        private void AssertInvalidArgument(string expectedMessage)
        {
            AssertException.Throws<InvalidArgumentException>(expectedMessage, () => loanService.Onboard(onboardingRequest));
        }
    }
}
