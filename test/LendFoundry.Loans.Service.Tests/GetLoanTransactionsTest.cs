﻿using LendFoundry.Amortization.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Service.Tests.Fake;
using LendFoundry.Loans.TransactionLog.Client;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Calendar.Client;
using LendFoundry.Loans.Payment;
using Xunit;
using LendFoundry.Loans.Investors;
using LendFoundry.Loans.Service.Summary;

namespace LendFoundry.Loans.Service.Tests
{
    public class GetLoanTransactionsTest
    {
        [Fact]
        public void ReturnEmptyListOfTransactionsWhenNoTransactionIsFound()
        {
            var transactionLogMock = new Mock<ITransactionLogService>();
            ILoanService loanService = new LoanService
            (
                new FakeLoanRepository(),
                new FakeBorrowerRepository(),
                new FakePaymentScheduleService(),
                new FakeEventHub(),
                transactionLogMock.Object,
                Mock.Of<IAmortizationService>(),
                new UtcTenantTime(),
                Mock.Of<ILogger>(),
                Mock.Of<ICalendarService>(),
                Mock.Of<IInvestorService>(),
                Mock.Of<ILoanEngineResolver>(),
                Mock.Of<ILoanSummaryBuilder>(),
                new LoanConfiguration()
            );

            transactionLogMock
                .Setup(transactionLog => transactionLog.GetTransactions("loan001"))
                .Returns(new List<TransactionInfo>());

            IEnumerable<ITransaction> transactions = loanService.GetTransactions("loan001");
            Assert.NotNull(transactions);
            Assert.Equal(0, transactions.Count());
        }

        [Fact]
        public void GetTwoTransactions()
        {
            var tenantTime = new UtcTenantTime();
            var transactionLogMock = new Mock<ITransactionLogService>();
            ILoanService loanService = new LoanService
            (
                new FakeLoanRepository(),
                new FakeBorrowerRepository(),
                new FakePaymentScheduleService(),
                new FakeEventHub(),
                transactionLogMock.Object,
                Mock.Of<IAmortizationService>(),
                tenantTime,
                Mock.Of<ILogger>(),
                Mock.Of<ICalendarService>(),
                Mock.Of<IInvestorService>(),
                Mock.Of<ILoanEngineResolver>(),
                Mock.Of<ILoanSummaryBuilder>(),
                new LoanConfiguration()
            );

            var transactionInfo1 = new TransactionInfo
            {
                LoanReference = "loan001",
                Id = "tx101",
                Code = "247",
                Description = "Payment",
                Date = tenantTime.Create(2015, 11, 5),
                Amount = 150.0,
                Notes = "It's all right"
            };

            var transactionInfo2 = new TransactionInfo
            {
                LoanReference = "loan001",
                Id = "tx102",
                Code = "300",
                Description = "Late fee",
                Date = tenantTime.Create(2015, 12, 31),
                Amount = 15.0,
                Notes = "Lorem ipsum"
            };

            transactionLogMock
                .Setup(transactionLog => transactionLog.GetTransactions("loan001"))
                .Returns(new List<TransactionInfo>
                {
                    transactionInfo1,
                    transactionInfo2
                });

            var transactions = loanService.GetTransactions("loan001").ToList();
            Assert.NotNull(transactions);
            Assert.Equal(2, transactions.Count);

            AssertTransaction(transactionInfo1, transactions[0]);
            AssertTransaction(transactionInfo2, transactions[1]);
        }

        private static void AssertTransaction(TransactionInfo expected, ITransaction actual)
        {
            Assert.NotNull(actual);
            Assert.Equal(expected.LoanReference, actual.LoanReferenceNumber);
            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.Code, actual.Code);
            Assert.Equal(expected.Description, actual.Description);
            Assert.Equal(expected.Date, actual.Date);
            Assert.Equal(expected.Amount, actual.Amount);
            Assert.Equal(expected.Notes, actual.Notes);
        }
    }
}
