﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Service.Tests
{
    public class TestData
    {
        public static IOnboardingRequest AnyValidOnboardingRequest
        {
            get
            {
                return new OnboardingRequest
                {
                    ReferenceNumber = "loan001",
                    FundingSource = "crb",
                    Purpose = "Home improvement",
                    Terms = new LoanTerms {
                        ApplicationDate = DateTimeOffset.Now,
                        OriginationDate = DateTimeOffset.Now,
                        FundedDate = DateTimeOffset.Now,
                        LoanAmount = 5000.0,
                        Term = 36,
                        Rate = 8.24,
                        RateType = RateType.Percent,
                        Fees = new List<Fee> {
                            new Fee { Amount = 100, Code = "fc", Type = FeeType.Fixed }
                        }
                    },
                    Investor = new LoanInvestor { Id = "inv001", LoanPurchaseDate = DateTimeOffset.Now },
                    Scores = new List<IScore> { new Score { Name = "FICO", InitialScore = "500", InitialDate = DateTimeOffset.Now } },
                    Grade = "ABC",
                    MonthlyIncome = 10000,
                    PreCloseDti = 5,
                    PostCloseDti = 9,
                    HomeOwnership = "Owner",
                    PaymentMethod = PaymentMethod.Check,                    
                    Borrowers = new List<IBorrower>
                    {
                        new Borrower
                        {
                            ReferenceNumber = "borrower001",
                            IsPrimary = true,
                            FirstName = "John",
                            MiddleInitial = "Z",
                            LastName = "Doe",
                            DateOfBirth = new DateTime(1977, 7, 20),
                            SSN = "721-07-4426",
                            Email = "john@gmail.com",
                            Addresses = new[]
                            {
                                new Address
                                {
                                    IsPrimary = true,
                                    Line1 = "196 25th Avenue",
                                    City = "San Francisco",
                                    State = "CA",
                                    ZipCode = "94121"
                                }
                            },
                            Phones = new[]
                            {
                                new Phone
                                {
                                    IsPrimary = true,
                                    Type = PhoneType.Mobile,
                                    Number = "+12014074907"
                                }
                            }
                        }
                    }
                };
            }
        }
    }
}
