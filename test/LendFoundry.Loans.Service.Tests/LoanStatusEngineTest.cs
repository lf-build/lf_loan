﻿using System;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Repository;
using LendFoundry.Loans.Status;
using Moq;
using Xunit;

namespace LendFoundry.Loans.Service.Tests
{
    public class LoanStatusEngineTest
    {
        private Mock<ILoanRepository> mockLoanRepository = new Mock<ILoanRepository>();
        private Mock<IEventHubClient> mockEventHub = new Mock<IEventHubClient>();
        private Mock<ILogger> mockLogger = new Mock<ILogger>();
        private LoanConfiguration mockLoanConfiguration = Mock.Of<LoanConfiguration>();

        ILoanStatusEngine Engine
        {
            get
            {
                return new StatusEngine
                (
                    mockLoanRepository.Object,
                    mockEventHub.Object,
                    mockLogger.Object,
                    mockLoanConfiguration
                );
            }
        }

        [Fact]
        public void GetStatus_WhenLoanReferenceIsInvalid()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                Engine.GetStatus(string.Empty);
            });
        }

        [Fact]
        public void GetStatus_WhenLoanDoesNotExists()
        {
            Assert.Throws<LoanNotFoundException>(() =>
            {
                // arrange
                var referenceNumber = "ln001";
                mockLoanRepository.Setup(s => s.Get(referenceNumber))
                                  .Returns(It.IsAny<ILoan>());

                // act
                Engine.GetStatus(referenceNumber);

                // assert
                mockLoanRepository.Verify(s => s.Get(It.IsAny<LoanStatus>()), Times.Once);
            });
        }

        [Fact]
        public void GetStatus_WhenLoanExistsButThereIsNoConfiguration()
        {
            Assert.Throws<Exception>(() =>
            {
                // arrange
                var referenceNumber = "ln001";
                var loan = Mock.Of<ILoan>();
                mockLoanRepository.Setup(s => s.Get(referenceNumber))
                                  .Returns(loan);

                // act
                Engine.GetStatus(referenceNumber);

                // assert
                mockLoanRepository.Verify(s => s.Get(It.IsAny<string>()), Times.Once);
                mockLogger.Verify(v => v.Error(It.IsAny<string>()));
            });
        }

        [Fact]
        public void GetStatus_WhenLoanAndConfigurationExistsButStatusCodeHasNoMapping()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                // arrange
                var referenceNumber = "ln001";
                var loan = Mock.Of<ILoan>();
                loan.Status = new LoanStatus("100.03", "InService");
                mockLoanConfiguration.LoanStatus = new List<LoanStatus>
                {
                    new LoanStatus("100.01", "OnBoarding")
                };
                mockLoanRepository.Setup(s => s.Get(referenceNumber))
                                  .Returns(loan);

                // act
                Engine.GetStatus(referenceNumber);

                // assert
                mockLoanRepository.Verify(s => s.Get(It.IsAny<string>()));
            });
        }

        [Fact]
        public void GetStatus_WhenLoanAndConfigurationExists()
        {
            // arrange
            var referenceNumber = "ln001";
            var loan = Mock.Of<ILoan>();
            loan.Status = new LoanStatus("100.03", "InService")
            {
                StartDate = new DateTime(2015, 01, 01),
                EndDate = new DateTime(2015, 02, 01),
                LabelName = "from loan",
                LabelStyle = "from loan",
                Actions = new[] { Actions.AccrueInterest },
                Transitions = new[] { "100.01", "100.02" }
            };
            mockLoanConfiguration.LoanStatus = new List<LoanStatus>
            {
                new LoanStatus("100.03", "InService")
                {
                    StartDate = new DateTime(2015, 01, 02),
                    EndDate = new DateTime(2015, 02, 02),
                    LabelName = "from config",
                    LabelStyle = "from config",
                    Actions = new[] { Actions.ManualACH, Actions.ManualACH },
                    Transitions = new[] { "200.01", "200.01" }
                }
            };
            mockLoanRepository.Setup(s => s.Get(referenceNumber)).Returns(loan);

            // act
            var status = Engine.GetStatus(referenceNumber);

            // assert
            Assert.NotNull(status);
            Assert.Equal(status.Code, "100.03");
            Assert.Equal(status.Name, "InService");
            Assert.Equal(status.StartDate, new DateTime(2015, 01, 01));
            Assert.Equal(status.EndDate, new DateTime(2015, 02, 01));
            Assert.Equal(status.LabelName, "from config");
            Assert.Equal(status.LabelStyle, "from config");
            Assert.True(status.Actions.All(x => x == Actions.ManualACH));
            Assert.True(status.Transitions.All(x => x == "200.01"));
            mockLoanRepository.Verify(s => s.Get(It.IsAny<string>()));
        }

        [Fact]
        public void UpdateLoanStatus_WhenLoanReferenceIsNull()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                var loanReference = string.Empty;
                var nextStatus = "100.03";
                Engine.UpdateLoanStatus(loanReference, nextStatus, null, null);
            });
        }

        [Fact]
        public void UpdateLoanStatus_LoanDoesNotExists()
        {
            Assert.Throws<LoanNotFoundException>(() =>
            {
                var loanReference = "ln001";
                var nextStatus = "100.03";
                Engine.UpdateLoanStatus(loanReference, nextStatus, null, null);
            });
        }

        [Fact]
        public void UpdateLoanStatus_WhenNextStatusIsNull()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                var loanReference = "ln001";
                var nextStatus = string.Empty;
                Engine.UpdateLoanStatus(loanReference, nextStatus, null, null);
            });
        }

        [Fact]
        public void UpdateLoanStatus_WhenLoanConfigurationStatusIsNotFilled()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                // arrange
                var loanReference = "ln001";
                var nextStatus = "100.03";
                var loan = new Loan { };
                mockLoanRepository.Setup(s => s.Get(It.IsAny<string>())).Returns(loan);

                // act
                Engine.UpdateLoanStatus(loanReference, nextStatus, null, null);
            });
        }

        [Fact]
        public void UpdateLoanStatus_WhenNextStatusIsNotMappedInConfiguration()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                // arrange
                var loanReference = "ln001";
                var nextStatus = "100.03";
                var loan = new Loan { };
                mockLoanConfiguration.LoanStatus = new[]
                {
                    new LoanStatus("100.01", ""),
                    new LoanStatus("100.02", "")
                }.ToList();
                mockLoanRepository.Setup(s => s.Get(It.IsAny<string>())).Returns(loan);

                // act
                Engine.UpdateLoanStatus(loanReference, nextStatus, null, null);
            });
        }

        [Fact]
        public void UpdateLoanStatus_WhenNextStatusIsIlegalMove()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                // arrange
                var loanReference = "ln001";
                var nextStatus = "100.04";
                var loan = new Loan { Status = new LoanStatus("100.02", "") };
                mockLoanConfiguration.LoanStatus = new[]
                {
                    new LoanStatus("100.02", "")
                    {
                        Transitions = new[]
                        {
                            "100.03"
                        }
                    },
                    new LoanStatus("100.03", "")
                    {
                        Transitions = new[]
                        {
                            "100.04", "100.07", "100.08", "100.09", "200.01","200.02", "200.03", "200.04",
                            "200.05", "200.06","300.02", "300.05"
                        }
                    },
                    new LoanStatus("100.04", "")
                    {
                        Transitions = new[]
                        {
                            "100.03", "100.05", "100.07", "100.08", "100.09", "200.01", "200.02", "200.03",
                            "200.04", "200.05", "200.06", "300.01", "300.02", "300.05"
                        }
                    }
                }.ToList();
                mockLoanRepository.Setup(s => s.Get(It.IsAny<string>())).Returns(loan);

                // act
                Engine.UpdateLoanStatus(loanReference, nextStatus, null, null);
            });
        }

        [Fact]
        public void UpdateLoanStatus_WhenDateRangeIsInvalid()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                // arrange
                var loanReference = "ln001";
                var nextStatus = "100.03";
                var loan = new Loan { };
                var startDate = DateTimeOffset.Now.AddDays(1);
                var endDate = DateTimeOffset.Now;
                mockLoanRepository.Setup(s => s.Get(It.IsAny<string>())).Returns(loan);

                // act
                Engine.UpdateLoanStatus(loanReference, nextStatus, startDate, endDate);
            });
        }

        [Fact]
        public void UpdateLoanStatus_WhenAllEntryOk()
        {
            // arrange
            var loanReference = "ln001";
            var nextStatus = "100.07";
            var loan = new Loan { Status = new LoanStatus("100.03", "") };
            mockLoanConfiguration.LoanStatus = new[]
            {
                    new LoanStatus("100.03", "")
                    {
                        Transitions = new[]
                        {
                            "100.04", "100.07", "100.08", "100.09", "200.01","200.02", "200.03", "200.04",
                            "200.05", "200.06","300.02", "300.05"
                        }
                    },
                    new LoanStatus("100.04", "")
                    {
                        Transitions = new[]
                        {
                            "100.03", "100.05", "100.07", "100.08", "100.09", "200.01", "200.02", "200.03",
                            "200.04", "200.05", "200.06", "300.01", "300.02", "300.05"
                        }
                    },
                    new LoanStatus("100.07", "")
                }.ToList();
            mockLoanRepository.Setup(s => s.Get(It.IsAny<string>())).Returns(loan);

            // act
            Engine.UpdateLoanStatus(loanReference, nextStatus, null, null);

            // assert
            mockLoanRepository.Verify(s => s.Get(It.IsAny<string>()));
            mockLoanRepository.Verify(s => s.UpdateLoanStatus(It.IsAny<string>(), It.IsAny<LoanStatus>()));
            mockEventHub.Verify(v => v.Publish(It.IsAny<object>()));
        }
    }
}
