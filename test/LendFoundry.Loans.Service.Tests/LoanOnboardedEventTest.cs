﻿using LendFoundry.Amortization.Client;
using LendFoundry.Calendar.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Investors;
using LendFoundry.Loans.Payment;
using LendFoundry.Loans.Service.Events;
using LendFoundry.Loans.Service.Summary;
using LendFoundry.Loans.Service.Tests.Fake;
using LendFoundry.Loans.TransactionLog.Client;
using Moq;
using Xunit;

namespace LendFoundry.Loans.Service.Tests
{
    public class LoanOnboardedEventTest
    {
        private ILoanService loanService;
        private IOnboardingRequest onboardingRequest;
        private FakeEventHub eventHub;
        private FakeLoanRepository loanRepository;
        private FakeBorrowerRepository borrowerRepository;
        private FakePaymentScheduleService paymentScheduleService;

        public LoanOnboardedEventTest()
        {
            loanRepository = new FakeLoanRepository();
            borrowerRepository = new FakeBorrowerRepository();
            paymentScheduleService = new FakePaymentScheduleService();
            eventHub = new FakeEventHub();

            loanService = new LoanService(
                loanRepository,
                borrowerRepository,
                paymentScheduleService,
                eventHub,
                Mock.Of<ITransactionLogService>(),
                Mock.Of<IAmortizationService>(),
                new UtcTenantTime(),
                Mock.Of<ILogger>(),
                Mock.Of<ICalendarService>(),
                InvestorServiceMock(),
                Mock.Of<ILoanEngineResolver>(),
                Mock.Of<ILoanSummaryBuilder>(),
                new LoanConfiguration()
            );

            onboardingRequest = TestData.AnyValidOnboardingRequest;
        }

        private static IInvestorService InvestorServiceMock()
        {
            var mock = new Mock<IInvestorService>();
            mock.Setup(s => s.Exists(It.IsAny<string>())).Returns(true);
            return mock.Object;
        }

        [Fact]
        public void OneEventWasPublishedAfterOnboarding()
        {
            loanService.Onboard(onboardingRequest);

            Assert.Equal(1, eventHub.PublishedEvents.Count);
        }

        [Fact]
        public void TheEventNameIsLoanOnboarded()
        {
            loanService.Onboard(onboardingRequest);

            var publishedEvent = eventHub.PublishedEvents[0];
            Assert.Equal("LoanOnboarded", publishedEvent.EventName);
        }

        [Fact]
        public void TheEventDataContainsTheLoanReferenceNumber()
        {
            loanService.Onboard(onboardingRequest);

            var publishedEvent = eventHub.PublishedEvents[0];
            Assert.NotNull(publishedEvent.Data);
            Assert.Equal(typeof(LoanOnboarded), publishedEvent.Data.GetType());

            var loanOnboarded = publishedEvent.Data as LoanOnboarded;
            Assert.Equal(onboardingRequest.ReferenceNumber, loanOnboarded.ReferenceNumber);
        }

        [Fact]
        public void TheEventIsPublishedOnlyAfterTheLoanStatusIsChangedToOnboarded()
        {
            eventHub.OnPublish = () =>
            {
                AssertLoanStatus(LoanStatus.Onboarded);
            };

            loanService.Onboard(onboardingRequest);
        }

        [Fact]
        public void WhenInsertingLoanFails_DoNotPublishAnyEvent()
        {
            loanRepository.OnInsert = (loan) =>
            {
                throw new SomeException();
            };

            Assert.Throws<SomeException>(() =>
            {
                loanService.Onboard(onboardingRequest);
            });

            Assert.Equal(0, eventHub.PublishedEvents.Count);
        }

        [Fact]
        public void WhenStoringBorrowersFails_DoNotPublishAnyEvent()
        {
            borrowerRepository.OnStore = (borrowers, loanReferenceNumber) =>
            {
                throw new SomeException();
            };

            Assert.Throws<SomeException>(() =>
            {
                loanService.Onboard(onboardingRequest);
            });

            Assert.Equal(0, eventHub.PublishedEvents.Count);
        }

        [Fact]
        public void WhenCreatingPaymentScheduleFails_DoNotPublishAnyEvent()
        {
            paymentScheduleService.OnCreateSchedule = (loan) =>
            {
                throw new SomeException();
            };

            Assert.Throws<SomeException>(() =>
            {
                loanService.Onboard(onboardingRequest);
            });

            Assert.Equal(0, eventHub.PublishedEvents.Count);
        }

        [Fact]
        public void WhenChangingLoanStatusFails_DoNotPublishAnyEvent()
        {
            loanRepository.OnUpdateLoanStatus = () =>
            {
                throw new SomeException();
            };

            Assert.Throws<SomeException>(() =>
            {
                loanService.Onboard(onboardingRequest);
            });

            Assert.Equal(0, eventHub.PublishedEvents.Count);
        }

        private void AssertLoanStatus(LoanStatus expectedStatus)
        {
            var loan = loanService.GetLoan(onboardingRequest.ReferenceNumber);
            Assert.Same(expectedStatus, loan.Status);
        }
    }
}
