﻿using LendFoundry.Amortization.Client;
using LendFoundry.Calendar.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Investors;
using LendFoundry.Loans.Payment;
using LendFoundry.Loans.Service.Summary;
using LendFoundry.Loans.Service.Tests.Fake;
using LendFoundry.Loans.TransactionLog.Client;
using Moq;
using Xunit;

namespace LendFoundry.Loans.Service.Tests
{
    public class LoanOnboardingStatusTest
    {
        private ILoanService loanService;
        private IOnboardingRequest onboardingRequest;
        private FakeLoanRepository loanRepository;
        private FakeBorrowerRepository borrowerRepository;
        private FakePaymentScheduleService scheduleService;

        public LoanOnboardingStatusTest()
        {
            loanRepository = new FakeLoanRepository();
            borrowerRepository = new FakeBorrowerRepository();
            scheduleService = new FakePaymentScheduleService();
            loanService = new LoanService(
                loanRepository,
                borrowerRepository,
                scheduleService,
                new FakeEventHub(),
                Mock.Of<ITransactionLogService>(),
                Mock.Of<IAmortizationService>(),
                new UtcTenantTime(),
                Mock.Of<ILogger>(),
                Mock.Of<ICalendarService>(),
                InvestorServiceMock(),
                Mock.Of<ILoanEngineResolver>(),
                Mock.Of<ILoanSummaryBuilder>(),
                new LoanConfiguration()
            );
            onboardingRequest = TestData.AnyValidOnboardingRequest;
        }

        private static IInvestorService InvestorServiceMock()
        {
            var mock = new Mock<IInvestorService>();
            mock.Setup(s => s.Exists(It.IsAny<string>())).Returns(true);
            return mock.Object;
        }

        [Fact]
        public void InitialStatusIsOnboarding()
        {
            loanRepository.OnInsert = (loan) =>
            {
                Assert.Same(LoanStatus.Onboarding, loan.Status);
            };

            loanService.Onboard(onboardingRequest);
        }

        [Fact]
        public void FinalStatusIsOnboarded()
        {
            loanService.Onboard(onboardingRequest);

            AssertLoanStatus(LoanStatus.Onboarded);
        }

        [Fact]
        public void StatusIsChangedToOnboardedOnlyAtTheEndOfTheProcess()
        {
            loanRepository.OnInsert = (loan) =>
            {
                Assert.Same(LoanStatus.Onboarding, loan.Status);
            };

            borrowerRepository.OnStore = (borrowers, loanReferenceNumber) =>
            {
                AssertLoanStatus(LoanStatus.Onboarding);
            };

            scheduleService.OnCreateSchedule = (loan) =>
            {
                AssertLoanStatus(LoanStatus.Onboarding);
            };

            loanService.Onboard(onboardingRequest);

            AssertLoanStatus(LoanStatus.Onboarded);
        }

        private void AssertLoanStatus(LoanStatus expectedStatus)
        {
            var loan = loanService.GetLoan(onboardingRequest.ReferenceNumber);
            Assert.Same(expectedStatus, loan.Status);
        }
    }
}
