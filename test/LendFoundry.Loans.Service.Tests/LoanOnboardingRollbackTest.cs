﻿using LendFoundry.Amortization.Client;
using LendFoundry.Calendar.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Investors;
using LendFoundry.Loans.Payment;
using LendFoundry.Loans.Service.Summary;
using LendFoundry.Loans.Service.Tests.Fake;
using LendFoundry.Loans.TransactionLog.Client;
using Moq;
using Xunit;

namespace LendFoundry.Loans.Service.Tests
{
    public class LoanOnboardingRollbackTest
    {
        private ILoanService loanService;
        private IOnboardingRequest onboardingRequest;
        private FakeLoanRepository loanRepository;
        private FakeBorrowerRepository borrowerRepository;
        private FakePaymentScheduleService paymentScheduleService;
        private FakeEventHub eventHub;

        public LoanOnboardingRollbackTest()
        {
            loanRepository = new FakeLoanRepository();
            borrowerRepository = new FakeBorrowerRepository();
            paymentScheduleService = new FakePaymentScheduleService();
            eventHub = new FakeEventHub();

            loanService = new LoanService(
                loanRepository,
                borrowerRepository,
                paymentScheduleService,
                eventHub,
                Mock.Of<ITransactionLogService>(),
                Mock.Of<IAmortizationService>(),
                new UtcTenantTime(),
                Mock.Of<ILogger>(),
                Mock.Of<ICalendarService>(),
                InvestorServiceMock(),
                Mock.Of<ILoanEngineResolver>(),
                Mock.Of<ILoanSummaryBuilder>(),
                new LoanConfiguration()
            );

            onboardingRequest = TestData.AnyValidOnboardingRequest;
        }

        private static IInvestorService InvestorServiceMock()
        {
            var mock = new Mock<IInvestorService>();
            mock.Setup(s => s.Exists(It.IsAny<string>())).Returns(true);
            return mock.Object;
        }

        private void AssertEverythingIsDeleted()
        {
            Assert.False(loanRepository.ContainsLoan(onboardingRequest.ReferenceNumber));
            Assert.False(borrowerRepository.ContainsBorrowers(onboardingRequest.ReferenceNumber));
            Assert.False(paymentScheduleService.ContainsSchedule(onboardingRequest.ReferenceNumber));
        }

        [Fact]
        public void WhenStoringBorrowersFails_DeleteEverything()
        {
            borrowerRepository.OnStore = (borrowers, loanReferenceNumber) =>
            {
                throw new SomeException();
            };

            Assert.Throws<SomeException>(() =>
            {
                loanService.Onboard(onboardingRequest);
            });

            AssertEverythingIsDeleted();
        }

        [Fact]
        public void WhenCreatingPaymentScheduleFails_DeleteEverything()
        {
            paymentScheduleService.OnCreateSchedule = (loan) =>
            {
                throw new SomeException();
            };

            Assert.Throws<SomeException>(() =>
            {
                loanService.Onboard(onboardingRequest);
            });

            AssertEverythingIsDeleted();
        }

        [Fact]
        public void WhenChangingLoanStatusFails_DeleteEverything()
        {
            loanRepository.OnUpdateLoanStatus = () =>
            {
                throw new SomeException();
            };

            Assert.Throws<SomeException>(() =>
            {
                loanService.Onboard(onboardingRequest);
            });

            AssertEverythingIsDeleted();
        }
    }
}
