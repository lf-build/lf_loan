﻿using LendFoundry.Amortization.Client;
using LendFoundry.Borrowers.Repository;
using LendFoundry.Calendar.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Testing;
using LendFoundry.Loans.Investors;
using LendFoundry.Loans.Repository;
using LendFoundry.Loans.Schedule;
using LendFoundry.Loans.Service.Summary;
using LendFoundry.Loans.Service.Tests.Fake;
using LendFoundry.Loans.TransactionLog.Client;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LendFoundry.Loans.Service.Tests
{
    public class LoanFiltersTest
    {
        private ILoanService loanService;
        private Mock<IBorrowerRepository> borrowerRepositoryMock;
        private Mock<ILoanRepository> loanRepositoryMock;
        private Mock<IPaymentScheduleService> scheduleServiceMock;

        private List<string> loanRefNumbers;

        private Loan loan1;
        private Loan loan2;

        private List<IBorrower> borrowersOfLoan1;
        private List<IBorrower> borrowersOfLoan2;

        private PaymentScheduleSummary summaryOfLoan1;
        private PaymentScheduleSummary summaryOfLoan2;

        public LoanFiltersTest()
        {
            loanRepositoryMock = new Mock<ILoanRepository>();
            scheduleServiceMock = new Mock<IPaymentScheduleService>();
            borrowerRepositoryMock = new Mock<IBorrowerRepository>();

            loanService = new LoanService
            (
                loanRepositoryMock.Object,
                borrowerRepositoryMock.Object,
                scheduleServiceMock.Object,
                new FakeEventHub(),
                Mock.Of<ITransactionLogService>(),
                Mock.Of<IAmortizationService>(),
                new UtcTenantTime(),
                Mock.Of<ILogger>(),
                Mock.Of<ICalendarService>(),
                Mock.Of<IInvestorService>(),
                Mock.Of<ILoanEngineResolver>(),
                Mock.Of<ILoanSummaryBuilder>(),
                new LoanConfiguration()
            );

            loan1 = new Loan { ReferenceNumber = "loan001" };
            loan2 = new Loan { ReferenceNumber = "loan002" };

            borrowersOfLoan1 = new List<IBorrower>();
            borrowersOfLoan2 = new List<IBorrower>();

            summaryOfLoan2 = new PaymentScheduleSummary();
            summaryOfLoan1 = new PaymentScheduleSummary();

            loanRefNumbers = new List<string>
            {
                loan1.ReferenceNumber,
                loan2.ReferenceNumber
            };

            loanRepositoryMock
                .Setup(repository => repository.Get(loanRefNumbers))
                .Returns(new List<ILoan>
                {
                    loan1,
                    loan2
                });

            borrowerRepositoryMock
                .Setup(repository => repository.All("loan001"))
                .Returns(borrowersOfLoan1);

            borrowerRepositoryMock
                .Setup(repository => repository.All("loan002"))
                .Returns(borrowersOfLoan2);

            scheduleServiceMock
                .Setup(service => service.GetSummary("loan001"))
                .Returns(summaryOfLoan1);

            scheduleServiceMock
                .Setup(service => service.GetSummary("loan002"))
                .Returns(summaryOfLoan2);
        }

        private void AssertLoanInfos(IEnumerable<ILoanInfo> loanInfos)
        {
            Assert.NotNull(loanInfos);
            Assert.Equal(2, loanInfos.Count());

            var loanInfo1 = loanInfos.First();
            Assert.Same(loan1, loanInfo1.Loan);
            Assert.Same(borrowersOfLoan1, loanInfo1.Loan.Borrowers);
            Assert.Same(summaryOfLoan1, loanInfo1.Summary);

            var loanInfo2 = loanInfos.Last();
            Assert.Same(loan2, loanInfo2.Loan);
            Assert.Same(borrowersOfLoan2, loanInfo2.Loan.Borrowers);
            Assert.Same(summaryOfLoan2, loanInfo2.Summary);
        }

        [Fact]
        public void WithDueDatesWithinRelativeIntervalOfDays()
        {
            scheduleServiceMock
                .Setup(service => service.GetLoans(16, 30))
                .Returns(loanRefNumbers);

            var loanInfos = loanService.GetLoans
            (
                minDaysDue: 16,
                maxDaysDue: 30
            );

            AssertLoanInfos(loanInfos);
        }

        [Fact]
        public void WithDueDatesWithinRelativeIntervalOfDays_MinDaysDueCannotBeGreaterThanMaxDaysDue()
        {
            Assert.Throws<InvalidArgumentException>(() =>
                loanService.GetLoans
                (
                    minDaysDue: 2,
                    maxDaysDue: 1
                )
            );
        }

        [Fact]
        public void WithDueDatesWithinRelativeIntervalOfDays_MinDaysDueCannotBeNegative()
        {
            Assert.Throws<InvalidArgumentException>(() =>
                loanService.GetLoans
                (
                    minDaysDue: -1,
                    maxDaysDue: 1
                )
            );
        }

        [Fact]
        public void WithGivenStatus()
        {
            loanRepositoryMock
                .Setup(repository => repository.Get(LoanStatus.InService))
                .Returns(new List<ILoan>
                {
                    loan1,
                    loan2
                });

            var loanInfos = loanService.GetLoans(LoanStatus.InService);

            AssertLoanInfos(loanInfos);
        }

        [Fact]
        public void WithGivenStatus_StatusCannotBeNull()
        {
            AssertException.Throws<InvalidArgumentException>("status cannot be null", () =>
                loanService.GetLoans(null)
            );
        }

        [Fact]
        public void WithGivenStatus_StatusCodeCannotBeNullOrEmpty()
        {
            AssertException.Throws<InvalidArgumentException>("status.Code cannot be null", () =>
                loanService.GetLoans(new LoanStatus(null, "In service"))
            );

            AssertException.Throws<InvalidArgumentException>("status.Code cannot be null", () =>
                loanService.GetLoans(new LoanStatus("  ", "In service"))
            );
        }
    }
}
