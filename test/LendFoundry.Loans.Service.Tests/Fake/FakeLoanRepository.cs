﻿using LendFoundry.Loans.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Service.Tests.Fake
{
    public class FakeLoanRepository : ILoanRepository
    {
        public Action<ILoan> OnInsert { get; set; }
        public Action OnUpdateLoanStatus { get; set; }

        private IList<ILoan> Loans { get; } = new List<ILoan>();

        public ILoan Get(string loanReferenceNumber)
        {
            return Loans.First(loanRecord => loanRecord.ReferenceNumber == loanReferenceNumber);
        }

        public void Insert(ILoan loan)
        {
            if (OnInsert != null)
                OnInsert(loan);

            AssertLoanDoesNotExist(loan.ReferenceNumber);

            Loans.Add(loan);
        }

        public bool ContainsLoan(string loanReferenceNumber)
        {
            return Loans.Any(loan => loan.ReferenceNumber == loanReferenceNumber);
        }

        public void UpdateLoanStatus(string referenceNumber, LoanStatus loanStatus)
        {
            if(OnUpdateLoanStatus != null)
                OnUpdateLoanStatus();

            var loan = Get(referenceNumber);

            loan.Status = loanStatus;
        }

        public void Delete(string loanReferenceNumber)
        {
            Loans.Remove(Get(loanReferenceNumber));
        }

        private void AssertLoanDoesNotExist(string referenceNumber)
        {
            if (ContainsLoan(referenceNumber))
                    throw new DuplicateReferenceNumberException($"A loan already exists with reference number '{referenceNumber}'");
        }

        public IEnumerable<ILoan> Get(IEnumerable<string> loanReferenceNumbers)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ILoan> Get(LoanStatus status)
        {
            throw new NotImplementedException();
        }

        public void UpdateLoanBankAccounts(string referenceNumber, IEnumerable<IBankAccount> bankAccounts)
        {
            throw new NotImplementedException();
        }

        public void UpdatePaymentMethod(string referenceNumber, PaymentMethod paymentMethod)
        {
            throw new NotImplementedException();
        }
    }
}
