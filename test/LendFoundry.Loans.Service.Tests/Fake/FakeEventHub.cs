﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.EventHub.Client;

namespace LendFoundry.Loans.Service.Tests.Fake
{
    public class FakeEventHub : IEventHubClient
    {
        public IList<PublishedEvent> PublishedEvents { get; } = new List<PublishedEvent>();
        public Action OnPublish { get; set; } = () => { };

        public Task<bool> Publish<T>(string eventName, T data)
        {
            OnPublish();
            PublishedEvents.Add(new PublishedEvent(eventName, data));
            return null;
        }

	    public void PublishBatchWithInterval<T>(List<T> events, int interval = 100)
	    {
		    throw new NotImplementedException();
	    }

	    public void On(string eventName, Action<EventInfo> handler)
        {
        }

        public void Start()
        {
        }

        public void StartAsync()
        {
        }

        public Task<bool> Publish<T>(T @event)
        {
            return new Task<bool>(() => true);
        }
    }
}
