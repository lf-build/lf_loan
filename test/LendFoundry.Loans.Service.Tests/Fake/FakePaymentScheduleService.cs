﻿using System;
using System.Collections.Generic;
using LendFoundry.Loans.Schedule;
using System.Linq;
using LendFoundry.Loans.Payment;

namespace LendFoundry.Loans.Service.Tests.Fake
{
    public class FakePaymentScheduleService : IPaymentScheduleService
    {
        public Action<ILoan> OnCreateSchedule { get; set; } = loan => { };

        private IList<IPaymentSchedule> Schedules { get; } = new List<IPaymentSchedule>();

        public void Create(ILoan loan)
        {
            Create(loan, new IInstallment[] { });
        }

        public void Create(ILoan loan, params IInstallment[] installments)
        {
            OnCreateSchedule(loan);

            if (ContainsSchedule(loan.ReferenceNumber))
                throw new ArgumentException("Payment schedule already exists for loan: " + loan.ReferenceNumber);

            Schedules.Add(new PaymentSchedule
            {
                LoanReferenceNumber = loan.ReferenceNumber,
                Installments = installments.ToList()
            });
        }

        public void Delete(string loanReferenceNumber)
        {
            Schedules.Remove(Get(loanReferenceNumber));
        }

        public bool ContainsSchedule(string loanReferenceNumber)
        {
            return Schedules.Any(candidate => candidate.LoanReferenceNumber == loanReferenceNumber);
        }

        public IPaymentSchedule Get(string loanReferenceNumber)
        {
            return Schedules.First(candidate => candidate.LoanReferenceNumber == loanReferenceNumber);
        }

        public void ReconcileTransaction(string loanReferenceNumber, string transactionId, DateTime transactionDate, double amount)
        {
            throw new NotImplementedException();
        }

        public IPaymentScheduleSummary GetSummary(string loanReferenceNumber)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetLoans(int minDaysDue, int maxDaysDue)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetLoans(int minDaysDue)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetLoansDueIn(DateTimeOffset dueDate)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IPaymentSchedule> GetInstallmentsDueIn(DateTimeOffset date, PaymentMethod paymentMethod)
        {
            throw new NotImplementedException();
        }

        public void UpdateInstallmentStatus(string loanReferenceNumber, DateTimeOffset dueDate, InstallmentStatus newStatus)
        {
            throw new NotImplementedException();
        }

        public void ConfirmPayment(string loanReferenceNumber, string paymentId, DateTimeOffset date)
        {
            throw new NotImplementedException();
        }

        public void CancelPayment(string loanReferenceNumber, string paymentId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IPaymentSchedule> GetInstallmentsDueIn(DateTimeOffset date, PaymentMethod paymentMethod, InstallmentType type)
        {
            throw new NotImplementedException();
        }

        public IPayoffAmount GetPayoffAmount(string loanReferenceNumber, DateTimeOffset payoffDate)
        {
            throw new NotImplementedException();
        }

        public void UpdatePaymentMethodOfScheduledInstallments(string loanReferenceNumber, PaymentMethod newPaymentMethod)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetLoansInPayOff()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetLoansInDelinquent()
        {
            throw new NotImplementedException();
        }

        public void AddPayment(PaymentInfo payment)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IPaymentSchedule> GetInstallmentsDueInByAnniversaryDate(DateTimeOffset date, PaymentMethod paymentMethod, InstallmentType type)
        {
            throw new NotImplementedException();
        }

        public double GetCurrentDue(string loanReferenceNumber, DateTimeOffset date)
        {
            throw new NotImplementedException();
        }
    }
}
