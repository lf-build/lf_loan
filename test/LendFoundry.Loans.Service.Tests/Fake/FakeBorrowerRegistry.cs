﻿using LendFoundry.Borrowers.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace LendFoundry.Loans.Service.Tests.Fake
{
    public class FakeBorrowerRepository : IBorrowerRepository
    {
        private Dictionary<string, List<IBorrower>> Borrowers { get; } = new Dictionary<string, List<IBorrower>>();

        public Action<IList<IBorrower>, string> OnStore { get; set; } = (borrowers, loanReferenceNumber) => { };

        public IEnumerable<IBorrower> All(string loanReferenceNumber)
        {
            return GetBorrowersFor(loanReferenceNumber);
        }

        public void Store(IEnumerable<IBorrower> borrowers, string loanReferenceNumber)
        {
            OnStore(borrowers.ToList(), loanReferenceNumber);
            GetBorrowersFor(loanReferenceNumber).AddRange(borrowers);
        }

        private List<IBorrower> GetBorrowersFor(string loanReferenceNumber)
        {
            if (!Borrowers.ContainsKey(loanReferenceNumber))
                Borrowers[loanReferenceNumber] = new List<IBorrower>();

            return Borrowers[loanReferenceNumber];
        }

        public void Delete(IEnumerable<IBorrower> borrowers, string loanReferenceNumber)
        {
            Borrowers.Remove(loanReferenceNumber);
        }

        public bool ContainsBorrowers(string loanReferenceNumber)
        {
            return Borrowers.ContainsKey(loanReferenceNumber);
        }

        public IBorrower Get(string loanReferenceNumber, string borrowerReferenceNumber)
        {
            throw new NotImplementedException();
        }

        public Task Update(string loanReferenceNumber, IBorrower borrower)
        {
            throw new NotImplementedException();
        }

        public Task<IBorrower> Get(string id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IBorrower>> All(Expression<Func<IBorrower, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            throw new NotImplementedException();
        }

        public void Add(IBorrower item)
        {
            throw new NotImplementedException();
        }

        public void Remove(IBorrower item)
        {
            throw new NotImplementedException();
        }

        public void Update(IBorrower item)
        {
            throw new NotImplementedException();
        }

        public int Count(Expression<Func<IBorrower, bool>> query)
        {
            throw new NotImplementedException();
        }
    }
}
