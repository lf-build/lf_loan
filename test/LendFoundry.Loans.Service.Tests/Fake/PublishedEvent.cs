﻿namespace LendFoundry.Loans.Service.Tests.Fake
{
    public class PublishedEvent
    {
        public string EventName { get; }
        public object Data { get; }

        public PublishedEvent(string eventName, object data)
        {
            EventName = eventName;
            Data = data;
        }
    }
}
