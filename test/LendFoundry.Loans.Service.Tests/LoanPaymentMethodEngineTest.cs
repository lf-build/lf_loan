﻿using System;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Events;
using LendFoundry.Loans.Repository;
using LendFoundry.Loans.Schedule;
using Moq;
using Xunit;

namespace LendFoundry.Loans.Service.Tests
{
    public class LoanPaymentMethodEngineTest
    {
        private Mock<IEventHubClient> mockEventHub = new Mock<IEventHubClient>();
        private Mock<IPaymentScheduleService> mockPaymentSchedule = new Mock<IPaymentScheduleService>();
        private Mock<ILoanRepository> mockLoanRepository = new Mock<ILoanRepository>();
        private Mock<ILogger> mockLogger = new Mock<ILogger>();
        private Mock<ITenantTime> mockTenantTime = new Mock<ITenantTime>();

        IPaymentMethodEngine Engine
        {
            get
            {
                return new PaymentMethodEngine
                (
                    mockLoanRepository.Object,
                    mockEventHub.Object,
                    mockLogger.Object,
                    mockPaymentSchedule.Object,
                    mockTenantTime.Object
                );
            }
        }

        [Fact]
        public void Change_WhenHasInvalidReferenceNumber()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                Engine.Change(string.Empty, PaymentMethod.ACH, null);
            });
        }

        [Fact]
        public void Change_WhenHasInvalidPaymentMethod()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                Engine.Change(string.Empty, PaymentMethod.Undefined, null);
            });
        }

        [Fact]
        public void Change_WhenNewPaymentMethodIsTheActiveOne()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                // arrange
                var loan = new Loan { PaymentMethod = PaymentMethod.ACH };
                mockLoanRepository.Setup(s => s.Get(It.IsAny<string>())).Returns(loan);

                //act
                Engine.Change("ln001", PaymentMethod.ACH, null);
            });
        }

        [Fact]
        public void Change_WhenLoanNotFound()
        {
            Assert.Throws<LoanNotFoundException>(() =>
            {
                // arrange
                var loan = It.IsAny<ILoan>();
                mockLoanRepository.Setup(s => s.Get(It.IsAny<string>())).Returns(loan);

                //act
                Engine.Change("ln001", PaymentMethod.ACH, null);
            });
        }

        [Fact]
        public void Change_AchToCheckWhenItHasScheduledExtraPayment()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                // arrange
                var loan = new Loan { PaymentMethod = PaymentMethod.ACH };
                var paymentSchedule = new PaymentSchedule
                {
                    Installments = new[]
                    {
                        new Installment { Type = InstallmentType.Additional, Status = InstallmentStatus.Scheduled },
                        new Installment { Type = InstallmentType.Scheduled, Status = InstallmentStatus.Scheduled },
                        new Installment { Type = InstallmentType.Scheduled, Status = InstallmentStatus.Scheduled }
                    }
                };
                mockLoanRepository.Setup(s => s.Get(It.IsAny<string>())).Returns(loan);
                mockPaymentSchedule.Setup(s => s.Get(It.IsAny<string>())).Returns(paymentSchedule);

                //act
                Engine.Change("ln001", PaymentMethod.Check, null);
            });
        }

        [Fact]
        public void Change_AchToCheckWhenItHasCompletedExtraPayment()
        {
            // arrange
            var loan = new Loan { ReferenceNumber = "ln001", PaymentMethod = PaymentMethod.ACH };
            var dueDate = new DateTimeOffset(new DateTime(2016, 03, 11));
            var currentDate = new DateTimeOffset(new DateTime(2016, 03, 07));
            var paymentSchedule = new PaymentSchedule
            {
                Installments = new[]
                {
                    new Installment { DueDate = dueDate.AddMonths(-1), Type = InstallmentType.Additional, Status = InstallmentStatus.Completed },
                    new Installment { DueDate = dueDate,               Type = InstallmentType.Scheduled,  Status = InstallmentStatus.Scheduled },
                    new Installment { DueDate = dueDate.AddMonths(1),  Type = InstallmentType.Scheduled,  Status = InstallmentStatus.Scheduled }
                }
            };
            mockLoanRepository.Setup(s => s.Get("ln001")).Returns(loan);
            mockPaymentSchedule.Setup(s => s.Get("ln001")).Returns(paymentSchedule);
            mockTenantTime.Setup(s => s.Now).Returns(currentDate);

            //act
            Engine.Change("ln001", PaymentMethod.Check, (msg) =>
            {
                Assert.Contains("Payment Method switched from ACH to Check", msg);
                mockLoanRepository.Verify(v => v.UpdatePaymentMethod("ln001", PaymentMethod.Check));
                mockEventHub.Verify(v => v.Publish(It.IsAny<PaymentMethodChanged>()));
            });
        }

        [Fact]
        public void Change_AchToCheckWhenItHasPayoff()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                // arrange
                var loan = new Loan { PaymentMethod = PaymentMethod.ACH };
                var paymentSchedule = new PaymentSchedule
                {
                    Installments = new[]
                    {
                        new Installment { Type = InstallmentType.Payoff },
                        new Installment { Type = InstallmentType.Scheduled },
                        new Installment { Type = InstallmentType.Scheduled }
                    }
                };
                mockLoanRepository.Setup(s => s.Get(It.IsAny<string>())).Returns(loan);
                mockPaymentSchedule.Setup(s => s.Get(It.IsAny<string>())).Returns(paymentSchedule);

                //act
                Engine.Change("ln001", PaymentMethod.Check, null);
            });
        }

        [Fact]
        public void Change_AchToCheck_WhenAchWindowIsNotValid()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                // arrange
                var loan = new Loan { PaymentMethod = PaymentMethod.ACH };
                var dueDate = new DateTimeOffset(new DateTime(2016, 03, 11));
                var currentDate = new DateTimeOffset(new DateTime(2016, 03, 08));
                var paymentSchedule = new PaymentSchedule
                {
                    Installments = new[]
                    {
                        new Installment { DueDate = dueDate, Type = InstallmentType.Scheduled },
                    }
                };
                mockLoanRepository.Setup(s => s.Get(It.IsAny<string>())).Returns(loan);
                mockPaymentSchedule.Setup(s => s.Get(It.IsAny<string>())).Returns(paymentSchedule);
                mockTenantTime.Setup(s => s.Now).Returns(currentDate);

                //act
                Engine.Change("ln001", PaymentMethod.Check, null);
            });
        }

        [Fact]
        public void Change_AchToCheckWhenItHasValidInformation()
        {
            // arrange
            var loan = new Loan { PaymentMethod = PaymentMethod.ACH };
            var dueDate = new DateTimeOffset(new DateTime(2016, 03, 11));
            var currentDate = new DateTimeOffset(new DateTime(2016, 03, 07));
            var paymentSchedule = new PaymentSchedule
            {
                Installments = new[]
                {
                    new Installment { DueDate = dueDate, Type = InstallmentType.Scheduled },
                    new Installment { DueDate = dueDate.AddMonths(1), Type = InstallmentType.Scheduled },
                    new Installment { DueDate = dueDate.AddMonths(2), Type = InstallmentType.Scheduled }
                }
            };
            mockLoanRepository.Setup(s => s.Get(It.IsAny<string>())).Returns(loan);
            mockPaymentSchedule.Setup(s => s.Get(It.IsAny<string>())).Returns(paymentSchedule);
            mockTenantTime.Setup(s => s.Now).Returns(currentDate);

            // act
            Engine.Change("ln001", PaymentMethod.Check, (msg) =>
            {
                // assert
                Assert.Contains("Payment Method switched from ACH to Check", msg);
                mockLoanRepository.Verify(s => s.Get(It.IsAny<string>()));
                mockPaymentSchedule.Verify(s => s.Get(It.IsAny<string>()));
                mockLoanRepository.Verify(v => v.UpdatePaymentMethod(It.IsAny<string>(), PaymentMethod.Check));
                mockEventHub.Verify(v => v.Publish(It.IsAny<PaymentMethodChanged>()));
            });
        }

        [Fact]
        public void Change_CheckToAchWhenItHasNoBankAccount()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                // arrange
                var loan = new Loan { PaymentMethod = PaymentMethod.Check };
                var paymentSchedule = new PaymentSchedule
                {
                    Installments = new[]
                    {
                        new Installment { Type = InstallmentType.Scheduled },
                        new Installment { Type = InstallmentType.Scheduled },
                        new Installment { Type = InstallmentType.Scheduled }
                    }
                };
                mockLoanRepository.Setup(s => s.Get(It.IsAny<string>())).Returns(loan);
                mockPaymentSchedule.Setup(s => s.Get(It.IsAny<string>())).Returns(paymentSchedule);

                //act
                Engine.Change("ln001", PaymentMethod.ACH, null);
            });
        }

        [Fact]
        public void Change_CheckToAchWhenItHasBankAccount()
        {
            // arrange
            var loan = new Loan
            {
                PaymentMethod = PaymentMethod.Check,
                BankAccounts = new[]
                {
                    new BankAccount { AccountNumber = "333", RoutingNumber = "444", IsPrimary = true },
                    new BankAccount { AccountNumber = "555", RoutingNumber = "666", IsPrimary = false },
                }
            };
            var paymentSchedule = new PaymentSchedule
            {
                Installments = new[]
                {
                    new Installment { Type = InstallmentType.Scheduled },
                    new Installment { Type = InstallmentType.Scheduled },
                    new Installment { Type = InstallmentType.Scheduled }
                }
            };
            mockLoanRepository.Setup(s => s.Get(It.IsAny<string>())).Returns(loan);
            mockPaymentSchedule.Setup(s => s.Get(It.IsAny<string>())).Returns(paymentSchedule);

            // act
            Engine.Change("ln001", PaymentMethod.ACH, (msg) =>
            {
                // assert
                Assert.Contains("Payment Method switched from Check to ACH", msg);
                mockLoanRepository.Verify(s => s.Get(It.IsAny<string>()));
                mockPaymentSchedule.Verify(s => s.Get(It.IsAny<string>()), Times.Never);
                mockLoanRepository.Verify(v => v.UpdatePaymentMethod(It.IsAny<string>(), PaymentMethod.ACH));
                mockEventHub.Verify(v => v.Publish(It.IsAny<PaymentMethodChanged>()));
            });
        }
    }
}
