﻿using LendFoundry.Amortization.Client;
using LendFoundry.Calendar.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Investors;
using LendFoundry.Loans.Schedule;
using LendFoundry.Loans.Client;
using LendFoundry.Loans.Service.Tests.Fake;
using LendFoundry.Loans.TransactionLog.Client;
using Moq;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using Xunit;
using LendFoundry.Loans.Service.Summary;

namespace LendFoundry.Loans.Service.Tests
{
    public class GetLoanInfoTest
    {
        private const string LoanReferenceNumber = "loan001";

        private ILoanService loanService;

        private FakeBorrowerRepository borrowerRepository;

        private Loan loan;
        private Installment firstScheduledInstallment;
        private Installment lastPaidInstallment;
        private Installment latestInstallment;
        private PaymentScheduleSummary paymentScheduleSummary;

        public GetLoanInfoTest()
        {
            var loanRepository = new FakeLoanRepository();
            var paymentScheduleServiceMock = new Mock<IPaymentScheduleService>();
            borrowerRepository = new FakeBorrowerRepository();

            loanService = new LoanService(
                loanRepository,
                borrowerRepository,
                paymentScheduleServiceMock.Object,
                new FakeEventHub(),
                Mock.Of<ITransactionLogService>(),
                Mock.Of<IAmortizationService>(),
                new UtcTenantTime(),
                Mock.Of<ILogger>(),
                Mock.Of<ICalendarService>(),
                Mock.Of<IInvestorService>(),
                Mock.Of<ILoanEngineResolver>(),
                Mock.Of<ILoanSummaryBuilder>(),
                new LoanConfiguration()                              
            );

            loan = new Loan
            {
                ReferenceNumber = LoanReferenceNumber,
                Terms = new LoanTerms(),
                Borrowers = new List<IBorrower>()
            };

            lastPaidInstallment = new Installment { Status = InstallmentStatus.Completed };
            latestInstallment = new Installment { Status = InstallmentStatus.Pending };
            firstScheduledInstallment = new Installment { Status = InstallmentStatus.Scheduled };

            loanRepository.Insert(loan);

            paymentScheduleSummary = new PaymentScheduleSummary();

            paymentScheduleServiceMock
                .Setup(scheduleService => scheduleService.GetSummary(LoanReferenceNumber))
                .Returns(paymentScheduleSummary);
        }

        private static ILoan CreateLoanMock()
        {
            Mock<ILoan> loanMock = new Mock<ILoan>();
            loanMock.Setup(loan => loan.ReferenceNumber).Returns(LoanReferenceNumber);
            return loanMock.Object;
        }

        [Fact]
        public void LoanInfoIsNotNull()
        {
            var loanInfo = loanService.GetLoanInfo(LoanReferenceNumber);
            Assert.NotNull(loanInfo);
        }

        [Fact]
        public void GetTheLoanFromTheRepository()
        {
            var loanInfo = loanService.GetLoanInfo(LoanReferenceNumber);
            Assert.Same(loan, loanInfo.Loan);
        }

        [Fact]
        public void GetTheSummaryInformationFromThePaymentScheduleService()
        {
            var loanInfo = loanService.GetLoanInfo(LoanReferenceNumber);
            Assert.Same(paymentScheduleSummary, loanInfo.Summary);
        }
        
        [Fact]
        public void GetBorrowersFromBorrowerRepository()
        {
            var expectedBorrowers = new List<IBorrower>
            {
                Mock.Of<IBorrower>(),
                Mock.Of<IBorrower>()
            };

            borrowerRepository.Store(expectedBorrowers, LoanReferenceNumber);

            var loanInfo = loanService.GetLoanInfo(LoanReferenceNumber);

            var actualBorrowers = loanInfo.Loan.Borrowers;
            Assert.Equal(2, actualBorrowers.Count());
            Assert.Same(expectedBorrowers[0], actualBorrowers.First());
            Assert.Same(expectedBorrowers[1], actualBorrowers.Last());
        }

        //[Fact]
        //public void Client_GetLoanDetails()
        //{
        //    // Loan host
        //    string host = "192.168.99.100";
        //    int port = 5007;

        //    // Token generation
        //    var tokenHandler = new TokenHandler(null, null, null);
        //    var token = tokenHandler.Issue("my-tenant", "some-app");
        //    var reader = new StaticTokenReader(token.Value);

        //    // Service client generation
        //    IServiceClient serviceClient = new ServiceClient(
        //        accessor: new EmptyHttpContextAccessor(),
        //        logger: Mock.Of<ILogger>(),
        //        tokenReader: reader,
        //        endpoint: host,
        //        port: port
        //    );

        //    var logger = new Mock<ILogger>();

        //    var loanServiceClient = new LendFoundry.Loans.Client.LoanService(serviceClient, logger.Object);

        //    loanServiceClient.GetLoanDetails("0001");
        //}
    }
}