﻿using System;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Amortization.Client;
using LendFoundry.Borrowers.Repository;
using LendFoundry.Calendar.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Events;
using LendFoundry.Loans.Investors;
using LendFoundry.Loans.Repository;
using LendFoundry.Loans.Schedule;
using LendFoundry.Loans.TransactionLog.Client;
using Moq;
using Xunit;
using LendFoundry.Loans.Service.Summary;

namespace LendFoundry.Loans.Service.Tests
{
    public class LoanBankAccountEngineTest
    {
        private ILoanService loanService = null;
        private Mock<ILoanRepository> loanRepositoryMock = new Mock<ILoanRepository>();
        private Mock<IBorrowerRepository> borrowerRepositoryMock = new Mock<IBorrowerRepository>();
        private Mock<IPaymentScheduleService> paymentScheduleMock = new Mock<IPaymentScheduleService>();
        private Mock<IEventHubClient> eventhubMock = new Mock<IEventHubClient>();
        private Mock<ITransactionLogService> transactionMock = new Mock<ITransactionLogService>();
        private Mock<IAmortizationService> amortizationMock = new Mock<IAmortizationService>();
        private Mock<ITenantTime> tenantTimeMock = new Mock<ITenantTime>();
        private Mock<ILogger> loggerMock = new Mock<ILogger>();
        private Mock<ICalendarService> calendarMock = new Mock<ICalendarService>();
        private Mock<IInvestorService> investorMock = new Mock<IInvestorService>();
        private Mock<ILoanStatusEngine> loanStatusMock = new Mock<ILoanStatusEngine>();
        LoanConfiguration loanConfigMock = Mock.Of<LoanConfiguration>();

        BankAccount BankAccount
        {
            get
            {
                return new BankAccount
                {
                    AccountNumber = "123",
                    AccountType = BankAccountType.Checking,
                    BankName = "Some Name",
                    EffectiveDate = DateTimeOffset.Now,
                    IsPrimary = true,
                    RoutingNumber = "123"
                };
            }
        }

        IBankAccountEngine Engine
        {
            get
            {
                return new BankAccountEngine(loanRepositoryMock.Object, eventhubMock.Object, new UtcTenantTime(), loggerMock.Object);
            }
        }

        public LoanBankAccountEngineTest()
        {
            loanService = new LoanService
            (
                loanRepositoryMock.Object,
                borrowerRepositoryMock.Object,
                paymentScheduleMock.Object,
                eventhubMock.Object,
                transactionMock.Object,
                amortizationMock.Object,
                tenantTimeMock.Object,
                loggerMock.Object,
                calendarMock.Object,
                investorMock.Object,
                Mock.Of<ILoanEngineResolver>(),
                Mock.Of<ILoanSummaryBuilder>(),
                loanConfigMock                             
            );
        }

        [Fact]
        public void AddBankAccount_WhenHasNoAccount()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                var loanReferenceNumber = "123456";

                // act
                Engine.Create(loanReferenceNumber, null);
            });
        }

        [Fact]
        public void AddBankAccount_WhenHasNoRoutingNumber()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                var loanReferenceNumber = "123456";
                var objBank = BankAccount;
                objBank.RoutingNumber = string.Empty;

                // act
                Engine.Create(loanReferenceNumber, objBank);
            });
        }

        [Fact]
        public void AddBankAccount_WhenHasNoAccountNumber()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                var loanReferenceNumber = "123456";
                var objBank = BankAccount;
                objBank.AccountNumber = string.Empty;

                // act
                Engine.Create(loanReferenceNumber, objBank);
            });
        }

        [Fact]
        public void AddBankAccount_WhenHasNoAccountType()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                var loanReferenceNumber = "123456";
                var objBank = BankAccount;
                objBank.AccountType = BankAccountType.Undefined;

                // act
                Engine.Create(loanReferenceNumber, objBank);
            });
        }

        [Fact]
        public void AddBankAccount_WhenLoanDoesNotExists()
        {
            Assert.Throws<LoanNotFoundException>(() =>
            {
                Engine.Create("123456", BankAccount);
            });
        }

        [Fact]
        public void AddBankAccount_WhenNoLoanReferenceNumberInformed()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                Engine.Create(string.Empty, It.IsAny<IBankAccount>());
            });
        }

        [Fact]
        public void AddBankAccount_AllowMultipleBankAccountsWithSameAccountNumberButDifferentRoutingNumbers()
        {
            var bankAccount1 = new BankAccount
            {
                AccountNumber = "account-A",
                AccountType = BankAccountType.Checking,
                BankName = "Bank of America",
                EffectiveDate = new DateTimeOffset(2016, 1, 15, 0, 0, 0, TimeSpan.Zero),
                IsPrimary = true,
                RoutingNumber = "routing-number-1"
            };

            var loan = new Loan { BankAccounts = new[] { bankAccount1 } };

            loanRepositoryMock
                .Setup(s => s.Get("loan-1"))
                .Returns(loan);

            borrowerRepositoryMock
                .Setup(s => s.All("loan-1"))
                .Returns(It.IsAny<IEnumerable<IBorrower>>());

            var bankAccount2 = new BankAccount
            {
                AccountNumber = "account-A",
                AccountType = BankAccountType.Checking,
                BankName = "Chase",
                EffectiveDate = new DateTimeOffset(2016, 2, 15, 0, 0, 0, TimeSpan.Zero),
                IsPrimary = false,
                RoutingNumber = "routing-number-2"
            };

            Engine.Create("loan-1", bankAccount2);

            loanRepositoryMock.Verify(r => r.UpdateLoanBankAccounts("loan-1", new[] { bankAccount1, bankAccount2 }));
        }

        [Fact]
        public void AddBankAccount_DoNotAllowMultipleBankAccountsWithSameAccountAndRoutingNumbers()
        {
            var bankAccount1 = new BankAccount
            {
                AccountNumber = "account-A",
                AccountType = BankAccountType.Checking,
                BankName = "Bank of America",
                EffectiveDate = new DateTimeOffset(2016, 1, 15, 0, 0, 0, TimeSpan.Zero),
                IsPrimary = true,
                RoutingNumber = "routing-number-1"
            };

            var loan = new Loan { BankAccounts = new[] { bankAccount1 } };

            loanRepositoryMock
                .Setup(s => s.Get("loan-1"))
                .Returns(loan);

            borrowerRepositoryMock
                .Setup(s => s.All("loan-1"))
                .Returns(It.IsAny<IEnumerable<IBorrower>>());

            var bankAccount2 = new BankAccount
            {
                AccountNumber = "account-A",
                AccountType = BankAccountType.Checking,
                BankName = "Chase",
                EffectiveDate = new DateTimeOffset(2016, 2, 15, 0, 0, 0, TimeSpan.Zero),
                IsPrimary = false,
                RoutingNumber = "routing-number-1"
            };

            Assert.Throws<InvalidArgumentException>(() =>
            {
                Engine.Create("loan-1", bankAccount2);
            });

            loanRepositoryMock.Verify(
                r => r.UpdateLoanBankAccounts(It.IsAny<string>(), It.IsAny<IEnumerable<IBankAccount>>()),
                Times.Never);
        }

        [Fact]
        public void AddBankAccount_WhenLoanHasNoBankAccount()
        {
            // arrange
            var loanReferenceNumber = "123456";
            var loan = new Loan { };

            loanRepositoryMock.Setup(s => s.Get(It.IsAny<string>())).Returns(loan);
            loanRepositoryMock.Setup(s => s.UpdateLoanBankAccounts(It.IsAny<string>(), It.IsAny<IList<IBankAccount>>()));
            borrowerRepositoryMock.Setup(s => s.All(It.IsAny<string>())).Returns(It.IsAny<IEnumerable<IBorrower>>());
            eventhubMock.Setup(s => s.Publish(It.IsAny<LoanPrimaryBankAccountChanged>()));

            var newBankAccount = BankAccount;
            newBankAccount.IsPrimary = false;
            newBankAccount.EffectiveDate = DateTimeOffset.MinValue;

            // act
            Engine.Create(loanReferenceNumber, newBankAccount);

            // assert
            Assert.True(newBankAccount.IsPrimary);
            Assert.True(newBankAccount.EffectiveDate > DateTimeOffset.MinValue);
            loanRepositoryMock.Verify(v => v.UpdateLoanBankAccounts(It.IsAny<string>(), It.IsAny<IList<IBankAccount>>()), Times.Once);
            eventhubMock.Verify(s => s.Publish(It.IsAny<LoanPrimaryBankAccountChanged>()), Times.Once);
        }

        [Fact]
        public void AddBankAccount_WhenLoanAlreadyHasBankAccount_AndNewOneIsPrimary()
        {
            // arrange
            var loanReferenceNumber = "123456";
            var loan = new Loan { BankAccounts = new[] { BankAccount } };

            loanRepositoryMock.Setup(s => s.Get(It.IsAny<string>())).Returns(loan);
            loanRepositoryMock.Setup(s => s.UpdateLoanBankAccounts(It.IsAny<string>(), It.IsAny<IList<IBankAccount>>()));
            borrowerRepositoryMock.Setup(s => s.All(It.IsAny<string>())).Returns(It.IsAny<IEnumerable<IBorrower>>());
            eventhubMock.Setup(s => s.Publish(It.IsAny<LoanPrimaryBankAccountChanged>()));

            var newBankAccount = new BankAccount
            {
                AccountNumber = "222222",
                AccountType = BankAccountType.Checking,
                BankName = "Some Name",
                EffectiveDate = DateTimeOffset.MinValue,
                IsPrimary = true,
                RoutingNumber = "222222"
            };

            // act
            Engine.Create(loanReferenceNumber, newBankAccount);

            // assert
            Assert.True(newBankAccount.IsPrimary);
            Assert.True(newBankAccount.EffectiveDate > DateTimeOffset.MinValue);
            loanRepositoryMock.Verify(v => v.UpdateLoanBankAccounts(It.IsAny<string>(), It.IsAny<IList<IBankAccount>>()), Times.Once);
            eventhubMock.Verify(s => s.Publish(It.IsAny<LoanPrimaryBankAccountChanged>()), Times.Once);
        }

        [Fact]
        public void AddBankAccount_WhenLoanAlreadyHasBankAccount_AndNewOneIsNotPrimary()
        {
            // arrange
            var loanReferenceNumber = "123456";
            var loan = new Loan { BankAccounts = new[] { BankAccount } };

            loanRepositoryMock.Setup(s => s.Get(It.IsAny<string>())).Returns(loan);
            loanRepositoryMock.Setup(s => s.UpdateLoanBankAccounts(It.IsAny<string>(), It.IsAny<IList<IBankAccount>>()));
            borrowerRepositoryMock.Setup(s => s.All(It.IsAny<string>())).Returns(It.IsAny<IEnumerable<IBorrower>>());
            eventhubMock.Setup(s => s.Publish(It.IsAny<LoanPrimaryBankAccountChanged>()));

            var newBankAccount = new BankAccount
            {
                AccountNumber = "222222",
                AccountType = BankAccountType.Checking,
                BankName = "Some Name",
                EffectiveDate = DateTimeOffset.MinValue,
                IsPrimary = false,
                RoutingNumber = "222222"
            };

            // act
            Engine.Create(loanReferenceNumber, newBankAccount);

            // assert
            Assert.False(newBankAccount.IsPrimary);
            Assert.True(newBankAccount.EffectiveDate > DateTimeOffset.MinValue);
            loanRepositoryMock.Verify(v => v.UpdateLoanBankAccounts(It.IsAny<string>(), It.IsAny<IList<IBankAccount>>()), Times.Once);
            eventhubMock.Verify(s => s.Publish(It.IsAny<LoanPrimaryBankAccountChanged>()), Times.Never);
        }

        [Fact]
        public void SetBankAccountAsPrimary_WhenLoanReferenceNumberNotInformed()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                Engine.SetAsPrimary(string.Empty, "123456");
            });
        }

        [Fact]
        public void SetBankAccountAsPrimary_WhenAccountNumberNotInformed()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                Engine.SetAsPrimary("123456", string.Empty);
            });
        }

        [Fact]
        public void SetBankAccountAsPrimary_WhenLoanDoesNotExists()
        {
            Assert.Throws<LoanNotFoundException>(() =>
            {
                Engine.SetAsPrimary("999999", "123456");
            });
        }

        [Fact]
        public void SetBankAccountAsPrimary_WhenLoanHasNoBankAccounts()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                // arrange
                var loanReferenceNumber = "123456";
                var loan = new Loan { };

                loanRepositoryMock.Setup(s => s.Get(It.IsAny<string>())).Returns(loan);
                loanRepositoryMock.Setup(s => s.UpdateLoanBankAccounts(It.IsAny<string>(), It.IsAny<IList<IBankAccount>>()));
                borrowerRepositoryMock.Setup(s => s.All(It.IsAny<string>())).Returns(It.IsAny<IEnumerable<IBorrower>>());

                // act
                Engine.SetAsPrimary(loanReferenceNumber, "123456");
            });
        }

        [Fact]
        public void SetBankAccountAsPrimary_WhenInformedAccountNumberAlreadyIsThePrimary()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                var loanReferenceNumber = "123456";
                var loan = new Loan { BankAccounts = new[] { BankAccount } };

                loanRepositoryMock.Setup(s => s.Get(It.IsAny<string>())).Returns(loan);
                loanRepositoryMock.Setup(s => s.UpdateLoanBankAccounts(It.IsAny<string>(), It.IsAny<IList<IBankAccount>>()));
                borrowerRepositoryMock.Setup(s => s.All(It.IsAny<string>())).Returns(It.IsAny<IEnumerable<IBorrower>>());

                // act
                Engine.SetAsPrimary(loanReferenceNumber, "123");
            });
        }

        [Fact]
        public void SetBankAccountAsPrimary_WhenInformedAccountNumberDoesNotExists()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                var loanReferenceNumber = "123456";
                var loan = new Loan { BankAccounts = new[] { BankAccount } };

                loanRepositoryMock.Setup(s => s.Get(It.IsAny<string>())).Returns(loan);
                loanRepositoryMock.Setup(s => s.UpdateLoanBankAccounts(It.IsAny<string>(), It.IsAny<IList<IBankAccount>>()));
                borrowerRepositoryMock.Setup(s => s.All(It.IsAny<string>())).Returns(It.IsAny<IEnumerable<IBorrower>>());

                // act
                Engine.SetAsPrimary(loanReferenceNumber, "999");
            });
        }

        [Fact]
        public void SetBankAccountAsPrimary_WhenAllOK()
        {
            // arrange
            var loanReferenceNumber = "123456";
            var loan = new Loan { BankAccounts = new[] { BankAccount, BankAccount } };
            loan.BankAccounts = loan.BankAccounts.Select((el, pos) =>
            {
                el.AccountNumber = $"00{pos}";
                el.IsPrimary = pos == 0;
                return el;
            }).ToList();

            loanRepositoryMock.Setup(s => s.Get(It.IsAny<string>())).Returns(loan);
            loanRepositoryMock.Setup(s => s.UpdateLoanBankAccounts(It.IsAny<string>(), It.IsAny<IList<IBankAccount>>()));
            borrowerRepositoryMock.Setup(s => s.All(It.IsAny<string>())).Returns(It.IsAny<IEnumerable<IBorrower>>());
            eventhubMock.Setup(s => s.Publish(It.IsAny<LoanPrimaryBankAccountChanged>()));

            // act
            Engine.SetAsPrimary(loanReferenceNumber, "001");

            // assert
            loanRepositoryMock.Verify(v => v.UpdateLoanBankAccounts(It.IsAny<string>(), It.IsAny<IList<IBankAccount>>()), Times.Once);
            eventhubMock.Verify(s => s.Publish(It.IsAny<LoanPrimaryBankAccountChanged>()), Times.Once);
        }

        [Fact]
        public void AccountNumberIsUniquePerLoan()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                var loanReferenceNumber = "123456";
                var account1 = BankAccount;
                var account2 = BankAccount;
                var account3 = BankAccount;
                account1.AccountNumber = "1";
                account2.AccountNumber = "2";
                account3.AccountNumber = "2";
                var loan = new Loan
                {
                    BankAccounts = new[] { account1, account2 }
                };

                loanRepositoryMock.Setup(s => s.Get(It.IsAny<string>())).Returns(loan);

                // act
                Engine.Create(loanReferenceNumber, account3);
            });
        }

    }
}
