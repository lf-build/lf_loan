﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Api.Controllers;
using LendFoundry.Loans.Service;
using Microsoft.AspNet.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LendFoundry.Loans.Api.Tests
{
    public class LoansApiControllerTest
    {
        Mock<ILogger> Logger;
        Mock<ILoanService> LoanService;

        public LoansApiControllerTest()
        {
            Logger = new Mock<ILogger>();
            LoanService = new Mock<ILoanService>();
        }

        [Fact(Skip ="needs refactoring")]
        public void WhenOnboard_HasListOfLoansWithInvalidLoan_ReturnBadRequest()
        {
            var listLoan = Helper.GetLoansMock();
            listLoan = listLoan.Select(lo =>
            {
                lo.Borrowers = null;
                return lo;
            }).ToList();
            var obj = new LoanController(Logger.Object, LoanService.Object, Mock.Of<IEventHubClient>());
            var result = (ErrorResult)obj.Onboard(listLoan);

            Assert.Equal(400, result.StatusCode);
            LoanService.Verify(v => v.Onboard(It.IsAny<OnboardingRequest>()), Times.Never);
        }

        [Fact]
        public void WhenOnboard_HasListOfLoans_SaveIt()
        {
            LoanService.Setup(s => s.Onboard(It.IsAny<OnboardingRequest>()));
            LoanService.Setup(s => s.GetLoan(It.IsAny<string>())).Returns(new Loan());

            var listLoan = Helper.GetLoansMock();
            var obj = new LoanController(Logger.Object, LoanService.Object, Mock.Of<IEventHubClient>());
            var result = (HttpStatusCodeResult)obj.Onboard(listLoan);

            Assert.Equal(200, result.StatusCode);
            LoanService.Verify(v => v.Onboard(It.IsAny<OnboardingRequest>()), Times.Exactly(listLoan.Count));
        }

        [Fact(Skip = "needs refactoring")]
        public void WhenRetrieveLoanAndTransactionInfo_HasLoan_ReturnLoanTransactionAndInstallments()
        {
            LoanService.Setup(s => s.GetLoanInfo(It.IsAny<string>()))
                       .Returns(Mock.Of<ILoanInfo>());
            LoanService.Setup(s => s.GetPaymentSchedule(It.IsAny<string>()))
                     .Returns(Mock.Of<IPaymentSchedule>());

            var obj = new LoanController(Logger.Object, LoanService.Object, Mock.Of<IEventHubClient>());
            var result = (HttpStatusCodeResult)obj.GetLoanDetails("123");

            Assert.Equal(200, result.StatusCode);
            LoanService.Verify(v => v.GetLoanInfo(It.IsAny<string>()), Times.Once);
            LoanService.Verify(v => v.GetTransactions(It.IsAny<string>()), Times.Once);
            LoanService.Verify(v => v.GetPaymentSchedule(It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void WhenRetrieveLoanInfo_HasLoan_ReturnLoanInfo()
        {
            LoanService.Setup(s => s.GetLoanInfo(It.IsAny<string>()))
                       .Returns(Mock.Of<ILoanInfo>());

            var obj = new LoanController(Logger.Object, LoanService.Object, Mock.Of<IEventHubClient>());
            var result = (ObjectResult)obj.GetLoanInfo("123");

            Assert.Equal(200, result.StatusCode);
            LoanService.Verify(v => v.GetLoanInfo(It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void WhenRetrieveTransactionInfo_HasLoan_ReturnListOfTransactions()
        {
            LoanService.Setup(s => s.GetTransactions(It.IsAny<string>()))
                       .Returns(new List<Transaction>());

            var obj = new LoanController(Logger.Object, LoanService.Object, Mock.Of<IEventHubClient>());
            var result = (ObjectResult)obj.GetTransactions("123");

            Assert.Equal(200, result.StatusCode);
            LoanService.Verify(v => v.GetTransactions(It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void WhenRetrievePaymentScheduleInfo_HasLoan_ReturnListOfInstallments()
        {
            LoanService.Setup(s => s.GetPaymentSchedule(It.IsAny<string>()))
                       .Returns(Mock.Of<IPaymentSchedule>());

            var obj = new LoanController(Logger.Object, LoanService.Object, Mock.Of<IEventHubClient>());
            var result = (ObjectResult)obj.GetPaymentSchedule("123");

            Assert.Equal(200, result.StatusCode);
            LoanService.Verify(v => v.GetPaymentSchedule(It.IsAny<string>()), Times.Once);
        }
    }

    public static class Helper
    {
        public static List<OnboardingRequest> GetLoansMock()
        {
            var list = new List<OnboardingRequest>();
            new string[] { "000111", "000222", "000333" }.ToList().ForEach(refNumber =>
            {
                list.Add(new OnboardingRequest
                {
                    FundingSource = "FundingSource",
                    Purpose = "Car financing",
                    ReferenceNumber = refNumber,
                    Terms = new LoanTerms
                    {
                        LoanAmount = 500,
                        OriginationDate = DateTime.Now,
                        Rate = 500,
                        RateType = RateType.Factor,
                        Term = 10,
                        Fees = new List<IFee>
                        {
                            new Fee { Amount = 500,Code = "FeeName1", Type = FeeType.Fixed },
                            new Fee { Amount = 500,Code = "FeeName2", Type = FeeType.Percent },
                        },
                    },
                    Borrowers = GetBorrowersMock().ToArray()
                });
            });
            return list;
        }

        public static List<Borrower> GetBorrowersMock()
        {
            return new List<Borrower>
            {
                new Borrower
                {
                    Addresses = new[]
                    {
                        new Address
                        {
                            City = "Bohemia",
                            IsPrimary = true,
                            Line1 = "(000) 000-00-00",
                            Line2 = "(000) 000-00-00",
                            State = "New York",
                            ZipCode = "BOH0000"
                        },
                        new Address
                        {
                            City = "New Jersey",
                            IsPrimary = false,
                            Line1 = "(000) 000-00-00",
                            Line2 = "(000) 000-00-00",
                            State = "New York",
                            ZipCode = "BOH0000"
                        }
                    },
                    DateOfBirth = DateTime.Now.AddYears(-30),
                    Email = "Email",
                    FirstName = "FirstName",
                    IsPrimary = false,
                    LastName = "LastName",
                    MiddleInitial = "MiddleInitial",
                    Phones = new[]
                    {
                        new Phone { IsPrimary = true, Number = "(000) 000-00-00", Type = PhoneType.Home },
                        new Phone { IsPrimary = false, Number = "(000) 000-00-00", Type = PhoneType.Mobile }
                    },
                    ReferenceNumber = "123456789",
                    SSN = "SSN"
                }
            };
        }
    }
}
