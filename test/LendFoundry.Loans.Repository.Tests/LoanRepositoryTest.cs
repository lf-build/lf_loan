﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Testing.Mongo;
using LendFoundry.Tenant.Client;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LendFoundry.Loans.Repository.Tests
{
    public class LoanRepositoryTest
    {
        private const string TenantId = "tenant001";

        private Mock<ITenantService> tenantServiceMock;

        private Borrower Borrower
        {
            get
            {
                return new Borrower
                {
                    ReferenceNumber = string.Empty,
                    LoanReferenceNumber = string.Empty,
                    IsPrimary = true,
                    FirstName = "John",
                    MiddleInitial = "Z",
                    LastName = "Doe",
                    DateOfBirth = new DateTime(1977, 7, 20),
                    SSN = "721-07-4426",
                    Email = "johnsnow@gmail.com",
                    Addresses = new List<Address>
                    {
                        new Address
                        {
                            IsPrimary = true,
                            Line1 = "196 25th Avenue",
                            City = "San Francisco",
                            State = "CA",
                            ZipCode = "94121"
                        }
                    }.ToArray(),
                    Phones = new List<Phone>
                    {
                        new Phone
                        {
                            IsPrimary = true,
                            Type = PhoneType.Home,
                            Number = "65202108"
                        }
                    }.ToArray()
                };
            }
        }

        private Loan Loan
        {
            get
            {
                return new Loan
                {
                    ReferenceNumber = string.Empty,
                    FundingSource = "ACME",
                    Purpose = "Home improvement",
                    Status = LoanStatus.InService,
                    PaymentMethod = PaymentMethod.ACH,
                    BankAccounts = new[] {
                        new BankAccount
                        {
                            AccountNumber = "123456789",
                            AccountType = BankAccountType.Checking,
                            RoutingNumber = "123456789"
                        }
                    },
                    Terms = new LoanTerms
                    {
                        OriginationDate = new DateTime(2014, 12, 31),
                        LoanAmount = 5.000,
                        Term = 1,
                        Rate = 13.72,
                        RateType = RateType.Percent,
                        Fees = new List<IFee>
                        {
                            new Fee
                            {
                                Amount = 100.00,
                                Code = "Code", /* new property */
                                Type = FeeType.Fixed
                            }
                        },
                        FundedDate = new DateTime(2014, 12, 31),
                        ApplicationDate = new DateTime(2014, 12, 31),
                        APR = 10.5
                    },
                    Investor = new LoanInvestor { Id = "123456", LoanPurchaseDate = DateTime.Now },
                    Scores = new List<IScore> { new Score { InitialDate = DateTime.Now, Name = "Name", InitialScore = "InitialScore", UpdatedDate = DateTime.Now.AddDays(1), UpdatedScore = "UpdatedScore" } },
                    Grade = "Grade",
                    CampaignCode = "CampaignCode",
                    HomeOwnership = "HomeOwnership",
                    MonthlyIncome = 2,
                    PostCloseDti = 2.2,
                    PreCloseDti = 1.1,
                    Borrowers = new List<IBorrower>()
                };
            }
        }

        private IBankAccount BankAccountValue
        {
            get
            {
                return new BankAccount
                {
                    AccountNumber = "123456",
                    AccountType = BankAccountType.Checking,
                    BankName = "Royal Bank",
                    RoutingNumber = "123456",
                    EffectiveDate = DateTimeOffset.Now,
                    IsPrimary = true
                };
            }
        }

        public LoanRepositoryTest()
        {
            tenantServiceMock = new Mock<ITenantService>();

            tenantServiceMock
                .Setup(tenantService => tenantService.Current)
                .Returns(new TenantInfo { Id = TenantId });
        }

        [MongoFact]
        public void Insert()
        {
            MongoTest.Run((config) =>
            {
                // arrange
                var loan = Loan;
                string loanReferenceNumber = "Loan001";
                var repository = GetRepository(config);
                loan.ReferenceNumber = loanReferenceNumber;
                loan.Borrowers = GetListBorrowerMock(loanReferenceNumber);
                repository.Insert(loan);

                // act                
                var result = repository.Get(loanReferenceNumber);

                // assert
                Assert.NotNull(result);
                Assert.Equal(loanReferenceNumber, result.ReferenceNumber);
                Assert.NotNull(result.BankAccounts);
                Assert.NotNull(result.Terms);
                Assert.NotEmpty(result.Terms.Fees);
                Assert.NotNull(result.Investor);
                Assert.NotEmpty(result.Scores);
            });
        }

        [MongoFact]
        public void GetLoanByLoanReferenceNumber()
        {
            MongoTest.Run((config) =>
            {
                // arrange
                var loan = Loan;
                string loanReferenceNumber = "Loan001";
                var repository = GetRepository(config);
                loan.ReferenceNumber = loanReferenceNumber;
                loan.Borrowers = GetListBorrowerMock(loanReferenceNumber);
                repository.Insert(loan);

                // act                
                var result = repository.Get(loanReferenceNumber);

                // assert
                Assert.NotNull(result);
                Assert.Equal(loanReferenceNumber, result.ReferenceNumber);
                Assert.NotNull(result.BankAccounts);
                Assert.NotNull(result.Terms);
                Assert.NotEmpty(result.Terms.Fees);
                Assert.NotNull(result.Investor);
                Assert.NotEmpty(result.Scores);
            });
        }

        [MongoFact]
        public void GetLoansByListOfLoanReferenceNumbers()
        {
            MongoTest.Run((config) =>
            {
                // arrange
                var loan = Loan;
                var loans = new[] { Loan, Loan };
                var repository = GetRepository(config);
                loans = loans.Select((l, i) =>
                {
                    l.ReferenceNumber = $"loan{i}";
                    var borrower = Borrower;
                    borrower.ReferenceNumber = $"borrower{i}";
                    borrower.LoanReferenceNumber = l.ReferenceNumber;
                    l.Borrowers = new List<IBorrower> { borrower };

                    return l;
                }).ToArray();


                foreach (var item in loans)
                    repository.Insert(item);

                // act
                var results = repository.Get(loans.Select(l => l.ReferenceNumber));

                // assert
                Assert.NotNull(results);
                Assert.Equal(loans.First().ReferenceNumber, results.First().ReferenceNumber);
                Assert.Equal(loans.Last().ReferenceNumber, results.Last().ReferenceNumber);
                Assert.NotNull(loans.First().BankAccounts);
                Assert.NotNull(loans.First().Terms);
                Assert.NotEmpty(loans.Last().Terms.Fees);
                Assert.NotNull(loans.Last().Investor);
                Assert.NotEmpty(loans.Last().Scores);
            });
        }

        [MongoFact]
        public void GetLoanByLoanStatus()
        {
            MongoTest.Run((config) =>
            {
                // arrange
                var loan = Loan;
                string loanReferenceNumber = "Loan001";
                var loanStatus = LoanStatus.InService;
                var repository = GetRepository(config);
                loan.ReferenceNumber = loanReferenceNumber;
                loan.Borrowers = GetListBorrowerMock(loanReferenceNumber);
                repository.Insert(loan);

                // act                
                var results = repository.Get(loanStatus);

                // assert
                Assert.NotNull(results);
                Assert.Equal(loanReferenceNumber, results.First().ReferenceNumber);
                Assert.Equal(loanStatus.Code, results.First().Status.Code);

                Assert.NotNull(results.First().BankAccounts);
                Assert.NotNull(results.First().Terms);
                Assert.NotEmpty(results.Last().Terms.Fees);
                Assert.NotNull(results.Last().Investor);
                Assert.NotEmpty(results.Last().Scores);
            });
        }

        [MongoFact]
        public void UpdateLoan_UpdateLoanStatus()
        {
            MongoTest.Run((config) =>
            {
                // arrange
                var loan = Loan;
                string loanReferenceNumber = "Loan001";
                var loanStatus = LoanStatus.InService;
                var repository = GetRepository(config);
                loan.ReferenceNumber = loanReferenceNumber;
                loan.Borrowers = GetListBorrowerMock(loanReferenceNumber);
                repository.Insert(loan);

                // act                
                var result = repository.Get(loanReferenceNumber);

                // assert
                Assert.NotNull(result);
                Assert.Equal(loanReferenceNumber, result.ReferenceNumber);

                result = null;
                repository.UpdateLoanStatus(loanReferenceNumber, LoanStatus.Onboarding);

                result = repository.Get(loanReferenceNumber);

                Assert.NotNull(result);
                Assert.Equal(loanReferenceNumber, result.ReferenceNumber);
                Assert.Equal(LoanStatus.Onboarding.Code, result.Status.Code);

                Assert.NotNull(result.BankAccounts);
                Assert.NotNull(result.Terms);
                Assert.NotEmpty(result.Terms.Fees);
                Assert.NotNull(result.Investor);
                Assert.NotEmpty(result.Scores);
            });
        }

        [MongoFact]
        public void DeleteLoan_ByLoanReferenceNumber()
        {
            MongoTest.Run((config) =>
            {
                // arrange
                var loan = Loan;
                string loanReferenceNumber = "Loan001";
                var repository = GetRepository(config);
                loan.ReferenceNumber = loanReferenceNumber;
                loan.Borrowers = GetListBorrowerMock(loanReferenceNumber);
                repository.Insert(loan);

                // act                
                var result = repository.Get(loanReferenceNumber);

                // assert
                Assert.NotNull(result);
                Assert.Equal(loanReferenceNumber, result.ReferenceNumber);
                Assert.NotNull(result.BankAccounts);
                Assert.NotNull(result.Terms);
                Assert.NotEmpty(result.Terms.Fees);
                Assert.NotNull(result.Investor);
                Assert.NotEmpty(result.Scores);

                repository.Delete(loanReferenceNumber);

                result = repository.Get(loanReferenceNumber);

                Assert.Null(result);
            });
        }

        [MongoFact]
        public void WhenFindByReferenceNumber_HasNullRefNumber_ReturnArgumentException()
        {
            MongoTest.Run((config) =>
            {
                Assert.Throws<ArgumentException>(() =>
                {
                    GetRepository(config).Get("");
                });
            });
        }

        [MongoFact]
        public void WhenInsert_HasNullLoan_ReturnArgumentException()
        {
            MongoTest.Run((config) =>
            {
                Assert.Throws<ArgumentException>(() =>
                {
                    GetRepository(config).Insert(null);
                });
            });
        }

        [MongoFact]
        public void WhenInsert_LoanHasNullBorrowersList_ReturnArgumentException()
        {
            MongoTest.Run((config) =>
            {
                Assert.Throws<ArgumentException>(() =>
                {
                    GetRepository(config).Insert(new Loan());
                });
            });
        }

        [MongoFact]
        public void WhenDelete_HasNullLoan_ReturnArgumentException()
        {
            MongoTest.Run((config) =>
            {
                Assert.Throws<ArgumentException>(() =>
                {
                    GetRepository(config).Delete("");
                });
            });
        }

        public ILoanRepository GetRepository(IMongoConfiguration repoConfig)
        {
            return new LoanRepository(repoConfig, tenantServiceMock.Object);
        }

        public List<IBorrower> GetListBorrowerMock(string loanReferenceNumber)
        {
            return new List<IBorrower>
            {
                new Borrower
                {
                    TenantId = TenantId,
                    ReferenceNumber = "111111",
                    LoanReferenceNumber = loanReferenceNumber,
                    IsPrimary = true,
                    FirstName = "John",
                    MiddleInitial = "Z",
                    LastName = "Doe",
                    DateOfBirth = new DateTime(1977, 7, 20),
                    SSN = "721-07-4426",
                    Email = "johnsnow@gmail.com",
                    Addresses = new List<Address>
                    {
                        new Address
                        {
                            IsPrimary = true,
                            Line1 = "196 25th Avenue",
                            Line2 = "Line2",
                            City = "San Francisco",
                            State = "CA",
                            ZipCode = "94121"
                        }
                    }.ToArray(),
                    Phones = new List<Phone>
                    {
                        new Phone
                        {
                            IsPrimary = true,
                            Type = PhoneType.Home,
                            Number = "65202108"
                        }
                    }.ToArray()
                },
                new Borrower
                {
                    TenantId = TenantId,
                    ReferenceNumber = "222222",
                    LoanReferenceNumber = loanReferenceNumber,
                    IsPrimary = true,
                    FirstName = "John",
                    MiddleInitial = "Z",
                    LastName = "Doe",
                    DateOfBirth = new DateTime(1977, 7, 20),
                    SSN = "721-07-4426",
                    Email = "johnsnow@gmail.com",
                    Addresses = new List<Address>
                    {
                        new Address
                        {
                            IsPrimary = true,
                            Line1 = "196 25th Avenue",
                            Line2 = "Line2",
                            City = "San Francisco",
                            State = "CA",
                            ZipCode = "94121"
                        }
                    }.ToArray(),
                    Phones = new List<Phone>
                    {
                        new Phone
                        {
                            IsPrimary = true,
                            Type = PhoneType.Home,
                            Number = "65202108"
                        }
                    }.ToArray()
                },
            };
        }

        [MongoFact]
        public void AddLoanBankAccount()
        {
            MongoTest.Run((config) =>
            {
                // arrange
                var loan = Loan;
                string loanReferenceNumber = "loan001";
                var repository = GetRepository(config);
                loan.ReferenceNumber = loanReferenceNumber;
                loan.Borrowers = GetListBorrowerMock(loanReferenceNumber);
                loan.BankAccounts = new[] { BankAccountValue };
                repository.Insert(loan);

                // act                
                var result = repository.Get(loanReferenceNumber);

                // assert
                Assert.NotNull(result);
                Assert.NotNull(result.Id);
                Assert.NotNull(result.BankAccounts);
            });
        }

        [MongoFact]
        public void UpdateLoanBankAccount()
        {
            MongoTest.Run((config) =>
            {
                // arrange
                var accountNumber = "999999";
                var loan = Loan;
                string loanReferenceNumber = "loan001";
                var repository = GetRepository(config);
                loan.ReferenceNumber = loanReferenceNumber;
                loan.Borrowers = GetListBorrowerMock(loanReferenceNumber);
                var notPrimaryAccount = BankAccountValue;
                notPrimaryAccount.IsPrimary = false;
                notPrimaryAccount.AccountNumber = accountNumber;
                loan.BankAccounts = new[] { BankAccountValue, notPrimaryAccount };
                repository.Insert(loan);

                // act                
                var result = repository.Get(loanReferenceNumber);

                // assert
                Assert.NotNull(result);
                Assert.NotNull(result.Id);
                Assert.NotNull(result.BankAccounts);
                Assert.True(result.BankAccounts.Count(a => a.IsPrimary == true) == 1);

                // arrange  
                result.BankAccounts = result.BankAccounts.Select(a =>
                {
                    a.IsPrimary = a.AccountNumber == accountNumber;
                    return a;
                }).ToList();

                // act
                repository.UpdateLoanBankAccounts(loanReferenceNumber, result.BankAccounts);
                result = repository.Get(loanReferenceNumber);

                // arrange
                Assert.Equal(accountNumber, result.BankAccounts.First(a => a.IsPrimary == true).AccountNumber);
                Assert.Equal(2, result.BankAccounts.Count());
            });
        }
    }
}