FROM registry.lendfoundry.com/base:beta8

ADD ./global.json /app/

ADD ./src/LendFoundry.Loans.Abstractions /app/src/LendFoundry.Loans.Abstractions
WORKDIR /app/src/LendFoundry.Loans.Abstractions
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Loans.Investors /app/src/LendFoundry.Loans.Investors
WORKDIR /app/src/LendFoundry.Loans.Investors
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Loans.Repository /app/src/LendFoundry.Loans.Repository
WORKDIR /app/src/LendFoundry.Loans.Repository
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Loans.Schedule /app/src/LendFoundry.Loans.Schedule
WORKDIR /app/src/LendFoundry.Loans.Schedule
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Borrowers.Repository /app/src/LendFoundry.Borrowers.Repository
WORKDIR /app/src/LendFoundry.Borrowers.Repository
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Loans.Service /app/src/LendFoundry.Loans.Service
WORKDIR /app/src/LendFoundry.Loans.Service
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Loans.Api /app/src/LendFoundry.Loans.Api
WORKDIR /app/src/LendFoundry.Loans.Api
RUN eval "$CMD_RESTORE"
RUN dnu build

EXPOSE 5000
ENTRYPOINT dnx kestrel
